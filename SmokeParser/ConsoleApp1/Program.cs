﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParserFumo
{
    class Program
    {
        //static List<byte>  tmp = new List<byte>();  //5 X 10Y 5Z -> 250 elementi

        static List<byte> tmp2 = new List<byte>();
        static List<byte> tmp = new List<byte>();
        static List<List<byte>> listaFinale = new List<List<byte>>();
        static List<List<byte>> listaFinaleRidotta = new List<List<byte>>();

        // x = 4 y = 207 z = 4
        static int passox = 2;
        static int passoy = 50;
        static int passoz = 2;

        static int passoX = 17;
        static int passoY = 831;
        static int passoZ = 17;

        static void Main(string[] args)
        {
            //List<List<byte>> listaFinale = new List<List<byte>>();

            string path = "D:\\mrfir\\RepositoryGIT\\frejus_mp_2018_test\\Prodige\\CPCenTreinta_01.s3d";
            //string path = "D:\\beatr\\Dropbox\\Università\\Material Ilaria\\CPCenTreinta_01.s3d";
            var lettore = new BinaryReader(File.OpenRead(path));

            //salto la prima parte composta da 4 + 32 + 4 Byte
            lettore.ReadBytes(40);

            while (true)
            {


                try
                {
                    lettore.ReadBytes(28);
                    int dimBlocco = lettore.ReadInt32();
                    var blocco = lettore.ReadBytes(dimBlocco);
                    List<byte> l = new List<byte>();

                    MemoryStream m = new MemoryStream(blocco);
                    BinaryReader b = new BinaryReader(m);

                    while (true) //decompressione
                    {
                        try
                        {
                            var i = b.ReadByte();
                            if (i == 255)
                            {
                                var valore = b.ReadByte();
                                var ripetizione = b.ReadByte();
                                for (short k = 0; k < ripetizione; k++)
                                {
                                    l.Add(valore);

                                }

                            }
                            else
                            {
                                l.Add(i);
                            }

                        }
                        catch (Exception e)
                        {
                            break;
                        }
                    }
                    listaFinale.Add(l);

                    //listaParticelle.Add(particelle);
                    lettore.ReadBytes(4);
                }
                catch (Exception e)
                {
                    break;

                }


            }






            /*
            for (byte i = 0; i< 250; i++)
            {
                tmp.Add(i);
            }
           
    */
            foreach (var p in listaFinale)
            {
                tmp = p;
                tmp2.Clear();
                for (var z = 0; z < passoz; z++)
                {
                    for (var y = 0; y < passoy; y++)
                    {
                        for (var x = 0; x < passox; x++)
                        {
                            tmp2.Add(TrasformaScala(x, y, z));
                        }
                    }
                }
                listaFinaleRidotta.Add(tmp2);
            }

            List<byte> listaFinaleRidottaByte = new List<byte>();
            foreach (var lista in listaFinaleRidotta)
            {
                foreach (var item in lista)
                {
                    if(item != 0)
                    {

                    }
                    listaFinaleRidottaByte.Add(item);
                }

            }

            //lista finale ridotta ha 4001 liste
            // c# serialize list per scrivere su file lista finale ridotta

            //Serialization
            Serializer.Save("lista.bin", listaFinaleRidottaByte);
            //Deserialization
            // Console.WriteLine(listaFinaleRidottaByte[5427600]);
            listaFinaleRidottaByte = Serializer.Load<List<byte>>("lista.bin");
            //Console.WriteLine(listaFinaleRidottaByte[5427600]);



            //ora hai lista finale con 3 elementi e ognuno contiene 3 elementi con valore 1

            //devi salvare in una nuova lista di liste la versione ridotta ovvero una lista di liste con solo una lista e con valore 1 (media dei 3x3)


        }



        private static byte TrasformaScala(float x, float y, float z)
        {
            var t = TrasformaVettore(x, y, z);
            double sum = 0;
            sum = toLineareValore(t[0], t[1], t[2]);
            /*
            for (var i = -1; i < 2; i++)
            {
                for (var j = -1; j < 2; j++)
                {
                    for (var k = -1; k < 2; k++)
                    {
                        sum += toLineareValore(t[0] + i, t[1] + j, t[2] + k);
                    }
                }
            }*/

            return Convert.ToByte(sum);
        }

        private static List<int> TrasformaVettore(float x, float y, float z)
        {
            var t = new List<int>();
            int X = (int)Math.Floor((x / passox) * passoX) + (int)Math.Floor((double)(passoX) / (2 * passox));

            int Y = (int)Math.Floor((y / passoy) * passoY) + (int)Math.Floor((double)(passoY) / (2 * passoy));

            int Z = (int)Math.Floor((z / passoz) * passoZ) + (int)Math.Floor((double)(passoZ) / (2 * passoz));
            t.Add(X);
            t.Add(Y);
            t.Add(Z);
            return t;
        }
        private static byte toLineareValore(int x, int y, int z)
        {
            if (x < 0)
            {
                x = 0;
            }
            if (y < 0)
            {
                y = 0;
            }
            if (z < 0)
            {
                z = 0;
            }
            return tmp[(y * passoX + x) + (passoX * passoY * z)];
        }
    }
}


