﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using xsens;

[NetworkSettings(channel = 4, sendInterval = 0.001f)] //10 volte al secondo (ma inutile visto che non ci sono SyncVar)
public class DataStreamXsens : NetworkBehaviour
{
    [HideInInspector]
    public XsLiveAnimator anim;

    private void Start()
    {
        //anim = GetComponent<XsLiveAnimator>();
        InitData();
        if (isLocalPlayer && GameManager.Instance.debugTuta)
        {
            StartCoroutine(TimerData());
        }
        
    }

    float counter = 0;
    public void SendData(Vector3[] pos, Quaternion[] rot)
    {
       // Debug.Log("Snd: "+rot[22].ToString() +" "+ rot[22].eulerAngles.ToString());
        //Debug.Log("(" + rot[22].x.ToString("0.00000000") + ", " + rot[22].y.ToString("0.00000000") + ", " + rot[22].z.ToString("0.00000000") + ", " + rot[22].w.ToString("0.00000000") + ")");
        counter += Time.deltaTime;
        if(counter < 0.08)
        {
            return;
        }
        counter = 0;
        StringBuilder sb = new StringBuilder("");
        foreach (var s in pos)
        {
            // sb.Append(s.ToString() + "_");
            sb.Append("" + s.x.ToString("0.0000") + " " + s.y.ToString("0.0000") + " " + s.z.ToString("0.0000") +"_");

        }
        sb.Append(":");
        foreach (var s in rot)
        {
            sb.Append(""+s.x.ToString("0.00000000")+" "+ s.y.ToString("0.00000000")+" "+ s.z.ToString("0.00000000")+" " + s.w.ToString("0.00000000")+"_");
        }

        //Debug.Log(sb.ToString());
        //Vector3[] posa;
        //Quaternion[] rota;
        //SplitToArray(sb.ToString(), out posa, out rota);

        // Send your string over the network

        if (isServer)
        {
            RpcForwardEvent(Zip(sb.ToString()));
        }
        else
        {
            CmdForwardEvent(Zip(sb.ToString()));
        }
       
    }
    void SplitToArray(string str, out Vector3[] pos, out Quaternion[] rot)
    {
       
        var dataRaw = str.Split(':');
        var posRaw = dataRaw[0].Substring(0, dataRaw[0].Length-2).Split('_'); //tolto l'_ in eccesso
        var rotRaw = dataRaw[1].Substring(0, dataRaw[1].Length-1).Split('_');
        //Debug.Log(dataRaw[0]);
        //Debug.Log(dataRaw[1]);
        pos = new Vector3[posRaw.Length];
        rot = new Quaternion[rotRaw.Length];
        int i = 0;
        foreach (var posSinglePos in posRaw)
        {
            var raw = posSinglePos.Substring(0, posSinglePos.Length - 1);
            string[] sArray = raw.Split(' ');
            pos[i] = new Vector3(
                float.Parse(sArray[0]),
                float.Parse(sArray[1]),
                float.Parse(sArray[2])
             );
            i++;
        }

        i = 0;
        foreach (var rotSinglePos in rotRaw)
        {
            var raw = rotSinglePos.Substring(0, rotSinglePos.Length - 1);
            string[] sArray = raw.Split(' ');
            rot[i] = new Quaternion(
                float.Parse(sArray[0]),
                float.Parse(sArray[1]),
                float.Parse(sArray[2]), 
                float.Parse(sArray[3])
             );
            i++;
        }
       // Debug.Log("Ric: (" + rot[22].x.ToString("0.00000000") + ", " + rot[22].y.ToString("0.00000000") + ", " + rot[22].z.ToString("0.00000000") + ", " + rot[22].w.ToString("0.00000000") + ")");

    }


    [Command(channel = 4)]
    void CmdForwardEvent(byte[] data)
    {
        //Debug.Log("Dati spediti: " + data);
        if(data != null)
        {
            RpcForwardEvent(data);
            LogicaAnimazioneRicevuta(data);
        }
          
        
    }

    void LogicaAnimazioneRicevuta(byte[] data)
    {
        if (!isLocalPlayer)
        {
            var stringa = Unzip(data);
            Vector3[] pos;
            Quaternion[] rot;
            SplitToArray(stringa, out pos, out rot);
            if (pos != null && rot != null && anim != null)
            {
                lastRecivedPos = pos;
                lastRecivedRot = rot;
                ultimoUpdate = 0;
            }
        }
    }

    Vector3[] currPos, lastRecivedPos;

    Quaternion[] currRot, lastRecivedRot;

    [ClientRpc(channel = 4)]
    void RpcForwardEvent(byte[] data)
    {
        //Debug.Log("Dati ricevuti: " + data);
        LogicaAnimazioneRicevuta(data);

    }

    public float InvervalloInterpolazione = 0.1f;
    float ultimoUpdate = 0;

    private void LateUpdate()
    {
        ultimoUpdate += Time.deltaTime;
        if (currPos == null && currRot == null && lastRecivedPos != null && lastRecivedRot != null)
        {
            currPos = lastRecivedPos;
            currRot = lastRecivedRot;
        }
        if (currPos != null && currRot != null && lastRecivedPos != null && lastRecivedRot != null)
        {
            for(int i = 0; i< currPos.Length; i++)
            {
                currPos[i] = Vector3.Lerp(currPos[i], lastRecivedPos[i], Math.Min(ultimoUpdate / InvervalloInterpolazione, 1));
            }
            for (int i = 0; i < currRot.Length; i++)
            {
                
                currRot[i] = Quaternion.Lerp(currRot[i], lastRecivedRot[i], Math.Min(ultimoUpdate / InvervalloInterpolazione, 1));               
            }
        }

        if (currPos != null && currRot != null)
        {
            anim.updateMvnActor(anim.currentPose, currPos, currRot);
            anim.updateModel(anim.currentPose, anim.targetModel);
        }
       
    }

    public bool CheckIsLocalPlayer()
    {
        return this.isLocalPlayer;
    }

    IEnumerator TimerData()
    {
        while(true)
        {
            FakeDataGenerator();
            yield return new WaitForSeconds(0.011f);
        }
        
    }


    const int ossa = 22;
    void FakeDataGenerator()
    {
        
        var posLista = new List<Vector3>();
        var rotLista = new List<Quaternion>();
        for(int i =0; i<23; i++)
        {
            posLista.Add(lastRecivedPos[i] + new Vector3(0.01f,0,0));
            rotLista.Add( new Quaternion(
                lastRecivedRot[i].x + UnityEngine.Random.Range(-0.001f, 0.001f),
                lastRecivedRot[i].y - UnityEngine.Random.Range(-0.01f, 0.01f),
                lastRecivedRot[i].z + UnityEngine.Random.Range(-0.0001f, 0.0001f),
                lastRecivedRot[i].w - UnityEngine.Random.Range(-0.001f, 0.001f)));
        }


        lastRecivedPos = posLista.ToArray();
        lastRecivedRot = rotLista.ToArray();

        this.SendData(lastRecivedPos, lastRecivedRot);
        ultimoUpdate = 0;
    }

    void InitData()
    {
        var posLista = new List<Vector3>();
        var rotLista = new List<Quaternion>();
        for (int i = 0; i < 23; i++)
        {
            posLista.Add(new Vector3());
            rotLista.Add(new Quaternion(0,0.1f,0,0));
        }


        lastRecivedPos = posLista.ToArray();
        lastRecivedRot = rotLista.ToArray();
        currPos = posLista.ToArray();
        currRot = rotLista.ToArray();
    }



    static byte[] Zip(string str)
    {
        var bytes = Encoding.UTF8.GetBytes(str);

        using (var msi = new MemoryStream(bytes))
        using (var mso = new MemoryStream())
        {
            using (var gs = new GZipStream(mso, CompressionMode.Compress))
            {
                CopyTo(msi, gs);
            }

            return mso.ToArray();
        }
    }

    static string Unzip(byte[] bytes)
    {
        using (var msi = new MemoryStream(bytes))
        using (var mso = new MemoryStream())
        {
            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
            {
                CopyTo(gs, mso);
            }

            return Encoding.UTF8.GetString(mso.ToArray());
        }
    }

    static void CopyTo(Stream src, Stream dest)
    {
        byte[] bytes = new byte[4096];
        int cnt;
        while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
        {
            dest.Write(bytes, 0, cnt);
        }
    }
}
