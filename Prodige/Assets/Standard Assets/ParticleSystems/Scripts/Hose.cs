using System;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets.Effects
{
    public class Hose : MonoBehaviour
    {
        public float maxPower = 20;
        public float minPower = 5;
        public float changeSpeed = 5;
        public List<ParticleSystem> hoseWaterSystems;
        public AudioSource aSource;

        public float m_Power;
        public bool attivo = false;
        public bool debug = false;

        public bool fakeIdrante = false;

        private void Start()
        {
            //aSource = GetComponent<AudioSource>();
            aSource.volume = 0f;
        }

        // Update is called once per frame
        private void Update()
        {
            /*
           if(debug)
            {
                if (Input.GetKey(KeyCode.K))
                {
                    attivo = true;
                }
                else
                {
                    attivo = false;
                }
            }
           
            m_Power = Mathf.Lerp(m_Power, attivo ? maxPower : minPower, Time.deltaTime*changeSpeed);

            foreach (var system in hoseWaterSystems)
            {
				ParticleSystem.MainModule mainModule = system.main;
                mainModule.startSpeed = m_Power;
                var emission = system.emission;
                emission.enabled = (m_Power > minPower*1.1f);
            }
            */
        }

        public ParticleSystem sparaCollider;
        public void DisattivaColliders()
        {
            if(hoseWaterSystems.Contains(sparaCollider))
            {
                hoseWaterSystems.Remove(sparaCollider);
            }
        }

        public void AccendiAcqua()
        {
            aSource.Play();
            aSource.loop = true;
            if(fakeIdrante)
            {
                SetPower(maxPower);
            }
            attivo = true;
            foreach (var system in hoseWaterSystems)
            {
                system.Play();
            }
        }

        public void SpegniAcqua()
        {
            aSource.Stop();
            attivo = false;
            foreach (var system in hoseWaterSystems)
            {
                system.Stop();
            }
        }
        public void SetPower(float power)
        {
            m_Power = power;
            aSource.volume = power / maxPower;
            foreach (var system in hoseWaterSystems)
            {
                if(system.name == "Hose_Water_Particles")
                {
                    ParticleSystem.MainModule mainModule = system.main;
                    //mainModule.startSpeed.constantMax = m_Power;
                    var emission = system.emission;
                    emission.enabled = (m_Power > minPower * 1.1f);
                }
                else
                {
                    ParticleSystem.MainModule mainModule = system.main;
                    mainModule.startSpeed = m_Power;
                    var emission = system.emission;
                    emission.enabled = (m_Power > minPower * 1.1f);
                }
               
            }
        }

        

    }
}
