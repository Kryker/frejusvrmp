using UnityEngine;
using System.Collections;

public class LogicBreakableRopes : MonoBehaviour
{
    public UltimateRope Rope1;
    public UltimateRope Rope2;

    bool bBroken1;
    bool bBroken2;

    void Start()
    {
        bBroken1 = false;
        bBroken2 = false;
    }



    void OnRopeBreak(UltimateRope.RopeBreakEventInfo breakInfo)
    {
        if(breakInfo.rope == Rope1) bBroken1 = true;
        if(breakInfo.rope == Rope2) bBroken2 = true;
    }
}
