﻿﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace CybSDK
{

    [CustomEditor(typeof(CVirtHapticEmitter))]
    public class CVirtHapticEmitterEditor : Editor
    {

        private static Color orange = new Color(1f, 0.549f, 0f);

        public override void OnInspectorGUI()
        {
            CVirtHapticEmitter targetScript = (CVirtHapticEmitter)target;

            targetScript.timespan = EditorGUILayout.FloatField("Timespan", targetScript.timespan);
            targetScript.autorepeat = EditorGUILayout.Toggle("Autorepeat", targetScript.autorepeat);

            targetScript.distance = EditorGUILayout.FloatField("Distance", targetScript.distance);

            targetScript.forceOverTime = EditorGUILayout.CurveField("Force over Time", targetScript.forceOverTime, Color.green, Rect.MinMaxRect(0, 0, 1, 1), GUILayout.Height(80));
            targetScript.forceOverDistance = EditorGUILayout.CurveField("Force over Distance", targetScript.forceOverDistance, orange, Rect.MinMaxRect(0, 0, 1, 1), GUILayout.Height(80));
        }

        public void OnSceneGUI()
        {
            CVirtHapticEmitter targetScript = (CVirtHapticEmitter)target;

            Handles.color = orange;
            Handles.DrawWireDisc(targetScript.transform.position, targetScript.transform.up, targetScript.distance);
        }

    }

}
