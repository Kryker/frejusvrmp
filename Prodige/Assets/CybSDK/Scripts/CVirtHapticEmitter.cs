﻿using UnityEngine;
using System.Collections;

namespace CybSDK
{

    public class CVirtHapticEmitter : MonoBehaviour
    {

        private CVirtHapticListener hapticListener;

        public float timespan = 3.0f;
        public bool autorepeat = false;
        public float distance = 4.0f;

        public AnimationCurve forceOverTime = AnimationCurve.EaseInOut(0, 1, 1, 0);
        public AnimationCurve forceOverDistance = AnimationCurve.Linear(0, 0, 1, 1);

        private float timeStart;

        // Use this for initialization
        void Start()
        {
            timeStart = Time.time;

            hapticListener = CVirtHapticListener.getInstance();
            if(hapticListener != null)
                hapticListener.AddEmitter(this);
        }

        // Use this for deinitialization
        void OnDestroy()
        {
            if (hapticListener != null)
                hapticListener.RemoveEmitter(this);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public float EvaluateForce(Vector3 listenerPosition)
        {
            float timeDelta = (Time.time - timeStart) % timespan;
            int loopCount = (int)((Time.time - timeStart) / timespan);

            if (autorepeat || loopCount == 0)
            {
                //Evaluate time
                float forceTimeP = forceOverTime.Evaluate(timeDelta / timespan);

                //Evaluate distance
                float dist = Vector3.Distance(this.transform.position, listenerPosition) / distance;
                dist = Mathf.Max(Mathf.Min(dist, 1), 0);
                float forceDistP = 1 - forceOverDistance.Evaluate(dist);
                return forceTimeP * forceDistP;
            }
            else
            {
                return 0;
            }
        }

    }

}
