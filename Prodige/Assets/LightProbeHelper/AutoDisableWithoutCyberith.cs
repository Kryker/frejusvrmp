﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisableWithoutCyberith : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GameManager.Instance != null && GameManager.Instance.Platform != ControllerType.CVirtualizer)
            gameObject.SetActive(false);
        else
        {
            var tc = GameObject.FindGameObjectWithTag("TunnelController");
            if (tc != null && tc.GetComponent<TunnelController>().Platform != ControllerType.CVirtualizer)
                gameObject.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
