﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class TestRemyAgent : MonoBehaviour {
    public GameObject destinazione;
    private NavMeshAgent NVA;
    private ThirdPersonCharacter TPC;
    public bool uscito = false;
	// Use this for initialization
	void Start () {
        NVA = GetComponent<NavMeshAgent>();
        NVA.updateRotation = false;
        TPC = GetComponent<ThirdPersonCharacter>();
       
	}

    // Update is called once per frame
    bool firstTime = true;
    void Update()
    {

        if (uscito)
        {
            if(firstTime)
            {
                NVA.enabled = true;
                NVA.SetDestination(destinazione.transform.position);
                firstTime = false;
            }
            if (NVA.remainingDistance >= NVA.stoppingDistance)
            {
                TPC.Move(NVA.desiredVelocity, false, false);
            }
            else
            {
                TPC.Move(new Vector3(0, 0, 0), false, false);
            }
        }
    }
}
