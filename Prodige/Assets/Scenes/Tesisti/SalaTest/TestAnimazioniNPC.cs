﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimazioniNPC : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(Esci());
	}

    IEnumerator Esci()
    {
        yield return new WaitForSeconds(2f);
        this.GetComponent<Animator>().SetBool("Uscire", true);
    }

        public void InizioUscita()
    {
        GetComponent<FakeParenting>().enabled = false;    
        Debug.Log("Apro porta.");
        StartCoroutine(TrslazioneRitardata());
    }
    public GameObject posFinale;
    IEnumerator TrslazioneRitardata()
    {
        yield return new WaitForSeconds(2f);
        //transform.DOMoveX(this.transform.position.x + 0.6f, 4f);
        transform.DOMoveX(posFinale.transform.position.x-0.2f , 4.5f);
        yield return new WaitForSeconds(2.8f);
        transform.DOMoveX(posFinale.transform.position.x, 2.2f);
        transform.DOMoveY(posFinale.transform.position.y, 2.2f);

        //transform.DOMoveY(this.transform.position.y - 1.05f, 4f);

    }

    public void ScesoDalMezzo()
    {
        Debug.Log("Sceso dal mezzo.");
    }
}
