﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class NetworkMaster : NetworkBehaviour {

    public NetworkSlave slave;
    public virtual void Start()
    {
        slave.masterNet = this;
    }

    public virtual void Update() { }
}
