﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManigliaFireTruckNet : GenericItemNet
{
    public override void Interact(ItemControllerNet c, ControllerHand hand) {
        base.Interact(c, hand);
        var p = c.GetComponent<PlayerStatusNet>();
        if (p != null && p.Ruolo == SpawnMode.Componente)
        {
            if(isServer)
            {
                GameManager.Instance.TCN.GPN.SetRitornoAlCamion(true, SpawnMode.Componente);
            }
            else
            {
                GameManager.Instance.TCN.GPN.CmdRitornoAlCamion(true, SpawnMode.Componente);
            }
            
            GameManager.Instance.TCN.EndGame();
        }
    }


}
