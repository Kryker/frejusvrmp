﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeWeaveController : MonoBehaviour {


    public float toxicity = 10.0f;
    public float timeBetweenWaeave = 2.0f;
    public float weaveSpeed = 1.0f;
    public float overallSpeed = 1.0f;
    [SerializeField]
    private bool started = false;
    public GameObject weave;

    // Use this for initialization
    void Start () {
	}
    // other.gameObject.SendMessageUpwards("Intoxicate", Toxicity, SendMessageOptions.DontRequireReceiver);
    // Update is called once per frame

    private float timeToNext = 0;
    private double timeEspleased = 0;
    private bool side = true;
    void Update () {
		if(started)
        {
            timeEspleased += Time.deltaTime;
            timeToNext -= Time.deltaTime;
            
            if (timeToNext < 0)
            {
                timeToNext = timeBetweenWaeave;
                if(GameManager.Instance.DannoDaFumo)
                {
                    CreateWeave();
                }
               
            }
     }

	}

    public void StartSmoke()
    {
        started = true;
    }

    void CreateWeave()
    {
        var child = Instantiate(weave, this.transform).GetComponent<SmokeWeave>();
        side = !side;
        child.speed = side ? weaveSpeed : -weaveSpeed;       // mando onde in enrambe dle direzioni
        child.toxicity = toxicity;
        child.lifeTime = timeEspleased/ overallSpeed/ weaveSpeed;
       
    }


}
