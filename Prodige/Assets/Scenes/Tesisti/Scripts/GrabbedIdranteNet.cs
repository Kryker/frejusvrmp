﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GrabbedIdranteNet : GrabbableItemNet
{
    //public ParticleSystem extEmit;
    public UnityStandardAssets.Effects.Hose hose;
    [SyncVar(hook="CambioRotazione")]  //applico la rotazione extra
    float rotation = 0;
    Quaternion levaInitialRotation;
    public LevaIdranteNet leva;

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        ContattoLeva(true, 1);
    }


    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        ContattoLeva(false, 0);
    }


    public override void Update()
    {
        base.Update();
        if (initialized)
        {
          
           
        }

       
    }

    public override void SaveState()
    {
        var p = (IdranteNet)Item;
        p.SonoStatoDroppato();
        base.SaveState();

    }

    private void CambioStatoFuoco(bool firing)
    {
        if(firing)
        {
            hose.AccendiAcqua();
        }
        else
        {
            hose.SpegniAcqua();
        }
    }
    /// other
    /// 
    [SyncVar(hook = "CambioStatoFuoco")]
     bool firing;
    public override void Start()
    {
        if (isServer)
            firing = false;
        else
        {
            //var c = extEmit.collision;
            //c.sendCollisionMessages = false;
            hose.DisattivaColliders();
        }
        base.Start();
        levaInitialRotation = leva.Slave.transform.localRotation;
    }
    public override void Initialize()
    {
        if (!initialized)
        {
            base.Initialize();
            if (initialized)
            {
                var i = Player.GetComponent<InputManagementNet>();

                if (Player.Type != ItemControllerType.VR)
                {
                    var c1 = Player;
                    if (c1 != null)
                    {
                        if (controller == c1.LeftController)
                        {
                            i.OnLeftMouseClicked += ClickButton;
                            i.OnLeftMouseUnclicked += UnClickButton;
                        }
                        else if (controller == c1.RightController)
                        {
                            i.OnRightMouseClicked += ClickButton;
                            i.OnRightMouseUnclicked += UnClickButton;
                        }
                    }
                }
               
            }
        }
    }
    [HideInInspector]
    public VibrationController vibr;
    public override void SetUp(ItemControllerNet ic, ControllerHand hand, int i)
    {
        base.SetUp(ic, hand, i);
        if (isServer)
        {
            if (ic != null && ic.Type == ItemControllerType.VR)
            {
                if (ic.isLocalPlayer)
                {
                    if (hand == ControllerHand.LeftHand)
                        vibr = ic.LeftController.GetComponent<VibrationController>();
                    else if (hand == ControllerHand.RightHand)
                        vibr = ic.RightController.GetComponent<VibrationController>();
                }
                else
                    RpcSetVibrationController(ic.GetComponent<NetworkIdentity>(), hand);
            }
        }
    }
    [ClientRpc]
    private void RpcSetVibrationController(NetworkIdentity netID, ControllerHand hand)
    {
        if (netID != null)
        {
            var ic = netID.GetComponent<ItemControllerNet>();
            if (ic != null && ic.Type == ItemControllerType.VR && ic.isLocalPlayer)
            {
                if (hand == ControllerHand.LeftHand)
                    vibr = ic.LeftController.GetComponent<VibrationController>();
                else if (hand == ControllerHand.RightHand)
                    vibr = ic.RightController.GetComponent<VibrationController>();
            }
        }
    }

    //TODO: gestire la rotazione in base alla percentuale
    float lastPerc = 0;
    public float maxVibration = 0.9f;
    public float maxPower = 20f;
    public void ContattoLeva(bool attivo, float percentuale = 0f)
    {
       
        if (Item == null)
        { return; }
        percentuale = Math.Max(0, Math.Min(percentuale, 1));
        if (attivo && percentuale > 0)
        {
            if(!firing)
            {
               // Debug.Log("click");
                hose.AccendiAcqua();
                firing = true;
                if (vibr != null)
                {
                    lastPerc = percentuale * maxVibration - 0.1f;
                    if(lastPerc > 0)
                    {
                        vibr.StartVibration(0.05f, lastPerc, 0.04f, this);
                    }
                   
                }

            }
            if (firing)
            {
                percentuale = Math.Min(Math.Abs(percentuale), 20);
                this.SetRotation(percentuale);
                hose.SetPower(percentuale*maxPower);
                if (vibr != null && lastPerc != percentuale)
                {
                    vibr.StopVibration(0.05f, lastPerc, 0.04f, this);
                    lastPerc = percentuale* maxVibration -0.1f;
                    if (lastPerc > 0)
                    {
                        vibr.StartVibration(0.05f, lastPerc, 0.04f, this);
                    }
                   

                }

            }

        }
        else
        {
            hose.SpegniAcqua();
            this.SetRotation(0);
            firing = false;
            //Debug.Log("unclick");
            if (vibr != null)
            {
                vibr.StopVibration(0.05f, lastPerc, 0.04f, this);
            }
        }
       
    }

    void SetRotation(float percentuale)
    {
        rotation = percentuale;
    }

    Vector3 minusZedAxe = new Vector3(0, 0, -1); //meno chiamate alla GC
    public float RotationAmplitude = 20f;
    void CambioRotazione(float percentuale)
    {
        rotation = percentuale;
        if (initialized)
        {
            leva.Slave.transform.localRotation = levaInitialRotation;
            leva.Slave.transform.Rotate(minusZedAxe, rotation* RotationAmplitude);
        }
    }
}
