﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HeavyTruckNPC : CarNPC
{

    // Use this for initialization
    public ParticleSystem fumoCamionFrontale;


    public void ActivateSmoke()
    {
        fumoCamionFrontale.Play();
        RpcActivateSmoke();
        //qui va comunicato al tunnel controller di avviare la procedura del pompiere
        GameManager.Instance.TCN.StartedSmokeTruck();
    }

    [ClientRpc]
    private void RpcActivateSmoke()
    {
        fumoCamionFrontale.Play();
    }
}
