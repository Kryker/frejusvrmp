﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GrabbedForbiciNet : GrabbableItemNet
{
    private NetworkAnimator NA;
    
    private CanBeCutNet[] oggettiTagliabili;
    public Collider colliderForbici;
    public AudioClip suonoInUso;
    public AudioSource sorgenteAudio;

    public override void Start()
    {
        base.Start();
        NA = GetComponent<NetworkAnimator>();
        oggettiTagliabili = FindObjectsOfType<CanBeCutNet>();
        sorgenteAudio.loop = false;
        sorgenteAudio.Stop();
    }

    [ClientRpc]
    void RpcRiproduciAudioForbici()
    {
        sorgenteAudio.PlayOneShot(suonoInUso);
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        if(Item == null || !isServer)
        { return; }
        //Debug.Log("click");

        sorgenteAudio.PlayOneShot(suonoInUso);
        RpcRiproduciAudioForbici();
        NA.animator.SetBool("Chiusa",true);
            if (vibr != null)
            {
                vibr.StartVibration(0.05f, 0.65f, 0.04f, this);
            }

        var i = Player.GetComponent<InputManagementNet>();

        if (Player.Type == ItemControllerType.VR)
        {
            foreach (var oggetto in oggettiTagliabili)
            {
                if (colliderForbici.bounds.Intersects(oggetto.collider.bounds))
                {
                    oggetto.SonoStatoTagliato();
                }
            }
        }
        else
        {
            var ICN = this.Player.GetComponent<ItemControllerNet>();

            string[] layers = new string[4];
            layers[0] = "Cintura";
            var mask = LayerMask.GetMask(layers);
            var hits = ICN.RayCaster.RayCastAll(ICN.RaycastMaxDistance, mask);

            if (hits.Length > 0)
            {
                foreach (RaycastHit hit in hits)
                {
                    var o = hit.collider.transform;
                    if(o.name == "Cintura" || o.name == "CinturaOK")
                    {
                         var tmp = o.GetComponent<NetworkSlave>().masterNet;
                        if(tmp != null)
                        {
                            ((CinturaNet)tmp).Taglia();
                        } else
                        { Debug.LogError("errore nel cast della cintura?"); }



                    }
                }
            }
        }
    }


    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        if (Item == null)
        { return; }
       
        //Debug.Log("unclick");
        NA.animator.SetBool("Chiusa", false);
        if (vibr != null)
        {
            vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
        }
    }
    /*
    public override void SaveState()
    {
        if (isServer)
            NA.animator.SetBool("Chiusa", false);
        if (vibr != null)
            vibr.StopVibration(0.05f, 0.65f, 0.04f, this);     
        base.SaveState();
    }
    */

    public override void Initialize()
    {
        if (!initialized)
        {
            base.Initialize();
            if (initialized)
            {
                var i = Player.GetComponent<InputManagementNet>();

                if (Player.Type == ItemControllerType.VR)
                {
                    var ic = controller.GetComponent<ControllerManager>();
                    if (ic != null)
                    {
                        if (ic.Hand == ControllerHand.LeftHand)
                        {
                            i.OnLeftPadPressed += ClickButton;
                            i.OnLeftPadUnpressed += UnClickButton;
                        }
                        else if (ic.Hand == ControllerHand.RightHand)
                        {
                            i.OnRightPadPressed += ClickButton;
                            i.OnRightPadUnpressed += UnClickButton;
                        }
                    }
                }
                else
                {
                    var c1 = Player;
                    if (c1 != null)
                    {
                        if (controller == c1.LeftController)
                        {
                            i.OnLeftMouseClicked += ClickButton;
                            i.OnLeftMouseUnclicked += UnClickButton;
                        }
                        else if (controller == c1.RightController)
                        {
                            i.OnRightMouseClicked += ClickButton;
                            i.OnRightMouseUnclicked += UnClickButton;
                        }
                    }
                }
            }
            this.Slave.transform.localPosition = Vector3.zero;
            this.Slave.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }

       
    }
    [HideInInspector]
    public VibrationController vibr;
    //this.Slave.transform.rotation = Quaternion.Euler(0, 90, 0);

    public override void SetUp(ItemControllerNet ic, ControllerHand hand, int i)
    {
        base.SetUp(ic, hand, i);
       
        if (isServer)
        {
            if (ic != null && ic.Type == ItemControllerType.VR)
            {
                if (ic.isLocalPlayer)
                {
                    if (hand == ControllerHand.LeftHand)
                        vibr = ic.LeftController.GetComponent<VibrationController>();
                    else if (hand == ControllerHand.RightHand)
                        vibr = ic.RightController.GetComponent<VibrationController>();
                }
                else
                    RpcSetVibrationController(ic.GetComponent<NetworkIdentity>(), hand);
            }
        }
    }

    [ClientRpc]
    private void RpcSetVibrationController(NetworkIdentity netID, ControllerHand hand)
    {
        if (netID != null)
        {
            var ic = netID.GetComponent<ItemControllerNet>();
            if (ic != null && ic.Type == ItemControllerType.VR && ic.isLocalPlayer)
            {
                if (hand == ControllerHand.LeftHand)
                    vibr = ic.LeftController.GetComponent<VibrationController>();
                else if (hand == ControllerHand.RightHand)
                    vibr = ic.RightController.GetComponent<VibrationController>();
            }
        }
    }

    public override void Update()
    {
        base.Update();
      
    }

}
