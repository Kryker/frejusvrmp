﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevaIdranteNet : GenericItemNet
{

    public  GrabbedIdranteNet _GrabbedIdrante;
    public Transform puntaLeva;
    bool isHanded = false; //NB questa si usa solo con il VR, in caso di mouse si controlla direttamente il ContattoLeva() al livello superiore (GrabbedIdranteNet)
    public Vector3 maxDistance;
    public float maxElongazioneLeva;
    ControllerHand _hand;

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();
        //seguo la mano del player finche rimane dentro parametri di sicurezza
        //this.GrabbedIdrante.Slave.transform.rotation = ;
        if (isHanded)
        {
            Vector3 controller = new Vector3();
            //l'asse cambia devo usare le local!
            Vector3 PosizioneLeva = this.puntaLeva.localPosition;
            switch (_hand)
            {
            case ControllerHand.LeftHand:
                controller = this.puntaLeva.InverseTransformPoint(Player.VRLeftController.position);
                break;
            case ControllerHand.RightHand:
                controller = this.puntaLeva.InverseTransformPoint(Player.VRRightController.position);
                break;
            }
            
            
            if (Math.Abs(controller.z - PosizioneLeva.z) > maxDistance.z  //tolleranza ai lati
                  ||
                  Math.Abs(controller.y - PosizioneLeva.y) > maxDistance.y   //tolleranza ai lati
                  ||
                  controller.x - PosizioneLeva.x < -0.2 //droppa superato il limite opposto
                  ||
                  controller.x - PosizioneLeva.x > maxDistance.x)   //droppa se tira troppo
            {
                FineInterazioneLeva(null, new ClickedEventArgs());
            }
            else
            {
                //sono nei limiti, ruoto la leva in maniera appropriata
                //_GrabbedIdrante.ContattoLeva(true, 20);
                
                _GrabbedIdrante.ContattoLeva(true, (controller.x - PosizioneLeva.x) / maxElongazioneLeva);
            }
        }
      
      
    }



    public override void Interact(GenericItemSlave slave, ItemControllerNet cn, ControllerHand hand)
    {
        if (_GrabbedIdrante.Player != null && _GrabbedIdrante.Player == cn) //puoi interagire solo se stai tenendo in mano tu il tubo, da cambiare se volgiamo multi pompiere
        {
            Interact(cn, hand);
        }
    }
    
    //inizio tocco della leva
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        isHanded = true;

        // TODO: Left e right da verificare mano
        _hand = hand;
        switch (hand)
        {
            case ControllerHand.LeftHand:
                c.GetComponent<InputManagementNet>().OnLeftTriggerUnclicked += FineInterazioneLeva;
                break;
            case ControllerHand.RightHand:
                c.GetComponent<InputManagementNet>().OnRightTriggerUnclicked += FineInterazioneLeva;
                break;
        }
        
        this.Player = c;
        SetCanInteract(false, null);
      
    }

    private void FineInterazioneLeva(object sender, ClickedEventArgs e)
    {
        if(isHanded)
        {
            isHanded = false;
            switch (_hand)
            {
                case ControllerHand.LeftHand:
                    this.Player.GetComponent<InputManagementNet>().OnLeftTriggerUnclicked -= FineInterazioneLeva;
                    break;
                case ControllerHand.RightHand:
                    this.Player.GetComponent<InputManagementNet>().OnRightTriggerUnclicked -= FineInterazioneLeva;
                    break;
            }
            SetCanInteract(true, null);
            this.Player = null;
            _GrabbedIdrante.ContattoLeva(false, 0);
        }
      
    }

}

