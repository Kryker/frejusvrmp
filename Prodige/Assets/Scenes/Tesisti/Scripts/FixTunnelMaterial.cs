﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixTunnelMaterial : MonoBehaviour {

    // Use this for initialization

    public Obi.ObiCollisionMaterial materialeStrada;
	void Start () {
        var strade = GetComponentsInChildren<Obi.ObiCollider>(true);
        foreach(var strada in strade)
        {
            strada.CollisionMaterial = materialeStrada;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
