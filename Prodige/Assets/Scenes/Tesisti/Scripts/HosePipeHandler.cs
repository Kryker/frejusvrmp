﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class HosePipeHandler : MonoBehaviour {

    public ObiCollider AttaccoHose;
    public ObiCollider IngressoAcqua;
    public float hookExtendRetractSpeed = 0.2f;
    public Material material;
    public float stiffness = 0.997f;
    public float slack = 0;
    public float stretchingScale = 1f;
    public float bending = 0.2f;
    public float stiffneddBending = 0.9f;
    public float resolution = 0.2f;
    public uint smoothing = 2;
    public ObiCollisionMaterial materialeFisico;
    public ObiRopeSection section;
    public int ParticelleExtra = 20;

    protected ObiRope rope;
    protected ObiCatmullRomCurve curve;  
    protected ObiRopeCursor cursor;

    [HideInInspector]
    public bool attached = false;
    [HideInInspector]
    public RopeHandlerNet masterNet;
    [HideInInspector]
    public ObiSolver solver;
    //[HideInInspector]
    //public int index;

    protected virtual void Awake()
    { 
        rope = this.gameObject.AddComponent<ObiRope>();
        curve = this.gameObject.AddComponent<ObiCatmullRomCurve>();

    }

   

    public virtual void InitRope(float delay)
    {
        StartCoroutine(InitWithDelay(delay));
    }

    protected virtual void Start() { }

    public virtual  IEnumerator InitWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        // Create both the rope and the solver:	
       

        // Provide a solver and a curve:
        rope.pooledParticles = ParticelleExtra;
        rope.Solver = masterNet.solver;
        rope.ropePath = curve;
        rope.GetComponent<MeshRenderer>().material = material;
        rope.CollisionMaterial = materialeFisico;

        // Configure rope and solver parameters:
        rope.resolution = resolution;
        rope.smoothing = smoothing;
            

        rope.uvScale = new Vector2(1, 5);
        rope.normalizeV = false;
        rope.uvAnchor = 1;
        rope.section = section;
        //
        rope.DistanceConstraints.stretchingScale = stretchingScale;
        rope.DistanceConstraints.stiffness = stiffness;//0.9
        rope.DistanceConstraints.slack = slack;//0.9

        rope.BendingConstraints.maxBending = bending;
        rope.BendingConstraints.stiffness = stiffneddBending;
       
        //rope.pooledParticles = 100; //questo forse rompe.
        //


        rope.sectionThicknessScale = 0.41f;
        // Add a cursor to be able to change rope length:
        cursor = this.gameObject.AddComponent<ObiRopeCursor>();
        cursor.normalizedCoord = 1f;
        cursor.direction = true;
    }

 

    public virtual void LaunchHook()
    {
         StartCoroutine(AttachHook());
    }


    protected virtual IEnumerator AttachHook()
    {
        yield return new WaitForSeconds(0.5f);
        if(!attached)
        {
            attached = true;

            Vector3 localHit = curve.transform.InverseTransformPoint(AttaccoHose.transform.position);

            // Procedurally generate the initial rope shape (a simple straight line):
            curve.controlPoints.Clear();

            curve.controlPoints.Add(Vector3.zero);
            curve.controlPoints.Add(Vector3.zero);
            curve.controlPoints.Add(localHit);
            curve.controlPoints.Add(localHit);

            // Generate the particle representation of the rope (wait until it has finished):
            yield return rope.GeneratePhysicRepresentationForMesh();

            // Pin both ends of the rope (this enables two-way interaction between character and rope):
            ObiPinConstraintBatch pinConstraints = rope.PinConstraints.GetFirstBatch();
            pinConstraints.AddConstraint(0, IngressoAcqua, Vector3.zero, 1);

            pinConstraints.AddConstraint(rope.UsedParticles - 1, AttaccoHose,
                                                            Vector3.zero, 1);

            // Add the rope to the solver to begin the simulation:
            rope.AddToSolver(null);
            rope.GetComponent<MeshRenderer>().enabled = true;
        }
       

        
    }

    protected virtual void DetachHook()
    {
        /*
        // Detach hook:
        rope.RemoveFromSolver(null);
        rope.GetComponent<MeshRenderer>().enabled = false;

        attached = false;
        */
    }
    bool once = true;
    public bool extendible = true;
    bool tempoPassato = false;
    void Update()
    {
        if (attached && extendible)
        {
            if(once)
            {
                once = false;
                StartCoroutine(EstendiConRitardo());
            }
            if(tempoPassato)
            {
                cursor.ChangeLength(rope.RestLength + hookExtendRetractSpeed * Time.deltaTime);
            }
           
        }

        }

    IEnumerator EstendiConRitardo ()
    {
        yield return new WaitForSeconds(3.0f);
        tempoPassato = true;
        yield return new WaitForSeconds(28.0f);
        tempoPassato = false;

    }
}

