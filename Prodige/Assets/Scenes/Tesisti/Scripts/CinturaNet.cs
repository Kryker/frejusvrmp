﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CinturaNet : NetworkMaster {
    [SyncVar(hook = "CambioStatoCintura")]
    bool tagliata = false;
    public event MyDelegate cinturaTagliata;
    public GameObject ScisorPrefabGrabbed;
    public CinturaNet Altra;
    public CanBeCutNet Relativo;
    public GameObject veraCintura;
    public bool posizioneCorretta = false;

    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            Relativo.OggettoTagliato += OggettoTagliato;
        }
    }
       
        

    public void Taglia()
    {
        if (!tagliata)
        {
            OggettoTagliato();
        }
    }
    
    public void OggettoTagliato()
    {
        if(isServer)
        {
            MyOggettoTagliato();
        }
        else
        {
            CmdOggettoTagliato();
        }
        
    }

    void MyOggettoTagliato()
    {
        if (!tagliata)
        {
            tagliata = true;
            CambioStatoCintura(tagliata);
            Altra.tagliata = true;
            if (posizioneCorretta)
            {
                GameManager.Instance.TCN.GPN.TaglioCinturaOk = true;
            }
        }
    }

    [Command]
    public void CmdOggettoTagliato()
    {
        MyOggettoTagliato();
    }

    private void CambioStatoCintura(bool tagliata)
    {
        if (tagliata)
        {
            if(cinturaTagliata != null)
            {
                cinturaTagliata.Invoke();
            }
            
            this.slave.gameObject.SetActive(false);
            veraCintura.SetActive(false);
        }
        else
        {
            Debug.Log("Magia? si è riparata la cintura?");
            this.slave.gameObject.SetActive(true);
            veraCintura.SetActive(true);

        }
    }




}
