﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class NPCCiviliBaseNet : NPCBaseNet
{
    public bool hideFromStart = true;
    [HideInInspector]
    //[SyncVar(hook= "CambioVisibilita")]
    public bool visibleSkin = true;

    public Animator portaEmergenza;
    public Animator portaEmergenzaInterna;

    protected SkinnedMeshRenderer[] parts;

    public Transform posizioneFinaleRispettoAlMezzo;
    protected override void Start()
    {
        if(hideFromStart)
        {
            parts = GetComponentsInChildren<SkinnedMeshRenderer>();
            foreach (var el in parts)
            {
                el.enabled = false;
            }
        }
       
        base.Start();
        
    }

    public virtual void CambioVisibilita(bool status)
    {
        visibleSkin = status;
        foreach (var el in parts)
        {
            el.enabled = status;
        }
    }

    [ClientRpc]
    public virtual void RpcCambioVisibilita(bool status)
    {
        CambioVisibilita(status);
    }

    public float tempoUscitaMezzo = 4.5f;
    public virtual void InizioUscita()
    {      
        if (!isServer) { return; }
        StartCoroutine(TraslazioneUscendo());
    }

    public float ritardoTraslazioneUscendo = 0.2f;
    public bool attivaTralsazione = false;
    protected virtual IEnumerator TraslazioneUscendo()
    {
        yield return new WaitForSeconds(ritardoTraslazioneUscendo);
        if(attivaTralsazione)
        {
            this.transform.DOMove(posizioneFinaleRispettoAlMezzo.position, tempoUscitaMezzo);
        }
       // 
    }



    protected override void DestinazioneRaggiunta()
    {
        if (!isServer) { return; }
        switch (pathCounter)
        {
            case 0:
                //non esiste
                break;
            case 1:
                portaEmergenza.SetBool("OpenPorta", true);
                NextPath();
                break;
            case 2:
                portaEmergenzaInterna.SetBool("OpenPorta", true);
                NextPath();
                break;
            case 3:
                //salvezza
                break;
        }
    }
}
