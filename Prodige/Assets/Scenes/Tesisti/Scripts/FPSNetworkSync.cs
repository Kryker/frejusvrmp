﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//[RequireComponent(typeof(CustomNetworkTransformChild))]
[NetworkSettings(channel = 0, sendInterval = 0.016f)]
public class FPSNetworkSync : NetworkBehaviour
{

    [SerializeField]
    [Range(0, 120)]
    private float NetworkSendRate = 9;
    [SerializeField]
    private GameObject Eye;
    
    //[SerializeField]
    //private float MovementThreshold = 0.001f;
    [SerializeField]
    private float snapThreshold = 5f;
    [SerializeField]
    private float InterpolateMovementFactor = 1;
    [SerializeField]
    private float InterpolateRotationFactor = 1;
    // [SyncVar(hook = "OnPositionReceived")]
    //[SyncVar(hook = "OnRotationReceived")]


    private Vector3 _lastPosChar;
    private Quaternion _lastRotationChar;


    private Vector3 _lastPositionEye;
    private Quaternion _lastRotationEye;




    [HideInInspector]

    float NextSend = 0;
    float LastSend = 0;
    float _t_r = 0;
    float _t_l = 0;
    // Use this for initialization

    void Start()
    {
        if (isServer)
        {
            Vector3 _lastPosChar = this.transform.position;
            Quaternion _lastRotationChar = this.transform.rotation;

            Vector3 _lastPositionEye = Eye.transform.localPosition;
            Quaternion _lastRotationEye = Eye.transform.localRotation;

        }
    }

    private void LateUpdate()
    {
        if (!hasAuthority)
        {
            Interpolate();
        }
    }


    private void Update()
    {
        var now = Time.time;
        if (hasAuthority && NetworkSendRate > 0 && now > NextSend)
        {
           if(!isServer)
            {
                CmdSendPosition(this.transform.position,
                    Eye.transform.localPosition);
                CmdSendRotation(this.transform.rotation, Eye.transform.localRotation);
            }
           else
            {
                RpcSendPosition(this.transform.position, Eye.transform.localPosition);
                RpcSendRotation(this.transform.rotation, Eye.transform.localRotation);
            }


            NextSend = now + 1 / NetworkSendRate;
        }
    }

    [Command]
    private void CmdSendPosition(Vector3 Char, Vector3 Eye)
    {
        _t_l = 0;
        _lastPosChar = Char;
        _lastPositionEye = Eye;
        RpcSendPosition(Char, Eye);
    }

    [ClientRpc]
    private void RpcSendPosition(Vector3 Char, Vector3 Eye)
    {
        _t_l = 0;
        _lastPosChar = Char;
        _lastPositionEye = Eye;
    }

    [Command]
    private void CmdSendRotation(Quaternion Char, Quaternion Eye)
    {
        _t_r = 0;
        _lastRotationChar = Char;
        _lastRotationEye = Eye;
        RpcSendRotation(Char, Eye);
    }

    [ClientRpc]
    private void RpcSendRotation(Quaternion Char, Quaternion Eye)
    {
        _t_r = 0;
        _lastRotationChar = Char;
        _lastRotationEye = Eye;
    }

    private void Interpolate()
    {
        if (InterpolateMovementFactor == 0 || Vector3.Distance(this.transform.position, _lastPosChar) > snapThreshold)
             this.transform.position = _lastPosChar;
        else
        {
            _t_l += (Time.deltaTime * InterpolateMovementFactor);
            if (_t_l > 1)
                _t_l = 1;
 
            this.transform.localPosition = Vector3.Lerp(this.transform.position, _lastPosChar, _t_l);   
        }

        if (InterpolateRotationFactor == 0)
            this.transform.rotation = _lastRotationChar;

        else
        {
            _t_r += (Time.deltaTime * InterpolateRotationFactor);
            if (_t_r > 1)
                _t_r = 1;
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, _lastRotationChar, _t_r);

        }

        Eye.transform.localPosition = _lastPositionEye;
        Eye.transform.localRotation = _lastRotationEye;

    }


}
