﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class GestorePunteggiNet : NetworkBehaviour
{
    public struct Punteggi //uso class al posto di struct per poter usare puntatori senza andare in unsafe:(
    {
        public bool PrioritaAlCivile;
        public bool MantenuteDistanzeSicurezzaIcendio;
        public bool IncendioSpentoEntro5Min;
        public bool RitornoAlCamion;
        public bool CivileSoccorso;
        public bool TaglioCinturaOk;
        public bool AttaccoIncendioBasso;

        public bool _SOSRequested;
        public float SOSRequestedTime;
        public bool DistanceMaintained;
        public float AlarmTriggeredTime;
        public bool _AlarmTriggered;
        public bool _arrowsOn;
        public float ArrowsOnTime;
        public bool _engineOff;
        public float EngineOffTime;
        public bool estintoreUsato;
    };
    #region vars
    //Vigile del fuoco:
    
    [SyncVar]
    public bool PrioritaAlCivile = true;
    [SyncVar]
    public bool MantenuteDistanzeSicurezzaIcendio = true;
    [SyncVar]
    public bool IncendioSpentoEntro5Min = false;
    [SyncVar]
    public bool RitornoAlCamion = false;
    [SyncVar]
    public bool CivileSoccorso = false;
    [SyncVar]
    public bool TaglioCinturaOk = false;
    [SyncVar]
    public bool AttaccoIncendioBasso = false;

    //camionista:
    [SyncVar]
    public bool estintoreUsato = false; //solo camionista la userà

    //solo guidatore:
    [SyncVar]
    public bool DistanceMaintained = true; //ha ancora senso?

    //condivise camionista e guidatore:
    //versione Guidatore:
    [SyncVar]
    public bool Car_SOSRequested = false;
    [SyncVar]
    public float CarSOSRequestedTime = float.MinValue;
    [SyncVar]
    public float CarAlarmTriggeredTime = float.MinValue;
    [SyncVar]
    public bool Car_AlarmTriggered = false;
    [SyncVar]
    public bool Car_arrowsOn = false;
    [SyncVar]
    public float CarArrowsOnTime = float.MinValue;
    [SyncVar]
    public bool Car_engineOff = false;
    [SyncVar]
    public float CarEngineOffTime = float.MinValue;
    //versione Camionista:
    [SyncVar]
    public bool Truck_SOSRequested = false;
    [SyncVar]
    public float TruckSOSRequestedTime = float.MinValue;
    [SyncVar]
    public float TruckAlarmTriggeredTime = float.MinValue;
    [SyncVar]
    public bool Truck_AlarmTriggered = false;
    [SyncVar]
    public bool Truck_arrowsOn = false;
    [SyncVar]
    public float TruckArrowsOnTime = float.MinValue;
    [SyncVar]
    public bool Truck_engineOff = false;
    [SyncVar]
    public float TruckEngineOffTime = float.MinValue;
    #endregion

    public void Reset()
    {
        
    }
    public Punteggi OttieniPunteggi(SpawnMode ruolo)
    {
        var score = new Punteggi();
        switch(ruolo)
        {
            case SpawnMode.Driver:
                score.AlarmTriggeredTime = CarAlarmTriggeredTime;
                score._AlarmTriggered = Car_AlarmTriggered;
                score._arrowsOn = Car_arrowsOn;
                score.ArrowsOnTime = CarArrowsOnTime;
                score.DistanceMaintained = DistanceMaintained;
                score.EngineOffTime = CarEngineOffTime;
                score._engineOff = Car_engineOff;
                score._SOSRequested = Car_SOSRequested;
                score.SOSRequestedTime = CarSOSRequestedTime;
            break;
            case SpawnMode.Passenger:
                score.AlarmTriggeredTime = CarAlarmTriggeredTime;
                score._AlarmTriggered = Car_AlarmTriggered;
                score._arrowsOn = Car_arrowsOn;
                score.ArrowsOnTime = CarArrowsOnTime;
                score.DistanceMaintained = DistanceMaintained;
                score.EngineOffTime = CarEngineOffTime;
                score._engineOff = Car_engineOff;
                score._SOSRequested = Car_SOSRequested;
                score.SOSRequestedTime = CarSOSRequestedTime;
                break;
            case SpawnMode.TruckDriver:
                score.AlarmTriggeredTime = TruckAlarmTriggeredTime;
                score._AlarmTriggered = Truck_AlarmTriggered;
                score._arrowsOn = Truck_arrowsOn;
                score.ArrowsOnTime = TruckArrowsOnTime;
                score.estintoreUsato = estintoreUsato;
                score.EngineOffTime = TruckEngineOffTime;
                score._engineOff = Truck_engineOff;
                score._SOSRequested = Truck_SOSRequested;
                score.SOSRequestedTime = TruckSOSRequestedTime;
                break;
            case SpawnMode.Componente:
                score.PrioritaAlCivile = PrioritaAlCivile;
                score.MantenuteDistanzeSicurezzaIcendio = MantenuteDistanzeSicurezzaIcendio;
                score.IncendioSpentoEntro5Min = IncendioSpentoEntro5Min;
                score.RitornoAlCamion = RitornoAlCamion;
                score.CivileSoccorso = CivileSoccorso;
                score.TaglioCinturaOk = TaglioCinturaOk;
                score.AttaccoIncendioBasso = AttaccoIncendioBasso;
                break;
        }
        return score;
    }
    
    #region  Sezione VF
    public void SetPrioritaAlCivile(bool status, SpawnMode ruolo)
    {
        PrioritaAlCivile = status; //((.PrioritaAlCivile = status;
    }


    public void SetMantenuteDistanzeSicurezzaIcendio(bool status, SpawnMode ruolo)
    {
        MantenuteDistanzeSicurezzaIcendio = status;
    }


    public void SetCivileSoccorso(bool status, SpawnMode ruolo)
    {
        CivileSoccorso = status;
    }


    public void SetRitornoAlCamion(bool status, SpawnMode ruolo)
    {
        RitornoAlCamion = status;
    }

    public void SetIncendioSpentoEntro5Min(bool status, SpawnMode ruolo)
    {
        IncendioSpentoEntro5Min = status;
    }
    #endregion
    #region Sezione SOLO CAMIONISTA 
    public void SetEstintoreUsato(bool status, SpawnMode ruolo)
    {
        estintoreUsato = status;
    }
    #endregion
    #region Sezione SOLO Civile 
    public void SetDistanceMaintained(bool status, SpawnMode ruolo)
    {
        DistanceMaintained = status;
    }
    #endregion
    #region Sezione CONDIVISA Civile e Camionista
    public void SetAlarmTriggered(bool status, SpawnMode ruolo)
    {
        switch (ruolo)
        {
            case SpawnMode.Driver:
                if (!Car_AlarmTriggered && status)
                {
                    CarAlarmTriggeredTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_AlarmTriggered = status;
                break;
            case SpawnMode.Passenger:
                if (!Car_AlarmTriggered && status)
                {
                    CarAlarmTriggeredTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_AlarmTriggered = status;
                break;
            case SpawnMode.TruckDriver:
                if (!Truck_AlarmTriggered && status)
                {
                    TruckAlarmTriggeredTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Truck_AlarmTriggered = status;
                break;

        }
        GameManager.Instance.TCN.StartWarningProcedures();
    }


    public void SetArrowsOn(bool status, SpawnMode ruolo)
    {
        switch (ruolo)
        {
            case SpawnMode.Driver:
                if (!Car_arrowsOn && status)
                {
                    CarArrowsOnTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_arrowsOn = status;
                break;
            case SpawnMode.Passenger:
                if (!Car_arrowsOn && status)
                {
                    CarArrowsOnTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_arrowsOn = status;
                break;
            case SpawnMode.TruckDriver:
                if (!Truck_arrowsOn && status)
                {
                    TruckArrowsOnTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Truck_arrowsOn = status;
                break;
        }
    }


    public void SetEngineOff(bool status, SpawnMode ruolo)
    {
        switch (ruolo)
        {
            case SpawnMode.Driver:
                if (!Car_engineOff && status)
                {
                    CarEngineOffTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_engineOff = status;
                break;
            case SpawnMode.Passenger:
                if (!Car_engineOff && status)
                {
                    CarEngineOffTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_engineOff = status;
                break;
            case SpawnMode.TruckDriver:
                if (!Truck_engineOff && status)
                {
                    TruckEngineOffTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Truck_engineOff = status;
                break;
        }
    }



    public void SetSOSRequested(bool status, SpawnMode ruolo)
    {
        switch (ruolo)
        {
            case SpawnMode.Driver:
                if (!Car_SOSRequested && status)
                {
                    CarSOSRequestedTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime ;
                }
                Car_SOSRequested = status;
                break;
            case SpawnMode.Passenger:
                if (!Car_SOSRequested && status)
                {
                    CarSOSRequestedTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Car_SOSRequested = status;
                break;
            case SpawnMode.TruckDriver:
                if (!Truck_SOSRequested && status)
                {
                    TruckSOSRequestedTime = Time.time - GameManager.Instance.TCN.GetPlayerController().GetComponent<StageControllerNet>().StartTime;
                }
                Truck_SOSRequested = status;
                break;
        }
        if (status && !Truck_SOSRequested && !Car_SOSRequested)
        {
            GameManager.Instance.TCN.StartWarningProcedures();
            GameManager.Instance.TCN.EndGame();
        }
    }

    public bool GetSOSRequested(SpawnMode ruolo)
    {
        switch (ruolo)
        {
            case SpawnMode.Driver:
                return Car_SOSRequested;
            case SpawnMode.Passenger:
                return Car_SOSRequested;
            case SpawnMode.TruckDriver:
                return Truck_SOSRequested;
        }
        return false;
    }
    public bool GetAlarmTriggered(SpawnMode ruolo)
    {
        switch (ruolo)
        {
            case SpawnMode.Driver:
                return Car_AlarmTriggered;
            case SpawnMode.Passenger:
                return Car_AlarmTriggered;
            case SpawnMode.TruckDriver:
                return Truck_AlarmTriggered;
        }
        return false;
    }

    #endregion
    #region comandiRemoti
    [Command]
    public void CmdSetMantenuteDistanzeSicurezzaIcendio(bool status, SpawnMode ruolo)
    {
        SetMantenuteDistanzeSicurezzaIcendio(status, ruolo);
    }
    [Command]
    public void CmdCivileSoccorso(bool status, SpawnMode ruolo)
    {
        SetCivileSoccorso(status, ruolo);
    }
    [Command]
    public void CmdEstintoreUsato(bool status, SpawnMode ruolo)
    {
        SetEstintoreUsato(status, ruolo);
    }
    [Command]
    public void CmdSetPrioritaAlCivile(bool status, SpawnMode ruolo)
    {
        SetPrioritaAlCivile(status, ruolo);
    }
    [Command]
    public void CmdSetSOSRequested(bool status, SpawnMode ruolo)
    {
        SetSOSRequested(status, ruolo);
    }
    [Command]
    public void CmdSetEngineOff(bool status, SpawnMode ruolo)
    {
        SetEngineOff(status, ruolo);
    }

    [Command]
    public void CmdSetArrowsOn(bool status, SpawnMode ruolo)
    {
        SetArrowsOn(status, ruolo);
    }
    [Command]
    public void CmdSetAlarmTriggered(bool status, SpawnMode ruolo)
    {
        SetAlarmTriggered(status, ruolo);
    }
    [Command]
    public void CmdSetDistanceMaintained(bool status, SpawnMode ruolo)
    {
        SetDistanceMaintained(status, ruolo);//.DistanceMaintained = status;
    }
    [Command]
    public void CmdIncendioSpentoEntro5Min(bool status, SpawnMode ruolo)
    {
        SetIncendioSpentoEntro5Min(status, ruolo);
    }
    [Command]
    public void CmdRitornoAlCamion(bool status, SpawnMode ruolo)
    {
        SetRitornoAlCamion(status, ruolo);
    }
    #endregion
}
