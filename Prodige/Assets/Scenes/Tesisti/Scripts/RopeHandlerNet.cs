﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Obi;

public class RopeHandlerNet : NetworkBehaviour{


    public TuboCamionToCoil SlaveCamionToCoil;
    public TuboCoilToCamion SlaveCoilToCamion;
    [HideInInspector]
    public ObiSolver solver;

  



    private void Awake()
    {
        solver = this.gameObject.AddComponent<ObiSolver>();// GameObject.Find("ObiSolver").GetComponent<ObiSolver>();//gameObject.AddComponent<ObiSolver>();
        solver.maxParticles = 300;
        solver.UpdateOrder = ObiSolver.SimulationOrder.LateUpdate;

        solver.parameters.advectionRadius = 0.1f;
        solver.parameters.sleepThreshold = 0.1f;
        solver.simulateWhenInvisible = true; //meglio farlo, altrimenti avrà comportamenti imprevedibili, testato   

        //solver.parameters.interpolation = Oni.SolverParameters.Interpolation.Interpolate;
        solver.substeps = 2; 

        solver.distanceConstraintParameters.iterations = 2;
        solver.distanceConstraintParameters.evaluationOrder = Oni.ConstraintParameters.EvaluationOrder.Sequential;
        solver.distanceConstraintParameters.SORFactor = 1;

        solver.pinConstraintParameters.iterations = 1 ;
        solver.pinConstraintParameters.evaluationOrder = Oni.ConstraintParameters.EvaluationOrder.Sequential;
        solver.pinConstraintParameters.SORFactor = 0.1f;

        solver.collisionConstraintParameters.iterations = 1; //aggiunta da noi
        solver.collisionConstraintParameters.evaluationOrder = Oni.ConstraintParameters.EvaluationOrder.Sequential;
        solver.collisionConstraintParameters.SORFactor = 0.1f;

        solver.bendingConstraintParameters.iterations = 1;
        solver.bendingConstraintParameters.evaluationOrder = Oni.ConstraintParameters.EvaluationOrder.Sequential;
        solver.bendingConstraintParameters.SORFactor = 2f;

        solver.parameters.damping = 0.98f;
        solver.parameters.fluidDenoising = 0.4f; //0.4

        solver.collisionLayers = LayerMask.GetMask("Default", "Firetruck", "NPC", "Rope");

       

        solver.particleCollisionConstraintParameters.enabled = false;
        solver.volumeConstraintParameters.enabled = false;
        solver.densityConstraintParameters.enabled = false;
        solver.stitchConstraintParameters.enabled = false;
        solver.skinConstraintParameters.enabled = false;
        solver.tetherConstraintParameters.enabled = false;
    }


    private void Start()
    {
        SlaveCamionToCoil.masterNet = this;
        SlaveCoilToCamion.masterNet = this;
        //imnportate non istanziare puù corde contemporaneamtne obi lo proibisce. (si pianta)
        SlaveCamionToCoil.InitRope(5);
        SlaveCoilToCamion.InitRope(10);

    }

    [ClientRpc]
    void RpcCordaCamiionToCoilLanciata()
    {
        SlaveCamionToCoil.LaunchHook();
    }
    public void CordaCamiionToCoilLanciata()
    {
        RpcCordaCamiionToCoilLanciata();
    }

    [ClientRpc]
    void RpcCordaCoilToCamiionLanciata()
    {
        SlaveCoilToCamion.LaunchHook();
    }

    public void CordaCoilToCamiionLanciata()
    {
        RpcCordaCoilToCamiionLanciata();
    }


}
