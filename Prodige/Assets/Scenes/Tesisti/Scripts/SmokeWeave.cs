﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeWeave : MonoBehaviour {

    // Use this for initialization
    public double lifeTime = 0f;
    public float speed = 1.0f;
    public float toxicity = 10.0f;
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        lifeTime -= Time.deltaTime;
        if(lifeTime <= 0)
        {
            Destroy(this.gameObject);
        }
        else
        {
            this.transform.localPosition += new Vector3(0, 0, -speed * Time.deltaTime);
        }
	}

    private void OnTriggerEnter(Collider collision)
    {
        //Debug.Log(collision.gameObject.name);
        collision.gameObject.SendMessageUpwards("Intoxicate", toxicity, SendMessageOptions.DontRequireReceiver);
    }
}
