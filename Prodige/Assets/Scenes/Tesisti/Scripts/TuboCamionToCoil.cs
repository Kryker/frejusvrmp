﻿using System.Collections;
using UnityEngine;

public class TuboCamionToCoil : HosePipeHandler
{

    public PickUpCoil coil;
    

    protected override void Start()
    {
        base.Start();
        coil.PrimaInterazione += () => {
            LaunchHook();
            masterNet.CordaCamiionToCoilLanciata();
            };
    }




}
