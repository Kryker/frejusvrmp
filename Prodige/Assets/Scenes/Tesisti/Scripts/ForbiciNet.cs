﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ForbiciNet : GenericItemNet
{
    public event MyDelegate SonoStatoRaccolto;
    [SyncVar(hook = "CambioStatoVisibilita")]
    public bool visibili = true;

    //solo se sei componente puoi prendere l'idrante
    public override bool CanInteract(ItemControllerNet c)
    {
        return base.CanInteract(c) && c.GetComponent<PlayerStatusNet>().Ruolo == SpawnMode.Componente;

    }


    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        base.Interact(c, hand);
        if(SonoStatoRaccolto != null)
        {
            SonoStatoRaccolto.Invoke();
        }
        this.GetComponent<Rigidbody>().isKinematic = false;
        //devo gestire cose quando viene preso? si tipo il passaggio del tubo
    }

    public void CambioStatoVisibilita(bool stato)
    {
        visibili = stato;
        var p = GetComponentsInChildren<MeshRenderer>();
        foreach(var t in p)
        {
            t.enabled = stato;
        }
    }
}
