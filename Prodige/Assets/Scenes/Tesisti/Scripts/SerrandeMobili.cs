﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerrandeMobili : MonoBehaviour {

    public float speed = 0.5f;
    //creao lista game obj
    Transform[] elementi; 

	// Use this for initialization
	void Start () {
        elementi = this.GetComponentsInChildren<Transform>();
        
	}
	public void AprireSerranda()
    {
        foreach (var i in elementi)
        {
            if (i != this.transform)
            {
                if(i.name == "porte_laterali.002")
                {
                    i.GetComponent<MeshRenderer>().enabled = false;
                }
                var distance = (this.transform.position - i.transform.position).y;
                i.DOMoveY(this.transform.position.y, distance / speed);
            }
        }
    }

}
