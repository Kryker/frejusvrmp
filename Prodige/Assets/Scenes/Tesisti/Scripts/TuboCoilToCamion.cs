﻿using Obi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TuboCoilToCamion : HosePipeHandler
{

    bool oncePick = true;
    public GameObject attaccoSulRetro;
    public TargetLowBandFilter target;
    
    protected override void Awake()
    {
        base.Awake();
    }


    //Devono farlo tutti sei client che server!
    public void Attivami()
    {
        if (oncePick)
        {
            oncePick = false;
            LaunchHook();
            masterNet.CordaCoilToCamiionLanciata();
            StartCoroutine(FissaMetaCorda());
        }
    }
    public IEnumerator FissaMetaCorda()
    {
        yield return new WaitForSeconds(3.1f);

        ObiPinConstraints constraints = rope.GetComponent<ObiPinConstraints>();
        var partieclleUsate = rope.UsedParticles;
        ObiPinConstraintBatch batch = constraints.GetFirstBatch() as ObiPinConstraintBatch;

        // remove the constraints from the solver, because we cannot modify the constraints list while the solver is using it.
        constraints.RemoveFromSolver(null);

        ObiColliderBase inizio = IngressoAcqua as ObiColliderBase;
        ObiColliderBase meta = tappaObbligatoriaAMeta as ObiColliderBase;
        ObiColliderBase fine = AttaccoHose as ObiColliderBase;
        batch.AddConstraint(0, inizio, Vector3.zero, 1.0f);
        batch.AddConstraint(partieclleUsate / 4, meta, Vector3.zero, 0.6f);
        batch.AddConstraint(partieclleUsate / 4 + 1, tappaObbligatoriaAMetaLonatana as ObiColliderBase, Vector3.zero, 0.6f);
        batch.AddConstraint(partieclleUsate - 1, fine, Vector3.zero, 1.0f);
        // add all constraints back:
        constraints.AddToSolver(null);
    }


    public ObiCollider tappaObbligatoriaAMeta;
    public ObiCollider tappaObbligatoriaAMetaLonatana;
    public ObiCollider tappaObbligatoriaPaddingFine;
    public void AttaccatiSulRetro()
    {
        //forzo il a farlo passare sul lato del camion:
        ObiPinConstraints constraints = rope.GetComponent<ObiPinConstraints>();
        var partieclleUsate = rope.UsedParticles;
        ObiPinConstraintBatch batch = constraints.GetFirstBatch() as ObiPinConstraintBatch;

        // remove the constraints from the solver, because we cannot modify the constraints list while the solver is using it.
        constraints.RemoveFromSolver(null);

        ObiColliderBase inizio = IngressoAcqua as ObiColliderBase;
        ObiColliderBase meta = tappaObbligatoriaAMeta as ObiColliderBase;
        ObiColliderBase fine = AttaccoHose as ObiColliderBase;
        batch.AddConstraint(0, inizio, Vector3.zero, 1.0f);
        batch.AddConstraint(partieclleUsate/4, meta, Vector3.zero, 0.6f); 
        batch.AddConstraint(partieclleUsate / 4 + 1, tappaObbligatoriaAMetaLonatana as ObiColliderBase, Vector3.zero, 0.6f);
        batch.AddConstraint(partieclleUsate -3, tappaObbligatoriaPaddingFine as ObiColliderBase, Vector3.zero, 0.8f);
        batch.AddConstraint(partieclleUsate-1, fine, Vector3.zero, 1.0f);
        // add all constraints back:
        constraints.AddToSolver(null);

        target.target = attaccoSulRetro;
    }


    }
