﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PickUpCoil : GenericItemNet
{
    public GameObject camionInFiamme;
    public float distanzaMinima = 50.0f;
    public IdranteNet idrante;

    public event MyDelegate Droppato;
    public event MyDelegate PrimaInterazione;
    protected bool firstTime = true;

    [SyncVar(hook = "OnVisibleChange")]
    bool visible = true;

    private ControllerHand hand;

    //public GameObject
    // Use this for initialization
    public override void Start () {
        base.Start();
        
	}

    // Update is called once per frame
    public override void Update () {
        base.Update();
    }

    public override bool CanInteract(ItemControllerNet c)
    {
        return base.CanInteract(c) && c.GetComponent<PlayerStatusNet>().Ruolo == SpawnMode.Componente;

    }

    ItemControllerNet oldController;
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        base.Interact(c, hand);
        //c.DropItem(ControllerHand.RightHand, true);
        //c.DropItem(ControllerHand.LeftHand, true);
        oldController = c;
        switch (hand)
        {
            case ControllerHand.LeftHand:
                c.GetComponent<InputManagementNet>().OnLeftTriggerUnclicked += FineInterazione;
                c.GetComponent<InputManagementNet>().OnLeftMouseUnclicked += FineInterazione;
                break;
            case ControllerHand.RightHand:
                c.GetComponent<InputManagementNet>().OnRightTriggerUnclicked += FineInterazione;
                c.GetComponent<InputManagementNet>().OnRightMouseUnclicked += FineInterazione;
                break;
        }
        this.hand = hand;
        if(firstTime)
        {
            firstTime = false;
            InizioInterazioneFake();
            //Casto il tubo!

        }
       
    }

 


    public void FineInterazione(object sender, ClickedEventArgs e)
    {
      var input = oldController.GetComponent<InputManagementNet>();
      if(input != null)
        {
            switch (this.hand)
            {
                case ControllerHand.LeftHand:
                    input.OnLeftTriggerUnclicked -= FineInterazione;
                    input.OnLeftMouseUnclicked -= FineInterazione;
                    try {input.GetComponent<ItemControllerNet>().DropItem(ControllerHand.LeftHand, true); } catch (Exception) { }
            break;
                case ControllerHand.RightHand:
                    input.OnRightTriggerUnclicked -= FineInterazione;
                    input.OnRightMouseUnclicked -= FineInterazione;
                    try { input.GetComponent<ItemControllerNet>().DropItem(ControllerHand.RightHand, true); } catch(Exception) { }
                   
                    break;
            }
        }


        this.GetComponent<Rigidbody>().isKinematic = true;
        if(isServer)
        {
            this.transform.DOMoveY(0.02f, 1.0f);
            this.transform.DORotate(new Vector3(-90.00001f, 0f, 32f), 1.0f);
        }
        
       
       
      
        //verifico la distanza dal camion se attivare o meno il resto
        if ((camionInFiamme.transform.position - this.transform.position).sqrMagnitude <= distanzaMinima* distanzaMinima)
        {
            //non è piu raccoglibile, ma si attivano i suoi figli
            //da implementare, al mometo si attiva l'idrante
            StartCoroutine(CambiaVisibilitaRitardo());    
            this.SetCanInteract(false);
            if(Droppato != null)
            {
                Droppato.Invoke();
            }
        }
        
    }

    IEnumerator CambiaVisibilitaRitardo()
    {
        yield return new WaitForSeconds(1.3f);
        visible = false;
        if(idrante.isActiveAndEnabled) //altrimenti c'è l'idrante fake nella mano del VF
        {
            idrante.sonoAttivo = true;
             if (isServer)
            {
                idrante.CambioStatoAttivo(true);
            }
        }
       
        if (isServer)
        {
            OnVisibleChange(false);
            
        }
    }


    void OnVisibleChange(bool status)
    {
        var parts = GetComponentsInChildren<MeshRenderer>();
        foreach(var el in parts)
        {
         
                el.enabled = status;
         
        }
        var Cparts = GetComponentsInChildren<Collider>();
        foreach (var el in Cparts)
        {
          
                el.enabled = status;

        }
    }

    public void FineInterazioneFake()
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
        this.transform.DOMoveY(0.01f, 1.0f);
        this.transform.DORotate(new Vector3(-90.00001f, 0f ,32f), 1.0f);
        StartCoroutine(CambiaVisibilitaRitardo());
        if (Droppato != null)
        {
            Droppato.Invoke();
        }
    }

    public void InizioInterazioneFake()
    {
        if (PrimaInterazione != null)
        {
            //rotoloTubo.transform.SetParent(this.transform);
            PrimaInterazione.Invoke();
        }
    }



}
