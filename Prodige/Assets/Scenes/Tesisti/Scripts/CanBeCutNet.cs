﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class CanBeCutNet : NetworkBehaviour {

    public delegate void tagliato();
    public event tagliato OggettoTagliato;
    new public Collider collider;
    public AudioSource AS;
    public AudioClip taglio;
   

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SonoStatoTagliato()
    {
        if(isServer)
        {
            RpcAudioTaglio();
            if(!AS.isPlaying)   
                AS.PlayOneShot(taglio);
            if (OggettoTagliato != null)
            {
                OggettoTagliato.Invoke();
               
               
            }
        }
        else
        {
            CmdRiproduciAudio();
        }
    }

    [ClientRpc]
    public void RpcAudioTaglio()
    {
        if (!AS.isPlaying)
            AS.PlayOneShot(taglio);
    }

    [Command]
    public void CmdRiproduciAudio()
    {
        RpcAudioTaglio();
        if (OggettoTagliato != null)
        {
            OggettoTagliato.Invoke();
        }
    }

}
