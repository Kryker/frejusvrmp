﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnableTruckInteractionsNet : NetworkBehaviour
{

    VehicleDataNet Vehicle;
    CarMovementScriptNet engine;
    [SyncVar(hook = "OnInteractionsEnabledChanged")]
    bool interactionsenabled;
    ManigliaPortaNet ExtLeftCarHandle;
    ManigliaPortaNet ExtRightCarHandle;
    public ManigliaPortaNet IntLeftCarHandle;
    ManigliaPortaNet IntRightCarHandle;
    public AudioSource EngineSound;
    public ExtinguisherNet EstintoreTruck;
    FrecceNet Arrows;
    ChiaviNet Keys;
    //public ForceBrakeNet forceBrake;
    //public StopLimitCheckNet LimitCheck;
    /*public*/ PreserveY LightLeft, LightRight;
    bool TCEventsSubscribed = false;
    // Use this for initialization

    void Awake()
    {
        engine = GetComponent<CarMovementScriptNet>();
        ExtLeftCarHandle = IntLeftCarHandle.LinkedHandle;
        //IntRightCarHandle = IntLeftCarHandle.OppositeHandle;
        //ExtRightCarHandle = IntRightCarHandle.LinkedHandle;
        Vehicle = GetComponent<TruckControllerNet>();
        Arrows = GetComponent<FrecceNet>();
        Keys = GetComponent<ChiaviNet>();
        engine.CarStopped += EnableInteractions;
        engine.Braking += EngineSoundOn;
        engine.StopBraking += EngineSoundOff;
    }

    private void Start()
    {
        if (isServer)
        {
            if (isClient)
                interactionsenabled = false;
            else
                OnInteractionsEnabledChanged(false);
        }
        else
            OnInteractionsEnabledChanged(interactionsenabled);
    }

    private void OnInteractionsEnabledChanged(bool state)
    {
        if (state)
        {
            var body = transform.Find("Body");
            //foreach (Collider c in body.GetComponents<Collider>())
                //c.enabled = true;

            engine.Braking -= EngineSoundOn;
            engine.StopBraking -= EngineSoundOff;
            if (LightLeft != null)
                LightLeft.enabled = false;
            if (LightRight != null)
                LightRight.enabled = false;
        }
        interactionsenabled = state;
    }

    private void EngineSoundOn()
    {
        EngineSound.volume = 1;
    }
    private void EngineSoundOff()
    {
        EngineSound.volume = 0;
    }

    public void EnableInteractions()
    {

        if (isServer)
        {
            if (!interactionsenabled)
            {
                engine.StopEngine();
                GameManager.Instance.TCN.StopBraking();
                GameManager.Instance.TCN.CantBrake();
                if (Vehicle.LeftOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.LeftOccupant);
                    p.GetComponent<CharacterController>().enabled = true;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = true;
                    p.GetComponent<PlayerStatusNet>().VehicleStopped();
                }


                if(ExtLeftCarHandle != null)
                {
                    ExtLeftCarHandle.SetCanInteract(true, null);
                }

                if (ExtRightCarHandle != null)
                {
                    ExtRightCarHandle.SetCanInteract(true, null);
                }

                if (IntLeftCarHandle != null)
                {
                    IntLeftCarHandle.SetCanInteract(true, null);
                }

                if (IntRightCarHandle != null)
                {
                    IntRightCarHandle.SetCanInteract(true, null);
                }

                if (Keys != null)
                {
                    Keys.SetCanInteract(true, null);
                }

                if (Arrows != null)
                {
                    Arrows.SetCanInteract(true, null);
                }

                if (EstintoreTruck != null)
                {
                    EstintoreTruck.SetCanInteract(true, null);
                }

                // IntLeftCarHandle.CanInteract(true, null);
                //  IntRightCarHandle.CanInteract(true, null);
                //  Keys.CanInteract(true, null);
                //  Arrows.CanInteract(true, null);

                engine.Braking -= Vehicle.Brake;
                engine.StopBraking -= Vehicle.StopBraking;
                engine.BrakeDisabled.Invoke();
                engine.BrakeEnabled -= GameManager.Instance.TCN.CanBrake;
                engine.BrakeDisabled -= GameManager.Instance.TCN.CantBrake;
                engine.Braking -= GameManager.Instance.TCN.Brake;
                engine.StopBraking -= GameManager.Instance.TCN.StopBraking;
                if(Vehicle.LeftOccupant != null)
                {
                    GameManager.Instance.TCN.SetBrochureAvailable(Vehicle.LeftOccupant);
                }
                
                

                if (!isClient)
                {
                    var body = transform.Find("Body");
                    if(body != null)
                    {
                        var collidersDelBody = body.GetComponents<Collider>();
                        if (collidersDelBody != null)
                        {
                            foreach (Collider c in collidersDelBody)
                                c.enabled = true;
                        }
                        else
                        {
                            Debug.Log("Il camion non ha colliders? " + gameObject.name);
                        }
                    }
                    else
                    {
                        Debug.Log("Il camion deve avere un oggetto chiamato body.");
                    }
                   
                    

                    engine.Braking -= EngineSoundOn;
                    engine.StopBraking -= EngineSoundOff;

                    if (LightLeft != null)
                        LightLeft.enabled = false;
                    if (LightRight != null)
                        LightRight.enabled = false;
                }

                if (isClient)
                    interactionsenabled = true;
                else
                    OnInteractionsEnabledChanged(true);
            }
        }
    }

    public void Reset()
    {
        DisableInteractions();
    }

    private void DisableInteractions()
    {
        if (isServer)
        {
            if (interactionsenabled)
            {
                engine.StartEngine();
                if (Vehicle.LeftOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.LeftOccupant);
                    p.GetComponent<CharacterController>().enabled = false;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = false;
                    p.GetComponent<PlayerStatusNet>().VehicleStarted();
                }
                if (Vehicle.RightOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.RightOccupant);
                    p.GetComponent<CharacterController>().enabled = false;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = false;
                }

                ExtLeftCarHandle.SetCanInteract(false, null);
                ExtRightCarHandle.SetCanInteract(false, null);
                IntLeftCarHandle.SetCanInteract(false, null);
                IntRightCarHandle.SetCanInteract(false, null);
                Keys.SetCanInteract(false, null);
                Arrows.SetCanInteract(false, null);

                engine.Braking += Vehicle.Brake;
                engine.StopBraking += Vehicle.StopBraking;
                engine.BrakeEnabled.Invoke();
                engine.BrakeEnabled += GameManager.Instance.TCN.CanBrake;
                engine.BrakeDisabled += GameManager.Instance.TCN.CantBrake;
                engine.Braking += GameManager.Instance.TCN.Brake;
                engine.StopBraking += GameManager.Instance.TCN.StopBraking;
                GameManager.Instance.TCN.SetBrochureUnavailable(Vehicle.LeftOccupant);

                if (!isClient)
                {
                    var body = transform.Find("Body");
                    foreach (Collider c in body.GetComponents<Collider>())
                        c.enabled = false;

                    engine.Braking += EngineSoundOn;
                    engine.StopBraking += EngineSoundOff;

                    if (LightLeft != null)
                        LightLeft.enabled = true;
                    if (LightRight != null)
                        LightRight.enabled = true;
                }

                if (isClient)
                    interactionsenabled = false;
                else
                    OnInteractionsEnabledChanged(false);
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (isServer && !TCEventsSubscribed && GameManager.Instance.TCN != null)
        {
            engine.Braking += Vehicle.Brake;
            engine.Braking += GameManager.Instance.TCN.Brake;
            engine.StopBraking += Vehicle.StopBraking;
            engine.StopBraking += GameManager.Instance.TCN.StopBraking;
            engine.BrakeEnabled += GameManager.Instance.TCN.CanBrake;
            engine.StopEnabled += GameManager.Instance.TCN.CanStop;
            engine.BrakeDisabled += GameManager.Instance.TCN.CantBrake;
            engine.BrakeDisabled += Vehicle.StopBraking;
            TCEventsSubscribed = true;
        }
    }
}
