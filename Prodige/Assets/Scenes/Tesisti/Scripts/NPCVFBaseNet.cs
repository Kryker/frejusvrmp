﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using DG.Tweening;

public class NPCVFBaseNet : NPCBaseNet
{

    protected FireTruckControllerNPC FT;
    public Transform posizioneDiscesaMezzo;

    protected override void Start()
    {
        base.Start();
        FT = (FireTruckControllerNPC)mezzo;
        this.GetComponent<FixRotation>().enabled = false;

    }

    protected bool lookAt = false;
    protected Transform target;
    protected virtual void LateUpdate()
    {
        //base.LateUpdate();
        //aggiungo il lookAt
        if (lookAt)
        {
            transform.LookAt(target.transform);
        }

    }


    public float ritardoTraslazione = 1f;
    public virtual IEnumerator ChiudoPortaCamion()
    {
        yield return new WaitForSeconds(ritardoTraslazione);

        if(this.transform.position.x > posizioneDiscesaMezzo.transform.position.x)
            transform.DOMoveX(posizioneDiscesaMezzo.transform.position.x + 0.2f, 1.5f);
         else
            transform.DOMoveX(posizioneDiscesaMezzo.transform.position.x - 0.2f, 1.5f);

        transform.DOMoveZ(posizioneDiscesaMezzo.transform.position.z, 4.0f);
        yield return new WaitForSeconds(1.2f);
        transform.DOMoveX(posizioneDiscesaMezzo.transform.position.x, 2.2f);
        transform.DOMoveY(posizioneDiscesaMezzo.transform.position.y, 2.2f);
        yield return new WaitForSeconds(2.5f);
        this.GetComponent<FixRotation>().enabled = true;
 
    }



    /// <summary>
    /// gestiti da Animator Controller
    /// </summary>




}

