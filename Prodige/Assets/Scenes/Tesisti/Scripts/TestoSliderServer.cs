﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestoSliderServer : MonoBehaviour {

    public Text testo;
    public Slider slider;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnSliderChange()
    {
        testo.text = slider.value.ToString("0.0") + "x";
    }
}
