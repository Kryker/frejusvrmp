﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class CallBackTransition : StateMachineBehaviour
{

    // Use this for initialization
    public string callBackExit;
    public string callBackEnter;
    private bool endInvoked = false;

    // Estendibile per ogni tipo di cambio stato
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!endInvoked)
        {
            TryInvoke(animator, callBackExit);
        }
        
        base.OnStateExit(animator, stateInfo, layerIndex);
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        TryInvoke(animator, callBackEnter);
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }


    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(!endInvoked && stateInfo.normalizedTime >= 0.95)
        {
            endInvoked = true;
            TryInvoke(animator, callBackExit);
        }
        base.OnStateUpdate(animator, stateInfo, layerIndex);
    }

        //da riformattare
        private void TryInvoke(Animator animator, string method)
    {
        if (method != "")
        {
            NPCBaseNet npc = animator.gameObject.GetComponent<NPCBaseNet>();

            if (npc != null)
            {
                //uso la reflection per chiamare una funziona grazie al suo nome passato come stringa
                //questo permette di usare questa classe per vari utilizzi
                //!!!NB. grazie alla reflection posso chiamare metodi che non esistono in realità in NPCBaseNet ma nelle sotto classi si,
                //finche in runtime qui passino oggetti che hanno effettivamente i metodi richiesti
                var thisType = npc.GetType();
                MethodInfo theMethod = thisType.GetMethod(method);
                theMethod.Invoke(npc, null);
            }
            else
            {
                CarNPC p = animator.gameObject.GetComponent<CarNPC>();
                NaspoOfflineAnimation n = animator.gameObject.GetComponent<NaspoOfflineAnimation>();
                if (p != null)
                {
                    var thisType = p.GetType();
                    MethodInfo theMethod = thisType.GetMethod(method);
                    theMethod.Invoke(p, null);
                }
                else if (n != null)
                {
                        var thisType = n.GetType();
                        MethodInfo theMethod = thisType.GetMethod(method);
                        theMethod.Invoke(n, null);
                }
                else
                {
                    //Debug.LogError("Error in Callback!." + animator.gameObject.name + " method: " + method);
                    var npcTest = animator.gameObject.GetComponent<TestAnimazioniNPC>();
                    if(npcTest != null)
                    {
                        var thisType = npcTest.GetType();
                        MethodInfo theMethod = thisType.GetMethod(method);
                        theMethod.Invoke(npcTest, null);
                    }
                }

            }
        }


    }
}
