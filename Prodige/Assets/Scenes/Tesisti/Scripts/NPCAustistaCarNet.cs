﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.AI;


//tutto questo deve essere gestito solo dal server, il client è passivo
//quindi le handle le attivo solo nel server, poi il Networkanimator porvvederà a passare i bool
//al client e la trasform le posizioni

public class NPCAustistaCarNet : NPCCiviliBaseNet
{

 
    public override void InizioUscita()
    {
        if (!isServer) { return; }
        if (!mezzo.GetComponent<VehicleDataNet>().portaSinistraAperta)
        {
            mezzo.GetComponent<VehicleDataNet>().portaSinistraAperta = true;
        }
        base.InizioUscita();
        StartCoroutine(ChiudiPorta());
    }

    IEnumerator ChiudiPorta()
    {
        yield return new WaitForSeconds(3.0f);
        mezzo.GetComponent<VehicleDataNet>().portaSinistraAperta = false;
    }
    public void FineUscita()
    {
        if (!isServer) { return; }
    }

    protected override void DestinazioneRaggiunta()
    {
        if (!isServer) { return; }
        switch (pathCounter)
        {
            case 0:
                //non esiste
                break;
          
            case 1:
                portaEmergenza.SetBool("OpenPorta", true);
                NextPath();
                break;
            case 2:
                portaEmergenzaInterna.SetBool("OpenPorta", true);
                NextPath();
                break;
            case 3:
                //salvezza
                break;
        }
    }

}
