﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using DG.Tweening;


//tutto questo deve essere gestito solo dal server, il client è passivo
//quindi le handle le attivo solo nel server, poi il Networkanimator porvvederà a passare i bool
//al client e la trasform le posizioni

public class NPCPassengerNet : NPCCiviliBaseNet
{

    //queste variabili hanno senso solo se sono server
    protected bool fireOnSight = false;
    [SyncVar]
    protected bool cinturaBloccata = true;
    public int secondiAttesaIncendio = 10;

    AudioSource aSource;
    public List<AudioClip> listAudioClips;
    protected override void Start () {
        base.Start();
       
        
        if(isServer)
        {
            //mezzo.br += AvvistatoIncendio;
            mezzo.gameObject.GetComponent<CarMovementScriptNet>().StopEnabled += AvvistatoIncendio;// gestito tramite timer ora
       
            var t = mezzo.GetComponent<CinturaNet>();
            if (t != null)
            {
                t.cinturaTagliata += TagliateCinture;
            }
        }
        this.GetComponent<Collider>().enabled = false;
        aSource = GetComponent<AudioSource>();
        initRot = this.transform.rotation;
    }

    //sovrascrivo la versione base che prevede la disattivazione del fake parenting
    public override void UscitaAbilitata()
    {
        carStopped = true;
        //aSource.PlayOneShot(listAudioClips[1]);
        StartCoroutine(RichiamoAiuto());
        if (!isServer) { return; }
        AC.SetBool("Uscire", true);
    }

    IEnumerator RichiamoAiuto()
    {
        //yield return new WaitForSeconds(2);
        yield return new WaitForSeconds(5);
        RpcRemoveFakeParenting();

        while (cinturaBloccata)
        {
            if(!aSource.isPlaying)
                aSource.PlayOneShot(listAudioClips[2]);
            yield return new WaitForSeconds(10);
        }
      
    }


    Quaternion initRot;
    protected override void Update () {
        base.Update();

    }
    /*
    private void LateUpdate()
    {
        initRot
        
        if (!GetComponent<FakeParenting>().enabled && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Exiting Car"))
        {
            if (oldPos.sqrMagnitude > 0 && oldPos.sqrMagnitude < 5) //Cosi evito la transazione intermedia
            {
                initalDiff = vita.transform.localPosition - oldPos;
                oldPos = vita.transform.localPosition;
                this.transform.position = new Vector3(this.transform.position.x + initalDiff.x, this.transform.position.y, this.transform.position.z + initalDiff.z);
                this.vita.transform.position -= new Vector3(initalDiff.x, 0, initalDiff.z);
            }
            else
            {
                oldPos = vita.transform.localPosition;
            }

        }
    }*/
    
    public void OnAnimatorMove()
    {
        if (isServer)
        {
            // we implement this function to override the default root motion.
            // this allows us to modify the positional speed before it's applied.
            if (Time.deltaTime > 0)
            {
                // Vector3 v = (m_Animator.deltaPosition * m_MoveSpeedMultiplier) / Time.deltaTime;

                // we preserve the existing y part of the current velocity.
                //v.y = m_Rigidbody.velocity.y;
                //m_Rigidbody.velocity = v;
               
               
                this.transform.position += new Vector3(AC.deltaPosition.x, 0, AC.deltaPosition.z);
                var rot = AC.rootRotation.eulerAngles;
                this.transform.rotation = Quaternion.Euler(0, rot.y, 0);
                // this.transform.rotation = AC.bodyRotation;
                //Debug.Log(AC.deltaRotation);
               //AC.ApplyBuiltinRootMotion
            }
        }

       


    }
    

    public virtual void ApriPorta()
    {
        if (!isServer) { return; }
        mezzo.GetComponent<VehicleDataNet>().portaDestraAperta = true;
    }

    public void FineUscita()
    {
        if(isServer && false)
        {
            NVA.updateRotation = false;
            // mezzo.GetComponent<VehicleDataNet>().portaDestraAperta = false;
            Vector3 direction = (path[0].position - transform.position).normalized;
            Quaternion qDir = Quaternion.LookRotation(direction);
            transform.rotation = qDir;
            NVA.updateRotation = true;
        }
       
    }



    //gestita in cinturaNet
    public virtual void TagliateCinture()
    {
        if (!isServer) { return; }
        cinturaBloccata = false;
        AC.SetBool("Intrappolata", false);
        this.GetComponent<FakeParenting>().enabled = false; // sblocco il vincolo che la lega alla macchina
        this.GetComponent<Collider>().enabled = true;
        this.EventoDestinazioneRaggiunta += DestinazioneRaggiunta;
        if (isServer)
        {
            GameManager.Instance.TCN.GPN.SetCivileSoccorso(true, SpawnMode.Componente);

        }
        else
        {
            GameManager.Instance.TCN.GPN.CmdCivileSoccorso(true, SpawnMode.Componente);

        }
    }




    //Gestito tramite il brakeEnabled di CarMovementScriptNet che viene triggerato
    //alla collisione con una sfera nel tunnel ad una certa distanza
    public virtual void AvvistatoIncendio()
    {

        StartCoroutine(IncendioRitardato(8));
    }
    protected virtual IEnumerator IncendioRitardato(float time)
    {
        yield return new WaitForSeconds(time);
        //aSource.PlayOneShot(listAudioClips[0]);
        fireOnSight = true;
        if (isServer)
        {
            AC.SetBool("VistoIncendio", true);
        }


    }

}
