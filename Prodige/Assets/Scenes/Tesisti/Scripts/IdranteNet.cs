﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class IdranteNet : GenericItemNet
{
    public event MyDelegate SonoStatoRaccolto;
    public UnityStandardAssets.Effects.Hose hose;
    public TargetLowBandFilter HoseTubo;
    public bool TrainingItem = false;
    private bool once = true;
    public GameObject Rope;
    public PickUpCoil Coil;

    [SyncVar(hook = "CambioStatoAttivo")]
    public bool sonoAttivo = false;

    [SyncVar(hook = "CordaDisattivata")]
    public bool cordaAttiva = true;
    ItemControllerNet c;

    [SyncVar(hook = "CambiaVisibilitaIdrante")]
    public bool visibilitaIdrante = true;

    public void CambiaVisibilitaIdrante(bool status)
    {
        this.gameObject.SetActive(status);
        visibilitaIdrante = status;
    }


    public GameObject targetIniziale;
  
    public void SonoStatoDroppato()
    {
        if(isServer)
        {
            cordaAttiva = false;
            CordaDisattivata(false);
        }
        else
        {
            CmdSonoStatoDroppato();
        }
    }
    [Command]
    public void CmdSonoStatoDroppato()
    {
        cordaAttiva = false;
        CordaDisattivata(false);
    }


    //solo se sei componente puoi prendere l'idrante
    public override void Start()
    {
        base.Start();
        if(TrainingItem)
        {
            GameManager.Instance.TCN.ComponenteEntraInGioco += DisabilitoRope;
        }
        Coil.Droppato += AbilitaTrasmissione;

        
    }

    void AbilitaTrasmissione()
    {
        this.GetComponent<CustomNetworkTransform>().enabled = true;
    }

    public override bool CanInteract(ItemControllerNet c)
    {
        return base.CanInteract(c) && c.GetComponent<PlayerStatusNet>().Ruolo == SpawnMode.Componente;
         
    }

    public void CambioStatoAttivo(bool stato)
    {
        sonoAttivo = stato;
        if (stato)
        {
            StartCoroutine(DropRitardato());
        }
        else
        {
            SetCanInteract(false);
            GetComponent<Collider>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            //GetComponent<FakeParenting>().enabled = false;
        }
    }

    IEnumerator DropRitardato()
    {
        yield return new WaitForSeconds(1.4f);
        this.GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<FakeParenting>().enabled = false;
        GetComponent<Collider>().enabled = true;
        SetCanInteract(true);
    }

    [Command]
    void CmdInizioSpegnimentoIncendio()
    {
        GameManager.Instance.TCN.InizioSpegnimentoIncendio(false);
    }

    [Command]
    void CmdRaccoltoInvoke()
    {
        if (SonoStatoRaccolto != null)
        {
            SonoStatoRaccolto.Invoke();
        }
    }

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        base.Interact(c, hand);
        this.c = c;
        if (once && !TrainingItem)
        {
            once = false;
            if (isServer)
            {
                GameManager.Instance.TCN.InizioSpegnimentoIncendio(false);
            }
            else
            {
                CmdInizioSpegnimentoIncendio();
            }

        }

        if (isServer)
        {
            if (SonoStatoRaccolto != null)
            {
                SonoStatoRaccolto.Invoke();
            }

        }
        else
        {
            CmdRaccoltoInvoke();
        }

        if(Rope != null)
        {
            //devo gestire cose quando viene preso? si tipo il passaggio del tubo
            if (hand == ControllerHand.LeftHand)
            {
                //HoseTubo.transform.SetParent(c.FakeLeftHand.transform);
                //HoseTubo.transform.localPosition = new Vector3(0, 0, 0);
                HoseTubo.target = c.FakeLeftHand.transform.Find("GrabbedIdrante").Find("AttaccoTubo").gameObject;
            }
            else if (hand == ControllerHand.RightHand)
            {
                //HoseTubo.transform.SetParent(c.FakeRightHand.transform);
                //HoseTubo.transform.localPosition = new Vector3(0, 0, 0);
                HoseTubo.target = c.FakeRightHand.transform.Find("GrabbedIdrante").Find("AttaccoTubo").gameObject;
            }
        }
      
        
    }

    public override void DropParent()
    {
        base.DropParent();
        if(c != null && c.LeftController != null && c.RightController != null)
        {
            var vibr = c.LeftController.GetComponent<VibrationController>();
            if (vibr != null)
            {
                vibr.StopVibration(0.05f, 0, 0.04f, this);
            }
            var vibr2 = c.RightController.GetComponent<VibrationController>();
            if (vibr2 != null)
            {
                vibr.StopVibration(0.05f, 0, 0.04f, this);
            }
        }
        
        if(isServer)
        {
            this.GetComponent<Rigidbody>().isKinematic = true;
            this.transform.DOMoveY(0.03f, 1.0f);
        }
       
    }

    public void FakeInteract()
    {
       // HoseTubo.transform.SetParent(this.transform);
        //HoseTubo.transform.localPosition = new Vector3(0, 0, 0);
    }
    
    public void DisabilitoRope()
    {
        CordaDisattivata(false);
        cordaAttiva = false;
        HoseTubo.target = targetIniziale;
    }

    public void CordaDisattivata(bool status)
    {
        if(!status && Rope != null)
        {
            HoseTubo.target = targetIniziale;
        }
       
    }
}
