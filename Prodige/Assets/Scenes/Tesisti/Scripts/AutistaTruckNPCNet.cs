﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutistaTruckNPCNet : NPCCiviliBaseNet
{


    public override void InizioUscita()
    {
        //this.GetComponent<FixRotation>().enabled = false;
        if (!isServer) { return; }
        mezzo.GetComponent<ChiaviNet>().ForceKeyTurned(true);
        //NVA.speed = 1.2f;
        if (!mezzo.GetComponent<VehicleDataNet>().portaSinistraAperta)
        {
            mezzo.GetComponent<VehicleDataNet>().portaSinistraAperta = true;
        }

        base.InizioUscita();
    }
    public void FineUscita()
    {
        if (!isServer) { return; }
        mezzo.GetComponent<VehicleDataNet>().portaSinistraAperta = false;
    }

    protected override void DestinazioneRaggiunta()
    {
        if (!isServer) { return; }
        switch (pathCounter)
        {
            case 0:
                //non esiste
                break;
            case 1:
                //WorkAround
                NVA.speed = 4.5f;
                NextPath();
                break;
            case 2:
                if (!GameManager.Instance.NoCamionistaNPC)
                {
                    portaEmergenza.SetBool("OpenPorta", true);
                    NextPath();
                }

                break;
            case 3:
                if (!GameManager.Instance.NoCamionistaNPC)
                {
                    portaEmergenzaInterna.SetBool("OpenPorta", true);
                    NextPath();
                }
                break;
            case 4:
                //salvezza
                break;
        }
    }

    protected override IEnumerator TraslazioneUscendo()
    {
        yield return new WaitForSeconds(ritardoTraslazioneUscendo);

        /*
        yield return new WaitForSeconds(ritardoTraslazioneUscendo);
        //transform.DOMoveX(this.transform.position.x + 0.6f, 4f);
        transform.DOMoveX(posizioneFinaleRispettoAlMezzo.transform.position.x - 0.2f, 4.5f);
       
        yield return new WaitForSeconds(2.8f);
        transform.DOMoveX(posizioneFinaleRispettoAlMezzo.transform.position.x, 0.8F);
        transform.DOMoveZ(posizioneFinaleRispettoAlMezzo.transform.position.z, 0.8F);
        transform.DOMoveY(posizioneFinaleRispettoAlMezzo.transform.position.y, 0.8f);
        */
    }


}