﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public delegate void MyDelegate();
public class FireTruckControllerNPC : CarNPC
{


    public Animator PortaSXController;
    public bool PortaSx
    {
        get { return _PortaSX; }
        set { _PortaSX = value; CambioPortaSX(value); }
    }
    [SyncVar(hook = "CambioPortaSX")]
    private bool _PortaSX = false;


    public Animator PortaDXController;
    public bool PortaDx
    {
        get { return _PortaDx; }
        set { _PortaDx = value; CambioPortaDX(value); }
    }
    [SyncVar(hook = "CambioPortaDX")]
    private bool _PortaDx = false;

    public SerrandeMobili SaracinescaCamionLaterale;
    public bool AproCamion
    {
        get { return _AproCamion; }
        set { _AproCamion = value; CambioAproCamion(value); }
    }
    [SyncVar(hook = "CambioAproCamion")]
    private bool _AproCamion = false;

    public Animator PianoNellaSaracinescaLaterale;
    public bool TiroIlPiano
    {
        get { return _TiroIlPiano; }
        set { _TiroIlPiano = value; CambioTiroIlPiano(value); }
    }
    [SyncVar(hook = "CambioTiroIlPiano")]
    private bool _TiroIlPiano = false;

    public SerrandeMobili SaracinescaCamionPosteriore;
    public bool ApriVanoRetro
    {
        get { return _ApriVanoRetro; }
        set { _ApriVanoRetro = value; CambioApriVanoRetro(value); }
    }
    [SyncVar(hook = "CambioApriVanoRetro")]
    private bool _ApriVanoRetro = false;





    //public Animator Naspo;
    public bool GiraNaspo
    {
        get { return _GiraNaspo; }
        set { _GiraNaspo = value; CambioGiraNaspo(value); }
    }
    [SyncVar(hook = "CambioGiraNaspo")]
    private bool _GiraNaspo = false;


    AudioSource aSource;
    public List<AudioClip> audioClipList;
    public override void Start()
    {
        base.Start();
        GameManager.Instance.TCN.NAFiretruck = this;
        aSource = GetComponent<AudioSource>();
        aSource.loop = true;
        PortaSXController.GetComponent<AudioSource>().loop = false;
        PortaDXController.GetComponent<AudioSource>().loop = false;
    }

    public AudioClip suonoAperturaPorta;
    public AudioClip suonoChiusuraPorta;
    private void CambioPortaSX(bool stato)
    {
        PortaSXController.SetBool("Aperta", stato);
        var AS = PortaSXController.GetComponent<AudioSource>();
        if (AS != null && !AS.isPlaying)
        {
            if (stato)
                AS.PlayOneShot(suonoAperturaPorta);

            else
                AS.PlayOneShot(suonoChiusuraPorta);
        }
    }
    private void CambioPortaDX(bool stato)
    {
        PortaDXController.SetBool("Aperta", stato);
        var AS = PortaDXController.GetComponent<AudioSource>();
        if (AS != null && !AS.isPlaying)
        {
            if (stato)
                AS.PlayOneShot(suonoAperturaPorta);

            else
                AS.PlayOneShot(suonoChiusuraPorta);
        }
    }
    private void CambioAproCamion(bool stato)
    {
        if(stato)
        {
            var AS = SaracinescaCamionLaterale.GetComponent<AudioSource>();
            if (AS != null && !AS.isPlaying)
                AS.PlayOneShot(suonoAperturaSaracinesca);
            SaracinescaCamionLaterale.AprireSerranda();
        }
       
    }

    public AudioClip suonoPianoCheScorre;
    private void CambioTiroIlPiano(bool stato)
    {
        if(stato)
        {
            var AS = PianoNellaSaracinescaLaterale.GetComponent<AudioSource>();
            if (AS != null && !AS.isPlaying)
                AS.PlayOneShot(suonoPianoCheScorre);
            PianoNellaSaracinescaLaterale.SetBool("Aperta", stato);
        }  
    }

    public AudioClip suonoAperturaSaracinesca;
    private void CambioApriVanoRetro(bool stato)
    {
        if (stato)
        {
            var AS = SaracinescaCamionPosteriore.GetComponent<AudioSource>();
            if (AS != null && !AS.isPlaying)
                AS.PlayOneShot(suonoAperturaSaracinesca);
            SaracinescaCamionPosteriore.AprireSerranda();
        }
    }

    public HosePipeHandler idranteNaspo;
    private void CambioGiraNaspo(bool stato)
    {
        //Naspo.SetBool("Gira", stato);
        if(!stato)
        {
            //idranteNaspo.attached = false;
        }
        
    }

    public FakeParenting naspo;

    public void AttivaNaspo()
    {
        naspo.enabled = false;
    }

    [SyncVar(hook = "OnChangeMovimento")]
    public bool inMovimento = false;

    public void OnChangeMovimento(bool status)
    {
        aSource.Stop();
        if (status)
        {
            NA.animator.SetBool("Partenza", true);
            aSource.clip = audioClipList[0];
            aSource.Play();
        }
        else
        {
            
            aSource.clip = audioClipList[1];
            aSource.Play();
        }
    }

    public override void UscitaAbilitata()
    {
        if(isServer)
        {
            OnChangeMovimento(false);
        }
        inMovimento = false;
        this.GetComponent<NetworkTransform>().enabled = true;
        base.UscitaAbilitata();
    }
}
