﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using DG.Tweening;

public class NPCVFComponenteNet : NPCVFBaseNet
{
    public NPCVFAutistaNet NPCAutista; //contiene l'idrante e le forbici
    public CinturaNet cintura;
    public Transform mano;
    public GameObject HoseTubo;
    public GameObject incendio;

    [SyncVar(hook = "CambiaVisibilitaIdrante")]
    public bool visibilitaIdrante = false;
    public GameObject IdranteFake;

    [SyncVar(hook = "CambiaVisibilitaForbici")]
    public bool visibilitaForbici = false;
    public GameObject ForbiciFake;


    public void CambiaVisibilitaIdrante(bool status)
    {
        IdranteFake.GetComponent<MeshRenderer>().enabled = status;
        var pezzi = IdranteFake.GetComponentsInChildren<MeshRenderer>();
        foreach (var el in pezzi)
        {
            el.enabled = status;
        }
        visibilitaIdrante = status;
    }

    public void CambiaVisibilitaForbici(bool status)
    {
        var pezzi = ForbiciFake.GetComponentsInChildren<MeshRenderer>();
        foreach(var el in pezzi)
        {
            el.enabled = status;
        }
        visibilitaForbici = status;
    }

    protected override void Start()
    {
        base.Start();
    }

    //public GameObject lookAtPasseggera;
    public GameObject lookAtDirezioneGiusta;
    protected override void DestinazioneRaggiunta()
    {
        if (!isServer) { return; }
        switch (pathCounter)
        {
            case 1: //arrivato al civile in macchina dietro
                lookAt = true;
                target = NPCAutista.mano;
                break;
                
            case 2: //arrivato al civile in macchina
                this.transform.DORotate(new Vector3(0, -10, 0), 0.1f).OnComplete( () => { AC.SetBool("ArrivatoAlCivile", true); });
                NVA.enabled = false;
                //lookAt = true;
                //target = lookAtPasseggera.transform;
                //spawn forbici
                break;
                
            case 3: //tornato al camion per prendere l'idrante
                AC.SetBool("ArrivatoAlCamion", true);
                lookAt = true;
                target = HoseTubo.transform;
                break;
            case 4: //Vado vicino all'incedio
                AC.SetBool("DroppaTubo", true);
                break;
            case 5: //Torno al camion e fine
                break;


        }
    }

    /// <summary>
    /// gestiti da Animator Controller
    /// </summary>

    public virtual void ForbiciUsate()
    {
        //questo triggera l'npc che uscrirà dalla macchina
        if (!isServer) { return; }
        
            cintura.OggettoTagliato();
            CambiaVisibilitaForbici(false);
            visibilitaForbici = false;
            //torno al camion per prendere l'idrante
            NextPath();
            lookAt = false;
        
       
    }

    public virtual void ForbiciPrese()
    {
        if(isServer)
        {
            //avviso l'NPC che ho preso le forbici
            NPCAutista.ForbiciPrese();
            lookAt = false;

            NPCAutista.forbici.visibili = false;
            NPCAutista.forbici.CambioStatoVisibilita(false);
            CambiaVisibilitaForbici(true);
            visibilitaForbici = true;
            NextPath();
        }
        
        // this.transform.DOLocalRotate(this.transform.localRotation.eulerAngles + new Vector3(0, -90, 0), 1, RotateMode.Fast);
        /*
         NPCAutista.forbici.GetComponent<FakeParenting>().enabled = true;
         NPCAutista.forbici.GetComponent<FakeParenting>().SetFakeParent(mano, new Vector3(0, 0, 0));

         */

    }


    public virtual void PrendoIdrante()
    {
        if (!isServer) { return; }
        
            //this.transform.DOLocalRotate(new Vector3(0, -228.45f, 0), 1, RotateMode.Fast);
            //devo dire all'NPC che ho preso l'idrante e quindi farlo saprire
            NPCAutista.Coil.GetComponent<FakeParenting>().enabled = true;
            NPCAutista.Coil.GetComponent<FakeParenting>().SetFakeParent(mano, new Vector3(0, 0, 0));
            NPCAutista.Coil.InizioInterazioneFake();
           
            //StartCoroutine(RitardoIdrante());
        
   
        //vado all'icendio
    }

    public virtual void FinePrendoIdranate()
    {
        if (!isServer) { return; }
        lookAt = false;
        NextPath();
    }

    IEnumerator RitardoIdrante()
    {
        yield return new WaitForSeconds(0.3f); //era 1
        //NextPath();
    }

    public TargetLowBandFilter cavoIdrante;
    public GameObject FintoAttaccoIdrante;
    public virtual void DropIdrante() //droppo il tubo
    {
        if(isServer)
        {
            NPCAutista.Coil.GetComponent<FakeParenting>().enabled = false;
            //StartCoroutine(AttivoRitardato());
            //NPCAutista.idrante.GetComponent<FakeParenting>().enabled = true;
            //NPCAutista.idrante.GetComponent<FakeParenting>().SetFakeParent(mano, new Vector3(0, 0.2f, 0), new Quaternion(0.0783208f, -0.6245419f, 0.1378017f, -0.7647379f));
            NPCAutista.idrante.visibilitaIdrante = false;
            NPCAutista.idrante.CambiaVisibilitaIdrante(false);
            visibilitaIdrante = true;
            CambiaVisibilitaIdrante(true);
            //Attivo la corda Rope:
            NPCAutista.idrante.FakeInteract(); //non fa niente al momento
            cavoIdrante.target = FintoAttaccoIdrante;
            //
            NPCAutista.Coil.FineInterazioneFake();

        }

    }
    /*
    IEnumerator AttivoRitardato()
    {
        yield return new WaitForSeconds(0.2f);
        NPCAutista.Coil.GetComponent<Rigidbody>().isKinematic = false; ;
    }*/

    public virtual void AllargoIdrante()
    {
        //animo il tubo per allargarlo
        
    }

    public virtual void InizioSpegnimento()
    {
        if(!isServer) { return; }
        DropIdrante();
        //inizio procedura per abbassare fiamme e fumo
        lookAt = true;
        target = incendio.transform;
        //this.transform.DOLocalRotate(new Vector3(0, 50, 0), 0.5f, RotateMode.Fast);
        IdranteFake.GetComponent<UnityStandardAssets.Effects.Hose>().AccendiAcqua();
        GameManager.Instance.TCN.InizioSpegnimentoIncendio(true);
        GameManager.Instance.TCN.TuttiIFuochiSonoSpenti += FineIncendio;
    }

    public virtual void AproPortaCamion()
    {
        if (!isServer) { return; }
        FT.PortaDx = true;
        //transform.DOMoveZ(this.transform.position.z - 0.5f, 1);
        StartCoroutine(ChiudoPortaCamion());
    }
   
    public override IEnumerator ChiudoPortaCamion()
    {
       yield return base.ChiudoPortaCamion();
       FT.PortaDx = false;
    }

    /*
    public virtual void ChiudoPortaCamion()
    {
       
        //AC.SetBool("RotazioneDopoUScitaDalMezzo", true);

        //this.transform.DOLocalRotate(this.transform.localRotation.eulerAngles + new Vector3(0, -90, 0), 1, RotateMode.Fast).OnComplete(() => { FT.PortaDx = false; });

    }*/

    /// <summary>
    /// Gestiti tramite delegate
    /// </summary>
    /// 

    public virtual void ArrivatoAutistaAlCivile()
    {
        if (!isServer) { return; }
        if (!this.gameObject.activeInHierarchy || !this.enabled) { return; }
          
        AC.SetBool("ArrivatoIlGuidatoreConForbici", true);

    }

    //da gestire esternamente dove spengo l'incendio! -> tcn COROUTINE
    public virtual void FineIncendio()
    {

        if (!this.gameObject.activeInHierarchy || !this.enabled) { return; }
        IdranteFake.GetComponent<UnityStandardAssets.Effects.Hose>().SpegniAcqua();
        if (!isServer) { return; }
        visibilitaIdrante = false;
        CambiaVisibilitaIdrante(false);

        AC.SetBool("FineIncendio", true);
        NextPath();
        lookAt = false;
    }


}
