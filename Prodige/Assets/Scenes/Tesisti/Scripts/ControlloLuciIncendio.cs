﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlloLuciIncendio : MonoBehaviour {

    public List<GameObject> sezioniTunnel;
	// Use this for initialization

    public int indiceMaterialeEmitterLampione = 2;
    public bool spegnimentoParziale = false;

    bool areaSpenta = false;
    bool tunnelSpento = false;
    public void FireStarted()
    {
        if (spegnimentoParziale)
        {
            if(areaSpenta)
            {
                AccensioneParziale(sezioniTunnel);
                areaSpenta = false;
            }
            else
            {
                SpegnimentoParziale(sezioniTunnel);
                areaSpenta = true;
            }
          
        }
        else
        {
            var tot = this.transform.childCount;
            List<GameObject> sezioni = new List<GameObject>();
            for (int i = 0; i < tot; i++)
            {
                sezioni.Add(this.transform.GetChild(i).gameObject);
            }
            if (tunnelSpento)
            {
                tunnelSpento = false;
                areaSpenta = false;
                AccensioneParziale(sezioni);
            }

            else
            {
                SpegnimentoParziale(sezioni);
                tunnelSpento = true;
                areaSpenta = true;
            }
               
        }
    }

    private void SpegnimentoParziale(List<GameObject> l)
    {
        foreach (var el in l)
        {
            Transform sezione = el.transform.Find("TunnelSection");
            Transform sezioneFR = el.transform.Find("TunnelIdranteFRA");
            Transform lampioni;
            if(sezione != null)
            {
                lampioni = sezione.Find("LampioniAccesi");
            }
            else if(sezioneFR != null)
            {
                lampioni = sezioneFR.Find("LampioniAccesi");
            }
            else
            {
                lampioni = sezione;
            }

            if(lampioni != null)
            {
                var lampioneDX = lampioni.Find("LampioneDX");
                var lampioneSX = lampioni.Find("LampioneSX");
                if (lampioneDX != null)
                    SetEmitterTo(lampioneDX.gameObject, indiceMaterialeEmitterLampione, 0);
                if (lampioneSX != null)
                    SetEmitterTo(lampioneSX.gameObject, indiceMaterialeEmitterLampione, 0);

                var luceDX = lampioni.Find("TunnelLight.R");
                var luceSX = lampioni.Find("TunnelLight.L");
                if (luceDX != null)
                    luceDX.gameObject.SetActive(false);
                if (luceSX != null)
                    luceSX.gameObject.SetActive(false);
            }
           
        }
    }

    private void AccensioneParziale(List<GameObject> l)
    {
        foreach (var el in l)
        {
            Transform sezione = el.transform.Find("TunnelSection");
            Transform sezioneFR = el.transform.Find("TunnelIdranteFRA");
            Transform lampioni;
            if (sezione != null)
            {
                lampioni = sezione.Find("LampioniAccesi");
            }
            else if (sezioneFR != null)
            {
                lampioni = sezioneFR.Find("LampioniAccesi");
            }
            else
            {
                lampioni = sezione;
            }

            if (lampioni != null)
            {
                var lampioneDX = lampioni.Find("LampioneDX");
                var lampioneSX = lampioni.Find("LampioneSX");
                if (lampioneDX != null)
                    SetEmitterTo(lampioneDX.gameObject, indiceMaterialeEmitterLampione, 2);
                if (lampioneSX != null)
                    SetEmitterTo(lampioneSX.gameObject, indiceMaterialeEmitterLampione, 2);

                var luceDX = lampioni.Find("TunnelLight.R");
                var luceSX = lampioni.Find("TunnelLight.L");
                if (luceDX != null)
                    luceDX.gameObject.SetActive(true);
                if (luceSX != null)
                    luceSX.gameObject.SetActive(true);
            }

        }
    }

    private void SetEmitterTo(GameObject el, int index, float val)
    {
        Renderer renderer = el.GetComponent<Renderer>();
        Material mat = renderer.materials[index];
        Color finalColor = Color.white * val;
        mat.SetColor("_EmissionColor", finalColor);
    }
}
