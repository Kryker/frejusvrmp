﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ActivateSmokeNet : NetworkBehaviour
{    
    private void Start()
    {
    if (!isServer)
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        {
            var c = other.GetComponent<TruckControllerNet>();
            var d = other.GetComponent<HeavyTruckNPC>();
            if (c != null)
            {
                c.ActivateSmoke();
                //ok
                gameObject.SetActive(false);
            }
            else if(d!= null)
            {
                d.ActivateSmoke();
                GameManager.Instance.TCN.CanStop();
                //ok
                gameObject.SetActive(false);
            }
        }
    }
}
