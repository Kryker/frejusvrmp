﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


    class TuboCoilToIdrante : HosePipeHandler
{

    public IdranteNet idrante;
    bool oncePick = true;

    
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        idrante.SonoStatoRaccolto += () => {
            if (oncePick)
            {
                oncePick = false;
                LaunchHook();
            }
        };
    }


    }
