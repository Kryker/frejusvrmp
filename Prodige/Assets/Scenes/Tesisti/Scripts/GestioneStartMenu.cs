﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestioneStartMenu : MonoBehaviour {

    // Use this for initialization
    public GameObject menuGuidatore, menuCamion, menuPasseggero, menuVigile;
	
    public void RuoloImpostato(SpawnMode mode)
    {
        switch(mode)
        {
            case SpawnMode.Driver:
                menuGuidatore.SetActive(true);
                break;
            case SpawnMode.Passenger:
                menuPasseggero.SetActive(true);
                break;
            case SpawnMode.TruckDriver:
                menuCamion.SetActive(true);
                break;
            case SpawnMode.Componente:
                menuVigile.SetActive(true);
                break;
            default:
                break;
        }
        
    }

}
