﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using DG.Tweening;

public class NPCVFAutistaNet : NPCVFBaseNet
{
    public IdranteNet idrante;
    public PickUpCoil Coil;
    public ForbiciNet forbici;
    public Transform mano;
    public NPCVFComponenteNet NPCComponente;
    
    public GameObject pianoCamionVF;
   
    //oggetti da guardare:
    //public GameObject RipianoCamionVF;
    //public GameObject RipianoCamionVFBobina;
    //public GameObject RipianoCamionVFDietro;
    //

    protected override void Start()
    {
        base.Start();
        if (!isServer) { return; }
        /*
        if(idrante != null)
        {
            idrante.SonoStatoRaccolto += () =>
            {
              
            };
        }*/
        if (forbici != null)
        {
            forbici.SonoStatoRaccolto += () =>
            {
                ForbiciPrese();
            };
        }

        if(Coil != null)
        {
            Coil.Droppato += () =>
            {
                FineTubo();
            };
            Coil.PrimaInterazione += () =>
            {
                IdrantePreso();
            };
        }

        forbici.CambioStatoVisibilita(false);
        
    }

    protected override void DestinazioneRaggiunta()
    {
        if (!isServer) { return; }
        switch (pathCounter)
        {
            case 1: //arrivato al civile in macchina
                AC.SetBool("ArrivatoAlCivile", true);
                NPCComponente.ArrivatoAutistaAlCivile();
                break;
            case 2: //tornato al camion per aprire la saracinesca
                lookAt = true;
                target = pianoCamionVF.transform;
                AC.SetBool("ArrivatoAlCamion", true);
                break;
            case 3: //riposiziono nel posto giunsto per l'idrante -> la condizione qui è che venga preso l'idrante
                AC.SetBool("IdrantePreso", true);
                break;
            case 4: //mi sposto dietro al camion per collegare la pompa
                AC.SetBool("ArrivatoSulRetro", true);
                NVA.enabled = false;
                break;

        }
    }



    /// <summary>
    /// gestiti da Animator Controller
    /// </summary>
    public virtual void InizioAttaccoTubo()
    {
        StartCoroutine(RitardoCollegamentoIdrante());
    }

    IEnumerator RitardoCollegamentoIdrante()
    {
       yield return new WaitForSeconds(1.3f);
        hoseToCamion.AttaccatiSulRetro();
    }

    public virtual void AproCamion()        //devo far parire animazione delle saracinesce che salgono
    {
        if (!isServer) { return; }
        StartCoroutine(VanoConRitardoLato());

    }
    IEnumerator VanoConRitardoLato()
    {
        yield return new WaitForSeconds(1.5f);
        FT.AproCamion = true;
    }

    public virtual void TiroIlPiano()        //devo far partire l'animazione del piano che si tira
    {
        if (!isServer) { return; }
        FT.TiroIlPiano = true;
    }

    public virtual void SrotolaTubo()
    {
        if (!isServer) { return; }
        //devo far partire l'animazione del tubo che si srotola
        FT.GiraNaspo = true;
    }

    public TuboCoilToCamion hoseToCamion;
    public virtual void CorreSulRetro()
    {
        if (!isServer) { return; }
        //vado sul retro
        hoseToCamion.Attivami();
        NextPath();
    }

    public Transform PianoPosteriore;
    public virtual void ApriVanoRetro()        //devo far partire l'animazione del vano posteriore
    {
        if (!isServer) { return; }
        lookAt = true;
        target = PianoPosteriore;
        StartCoroutine(VanoConRitardo());
       
    }
    IEnumerator VanoConRitardo()
    {
        yield return new WaitForSeconds(1.5f);
        FT.ApriVanoRetro = true;
    }

    public virtual void FineAttaccoTubo()        //devo far partire l'animazione del vano posteriore
    {
        if (!isServer) { return; }
        lookAt = false;
       
       
    }

    public virtual void RiposizionoFiancoPiano()
    {
        if (!isServer) { return; }
        //permetto di prendere l'idrante:
        if (!NPCComponente.isActiveAndEnabled)
        { //deve farlo solo se non c'è L'NPC componente
            Coil.GetComponent<FakeParenting>().enabled = false;
        }
     
    }


    public virtual void PorgiForbici()
    {
        var FP = forbici.gameObject.AddComponent<FakeParenting>();
        FP.forceRotation = true;
        FP.SetFakeParent(
          mano,
            new Vector3(-0.008717151f, 0.1691465f, 0.02228021f),
           new Quaternion(-0.5875163f, -0.5241255f, 0.4208138f, 0.4505917f));
        forbici.GetComponent<Rigidbody>().isKinematic = true;

        if (!isServer) { return; }
       
        //forbici.GetComponent<FakeParenting>().FakelocalPosition = new Vector3(-0.036f, 0.1268f, 0.0394789f);
        forbici.visibili = true;
        if (!isClient)
        {
            forbici.CambioStatoVisibilita(true);
        }
        

        //cerco player
        foreach(var p in GameManager.Instance.TCN.GetPlayers())
        {
            if(p.Value.Ruolo == SpawnMode.Componente)
            {
                target = p.Value.transform;
                lookAt = true;
            }
        }
        if(NPCComponente.isActiveAndEnabled) //allora non esiste il player
        {
            target = NPCComponente.transform;
            lookAt = true;
        }

    }


    public virtual void AproPortaCamion()
    {
        if (!isServer) { return; }
        FT.PortaSx = true;
        //transform.DOMoveZ(this.transform.position.z - 0.5f, 1);
        StartCoroutine(ChiudoPortaCamion());
    }


    /*
    public virtual void ChiudoPortaCamion()
    {
        FT.PortaSx = false;
        //AC.SetBool("RotazioneDopoUScitaDalMezzo", true);

        //transform.DOLocalRotate(this.transform.localRotation.eulerAngles + new Vector3(0, 90, 0),1, RotateMode.Fast).OnComplete(() => { FT.PortaSx = false; });
    }*/


    public override IEnumerator ChiudoPortaCamion()
    {
        yield return base.ChiudoPortaCamion();
        FT.PortaSx = false;
    }
    /// <summary>
    /// Gestiti tramite delegate
    /// </summary>
    /// 

    //Quando il componente droppa il tubo in un punto valido
    public virtual void FineTubo()
    {
        if (!isServer) { return; }
        lookAt = false; //smetto di guardare il piano con i tubi
        
        this.transform.DORotate(new Vector3(0, 0, 0), 0.1f).OnComplete(() => { AC.SetBool("FineTubo", true); });


        FT.GiraNaspo = false;

    } 

    public virtual void IdrantePreso()
    {
        if (!isServer) { return; }
        this.transform.DOMoveX(this.transform.position.x, 1.5f).OnComplete(() => { NextPath(); }); //lo uso come timer, non fa nulla di moviemnto


    }

    [ClientRpc]
    protected void RpcForbiciPrese()
    {
        forbici.gameObject.GetComponent<FakeParenting>().enabled = false;
    }

    bool forbiciFatte = false;
    public virtual void ForbiciPrese()
    {
        if (!isServer) { return; }
        if (!forbiciFatte)
        {
            lookAt = false;
            forbiciFatte = true;
            forbici.gameObject.GetComponent<FakeParenting>().enabled = false;
            RpcForbiciPrese();
            AC.SetBool("ForbiciPrese", true);
            if(NPCComponente.isActiveAndEnabled)
            {
                forbici.visibili = false;
                if (isServer)
                {
                    forbici.CambioStatoVisibilita(false);
                }
            }
          
            StartCoroutine(ForbiciRitardo());
        }
       
    }

    IEnumerator ForbiciRitardo()
    {
        yield return new WaitForSeconds(1.8f);
        NextPath();
    }

   

}
