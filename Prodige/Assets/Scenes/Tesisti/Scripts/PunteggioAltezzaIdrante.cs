﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunteggioAltezzaIdrante : MonoBehaviour {

    public bool alto = false;
    int counter;
    public int maxColpi = 10;

    GestoreDannoFuoco GDF;
    public void Start()
    {
        GDF = this.transform.parent.parent.GetComponent<GestoreDannoFuoco>();
        GDF.listaColllderIdranti.Add(this);
    }

    public void Colpito()
    {
        counter++;
        if(counter > maxColpi)
        {
            GDF.SpegniTutti(alto);
        }
    }
}
