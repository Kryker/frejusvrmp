﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NPCControllerNet : NetworkBehaviour {

    public GameObject passeggero;
    public GameObject autistaCar;
    public GameObject autistaTruck;
    public GameObject autistaVF;
    public GameObject componenteVF;
    private TunnelControllerNet TCN;


    public void DeActivateAustistaCar()
    {
       autistaCar.gameObject.SetActive(false);
       RpcActivateObject(autistaCar, autistaCar.name);
    }

    public void DeActivateAustistaTruck()
    {
        autistaTruck.gameObject.SetActive(false);
        RpcActivateObject(autistaTruck, autistaTruck.name);
    }


    public void DeActivateComponenteVF()
    {
        componenteVF.gameObject.SetActive(false);
        RpcActivateObject(componenteVF, componenteVF.name);
    }

    [ClientRpc]
    public void RpcActivateObject(GameObject p, string n)
    {
       // Debug.Log(n);
        if(p != null)
            p.SetActive(false);

    }
}
