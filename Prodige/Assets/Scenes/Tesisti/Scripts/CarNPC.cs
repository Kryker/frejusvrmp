﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CarNPC : NetworkBehaviour
{
    public NetworkAnimator NA;
    [SerializeField]
    private bool arrivatoADestinazione = false;

    public event MyDelegate MezzoFermo;
    public virtual void Start()
    {
        //gestione della versione Non NPC
        var car = this.gameObject.GetComponent<CarMovementScriptNet>();
        if (car != null)
        {
            car.CarStopped += UscitaAbilitata;
        }
    }

    public void StartCamionNPC()
    {
        NA.animator.enabled = true;
        NA.animator.SetBool("start", true);
    }

    public virtual void UscitaAbilitata()
    {
        arrivatoADestinazione = true;
        if(MezzoFermo != null)
        {
            MezzoFermo.Invoke();
        }
    }
}
