﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GestoreDannoFuoco : NetworkBehaviour
{

    public SphereCollider TriggerDelDanno;

    float MaxDist = 1f;
    public float Damage = 10.0f;
    CanBeOnFireNet[] FireDelCamion;
    List<PlayerStatusNet> Players;
    // Use this for initialization
    void Start()
    {
        if(isServer)
        {
            Players = new List<PlayerStatusNet>();
            FireDelCamion = GetComponents<CanBeOnFireNet>();
            //
        }
        else
        { TriggerDelDanno.enabled = false; }
       
    }

    public void InizioDannoDaFuoco()
    {
        StartCoroutine(GestoreDimensioniIncendio());
    }


    public float tempMax = 500.0f;
    public float tempMin = 100.0f;
    public float intervalloEsplosioni = 2.5f;
    



    public float raggioMassimo = 10.0f;
    IEnumerator GestoreDimensioniIncendio()
    {
        yield return new WaitForSeconds(20);
        while (true)
        {
            yield return new WaitForSeconds(intervalloEsplosioni);
            var temp = TemperaturaMedia();

            temp = Mathf.Min(tempMax, temp);
            //Debug.Log(temp);
            if(temp > tempMin)
            {
                var interpolazione = (temp- tempMin) / tempMax;
                MaxDist = Mathf.Lerp(1, 10, interpolazione);
                TriggerDelDanno.radius = MaxDist;
                ApplicaDannoAChiEDentro();
            }
            else
            {
                TriggerDelDanno.radius = 0.0f;
            }
           
        }

    }

    public float TemperaturaMedia()
    {
        double temperatura = 0;
        foreach (var el in FireDelCamion)
        {
            temperatura += el.Temperature;
        }
        temperatura /= FireDelCamion.Length;
        return (float)temperatura;
    }

    public void ApplicaDannoAChiEDentro()
    {
       foreach(var player in Players)
        {
            if(player.Ruolo == SpawnMode.Componente)
            {
                GameManager.Instance.TCN.GPN.MantenuteDistanzeSicurezzaIcendio = false;//, SpawnMode.Componente);
            }

            var dist = Vector3.Distance(player.transform.position, transform.position);
            var distanzaNormalizzata =1- (dist / MaxDist);
            if (distanzaNormalizzata > 0)
            {
                player.SendMessage("ApplyDamageWithPain", (Damage/3) + (Damage * distanzaNormalizzata), SendMessageOptions.DontRequireReceiver);

            }
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        var Player = other.gameObject.GetComponent<PlayerStatusNet>();
        if (Player != null && !Players.Contains(Player))
        {
            Players.Add(Player);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        var Player = other.gameObject.GetComponent<PlayerStatusNet>();

        if (Player != null && Players.Contains(Player))
        {
            Players.Remove(Player);
        }
    }

    //verrà chiamata solo sul server, perchè sono sicuro che solo il server avrà le collisioni
    public void SpegniTutti( bool alto)
    {
        if (!isServer) { return; }
        if(!alto)
        {
            GameManager.Instance.TCN.GPN.AttaccoIncendioBasso = true;
           //alora devo settare il punteggio
           //Debug.LogError("Chiamato punteggio basso true");
        }else
        {
            //Debug.LogError("Chiamato punteggio basso false");
        }
        foreach(var el in listaColllderIdranti)
        {
            el.enabled = false;
            el.GetComponent<Collider>().enabled = false;
        }
    }
    [HideInInspector]
    public List<PunteggioAltezzaIdrante> listaColllderIdranti;


}
