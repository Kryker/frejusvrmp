﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class NPCBaseNet : NetworkBehaviour
{
    protected Animator AC;
    public CarNPC mezzo;
    [HideInInspector]
    public bool carStopped = false, uscitaCompletata = false;
    protected NavMeshAgent NVA;
    protected ThirdPersonCharacter TPC;
    //public Transform uscitaEmergenza;
    protected bool movimento = true;
    public event MyDelegate EventoDestinazioneRaggiunta;
    public List<Transform> path;
    protected int pathCounter = 0;
    public float ritardoUscitaMezzo = 4.0f;
    //public GameObject sedile;


    // Use this for initialization
    protected virtual void Start()
    {
     
        if (!isServer)
        {
            //Destroy(this.GetComponent<FakeParenting>());
            Destroy(this.GetComponent<NavMeshAgent>());
            //Destroy(this.GetComponent<ThirdPersonCharacter>());
            //Destroy(this);
            return;
        }
        AC = GetComponent<Animator>();
        NVA = GetComponent<NavMeshAgent>();
        TPC = GetComponent<ThirdPersonCharacter>();
       

        if (mezzo != null)
        {
            mezzo.MezzoFermo += UscitaAbilitata; 
            //var mezzo.GetComponent<Car>()
        }
       
       
    }

    //quando escono dalla macchina non serve più il fake parenintg
    [ClientRpc]
    protected void RpcRemoveFakeParenting()
    {
        if(!isServer) //nel caso in cui server e client deve restare 
        {
            Destroy(this.GetComponent<FakeParenting>());
        }
       
    }


    // Update is called once per frame
    int lastPath = -1;
    bool once = false;
    protected virtual void Update () {
        if(!isServer)
        {
            return;
        }
        if (uscitaCompletata && NVA.isOnNavMesh && NVA.remainingDistance > NVA.stoppingDistance)
        {
            TPC.Move(NVA.desiredVelocity, false, false);
            movimento = true;
            once = false;
        }
        else
        {
            if(lastPath == pathCounter && !once)
            {
                once = false;
                TPC.Move(new Vector3(0, 0, 0), false, false);
            }
              
            if (movimento && lastPath != pathCounter)
            {
                if(EventoDestinazioneRaggiunta != null)
                {
                    lastPath = pathCounter;
                    EventoDestinazioneRaggiunta.Invoke();
                }
                movimento = false;
            }
        }
    }

    //gestyita con CarStopped di CarMovementScriptNet
    public virtual void UscitaAbilitata()
    {
        if(isServer && this.gameObject.activeSelf)
        {
            //this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, sedile.transform.position.z);
            /*
            carStopped = true;
            AC.SetBool("Uscire", true);
            this.GetComponent<FakeParenting>().enabled = false;
            */
            StartCoroutine(AbilitaUscitaRitardata());
        }
       
    }

    //questo lo chiamano i VF: sarebbero da unificare cambiando i nomi negli animator 
    public virtual void ScesoDalMezzo()
    {
        //sceso dal mezzo attivo starda e faccio un passo
        if(isServer)
        {
            StartCoroutine(ScesoDalMezzoDelay());
        }
        
    }

    public float delayDopoDiscesaDalMezzo = 0.1f;
    protected virtual IEnumerator ScesoDalMezzoDelay()
    {
        yield return new WaitForSeconds(delayDopoDiscesaDalMezzo);
        NextPath();
        uscitaCompletata = true;
        EventoDestinazioneRaggiunta += DestinazioneRaggiunta;
    }

    protected virtual IEnumerator AbilitaUscitaRitardata()
    {
        yield return new WaitForSeconds(ritardoUscitaMezzo); //attendo 5 secondi prima di permettere l'uscita

        if (this.gameObject.activeSelf)
        {
            RpcRemoveFakeParenting();
            carStopped = true;
            AC.SetBool("Uscire", true);
            this.GetComponent<FakeParenting>().enabled = false;
        } 
       
        
    }

    protected virtual void NextPath()
    {
        if(isServer)

        {
            NVA.enabled = true;
            NVA.SetDestination(path[pathCounter].position);
            pathCounter++;
        }
        
    }

    protected virtual void DestinazioneRaggiunta() { }


    

}
