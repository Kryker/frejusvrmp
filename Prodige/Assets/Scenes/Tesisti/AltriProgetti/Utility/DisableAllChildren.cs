﻿#if (UNITY_EDITOR) 
using UnityEngine;
using UnityEngine.Networking;
using UnityEditor;

//[CanEditMultipleObjects, CustomEditor(typeof(Transform))]
public class DisableAllChildren : Editor
{

    public override void OnInspectorGUI()
    {
        //target;
        DrawDefaultInspector();
        
        if (GUILayout.Button("DisabilitaTutto"))
        {
            //var c = GameObject.Find("TunnelBig");
            DisableChildren(((Transform) target).gameObject);
        }
        if (GUILayout.Button("DisabilitaAncheFisica"))
        {
            //var c = GameObject.Find("TunnelBig");
            DisableChildren(((Transform)target).gameObject, true);
        }
        if (GUILayout.Button("DisabilitaLOD"))
        {
            //var c = GameObject.Find("TunnelBig");
            DisableLOD(((Transform)target).gameObject);
        }
        if (GUILayout.Button("DistruggiTutto"))
        {
            //var c = GameObject.Find("TunnelBig");
            DestroyScriptsAndIdentity(((Transform)target).gameObject);
        }
    }
    void DisableChildren(GameObject g, bool collider = false)
    {
        foreach(Transform tmp in g.GetComponentsInChildren<Transform>())
        {
            foreach(MonoBehaviour scripts in tmp.GetComponents<MonoBehaviour>())
            {
                scripts.enabled = false;
                
            }
            foreach (MultiNetworkBehaviour scripts in tmp.GetComponents<MultiNetworkBehaviour>())
            {
                scripts.enabled = false;

            }
            foreach (NetworkBehaviour scripts in tmp.GetComponents<NetworkBehaviour>())
            {
                scripts.enabled = false;

            }
            if(collider)
            {
                foreach (Collider scripts in tmp.GetComponents<Collider>())
                {
                    scripts.enabled = false;

                }
            }
           
            if (tmp.gameObject != g)
            {
                DisableChildren(tmp.gameObject);
            }
            
        }
//PreferenzaDiMovimento c = g.GetComponent<PreferenzaDiMovimento>() ;
    }

    void DisableLOD(GameObject g)
    {
        foreach (Transform tmp in g.GetComponentsInChildren<Transform>())
        {
            foreach (var scripts in tmp.GetComponents<LODGroup>())
            {
                scripts.enabled = false;

            }
            if (tmp.gameObject != g)
            {
                DisableChildren(tmp.gameObject);
            }

        }
        //PreferenzaDiMovimento c = g.GetComponent<PreferenzaDiMovimento>() ;
    }

     void DestroyScriptsAndIdentity(GameObject g)
    {
        foreach (Transform tmp in g.GetComponentsInChildren<Transform>())
        {
            foreach (var scripts in tmp.GetComponents<NetworkIdentity>())
            {
                DestroyImmediate(scripts);

            }
            foreach (var scripts in tmp.GetComponents<MonoBehaviour>())
            {
                DestroyImmediate(scripts);

            }
            foreach (var scripts in tmp.GetComponents<NetworkAnimator>())
            {
                DestroyImmediate(scripts);

            }
            foreach (var scripts in tmp.GetComponents<Animator>())
            {
                DestroyImmediate(scripts);

            }
            if (tmp.gameObject != g)
            {
                DisableChildren(tmp.gameObject);
            }

        }
        //PreferenzaDiMovimento c = g.GetComponent<PreferenzaDiMovimento>() ;
    }

}

#endif