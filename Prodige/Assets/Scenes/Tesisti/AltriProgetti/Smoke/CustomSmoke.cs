﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(ParticlesController))]
public class CustomSmoke : MonoBehaviour
{

    ParticlesController mParticlesController = null;

    public float passoGrigliaX = 0.1f;
    public float passoGrigliaY = 0.1f;
    public float passoGrigliaZ = 0.1f;


    LoadSmokeFileASync smoke;

    public double start = 0;
    public float passoTemporale = 5;
    public float secondiTraInterpolazione = 0.1f;
    public Vector3 scale = new Vector3(1,1,1);

    private double timeEsplased = 0;
    public int indiceDiBlocco;


    public Color particleColor;

    private int contatoreDebug = 0;
    private int lastIndiceDiBlocco = -1;

    public bool debugPosition;

    void Start()
    {
        smoke = GameManager.Instance.SmokeFile;

        mParticlesController = GetComponent<ParticlesController>();
             
    }


    bool doneInitPosition = false;
    float frazione = 0.0f;
    float lastFrazione = 0.0f;
    void FixedUpdate()
    {
        if(smoke.debugSmoke)
        {
            return;
        }
        timeEsplased += Time.deltaTime;
        //contatoreDebug++;

        //gestione inizio fumo
        
        if (start >= 0 && (timeEsplased - start) >= 0)
        {
            var indiceFull = ((timeEsplased - start) / passoTemporale);
            frazione = (float) indiceFull % 1;
            indiceDiBlocco = (int)indiceFull;
            if (indiceDiBlocco >= 4000-1 || 
                ((lastIndiceDiBlocco == indiceDiBlocco) 
                &&( frazione- lastFrazione < secondiTraInterpolazione)
                ))
            {
                lastFrazione = frazione;
                indiceDiBlocco = lastIndiceDiBlocco;
                return;
            }
        }
        else
        {
            return;
        }
        if(indiceDiBlocco > smoke.listaFinaleRidotta.Count && !debugPosition)
        {
            return;
        }
        lastIndiceDiBlocco = indiceDiBlocco;
        //gestione impostazione scala delle particelle
        int particlesNum = mParticlesController.GetComponent<ParticleSystem>().particleCount;
        if (mParticlesController.IsReadyToUse() && particlesNum >= (smoke.particelleGrigliaZ * smoke.particelleGrigliaY * smoke.particelleGrigliaX))
        {
            if(debugPosition)
            {
                //if(contatoreDebug > 60)
               // {
                    //contatoreDebug = 0;
                    setUpParticlePosition(particlesNum);
               // }
               
            }
            else
            {
                if (!doneInitPosition)
                {
                    doneInitPosition = true;
                    if (!debugPosition)
                    {
                        setUpParticlePosition(particlesNum);
                    }

                }
                for (int indiceParticella = 0; indiceParticella < smoke.particelleGrigliaZ * smoke.particelleGrigliaY * smoke.particelleGrigliaX; indiceParticella++)
                {if(indiceDiBlocco < 4000)
                    { 
}
                    float val = Mathf.Lerp(smoke.listaFinaleRidotta[indiceDiBlocco][indiceParticella],
                        smoke.listaFinaleRidotta[indiceDiBlocco+1][indiceParticella]
                        , frazione);
                    float size = val/250;// / 128;
                    size = Math.Min(size, 1);
                   
                    mParticlesController.SetColor(indiceParticella, new Color(particleColor.r, particleColor.g, particleColor.b, size));
                }
            }
            
           

        }



    
    }

    

 
    private void setUpParticlePosition(int particlesNum)
    {
       
        //mParticlesController.SetVertexCount(particelleGrigliaZ * particelleGrigliaY * particelleGrigliaX);
        mParticlesController.SetVertexCount(particlesNum);


        int indiceParticella = 0;

        for (int z = 0; z < smoke.particelleGrigliaZ; z++)
        {
            for (int y = 0; y < smoke.particelleGrigliaY; y++) //la sua Y è la mia z
            {
                for (int x = 0; x < smoke.particelleGrigliaX; x++)
                {

                    mParticlesController.SetPosition(indiceParticella,
                    new Vector3(x * passoGrigliaX, y * passoGrigliaY, z * passoGrigliaZ));
                    mParticlesController.SetRotation(indiceParticella, 0f);
                    //mParticlesController.SetScale(indiceParticella, 1);
                    mParticlesController.SetScale3D(indiceParticella, scale);
                    if (debugPosition)
                    {
                        mParticlesController.SetColor(indiceParticella, particleColor);
                    }
                    else
                    {
                        mParticlesController.SetColor(indiceParticella, new Color(particleColor.r, particleColor.g, particleColor.b, 0));
                       
                    }
                    indiceParticella++;
                 
                }
            }

        }

    }


    public void StartSmoke()
    {
        start = timeEsplased;
    }

    public void LateStartSmoke(int indiceDiBlocco)
    {
        start = (indiceDiBlocco * passoTemporale) - timeEsplased;

    }
}
