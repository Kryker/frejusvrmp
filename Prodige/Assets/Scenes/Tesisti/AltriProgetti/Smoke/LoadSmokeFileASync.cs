﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Threading;


public class LoadSmokeFileASync : MonoBehaviour {
    public bool debugSmoke = true;
    public bool loadingDone = false;


    public int particelleGrigliaX = 6;
    public int particelleGrigliaY = 300;
    public int particelleGrigliaZ = 12;


    //partielle nel file originale
    public int passoX = 17;
    public int passoY = 831;
    public int passoZ = 17;

    public string path = "CPCenTreinta_01.s3d";
    public List<List<byte>> listaFinaleRidotta = new List<List<byte>>(3881);
    public Thread childThread;

    void Start () {
        if (!debugSmoke)
        {
            //Thread t = 
            //Task<void> task = readSmokeData();
            ThreadStart childref = new ThreadStart(readSmokeData);
    
            //Console.WriteLine("In Main: Creating the Child thread");
            childThread = new Thread(childref);
            childThread.Start();
        }
        else
        { loadingDone = true; }
    }
	
	void Update () {
      
  
    }

    private void readSmokeData()
    {
        List<List<byte>> listaFinale = new List<List<byte>>(3881);
        var lettore = new BinaryReader(File.OpenRead(path));
        //salto la prima parte composta da 4 + 32 + 4 Byte
        lettore.ReadBytes(40);

        //debug
        int skippatore = 0;
        //fine
        while (true)
        {
            try
            {
                lettore.ReadBytes(28);
                int dimBlocco = lettore.ReadInt32();
                var blocco = lettore.ReadBytes(dimBlocco);
                skippatore++;
                if (skippatore > 120)
                {


                    List<byte> l = new List<byte>(passoX* passoY* passoZ);

                    MemoryStream m = new MemoryStream(blocco);
                    BinaryReader b = new BinaryReader(m);

                    while (true) //decompressione
                    {
                        try
                        {
                            var i = b.ReadByte();
                            if (i == 255)
                            {
                                var valore = b.ReadByte();
                                var ripetizione = b.ReadByte();
                                for (short k = 0; k < ripetizione; k++)
                                {
                                    l.Add(valore);

                                }

                            }
                            else
                            {
                                l.Add(i);
                            }

                        }
                        catch (Exception e)
                        {
                            break;
                        }
                    }
                    listaFinale.Add(l);
                }
                lettore.ReadBytes(4);
            }
            catch (Exception e)
            {
                break;

            }


        }

        //fase di riduzione
        foreach (var p in listaFinale)
        {
            var tmp2 = new List<Byte>(particelleGrigliaZ* particelleGrigliaY* particelleGrigliaX);
            for (var z = 0; z < particelleGrigliaZ; z++)
            {
                for (var y = 0; y < particelleGrigliaY; y++)
                {
                    for (var x = 0; x < particelleGrigliaX; x++)
                    {
                        tmp2.Add(TrasformaScala(x, y, z, p));
                    }
                }
            }
            listaFinaleRidotta.Add(tmp2);
        }

        listaFinale.Clear();
        loadingDone = true;

    }

    private byte TrasformaScala(float x, float y, float z, List<byte> tmp)
    {
        var t = TrasformaVettore(x, y, z);
        double sum = toLineareValore(t[0], t[1], t[2], tmp);//0;
        /*
        for (var i = -1; i < 2; i++)
        {
            for (var j = -1; j < 2; j++)
            {
                for (var k = -1; k < 2; k++)
                {
                    sum += toLineareValore(t[0] + i, t[1] + j, t[2] + k, tmp);
                }
            }
        }
        */
        return Convert.ToByte(sum);// /27
    }

    private List<int> TrasformaVettore(float x, float y, float z)
    {
        var t = new List<int>();
        int X = (int)Math.Floor((x / particelleGrigliaX) * passoX) + (int)Math.Floor((double)(passoX) / (2 * particelleGrigliaX));

        int Y = (int)Math.Floor((y / particelleGrigliaY) * passoY) + (int)Math.Floor((double)(passoY) / (2 * particelleGrigliaY));

        int Z = (int)Math.Floor((z / particelleGrigliaZ) * passoZ) + (int)Math.Floor((double)(passoZ) / (2 * particelleGrigliaZ));
        t.Add(X);
        t.Add(Y);
        t.Add(Z);
        return t;
    }

    private byte toLineareValore(int x, int y, int z, List<byte> tmp)
    {
        if (x < 0)
        {
            x = 0;
        }
        if (y < 0)
        {
            y = 0;
        }
        if (z < 0)
        {
            z = 0;
        }
        return tmp[(y * passoX + x) + (passoX * passoY * z)];
    }
}
