﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AutoStartMMP : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var NM = GetComponent<NetworkManager>();
        NM.StartHost();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
