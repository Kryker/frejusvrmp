﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class StartFire : NetworkBehaviour {

    // Use this for initialization
    bool firstTime = true;
    //public NetworkManagerHUD NM;
	void Start () {
        //NM.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(firstTime)
        {
            firstTime = false;
            GameManager.Instance.TCN.CanStop();

            GameManager.Instance.TCN.StartSmokeFromFile();
        }
	}

    //void onConn
}
