﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public delegate void DestinazioneRaggiuntaEvento(GameObject o);


public class AgentController : MonoBehaviour {
    protected GameObject destinazione; 
    protected NavMeshAgent ac;
    protected bool destinazioneRaggiunta = false;
    public event DestinazioneRaggiuntaEvento AvvisoRaggiungimento;

    // Use this for initialization
    virtual public void Start () {
        ac = GetComponent<NavMeshAgent>();
    }

    virtual public void Update () {
        
	}

    protected void ChiamaSubscriberArrivo()
    {
        this.AvvisoRaggiungimento(this.gameObject);
    }

    virtual public void OnTriggerEnter(Collider other)
    {
        this.ac.SetDestination(destinazione.transform.position);
    }
}
