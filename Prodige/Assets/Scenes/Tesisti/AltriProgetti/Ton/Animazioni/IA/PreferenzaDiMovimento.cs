﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreferenzaDiMovimento : AgentController
{
    public GameObject[] possibiliDestinazioni;
    // Use this for initialization
    override public void Start () {
        base.Start();
        //possibiliDestinazioni = GameObject.FindGameObjectsWithTag("Porta");
        StartCoroutine("ControlloTargetVicino");
    }

    // Update is called once per frame
    override public void Update () {
        //
	}

    public float ControlloProssimita(GameObject target)
    {
        return Vector3.Distance(target.transform.position, this.transform.position);
    }

    IEnumerator ControlloTargetVicino()
    {
        float minDistance = Mathf.Infinity;
        GameObject closest = new GameObject();
        for (; ; )
        {
            bool changed = false;
            foreach(var target in possibiliDestinazioni)
            {
                float tmpDistance = ControlloProssimita(target);
                if (tmpDistance <= minDistance)
                {
                    minDistance = tmpDistance;
                    changed = true;
                    closest = target;
                }
                
            }
            if(changed && this.destinazione != closest)
            {
                this.destinazione = closest;
                this.ac.SetDestination(destinazione.transform.position);
            }
            if(minDistance <= 0.2f)
            {
                destinazioneRaggiunta = true;
                this.ChiamaSubscriberArrivo();
                StopCoroutine("ControlloTargetVicino");
                
            }
            
            yield return new WaitForSeconds(0.5f);
        }
    }


}
