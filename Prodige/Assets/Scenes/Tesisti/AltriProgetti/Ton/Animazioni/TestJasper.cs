﻿using UnityEngine;
using UnityEngine.AI;

public class TestJasper : MonoBehaviour {
    public Animator ac;
    public NavMeshAgent n;
    public float Vita = 10;
    public Vector3 lastFacing;
    public float animationSpeedMax = 12;
    public float animationRotationMax = 120;
    public float animationSpeed = 0;
    private bool fermo = false;

    // Use this for initialization
    void Start () {
        //ac = GetComponent<Animator>();
        n = GetComponent<NavMeshAgent>();
        //n.speed = 0.01f;

    }
	
	// Update is called once per frame
	void Update () {
        var speed = n.velocity.sqrMagnitude;
        //Debug.Log(speed);
        Vector3 currentFacing = transform.forward;
        float currentAngularVelocity = Vector3.Angle(currentFacing, lastFacing) / Time.deltaTime; //degrees per second.
        lastFacing = currentFacing;
       // Debug.Log(currentAngularVelocity);
        ac.SetFloat("Velocita_Frontale", speed);
        ac.SetFloat("Velocita_Laterale", currentAngularVelocity);
        ac.SetFloat("Vita", Vita);
        animationSpeed = System.Math.Max(currentAngularVelocity/animationRotationMax, speed / animationSpeedMax);
        animationSpeed = System.Math.Max(animationSpeed, 0.5f);
        if (speed / animationSpeedMax + currentAngularVelocity/ animationRotationMax <= 0)
        {
            fermo = true;
            animationSpeed = 1;
        }else
        {
            fermo = false;
        }
        ac.SetFloat("Speed", animationSpeed);
        ac.SetBool("Fermo", fermo);


    }
}
