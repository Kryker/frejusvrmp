﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DisableRopeNet : NetworkBehaviour {

   /*
    public float RopeExtensionSpeed;
    public GameObject CamionTubo;
    public GameObject TuboIdrante;
    public PickUpCoil PUC;


    protected bool stop = false;
    [SyncVar]
    protected bool attivaSrotolamento = false;
    [SyncVar]
    protected bool attivaSrotolamento2 = false;

    protected UltimateRope CamionTuboRope;
    protected UltimateRope TuboIdranteRope;
    private bool once = false;
    protected float m_fRopeExtension;
    protected float m_fRopeExtension2;


    void Start()
    {
        PUC.Droppato += TuboDroppato;
        PUC.PrimaInterazione += PrimaInterazione;
        CamionTuboRope = CamionTubo.GetComponent<UltimateRope>();
        TuboIdranteRope = TuboIdrante.GetComponent<UltimateRope>();
        m_fRopeExtension = CamionTuboRope != null ? CamionTuboRope.m_fCurrentExtension : 0.0f;
        m_fRopeExtension2 = CamionTuboRope != null ? TuboIdranteRope.m_fCurrentExtension : 0.0f;
     
    }


    void Update()
    {
        if (attivaSrotolamento) m_fRopeExtension += Time.deltaTime * RopeExtensionSpeed;
        if (attivaSrotolamento2) m_fRopeExtension2 += Time.deltaTime * RopeExtensionSpeed;
        
        //if(Input.GetKey(KeyCode.I)) m_fRopeExtension -= Time.deltaTime * RopeExtensionSpeed;
        
        if (CamionTuboRope != null)
        {
            m_fRopeExtension = Mathf.Clamp(m_fRopeExtension, 0.0f, CamionTuboRope.ExtensibleLength);
            CamionTuboRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, m_fRopeExtension - CamionTuboRope.m_fCurrentExtension);
        }

        if (TuboIdrante != null)
        {
            m_fRopeExtension = Mathf.Clamp(m_fRopeExtension, 0.0f, CamionTuboRope.ExtensibleLength);
            CamionTuboRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, m_fRopeExtension - CamionTuboRope.m_fCurrentExtension);
        }
    }
    
    bool onceDisabilitaRopes = true;
    private void LateUpdate()
    {
        if (onceDisabilitaRopes)
        {
            onceDisabilitaRopes = false;
            AbilitaDisabiltaRope(CamionTubo, false);
            AbilitaDisabiltaRope(TuboIdrante, false);
        }
    }
    
    private void AbilitaDisabiltaRope(GameObject g, bool status)
    {
        var t = g.GetComponentsInChildren<ConfigurableJoint>();
        Rigidbody a;
        foreach (var p in t)
        {
            a = p.GetComponent<Rigidbody>();
            if (a != null)
            {

                a.interpolation = RigidbodyInterpolation.Interpolate;
                a.collisionDetectionMode = CollisionDetectionMode.Continuous;
                a.isKinematic = !status;
            }
        }
    }

    public void MakeRopeStatic(GameObject el)
    {

        var t = el.GetComponentsInChildren<ConfigurableJoint>();
        Rigidbody a;
        CapsuleCollider b;
        foreach (var p in t)
        {
            
            a = p.GetComponent<Rigidbody>();
            b = p.GetComponent<CapsuleCollider>();
            if(a != null)
            {
                a.isKinematic = true;
            }
            if (b != null)
            {
                //b.enabled = false;
                Destroy(b);
            }
            Destroy(p);
           
        }
    }


    public void TuboDroppato()
    {

        
        if (!once)
        {
            MakeRopeStatic(CamionTubo);
            once = true;
            //AbilitaDisabiltaRope(TuboIdrante, true);
            //TuboIdranteRope.Regenerate(false);
            //attivaSrotolamento2 = true;
            attivaSrotolamento = false;
        }
    }


    //gestione primo tubo
    public void PrimaInterazione()
    {
        //CamionTuboRope.RopeStart = StartG;
        //CamionTuboRope.Regenerate(false);
        //CamionTuboRope.Regenerate(false);
        attivaSrotolamento = true;
        AbilitaDisabiltaRope(CamionTubo, true);
       
    }
*/
}
