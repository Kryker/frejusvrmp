﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovimento : MonoBehaviour {

    public float val = 0.5f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey("a"))
        {
            this.transform.position += new Vector3(0, 0, val * Time.deltaTime);
        }
        if (Input.GetKey("d"))
        {
            this.transform.position -= new Vector3(0, 0, val * Time.deltaTime);
        }

        if (Input.GetKey("z"))
        {
            val += 0.5f;
        }
        if (Input.GetKey("x"))
        {
            val -= 0.5f;
        }
    }
}
