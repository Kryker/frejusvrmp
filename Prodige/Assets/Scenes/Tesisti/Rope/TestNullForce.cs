﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNullForce : MonoBehaviour {
    public float waitTime = 0.5f;
	// Use this for initialization
	void Start () {
        StartCoroutine(resetForces());
	}
	
	// Update is called once per frame
	void Update () {
      
	}

    public IEnumerator resetForces()
    {
        while(true)
        {
            var t = GetComponentsInChildren<Rigidbody>();
            foreach (var p in t)
            {
                p.velocity = Vector3.zero;
                p.angularVelocity = Vector3.zero;
            }
            yield return new WaitForSeconds(waitTime);
        }
       
    }
}
