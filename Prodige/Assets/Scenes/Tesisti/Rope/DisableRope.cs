﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableRope : MonoBehaviour {

    private bool once = false;
    public bool stop = false;

    protected UltimateRope Rope;
    public float RopeExtensionSpeed;
    public bool attivaSrotolamento = false;
    float m_fRopeExtension;


    public GameObject ropeObject;
    void Start()
    {
        Rope = ropeObject.GetComponent<UltimateRope>();
        m_fRopeExtension = Rope != null ? Rope.m_fCurrentExtension : 0.0f;
    }


    void Update()
    {
        if (stop && !once)
        {
            MakeRopeStatic();
            once = true;
        }

        if (attivaSrotolamento) m_fRopeExtension += Time.deltaTime * RopeExtensionSpeed;
        /*
        if(Input.GetKey(KeyCode.I)) m_fRopeExtension -= Time.deltaTime * RopeExtensionSpeed;
        */
        if (Rope != null)
        {
            m_fRopeExtension = Mathf.Clamp(m_fRopeExtension, 0.0f, Rope.ExtensibleLength);
            Rope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, m_fRopeExtension - Rope.m_fCurrentExtension);
        }
    }


    public void MakeRopeStatic()
    {

        attivaSrotolamento = false;
        var t = GetComponentsInChildren<ConfigurableJoint>();
        Rigidbody a;
        CapsuleCollider b;
        foreach (var p in t)
        {
            
            a = p.GetComponent<Rigidbody>();
            b = p.GetComponent<CapsuleCollider>();
            if(a != null)
            {
                a.isKinematic = true;
            }
            if (b != null)
            {
                //b.enabled = false;
                DestroyImmediate(b, false);
            }
            DestroyImmediate(p, false);
           
        }
    }


}
