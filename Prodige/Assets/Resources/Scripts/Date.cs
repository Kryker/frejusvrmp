﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Date : MonoBehaviour {

	public Color TextColor = Color.red;
	public Text txtDate, txtHour;
	DateTime currentTime;
    float t;
	// Use this for initialization
	void Start () {

		currentTime = System.DateTime.Now;

		var m = txtDate.material;
		var newm = new Material(m);
		newm.color = TextColor;

		txtDate.color = TextColor;
		txtDate.material = newm;

		txtHour.color = TextColor;
		txtHour.material = newm;

		if (currentTime.Day < 10)
			txtDate.text = "0"+currentTime.Day + "/";
		else
			txtDate.text = currentTime.Day + "/";

		if (currentTime.Month < 10)
			txtDate.text += "0"+ currentTime.Month + "/";
		else
			txtDate.text += currentTime.Month + "/";

		txtDate.text += currentTime.Year;
	}

	void Update(){
        var now = Time.time;
        if (now >= t)
        {
            currentTime = System.DateTime.Now;
            if (currentTime.Hour < 10)
                txtHour.text = "0" + currentTime.Hour + ":";
            else
                txtHour.text = currentTime.Hour + ":";

            if (currentTime.Minute < 10)
                txtHour.text += "0" + currentTime.Minute;
            else
                txtHour.text += currentTime.Minute;
            t = now + 1;
        }
	}

}
