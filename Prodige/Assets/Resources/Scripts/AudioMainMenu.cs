﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMainMenu : MonoBehaviour
{

    public AudioSource rifugio, macchina, messaggio;
    AudioSourceFader rifugiofader;
    bool messageplayed = false;
    int nextcar;

    // Use this for initialization
    void Start()
    {
        rifugiofader = GetComponent<AudioSourceFader>();
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
        nextcar = UnityEngine.Random.Range(0, 30);
        rifugiofader.SetUpAndStart(0, 1, 0.35f, AudioSourceFader.FadeDirection.Raise);
    }

    // Update is called once per frame
    void Update()
    {
        var t = Time.time;
        if (t > 2 && !messageplayed)
        {
            messaggio.Play();
            messageplayed = true;
        }
        if (t >= nextcar)
        {
            macchina.Play();
            nextcar = (int)t + UnityEngine.Random.Range((int)macchina.clip.length, 30);
        }
    }
}
