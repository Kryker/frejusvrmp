﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstPersonItemController : ItemController
{
    public Transform FPSCamera;
    Transform raycasteditem;
    bool raycastednpc = false;
    public bool UIElement = false;
    public Image Crosshair;
    public Sprite BaseCrosshair;
    public Sprite CyanCrosshair;
    public Sprite MagentaCrosshair;
    public Sprite YellowCrosshair;
    public Sprite RainbowCrosshair;
    public ControllerButtonInput ButtonToInteract = ControllerButtonInput.Trigger;
    public ControllerButtonInput ButtonToUse = ControllerButtonInput.Pad;
    public float RaycastMaxDistance = 2;
    [HideInInspector]
    public Transform forcedtarget = null;

    public enum CrosshairState { None, Default, Cyan, Magenta, Yellow, Rainbow };
    CrosshairState CurrentCrosshair = CrosshairState.Default;
    CrosshairState ForcedCrosshair = CrosshairState.None;
    public RayCastUtil RayCaster;
    void SetCrosshair(CrosshairState s)
    {
        switch (s)
        {
            case CrosshairState.Cyan:
                Crosshair.sprite = CyanCrosshair;
                break;
            case CrosshairState.Magenta:
                Crosshair.sprite = MagentaCrosshair;
                break;
            case CrosshairState.Yellow:
                Crosshair.sprite = YellowCrosshair;
                break;
            case CrosshairState.Rainbow:
                Crosshair.sprite = RainbowCrosshair;
                break;
            default:
                Crosshair.sprite = BaseCrosshair;
                break;
        }
        CurrentCrosshair = s;
    }

    public void SetCrosshairPublic(CrosshairState s)
    {
        switch (s)
        {
            case CrosshairState.Cyan:
                Crosshair.sprite = CyanCrosshair;
                break;
            case CrosshairState.Magenta:
                Crosshair.sprite = MagentaCrosshair;
                break;
            case CrosshairState.Yellow:
                Crosshair.sprite = MagentaCrosshair;
                break;
            case CrosshairState.Rainbow:
                Crosshair.sprite = RainbowCrosshair;
                break;
            default:
                Crosshair.sprite = BaseCrosshair;
                break;
        }
        ForcedCrosshair = s;
    }

    private void Awake()
    {
        Type = ItemControllerType.FPS;
    }
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (InputManager != null)
        {
            InputManager.OnLeftMouseClicked += ToggleLeftBrochure;
            InputManager.OnRightMouseClicked += ToggleRightBrochure;
            InputManager.OnLeftMouseClicked += LeftInteractPressed;
            InputManager.OnRightMouseClicked += RightInteractPressed;
            InputManager.OnLeftDropPressed += LeftDropPressed;
            InputManager.OnRightDropPressed += RightDropPressed;
        }
    }

    public override void EnableRightBrochure()
    {
        FakeRightHand.Find("Brochure").GetComponent<BrochureEffect>().EnableBrochure();
        RightBrochureEnabled = true;
    }
    public override void EnableLeftBrochure()
    {
        FakeLeftHand.Find("Brochure").GetComponent<BrochureEffect>().EnableBrochure();
        LeftBrochureEnabled = true;
    }
    public override void DisableRightBrochure()
    {
        FakeRightHand.Find("Brochure").GetComponent<BrochureEffect>().DisableBrochure();
        RightBrochureEnabled = false;
    }
    public override void DisableLeftBrochure()
    {
        FakeLeftHand.Find("Brochure").GetComponent<BrochureEffect>().DisableBrochure();
        LeftBrochureEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit[] hits;
        string[] layers = new string[4];
        layers[0] = "Default";
        layers[1] = "MovingItem";
        layers[2] = "PlayerLayer";
        layers[3] = "UI";
        var mask = LayerMask.GetMask(layers);
        bool onehit = false;

        if (ItemRight == null || ItemLeft == null)
        {
            hits = RayCaster.RayCastAll(RaycastMaxDistance, mask);
            if (hits.Length > 0)
            {
                foreach (RaycastHit hit in hits)
                {
                    var o = hit.collider.transform;
                    if (transform != o)
                    {
                        if (o.tag == "Item")
                        {
                            if (o != ItemLeft || o != ItemRight && (!LeftBrochureEnabled || !RightBrochureEnabled))
                            {
                                if (o != raycasteditem)
                                {
                                    var g = o.GetComponent<GenericItem>();
                                    if (g != null)
                                        FPSInteract(g);
                                    else
                                    {
                                        var gs = o.GetComponent<GenericItemSlave>();
                                        if (gs != null)
                                            FPSInteract(gs);
                                        else
                                        {
                                            var gbs = GetGrabbableItemSlave(o);
                                            if (gbs != null)
                                                FPSInteract(gbs);
                                            else
                                                Debug.Log("No usable component on Item-tagged object: " + o.name);
                                        }
                                    }
                                }
                            }
                            else if (raycasteditem != null)
                            {

                                DeselectRaycastedItem();
                                if (ItemLeft != null || ItemRight != null)
                                    ShowCanUseFPS();
                                else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                    SetCrosshair(ForcedCrosshair);
                                else if (ForcedCrosshair == CrosshairState.None)
                                    SetCrosshair(CrosshairState.Default);
                                raycasteditem = null;
                            }
                            onehit = true;
                            break;
                        }
                        else if (o.tag == "NPC" && (ItemLeft == null || ItemRight == null))
                        {
                            var g = o.GetComponentInParent<NPCController>();
                            if (g == null)
                                Debug.Log("No NPC Controller component on NPC-tagged object: " + o.name);
                            if (g != null && g.CanInteract())
                            {
                                DeselectRaycastedItem();
                                raycasteditem = o;
                                raycastednpc = true;
                                ShowCanInteractFPS();
                                g.EnableOutline();
                            }
                            else if (ItemLeft != null || ItemRight != null)
                                ShowCanUseFPS();
                            else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                SetCrosshair(ForcedCrosshair);
                            else if (ForcedCrosshair == CrosshairState.None)
                                SetCrosshair(CrosshairState.Default);
                            onehit = true;
                            break;
                        }
                        else if (o.gameObject.layer == LayerMask.NameToLayer("UI"))
                        {
                            var uihit = hit.collider.transform.GetComponent<VRUIElement>();

                            if (uihit != null && uihit.IsElementActive() && raycasteditem != uihit.transform && !uihit.transition)
                            {

                                DeselectRaycastedItem();
                                raycasteditem = uihit.transform;
                                UIElement = true;
                                raycasteditem.GetComponent<VRUIElement>().Selected(this);
                                InputManager.OnMouseClicked += ClickEffect;
                                ShowCanInteractFPS();
                            }
                            else if (uihit == null || !uihit.IsElementActive() || uihit.transition)
                            {

                                DeselectRaycastedItem();
                                raycasteditem = null;
                                UIElement = false;
                                InputManager.OnMouseClicked -= ClickEffect;

                                if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                    SetCrosshair(ForcedCrosshair);
                                else if (ForcedCrosshair == CrosshairState.None)
                                    SetCrosshair(CrosshairState.Default);
                            }
                            onehit = true;
                        }
                    }
                }
            }

            if (!onehit && forcedtarget != null)
            {
                var g = forcedtarget.GetComponent<GenericItem>();
                if (g != null)
                {
                    if (g.CanInteract(this))
                    {
                        if (forcedtarget != raycasteditem)
                        {
                            raycasteditem = forcedtarget;
                            raycastednpc = false;
                            ShowCanInteractFPS();
                            g.EnableOutline(this);
                        }
                    }
                    else if (raycasteditem != null)
                    {
                        DeselectRaycastedItem();
                        if (ItemLeft != null || ItemRight != null)
                            ShowCanUseFPS();
                        else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                            SetCrosshair(ForcedCrosshair);
                        else if (ForcedCrosshair == CrosshairState.None)
                            SetCrosshair(CrosshairState.Default);
                        raycasteditem = null;
                    }
                }
                else
                {
                    var gis = forcedtarget.GetComponent<GenericItemSlave>();
                    if (gis != null)
                    {
                        if (gis.Master.CanInteract(this))
                        {
                            if (forcedtarget != raycasteditem)
                            {
                                raycasteditem = forcedtarget;
                                raycastednpc = false;
                                ShowCanInteractFPS();
                                gis.Master.EnableOutline(this);
                            }
                        }
                        else if (raycasteditem != null)
                        {
                            DeselectRaycastedItem();
                            if (ItemLeft != null || ItemRight != null)
                                ShowCanUseFPS();
                            else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                SetCrosshair(ForcedCrosshair);
                            else if (ForcedCrosshair == CrosshairState.None)
                                SetCrosshair(CrosshairState.Default);
                            raycasteditem = null;
                        }
                    }
                    else
                    {
                        var gbs = GetGrabbableItemSlave(forcedtarget);
                        if (gbs != null)
                        {
                            if (forcedtarget != raycasteditem)
                            {
                                raycasteditem = forcedtarget;
                                raycastednpc = false;
                                ShowCanInteractFPS();
                                gbs.EnableOutline(this);
                            }
                        }
                        else
                            Debug.Log("No usable component on Item-tagged object: " + forcedtarget.name);
                    }
                }

            }
            else if (!onehit && raycasteditem != null)
            {
                DeselectRaycastedItem();
                if (ItemLeft != null || ItemRight != null)
                    ShowCanUseFPS();
                else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                    SetCrosshair(ForcedCrosshair);
                else if (ForcedCrosshair == CrosshairState.None)
                    SetCrosshair(CrosshairState.Default);
                raycasteditem = null;
            }
        }

    }


    private void GrabItem(GrabbableItem i, ControllerHand hand)
    {
        var g = i.Item;
        i.Player.DropItem(g.transform, true);
        raycasteditem = g.transform;
        GrabItem(g, hand);
    }

    private void FPSInteract(GrabbableItemSlave gbs)
    {
        if (gbs != null)
        {
            if (gbs.transform.GetChild(0) != raycasteditem)
            {
                DeselectRaycastedItem();
                raycasteditem = gbs.transform.GetChild(0);
                ShowCanInteractFPS();
                gbs.EnableOutline(this);
            }
        }
    }

    private void FPSInteract(GenericItemSlave gs)
    {
        if (gs != null && gs.Master.CanInteract(this))
        {
            if (gs.transform != raycasteditem)
            {
                DeselectRaycastedItem();
                raycasteditem = gs.transform;
                ShowCanInteractFPS();
                gs.Master.EnableOutline(this);
            }
        }
    }

    private void DeselectRaycastedItem()
    {
        if (raycasteditem != null)
        {
            if (!raycastednpc && !UIElement)
            {
                var gi = raycasteditem.GetComponent<GenericItem>();
                if (gi != null)
                    gi.DisableOutline(this);
                else
                {
                    var gis = raycasteditem.GetComponent<GenericItemSlave>();
                    if (gis != null)
                        gis.Master.DisableOutline(this);
                    else
                    {
                        var gbs = GetGrabbableItemSlave(raycasteditem);
                        if (gbs != null)
                            gbs.DisableOutline(this);
                    }
                }
            }
            else if (raycastednpc)
            {
                raycasteditem.GetComponentInParent<NPCController>().DisableOutline();
                raycastednpc = false;
            }
            else
            {
                var b = raycasteditem.GetComponent<VRUIElement>();

                if (!b.transition)
                    b.UnSelected();

                if (!b.IsElementActive() || !b.gameObject.activeSelf)
                    b.UnSelected();

                UIElement = false;
                InputManager.OnMouseClicked -= ClickEffect;
            }
        }
    }

    private void FPSInteract(GenericItem g)
    {
        if (g != null && g.CanInteract(this))
        {
            if (g.transform != raycasteditem)
            {
                /* if ((ItemLeft == null && (ItemRight == null || ItemRightIndex == -1 || (g.ItemCode != GetCurrentItem(ControllerHand.RightHand).Code || (g.ItemCode == GetCurrentItem(ControllerHand.RightHand).Code && !GetCurrentItem(ControllerHand.RightHand).TwoHands))))
   || ((ItemRight == null && (ItemLeft == null || ItemLeftIndex == -1 || (g.ItemCode != GetCurrentItem(ControllerHand.LeftHand).Code || (g.ItemCode == GetCurrentItem(ControllerHand.LeftHand).Code && !GetCurrentItem(ControllerHand.LeftHand).TwoHands))))))
                 { }*/
                DeselectRaycastedItem();
                raycasteditem = g.transform;
                raycastednpc = false;
                ShowCanInteractFPS();
                g.EnableOutline(this);
                /*}
                else if (ItemLeft != null || ItemRight != null)
                {
                    ShowCanUseFPS();
                }
                else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                    SetCrosshair(ForcedCrosshair);
                else if (ForcedCrosshair == CrosshairState.None)
                    SetCrosshair(CrosshairState.Default);*/
            }
        }
    }

    private void ShowCanUseFPS()
    {
        switch (ButtonToUse)
        {
            case ControllerButtonInput.Any:
                SetCrosshair(CrosshairState.Rainbow);
                break;
            case ControllerButtonInput.Pad:
                SetCrosshair(CrosshairState.Yellow);
                break;
            case ControllerButtonInput.Grip:
                SetCrosshair(CrosshairState.Cyan);
                break;
            default:
                SetCrosshair(CrosshairState.Magenta);
                break;
        }
    }

    private void ShowCanInteractFPS()
    {
        switch (ButtonToInteract)
        {
            case ControllerButtonInput.Any:
                SetCrosshair(CrosshairState.Rainbow);
                break;
            case ControllerButtonInput.Pad:
                SetCrosshair(CrosshairState.Yellow);
                break;
            case ControllerButtonInput.Grip:
                SetCrosshair(CrosshairState.Cyan);
                break;
            default:
                SetCrosshair(CrosshairState.Magenta);
                break;
        }
    }

    public void ClickEffect(object sender, ClickedEventArgs e)
    {
        if (BlipSource.isPlaying)
            BlipSource.Stop();
        BlipSource.Play();
    }

    public override void DropItem(ControllerHand hand, bool forced)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRight != null)
            {
                var i = ItemRight.GetComponent<GenericItem>();
                if (i != null)
                {
                    i.Player = null;
                    if (i.ItemCode != ItemCodes.Generic)
                    {
                        if (forced || GetCurrentItem(ControllerHand.RightHand).GetComponent<GrabbableItem>().CanDrop())
                        {
                            i.DisableOutline(this);
                            i.EnableItem(this, hand);
                            i.EnablePhysics();
                            var g = DisableItem(hand);
                            RightItemSource.clip = g.Drop;
                            RightItemSource.volume = g.DropVolume;
                            RightItemSource.Play();
                            SetCrosshair(CrosshairState.Default);
                        }
                    }
                    else
                    {
                        i.DisableOutline(this);
                        i.DropParent();
                        i.EnablePhysics();
                        ItemRight = null;
                        RightItemSource.clip = DefaultDropSound;
                        RightItemSource.volume = DefaultDropSoundVolume;
                        RightItemSource.Play();
                        SetCrosshair(CrosshairState.Default);
                    }
                }
                else if (RightBrochureEnabled)
                    DisableRightBrochure();
                /*else
                {
                    var gis = ItemRight.GetComponent<GenericItemSlave>();
                    if (gis != null)
                    {
                        if (gis.Master.ItemCode != ItemCodes.Generic)
                        {
                            if (GetCurrentItem(ControllerHand.RightHand).GetComponent<GrabbableItem>().CanDrop())
                            {
                                gis.Master.DisableOutline(this);
                                gis.Master.EnableItem(transform);
                                gis.Master.EnablePhysics();
                                var g = DisableItem(hand);
                                RightItemSource.clip = g.Drop;
                                RightItemSource.volume = g.DropVolume;
                                RightItemSource.Play();
                                SetCrosshair(CrosshairState.Default);
                            }
                        }
                        else
                        {
                            gis.Master.DisableOutline(this);
                            gis.Master.DropParent();
                            gis.Master.EnablePhysics();
                            ItemRight = null;
                            RightItemSource.clip = DefaultDropSound;
                            RightItemSource.volume = DefaultDropSoundVolume;
                            RightItemSource.Play();
                            SetCrosshair(CrosshairState.Default);
                        }
                    }
                }*/
            }
            RightInteracting = false;
        }
        if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeft != null)
            {
                var i = ItemLeft.GetComponent<GenericItem>();
                if (i != null)
                {
                    i.Player = null;
                    if (i.ItemCode != ItemCodes.Generic)
                    {
                        if (forced || GetCurrentItem(ControllerHand.LeftHand).GetComponent<GrabbableItem>().CanDrop())
                        {
                            i.DisableOutline(this);
                            i.EnableItem(this, hand);
                            i.EnablePhysics();
                            var g = DisableItem(hand);
                            LeftItemSource.clip = g.Drop;
                            LeftItemSource.volume = g.DropVolume;
                            LeftItemSource.Play();
                            SetCrosshair(CrosshairState.Default);
                        }
                    }
                    else
                    {
                        i.DisableOutline(this);
                        i.DropParent();
                        i.EnablePhysics();
                        ItemLeft = null;
                        LeftItemSource.clip = DefaultDropSound;
                        LeftItemSource.volume = DefaultDropSoundVolume;
                        LeftItemSource.Play();
                        SetCrosshair(CrosshairState.Default);
                    }
                }
                else if (LeftBrochureEnabled)
                    DisableLeftBrochure();
                /*else
                {
                    var gis = ItemLeft.GetComponent<GenericItemSlave>();
                    if (gis != null)
                    {
                        if (gis.Master.ItemCode != ItemCodes.Generic)
                        {
                            if (GetCurrentItem(ControllerHand.LeftHand).GetComponent<GrabbableItem>().CanDrop())
                            {
                                gis.Master.DisableOutline(this);
                                gis.Master.EnableItem(transform);
                                gis.Master.EnablePhysics();
                                var g = DisableItem(hand);
                                LeftItemSource.clip = g.Drop;
                                LeftItemSource.volume = g.DropVolume;
                                LeftItemSource.Play();
                                SetCrosshair(CrosshairState.Default);
                            }
                        }
                        else
                        {
                            gis.Master.DisableOutline(this);
                            gis.Master.DropParent();
                            gis.Master.EnablePhysics();
                            ItemLeft = null;
                            LeftItemSource.clip = DefaultDropSound;
                            LeftItemSource.volume = DefaultDropSoundVolume;
                            LeftItemSource.Play();
                            SetCrosshair(CrosshairState.Default);
                        }
                    }
                }*/
            }
            LeftInteracting = false;
        }
    }

    public void RightInteractPressed(object sender, ClickedEventArgs e)
    {
        if (!RightBrochureEnabled && (raycasteditem != null || forcedtarget != null))
        {
            if (ItemRight == null && !RightInteracting)
            {
                RightInteracting = true;

                if (raycastednpc)
                {
                    ClickItem(raycasteditem, ControllerHand.RightHand);
                    RightInteracting = false;
                }
                else if (UIElement)
                {
                    raycasteditem.GetComponent<VRUIElement>().Click(this, new ClickedEventArgs());
                    RightInteracting = false;
                }
                else if (raycasteditem.GetComponent<GenericItem>() != null && raycasteditem.GetComponent<GenericItem>().Grabbable)
                    GrabItem(raycasteditem.GetComponent<GenericItem>(), ControllerHand.RightHand);
                else if (raycasteditem.GetComponent<GenericItemSlave>() != null && raycasteditem.GetComponent<GenericItemSlave>().Master.Grabbable)
                    GrabItem(raycasteditem.GetComponent<GenericItemSlave>().Master, ControllerHand.RightHand);
                else if (GetGrabbableItemSlave(raycasteditem) != null && GetGrabbableItemSlave(raycasteditem).Master.Item.Grabbable)
                    GrabItem(GetGrabbableItemSlave(raycasteditem).Master, ControllerHand.RightHand);
                else if (ItemRight == null || forcedtarget != null)
                {
                    ClickItem(raycasteditem, ControllerHand.RightHand);
                    RightInteracting = false;
                }
            }
        }
    }
    public void LeftInteractPressed(object sender, ClickedEventArgs e)
    {
        if (!LeftBrochureEnabled && (raycasteditem != null || forcedtarget != null))
        {
            if (ItemLeft == null && !LeftInteracting)
            {
                LeftInteracting = true;

                if (raycastednpc)
                {
                    ClickItem(raycasteditem, ControllerHand.LeftHand);
                    LeftInteracting = false;
                }
                else if (UIElement)
                {
                    raycasteditem.GetComponent<VRUIElement>().Click(this, new ClickedEventArgs());
                    LeftInteracting = false;
                }
                else if (raycasteditem.GetComponent<GenericItem>() != null && raycasteditem.GetComponent<GenericItem>().Grabbable)
                    GrabItem(raycasteditem.GetComponent<GenericItem>(), ControllerHand.LeftHand);
                else if (raycasteditem.GetComponent<GenericItemSlave>() != null && raycasteditem.GetComponent<GenericItemSlave>().Master.Grabbable)
                    GrabItem(raycasteditem.GetComponent<GenericItemSlave>().Master, ControllerHand.LeftHand);
                else if (GetGrabbableItemSlave(raycasteditem) != null && GetGrabbableItemSlave(raycasteditem).Master.Item.Grabbable)
                    GrabItem(GetGrabbableItemSlave(raycasteditem).Master, ControllerHand.LeftHand);
                else if (ItemLeft == null || forcedtarget != null)
                {
                    ClickItem(raycasteditem, ControllerHand.LeftHand);
                    LeftInteracting = false;
                }
            }
        }
    }


    public void SetBrochureAvailable(bool state)
    {
        if (BrochureAvailable == state)
            return;
        if (BrochureAvailable)
        {
            if (LeftBrochureEnabled)
                ToggleBrochure(ControllerHand.LeftHand);
            else if (RightBrochureEnabled)
                ToggleBrochure(ControllerHand.RightHand);
        }
        BrochureAvailable = state;
    }

    public override void GrabItem(GenericItem i, ControllerHand hand)
    {
        i.Interact(this, hand);
        if (i.ItemCode == ItemCodes.Generic)
        {
            if (i.Player != null)
                i.Player.DropItem(i.transform, true);
            i.Player = this;
            i.DisablePhysics();
            i.DisableOutline(this);
            if (hand == ControllerHand.LeftHand)
            {
                i.ForceParent(LeftController, true);
                ItemLeft = i.transform;
                LeftItemSource.clip = DefaultGrabSound;
                LeftItemSource.volume = DefaultGrabSoundVolume;
                LeftItemSource.Play();
            }
            else if (hand == ControllerHand.RightHand)
            {
                i.ForceParent(RightController, true);
                ItemRight = i.transform;
                RightItemSource.clip = DefaultGrabSound;
                RightItemSource.volume = DefaultGrabSoundVolume;
                RightItemSource.Play();
            }
        }
        else
        {
            i.Player = this;
            i.DisablePhysics();
            i.DisableItem(this, hand);

            var g = EnableItem(i.ItemCode, raycasteditem, hand);
            if (g.Slave != null)
            {
                i.ForceParent(g.Slave.transform.GetChild(0), false);
                g.transform.rotation = GetCurrentItem(hand).Slave.transform.rotation;
                g.transform.position = GetCurrentItem(hand).Slave.transform.position;
            }
            else
            {
                if (hand == ControllerHand.LeftHand)
                    i.ForceParent(LeftController, false);
                else if (hand == ControllerHand.RightHand)
                    i.ForceParent(RightController, false);
                i.ForceParent(LeftController, false);
                g.transform.rotation = GetCurrentItem(hand).transform.rotation;
                g.transform.position = GetCurrentItem(hand).transform.position;
            }
            if (hand == ControllerHand.LeftHand)
            {
                LeftItemSource.clip = g.Grab;
                LeftItemSource.volume = g.GrabVolume;
                LeftItemSource.Play();
            }
            else if (hand == ControllerHand.RightHand)
            {
                RightItemSource.clip = g.Grab;
                RightItemSource.volume = g.GrabVolume;
                RightItemSource.Play();
            }
        }

        raycasteditem = null;
        ShowCanUseFPS();
        if (hand == ControllerHand.RightHand)
            RightInteracting = false;
        else if (hand == ControllerHand.LeftHand)
            LeftInteracting = false;
    }

    private void ClickItem(Transform i, ControllerHand hand)
    {
        if (raycastednpc)
        {
            var n = i.GetComponentInParent<NPCController>();
            n.Interact(this, hand);
        }
        else
        {
            var gi = i.GetComponent<GenericItem>();
            if (gi != null)
                gi.Interact(this, hand);
            else
            {
                var gis = i.GetComponent<GenericItemSlave>();
                if (gis != null)
                    gis.Interact(this, hand);
            }
        }
    }


    GrabbableItem EnableItem(ItemCodes code, Transform t, ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            for (int i = 0; i < ItemsRight.Length; i++)
            {
                var item = ItemsRight[i].GetComponent<GrabbableItem>();
                if (item != null && item.Code == code)
                {
                    return EnableItem(i, t, hand);
                }
            }
        }
        else if (hand == ControllerHand.LeftHand)
        {
            for (int i = 0; i < ItemsLeft.Length; i++)
            {
                var item = ItemsLeft[i].GetComponent<GrabbableItem>();
                if (item != null && item.Code == code)
                {
                    return EnableItem(i, t, hand);
                }
            }
        }
        return null;
    }

    Vector3 CurrentItemPos(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex != -1)
                return ItemsRight[ItemRightIndex].transform.position;
            else return new Vector3(0, 0, 0);
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex != -1)
                return ItemsLeft[ItemLeftIndex].transform.position;
            else return new Vector3(0, 0, 0);
        }
        else
            return new Vector3(0, 0, 0);
    }

    Quaternion CurrentItemRot(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex != -1)
                return ItemsRight[ItemRightIndex].transform.rotation;
            else
                return new Quaternion(0, 0, 0, 0);
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex != -1)
                return ItemsLeft[ItemLeftIndex].transform.rotation;
            else
                return new Quaternion(0, 0, 0, 0);
        }
        else
            return new Quaternion(0, 0, 0, 0);
    }

    GrabbableItem DisableItem(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex != -1)
            {
                return DisableItem(ItemRightIndex, hand);
            }
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex != -1)
            {
                return DisableItem(ItemLeftIndex, hand);
            }
        }
        return null;

    }

    GrabbableItem EnableItem(int i, Transform t, ControllerHand hand)
    {
        GrabbableItem g = null;
        GenericItem gi = null;
        if (t != null)
            gi = t.GetComponent<GenericItem>();

        if (hand == ControllerHand.RightHand)
        {
            if (i < 0 || i > ItemsRight.Length)
                return g;
            ItemRightIndex = i;
            ItemRight = t;
            g = ItemsRight[i].GetComponent<GrabbableItem>();
            if (g.HideController)
            {
                RightController.Find("Model").gameObject.SetActive(false);
            }
            if (t != null)
            {
                if (gi != null)
                    g.LoadState(gi);
                else
                    g.LoadState();
            }
            else
                g.LoadState();
            return g;
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (i < 0 || i > ItemsLeft.Length)
                return null;
            ItemLeftIndex = i;
            ItemLeft = t;
            g = ItemsLeft[i].GetComponent<GrabbableItem>();
            if (g.HideController)
            {
                LeftController.Find("Model").gameObject.SetActive(false);
            }
            if (t != null)
            {
                if (gi != null)
                    g.LoadState(gi);
                else
                    g.LoadState();
            }
            else
                g.LoadState();
            return g;
        }
        else
            return null;

    }

    GrabbableItem DisableItem(int i, ControllerHand hand)
    {
        GrabbableItem g = null;
        if (hand == ControllerHand.RightHand)
        {
            if (i < 0 || i > ItemsRight.Length || ItemRightIndex == -1)
                return g;
            ItemRightIndex = -1;
            ItemRight = null;
            g = ItemsRight[i].GetComponent<GrabbableItem>();
            if (g.HideController)
            {
                RightController.Find("Model").gameObject.SetActive(true);
            }
            g.SaveState();
            return g;
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (i < 0 || i > ItemsLeft.Length || ItemLeftIndex == -1)
                return null;
            ItemLeftIndex = -1;
            ItemLeft = null;
            g = ItemsLeft[i].GetComponent<GrabbableItem>();
            if (g.HideController)
            {
                LeftController.Find("Model").gameObject.SetActive(true);
            }
            g.SaveState();
            return g;
        }
        else
            return null;
    }

    public override void ToggleBrochure(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (RightBrochureEnabled)
                DisableRightBrochure();
            else if (BrochureAvailable && !LeftBrochureEnabled && ItemRight == null && raycasteditem == null && forcedtarget == null)
                EnableRightBrochure();
            rightoperating = false;
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (LeftBrochureEnabled)
                DisableLeftBrochure();
            else if (BrochureAvailable && !RightBrochureEnabled && ItemLeft == null && raycasteditem == null && forcedtarget == null)
                EnableLeftBrochure();
            leftoperating = false;
        }
    }
}
