﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshObstacle))]
public class CharacterControllerColliderManager : MonoBehaviour
{
    public Transform Head;
    public float height = 0.0f;
    float prevy = 0.0f;

    public enum Position { Standing, Crouched, Proned };
    Position Posizione = Position.Standing;
    // Use this for initialization
    void Start()
    {
    if (height == 0.0f)
    {
        height = Head.position.y;
        if (height < 1.62f)
            height = 1.62f;
    }
            
    GetComponent<CharacterController>().height = height;
    GetComponent<NavMeshObstacle>().height = height;
           
    }    

    // Update is called once per frame
    void Update()
    {
    var heady = Head.position.y;

            if (heady != prevy)
            {
                if (Head.localPosition.y >= 2)
                {
                    GetComponent<CharacterController>().height = 2;
                    var c = GetComponent<CharacterController>().center;
                    GetComponent<CharacterController>().center = new Vector3(GetComponent<CharacterController>().center.x, 1, GetComponent<CharacterController>().center.z);
                  
                    GetComponent<NavMeshObstacle>().height = 2;

                    GetComponent<NavMeshObstacle>().center = new Vector3(GetComponent<NavMeshObstacle>().center.x, 1, GetComponent<NavMeshObstacle>().center.z);
                    if (Posizione != Position.Standing)
                        Posizione = Position.Standing;
                }
                else if (Head.localPosition.y >= 1 && Head.localPosition.y < 2)
                {
                        GetComponent<CharacterController>().height = Head.localPosition.y;
                        var c = GetComponent<CharacterController>().center;
                        GetComponent<CharacterController>().center = new Vector3(GetComponent<CharacterController>().center.x, Head.localPosition.y / 2, GetComponent<CharacterController>().center.z);
                       
                    GetComponent<NavMeshObstacle>().height = Head.localPosition.y;
                    GetComponent<NavMeshObstacle>().center = new Vector3(GetComponent<NavMeshObstacle>().center.x, Head.localPosition.y / 2, GetComponent<NavMeshObstacle>().center.z);
                    if (Posizione != Position.Standing)
                        Posizione = Position.Standing;
                }
                else if (Head.localPosition.y < 1)
                {
                        GetComponent<CharacterController>().height = 1;
                        var c = GetComponent<CharacterController>().center;
                        GetComponent<CharacterController>().center = new Vector3(GetComponent<CharacterController>().center.x, 0.5f, GetComponent<CharacterController>().center.z);
       
                    GetComponent<NavMeshObstacle>().height = 1;
                    GetComponent<NavMeshObstacle>().center = new Vector3(GetComponent<NavMeshObstacle>().center.x, 0.5f, GetComponent<NavMeshObstacle>().center.z);
                    if (Posizione != Position.Crouched)
                        Posizione = Position.Crouched;

                }
            prevy = heady;
        }
            
    }
}
