﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Frecce : GenericItem {
    
    public Vector3 ButtonPositionAction;
    Vector3 ButtonPositionRest;
    public List<AudioClip> clips;
    public AudioSource audiosource;
   // MeshRenderer mr;
    /*public */VehicleData Vehicle;
    StageController SC;

    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.Arrows;
        if(Slave == null)
            ButtonPositionRest = transform.localPosition;
        else
            ButtonPositionRest = Slave.transform.localPosition;
        Vehicle = GetComponent<VehicleData>();
        Vehicle.StopArrows();
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if(tc != null)
            SC = tc.GetComponent<StageController>();
        if (ItemActive)
            { 
            ButtonPressed();
            Vehicle.StartArrows();
            if(SC != null)
                SC.ArrowsOn = true;
            }
        //mr = transform.GetComponent<MeshRenderer>();
    }

    public override void Start()
    {
        base.Start();
    }


    // Update is called once per frame
    public override void Update()
    {

    }
    public override void ClickButton(object sender)
    {
        ButtonPressed();
    }
    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        ButtonPressed();
    }

    void ButtonPressed()
    {
        if (Slave == null)
            transform.localPosition = ButtonPositionAction;
        else
            Slave.transform.localPosition = ButtonPositionAction;
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        ButtonUnpressed();
    }
    public override void UnClickButton(object sender)
    {
        ButtonUnpressed();
    }
    internal void ArrowsOn()
    {
        if(Slave != null)
        {
            var mr = Slave.GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 3);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        else
        {
            var mr = GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 3);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        audiosource.clip = clips[0];
        audiosource.Play();
    }

    void ButtonUnpressed()
    {
        if (Slave == null)
            transform.localPosition = ButtonPositionRest;
        else
            Slave.transform.localPosition = ButtonPositionRest;
    }
    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        if (!ItemActive)
        {
            ButtonPressed();
            Vehicle.StartArrows();
            ItemActive = true;
            if (SC != null)
                SC.ArrowsOn = true;
        }
        else
        {
            ButtonUnpressed();
            Vehicle.StopArrows();
            ItemActive = false;
            if (SC != null)
                SC.ArrowsOn = false;
        }
    }
    public override void Interact(ItemController c, ControllerHand hand)
    {        
        if (!ItemActive)
            {
            ButtonPressed();
            Vehicle.StartArrows();
            ItemActive = true;
            if(SC != null)
                SC.ArrowsOn = true;
        }
        else
            {
            ButtonUnpressed();
            Vehicle.StopArrows();
            ItemActive = false;
            if (SC != null)
                SC.ArrowsOn = false;
        }
    }
    public override void Reset()
    {
        ButtonUnpressed();
        Vehicle.StopArrows();
        if (SC != null)
            SC.ArrowsOn = false;
        ItemActive = false;
        base.Reset();
    }
    internal void ArrowsOff()
    {
        if(Slave != null)
        {
            var mr = Slave.GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 0);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        else
        {
            var mr = GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 0);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        audiosource.clip = clips[1];
        audiosource.Play();
    }
}
