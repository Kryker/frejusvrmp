﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CamionistaController : NPCController
{
    Transform target;
    //Vector3 oldpos;
    public float WalkSpeed = 1.5f;
    public float RunSpeed = 5f;
    //bool onetry = false;
    AdvancedStateMachineBehaviour behaviour;
    public EntrataRifugio ExternalDoor, InternalDoor;
    public List<Transform> Target;
    StageController SC;
    int currtarget = -1;

    // Use this for initialization
    void Start ()
        {
        agent = GetComponent<NavMeshAgent>();
        CanInteract(false);
        behaviour = animator.GetBehaviour<AdvancedStateMachineBehaviour>();
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if(tc != null)
        {
            SC = tc.GetComponent<StageController>();
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(currtarget == 1 && animator.GetBool("Idle") && ExternalDoor.opening)
        {
        WalkInjured();
        }
        else if (currtarget == 2 && animator.GetBool("Idle") && InternalDoor.opening)
        {
        WalkInjured();
        SC.CamionistaSaved = true;
        }
    }


    public void Walk()
    {
        if (!animator.GetBool("Walk"))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Run", false);
            animator.SetBool("Walk", true);
            if(!animator.GetBool("Injured"))
                agent.speed = WalkSpeed;
            else
                agent.speed = WalkSpeed*0.9f;
        }
    }

    
    public void Trip()
    {
        if(animator.GetBool("Walk"))
            {
            animator.SetBool("Walk", false);
            agent.isStopped = true;
            target = null;
            animator.SetBool("Trip", true);
        }
        else if (animator.GetBool("Run"))
            {
            animator.SetBool("Run", false);
            agent.isStopped = true;
            target = null;
            animator.SetBool("Trip", true);
        }
        else if(animator.GetBool("Idle"))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Trip", true);
            animator.SetBool("CoughAvailable", false);
        }
        animator.GetComponent<CapsuleCollider>().direction = 2;
        animator.GetComponent<CapsuleCollider>().center = new Vector3(0,0,0);
        CanInteract(true);
    }

    public void StandUp()
    {
        if (animator.GetBool("Trip"))
        {
            animator.SetBool("Trip", false);
            animator.SetBool("StandUp", true);
            animator.GetComponent<CapsuleCollider>().direction = 1;
            animator.GetComponent<CapsuleCollider>().center = new Vector3(0, 1, 0);
            CanInteract(false);
            behaviour.StatePlayed += WalkInjured;
            behaviour.StatePlayed += LetHimCough;
        }        
    }

    private void LetHimCough(AdvancedStateMachineBehaviour a)
    {
        animator.SetBool("CoughAvailable", true);
    }

    /* public void WalkInjured()
    {
        animator.SetBool("StandUp", false);
        animator.SetBool("Injured", true);
        WalkTo(FinalTarget);
        behaviour.StatePlayed -= WalkInjured;
    }*/

    public void WalkInjured(AdvancedStateMachineBehaviour st)
    {
        /*if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
        {*/
        WalkInjured();
        //}
    }

    void WalkInjured()
    {
        /*if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animator.IsInTransition(0))
        {*/
        animator.SetBool("StandUp", false);
        animator.SetBool("Injured", true);
        animator.SetBool("Walk", false);
        WalkToNextTarget();
        behaviour.StatePlayed -= WalkInjured;
        //}
    }

    private Transform NextTarget()
    {
        currtarget++;
        return Target[currtarget];
    }

    /*public void WalkTo(Transform t)
    {
        target = t;
        Walk();
        agent.SetDestination(target.position);
        agent.isStopped = false;
        oldpos = target.position;
    }*/

    public void WalkToNextTarget()
    {
        target = NextTarget();
        Walk();
        agent.SetDestination(target.position);
        agent.isStopped = false;
        //oldpos = target.position;
    }

    public void Run()
    {
        if (!animator.GetBool("Run"))
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Walk", false);
            animator.SetBool("Run", true);
            agent.speed = RunSpeed;
        }
    }

    public void Stop()
    {
        if (!animator.GetBool("Idle"))
        {
            target = null;
            animator.SetBool("Idle", true);
            animator.SetBool("Walk", false);
            animator.SetBool("Run", false);
            agent.isStopped = true;
        }
    }

    /*public void RunTo(Transform t)
    {
        target = t;
        Run();
        agent.SetDestination(target.position);
        agent.isStopped = false;
        oldpos = target.position;
    }*/

    public void RunToNextTarget()
    {
        target = NextTarget();
        Run();
        agent.SetDestination(target.position);
        agent.isStopped = false;
        //oldpos = target.position;
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
        if(CanInteract())
            StandUp();
    }
    

    public override void Reset()
    {
        base.Reset();
    }

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract())
            StandUp();
    }
}
