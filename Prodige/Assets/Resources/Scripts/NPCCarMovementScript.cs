﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCarMovementScript : MonoBehaviour {


    public float MotorForce = 600, BrakeForce = 60000;
    Rigidbody body;
    public WheelCollider WheelColFR;
    public WheelCollider WheelColFL;
    public WheelCollider WheelColCR;
    public WheelCollider WheelColCL;
    public WheelCollider WheelColRR;
    public WheelCollider WheelColRL;
    bool braking = false;
    public float MaxSpeed = 70;
    bool forcebrake = false;
    public EngineType Type = EngineType.Rear;
    public bool CanMove = false;

    public enum EngineType { Front, Rear, Both };
    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody>();
    }
    
	// Update is called once per frame
	void Update ()
        {
        if(!CanMove)
            return;
        if (forcebrake && !braking)
        {
            if (!braking)
            {
                braking = true;
            }
            if (WheelColFR != null)
            {
                WheelColFR.motorTorque = 0;
            }

            if (WheelColFL != null)
            {
                WheelColFL.motorTorque = 0;
            }

            if (WheelColRR != null)
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = BrakeForce;
            }

            if (WheelColRL != null)
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = BrakeForce;
            }


        }
        else if(!braking && body.velocity.magnitude*3.6 <= MaxSpeed)
        {
            if (WheelColFR != null && (Type == EngineType.Front || Type == EngineType.Both))
                WheelColFR.motorTorque = MotorForce;
            else if (WheelColFR != null)
                WheelColFR.motorTorque = 0;

            if (WheelColFL != null && (Type == EngineType.Front || Type == EngineType.Both))
                WheelColFL.motorTorque = MotorForce;
            else if (WheelColFL != null)
                WheelColFL.motorTorque = 0;

            if (WheelColRR != null && (Type == EngineType.Rear || Type == EngineType.Both))
            {
                WheelColRR.motorTorque = MotorForce;
                WheelColRR.brakeTorque = 0;
            }
            else if (WheelColRR != null)
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = 0;
            }

            if (WheelColRR != null && (Type == EngineType.Rear || Type == EngineType.Both))
            {
                WheelColRL.motorTorque = MotorForce;
                WheelColRL.brakeTorque = 0;
            }
            else if (WheelColRL != null)
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = 0;
            }
        }
        else if(!braking || body.velocity.magnitude == 0)
        {
            if (WheelColFR != null)
            {
                WheelColFR.motorTorque = 0;
            }

            if (WheelColFL != null)
            {
                WheelColFL.motorTorque = 0;
            }

            if (WheelColRR != null)
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = BrakeForce;
            }

            if (WheelColRL != null)
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = BrakeForce;
            }

        }
        else if (!braking && body.velocity.magnitude >= MaxSpeed)
        {
             if (WheelColFR != null)
            {
                WheelColFR.motorTorque = 0;
            }

            if (WheelColFL != null)
            {
                WheelColFL.motorTorque = 0;
            }

            if (WheelColRR != null)
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = BrakeForce;
            }

            if (WheelColRL != null)
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = BrakeForce;
            }
        }

        /*if (Input.GetKeyDown(KeyCode.Space))
            {
            if (WheelColFR != null)
                WheelColFR.motorTorque = MotorForce;

            if (WheelColFL != null)
                WheelColFL.motorTorque = MotorForce;

            if (WheelColCR != null)
                WheelColCR.motorTorque = MotorForce;

            if (WheelColCL != null)
                WheelColCL.motorTorque = MotorForce;

            if (WheelColRR != null)
                WheelColRR.motorTorque = MotorForce;

            if (WheelColRL != null)
                WheelColRL.motorTorque = MotorForce;
            }
        if (Input.GetKeyUp(KeyCode.Space))
            {
            if (WheelColFR != null)
                WheelColFR.motorTorque = 0;

            if (WheelColFL != null)
                WheelColFL.motorTorque = 0;

            if (WheelColCR != null)
                WheelColCR.motorTorque = 0;

            if (WheelColCL != null)
                WheelColCL.motorTorque = 0;

            if (WheelColRR != null)
                WheelColRR.motorTorque = 0;

            if (WheelColRL != null)
                WheelColRL.motorTorque = 0;
            }*/
    }

    public void ForceBrake()
    {
        forcebrake = true;
    }
}
