﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class VehicleData : MonoBehaviour {

    public enum VehicleType { Car, Truck };

    /*public float DisplacementFromCenter;*/
    [Range(0.0f,100.0f)]
    public float FuelTankPercentage = 50.0f;
    public AudioSource _burningSound, _EngineSound;
    public Transform VehicleBody;
    public VehicleType Type;
    public bool CanBurn = false;
    public bool SingleAudioSource = false;
    List<FireEffectController> fires;
    bool braking = false;
    public bool Accelerating = false;
    CruscottoController Cruscotto;
    public List<ParticleSystem> Scarichi;
    bool arrowson = false;
    float tictime = 0.45f;
    float nexttic = 0;
    bool FrecceOn = false;
    /*public Light Cortesia;
    public float CortesiaIntensity = 4;
    bool first = false;*/
    public MeshRenderer Quadro;
    public bool FrecceOnFromStart = false;
    public int EmissionIndex = 3;
    public bool LightsOn = true;
    public bool EngineOn = false;
    float t = 0;
    bool isburning = false;
    public Transform Frontlights, BackLights;
    public List<MeshRenderer> FrontlightsMR;
    public List<int> FrontLightMRIndexes;
    Frecce PulsanteFrecce;
    public Transform ArrowsLights;
    public List<MeshRenderer> ArrowsMR;
    public List<int> ArrowMRIndexes;
    [HideInInspector]
    public Radio radio;
    VibrationController left, right;
    public VehicleSeat SeatedSpawnLeft, SeatedSpawnRight;
    //TunnelController TC;
    [HideInInspector]
    public PlayerStatus leftoccupant;
    [HideInInspector]
    public PlayerStatus rightoccupant;
    /*public List<Text> TextData;
	public List<Text> TextKm;*/
    //bool cortesiaon = false;
    //Coroutine lastroutine;
    // Use this for initialization
    public virtual void Awake ()
        {
        radio = GetComponent<Radio>();
        PulsanteFrecce = GetComponent<Frecce>();
        Cruscotto = GetComponent<CruscottoController>();
        fires = new List<FireEffectController>();
        if (EngineOn)
        {
            var fire = VehicleBody.GetComponent<CanBeOnFireSlave>();
            if(fire != null && fire.GetTemperature()<90)
                fire.SetTemperature(90);
            TurnOn();
            if (LightsOn)
                TurnOnLights();
        }
        else
            TurnOff();
        }

    /* public void CortesiaOff(float secondsbefore, float time)
     {
         if(Cortesia != null)
         {
             if (lastroutine != null)
                 StopCoroutine(lastroutine);
             lastroutine = StartCoroutine(CourtesyLightOff(secondsbefore, time));
         }
     }*/

    /*public void CortesiaOn ()
    {
        if (Cortesia != null)
        {
            if (lastroutine != null)
                StopCoroutine(lastroutine);
            lastroutine = StartCoroutine(CourtesyLightOn(0, 2));
        }
    }*/

    /*IEnumerator CourtesyLightOn(float secondsbefore, float time)
    {
        if (cortesiaon)
            yield break;
        yield return new WaitForSeconds(secondsbefore);
        var startInt = 0.2f;
        Cortesia.enabled = true;
        while (Cortesia.intensity < CortesiaIntensity)
        {
            Cortesia.intensity += startInt * Time.deltaTime / time;

            yield return null;
        }
        Cortesia.intensity = CortesiaIntensity;
        cortesiaon = true;
    }

    IEnumerator CourtesyLightOff(float secondsbefore, float time)
    {
        if (!cortesiaon)
            yield break;
        yield return new WaitForSeconds(secondsbefore);
        float startInt = Cortesia.intensity;
        
        while (Cortesia.intensity > 0)
        {
            Cortesia.intensity -= startInt * Time.deltaTime / time;

            yield return null;
        }
        Cortesia.intensity = 0;
        Cortesia.enabled = false;
        cortesiaon = false;
    }*/
    public virtual void Start()
    {
        /*var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
            TC = tc.GetComponent<TunnelController>();*/
        if(CanBurn)
        {
            VehicleBody.GetComponent<CanBeOnFireSlave>().Master.OnFireStarted += StartFire;
            VehicleBody.GetComponent<CanBeOnFireSlave>().Master.OnFireStopped += StopFire;
        }
        if (FrecceOnFromStart)
            StartArrows();
    }
    public void StopArrows()
    {
        FrecceOn = false;
        ArrowsOff();
    }

    public void StartArrows()
    {
        if (ArrowsLights != null || PulsanteFrecce != null)
        {
            FrecceOn = true;
            nexttic = Time.time;
        }
    }

    void ArrowsOn()
    {
        if (PulsanteFrecce != null)
            PulsanteFrecce.ArrowsOn();
        if (ArrowsLights != null)
        {
            ArrowsLights.gameObject.SetActive(true);
            for (int j = 0; j < ArrowsMR.Count; j++)
            {
                var m = ArrowsMR[j].materials[ArrowMRIndexes[j]];
                m.EnableKeyword("_EMISSION");
                var m1 = new Material[ArrowsMR[j].materials.Length];
                for (int i = 0; i < ArrowsMR[j].materials.Length; i++)
                {
                    if (i == ArrowMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = ArrowsMR[j].materials[i];
                }
                ArrowsMR[j].materials = m1;
            }
        }
        //audiosource.clip = clips[0];
        arrowson = true;
    }
    void ArrowsOff()
    {
        if (PulsanteFrecce != null)
            PulsanteFrecce.ArrowsOff();
        if (ArrowsLights != null)
        {
            ArrowsLights.gameObject.SetActive(false);
            for (int j = 0; j < ArrowsMR.Count; j++)
            {
                var m = ArrowsMR[j].materials[ArrowMRIndexes[j]];
                m.DisableKeyword("_EMISSION");
                var m1 = new Material[ArrowsMR[j].materials.Length];
                for (int i = 0; i < ArrowsMR[j].materials.Length; i++)
                {
                    if (i == ArrowMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = ArrowsMR[j].materials[i];
                }
                ArrowsMR[j].materials = m1;
            }
        }
        //audiosource.clip = clips[0];
        arrowson = false;
    }

    // Update is called once per frame
    public virtual void Update ()
    {
        var now = Time.time;
        if (EngineOn && !isburning && t <= now)
        {
            VehicleBody.SendMessage("WarmUp", 90, SendMessageOptions.DontRequireReceiver);
            t = now + 0.6f;
        }
        if (FrecceOn)
        {
            if (nexttic <= now)
            {
                if (!arrowson)
                {
                    ArrowsOn();
                    nexttic = now + tictime;
                }
                else
                {
                    ArrowsOff();
                    nexttic = now + tictime;
                }
            }
        }
    }

    public bool IsBurning()
        {
        return isburning;
        }

    void StartFireLoop()
        {
        VehicleBody.SendMessage("StartFireLoop", null, SendMessageOptions.DontRequireReceiver);
        }

    void StopFireLoop()
        {
        VehicleBody.SendMessage("StopFireLoop", null, SendMessageOptions.DontRequireReceiver);
        }

    /*void FireStarted(List<FireEffectController> parts)
        {
        if(SingleAudioSource && !burningsound.isPlaying)
            burningsound.Play();
        foreach (FireEffectController f in parts)
            if (!fires.Contains(f))
                fires.Add(f);
        isburning = true;
        }*/

    void StartFire(CanBeOnFire c)
    {
        var parts = c.parts;
        if (SingleAudioSource && !_burningSound.isPlaying)
            _burningSound.Play();
        foreach (FireEffectController f in parts)
            if (!fires.Contains(f))
                fires.Add(f);
        isburning = true;
    }

    void StopFire(CanBeOnFire c)
    {
        var parts = c.parts;
        foreach (FireEffectController f in parts)
            if (fires.Contains(f))
                fires.Remove(f);
        if (fires.Count == 0)
        {
            if (SingleAudioSource && _burningSound.isPlaying)
                _burningSound.Stop();
            isburning = false;
        }
    }

    /*void StopFire(List<FireEffectController> parts)
        {
        foreach (FireEffectController f in parts)
            if (fires.Contains(f))
                fires.Remove(f);
        if (fires.Count == 0)
            {
            if(SingleAudioSource && burningsound.isPlaying)
                burningsound.Stop();
            isburning = false;
            }
        }*/

    public void TurnOff()
    {
        if (radio != null)
            radio.TurnOff();
        foreach(ParticleSystem s in Scarichi)
            s.Stop();

        if (Quadro != null)
        {
            var m = Quadro.materials;
            var newm = new Material[m.Length];
            for (int i = 0; i < m.Length; i++)
            {
                if (i == EmissionIndex)
                {
                    var newmat = new Material(m[i]);
                    newmat.DisableKeyword("_EMISSION");
                    newm[i] = newmat;
                }
                else
                    newm[i] = m[i];

            }
            Quadro.materials = newm;
        }

		/*if (TextData != null) {

			Color TextColor = hexToColor ("00486FFF");

			for (int j = 0; j < TextData.Count; j++) {

				var m = TextData [j].material;
				var newm = new Material(m);
				newm.color = TextColor;

				TextData[j].color = TextColor;
				TextData[j].material = newm;
			}
		}

		if (TextKm != null) {

			Color TextColor = hexToColor ("560000FF");

			for (int j = 0; j < TextKm.Count; j++) {

				var m = TextKm[j].material;
				var newm = new Material(m);
				newm.color = TextColor;

				TextKm[j].color = TextColor;
				TextKm[j].material = newm;
			}
		}*/

        if(LightsOn)
            TurnOffLights();
        if (braking)
            StopBraking();
        /*
        if (!first)
            first = true;
        else
            CortesiaOn();*/
        if (Cruscotto != null)
            Cruscotto.TurnOff();
    EngineOn = false;
    }

    public void Brake()
    {
        if (!braking)
        {
            if (BackLights != null)
                BackLights.gameObject.SetActive(true);
            
            braking = true;
        }
    }

    public void StopBraking()
    {
        if (braking)
        {
            if (BackLights != null)
                BackLights.gameObject.SetActive(false);
            braking = false;
        }
    }

    public void TurnOnLights()
    {
        if (Frontlights != null)
        {
            Frontlights.gameObject.SetActive(true);
            for (int j = 0; j < FrontlightsMR.Count; j++)
            {
                var m = FrontlightsMR[j].materials[FrontLightMRIndexes[j]];
                m.EnableKeyword("_EMISSION");
                var m1 = new Material[FrontlightsMR[j].materials.Length];
                for (int i = 0; i < FrontlightsMR[j].materials.Length; i++)
                {
                    if (i == FrontLightMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = FrontlightsMR[j].materials[i];
                }
                FrontlightsMR[j].materials = m1;
            }
        }
    }

    public void TurnOffLights()
    {
        if (Frontlights != null)
        {
            Frontlights.gameObject.SetActive(false);
            for (int j = 0; j < FrontlightsMR.Count; j++)
            {
                var m = FrontlightsMR[j].materials[FrontLightMRIndexes[j]];
                m.DisableKeyword("_EMISSION");
                var m1 = new Material[FrontlightsMR[j].materials.Length];
                for (int i = 0; i < FrontlightsMR[j].materials.Length; i++)
                {
                    if (i == FrontLightMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = FrontlightsMR[j].materials[i];
                }
                FrontlightsMR[j].materials = m1;
            }
        }
    }

    public void TurnOn()
    {
        if(radio != null)
            radio.TurnOn();
        foreach (ParticleSystem s in Scarichi)
            s.Play();
        if (Quadro != null)
        {
            var m = Quadro.materials;
            var newm = new Material[m.Length];
            for (int i = 0; i < m.Length; i++)
            {
                if (i == EmissionIndex)
                {
                    var newmat = new Material(m[i]);
                    newmat.EnableKeyword("_EMISSION");
                    newm[i] = newmat;
                }
                else
                    newm[i] = m[i];

            }
            Quadro.materials = newm;
        }

		/*if (TextData != null) {

			Color TextColor = hexToColor ("00FF6FFF");

			for (int j = 0; j < TextData.Count; j++) {

				var m = TextData[j].material;
				var newm = new Material(m);
				newm.color = TextColor;

				TextData[j].color = TextColor;
				TextData[j].material = newm;
			}
		}

		if (TextKm != null) {

			Color TextColor = hexToColor ("FF0000FF");

			for (int j = 0; j < TextKm.Count; j++) {

				var m = TextKm[j].material;
				var newm = new Material(m);
				newm.color = TextColor;

				TextKm[j].color = TextColor;
				TextKm[j].material = newm;
			}
		}*/

        if (LightsOn)
            TurnOnLights();

        /*if (!first)
            first = true;
        else
            CortesiaOff(0, 2);*/
        if(Cruscotto != null)
            Cruscotto.TurnOn();
        EngineOn = true;
    }

	public static Color hexToColor(string hex)
	{
		hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		//Only use alpha if the string has enough characters
		if(hex.Length == 8){
			a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r,g,b,a);
	}
}
