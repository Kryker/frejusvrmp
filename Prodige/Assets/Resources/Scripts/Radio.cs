﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Radio : MonoBehaviour
{
    public AudioSource noise, message;
    bool TurnedOff = true;
    public AudioClip MessageITA, MessageENG, MessageFRA, AlarmMessageITA, AlarmMessageENG, AlarmMessageFRA;
    AudioClip Message, AlarmMessage;
    //float nextmessage = 5;
    bool firemessageplayed, firstmessageplayed = false;
    // Use this for initialization
    void Start()
    {
        if(noise == null)
            noise = GetComponents<AudioSource>()[0];
        if(message == null)
            message = GetComponents<AudioSource>()[1];
        if(GameManager.Instance != null)
        {
            switch(GameManager.Instance.AppLanguage)
            {
                case SystemLanguage.Italian:
                    Message = MessageITA;
                    AlarmMessage = AlarmMessageITA;
                    break;
                case SystemLanguage.French:
                    Message = MessageFRA;
                    AlarmMessage = AlarmMessageFRA;
                    break;
                default:
                    Message = MessageENG;
                    AlarmMessage = AlarmMessageENG;
                    break;
            }
        }
        else
        {
            Message = MessageITA;
            AlarmMessage = AlarmMessageITA;
        }
        message.clip = Message;
        //message.Play();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TurnOff()
    {
        if (!TurnedOff)
        {
            if (message.isPlaying)
                message.Stop();
            noise.Stop();
            TurnedOff = true;
        }
    }

    public void volume(float volume)
    {
        message.volume = volume;
        noise.volume = volume;
    }

    public void TurnOn()
    {
        if (TurnedOff && GameManager.Instance.TCN.GameStarted)
        {
            if (!firstmessageplayed)
                { 
                message.Play();
                firstmessageplayed = true;
                }
            noise.Play();
            TurnedOff = false;
            if (GameManager.Instance.TCN.firestarted && !firemessageplayed)
                FireMessage();
        }
    }

    internal void FireMessage()
    {
        if (firemessageplayed || TurnedOff)
            return;
        if ((message.isPlaying))
            message.Stop();
        message.clip = AlarmMessage;
        if (!noise.isPlaying)
            noise.Play();
        message.Play();
        firemessageplayed = true;
    }
}
