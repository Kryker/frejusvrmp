﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetLegRotation : MonoBehaviour {

    public FootSwinger swing;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Quaternion temp1, temp2;
        transform.rotation = swing.determineAverageLegControllerRotationPublic(out temp1, out temp2);
	}
}
