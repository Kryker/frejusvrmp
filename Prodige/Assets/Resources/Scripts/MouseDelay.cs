﻿using UnityEngine;
using System.Collections;

public class MouseDelay : MonoBehaviour
    {
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 };
    public MouseDelay.RotationAxes axes = RotationAxes.MouseXAndY;
    public float amount = 0.02f;
    public float maxAmount = 0.03f;
    public float smooth = 3;
    private Vector3 def;
    public Vector3 curPos;
    public float factorX;
    public float factorY;

    void Start()
        {
        def = transform.localPosition;
        }

    void Update()
        {
        curPos = transform.localPosition;
        if (axes == RotationAxes.MouseXAndY)
            {
            factorX = -Input.GetAxis("Mouse X") * amount;
            factorY = -Input.GetAxis("Mouse Y") * amount;
            }
        else if (axes == RotationAxes.MouseX)
            {
            factorX = -Input.GetAxis("Mouse X") * amount;
            }
        else if (axes == RotationAxes.MouseY)
            {
            factorY = -Input.GetAxis("Mouse Y") * amount;
            }
        if (factorX > maxAmount)
            factorX = maxAmount;

        if (factorX < -maxAmount)
            factorX = -maxAmount;

        if (factorY > maxAmount)
            factorY = maxAmount;

        if (factorY < -maxAmount)
            factorY = -maxAmount;


        Vector3 pos = new Vector3(def.x + factorX, def.y + factorY, def.z);
        transform.localPosition = Vector3.Lerp(transform.localPosition, pos, Time.deltaTime * smooth);
        }
    }