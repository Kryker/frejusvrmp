﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NicchiaVicina : MonoBehaviour
{
	public enum Lato { Sinistro, Destro }
	public Lato LatoCartelli;
    public int ForcedValue;
    public Color TextColor = Color.black;
    Transform CartelloDX, CartelloSX;
	TunnelTelephoneSectionControllerNet telefono, _sezioneDiRiferimento;
    public Transform SezioneDiRiferimento;
	int num;
    bool startlater = false;

	public void Start()
{
         _sezioneDiRiferimento = SezioneDiRiferimento.GetComponentInChildren<TunnelTelephoneSectionControllerNet>();
        if (_sezioneDiRiferimento == null)
            startlater = true;
        else
        {
            CartelloDX = transform.Find("SOSItalia");
            CartelloSX = transform.Find("SOSFrancia");

            if (CartelloSX != null)
            {
                if (LatoCartelli == Lato.Sinistro)
                {
                    if (transform.position.z < SezioneDiRiferimento.position.z)
                        telefono = _sezioneDiRiferimento.Precedente;
                    else
                        telefono = _sezioneDiRiferimento;
                }
                else
                {
                    if (transform.position.z < SezioneDiRiferimento.position.z)
                        telefono = _sezioneDiRiferimento;
                    else
                        telefono = _sezioneDiRiferimento.Successiva;
                }

                var txt = CartelloSX.GetComponentInChildren<Text>();
                var m = txt.material;
                var newm = new Material(m);
                newm.color = TextColor;
                txt.color = TextColor;
                txt.material = newm;
                if (telefono != null)
                {
                    if (LatoCartelli == Lato.Destro)
                        num = telefono.NumNicchiaDX;
                    else
                        num = telefono.NumNicchiaSX;

                    txt.text = num.ToString();
                }
                else
                    txt.text = Convert.ToInt32(ForcedValue).ToString();
            }
            if (CartelloDX != null)
            {
                if (LatoCartelli == Lato.Sinistro)
                {
                    if (transform.position.z < SezioneDiRiferimento.position.z)
                        telefono = _sezioneDiRiferimento;
                    else
                        telefono = _sezioneDiRiferimento.Successiva;
                }
                else
                {
                    if (transform.position.z < SezioneDiRiferimento.position.z)
                        telefono = _sezioneDiRiferimento.Precedente;
                    else
                        telefono = _sezioneDiRiferimento;
                }

                var txt = CartelloDX.GetComponentInChildren<Text>();
                var m = txt.material;
                var newm = new Material(m);
                newm.color = TextColor;
                txt.color = TextColor;
                txt.material = newm;
                if (telefono != null)
                {
                    if (LatoCartelli == Lato.Destro)
                        num = telefono.NumNicchiaDX;
                    else
                        num = telefono.NumNicchiaSX;

                    txt.text = num.ToString();
                }
                else
                    txt.text = Convert.ToInt32(ForcedValue).ToString();
            }
            enabled = false;
        }
    }
    private void Update()
    {
        if (startlater)
        {
            _sezioneDiRiferimento = SezioneDiRiferimento.GetComponentInChildren<TunnelTelephoneSectionControllerNet>();
            if (_sezioneDiRiferimento != null)
            {
                CartelloDX = transform.Find("SOSItalia");
                CartelloSX = transform.Find("SOSFrancia");

                if (CartelloSX != null)
                {
                    if (LatoCartelli == Lato.Sinistro)
                    {
                        if (transform.position.z < SezioneDiRiferimento.position.z)
                            telefono = _sezioneDiRiferimento.Precedente;
                        else
                            telefono = _sezioneDiRiferimento;
                    }
                    else
                    {
                        if (transform.position.z < SezioneDiRiferimento.position.z)
                            telefono = _sezioneDiRiferimento;
                        else
                            telefono = _sezioneDiRiferimento.Successiva;
                    }

                    var txt = CartelloSX.GetComponentInChildren<Text>();
                    var m = txt.material;
                    var newm = new Material(m);
                    newm.color = TextColor;
                    txt.color = TextColor;
                    txt.material = newm;
                    if (telefono != null)
                    {
                        if (LatoCartelli == Lato.Destro)
                            num = telefono.NumNicchiaDX;
                        else
                            num = telefono.NumNicchiaSX;

                        txt.text = num.ToString();
                    }
                    else
                        txt.text = Convert.ToInt32(ForcedValue).ToString();
                }
                if (CartelloDX != null)
                {
                    if (LatoCartelli == Lato.Sinistro)
                    {
                        if (transform.position.z < SezioneDiRiferimento.position.z)
                            telefono = _sezioneDiRiferimento;
                        else
                            telefono = _sezioneDiRiferimento.Successiva;
                    }
                    else
                    {
                        if (transform.position.z < SezioneDiRiferimento.position.z)
                            telefono = _sezioneDiRiferimento.Precedente;
                        else
                            telefono = _sezioneDiRiferimento;
                    }

                    var txt = CartelloDX.GetComponentInChildren<Text>();
                    var m = txt.material;
                    var newm = new Material(m);
                    newm.color = TextColor;
                    txt.color = TextColor;
                    txt.material = newm;
                    if (telefono != null)
                    {
                        if (LatoCartelli == Lato.Destro)
                            num = telefono.NumNicchiaDX;
                        else
                            num = telefono.NumNicchiaSX;

                        txt.text = num.ToString();
                    }
                    else
                        txt.text = Convert.ToInt32(ForcedValue).ToString();
                }
                startlater = false;
                enabled = false;
            }
        }
    }
}
