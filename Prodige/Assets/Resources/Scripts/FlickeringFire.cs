﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class FlickeringFire : MonoBehaviour
    {    
    float minIntensity = 0.25f;
    float maxIntensity = 0.5f;

    float speed = 2;

    private float random;

    private void Start()
        {
        random = UnityEngine.Random.Range(0.0f, 65535.0f);
        }
    void Update()
        {
        float noise = Mathf.PerlinNoise(random, speed * Time.time);
        GetComponent<Light>().intensity = Mathf.Lerp(minIntensity, maxIntensity, noise);
        }
    }