﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public OverriddenNetworkDiscovery NetworkDiscovery;
    public OverriddenNetworkManager NetworkManager;


    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
  
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Italian:
                    AppLanguage = SystemLanguage.Italian;
                    break;
                case SystemLanguage.French:
                    AppLanguage = SystemLanguage.French;
                    break;
                default:
                    AppLanguage = SystemLanguage.English;
                    break;
            }
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        SteamVR_Fade.View(Color.black, 0);
        //UnityEngine.VR.VRSettings.enabled = false;
    }

    //Dichiara e inizializza qui sotto le variabili globali
    public ControllerType Platform;
    public StageControllerNet.GameResult ResultNet = null;
    public StageController.GameResult Result = null;
	[HideInInspector]
    public bool SinglePlayer = false;
    [HideInInspector]
    public bool SinglePlayerExtended = false;
    [HideInInspector]
    public int PlayerModel = -1;
	[HideInInspector]
    public SystemLanguage AppLanguage;
	[HideInInspector]
    public TunnelController TC;
	[HideInInspector]
    public TunnelControllerNet TCN;
	[HideInInspector]
    public bool Server = false;
	[HideInInspector]
    public float PlayerHeight = 0;
	[HideInInspector]
    public float PlayerArmScale = 1;
    public bool DebugMode = false;
    [HideInInspector]
    public uint MaxPlayers = uint.MaxValue; //lo imposto dalla selezione da server
    public LoadSmokeFileASync SmokeFile;
    public bool NoCamionistaNPC = false;
    public bool CorpoVisibile = false;
    public bool DannoDaFumo = false;
    public bool debugTuta = false;
}

