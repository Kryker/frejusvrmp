﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trip : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "NPC")
        {
            var a = other.GetComponentInParent<CamionistaController>();
            if (a != null)
            {
                a.Trip();
                gameObject.SetActive(false);
            }
        }
    }
}
