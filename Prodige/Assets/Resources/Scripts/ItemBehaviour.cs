﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{
    public enum ControllerButton { Trigger, Pad, LGrip, RGrip, Grip };
    public ControllerButton cbutton = ControllerButton.Trigger;
    public string mousebutton = "Fire Left Mouse";
    public bool UseMouseAsController;
    [HideInInspector]
    public Transform controller;
    [HideInInspector]
    public SteamVR_TrackedController _controller;
    public InputManagementNet InputNet;
    public InputManagementNet.Hand hand = InputManagementNet.Hand.Left;
    [HideInInspector]
    public bool Networked;

    // Use this for initialization
    void Start()
    {
        if (InputNet == null)
            Networked = false;
        else
            Networked = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual void OnServer()
    {

    }

    public virtual void OnClient()
    {

    }

    public virtual void OnDisable()
    {
        if (InputNet != null)
        {
            if (UseMouseAsController)
            {
                if (mousebutton == "Fire Left Mouse")
                {
                    InputNet.OnLeftMouseClicked -= HandleClicked;
                    InputNet.OnLeftMouseUnclicked -= HandleUnClicked;
                }
                else if (mousebutton == "Fire Right Mouse")
                {
                    InputNet.OnRightMouseClicked -= HandleClicked;
                    InputNet.OnLeftMouseUnclicked -= HandleUnClicked;
                }
            }
            else
            {
                if (hand == InputManagementNet.Hand.Left)
                    switch (cbutton)
                    {
                        case ControllerButton.Trigger:
                            InputNet.OnLeftTriggerClicked -= HandleClicked;
                            InputNet.OnLeftTriggerUnclicked -= HandleUnClicked;
                            break;
                        case ControllerButton.Grip:
                            InputNet.OnLeftGripped -= HandleClicked;
                            InputNet.OnLeftUngripped -= HandleUnClicked;
                            break;
                        case ControllerButton.Pad:
                            InputNet.OnLeftPadPressed -= HandleClicked;
                            InputNet.OnLeftPadUnpressed -= HandleUnClicked;
                            break;
                        default:
                            break;
                    }
                else
                    switch (cbutton)
                    {
                        case ControllerButton.Trigger:
                            InputNet.OnRightTriggerClicked -= HandleClicked;
                            InputNet.OnRightTriggerUnclicked += HandleUnClicked;
                            break;
                        case ControllerButton.Grip:
                            InputNet.OnRightGripped -= HandleClicked;
                            InputNet.OnRightUngripped -= HandleUnClicked;
                            break;
                        case ControllerButton.Pad:
                            InputNet.OnRightPadPressed -= HandleClicked;
                            InputNet.OnRightPadUnpressed -= HandleUnClicked;
                            break;
                        default:
                            break;
                    }
            }
        }

        if (controller != null)
        {
            _controller = controller.GetComponent<SteamVR_TrackedController>();
            if (_controller != null)
            {
                /*switch (cbutton)
                {
                    case ControllerButton.Trigger:
                        _controller.TriggerClicked -= HandleClicked;
                        _controller.TriggerUnclicked -= HandleUnClicked;
                        break;
                    case ControllerButton.Grip:
                        _controller.Gripped -= HandleClicked;
                        _controller.Ungripped -= HandleUnClicked;
                        break;
                    case ControllerButton.Pad:
                        _controller.PadClicked -= HandleClicked;
                        _controller.PadUnclicked -= HandleUnClicked;
                        break;
                    default:
                        break;
                }*/

                /*var ic = controller.GetComponent<ItemController>();
                if (ic != null)
                    ic.StopPulsePublic();*/
            }
            /*else
            {
                var c1 = controller.GetComponentInParent<FirstPersonItemController>();
                if (c1 != null)
                {
                    if (controller == c1.controllerleft)
                    {*/
            /*c1.OnLeftButtonClicked -= HandleClicked;
            c1.OnLeftButtonUnClicked -= HandleUnClicked;*/
            /*}
            else if (controller == c1.controllerright)
            {*/
            /*c1.OnRightButtonClicked -= HandleClicked;
            c1.OnRightButtonUnClicked -= HandleUnClicked;*/
            /* }
         }
     }*/
        }
    }
    public virtual void HandleClicked(object sender, ClickedEventArgs e)
    { }

    public virtual void HandleUnClicked(object sender, ClickedEventArgs e)
    { }

    public virtual void OnEnable()
    {
        if (InputNet != null)
        {
            if (UseMouseAsController)
            {
                if (mousebutton == "Fire Left Mouse")
                {
                    InputNet.OnLeftMouseClicked += HandleClicked;
                    InputNet.OnLeftMouseUnclicked += HandleUnClicked;
                }
                else if (mousebutton == "Fire Right Mouse")
                {
                    InputNet.OnRightMouseClicked += HandleClicked;
                    InputNet.OnLeftMouseUnclicked += HandleUnClicked;
                }
            }
            else
            {
                if (hand == InputManagementNet.Hand.Left)
                    switch (cbutton)
                    {
                        case ControllerButton.Trigger:
                            InputNet.OnLeftTriggerClicked += HandleClicked;
                            InputNet.OnLeftTriggerUnclicked += HandleUnClicked;
                            break;
                        case ControllerButton.Grip:
                            InputNet.OnLeftGripped += HandleClicked;
                            InputNet.OnLeftUngripped += HandleUnClicked;
                            break;
                        case ControllerButton.Pad:
                            InputNet.OnLeftPadPressed += HandleClicked;
                            InputNet.OnLeftPadUnpressed += HandleUnClicked;
                            break;
                        default:
                            break;
                    }
                else
                    switch (cbutton)
                    {
                        case ControllerButton.Trigger:
                            InputNet.OnRightTriggerClicked += HandleClicked;
                            InputNet.OnRightTriggerUnclicked += HandleUnClicked;
                            break;
                        case ControllerButton.Grip:
                            InputNet.OnRightGripped += HandleClicked;
                            InputNet.OnRightUngripped += HandleUnClicked;
                            break;
                        case ControllerButton.Pad:
                            InputNet.OnRightPadPressed += HandleClicked;
                            InputNet.OnRightPadUnpressed += HandleUnClicked;
                            break;
                        default:
                            break;
                    }
            }
        }
        if (controller != null)
        {
            _controller = controller.GetComponent<SteamVR_TrackedController>();
            if (_controller != null)
            {
                /*switch (cbutton)
                {
                    case ControllerButton.Trigger:
                        _controller.TriggerClicked += HandleClicked;
                        _controller.TriggerUnclicked += HandleUnClicked;
                        break;
                    case ControllerButton.Grip:
                        _controller.Gripped += HandleClicked;
                        _controller.Ungripped += HandleUnClicked;
                        break;
                    case ControllerButton.Pad:
                        _controller.PadClicked += HandleClicked;
                        _controller.PadUnclicked += HandleUnClicked;
                        break;
                    default:
                        break;
                }*/

                /*var ic = controller.GetComponent<ItemController>();
                if (ic != null)
                    ic.StartPulsePublic(cbutton);*/
            }
            else
            {
                /*var c1 = controller.GetComponentInParent<FirstPersonItemController>();
                if (c1 != null)
                {
                    if (controller == c1.controllerleft)
                    {*/
                /*c1.OnLeftButtonClicked += HandleClicked;
                c1.OnLeftButtonUnClicked += HandleUnClicked;*/
                /*}
                else if (controller == c1.controllerright)
                {*/
                /*c1.OnRightButtonClicked += HandleClicked;
                c1.OnRightButtonUnClicked += HandleUnClicked;*/
                /*}
            }*/
            }
        }
    }
}
