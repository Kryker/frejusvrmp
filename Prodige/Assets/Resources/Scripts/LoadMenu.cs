﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoadMenu : MonoBehaviour
{
    public string Scene = "MainMenu";

    public void Start()
    {
       LoadMenuScene();
    }
    // Use this for initialization
    public void LoadMenuScene()
    {
        SceneManager.LoadScene(Scene);
    }
}
    