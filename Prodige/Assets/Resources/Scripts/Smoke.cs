﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Smoke : MonoBehaviour
    {
    public float Toxicity = 10.0f;
    public float Temperature = 24.0f;
    [HideInInspector]
    public ParticleSystem ParticleSmoke;
    public float maxRange = 1000;
    bool discardfirst = false;
    public List<ParticleCollisionEvent> collisionEvents;
    TunnelController TC;    

    void Start()
    {
        ParticleSmoke = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
            TC = tc.GetComponent<TunnelController>();
    }

    private void OnParticleCollision(GameObject other)
    {
            if (other.gameObject.GetComponent<CanBeOnFire>() != null || other.gameObject.GetComponent<CanBeOnFireSlave>() != null || other.gameObject.GetComponent<CanBeOnFireNet>() != null)
            {
                if (collisionEvents == null)
                    return;
                int numCollisionEvents = ParticleSmoke.GetCollisionEvents(other, collisionEvents);
                int i = 0;

                while (i < numCollisionEvents)
                {
                    Vector3 pos = collisionEvents[i].intersection;
                    var realtemp = Temperature * (maxRange - Vector3.Distance(pos, other.transform.position)) / maxRange;
                    if (TC != null)
                    {
                        if (realtemp < TC.GetAmbientTemp())
                            realtemp = TC.GetAmbientTemp();
                    }
                    else
                    {
                        if (realtemp < 24.0f)
                            realtemp = 24.0f;
                    }
                    other.gameObject.SendMessageUpwards("WarmUp", realtemp, SendMessageOptions.DontRequireReceiver);
                    i++;
                }
            }
            else if (other.gameObject.GetComponent<Mouth>() != null)
        {
            if (!discardfirst)
            {
                discardfirst = true;
                return;
            }
            if (Toxicity > 0)
                    other.gameObject.SendMessageUpwards("Intoxicate", Toxicity, SendMessageOptions.DontRequireReceiver);
            }
    }
}