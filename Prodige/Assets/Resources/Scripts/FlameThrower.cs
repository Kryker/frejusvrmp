﻿using UnityEngine;
using System.Collections;
using System;

public class FlameThrower : MonoBehaviour
{
    public bool UseMouseAsController = false;
    string mousebutton = "Fire Left Mouse";
    public ControllerButton cbutton = ControllerButton.Trigger;
    public ParticleSystem flameEmit;
    VibrationController vibr;
    public Transform firelight;
    public float fuelduration = 30.0f;
    [HideInInspector]
    public Transform controller;
    SteamVR_TrackedController _controller;
    [HideInInspector]
    public Extinguisher Ext;
    public AudioClip SecuredButtonUp, SecuredButtonDown;
    AudioSource audiosource;
    Transform GrabbedExt;

    bool firing = false;

    private void Awake()
    {
        GrabbedExt = transform.parent;
        audiosource = GetComponent<AudioSource>();
    }

    private void OnDisable()
    {
        if (firing)
            StopFire();

        if (controller != null)
        {
            _controller = controller.GetComponent<SteamVR_TrackedController>();
            if (_controller != null)
            {
                /*switch (cbutton)
                {
                    case ControllerButton.Trigger:
                        _controller.TriggerClicked -= HandleClicked;
                        _controller.TriggerUnclicked -= HandleUnClicked;
                        break;
                    case ControllerButton.Grip:
                        _controller.Gripped -= HandleClicked;
                        _controller.Ungripped -= HandleUnClicked;
                        break;
                    case ControllerButton.Pad:
                        _controller.PadClicked -= HandleClicked;
                        _controller.PadUnclicked -= HandleUnClicked;
                        break;
                    default:
                        break;
                }*/

                //var ic = controller.GetComponent<VRItemController>();
                var ic = controller.GetComponent<ControllerManager>();
                if (ic != null)
                    ic.StopPulsePublic();
            }
            else
            {
                var c1 = controller.GetComponentInParent<FirstPersonItemController>();
                if (c1 != null)
                {
                    if (controller == c1.LeftController)
                    {
                        /*c1.OnLeftButtonClicked -= HandleClicked;
                        c1.OnLeftButtonUnClicked -= HandleUnClicked;*/
                    }
                    else if (controller == c1.RightController)
                    {
                        /*c1.OnRightButtonClicked -= HandleClicked;
                        c1.OnRightButtonUnClicked -= HandleUnClicked;*/
                    }
                }
            }
        }
    }

    internal void ParticleDisabled(AdvancedStateMachineBehaviour a)
    {
        StopFire();
    }

    internal void ParticleEnabled(AdvancedStateMachineBehaviour a)
    {
        StartFire();
    }

    private void OnEnable()
    {
            if(controller == null)
                controller = GrabbedExt.parent;
            if (controller != null)
                {
                _controller = controller.GetComponent<SteamVR_TrackedController>();
                if (_controller != null)
                {
                /*switch (cbutton)
                {
                    case ControllerButton.Trigger:
                        _controller.TriggerClicked += HandleClicked;
                        _controller.TriggerUnclicked += HandleUnClicked;
                        break;
                    case ControllerButton.Grip:
                        _controller.Gripped += HandleClicked;
                        _controller.Ungripped += HandleUnClicked;
                        break;
                    case ControllerButton.Pad:
                        _controller.PadClicked += HandleClicked;
                        _controller.PadUnclicked += HandleUnClicked;
                        break;
                    default:
                        break;
                }*/

                //var ic = controller.GetComponent<VRItemController>();
                var ic = controller.GetComponent<ControllerManager>();
                if (ic != null)
                        ic.StartPulsePublic(cbutton);
                }
                else
                {
                    var c1 = controller.GetComponentInParent<FirstPersonItemController>();
                    if (c1 != null)
                    {
                        if (controller == c1.LeftController)
                        {
                            /*c1.OnLeftButtonClicked += HandleClicked;
                            c1.OnLeftButtonUnClicked += HandleUnClicked;*/
                        }
                        else if (controller == c1.RightController)
                        {
                            /*c1.OnRightButtonClicked += HandleClicked;
                            c1.OnRightButtonUnClicked += HandleUnClicked;*/
                        }
                    }
                }
            }
        }


    /*private void HandleClicked(object sender, ClickedEventArgs e)
        {
        StartFire();
        }

    private void HandleUnClicked(object sender, ClickedEventArgs e)
        {
        StopFire();
        }*/


    void Update()
    {
        if (UseMouseAsController)
        {
            if (Input.GetButtonDown(mousebutton))
            {
                StartFire();
            }
            if (Input.GetButtonDown(mousebutton))
            {
                StopFire();
            }
        }
        if(firing)
            {
            var remainingtime = fuelduration * Ext.Fuel / 100;
            remainingtime = remainingtime - Time.deltaTime;
            if(remainingtime <= 0)
                {
                Ext.Fuel = 0;
                }
            else
                Ext.Fuel = remainingtime / fuelduration * 100;
            if (Ext.Fuel <= 0)
            {
                Ext.Fuel = 0.0f;
                CancelInvoke("Openfire");
                if (vibr != null)
                {
                    vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
                    vibr = null;
                }
                flameEmit.Stop();
                if (firelight != null)
                    firelight.GetComponent<Light>().enabled = false;
                //anim.SetBool("Pressed", false);
                flameEmit.GetComponent<AudioSource>().Stop();
                firing = false;

                if (_controller != null)
                {
                   /* switch (cbutton)
                    {
                        case ControllerButton.Trigger:
                            _controller.TriggerClicked -= HandleClicked;
                            _controller.TriggerUnclicked -= HandleUnClicked;
                            break;
                        case ControllerButton.Grip:
                            _controller.Gripped -= HandleClicked;
                            _controller.Ungripped -= HandleUnClicked;
                            break;
                        case ControllerButton.Pad:
                            _controller.PadClicked -= HandleClicked;
                            _controller.PadUnclicked -= HandleUnClicked;
                            break;
                        default:
                            break;
                    }*/
                }
                else
                {
                    var c1 = controller.GetComponentInParent<FirstPersonItemController>();
                    if (c1 != null)
                    {
                        if (controller == c1.LeftController)
                        {
                           /* c1.OnLeftButtonClicked -= HandleClicked;
                            c1.OnLeftButtonUnClicked -= HandleUnClicked;*/
                        }
                        else if (controller == c1.RightController)
                        {
                           /* c1.OnRightButtonClicked -= HandleClicked;
                            c1.OnRightButtonUnClicked -= HandleUnClicked;*/
                        }
                    }
                }
            }
        }
    }


        void Openfire()
        {
        flameEmit.Play();
        if(firelight!=null)
            firelight.GetComponent<Light>().enabled = true;
        flameEmit.GetComponent<AudioSource>().Play();

        if (vibr == null)
        {
            //var ic = controller.GetComponent<VRItemController>();
            var ic = controller.GetComponent<ControllerManager>();
            if (ic != null)
            {
                var v = ic.vibrationController;
                if (v.StartVibration(0.05f, 0.65f, 0.04f, this))
                    vibr = v;
            }
        }
        }


    void StartFire()
    {
        if (gameObject.activeSelf && Ext.Secured == SecureState.Open)
        {
            Invoke("Openfire", 0.1f);
            //var ic = controller.GetComponent<VRItemController>();
            var ic = controller.GetComponent<ControllerManager>();
            if (ic != null)
                ic.StopPulsePublic();
            firing = true;
        }
        else if (gameObject.activeSelf)
        {
            audiosource.Stop();
            audiosource.clip = SecuredButtonDown;
            audiosource.Play();
        }


    }

    void StopFire()
    {
        if (gameObject.activeSelf && (Ext == null || Ext.Secured == SecureState.Open))
        {
            CancelInvoke("Openfire");
            flameEmit.Stop();
            if (firelight != null)
                firelight.GetComponent<Light>().enabled = false;
            //anim.SetBool("Pressed", false);
            flameEmit.GetComponent<AudioSource>().Stop();
            if (vibr != null)
                { 
                vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
                vibr = null;
                }
            firing = false;
            }
            else if (gameObject.activeSelf)
            {
                audiosource.Stop();
                audiosource.clip = SecuredButtonUp;
                audiosource.Play();
            }
    }
}