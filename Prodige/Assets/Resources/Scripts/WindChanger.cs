﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(SteamVR_TrackedController))]
public class WindChanger : MonoBehaviour {
    
    public Transform controller;
    SteamVR_TrackedController _controller;
    public Transform tunnelController;
    public bool VRSupportEnabled = false;

    // Use this for initialization
    void Start ()
        {
        if (VRSupportEnabled && controller != null)
            {
            _controller = controller.GetComponent<SteamVR_TrackedController>();
            if (_controller != null)
                {
                _controller.Gripped += HandleGripped;
                _controller.Ungripped += HandleUngripped;
                }
            }
        }

    private void HandleGripped(object sender, ClickedEventArgs e)
        {
        tunnelController.GetComponent<TunnelController>().ChangeWindDirection(WindDirection.Favorable);
        }

    private void HandleUngripped(object sender, ClickedEventArgs e)
        {
        tunnelController.GetComponent<TunnelController>().ChangeWindDirection(WindDirection.Headwind);
        }

    // Update is called once per frame
    void Update()
        {
        if (!VRSupportEnabled)
            {
            if (Input.GetKeyDown(KeyCode.I))
                {
                tunnelController.GetComponent<TunnelController>().ChangeWindDirection(WindDirection.Favorable);
                }
            if (Input.GetKeyUp(KeyCode.I))
                {
                tunnelController.GetComponent<TunnelController>().ChangeWindDirection(WindDirection.Headwind);
                }
            }
        }
    
}
