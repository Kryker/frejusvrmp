﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableLimit : MonoBehaviour {

    public Collider PlayerLimit;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        {
            var c = other.transform.parent.GetComponentInParent<CarMovementScriptNet>();
              if (c != null)
            {
                PlayerLimit.enabled = true;
                gameObject.SetActive(false);
            }
        }
    }
}
