﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour {
    Stage CurrStage;
    /*public uint ErrorCount = 0;
    public uint BonusCount = 0;
    public uint MaxErrors = 3;
    public uint MaxBonus = 4;*/
    //public enum Stage {Start, InCar, InTunnel, End};
    bool _arrowsOn = false;
    float ArrowsOnTime;
    bool _engineOff = false;
    float EngineOffTime;

    public SpawnMode Ruolo;

    public bool ArrowsOn
    {
        get { return _arrowsOn; }
        set
        {
            if (!_arrowsOn && value)
            {
                ArrowsOnTime = Time.time;
            }
            _arrowsOn = value;
        }
    }
    public bool EngineOff
    {
        get { return _engineOff; }
        set
        {
            if (!_engineOff && value)
            {
                EngineOffTime = Time.time;
            }
            _engineOff = value;
        }
    }
    public bool SOSRequested
    {
        get { return _SOSRequested; }
        set
            {
            if (!_SOSRequested && value)
            {
                SOSRequestedTime = Time.time;
                if (TC != null)
                    TC.StartWarningProcedures();
                _SOSRequested = value;
                if (CurrStage == Stage.End)
                    EndGame();
            }
            else
                _SOSRequested = value;
        }
    }
    public bool AlarmTriggered
    {
        get { return _AlarmTriggered; }
        set
        {
            if (!_AlarmTriggered && value)
            {
                AlarmTriggeredTime = Time.time;
                if (TC != null)
                    TC.StartWarningProcedures();
            }
            _AlarmTriggered = value;
        }
    }
    public bool ShelterReached
    {
        get { return _ShelterReached; }
        set
        {
            if (!_ShelterReached && value)
            {
                ShelterReachedTime = Time.time;
            }
            _ShelterReached = value;
        }
    }
    bool _SOSRequested = false;
    float SOSRequestedTime;
    public bool DistanceMaintained = true;
    bool _AlarmTriggered = false;
    float AlarmTriggeredTime;
    bool _ShelterReached = false;
    float ShelterReachedTime;
    public bool CarLeft = false;
    public bool CamionistaSaved = false;
    //public bool BenchReached = false;
    //public float SecondsBeforeSpread = 120;
    uint fireCount;
    //bool windspeedchanged = false;
    bool SomethingBurning = false;
    Coroutine endgameroutine;
    public bool ExtinguisherUsed = false;
    TunnelController TC;
    float StartTime, EndTime;
    public bool firestarted;

    private void Awake()
    {
        TC = GetComponent<TunnelController>();
    }

    public class GameResult
    {
  
        public StageControllerNet.GameResult.Risultati risultati;

        public GameResult(StageController s)
        {
            risultati.StageReached = s.CurrStage;
            risultati.ArrowsOn = s.ArrowsOn;
            risultati.ArrowsOnTime = s.ArrowsOnTime - s.StartTime;
            risultati.EngineOff = s.EngineOff;
            risultati.EngineOffTime = s.EngineOffTime - s.StartTime;
            risultati.SOSRequested = s.SOSRequested;
            risultati.SOSRequestedTime = s.SOSRequestedTime - s.StartTime;
            risultati.AlarmTriggered = s.AlarmTriggered;
            risultati.AlarmTriggeredTime = s.AlarmTriggeredTime - s.StartTime;
            risultati.ShelterReached = s.ShelterReached;
            risultati.ShelterReachedTime = s.ShelterReachedTime - s.StartTime;
            risultati.DistanceMaintained = s.DistanceMaintained;
            //ExtinguisherUsed = s.ExtinguisherUsed;
            risultati.CamionistaSaved = s.CamionistaSaved;
            risultati.Time = s.EndTime - s.StartTime;
        }
        public override string ToString()
        {
            string s = "";
            if (risultati.DistanceMaintained)
                s += "DIS: 1 ";
            else
                s += "DIS: 0 ";
            if (risultati.ArrowsOn)
                s += "HAZ: 1 (" + MainMenu.FloatTimeToString(risultati.ArrowsOnTime) + ") ";
            else
                s += "HAZ: 0 ";
            if (risultati.EngineOff)
                s += "ENG: 1 (" + MainMenu.FloatTimeToString(risultati.EngineOffTime) + ") ";
            else
                s += "ENG: 0 ";
            if (risultati.AlarmTriggered)
                s += "ALA: 1 (" + MainMenu.FloatTimeToString(risultati.AlarmTriggeredTime) + ") ";
            else
                s += "ALA: 0 ";
            if (risultati.SOSRequested)
                s += "SOS: 1 (" + MainMenu.FloatTimeToString(risultati.SOSRequestedTime) + ") ";
            else
                s += "SOS: 0 ";
            if (risultati.ShelterReached)
                s += "SHE: 1 (" + MainMenu.FloatTimeToString(risultati.ShelterReachedTime) + ") ";
            else
                s += "SHE: 0 ";

            return s += "TIME: " + MainMenu.FloatTimeToString(risultati.Time);
        }
    }

    public void StopEndRoutine()
    {
        if (endgameroutine != null)
            StopCoroutine(endgameroutine);
    }
    

    internal void FireStarted()
    {
        firestarted = true;
    }

    public void EnterShelter()
    {
    GoInsideShelter();
    EndGame();
    }

    public void ExitShelter()
    {
        GoOutsideShelter();
        StopEndRoutine();
    }

    public uint FireCount
    {
        get { return fireCount; }
        set {
            var prev = fireCount;
            fireCount = value;
            if (fireCount > 0 && prev == 0)
                SomethingBurning = true;
            else if (fireCount == 0 && prev > 0)
                {
                //TC.DisableJingle();
                SomethingBurning = false;
                }
            }
    }

	// Use this for initialization
	void Start () {
        CurrStage = Stage.Start;
        StartTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        /*if (!windspeedchanged && (Time.time > StartTime + SecondsBeforeSpread) && !CarLeft)
        {
            TC.ChangeWindSpeed(1);
            windspeedchanged = true;
        }*/
    }

    public void GoOutOfCar()
    {
        CarLeft = true;
        CurrStage = Stage.InTunnel;
    }

    public void GoInsideCar()
    {
        CarLeft = false;
        CurrStage = Stage.InCar;
    }

    public void EndGame()
    {
        if (SOSRequested)
            endgameroutine = StartCoroutine(EndGameRoutine(0));
        else
            endgameroutine = StartCoroutine(EndGameRoutine(40));
    }

    public void LoseGame()
    {
        endgameroutine = StartCoroutine(EndGameRoutine(0));
    }

    IEnumerator EndGameRoutine(float time)
    {
        yield return new WaitForSeconds(time);
        EndTime = Time.time;
        SteamVRFade.fadeOut(2);
        yield return new WaitForSeconds(2);
        /*if (!CarLeft)
            {
            if (!ArrowsOn)
                ErrorCount++;
            if (!EngineOff)
                ErrorCount++;
            ErrorCount++;
            }
        else if (!ShelterReached && SomethingBurning)
            {
            if (!ArrowsOn)
                ErrorCount++;
            if (!EngineOff)
                ErrorCount++;
            if (ExtinguisherUsed)
                BonusCount++;
            if (SOSRequested)
                BonusCount++;
            if (AlarmTriggered)
                BonusCount++;
            ErrorCount++;
            }
        else
            {
            if (!ArrowsOn)
                ErrorCount++;
            if (!EngineOff)
                ErrorCount++;
            if (ExtinguisherUsed)
                BonusCount++;
            if (!SomethingBurning || ShelterReached)
                BonusCount++;
            if (SOSRequested)
                BonusCount++;
            if (AlarmTriggered)
                BonusCount++;
            }*/
    /*if(BenchReached)
        CurrStage = Stage.End;*/
    if(GameManager.Instance != null)
        GameManager.Instance.Result = new GameResult(this);
    TC.EndGame();
    }

    void GoInsideShelter()
    {
        CurrStage = Stage.End;
    }

    void GoOutsideShelter()
    {
        CurrStage = Stage.InTunnel;
    }
    /*public char CalculateRank()
    {
        if(ErrorCount > 1)
            {
            if ((ErrorCount - BonusCount) < MaxErrors / 2)
                return 'E';
            else
                return 'F';
            }
        else
            {
            float rank = (MaxErrors - ErrorCount + BonusCount) / MaxErrors + BonusCount;
            if (rank > 0.8f)
                return 'S';
            else if (rank > 0.6f)
                return 'A';
            else if (rank > 0.4f)
                return 'B';
            else if (rank > 0.2f)
                return 'C';
            else
                return 'D';
            }
    }*/

    public void Reset()
        {
        CarLeft = false;
        ExtinguisherUsed = false;
        CurrStage = Stage.Start;
        /*ErrorCount = 0;
        BonusCount = 0;*/
        ArrowsOn = false;
        EngineOff = false;
        SOSRequested = false;
        AlarmTriggered = false;
        ShelterReached = false;
        SomethingBurning = false;
        DistanceMaintained = true;
        firestarted = false;
        CamionistaSaved = false;
        //windspeedchanged = false;
        }
}
