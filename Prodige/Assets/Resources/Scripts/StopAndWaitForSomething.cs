﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAndWaitForSomething : MonoBehaviour {

    bool stopped = false;
    public bool Condition = false;
    public delegate void MyDelegate(AdvancedStateMachineBehaviour a);
    public MyDelegate DoSomething;
    public AdvancedStateMachineBehaviour behaviour;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(stopped)
        {
        if(Condition)
            {
            if (DoSomething != null)
                DoSomething.Invoke(behaviour);
            gameObject.SetActive(false);
            }
        }
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NPC")
        {
            var a = other.GetComponentInParent<CamionistaController>();
            if (a != null)
            {
                a.Stop();
                stopped = true;
            }
        }
    }
}
