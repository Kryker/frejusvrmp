﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEffectController : MonoBehaviour
{

    public List<Transform> FireEffects;
    public List<Transform> SmokeEffects;
    List<ParticleSystem> ParticleEffects;
    Transform FireLight;
    public bool UnlinkedFire = false;
    [HideInInspector]
    public bool initialized = false;

    // Use this for initialization

    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        if (!initialized)
        {
            ParticleEffects = new List<ParticleSystem>();
            FireLight = transform.Find("Fire light");
            if (FireLight != null)
            {
                FireLight.GetComponent<UltraReal.flicker>().enabled = false;
                FireLight.GetComponent<Light>().intensity = 0;
            }
            var p = GetComponent<ParticleSystem>();
            if (p != null)
                ParticleEffects.Add(p);
            if (!UnlinkedFire)
            {
                for (int i = 0; i < FireEffects.Count; i++)
                {
                    p = FireEffects[i].GetComponent<ParticleSystem>();
                    var m = p.main;
                    m.playOnAwake = false;
                    m.duration = ParticleEffects[0].main.duration;
                    m.loop = false;
                    var ext = p.externalForces;
                    ext.enabled = true;
                    ext.multiplier = 0.1f;
                    //vecchia gestione del fuoco
                    /*
                    var coll = p.collision;
                    coll.enabled = true;
                    coll.mode = ParticleSystemCollisionMode.Collision3D;
                    coll.type = ParticleSystemCollisionType.World;
                    coll.sendCollisionMessages = true;
                    coll.radiusScale = 0.5f;
                    var f = FireEffects[i].GetComponent<Fire>();
                    if (f == null)
                    {
                        FireEffects[i].gameObject.AddComponent<Fire>();
                        f = FireEffects[i].GetComponent<Fire>();
                    }
                    f.damagepp = 2;
                    f.force = 0;
                    */
                    ParticleEffects.Add(p);
                }
                for (int i = 0; i < SmokeEffects.Count; i++)
                {
                    p = SmokeEffects[i].GetComponent<ParticleSystem>();
                    var m = p.main;
                    m.loop = false;
                    m.playOnAwake = false;
                    m.duration = ParticleEffects[0].main.duration;
                    var ext = p.externalForces;
                    ext.enabled = true;
                    ext.multiplier = 1;
                    ParticleEffects.Add(p);
                }
            }
            initialized = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Pause()
    {
        if (ParticleEffects == null)
            Initialize();
        foreach (ParticleSystem p in ParticleEffects)
            p.Pause();

        if (FireLight != null)
            FireLight.GetComponent<UltraReal.flicker>().enabled = false;
    }

    public void Play()
    {
        if (ParticleEffects == null)
            Initialize();
        for (int i = 1; i < ParticleEffects.Count; i++)
        {
            DisableEmission(ParticleEffects[i]);
            ParticleEffects[i].Play();
            EnableEmission(ParticleEffects[i]);
        }

        if (FireLight != null)
            FireLight.GetComponent<UltraReal.flicker>().enabled = true;
    }

    public void DisableEmission(ParticleSystem p)
    {
        var e = p.emission;
        e.enabled = false;
    }

    public void EnableEmission(ParticleSystem p)
    {
        var e = p.emission;
        e.enabled = true;
    }

    public void DisableEmission()
    {
        if (ParticleEffects == null)
            Initialize();
        for (int i = 1; i < ParticleEffects.Count; i++)
            DisableEmission(ParticleEffects[i]);

        if (FireLight != null)
            FireLight.GetComponent<UltraReal.flicker>().enabled = false;
    }

    public void EnableEmission()
    {
        if (ParticleEffects == null)
            Initialize();
        for (int i = 1; i < ParticleEffects.Count; i++)
            EnableEmission(ParticleEffects[i]);

        if (FireLight != null)
            FireLight.GetComponent<UltraReal.flicker>().enabled = true;
    }

    public void Stop()
    {
        if (ParticleEffects == null)
            Initialize();
        foreach (ParticleSystem p in ParticleEffects)
            p.Stop();
        //ParticleEffects[0].Stop();

        if (FireLight != null)
        {
            FireLight.GetComponent<UltraReal.flicker>().enabled = false;
            FireLight.GetComponent<Light>().intensity = 0;
        }
    }

    public void EnableLoop()
    {
        if (ParticleEffects == null)
            Initialize();
        foreach (ParticleSystem p in ParticleEffects)
        {
            var m = p.main;
            m.loop = true;
        }
    }

    public void SetDuration(float d)
    {
        if (ParticleEffects == null)
            Initialize();
        foreach (ParticleSystem p in ParticleEffects)
        {
            var m = p.main;
            m.duration = d;
        }
    }

    public void DisableLoop()
    {
        if (ParticleEffects == null)
            Initialize();
        foreach (ParticleSystem p in ParticleEffects)
        {
            var m = p.main;
            m.loop = false;
        }
    }
}
