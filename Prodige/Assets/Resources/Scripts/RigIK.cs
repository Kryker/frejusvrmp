﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigIK : MonoBehaviour {
    
    public Transform LeftHandTarget, RightHandTarget, Head;
    public Transform ForcedLeftHandTarget, ForcedRightHandTarget;
    public Animator animator;
    public bool Driving = false;
    public bool IKEnabled = true;
    
	// Use this for initialization
	void Start () {
	}


    // Update is called once per frame
    bool debugOnce = false;
	void LateUpdate () {

        try
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.parent.GetComponent<PlayerSpawnerNet>().FPSCamera.transform.localEulerAngles.y, transform.eulerAngles.z);

        }
        catch (Exception) { }
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (IKEnabled)
        {
            if (ForcedLeftHandTarget != null)
            {
                animator.SetIKPosition(AvatarIKGoal.LeftHand, ForcedLeftHandTarget.position);
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            }
            else if (LeftHandTarget != null)
            {
                animator.SetIKPosition(AvatarIKGoal.LeftHand, LeftHandTarget.position);
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            }
            if (ForcedRightHandTarget != null)
            {
                animator.SetIKPosition(AvatarIKGoal.RightHand, ForcedRightHandTarget.position);
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            }
            else  if (RightHandTarget != null)
            {
                animator.SetIKPosition(AvatarIKGoal.RightHand, RightHandTarget.position);
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            }
            if (Head != null)
            {
                animator.SetLookAtPosition(new Ray(Head.position, Head.forward).GetPoint(0.5f));
                animator.SetLookAtWeight(1);
            }
        } 
    }
}
