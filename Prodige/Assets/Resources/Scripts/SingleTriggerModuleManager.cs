﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class SingleTriggerModuleManager : MonoBehaviour {

    TriggerModulesManager TMM;
    public ParticleSystem.TriggerModule Trigger;
	// Use this for initialization
	void Start ()
    {
        Trigger = transform.GetComponent<ParticleSystem>().trigger;
        Trigger.enabled = true;
        Trigger.enter = ParticleSystemOverlapAction.Callback;
        Trigger.inside = ParticleSystemOverlapAction.Ignore;
        var tc = GameObject.FindGameObjectWithTag("TunnelController").transform;
        TMM = tc.GetComponent<TriggerModulesManager>();
        if(enabled)
            TMM.AddTrigger(this);
    }
    
    void OneEnable()
    {
        TMM.AddTrigger(this);
    }

    private void OnDisable()
    {
        TMM.RemoveTrigger(this);
    }

    private void OnDestroy()
    {
        TMM.RemoveTrigger(this);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
