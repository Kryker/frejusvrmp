﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour
    {
    void Start()
        {
        Cursor.lockState = CursorLockMode.Locked;
        }

    void Update()
        {
        if (Input.GetKeyDown("escape"))
            {
            Cursor.lockState = CursorLockMode.None;
            }
        if (Input.GetMouseButtonDown(0))
            {
            Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }