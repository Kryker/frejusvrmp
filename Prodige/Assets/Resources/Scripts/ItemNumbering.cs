﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class ItemNumbering : MonoBehaviour {

    public List<GenericItemNet> GenericItems;
    [HideInInspector]
    public Dictionary<uint,GenericItemNet> Items;
    [HideInInspector]
    public uint nextid;

    void Start()
    {

       /* if (isServer)
        { 
            nextid = 0;
            Items = new Dictionary<uint, GenericItemNet>();
            foreach (var g in GenericItems)
            {
                var item = g.GetComponent<GenericItemNet>();
                g.ItemID = nextid;
                Items[nextid] = g;
                nextid++;
            }
        }*/
    }
	
    public GenericItemNet GetItemByItemID(uint id)
    {
        try
        {
            return Items[id];
        }
        catch (KeyNotFoundException)
        {
            return null;
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
