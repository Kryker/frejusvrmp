﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbedExtinguisher : GrabbableItem
{
    MeshRenderer PieceMeshRenderer = null;
    Material outlinedbutton = null;
    Material oldmat = null;
    public Transform Extinguisher;
    bool up = true;
    float t = 0;
    bool PulseEnabled = false;
    AdvancedStateMachineBehaviour buttonpressed, buttonunpressed;
    bool firing = false;
    public SicuraExt sicuraExt;
    Extinguisher _ext;
    public Animator animator;
    public Transform Button;
    public Shader Outline;
    public GenericItemSlave Sicura;
    public ControllerButton cbutton = ControllerButton.Trigger;
    public ParticleSystem extEmit;
    public float fuelduration = 30.0f;
    public AudioClip SecuredButtonUp, SecuredButtonDown;
    public AudioSource ExtinguisherSource;
    public AudioSource ButtonSource;
    [HideInInspector]
    public VibrationController vibr;
    SecureState _secured = SecureState.Unknown;
    bool securesetup = false;

    public override void SetUp(ItemController i, ControllerHand hand)
    {
        base.SetUp(i, hand);
        if (hand == ControllerHand.LeftHand)
            vibr = i.LeftController.GetComponent<VibrationController>();
        else if (hand == ControllerHand.RightHand)
            vibr = i.RightController.GetComponent<VibrationController>();
    }

    [HideInInspector]
    public SecureState Secured
    {
        get { return _secured; }
        set
        {
            if (!securesetup)
            {
                if (value == SecureState.Open)
                {
                    sicuraExt.Hide();
                    sicuraExt.RimuoviSicura();
                    StartPulse();
                }
                else if (value == SecureState.Closed)
                {
                    StopPulse();
                    sicuraExt.MettiSicura();
                    if (Player != null && Player.Type == ItemControllerType.FPS && Slave.gameObject.activeSelf)
                    {
                        ((FirstPersonItemController)Player).forcedtarget = /*Slave.transform.GetChild(0);*/ sicuraExt.Slave.transform;      //test passamano
                    }
                    sicuraExt.Show();
                }
                securesetup = true;
            }
            else
            {
                if (value == SecureState.Open)
                {
                    sicuraExt.PlaySound();
                    sicuraExt.RimuoviSicura();
                    if (Player != null && Player.Type == ItemControllerType.FPS && Slave.gameObject.activeSelf)
                    {
                        ((FirstPersonItemController)Player).forcedtarget = null;
                    }
                    StartPulse();
                }
                else if (value == SecureState.Closed)
                {
                    StopPulse();
                    sicuraExt.MettiSicura();
                    if (Player != null && Player.Type == ItemControllerType.FPS && Slave.gameObject.activeSelf)
                    {
                        ((FirstPersonItemController)Player).forcedtarget = /*Slave.transform.GetChild(0);*/ sicuraExt.Slave.transform;      //test passamano
                    }
                }
            }
            _secured = value;
        }
    }


    [HideInInspector]
    public Extinguisher Ext
    {
        get { return _ext; }
        set
        {
            if (value != null)
            {
                StopPulse();
                base.LoadState(value);
                GetBehaviours();
                Secured = value.Secured;
                value.OnBase = false;
            }
            _ext = value;
        }
    }

    public override void LoadState(GenericItem i)
    {
        var e = i as Extinguisher;
        Ext = e;
        //base.LoadState(Ext);      //dentro la property Ext
    }

    private void GetBehaviours()
    {
        if (animator == null)
            animator = Slave.transform.GetComponentInChildren<Animator>();
        var advancedbehaviours = animator.GetBehaviours<AdvancedStateMachineBehaviour>();
        if (advancedbehaviours != null)
        {
            foreach (AdvancedStateMachineBehaviour a in advancedbehaviours)
            {
                if (a.StateName == "Pressed")
                {
                    buttonpressed = a;
                }
                else if (a.StateName == "UnPressed")
                {
                    buttonunpressed = a;
                }
            }
            buttonpressed.StateEnter += ParticleEnabled;
            buttonunpressed.StateEnter += ParticleDisabled;
        }
    }

    public override void SaveState()
    {
        if (animator.GetBool("Pressed"))
            animator.SetBool("Pressed", false);
        firing = false;
        if (vibr != null)
            vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
        Ext.Secured = Secured;
        Secured = SecureState.Unknown;
        Ext = null;
        securesetup = false;
        StopPulse();
        if (Player != null && Player.Type == ItemControllerType.FPS)
        {
            ((FirstPersonItemController)Player).forcedtarget = null;
        }
        sicuraExt.StopPulse();
        sicuraExt.Reset();
        base.SaveState();
        /*if (gameObject.activeSelf)
            StartCoroutine(PlayAndDisable());*/
    }

    /*private IEnumerator PlayAndDisable()
    {
        if (source != null)
        {
            source.Play();
            yield return new WaitWhile(() => source.isPlaying);
        }
        base.SaveState();
    }*/

    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }

    internal void ParticleDisabled(AdvancedStateMachineBehaviour a)
    {
        StopFire();
    }

    internal void ParticleEnabled(AdvancedStateMachineBehaviour a)
    {
        StartFire();
    }
    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        if (Ext != null)
        {
            if (Secured == SecureState.Open && !animator.GetBool("Pressed"))
            {
                animator.SetBool("Pressed", true);
                StopPulse();
            }
            else
                ButtonSource.Play();
        }
    }


    public override void Initialize()
    {
        if (setupdone && !initialized)
        {
            var i = Player.GetComponent<InputManagement>();
            if(i == null)
            {
                return;
            }
            if (Player.Type == ItemControllerType.VR)
            {
                var ic = controller.GetComponent<ControllerManager>();
                if (ic != null)
                {
                    if (ic.Hand == ControllerHand.LeftHand)
                    {
                        i.OnLeftPadPressed += ClickButton;
                        i.OnLeftPadUnpressed += UnClickButton;
                    }
                    else if (ic.Hand == ControllerHand.RightHand)
                    {
                        i.OnRightPadPressed += ClickButton;
                        i.OnRightPadUnpressed += UnClickButton;
                    }
                }
            }
            else
            {
                var c1 = Player;
                if (c1 != null)
                {
                    if (controller == c1.LeftController)
                    {
                        i.OnLeftMouseClicked += ClickButton;
                        i.OnLeftMouseUnclicked += UnClickButton;
                    }
                    else if (controller == c1.RightController)
                    {
                        i.OnRightMouseClicked += ClickButton;
                        i.OnRightMouseUnclicked += UnClickButton;
                    }
                }
            }
            base.Initialize();
        }
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        if (Ext != null && animator.GetBool("Pressed"))
        {
            animator.SetBool("Pressed", false);
            StartPulse();
        }
    }

    // Update is called once per frame
    public override void Update()
    {
        if (initialized)
        {
            Pulse();
            if (Ext != null && !Ext.IsKinematic && (Ext.transform.parent != null && Ext.transform.parent != Ext.InitialParent))
            {
                Ext.currvelocity = (Slave.transform.position - Ext.prevpos) / Time.deltaTime;
                Ext.prevpos = Slave.transform.position;
            }
            if (firing)
            {
                var remainingtime = fuelduration * Ext.Fuel / 100;
                remainingtime = remainingtime - Time.deltaTime;
                if (remainingtime <= 0)
                {
                    Ext.Fuel = 0;
                }
                else
                    Ext.Fuel = remainingtime / fuelduration * 100;
                if (Ext.Fuel <= 0)
                {
                    Ext.Fuel = 0.0f;
                    CancelInvoke("Openfire");
                    if (vibr != null)
                        vibr.StopVibration(0.05f, 0.65f, 0.04f, this);

                    extEmit.Stop();
                    ExtinguisherSource.Stop();
                    firing = false;
                }
            }
        }
    }

    public void StartPulse()
    {
        if (!PulseEnabled)
        {
            PieceMeshRenderer = Button.GetComponent<MeshRenderer>();
            if (oldmat == null)
                oldmat = PieceMeshRenderer.materials[0];
            if (outlinedbutton == null)
            {
                outlinedbutton = new Material(Outline);
                outlinedbutton.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                outlinedbutton.SetColor("_OutlineColor", oldmat.GetColor("_OutlineColor"));
                outlinedbutton.SetFloat("_Outline", oldmat.GetFloat("_Outline"));
                //var x = outlinedbutton.GetFloat("_Outline");
            }
            var m = new Material[1];
            m[0] = outlinedbutton;
            PieceMeshRenderer.materials = m;
            PulseEnabled = true;
        }
    }
    public void StopPulse()
    {
        var m = new Material[1];
        m[0] = oldmat;

        if (PieceMeshRenderer != null)
            PieceMeshRenderer.materials = m;
        PulseEnabled = false;
    }

    void Pulse()
    {
        if (PulseEnabled)
        {

            if (up)
            {
                t += Time.deltaTime / 0.5f;
                var m1 = PieceMeshRenderer.materials[0];
                var c = m1.GetColor("_OutlineColor");
                m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                var m = new Material[1];
                m[0] = m1;
                PieceMeshRenderer.materials = m;
                if (t >= 1)
                {
                    up = false;
                    t = 0;
                }
            }
            else
            {
                t += Time.deltaTime / 0.5f;
                var m1 = PieceMeshRenderer.materials[0];
                var c = m1.GetColor("_OutlineColor");
                m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                var m = new Material[1];
                m[0] = m1;
                PieceMeshRenderer.materials = m;
                if (t >= 1)
                {
                    up = true;
                    t = 0;
                }
            }
        }
    }

    public void RimuoviSicura()
    {
        if (Ext != null)
            Secured = SecureState.Open;
    }

    /*internal void RimuoviSicuraHidden()
    {
        sicuraExt.Hide();
        sicuraExt.RimuoviSicura();
        StartPulse();
        if (Player != null && Player.Type == ItemControllerType.FPS)
        {
            ((FirstPersonItemController)Player).forcedtarget = null;
        }
    }

    internal void MettiSicura()
    {
        sicuraExt.MettiSicura();
        sicuraExt.StartPulse();
        var c = Slave.GetComponentInParent<FirstPersonItemController>();
        if (Player != null && Player.Type == ItemControllerType.FPS)
        {
            ((FirstPersonItemController)Player).forcedtarget = sicuraExt.Slave.transform;
        }
    }

    public  void OnDisable()
    {
        if (sicuraExt != null)
        {
            sicuraExt.enabled = false;
            sicuraExt.Reset();
        }
    }*/

    void Openfire()
    {
        extEmit.Play();
        ExtinguisherSource.Play();
        if (vibr != null)
            vibr.StartVibration(0.05f, 0.65f, 0.04f, this);
    }


    void StartFire()
    {
        if (Ext != null)
        {
            if (Slave.gameObject.activeSelf && Secured == SecureState.Open)
            {
                Invoke("Openfire", 0.1f);
                var ic = controller.GetComponent<ControllerManager>();
                if (ic != null)
                    ic.StopPulsePublic();
                firing = true;
            }
            else if (Slave.gameObject.activeSelf)
            {
                ButtonSource.Stop();
                ButtonSource.clip = SecuredButtonDown;
                ButtonSource.Play();
            }
        }

    }

    void StopFire()
    {
        if (Slave.gameObject.activeSelf && (Ext == null || Secured == SecureState.Open))
        {
            try
            {
                CancelInvoke("Openfire");
            }
            catch (Exception)
            { }
            extEmit.Stop();
            ExtinguisherSource.Stop();
            if (vibr != null)
                vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
            firing = false;
        }
        else if (Slave.gameObject.activeSelf)
        {
            ButtonSource.Stop();
            ButtonSource.clip = SecuredButtonUp;
            ButtonSource.Play();
        }
    }
}
