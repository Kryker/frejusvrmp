﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : GenericItem {

	// Use this for initialization
	public override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		
	}

    public override void Interact(ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            SceneManager.LoadScene("Tunnel");
        }
    }
}
