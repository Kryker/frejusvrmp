﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TelefonoSOS : GenericItem
{

    public PulsanteChiamataSOS pulsante;
    Vector3 StartPos;
    public BaseTel Base;
    Quaternion StartRot;
    Transform StartParent;
    public Transform CableEnd;
    public CableComponent CableStart;
    [HideInInspector]
    public bool OnBase = true;
    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.SOSTelephone;
        StartPos = transform.position;
        StartRot = transform.rotation;
        //StartParent = transform.parent;
    }

    internal void ReBase()
    {
        //transform.parent = StartParent;
        transform.position = StartPos;
        transform.rotation = StartRot;
        Drop();
        Base.DisableOutline();
    }

    public override void Start()
    {
        base.Start();
    }

    internal void Drop()
    {
        base.DropParent();
    }


    // Update is called once per frame
    public override void Update()
    {

    }

    public override void DropParent()
    {
        if (OnBase)
            ReBase();
        else
            Drop();
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
    }


    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override void Reset()
    {
        SetCanInteract(false);
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
    }
}
