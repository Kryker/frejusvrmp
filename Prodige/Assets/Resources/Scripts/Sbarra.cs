﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Sbarra : MonoBehaviour {

    Animator animator;
    bool opened = false;
    AdvancedStateMachineBehaviour Opened, Closed;
    bool operating = false;
    AudioSource audiosource;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        audiosource = GetComponent<AudioSource>();
        foreach (AdvancedStateMachineBehaviour a in animator.GetBehaviours<AdvancedStateMachineBehaviour>())
        {
            if (a.StateName == "Opened")
                Opened = a;
            else if (a.StateName == "Closed")
                Closed = a;
        }
        Opened.StateEnter += SbarraMoving;
        Opened.StatePlayed += SbarraOpened;
        Closed.StateEnter += SbarraMoving;
        Closed.StatePlayed += SbarraClosed;
	}

    private void SbarraOpened(AdvancedStateMachineBehaviour a)
    {
        operating = false;
        opened = true;
    }

    private void SbarraMoving(AdvancedStateMachineBehaviour a)
    {
        if (audiosource != null)
            audiosource.Play();
    }

    private void SbarraClosed(AdvancedStateMachineBehaviour a)
    {
        operating = false;
        opened = false;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Close()
    {
        if (opened && !operating)
        {
            //animator.SetBool("CloseSbarra", true);
            Debug.Log("Reminder: ho disattivato la sbarra chiusa");
        }
            
    }


    public void Open()
    {
        if(!opened && !operating)
            animator.SetBool("CloseSbarra", false);
    }
}
