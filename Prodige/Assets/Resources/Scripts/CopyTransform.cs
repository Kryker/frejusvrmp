﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyTransform : MonoBehaviour {

    public bool Local = false;
    public Transform Source;
    public Vector3 PositionOffset;
    public Vector3 RotationOffset;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        if (Local)
            { 
            transform.localRotation = Quaternion.Euler(Source.localEulerAngles + RotationOffset);
            transform.localPosition = Source.localPosition + PositionOffset;
            }
        else
            { 
            transform.rotation = Quaternion.Euler(Source.eulerAngles + RotationOffset);
            transform.position = Source.position + PositionOffset;
            }
    }
}
