﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioFadeScript : MonoBehaviour
{
    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }

    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime, float MinVolume)
    {
        float startVolume = audioSource.volume;
        if (MinVolume <= 0)
        {
            FadeOut(audioSource, FadeTime);
            yield break;
        }

        while (audioSource.volume > MinVolume)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        //audioSource.Stop();
        audioSource.volume = MinVolume;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        float startVolume = 0.2f;

        //audioSource.volume = 0;
        audioSource.Play();

        while (audioSource.volume < 1.0f)
        {
            audioSource.volume += startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.volume = 1f;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime, float MaxVolume)
    {
        float startVolume = 0.2f;

        if (MaxVolume >= 1)
        {
            FadeIn(audioSource, FadeTime);
            yield break;
        }

        //audioSource.volume = 0;
        audioSource.Play();

        while (audioSource.volume < MaxVolume)
        {
            audioSource.volume += startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.volume = MaxVolume;
    }
}