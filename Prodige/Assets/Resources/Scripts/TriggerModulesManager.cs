﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerModulesManager : MonoBehaviour {
    
    List<SingleTriggerModuleManager> Triggers;
    List<Collider> Actors;
    bool initialized = false;
    // Use this for initialization
    void Start()
    {
        DoStart();
    }

    void DoStart()
    {
        Triggers = new List<SingleTriggerModuleManager>();
        Actors = new List<Collider>();
        initialized = true;
    }
    public void AddActor(Collider c)
    {
        if (!initialized)
            DoStart();
        if (c == null)
            return;
        if(!Actors.Contains(c))
        {
            Actors.Add(c);
            UpdateActors();
        }
    }

    public void RemoveActor(Collider c)
    {
        if (c == null)
            return;
        if (Actors.Contains(c))
        {
            Actors.Remove(c);
            UpdateActors();
        }
    }

    public void RemoveTrigger(SingleTriggerModuleManager t) {
        if (t == null)
            return;
        if (Triggers.Contains(t))
            {
                Triggers.Remove(t);
            }
        }

    public void AddTrigger(SingleTriggerModuleManager t)
    {
        if (!initialized)
            DoStart();
        if (t == null)
            return;
        SetColliderList(t.Trigger);
        if (!Triggers.Contains(t))
            {
            Triggers.Add(t);
            }
    }

    public void UpdateActors()
    {
        foreach(SingleTriggerModuleManager t in Triggers)
        {
            SetColliderList(t.Trigger);
        }
    }

    public void SetColliderList(ParticleSystem.TriggerModule t)
    {
        for (int i = 0; i < Actors.Count; i++)
            if(t.GetCollider(i)!=Actors[i])
                t.SetCollider(i, Actors[i]);
        for (int i = Actors.Count; i< t.maxColliderCount; i++)
            t.SetCollider(i, null);

    }

        // Update is called once per frame
    void Update() {

    }
}
