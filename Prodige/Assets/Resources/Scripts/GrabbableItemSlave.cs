﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableItemSlave : MonoBehaviour {

    // Use this for initialization
    public GrabbableItem Master;
    public GrabbableItemNet MasterNet;
    [HideInInspector]
    public bool outlined = false;
    public List<Transform> Pieces;
    internal Vector3 _startingPos, _grabPos = Vector3.zero;
    internal Quaternion _startingRot, _grabRot = Quaternion.identity;
    private bool _seekParent = false;
    Transform _child;
    float _t = 0;
    float _v = 0;
    float _time = .02f;

    private void Awake()
    {
        if (transform.childCount > 0)
        {
            _child = transform.GetChild(0);
            _startingPos = _child.localPosition;
            _startingRot = _child.localRotation;
        }
    }
    void Start () {
		
	}
    public ItemCodes Code()
    {
        if (Master != null)
            return Master.Code;
        else
            return MasterNet.Code;
    }
    
    public bool CanDrop()
    {
        if (Master != null)
            return Master.CanDrop();
        else
            return MasterNet.CanDrop();
    }
    // Update is called once per frame
    void Update () {
		
	}

    private void LateUpdate()
    {
        if (_seekParent && _child != null)
        {
            var parentMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
            var targetpos = parentMatrix.MultiplyPoint3x4(_startingPos);
            var targetrot = /*(*/transform.rotation /** Quaternion.Inverse(startParentRotationQ))*/ * _startingRot;

            _child.position = Vector3.Lerp(_grabPos, targetpos, _t);
            _child.rotation = Quaternion.Lerp(_grabRot, targetrot, _t);
            _t += Time.deltaTime * _v;
            if (_t >= 1)
            {
                _child.localPosition = _startingPos;
                _child.localRotation = _startingRot;
                _seekParent = false;
            }
        }
    }

    public virtual void EnableOutline(ItemController c)
    {
        Material[] mts = null;
       
        var m = GetComponent<MeshRenderer>();
        if (m != null)
            mts = m.materials;

        Color col = Color.yellow;
        col.a = 255;
        if (mts != null)
        {
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }

        }
        foreach (Transform t in Pieces)
        {
            var mat = t.GetComponent<MeshRenderer>();
            if (mat != null)
                mts = mat.materials;
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }
        }
        outlined = true;
    }

    public void MoveToAndGetBack(Vector3 pos, Quaternion rot)
    {
        if (_child != null)
        {
            var startPos = _child.position;
            _child.position = _grabPos = pos;
            _child.rotation = _grabRot = rot;
            _v = Vector3.Distance(pos, startPos) / _time;
            _seekParent = true;
            _t = 0;
        }
    }

    public virtual void DisableOutline(ItemController c)
    {
        Material[] mts = null;
       
        var m = GetComponent<MeshRenderer>();
        if (m != null)
            mts = m.materials;

        Color col = Color.yellow;
        col.a = 0;
        if (mts != null)
        {
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }
        }
        foreach (Transform t in Pieces)
        {
            var mat = t.GetComponent<MeshRenderer>();
            if (mat != null)
                mts = mat.materials;
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }
        }
        outlined = false;
    }
    public virtual void EnableOutline(ItemControllerNet c)
    {
        Material[] mts = null;

        var m = GetComponent<MeshRenderer>();
        if (m != null)
            mts = m.materials;

        Color col = Color.yellow;
        col.a = 255;
        if (mts != null)
        {
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }

        }
        foreach (Transform t in Pieces)
        {
            var mat = t.GetComponent<MeshRenderer>();
            if (mat != null)
                mts = mat.materials;
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }
        }
        outlined = true;
    }

    public virtual void DisableOutline(ItemControllerNet c)
    {
        Material[] mts = null;

        var m = GetComponent<MeshRenderer>();
        if (m != null)
            mts = m.materials;

        Color col = Color.yellow;
        col.a = 0;
        if (mts != null)
        {
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }
        }
        foreach (Transform t in Pieces)
        {
            var mat = t.GetComponent<MeshRenderer>();
            if (mat != null)
                mts = mat.materials;
            foreach (Material ms in mts)
            {
                ms.SetColor("_OutlineColor", col);
            }
        }
        outlined = false;
    }
}
