﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuIK : MonoBehaviour {
    
    public Transform headTarget;
    public Transform leftArmTarget;
    public Transform rightArmTarget;
    public Transform leftLegTarget;
    public Transform rightLegTarget;

}
