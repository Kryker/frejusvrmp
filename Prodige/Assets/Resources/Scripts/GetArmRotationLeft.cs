﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetArmRotationLeft : MonoBehaviour {

    public FootSwinger swing;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Quaternion left, right;
        swing.determineAverageArmControllerRotationPublic(out left, out right);
        transform.rotation = left;
    }
}
