﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Globalization;

public class Metri : MonoBehaviour
{

    Text txt;
    int num;
    public Color TextColor = Color.black;

    public void Start()
    {


        float pos = transform.parent.position.z;

        num = 6415 - Convert.ToInt32(pos);

        txt = GetComponentInChildren<Text>();
        var m = txt.material;
        var newm = new Material(m);
        newm.color = TextColor;
        txt.color = TextColor;
        txt.material = newm;
        var str = num.ToString("N0", new CultureInfo("en-US"));
        string newstr = "<size=\"250\">";
        int j = 0;
        if (num > 999)
        {
            while (str[j] != ',' && j < str.Length)
            {
                newstr += str[j];
                j++;
            }
            newstr += "</size><size=\"200\">";
        }
        for (int i = j; i < str.Length; i++)
            newstr += str[i];
        newstr += "</size>";
        txt.text = newstr;
        //txt.text = num.ToString("N0", new CultureInfo("is-IS"));

    }
}
