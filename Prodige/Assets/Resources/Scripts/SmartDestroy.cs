﻿using UnityEngine;
using System.Collections.Generic;

public class SmartDestroy : MonoBehaviour
    {
    //Script made by: Mick Boere

    public float maxPerSecond = 3;
    public List<GameObject> list;

void Start()
        {
        list = new List<GameObject>();
        InvokeRepeating("Destroy", 0, 1 / maxPerSecond);
        }

    void Add(GameObject obj)
        {
        list.Add(obj);
        obj.SetActive(false);
        }

    void Destroy()
        {
        if (list.Count != 0)
            {
            Destroy(list[list.Count - 1]);
            list.Remove(list[list.Count - 1]);
            }
        }
    }