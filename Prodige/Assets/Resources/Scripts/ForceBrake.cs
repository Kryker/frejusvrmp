﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceBrake : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        {
            var c = other.transform.parent.GetComponentInParent<CarMovementScript>();
            if (c != null)
            {
                c.ForceBrake();
                gameObject.SetActive(false);
            }
        }
    }
}
