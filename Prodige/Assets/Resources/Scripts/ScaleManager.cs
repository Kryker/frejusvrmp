﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleManager : MonoBehaviour
{

    public List<float> ModelHeights;
    public float DebugHeight = 1.77f;
    public List<Transform> Rigs;

    // Use this for initialization
    void Start()
    {
        var Height = DebugHeight;
        if (GameManager.Instance != null)
        {

            for (int i = 0; i < Rigs.Count; i++)
            {
                var scale = GameManager.Instance.PlayerHeight / ModelHeights[i];
                Rigs[i].localScale = new Vector3(scale, scale, scale);
                var leftarm = Rigs[i].GetComponent<ModelCharacteristics>().LeftShoulder.GetChild(0);
                var rightarm = Rigs[i].GetComponent<ModelCharacteristics>().RightShoulder.GetChild(0);
                leftarm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
                rightarm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
            }
        }
        else
        {
            if (DebugHeight == 0)
                DebugHeight = UnityEngine.VR.InputTracking.GetLocalPosition(UnityEngine.VR.VRNode.CenterEye).y;
            for (int i = 0; i < Rigs.Count; i++)
            {
                var scale = DebugHeight / ModelHeights[i];
                Rigs[i].localScale = new Vector3(scale, scale, scale);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
