﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Panchina : GenericItem {
    
    public EntrataRifugio Rifugio;
    StageController SC;
    MeshRenderer MR;
    Material oldmat;
    Transform Destination;
    public Transform Panca;
    MeshCollider destcoll;
    public Shader Outline;
    [HideInInspector]
    public bool PulseEnabled = false;
    Material outlinedmaterial;
    float t;
    bool up = false;

    // Use this for initialization
    public override void Start () {
        ItemCode = ItemCodes.Bench;
        destcoll = GetComponent<MeshCollider>();
        Destination = transform.Find("Destination");
        if (Rifugio == null)
            Rifugio = transform.parent.GetComponent<EntrataRifugio>();
        base.Start();
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if(tc != null)
            SC = tc.GetComponent<StageController>();
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
        /*if (CanInteract())
        {
            SC.BenchReached = true;
            SC.EndGame();
        }*/
    }

    public override void SetCanInteract(bool can, ItemController c)
    {   if (can)
            EnableDest();
        else if (MR != null)
            DisableDest();
        base.SetCanInteract(can, c);
    }

    private void DisableDest()
    {
        destcoll.enabled = false;
        Destination.gameObject.SetActive(false);
    }

    private void EnableDest()
    {
        destcoll.enabled = true;
        Destination.gameObject.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
                SC.EndGame();
        }
    }

    // Update is called once per frame
    public override void Update()
    {
        Pulse();
    }

    public void StartPulse()
    {
        if (PulseEnabled)
            StopPulse();

        if (!PulseEnabled)
        {
            MR = Panca.GetComponent<MeshRenderer>();
            if (oldmat == null)
                oldmat = MR.materials[0];
            if (outlinedmaterial == null)
            {
                outlinedmaterial = new Material(Outline);
                outlinedmaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                outlinedmaterial.SetColor("_OutlineColor", oldmat.GetColor("_OutlineColor"));
                outlinedmaterial.SetFloat("_Outline", oldmat.GetFloat("_Outline"));
            }
            var m = new Material[1];
            m[0] = outlinedmaterial;
            MR.materials = m;
            PulseEnabled = true;
        }
    }
    public void StopPulse()
    {
        var m = new Material[1];
        m[0] = oldmat;

        MR.materials = m;
        PulseEnabled = false;
    }

    void Pulse()
    {
        if (PulseEnabled)
        {

            if (up)
            {
                t += Time.deltaTime / 0.5f;
                var m1 = MR.materials[0];
                var c = m1.GetColor("_OutlineColor");
                m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                var m = new Material[1];
                m[0] = m1;
                MR.materials = m;
                if (t >= 1)
                {
                    up = false;
                    t = 0;
                }
            }
            else
            {
                t += Time.deltaTime / 0.5f;
                var m1 = MR.materials[0];
                var c = m1.GetColor("_OutlineColor");
                m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                var m = new Material[1];
                m[0] = m1;
                MR.materials = m;
                if (t >= 1)
                {
                    up = true;
                    t = 0;
                }
            }
        }
    }

    public override void EnableOutline(ItemController c)
    {
        StopPulse();
        base.EnableOutline(c);
    }

    public override void DisableOutline(ItemController c)
    {
        StartPulse();
        base.DisableOutline(c);
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override void ClickButton(object sender, ClickedEventArgs e)
    { }
}
