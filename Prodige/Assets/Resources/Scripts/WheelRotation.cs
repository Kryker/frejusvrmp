﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelRotation : MonoBehaviour {
    public Transform flWheel, frWheel, clWheel, crWheel, rlWheel, rrWheel, steeringwheel;
    public WheelCollider flWheelCollider, frWheelCollider, clWheelCollider, crWheelCollider, rlWheelCollider, rrWheelCollider;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
        {
        if (flWheel != null)
            flWheel.localEulerAngles = new Vector3(flWheel.localEulerAngles.x, flWheel.localEulerAngles.y, flWheel.localEulerAngles.z);
        if (frWheel != null)
            frWheel.localEulerAngles = new Vector3(frWheel.localEulerAngles.x, frWheel.localEulerAngles.y, frWheel.localEulerAngles.z);

        if (flWheel != null)
            flWheel.Rotate(- flWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        if (frWheel != null)
            frWheel.Rotate(- frWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        if(clWheel!=null)
            clWheel.Rotate(- clWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        if (crWheel != null)
            crWheel.Rotate(- crWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        if (rlWheel != null)
            rlWheel.Rotate(- rlWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);
        if (rrWheel != null)
            rrWheel.Rotate(- rrWheelCollider.rpm / 60 * 360 * Time.deltaTime, 0, 0);

        }
    }
