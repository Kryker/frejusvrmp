﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawn : MonoBehaviour {

    public List<Transform> VehiclePrefabs;
    public List<Transform> SpawnedVehicles;
    public float MinTime = 10.0f, MaxTime = 30.0f;
    float nextcar = 0;
    public bool stop = false;
	// Use this for initialization
	void Start () {
        nextcar = 0;
    }

    public bool RemoveVehicle(Transform transform)
    {
       if(SpawnedVehicles.Contains(transform))
        {
            SpawnedVehicles.Remove(transform);
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update ()
    {
        if (!stop)
        {
            var t = Time.time;
            if (t >= nextcar)
            {
                nextcar = (int)t + UnityEngine.Random.Range(MinTime, MaxTime);
                SpawnVehicle();
            }
        }
    }

    private void SpawnVehicle()
    {
        int index = UnityEngine.Random.Range(0, VehiclePrefabs.Count);
        //var pos = VehiclePrefabs[index].position + transform.position;
        var t = Instantiate(VehiclePrefabs[index]);
        t.position = new Vector3(t.position.x, t.position.y, transform.position.z);
        SpawnedVehicles.Add(t);
    }
    public void Stop()
    {
        stop = true;
    }

    public void StartSpawn()
    {
        stop = false;
        foreach (Transform t in SpawnedVehicles)
            t.GetComponent<NPCCarMovementScript>().CanMove = true;
    }
}
