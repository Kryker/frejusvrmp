﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAvailable : MonoBehaviour
{
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.parent != null)
        {
            var c = other.transform.parent.GetComponentInParent<CarMovementScript>();
            if (c != null)
            {
                c.SetCanStop(true);
                gameObject.SetActive(false);
            }
        }
    }
}
