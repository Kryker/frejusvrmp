﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum TipoManiglia { Interna, Esterna };
public class ManigliaPorta : GenericItem
{
    public ManigliaPorta LinkedHandle;
    public ManigliaPorta OppositeHandle;
    AudioSource audiosource;
    //AudioClip clip;
    TunnelController TC;
    public VehicleSeat.Seat seat;
    VehicleData vehicle;
    public TipoManiglia Tipo;

    // Use this for initialization
    void Awake()
    {
        ItemCode = ItemCodes.CarHandle;
        if (Slave != null)
            audiosource = Slave.GetComponent<AudioSource>();
        else
            audiosource = transform.GetComponent<AudioSource>();
        vehicle = GetComponent<VehicleData>();
    }

    // Update is called once per frame
    public override void Update()
    {

    }

    public override void Start()
    {
        TC = GameObject.FindGameObjectWithTag("TunnelController").GetComponent<TunnelController>();
        base.Start();
    }

    void OpenHandle()
    {
        //CanInteract(false, null);
        /*if (OppositeHandle != null)
            OppositeHandle.CanInteract(false);*/
        audiosource.Play();
        if (seat == VehicleSeat.Seat.Left)
            TC.ManageCarHandleInteraction(vehicle.SeatedSpawnLeft);
        else
            TC.ManageCarHandleInteraction(vehicle.SeatedSpawnRight);
        /*if (LinkedHandle != null)
            LinkedHandle.CanInteract(true);
        if (OppositeHandle != null)
            if (OppositeHandle.LinkedHandle != null)
                OppositeHandle.LinkedHandle.CanInteract(true);*/
    }


    public override void ClickButton(object sender, ClickedEventArgs e)
    { }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override bool CanInteract(ItemController c)
    {
        if (base.CanInteract(c))
        {
            var p = c.GetComponent<PlayerStatus>();
            if (seat == VehicleSeat.Seat.Left)
            {
                if (Tipo == TipoManiglia.Esterna)
                {
                    if (vehicle.SeatedSpawnLeft.Occupant == null)
                        return true;
                }
                else
                {
                    if (vehicle.SeatedSpawnLeft.Occupant == p.transform)
                        return true;
                }
            }
            else
            {
                if (Tipo == TipoManiglia.Esterna)
                {
                    if (vehicle.SeatedSpawnRight.Occupant == null)
                        return true;
                }
                else
                {
                    if (vehicle.SeatedSpawnRight.Occupant == p.transform)
                        return true;
                }
            }
        }
        return false;
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
            OpenHandle();
    }

    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        if (base.CanInteract(c))
        {
            var p = c.GetComponent<PlayerStatus>();
            if (seat == VehicleSeat.Seat.Left)
            {
                if (Tipo == TipoManiglia.Esterna)
                {
                    if (vehicle.SeatedSpawnLeft.Occupant == null)
                        OpenHandle();
                }
                else
                {
                    if (vehicle.SeatedSpawnLeft.Occupant == p.transform)
                        OpenHandle();
                }
            }
            else
            {
                if (Tipo == TipoManiglia.Esterna)
                {
                    if (vehicle.SeatedSpawnRight.Occupant == null)
                        OpenHandle();
                }
                else
                {
                    if (vehicle.SeatedSpawnRight.Occupant == p.transform)
                        OpenHandle();
                }
            }
        }
    }
    public override void Reset()
    {
        base.Reset();
    }
}
