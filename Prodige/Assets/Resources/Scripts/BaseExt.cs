﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseExt : MonoBehaviour
{
    
    //bool outlined = false;
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
            var g = ItemController.GetGrabbableItemSlave(other.transform);

            if (g != null && g.Code() == ItemCodes.Extinguisher)
            {
                if (g.Master != null)
                {
                    var e = (GrabbedExtinguisher)g.Master;
                    e.Ext.Base = this;
                    e.Ext.OnBase = true;
                    EnableOutline();
                }
#if MULTIPLAYER
            else if (g.MasterNet != null)
                {
                    var en = (GrabbedExtinguisherNet)g.MasterNet;
                    en.Ext.Base = this;
                    en.Ext.OnBase = true;
                    EnableOutline();
                }
#endif
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var g = ItemController.GetGrabbableItemSlave(other.transform);

        if (g != null && g.Code() == ItemCodes.Extinguisher)
            {
                if (g.Master != null)
                {
                    var e = (GrabbedExtinguisher)g.Master;
                    if (!e.Ext.OnBase || e.Ext.Base != this)
                    {
                        e.Ext.Base = this;
                        e.Ext.OnBase = true;
                        EnableOutline();
                    }
            }
#if MULTIPLAYER
            else if (g.MasterNet != null)
                {
                    var en = (GrabbedExtinguisherNet)g.MasterNet;
                    if (!en.Ext.OnBase || en.Ext.Base != this)
                    {
                        en.Ext.Base = this;
                        en.Ext.OnBase = true;
                        EnableOutline();
                    }
            }
#endif
        }
    }
    private void OnTriggerExit(Collider other)
    {
        var g = ItemController.GetGrabbableItemSlave(other.transform);

        if (g != null && g.Code() == ItemCodes.Extinguisher)
            {
                if (g.Master != null)
                {
                    var e = (GrabbedExtinguisher)g.Master;
                    e.Ext.Base = null;
                    e.Ext.OnBase = false;
                    DisableOutline();
            }
#if MULTIPLAYER
            else if (g.MasterNet != null)
                {
                    var en = (GrabbedExtinguisherNet)g.MasterNet;
                    en.Ext.Base = null;
                    en.Ext.OnBase = false;
                    DisableOutline();

            }
#endif
        }
    }


    public virtual void EnableOutline()
    {
        var mts = GetComponent<MeshRenderer>().materials;
        Color c;
        foreach (Material m in mts)
        {
            c = m.GetColor("_OutlineColor");
            c.a = 255;
            m.SetColor("_OutlineColor", c);
        }
        //outlined = true;
    }

    public virtual void DisableOutline()
    {
        var mts = GetComponent<MeshRenderer>().materials;
        Color c;
        foreach (Material m in mts)
        {
            c = m.GetColor("_OutlineColor");
            c.a = 0;
            m.SetColor("_OutlineColor", c);
        }
        //outlined = false;
    }

}
