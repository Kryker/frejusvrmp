﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ciao : MonoBehaviour {
    Animator animator;
    public EntrataRifugio entrata;
    bool salutato = false;
    AdvancedStateMachineBehaviour state;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        if(entrata != null)
            entrata.Porta.PortaClose.StatePercentagePlayed += Wave;

        state = animator.GetBehaviour<AdvancedStateMachineBehaviour>();
        state.StatePlayed += StopWaving;
	}

    private void StopWaving(AdvancedStateMachineBehaviour a)
    {
        if (animator.GetBool("Wave"))
            animator.SetBool("Wave", false);
    }

    private void Wave(AdvancedStateMachineBehaviour a)
    {
        if (!salutato && entrata != null && entrata.Indoor)
            {
                animator.SetBool("Wave", true);
                salutato = true;
                if(entrata != null)
                    entrata.Porta.PortaClose.StatePercentagePlayed -= Wave;
            }
        }
    
}
