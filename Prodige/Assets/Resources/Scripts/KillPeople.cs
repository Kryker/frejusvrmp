﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPeople : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        var p = other.GetComponent<PlayerStatus>();
        if(p!=null)
        {
            p.Die();
        }
    }
}
