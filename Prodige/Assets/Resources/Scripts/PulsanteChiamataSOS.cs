﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulsanteChiamataSOS : GenericItem
{

    //public Vector3 ButtonPositionAction;
    public Animator animator;
    //Vector3 ButtonPositionRest;
    public List<AudioClip> clips;
    public bool ResponseAvailable = true;
    public AudioSource Speaker, Button;
    public Coroutine lastroutine;
    [HideInInspector]
    public float MaxDist;
    [HideInInspector]
    public float MinDist;
    [HideInInspector]
    public Vector3 StartSpeakerPos;
    //Transform StartParent;
    public bool InsideShelter = false;
    StageController SC;
    bool operating = false;
    AdvancedStateMachineBehaviour Pressed, Unpressed;
    public Posizione posizione;

    //TunnelController TC;
    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.SOSCall;
        //ButtonPositionRest = transform.localPosition;
    }

    public override void Start()
    {
        if (ResponseAvailable)
        {
            StartSpeakerPos = Speaker.transform.localPosition;
            MaxDist = Speaker.maxDistance;
            MinDist = Speaker.minDistance;
        }
        base.Start();
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
        {
            //TC = tc.GetComponent<TunnelController>();
            SC = tc.GetComponent<StageController>();
        }

        var advancedbehaviours = animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            if (ad.StateName == "Pressed")
            {
                Pressed = ad;
            }
            else if (ad.StateName == "Unpressed")
            {
                Unpressed = ad;
            }
        }
        Pressed.StateEnter += ButtonPressed;
        Pressed.StatePlayed += ButtonRelease;
        Unpressed.StateEnter += ButtonReleased;
        Unpressed.StatePlayed += StopOperating;
        //StartParent = transform.parent;
    }

    public void ButtonPressed(AdvancedStateMachineBehaviour st)
    {
        Button.clip = clips[0];
        Button.Play();
    }
    public void StopOperating(AdvancedStateMachineBehaviour st)
    {
        operating = false;
    }
    public void ButtonRelease(AdvancedStateMachineBehaviour st)
    {
        UnClickButton();
    }

    public void ButtonReleased(AdvancedStateMachineBehaviour st)
    {
        Button.clip = clips[1];
        Button.Play();
    }

    // Update is called once per frame
    public override void Update()
    {
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        ClickButton();
    }

    void ClickButton()
    {
        animator.SetBool("PressButton", true);
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        UnClickButton();
    }
    public override void ClickButton(object sender)
    {
        ClickButton();
    }

    public override void UnClickButton(object sender)
    {
        UnClickButton();
    }
    void UnClickButton()
    {
        animator.SetBool("PressButton", false);
    }

    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
            Call(false);
    }

    void Call (bool forced)
    {
        if (!operating)
        {
            operating = true;
            if (!ItemActive && SC != null)
            {
                ItemActive = true;
                if (ResponseAvailable)
                {
                    if (!SC.SOSRequested)
                    {
                        SC.AlarmTriggered = true;
                        if (InsideShelter)
                            SC.StopEndRoutine();
                        lastroutine = StartCoroutine(AudioMessaggeRequest());
                    }
                    if(!forced)
                        ClickButton();
                }
                else
                {
                    if (!SC.AlarmTriggered)
                    {
                        SC.AlarmTriggered = true;
                    }
                    if (!forced)
                        ClickButton();
                }
            }
            else if (!ItemActive && SC == null)
            {
                ItemActive = true;
                if (ResponseAvailable)
                    lastroutine = StartCoroutine(AudioMessaggeRequest());
                if (!forced)
                    ClickButton();
            }
            else if (!forced)
                ClickButton();
        }
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
            Call(false);
    }

    public void ForceCall()
    {
        Call(true);
    }


    private IEnumerator AudioMessaggeRequest()
    {
        /*if(!InsideShelter)
            TC.ChangeJingleVolume(0.3f);*/
        operating = true;
        /*ClickButton();
        audiosource.clip = clips[0];
        audiosource.Play();
        yield return new WaitUntil(() => !audiosource.isPlaying);
        UnClickButton();
        audiosource.clip = clips[1];
        audiosource.Play();*/
        yield return new WaitUntil(() => !Speaker.isPlaying);
        Speaker.clip = clips[2];
        Speaker.Play();
        yield return new WaitUntil(() => !Speaker.isPlaying);
        Speaker.clip = clips[3];
        Speaker.Play();
        yield return new WaitUntil(() => !Speaker.isPlaying);
        if (SC != null)
            SC.SOSRequested = true;
        operating = false;
        /* if(!InsideShelter)
         TC.ChangeJingleVolume(1);*/
    }

    public override void Reset()
    {
        if (ResponseAvailable)
            Speaker.Stop();
        ItemActive = false;
        if (SC != null)
        {
            SC.AlarmTriggered = false;
            SC.SOSRequested = false;
        }
    }

}
