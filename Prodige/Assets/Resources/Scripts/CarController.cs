﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
#if !MULTIPLAYER
    CarMovementScript Movement;
#else
    CarMovementScriptNet Movement;
#endif

    bool braking = false;
#if MULTIPLAYER
    bool initialized = false;
#endif
    // Use this for initialization
    private void Awake()
    {
    #if !MULTIPLAYER
        Movement = GetComponent<CarMovementScript>();
    #else
        Movement = GetComponent<CarMovementScriptNet>();
    #endif
    }
    private void Start()
    {
#if MULTIPLAYER
        Initialize();
#else
        Movement.RequestAccelerate();
#endif
    }
#if MULTIPLAYER
    void Initialize()
    {
        if (!initialized)
        {
            if(Movement.hasAuthority)
                Movement.RequestAccelerate();
            initialized = true;
        }
    }

#endif
    // Update is called once per frame
    void Update()
    {
#if MULTIPLAYER
        if (initialized && Movement.hasAuthority)
        {
#endif
            braking = IsBraking();
            if (braking)
                Movement.RequestBrake();
            else
            {
                if (Movement.IsBrakeRequested())
                    Movement.RequestStopBrake();
                if (!Movement.IsAcclerateRequested())
                    Movement.RequestAccelerate();
            }

            if (Movement.stopped || !Movement.CanMove)
                return;

            if ((Movement.IsBrakeRequested() && Movement.CanBrake) || Movement.forcebrake) //se sto frenando e posso frenare, o sono costretto a frenare
            {
                if (Movement.WheelColFR != null)
                {
                    Movement.WheelColFR.motorTorque = 0;
                }

                if (Movement.WheelColFL != null)
                {
                    Movement.WheelColFL.motorTorque = 0;
                }

                if (Movement.WheelColRR != null)
                {
                    Movement.WheelColRR.motorTorque = 0;
                    Movement.WheelColRR.brakeTorque = Movement.BrakeForce;
                }

                if (Movement.WheelColRL != null)
                {
                    Movement.WheelColRL.motorTorque = 0;
                    Movement.WheelColRL.brakeTorque = Movement.BrakeForce;
                }

            }
            else if (Movement.IsAcclerateRequested() && (!Movement.IsBrakeRequested() || !Movement.CanBrake) && Movement.body.velocity.magnitude * 3.6 <= Movement.MaxSpeed)      //se non sto frenando o non posso frenare e non sono troppo veloce
            {
                if (Movement.WheelColFR != null && (Movement.Type == EngineType.Front || Movement.Type == EngineType.Both))
                    Movement.WheelColFR.motorTorque = Movement.MotorForce;
                else if (Movement.WheelColFR != null)
                    Movement.WheelColFR.motorTorque = 0;

                if (Movement.WheelColFL != null && (Movement.Type == EngineType.Front || Movement.Type == EngineType.Both))
                    Movement.WheelColFL.motorTorque = Movement.MotorForce;
                else if (Movement.WheelColFL != null)
                    Movement.WheelColFL.motorTorque = 0;

                if (Movement.WheelColRR != null && (Movement.Type == EngineType.Rear || Movement.Type == EngineType.Both))
                {
                    Movement.WheelColRR.motorTorque = Movement.MotorForce;
                    Movement.WheelColRR.brakeTorque = 0;
                }
                else if (Movement.WheelColRR != null)
                {
                    Movement.WheelColRR.motorTorque = 0;
                    Movement.WheelColRR.brakeTorque = 0;
                }

                if (Movement.WheelColRR != null && (Movement.Type == EngineType.Rear || Movement.Type == EngineType.Both))
                {
                    Movement.WheelColRL.motorTorque = Movement.MotorForce;
                    Movement.WheelColRL.brakeTorque = 0;
                }
                else if (Movement.WheelColRL != null)
                {
                    Movement.WheelColRL.motorTorque = 0;
                    Movement.WheelColRL.brakeTorque = 0;
                }
            }
            else //if((IsBraking() && body.velocity.magnitude == 0) || (!IsBraking() && body.velocity.magnitude >= MaxSpeed))       //altrimenti
            {
                if (Movement.WheelColFR != null)
                    Movement.WheelColFR.motorTorque = 0;

                if (Movement.WheelColFL != null)
                    Movement.WheelColFL.motorTorque = 0;

                if (Movement.WheelColRR != null && (Movement.Type == EngineType.Rear || Movement.Type == EngineType.Both))
                {
                    Movement.WheelColRR.motorTorque = 0;
                    Movement.WheelColRR.brakeTorque = 0;
                }

                if (Movement.WheelColRL != null && (Movement.Type == EngineType.Rear || Movement.Type == EngineType.Both))
                {
                    Movement.WheelColRL.motorTorque = 0;
                    Movement.WheelColRL.brakeTorque = 0;
                }
            }
#if MULTIPLAYER
        }
#endif
    }

    bool IsBraking()
    {
        if (GameManager.Instance != null)
        {
            if (GameManager.Instance.TC != null)
            {
                var pc = GameManager.Instance.TC.GetPlayerController();
                if (pc != null)
                {
                    var im = pc.GetComponent<InputManagement>();
                    if (GameManager.Instance.Platform == ControllerType.MouseAndKeyboard || !UnityEngine.VR.VRDevice.isPresent)
                    {
                        if (im.IsLeftMouseClicked || im.IsRightMouseClicked)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        if (im.IsLeftGripped || im.IsRightGripped ||
                         im.IsLeftPadPressed || im.IsRightPadPressed ||
                         im.IsLeftTriggerClicked || im.IsRightTriggerClicked)
                            return true;
                        else
                            return false;
                    }
                }
            }
#if MULTIPLAYER
            else
            {
                if (GameManager.Instance.TCN != null)
                {
                    var pc = GameManager.Instance.TCN.GetPlayerController();
                    if (pc != null)
                    {
                        var im = pc.GetComponent<InputManagementNet>();
                        if (GameManager.Instance.Platform == ControllerType.MouseAndKeyboard || !UnityEngine.VR.VRDevice.isPresent)
                        {
                            if (im.IsLeftMouseClicked || im.IsRightMouseClicked)
                                return true;
                            else
                                return false;
                        }
                        else
                        {
                            if (im.IsLeftGripped || im.IsRightGripped ||
                               im.IsLeftPadPressed || im.IsRightPadPressed ||
                               im.IsLeftTriggerClicked || im.IsRightTriggerClicked)
                                return true;
                            else
                                return false;
                        }
                    }
                }
            }
#endif
        }
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        TunnelController TC = null;
        if (tc != null)
            TC = tc.GetComponent<TunnelController>();
        if (TC != null)
        {
            var pc = TC.GetPlayerController();
            if (pc != null)
            {
                var im = pc.GetComponent<InputManagement>();
                if (TC.Platform == ControllerType.MouseAndKeyboard || !UnityEngine.VR.VRDevice.isPresent)
                {
                    if (im.IsLeftMouseClicked || im.IsRightMouseClicked)
                        return true;
                    else
                        return false;
                }
                else
                {
                    if (im.IsLeftGripped || im.IsRightGripped ||
                                      im.IsLeftPadPressed || im.IsRightPadPressed ||
                                      im.IsLeftTriggerClicked || im.IsRightTriggerClicked)
                        return true;
                    else
                        return false;
                }

            }
        }
        return false;
    }

    private void OnDisable()
    {
#if !MULTIPLAYER
        Movement.RequestStopAccelerate();
        Movement.RequestStopBrake();
#else
        if (Movement.hasAuthority)
        {
            Movement.RequestStopAccelerate();
            Movement.RequestStopBrake();
        }
        Movement.RequestStopAccelerate();
        Movement.RequestStopBrake();
#endif

    }
}
