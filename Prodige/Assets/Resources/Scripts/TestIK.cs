﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestIK : MonoBehaviour {
    
    public Transform Rig;
    [SerializeField]
    Transform LeftHand;
    [SerializeField]
    Transform RightHand;
    [SerializeField]
    Transform HookHead;
    public Transform HookLeft;
    public Transform HookRight;
    public bool HookEnabled = false;

    Animator animator;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}

    private void OnAnimatorIK(int layerIndex)
    {
        if (HookEnabled)
        {
            animator.SetIKPosition(AvatarIKGoal.RightHand, HookRight.position);
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            animator.SetIKPosition(AvatarIKGoal.LeftHand, HookLeft.position);
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
            var ray = new Ray(HookHead.transform.position, HookHead.transform.rotation * Vector3.forward);
            Debug.DrawRay(HookHead.transform.position, HookHead.transform.rotation * Vector3.forward);
            animator.SetLookAtPosition(ray.GetPoint(5));
            animator.SetLookAtWeight(1);
        }
    }
}
