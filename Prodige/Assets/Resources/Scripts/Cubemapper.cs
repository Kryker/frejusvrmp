﻿using UnityEngine;
using System.Collections;

public class Cubemapper : MonoBehaviour
{
    /*
	 * Creates a cubemap from a camera and feeds it to a material
	 */


    public Camera sourceCamera;
    public int cubeMapRes = 128;
    public int cubeMapSize = 128;
    public bool createMipMaps = false;
    public LayerMask layerMask;
    RenderTexture renderTex;



    public RenderTexture GetRenderTexture()
    {
        RenderTexture oldtex= null;
        if (renderTex != null) //return renderTex;
            oldtex = renderTex;
        renderTex = new RenderTexture(cubeMapRes, cubeMapSize, 16);
        renderTex.isPowerOfTwo = true;
        renderTex.dimension = UnityEngine.Rendering.TextureDimension.Cube;
        renderTex.hideFlags = HideFlags.HideAndDontSave;
        renderTex.autoGenerateMips = createMipMaps;
        sourceCamera.RenderToCubemap(renderTex, layerMask);
        if (oldtex != null)
        {
            oldtex.DiscardContents();
            oldtex.Release();
            oldtex = null;
        }
        return renderTex;
    }


}