﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public VRUICanvas PlatformSelection, GameResult, GameResultComponente, MainView, Players, Training, ExitTraining, NetworkManagerHUD, ClientConnectPage,
        NetworkDiscoveryPage, StartServerPage, AvatarTweaking, AvatarSelection, numeroPlayers, numeroPlayersServerOnly, AttesaSmoke;
    public VRToggleController Ita, Fra, Eng;
    public VRButtonController Prev, Next, Platform;
    public AudioMainMenu effect;
    public LocalizerController Localizer;
    public String TunnelScene = "Tunnel";
    public Transform Cassa;
    ControllerType FakeType = ControllerType.ArmSwing;
    string fileName = "LogFile.txt";
    [SerializeField] protected GameObject HTCViveController, FirstPersonController, ArmSwingController, FootSwingController, CVirtController, KatVRController, FreeWalkController;
    [SerializeField] protected Sprite[] PlatformIcons, PlatformSelectedIcons;
    protected GameObject CurrentController;
    bool debugsingleplayer;
    public VRSelectableObject Avatar1, Avatar2, Avatar3;
    [HideInInspector]
    public bool ForcedHeight = false;
    public float DebugHeight = 1.77f;
    public Transform FakeCameraRig, FakeHeadTarget, FakeLeftControllerTarget, FakeRightControllerTarget, FakeRightTrackerTarget, FakeLeftTrackerTarget;
    [HideInInspector]
    public Transform Armature, LeftArm, RightArm;
    public List<string> ModelNames;
    public ScaleTweaker Tweaker;
    public VRButtonController Host, Client, Server, Broadcast, Cancel, CancelBroad, StartBroad, StopSterver, StartBroad2, CancelBroad2, InitBroad, InitBroad2, Back;
    public VRInputFieldController Address;
    public Text Description, Description2, Description3, Description4;
    string olddesc, olddesc2, olddesc3, olddesc4;
    Vector3 InitialFPSPosition, InitialHTCVivePosition;

    // Use this for initialization
    bool ciSonoPunteggiCivile = false;
    bool ciSonoPunteggiVF = false;
    void Start()
    {

        if (GameManager.Instance.ResultNet != null)
            NetworkTransport.Shutdown();
        InitialFPSPosition = FirstPersonController.transform.position;
        InitialHTCVivePosition = HTCViveController.transform.position;
        olddesc = Description.text;
        olddesc2 = Description2.text;
        olddesc3 = Description3.text;
        olddesc4 = Description4.text;
        Ita.OnToggle += ToggleIta;
        Eng.OnToggle += ToggleEng;
        Fra.OnToggle += ToggleFra;
        Avatar1.OnObjectClicked += Avatar1Selected;
        Avatar2.OnObjectClicked += Avatar2Selected;
        SteamVRFade.fadeIn(0.35f);
        StartCoroutine(AudioFadeScript.FadeIn(effect.rifugio, 0.35f));
        StartCoroutine(AudioFadeScript.FadeIn(effect.macchina, 0.35f));

        if (GameManager.Instance != null)
        {
            if (!UnityEngine.VR.VRDevice.isPresent)
            {
                FirstPersonController.SetActive(true);
            }
            else
            {
                HTCViveController.SetActive(true);       
            }
            switch (GameManager.Instance.AppLanguage)
            {
                case SystemLanguage.Italian:
                    Ita.SetState(true);
                    break;
                case SystemLanguage.French:
                    Fra.SetState(true);
                    break;
                default:
                    Eng.SetState(true);
                    break;
            }
            if (GameManager.Instance.Result == null && GameManager.Instance.ResultNet == null)
            {
                MainView.EnableElements();
            }
            else
            {
                Localizer.SelectedLanguage = GameManager.Instance.AppLanguage;
                Localizer.ApplyLanguage();

                StageControllerNet.GameResult.Risultati results = new StageControllerNet.GameResult.Risultati();
                Text obj;

                if (GameManager.Instance.Result != null)
                {
                    results = GameManager.Instance.Result.risultati;

                }
                else if (GameManager.Instance.ResultNet != null)
                {
                    results = GameManager.Instance.ResultNet.risultati;
                }

                if (results.Ruolo != SpawnMode.Componente)
                {
                    if (results.Ruolo == SpawnMode.TruckDriver)
                    {
                        obj = GameResult.transform.Find("Estintore").GetComponent<Text>();
                        SetBool(results.EstintoreUsato, obj);
                        obj.enabled = true;
                        GameResult.transform.Find("Distance").GetComponent<Text>().enabled = false;
                        GameResult.transform.Find("Names").Find("Distance").GetComponent<Text>().enabled = false;
                    }
                    else
                    {
                        //nascondo questo campo
                        obj = GameResult.transform.Find("Distance").GetComponent<Text>();
                        SetBool(results.DistanceMaintained, obj);
                        obj.enabled = false;
                        GameResult.transform.Find("Estintore").GetComponent<Text>().enabled = false;
                        GameResult.transform.Find("Names").Find("Estintore").GetComponent<Text>().enabled = false;
                    }

                    obj = GameResult.transform.Find("Arrows").GetComponent<Text>();
                    SetBool(results.ArrowsOn, obj);

                    obj = GameResult.transform.Find("Engine").GetComponent<Text>();
                    setFloat(results.EngineOffTime, results.EngineOff, obj);


                    obj = GameResult.transform.Find("Alarm").GetComponent<Text>();
                    setFloat(results.AlarmTriggeredTime, results.AlarmTriggered, obj);

                    obj = GameResult.transform.Find("SOS").GetComponent<Text>();
                    setFloat(results.SOSRequestedTime, results.SOSRequested, obj);

                    obj = GameResult.transform.Find("Shelter").GetComponent<Text>();
                    setFloat(results.ShelterReachedTime, results.ShelterReached, obj);  //results.StageReached == Stage.End

                    GameResult.transform.Find("Time").GetComponent<Text>().text = "<b>" + FloatTimeToString(results.Time) + "</b>";
                    GameResult.EnableElements();
                    ciSonoPunteggiCivile = true;
                }
                else
                {
                    obj = GameResultComponente.transform.Find("PeopleSafety").GetComponent<Text>();
                    SetBool(results.PrioritaAlCivile, obj);

                    obj = GameResultComponente.transform.Find("Distanza").GetComponent<Text>();
                    SetBool(results.MantenuteDistanzeSicurezzaIcendio, obj);

                    //obj = GameResultComponente.transform.Find("BackToTruck").GetComponent<Text>();
                    //SetBool(results.RitornoAlCamion, obj);

                    obj = GameResultComponente.transform.Find("IncendioSpento").GetComponent<Text>();
                    //SetBool(results.IncendioSpentoEntro5Min, obj);
                    SetBool(results.AttaccoIncendioBasso, obj);
                    

                    obj = GameResultComponente.transform.Find("Cesoie").GetComponent<Text>();
                    SetBool(results.CivileSoccorso && results.TaglioCinturaOk, obj);

                    //procedura completata:
                    obj = GameResultComponente.transform.Find("Procedura").GetComponent<Text>();
                    SetBool(results.RitornoAlCamion &&
                        results.IncendioSpentoEntro5Min &&
                        results.CivileSoccorso, obj);

                    //results.TaglioCinturaOk;
                    //results.AttaccoIncendioBasso;

                    GameResultComponente.transform.Find("Time").GetComponent<Text>().text = "<b>" + FloatTimeToString(results.Time) + "</b>";

                    GameResultComponente.EnableElements();
                    ciSonoPunteggiVF = true;
                }

                ResultToFile();


            }
        }
        else
        {
            Eng.SetState(true);
            if (!UnityEngine.VR.VRDevice.isPresent)
                FirstPersonController.gameObject.SetActive(true);
            else
                HTCViveController.gameObject.SetActive(true);
            MainView.EnableElements();
        }

        GameManager.Instance.MaxPlayers = uint.MaxValue;
        GameManager.Instance.SinglePlayer = false;
        GameManager.Instance.SinglePlayerExtended = false;

    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.J))
        {
            GameManager.Instance.SinglePlayer = false;
            GameManager.Instance.SinglePlayerExtended = true;
            GameManager.Instance.PlayerHeight = DebugHeight;
            GameManager.Instance.PlayerModel = 1;
            GameManager.Instance.SmokeFile.debugSmoke = true;
            GameManager.Instance.SmokeFile.loadingDone = true;
            //GameManager.Instance.Server = true;
            GameManager.Instance.NetworkManager.matchSize = 1;
            GameManager.Instance.NetworkManager.StartHost();

            GameManager.Instance.Platform = ControllerType.MouseAndKeyboard;
            if (UnityEngine.VR.VRDevice.isPresent)
            {
                GameManager.Instance.Platform = ControllerType.ArmSwing;
            }

        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameManager.Instance.SinglePlayer = false;
            GameManager.Instance.SinglePlayerExtended = true;
            GameManager.Instance.PlayerHeight = DebugHeight;
            GameManager.Instance.PlayerModel = 1;
            GameManager.Instance.SmokeFile.debugSmoke = true;
            GameManager.Instance.SmokeFile.loadingDone = true;
            //GameManager.Instance.Server = true;
            GameManager.Instance.NetworkManager.matchSize = 1;
            GameManager.Instance.NetworkManager.StartHost();

            GameManager.Instance.Platform = ControllerType.FreeWalk;
          

        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            GameManager.Instance.SinglePlayer = false;
            GameManager.Instance.SinglePlayerExtended = true;
            GameManager.Instance.PlayerHeight = DebugHeight;
            GameManager.Instance.PlayerModel = 1;
            GameManager.Instance.SmokeFile.debugSmoke = true;
            GameManager.Instance.SmokeFile.loadingDone = true;
            //GameManager.Instance.Server = true;
            GameManager.Instance.NetworkManager.matchSize = 2;
            GameManager.Instance.Platform = ControllerType.FreeWalk;
            GameManager.Instance.NetworkManager.StartHost();
            if (GameManager.Instance.NetworkDiscovery.Initialize())
                GameManager.Instance.NetworkDiscovery.StartBroadcasting();
           


        }
        /*
        if (ciSonoPunteggiCivile && Input.GetKeyDown(KeyCode.P))
        {
            ciSonoPunteggiCivile = false;
            GameResult.EnableElements();
        }
        if (ciSonoPunteggiVF && Input.GetKeyDown(KeyCode.P))
        {
            ciSonoPunteggiVF = false;
            GameResultComponente.EnableElements();
        }*/
    }


    private void SetBool(bool valore, Text destinazione)
    {
        if (valore)
        {
            destinazione.text = "<i>✓</i>   ";
            destinazione.color = Color.green;
        }
        else
        {
            destinazione.text = "<i>X</i>   ";
            destinazione.color = Color.red;
        }
    }

    private void setFloat(float valore, bool condizione, Text destinazione)
    {
        if (condizione)
        {
            destinazione.text = FloatTimeToString(valore);
            destinazione.color = Color.green;
        }
        else
        {
            destinazione.text = "<i>X</i>   ";
            destinazione.color = Color.red;
        }
    }
    private void Avatar1Selected(object c)
    {
        if (GameManager.Instance != null)
        {
            GetHeight();
            GameManager.Instance.PlayerModel = 1;
        }
        AvatarSelection.DisableElements();
        AvatarSelected(1);
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            LoadMirroredAvatar(1);
            AvatarTweaking.EnableElements();
            Tweaker.enabled = true;
        }
        else
            LoadPlatformSelection();
    }

    public void MirroredAvatarOK()
    {
        Destroy(Armature.gameObject);
        Tweaker.enabled = false;
        AvatarTweaking.DisableElements();
        GameManager.Instance.Platform = ControllerType.ArmSwing;
        PlatformSelection.EnableElements();
    }

    private void LoadMirroredAvatar(int index)
    {
        var m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[index - 1]);

        Armature = (Instantiate(m, FakeCameraRig) as GameObject).transform;
        LeftArm = Armature.GetComponent<ModelCharacteristics>().LeftShoulder.GetChild(0);
        RightArm = Armature.GetComponent<ModelCharacteristics>().RightShoulder.GetChild(0);
        var scale = GameManager.Instance.PlayerHeight / Armature.GetComponent<ModelCharacteristics>().Height;
        Armature.localScale = new Vector3(scale, scale, scale);
        LeftArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
        RightArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
        var vrik = Armature.GetComponent<RootMotion.FinalIK.VRIK>();
        vrik.solver.spine.headTarget = FakeHeadTarget;
        vrik.solver.leftArm.target = FakeLeftControllerTarget;
        vrik.solver.rightArm.target = FakeRightControllerTarget;
        vrik.solver.leftLeg.target = FakeLeftTrackerTarget;
        vrik.solver.rightLeg.target = FakeRightTrackerTarget;
        vrik.enabled = true;
        Armature.gameObject.SetActive(true);
        Armature.GetComponent<ModelCharacteristics>().HealthBar.parent.parent.gameObject.SetActive(false);
    }

    private void LoadAvatar()
    {
            if (UnityEngine.VR.VRDevice.isPresent)
            {
            var m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[GameManager.Instance.PlayerModel - 1]);
            Armature = (Instantiate(m, CurrentController.transform.Find("[CameraRig]")) as GameObject).transform;
            Armature.name = "Armature";
            LeftArm = Armature.GetComponent<ModelCharacteristics>().LeftShoulder.GetChild(0);
            RightArm = Armature.GetComponent<ModelCharacteristics>().RightShoulder.GetChild(0);
            var scale = GameManager.Instance.PlayerHeight / Armature.GetComponent<ModelCharacteristics>().Height;
            Armature.localScale = new Vector3(scale, scale, scale);
            LeftArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
            RightArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
            var vrik = Armature.GetComponent<RootMotion.FinalIK.VRIK>();
            CurrentController.GetComponent<RootMotion.Demos.VRIKPlatform>().ik = vrik;
            vrik.solver.spine.headTarget = CurrentController.GetComponent<MainMenuIK>().headTarget;
            vrik.solver.leftArm.target = CurrentController.GetComponent<MainMenuIK>().leftArmTarget;
            vrik.solver.rightArm.target = CurrentController.GetComponent<MainMenuIK>().rightArmTarget;
            vrik.solver.leftLeg.target = CurrentController.GetComponent<MainMenuIK>().leftLegTarget;
            vrik.solver.rightLeg.target = CurrentController.GetComponent<MainMenuIK>().rightLegTarget;
            vrik.enabled = true;
            CurrentController.GetComponent<Run>().SetAnimator(Armature.GetComponent<Animator>());
            CurrentController.GetComponent<Run>().IK = vrik;
            CurrentController.GetComponent<Run>().LeftUpLeg = Armature.GetComponent<ModelCharacteristics>().LeftUpLeg;
            CurrentController.GetComponent<Run>().RightUpLeg = Armature.GetComponent<ModelCharacteristics>().RightUpLeg;
            CurrentController.GetComponent<Run>().LeftShoulder = Armature.GetComponent<ModelCharacteristics>().LeftShoulder;
            CurrentController.GetComponent<Run>().RightShoulder = Armature.GetComponent<ModelCharacteristics>().RightShoulder;
            CurrentController.GetComponent<Run>().enabled = true;
            Armature.gameObject.SetActive(true);
            Armature.GetComponent<ModelCharacteristics>().HealthBar.parent.parent.gameObject.SetActive(false);

            for (int i = 0; i < Armature.childCount; i++)
                if (Armature.GetChild(i).GetComponent<SkinnedMeshRenderer>() != null)
                    Armature.GetChild(i).GetComponent<SkinnedMeshRenderer>().enabled = false;
        }
            else
            {
            var m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\FPS\\" + ModelNames[GameManager.Instance.PlayerModel - 1]);
            Armature = (Instantiate(m, CurrentController.transform) as GameObject).transform;
            Armature.name = "Armature";
            var rigik = Armature.GetComponent<RigIK>();
            CurrentController.GetComponent<Run>().SetAnimator(Armature.GetComponent<Animator>());
            CurrentController.GetComponent<Run>().FPSIK = rigik;
            CurrentController.GetComponent<Run>().enabled = true;
            Armature.gameObject.SetActive(true);
            Armature.GetComponent<ModelCharacteristics>().HealthBar.parent.parent.gameObject.SetActive(false);

            for (int i = 0; i < Armature.childCount; i++)
                if (Armature.GetChild(i).GetComponent<SkinnedMeshRenderer>() != null)
                    Armature.GetChild(i).GetComponent<SkinnedMeshRenderer>().enabled = false;
      
        }
       
    }

    private void Avatar2Selected(object c)
    {
        if (GameManager.Instance != null)
        {
            GetHeight();
            GameManager.Instance.PlayerModel = 2;
        }
        AvatarSelection.DisableElements();
        AvatarSelected(2);
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            LoadMirroredAvatar(2);
            AvatarTweaking.EnableElements();
            Tweaker.enabled = true;
        }
        else
            LoadPlatformSelection();
    }

    

    public void ClickBroadcasting()
    {
        PreAvvio(0, false, true);

    }

    public void ClickBack()
    {
        if (GameManager.Instance.NetworkDiscovery.broadcasting || Description2.text == "initialized")
            GameManager.Instance.NetworkDiscovery.StopBroadcasting();
        Description2.text = olddesc2;
        NetworkDiscoveryPage.DisableElements();
        AbilitaNetworkHUD();
    }

    private void AbilitaNetworkHUD()
    {
        AttesaSmoke.DisableElements();
        NetworkManagerHUD.EnableElements();
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            NetworkManagerHUD.transform.Find("Server").gameObject.SetActive(false);
        }

        
    }

    public void ClickListenForBroadcasting()
    {
        if (GameManager.Instance.NetworkDiscovery.StartListening())
        {
            StartBroad.gameObject.SetActive(false);
            Description2.text = "initialized (client)";
            CancelBroad.enabled = true;
            CancelBroad.gameObject.SetActive(true);
        }
    }

    private bool ServerOnly = false;
    public void ClickServer()
    {
        ServerOnly = true;
        NetworkManagerHUD.DisableElements();
        numeroPlayersServerOnly.EnableElements();
        AttesaSmoke.DisableElements();
    }



    public void ClickStopServer()
    {
        GameManager.Instance.NetworkDiscovery.StopBroadcasting();
        GameManager.Instance.Server = false;
        GameManager.Instance.NetworkManager.StopServer();
        Description3.text = olddesc3;
        StartServerPage.DisableElements();
        AbilitaNetworkHUD();
        AttesaSmoke.DisableElements();
    }

    public void ClickClient()
    {
        PreAvvio(0, false, false, true);
    }

    public void ClickCancelBroadcasting()
    {
        GameManager.Instance.NetworkDiscovery.StopBroadcasting();
        Description4.text = olddesc4;
        InitBroad2.enabled = true;
        InitBroad2.gameObject.SetActive(true);
        CancelBroad2.gameObject.SetActive(false);
    }

    public void ClickStartBroadcasting()
    {
        StartBroad2.gameObject.SetActive(false);
        Description4.text = "initialized (server)";
        CancelBroad2.enabled = true;
        CancelBroad2.gameObject.SetActive(true);
        GameManager.Instance.NetworkDiscovery.StartBroadcasting();
        AttesaSmoke.DisableElements();
    }

    public void ClickInitBroad()
    {
        if (GameManager.Instance.NetworkDiscovery.Initialize())
        {
            Description2.text = "initialized";
            InitBroad.gameObject.SetActive(false);
            StartBroad.enabled = true;
            StartBroad.gameObject.SetActive(true);
            AttesaSmoke.DisableElements();
        }
    }

    public void ClickInitBroad2()
    {
        if (GameManager.Instance.NetworkDiscovery.Initialize())
        {
            Description4.text = "initialized";
            InitBroad2.gameObject.SetActive(false);
            StartBroad2.enabled = true;
            StartBroad2.gameObject.SetActive(true);
            AttesaSmoke.DisableElements();
        }
    }

    public void Connect()
    {
        Description.text = olddesc;
        ClientConnectPage.DisableElements();
       
    }

    public void Disconnect()
    {
        Description.text = olddesc;
        ClientConnectPage.DisableElements();
        AbilitaNetworkHUD();
        AttesaSmoke.DisableElements();
    }

    public void ClickCancel()
    {
        GameManager.Instance.NetworkManager.client.Shutdown();
        Description.text = olddesc;
        AbilitaNetworkHUD();
        ClientConnectPage.DisableElements();
        AttesaSmoke.DisableElements();
    }

    public void ClickStopListening()
    {
        GameManager.Instance.NetworkDiscovery.StopBroadcasting();
        Description2.text = olddesc2;
        InitBroad.enabled = true;
        InitBroad.gameObject.SetActive(true);
        CancelBroad.gameObject.SetActive(false);
        AttesaSmoke.DisableElements();
    }

    public void ChangeInputValue(String str)
    {
        GameManager.Instance.NetworkManager.networkAddress = str;
    }

    private bool hostOnly = false;
    public void ClickHost()
    {
        hostOnly = true;
        NetworkManagerHUD.DisableElements();
        numeroPlayers.EnableElements();
    }

    private void StartAsHost()
    {
        GameManager.Instance.NetworkManager.matchSize = GameManager.Instance.MaxPlayers;
        GameManager.Instance.NetworkManager.StartHost();
        if (GameManager.Instance.NetworkDiscovery.Initialize())
            GameManager.Instance.NetworkDiscovery.StartBroadcasting();

    }


    private void ResultToFile()
    {
        if (GameManager.Instance == null || (GameManager.Instance.Result == null && GameManager.Instance.ResultNet == null))
            return;
        if (File.Exists(fileName))
        {
            var sr = File.AppendText(fileName);
            if (GameManager.Instance.Result != null)
                sr.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + GameManager.Instance.Result.ToString());
            else if (GameManager.Instance.ResultNet != null)
                sr.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + GameManager.Instance.ResultNet.ToString());
            sr.Close();
        }
        else
        {
            var sr = File.CreateText(fileName);
            if (GameManager.Instance.Result != null)
                sr.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + GameManager.Instance.Result.ToString());
            else if (GameManager.Instance.ResultNet != null)
                sr.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + GameManager.Instance.ResultNet.ToString());
            sr.Close();
        }
        if (GameManager.Instance.Result != null)
            Debug.Log(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + GameManager.Instance.Result.ToString());
        else if (GameManager.Instance.ResultNet != null)
            Debug.Log(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " " + GameManager.Instance.ResultNet.ToString());
    }

    public static string FloatTimeToString(float time)
    {
        try
        {

            TimeSpan t = TimeSpan.FromSeconds(time);
            return ((int)t.TotalMinutes).ToString("D2") + ":" + (t.Seconds).ToString("D2"); //t.TotalSeconds.ToString();//
        }
        catch (OverflowException)
        { }
        return "OF";
    }

    private void ToggleIta()
    {
        if (Ita.State)
        {
            Localizer.SelectedLanguage = SystemLanguage.Italian;
        }
    }
    private void ToggleEng()
    {
        if (Ita.State)
        {
            Localizer.SelectedLanguage = SystemLanguage.English;
        }
    }

    private void ToggleFra()
    {
        if (Ita.State)
        {
            Localizer.SelectedLanguage = SystemLanguage.French;
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }


    public void StartTraining()
    {
        Training.DisableElements();
        if (!UnityEngine.VR.VRDevice.isPresent)
        {
            CurrentController.GetComponent<CharacterMotor>().enabled = true;
            CurrentController.GetComponent<FirstPersonMainView>().enabled = false;
            CurrentController.GetComponent<FirstPersonItemController>().enabled = true;
            CurrentController.transform.Find("Main Camera").Find("Controller (right)").GetComponent<SteamVR_LaserPointer>().active = false;
            CurrentController.transform.Find("Main Camera").Find("Controller (right)").Find("New Game Object").gameObject.SetActive(false);
        }
        else
        {
            var ic = CurrentController.GetComponent<VRItemController>();
            ic.enabled = true;
            ic.LeftController.GetComponent<SphereCollider>().enabled = true;
            ic.LeftController.GetComponent<ControllerManager>().enabled = true;
            ic.LeftController.GetComponent<AudioSource>().enabled = false;
            ic.RightController.GetComponent<RayCastForController>().enabled = false;
            ic.RightController.GetComponent<SphereCollider>().enabled = true;
            ic.RightController.GetComponent<ControllerManager>().enabled = true;

            if ((GameManager.Instance == null && FakeType == ControllerType.KatWalk) || (GameManager.Instance != null && GameManager.Instance.Platform == ControllerType.KatWalk))
            {
                CurrentController.transform.Find("CameraHolder").GetComponent<KATDevice>().multiply = 3;
                CurrentController.transform.Find("CameraHolder").GetComponent<KATDevice>().multiplyBack = .5f;
            }
            else if ((GameManager.Instance == null && FakeType == ControllerType.CVirtualizer) || (GameManager.Instance != null && GameManager.Instance.Platform == ControllerType.CVirtualizer))
                CurrentController.GetComponent<CVirtPlayerController>().movementSpeedMultiplier = 1;
            else if ((GameManager.Instance == null && FakeType == ControllerType.FreeWalk) || (GameManager.Instance != null && GameManager.Instance.Platform == ControllerType.FreeWalk))
                CurrentController.GetComponent<CharacterControllerVR>().Blocked = false;
            else
                CurrentController.transform.Find("[CameraRig]").GetComponent<OpenVRSwinger>().SwingEnabled = true;
        }
        ExitTraining.EnableElements();
        if (Cassa != null)
            Cassa.gameObject.SetActive(true);
    }

    public void LoadBackFromTraining()
    {
        Training.DisableElements();
        Destroy(Armature.gameObject);
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            CurrentController.gameObject.SetActive(false);
            HTCViveController.gameObject.SetActive(true);
        }
        CurrentController = null;
        PlatformSelection.EnableElements();
    }

    public void NextPlatform()
    {
        if (GameManager.Instance != null)
        {
            if (GameManager.Instance.Platform != ControllerType.FreeWalk)

            {
                GameManager.Instance.Platform++;
                Platform.GetComponent<Image>().sprite = PlatformIcons[((int)GameManager.Instance.Platform) - 1];
                SpriteState st = new SpriteState();
                st.highlightedSprite = PlatformSelectedIcons[((int)GameManager.Instance.Platform) - 1];
                Platform.GetComponent<Button>().spriteState = st;
                if (GameManager.Instance.Platform == ControllerType.FreeWalk)
                    Next.ElementNotActive();
                else if (!Prev.IsElementActive())
                    Prev.ElementActive();
            }
        }
        else
        {
            if (FakeType != ControllerType.FreeWalk)
            {
                FakeType++;
                Platform.GetComponent<Image>().sprite = PlatformIcons[((int)FakeType) - 1];
                SpriteState st = new SpriteState();
                st.highlightedSprite = PlatformSelectedIcons[((int)FakeType) - 1];
                Platform.GetComponent<Button>().spriteState = st;
                if (FakeType == ControllerType.FreeWalk)
                    Next.ElementNotActive();
                else if (!Prev.IsElementActive())
                    Prev.ElementActive();
            }
        }
    }
    public void PrevPlatform()
    {
        if (GameManager.Instance != null)
        {
            if (GameManager.Instance.Platform != ControllerType.ArmSwing)
            {
                GameManager.Instance.Platform--;
                Platform.GetComponent<Image>().sprite = PlatformIcons[((int)GameManager.Instance.Platform) - 1];
                SpriteState st = new SpriteState();
                st.highlightedSprite = PlatformSelectedIcons[((int)GameManager.Instance.Platform) - 1];
                Platform.GetComponent<Button>().spriteState = st;
                if (GameManager.Instance.Platform == ControllerType.ArmSwing)
                    Prev.ElementNotActive();
                else if (!Next.IsElementActive())
                    Next.ElementActive();
            }
        }
        else
        {
            if (FakeType != ControllerType.ArmSwing)
            {
                FakeType--;
                Platform.GetComponent<Image>().sprite = PlatformIcons[((int)FakeType) - 1];
                SpriteState st = new SpriteState();
                st.highlightedSprite = PlatformSelectedIcons[((int)FakeType) - 1];
                Platform.GetComponent<Button>().spriteState = st;
                if (FakeType == ControllerType.ArmSwing)
                    Prev.ElementNotActive();
                else if (!Next.IsElementActive())
                    Next.ElementActive();
            }
        }
    }

    public void PlatformSelected()
    {
        PlatformSelection.DisableElements();
        if (GameManager.Instance != null)
        {
            if (UnityEngine.VR.VRDevice.isPresent)
            {
                HTCViveController.SetActive(false);
                switch (GameManager.Instance.Platform)
                {
                    case ControllerType.ArmSwing:
                        CurrentController = ArmSwingController;
                        break;
                    case ControllerType.CVirtualizer:
                        CurrentController = CVirtController;
                        break;
                    case ControllerType.FootSwing:
                        CurrentController = FootSwingController;
                        break;
                    case ControllerType.KatWalk:
                        CurrentController = KatVRController;
                        break;
                    case ControllerType.FreeWalk:
                        CurrentController = FreeWalkController;
                        break;
                }
                CurrentController.gameObject.SetActive(true);
                //if (GameManager.Instance.DebugMode)
            }
            else
            {
                GameManager.Instance.Platform = ControllerType.MouseAndKeyboard;
                CurrentController = FirstPersonController;
            }
        }
        else
        {
            if (UnityEngine.VR.VRDevice.isPresent)
            {
                HTCViveController.SetActive(false);
                switch (FakeType)
                {
                    case ControllerType.ArmSwing:
                        CurrentController = ArmSwingController;
                        break;
                    case ControllerType.CVirtualizer:
                        CurrentController = CVirtController;
                        break;
                    case ControllerType.FootSwing:
                        CurrentController = FootSwingController;
                        break;
                    case ControllerType.KatWalk:
                        CurrentController = KatVRController;
                        break;
                    case ControllerType.FreeWalk:
                        CurrentController = FreeWalkController;
                        break;
                }
                CurrentController.gameObject.SetActive(true);
                //if (GameManager.Instance.DebugMode)
            }
            else
            {
                CurrentController = FirstPersonController;
            }
        }
        Training.EnableElements();
        LoadAvatar();
    }

    public void LoadPlatformSelection()
    {
        AvatarSelection.DisableElements();
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            GameManager.Instance.PlayerModel = -1;
            //GameManager.Instance.SinglePlayer = true;
            GameManager.Instance.SinglePlayer = false;
            GameManager.Instance.SinglePlayerExtended = true;
            if (GameManager.Instance != null)
                GameManager.Instance.Platform = ControllerType.ArmSwing;
            else
                FakeType = ControllerType.ArmSwing;
            PlatformSelection.EnableElements();
        }
        else
            {
            if (GameManager.Instance != null)
                GameManager.Instance.Platform = ControllerType.MouseAndKeyboard;
            else
                FakeType = ControllerType.MouseAndKeyboard;
            PlatformSelected();
        }
    }
    
    private void LoadAvatarSelection()
    {
        Players.DisableElements();
        AvatarSelection.EnableElements();
    }
    public void LoadAvatarSelectionSP()
    {
        if (GameManager.Instance != null)
            GameManager.Instance.SinglePlayerExtended = true;
        else
            debugsingleplayer = true;

        LoadAvatarSelection();
    }
    public void LoadAvatarSelectionMP()
    {
        if (GameManager.Instance != null)
            GameManager.Instance.SinglePlayerExtended = false;
        else
            debugsingleplayer = false;

        LoadAvatarSelection();
    }
    public void AvatarSelected(int value)
    {
        if (GameManager.Instance != null)
            GameManager.Instance.PlayerModel = value;
    }
 
    public void LoadBackAvatarSelection()
    {
        GameManager.Instance.SinglePlayerExtended = false;
        AvatarSelection.EnableElements();
    }


    public void GetHeight()
    {
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            if (ForcedHeight)
                GameManager.Instance.PlayerHeight = DebugHeight;
            else
                GameManager.Instance.PlayerHeight = UnityEngine.VR.InputTracking.GetLocalPosition(UnityEngine.VR.VRNode.CenterEye).y;
        }
    }

    public void LoadPlayerSelection()
    {
        if (!Localizer.Localized)
        {
            if (Ita.State)
                Localizer.SelectedLanguage = SystemLanguage.Italian;
            else if (Fra.State)
                Localizer.SelectedLanguage = SystemLanguage.French;
            else
                Localizer.SelectedLanguage = SystemLanguage.English;
            Localizer.ApplyLanguage();
        }

        if (MainView.ElementsEnabled)
            MainView.DisableElements();
        Players.EnableElements();
    }
    public void LoadBackMainView()
    {
        if (Players.ElementsEnabled)
        {
            Players.DisableElements();
        }
        else if (GameResult.ElementsEnabled)
        {
            GameResult.DisableElements();
        }
        else if (GameResultComponente.ElementsEnabled)
        {
            GameResultComponente.DisableElements();
        }
        MainView.EnableElements();
        Localizer.Reset();
    }

    public void LoadBackPlayerSelection()
    {
        AvatarSelection.DisableElements();
        Players.EnableElements();
    }

    public void LoadBackPlayerOrAvatarSelection()
    {
        PlatformSelection.DisableElements();
        if (GameManager.Instance != null && !GameManager.Instance.SinglePlayerExtended)
            AvatarSelection.EnableElements();
        else
            Players.EnableElements();
    }



    public void ExitFromTraining()
    {
        if ((GameManager.Instance != null && GameManager.Instance.SinglePlayerExtended) || debugsingleplayer)
        {
            GameManager.Instance.NetworkManager.matchSize = 1;
            PreAvvio(0, true);
        }
        else
            LoadNetworkManager();
    }

    void LoadNetworkManager()
    {
        if (Training.ElementsEnabled)
            Training.DisableElements();
        else if (ExitTraining.ElementsEnabled)
        {
            if (!UnityEngine.VR.VRDevice.isPresent)
            {
                CurrentController.transform.position = InitialFPSPosition;
                CurrentController.GetComponent<CharacterMotor>().enabled = false;
                CurrentController.GetComponent<FirstPersonMainView>().enabled = true;
                CurrentController.GetComponent<FirstPersonItemController>().enabled = false;
                Debug.Log("disattivata questa riga altrimenti crashava.");
                // CurrentController.transform.Find("Main Camera").Find("Controller (right)").Find("New Game Object").gameObject.SetActive(true);
                CurrentController.transform.Find("Main Camera").Find("Controller (right)").GetComponent<SteamVR_LaserPointer>().active = true;
            }
            else
            {
                CurrentController.transform.position = InitialHTCVivePosition;
                CurrentController.GetComponent<VRItemController>().enabled = false;
                //CurrentController.transform.GetComponent<SphereCollider>().enabled = false;
                var ic = CurrentController.GetComponent<VRItemController>();
                ic.LeftController.GetComponent<ControllerManager>().enabled = false;
                ic.RightController.GetComponent<AudioSource>().enabled = true;
                ic.RightController.GetComponent<RayCastForController>().enabled = true;
                //ic.RightController.transform.GetComponent<SphereCollider>().enabled = false;
                ic.RightController.transform.GetComponent<ControllerManager>().enabled = false;

                if ((GameManager.Instance != null && FakeType == ControllerType.KatWalk) || (GameManager.Instance == null && FakeType == ControllerType.KatWalk))
                {
                    CurrentController.transform.GetChild(0).GetComponent<KATDevice>().multiply = 0;
                    CurrentController.transform.GetChild(0).GetComponent<KATDevice>().multiplyBack = 0;
                }
                else if ((GameManager.Instance != null && FakeType == ControllerType.CVirtualizer) || (GameManager.Instance == null && FakeType == ControllerType.CVirtualizer))
                    CurrentController.GetComponent<CVirtPlayerController>().movementSpeedMultiplier = 0;
                else if ((GameManager.Instance != null && FakeType == ControllerType.FreeWalk) || (GameManager.Instance == null && FakeType == ControllerType.FreeWalk))
                    CurrentController.GetComponent<CharacterControllerVR>().Blocked = true;
                else
                    CurrentController.transform.Find("[CameraRig]").GetComponent<OpenVRSwinger>().SwingEnabled = true;
            }
            if (Cassa != null)
                Cassa.gameObject.SetActive(false);
            ExitTraining.DisableElements();
        }
        AbilitaNetworkHUD();
    }




    //tesisti

    private void AvviaServer(uint n)
    {
        GameManager.Instance.Server = true;
        GameManager.Instance.NetworkManager.matchSize = n;
        GameManager.Instance.NetworkManager.StartServer();
        NetworkManagerHUD.DisableElements();
        Description3.text += GameManager.Instance.NetworkManager.matchName + " Port=" + GameManager.Instance.NetworkManager.networkPort;
        StartServerPage.EnableElements();
        if (GameManager.Instance.NetworkDiscovery.Initialize())
        {
            if (GameManager.Instance.NetworkDiscovery.StartBroadcasting())
            {
                Description4.text = "initialized (server)";
            }
            else
            {
                StartBroad2.enabled = true;
                StartBroad2.gameObject.SetActive(true);
                Description4.text = "initialized";
            }
        }
        else
        {
            InitBroad2.enabled = true;
            InitBroad2.gameObject.SetActive(true);
        }
    }



    public void ClickNumberPlayers(int n)
    {
        numeroPlayers.DisableElements();
        numeroPlayersServerOnly.DisableElements();
        GameManager.Instance.MaxPlayers = (uint)n;

        PreAvvio(n, false);


    }



    protected void PreAvvio(int nServer, bool SP, bool client = false, bool clientCustomIP = false)
    {
        numeroPlayers.DisableElements();
        numeroPlayersServerOnly.DisableElements();
        Training.DisableElements();
        NetworkManagerHUD.DisableElements();

        AttesaSmoke.EnableElements();
        StartCoroutine(DelayedLoad(nServer, SP, client, clientCustomIP));
    }


    IEnumerator DelayedLoad(int nServer, bool SP, bool client, bool clientCustomIP)
    {
        //copiato il startTraining
        if (!UnityEngine.VR.VRDevice.isPresent)
        {
            CurrentController.GetComponent<CharacterMotor>().enabled = true;
            CurrentController.GetComponent<FirstPersonMainView>().enabled = false;
            CurrentController.GetComponent<FirstPersonItemController>().enabled = true;
            CurrentController.transform.Find("Main Camera").Find("Controller (right)").GetComponent<SteamVR_LaserPointer>().active = false;
            CurrentController.transform.Find("Main Camera").Find("Controller (right)").Find("New Game Object").gameObject.SetActive(false);
        }
        else
        {
            var ic = CurrentController.GetComponent<VRItemController>();
            ic.enabled = true;
            ic.LeftController.GetComponent<SphereCollider>().enabled = true;
            ic.LeftController.GetComponent<ControllerManager>().enabled = true;
            ic.LeftController.GetComponent<AudioSource>().enabled = false;
            ic.RightController.GetComponent<RayCastForController>().enabled = false;
            ic.RightController.GetComponent<SphereCollider>().enabled = true;
            ic.RightController.GetComponent<ControllerManager>().enabled = true;

            if ((GameManager.Instance == null && FakeType == ControllerType.KatWalk) || (GameManager.Instance != null && GameManager.Instance.Platform == ControllerType.KatWalk))
            {
                CurrentController.transform.Find("CameraHolder").GetComponent<KATDevice>().multiply = 3;
                CurrentController.transform.Find("CameraHolder").GetComponent<KATDevice>().multiplyBack = .5f;
            }
            else if ((GameManager.Instance == null && FakeType == ControllerType.CVirtualizer) || (GameManager.Instance != null && GameManager.Instance.Platform == ControllerType.CVirtualizer))
                CurrentController.GetComponent<CVirtPlayerController>().movementSpeedMultiplier = 1;
            else
                CurrentController.transform.Find("[CameraRig]").GetComponent<OpenVRSwinger>().SwingEnabled = true;
        }

        if (Cassa != null)
            Cassa.gameObject.SetActive(true);


        while (!GameManager.Instance.SmokeFile.loadingDone)
        {
            //GameManager.Instance.SmokeFile.childThread.Join();
            yield return new WaitForSeconds(0.5f);
        }
        AttesaSmoke.EnableElements();

        //copiato il fineTraining
        Destroy(Armature.gameObject);
        if (UnityEngine.VR.VRDevice.isPresent)
        {
            CurrentController.gameObject.SetActive(false);
            HTCViveController.gameObject.SetActive(true);
        }
        CurrentController = null;


        if (SP)
        {
            GameManager.Instance.NetworkManager.StartHost();
        }
        else if(client)
        {
            if (GameManager.Instance.NetworkDiscovery.Initialize())
            {
                NetworkManagerHUD.DisableElements();
                NetworkDiscoveryPage.EnableElements();
                Description2.text = "initialized";
            }
            else
            {
                InitBroad.enabled = true;
                InitBroad.gameObject.SetActive(true);
                Description2.text = olddesc2;
            }
        }
        else if(clientCustomIP)
        {
            GameManager.Instance.NetworkManager.StartClient();
            Description.text += GameManager.Instance.NetworkManager.networkAddress + ":" + GameManager.Instance.NetworkManager.networkPort + "..";
            
            ClientConnectPage.EnableElements();
        }
        else
        {
            if (hostOnly)
            {
                StartAsHost();
            }
            else
            {
                //GameManager.Instance.SmokeFile.childThread.Join();
                AvviaServer((uint)nServer);
            }
        }
    }

    public void LoadBackFromNumPlayers()
    {
        numeroPlayers.DisableElements();
        numeroPlayersServerOnly.DisableElements();

        LoadNetworkManager();
    }

}
