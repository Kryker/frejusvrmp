﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationID : MonoBehaviour {

    public uint ID;
    LocalizerController Localizer;

	// Use this for initialization
	void Start () {
        Localizer = GameObject.FindGameObjectWithTag("Localizer").GetComponent<LocalizerController>();
        if (!Localizer.LocalizedObjects.Contains(this))
        {
            Localizer.LocalizedObjects.Add(this);
            if (Localizer.Localized)
                Localizer.ApplyLanguage(this);
        }
	}

    private void OnDestroy()
    {
        if (Localizer != null && Localizer.LocalizedObjects.Contains(this))
            Localizer.LocalizedObjects.Remove(this);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
