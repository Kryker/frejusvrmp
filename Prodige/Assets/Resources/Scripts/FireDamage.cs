﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(SphereCollider))]
public class FireDamage : MonoBehaviour {

    CanBeOnFire Parent;
    CanBeOnFireNet ParentNet;

    public bool ManageTemperature = false;
    //public float Temperature = 250;
    public float TimeBetweenBlasts = 0.8f;
    public int Importanza = 1;
    public List<CanBeOnFireSlave> canbeonfires;


    void Start () {
       // coll = GetComponent<SphereCollider>();
        //var p = GetComponentInParent<CanBeOnFire>();
      
        //if(p == null)
        //{
            //var pn = GetComponentInParent<CanBeOnFireNet>();
            //if (pn == null)
            //{
                var s = GetComponentInParent<CanBeOnFireSlave>();
                if(s != null)
                {
                    if (s.Master != null)
                        Parent = s.Master;
                    else if(s.MasterNet != null)
                        ParentNet = s.MasterNet;
                }
            //}
            //else
            //    ParentNet = pn;
        //}
        //else
         //   Parent = p;


        StartCoroutine(ScaldaVicini());
    } 
    public void SetParent(CanBeOnFire t)
    {
        Parent = t;
    }
    public void SetParent(CanBeOnFireNet t)
    {
        ParentNet = t;
    }
    public void SetParent(CanBeOnFireSlave t)
    {
        if (t.Master != null)
            Parent = t.Master;
        else if (t.MasterNet != null)
            ParentNet = t.MasterNet;
    }

    //gestore delle ondate di calore che fanno danno, a tutti gli elementi in lista: la lista la popolo da Unity
    IEnumerator ScaldaVicini()
    {
        while(true)
        {
            yield return new WaitForSeconds(TimeBetweenBlasts);
            float temperatura = 0.0f;
            bool burining = false;
            if (Parent != null && Parent.burning)
            {
                burining = true;
                temperatura = Parent.Temperature;
            }
               
            if (ParentNet != null && ParentNet.burning)
            {
                burining = true;
                temperatura = ParentNet.Temperature;
            }
            //Debug.Log(ParentNet.Slave.name);
            if(burining)
            {
                foreach (var el in canbeonfires)
                {
                    for(int i = 0; i < Importanza; i++)
                        el.SendMessage("WarmUp", temperatura, SendMessageOptions.DontRequireReceiver);
                }
            }
           

        }       
    }

    /* 
   //vecchia roba:
   //[HideInInspector]
   //public SphereCollider coll;
   //List<Collider> players = new List<Collider>();


   * Vecchio metodo 
  void Update () {
      var now = Time.time;
      if (((Parent != null && Parent.burning) || (ParentNet != null && ParentNet.burning)) && players.Count > 0)
      {
          if (now >= nextblast)
          {
              var colliders = RemoveNullColliders(players);
              foreach (Collider c in colliders)
              {
                  var t = c.transform;
                  var dist = Vector3.Distance(t.position, transform.position);

                  if (dist >= MaxDist / 2)
                      t.SendMessage("ApplyDamageWithPain", Damage * 0.25f, SendMessageOptions.DontRequireReceiver);
                  else if (dist >= MaxDist / 2 && dist < MaxDist * 0.75f)
                      t.SendMessage("ApplyDamageWithPain", Damage * 0.5f, SendMessageOptions.DontRequireReceiver);
                  else if (dist >= MaxDist * 0.25f && dist >= MaxDist / 2)
                      t.SendMessage("ApplyDamageWithPain", Damage * 0.75f, SendMessageOptions.DontRequireReceiver);
                  else if (dist > 0 && dist < MaxDist * 0.25f)
                      t.SendMessage("ApplyDamageWithPain", Damage, SendMessageOptions.DontRequireReceiver);
              }
              if (ManageTemperature)
              {
                  colliders = RemoveNullColliders(canbeonfires);
                  foreach (Collider c in canbeonfires)
                  {
                      var t = c.transform;
                      var dist = Vector3.Distance(t.position, transform.position);

                      if (dist >= MaxDist / 2)
                          t.SendMessage("WarmUp", Temperature * 0.25f, SendMessageOptions.DontRequireReceiver);
                      else if (dist >= MaxDist / 2 && dist < MaxDist * 0.75f)
                          t.SendMessage("WarmUp", Temperature * 0.5f, SendMessageOptions.DontRequireReceiver);
                      else if (dist >= MaxDist * 0.25f && dist >= MaxDist / 2)
                          t.SendMessage("WarmUp", Temperature * 0.75f, SendMessageOptions.DontRequireReceiver);
                      else if (dist > 0 && dist < MaxDist * 0.25f)
                          t.SendMessage("WarmUp", Temperature, SendMessageOptions.DontRequireReceiver);
                  }
              }
              nextblast = now + TimeBetweenBlasts;
          }
      }
  }

  private List<Collider> RemoveNullColliders(List<Collider> colliders)
  {
      List<Collider> result = new List<Collider>();
      var c = colliders.ToArray();
      for (int i = 0; i < c.Length; i++)
          if (c[i] != null)
              result.Add(c[i]);
      return result;
  }


  private void OnTriggerEnter(Collider other)
  {
      if(other.gameObject.layer == LayerMask.NameToLayer("PlayerLayer"))
          players.Add(other);
      if(ManageTemperature)
          if (other.GetComponent<CanBeOnFire>() || other.GetComponent<CanBeOnFireNet>() || other.GetComponent<CanBeOnFireSlave>())
              canbeonfires.Add(other);
  }

  private void OnTriggerExit(Collider other)
  {
      if (players.Contains(other))
          players.Remove(other);
      if (ManageTemperature)
          if (canbeonfires.Contains(other))
          canbeonfires.Remove(other);
  }
  */
}