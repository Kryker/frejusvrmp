﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceFader : MonoBehaviour
{


    public AudioSource source;
    Coroutine lastroutine, audiosourceroutine;
    public float minvolume { private set; get; }
    public float maxvolume { private set; get; }
    public float fadetime { private set; get; }
    public enum FadeDirection { Fade, Raise };
    public FadeDirection direction { private set; get; }
    bool transition = false;
    float originalvolume;
    bool fadesetted = false;

    private void Awake()
    {
        originalvolume = source.volume;
    }

    public void Reset()
    {
        source.volume = originalvolume;
        transition = false;
        if (lastroutine != null)
            StopCoroutine(lastroutine);
        if (audiosourceroutine != null)
            StopCoroutine(audiosourceroutine);
        fadesetted = false;
    }

    public void SetFader(float minvolume, float maxvolume, float fadetime, FadeDirection direction)
    {
        if (maxvolume > 1 || minvolume < -1 || (maxvolume == -1 && minvolume == -1) || (maxvolume == -1 && originalvolume < minvolume) || (minvolume == -1 && originalvolume > maxvolume))
            return;
        if (minvolume == -1)
            this.minvolume = originalvolume;
        else
            this.minvolume = minvolume;
        if (maxvolume == -1)
            this.maxvolume = originalvolume;
        else
            this.maxvolume = maxvolume;
        this.fadetime = fadetime;
        this.direction = direction;
        fadesetted = true;
    }

    public bool StartFader(float secondsbefore)
    {
        if (!fadesetted)
            return false;
        else if (direction == FadeDirection.Fade)
            FadeVolume(secondsbefore);
        else
            RaiseVolume(secondsbefore);

        return true;
    }

    bool RaiseVolume(float secondsbefore)
    {
        if (!fadesetted)
            return false;
        if (transition == false)
            lastroutine = StartCoroutine(RaiseVolumeCoroutine(secondsbefore));
        else
            ReRaiseVolume(secondsbefore);
        return true;
    }

    bool FadeVolume(float secondsbefore)
    {
        if (!fadesetted)
            return false;
        if (transition == false)
            lastroutine = StartCoroutine(FadeVolumeCoroutine(secondsbefore));
        else
            ReFadeVolume(secondsbefore);
        return true;
    }

    public bool ReFadeVolume(float secondsbefore)
    {
        if (!fadesetted)
            return false;
        if (transition)
        {
            var newroutine = StartCoroutine(ReFadeVolumeCoroutine(secondsbefore));
            lastroutine = newroutine;
        }
        else
            FadeVolume(secondsbefore);
        return true;
    }

    public bool ReRaiseVolume(float secondsbefore)
    {
        if (!fadesetted)
            return false;
        if (transition)
        {
            var newroutine = StartCoroutine(ReRaiseVolumeCoroutine(secondsbefore));
            lastroutine = newroutine;
        }
        else
            RaiseVolume(secondsbefore);
        return true;
    }

    IEnumerator RaiseVolumeCoroutine(float secondsbefore)
    {
        yield return new WaitForSeconds(secondsbefore);
        if (transition)
            yield break;
        transition = true;
        source.volume = minvolume;
        direction = FadeDirection.Raise;
        audiosourceroutine = StartCoroutine(Raise());
        yield return new WaitWhile(() => source.volume < maxvolume);
        transition = false;
    }

    IEnumerator FadeVolumeCoroutine(float secondsbefore)
    {
        yield return new WaitForSeconds(secondsbefore);
        if (transition)
            yield break;
        transition = true;
        source.volume = maxvolume;
        direction = FadeDirection.Fade;
        audiosourceroutine = StartCoroutine(Fade());
        yield return new WaitWhile(() => source.volume > minvolume);
        transition = false;
    }

    IEnumerator ReFadeVolumeCoroutine(float secondsbefore)
    {
        yield return new WaitForSeconds(secondsbefore);
        if (!transition)
            yield break;
        if (lastroutine != null)
            StopCoroutine(lastroutine);
        if (audiosourceroutine != null)
            StopCoroutine(audiosourceroutine);
        direction = FadeDirection.Fade;
        audiosourceroutine = StartCoroutine(Fade());
        yield return new WaitWhile(() => source.volume > minvolume);
        transition = false;
    }


    IEnumerator ReRaiseVolumeCoroutine(float secondsbefore)
    {
        yield return new WaitForSeconds(secondsbefore);
        if (!transition)
            yield break;
        if (lastroutine != null)
            StopCoroutine(lastroutine);
        if (audiosourceroutine != null)
            StopCoroutine(audiosourceroutine);
        direction = FadeDirection.Raise;
        audiosourceroutine = StartCoroutine(Raise());
        yield return new WaitWhile(() => source.volume < maxvolume);
        transition = false;
    }

    IEnumerator Raise()
    {
        float startVolume = source.volume;
        if (startVolume == 0)
            startVolume = 0.2f;

        while (source.volume < maxvolume)
        {
            source.volume += startVolume * Time.deltaTime / fadetime;

            yield return null;
        }

        source.volume = maxvolume;
    }

    IEnumerator Fade()
    {
        float startVolume = source.volume;

        while (source.volume > minvolume)
        {
            source.volume -= startVolume * Time.deltaTime / fadetime;

            yield return null;
        }

        //source.Stop();
        source.volume = minvolume;
    }


    public bool SetUpAndStart(float minvolume, float maxvolume, float time, AudioSourceFader.FadeDirection dir)
    {
        SetFader(minvolume, maxvolume, time, dir);
        return StartFader(0);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
