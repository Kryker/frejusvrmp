﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelTelephoneSectionController : TunnelSectionController
{
    public Nicchia NicchiaTelSX, NicchiaTelDX, NicchiaExtSX, NicchiaExtDX;
    public Semaforo SemaforoSX, SemaforoDX;
    bool leftflashingenabled, rightflashingenabled = false;
    bool flashingyellowon;
    float tictimeup = 0.6f;
    public int NumNicchiaDX, NumNicchiaSX;
    public TunnelTelephoneSectionController Precedente, Successiva;
    float tictimedown = 0.4f;
    float nexttic = 0;
    // Use this for initialization
    void OnEnable () {
    }

    public override void Start()
    {
        base.Start();
        if (NicchiaTelSX != null)
        {
            NicchiaTelSX.posizione = Posizione.Sinistra;
            NicchiaTelSX.tipo = NicheType.Tel;
        }
        if (NicchiaTelDX != null)
        {
            NicchiaTelDX.posizione = Posizione.Destra;
        NicchiaTelSX.tipo = NicheType.Tel;
    }
        if (NicchiaExtSX != null)
        {
            NicchiaExtSX.posizione = Posizione.Sinistra;
            NicchiaTelSX.tipo = NicheType.Ext;
        }
        if (NicchiaExtDX != null)
        {
            NicchiaExtDX.posizione = Posizione.Destra;
            NicchiaTelSX.tipo = NicheType.Ext;
        }
    }
    void Update()
    {
        if (leftflashingenabled || rightflashingenabled)
        {
            var now = Time.time;
            if (nexttic <= now)
            {
                if (!flashingyellowon)
                {
                    if (leftflashingenabled && rightflashingenabled)
                        SignOn();
                    if (leftflashingenabled)
                        SignOn(Posizione.Sinistra);
                    else
                        SignOn(Posizione.Destra);
                    flashingyellowon = true;
                    nexttic = now + tictimeup;
                }
                else
                {
                    if (leftflashingenabled && rightflashingenabled)
                        SignOff();
                    if (leftflashingenabled)
                        SignOff(Posizione.Sinistra);
                    else
                        SignOff(Posizione.Destra);
                    flashingyellowon = false;
                    nexttic = now + tictimedown;
                }
            }
        }
    }

    void SignOn()
    {
        SemaforoDX.SetState(Semaforo.Stato.Giallo);
        SemaforoSX.SetState(Semaforo.Stato.Giallo);
    }
    void SignOn(Posizione semaforo)
    {
        if (semaforo == Posizione.Sinistra)
            SemaforoSX.SetState(Semaforo.Stato.Giallo);
        else
            SemaforoDX.SetState(Semaforo.Stato.Giallo);
    }
    void SignOff()
    {
        SemaforoDX.SetState(Semaforo.Stato.Spento);
        SemaforoSX.SetState(Semaforo.Stato.Spento);
    }
    void SignOff(Posizione semaforo)
    {
        if (semaforo == Posizione.Sinistra)
            SemaforoSX.SetState(Semaforo.Stato.Spento);
        else
            SemaforoDX.SetState(Semaforo.Stato.Spento);
    }


    private void EnableRed()
    {
        if (leftflashingenabled || rightflashingenabled)
            SemaphoreOff();
        SemaforoSX.SetState(Semaforo.Stato.Rosso);
        SemaforoDX.SetState(Semaforo.Stato.Rosso);
    }

    private void SemaphoreOff()
    {
        leftflashingenabled = false;
        rightflashingenabled = false;
        SemaforoSX.SetState(Semaforo.Stato.Spento);
        SemaforoDX.SetState(Semaforo.Stato.Spento);
    }

    private void EnableYellow()
    {
        if (leftflashingenabled || rightflashingenabled)
            SemaphoreOff();
        SemaforoSX.SetState(Semaforo.Stato.Giallo);
        SemaforoDX.SetState(Semaforo.Stato.Giallo);
    }

    private void EnableGreen()
    {
        if (leftflashingenabled || rightflashingenabled)
            SemaphoreOff();
        SemaforoSX.SetState(Semaforo.Stato.Verde);
        SemaforoDX.SetState(Semaforo.Stato.Verde);
    }

    private void EnableRed(Posizione semaforo)
    {
        if(semaforo == Posizione.Destra)
        {
            if (rightflashingenabled)
                SemaphoreOff(semaforo);
            SemaforoDX.SetState(Semaforo.Stato.Rosso);
        }
        else
        {

            if (leftflashingenabled)
                SemaphoreOff(semaforo);
            SemaforoSX.SetState(Semaforo.Stato.Rosso);
        }
    }

    private void SemaphoreOff(Posizione semaforo)
    {
        if(semaforo == Posizione.Sinistra)
        {
            leftflashingenabled = false;
            SemaforoSX.SetState(Semaforo.Stato.Spento);
        }
        else
        {
            rightflashingenabled = false;
            SemaforoDX.SetState(Semaforo.Stato.Spento);
        }
    }

    private void EnableYellow(Posizione semaforo)
    {
        if (semaforo == Posizione.Destra)
        {
            if (rightflashingenabled)
                SemaphoreOff(semaforo);
            SemaforoDX.SetState(Semaforo.Stato.Giallo);
        }
        else
        {

            if (leftflashingenabled)
                SemaphoreOff(semaforo);
            SemaforoSX.SetState(Semaforo.Stato.Giallo);
        }
    }

    private void EnableGreen(Posizione semaforo)
    {
        if (semaforo == Posizione.Destra)
        {
            if (rightflashingenabled)
                SemaphoreOff(semaforo);
            SemaforoDX.SetState(Semaforo.Stato.Verde);
        }
        else
        {

            if (leftflashingenabled)
                SemaphoreOff(semaforo);
            SemaforoSX.SetState(Semaforo.Stato.Verde);
        }
    }

    public void StartWarning()
    {
        if (SemaforoDX != null && SemaforoSX != null)
            EnableYellowFlashing();
    }

    public void EnableYellowFlashing()
    {
        SemaphoreOff();
        leftflashingenabled = true;
        rightflashingenabled = true;
    }

    public void EnableYellowFlashing(Posizione semaforo)
    {
        SemaphoreOff(semaforo);
        if(semaforo == Posizione.Sinistra)
            leftflashingenabled = true;
        else if (semaforo == Posizione.Destra)
            rightflashingenabled = true;
    }

    public bool FlashingEnabled()
    {
        if (rightflashingenabled || leftflashingenabled)
            return true;
        else
            return false;
    }
    public bool FlashingEnabled(Posizione semaforo)
    {
        if ((semaforo == Posizione.Destra && rightflashingenabled) || (semaforo == Posizione.Sinistra && leftflashingenabled))
            return true;
        else
            return false;
    }
    public void StartFireProcedures()
    {
        if (TC == null)
        {
            var tc = GameObject.FindGameObjectWithTag("TunnelController");
            if (tc != null)
                TC = tc.GetComponent<TunnelController>();
            else
                {
                    if (SemaforoSX != null)
                        SemaforoSX.SetState(Semaforo.Stato.Rosso);
                    if (SemaforoDX != null)
                        SemaforoDX.SetState(Semaforo.Stato.Rosso);
                return;
                }
        }
        if (transform.position.z < TC.FireSection.transform.position.z)
            {
            if (SemaforoDX != null)
                SemaforoDX.SetState(Semaforo.Stato.Rosso);
            if (SemaforoSX != null)
                EnableYellowFlashing(Posizione.Sinistra);
            }
        else
            {
            if (SemaforoSX != null)
                SemaforoSX.SetState(Semaforo.Stato.Rosso);
            if (SemaforoDX != null)
                EnableYellowFlashing(Posizione.Destra);
            }
    }

    public override void Reset()
    {
        LightsOn(true);
        NicchiaTelSX.Reset();
        NicchiaTelDX.Reset();
        NicchiaExtSX.Reset();
        NicchiaExtDX.Reset();
        base.Reset();
    }

    /*public override void LightsOn(bool active)
        {
        if (active)
            {
            foreach (Transform t in Lights)
                {
                t.FindChild("LampioniAccesi").gameObject.SetActive(true);
                t.FindChild("LampioniSpenti").gameObject.SetActive(false);
                }
            }
        else
            {
            foreach(Transform t in Lights)
                {
                t.FindChild("LampioniAccesi").gameObject.SetActive(false);
                t.FindChild("LampioniSpenti").gameObject.SetActive(true);
                }
            }
        }*/
}
