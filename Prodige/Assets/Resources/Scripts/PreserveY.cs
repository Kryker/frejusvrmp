﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreserveY : MonoBehaviour {

    float startposY;
	// Use this for initialization
	void Start () {
        startposY = transform.position.y;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (transform.position.y != startposY)
            transform.position = new Vector3(transform.position.x, startposY, transform.position.z);
        if (transform.rotation.eulerAngles.x != 0 || transform.rotation.eulerAngles.z != 0)
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
	}
}
