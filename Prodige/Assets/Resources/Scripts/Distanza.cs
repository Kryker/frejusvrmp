﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Distanza : MonoBehaviour
{
    public enum Posizione { Destra, Sinistra};
    public Posizione PosizioneCartello;
    public int ForcedValue;
    public Transform RifugioDiRiferimento;
    TunnelShelterSectionControllerNet rifugio, _rifugioDiRiferimento;
    Text txt;
    public Color TextColor = new Color32(0x76, 0x76, 0x76, 0xFF);
    float dist;
    bool startlater = false;

    public void Start()
    {
        _rifugioDiRiferimento = RifugioDiRiferimento.GetComponentInChildren<TunnelShelterSectionControllerNet>();
        if (_rifugioDiRiferimento == null)
            startlater = true;
        else
        {
            if (PosizioneCartello == Posizione.Destra)
            {
                if (transform.parent.position.z < RifugioDiRiferimento.position.z)
                    rifugio = _rifugioDiRiferimento;// transform.parent.GetComponentInParent<TunnelShelterSectionController>();
                else
                    rifugio = _rifugioDiRiferimento/*transform.parent.GetComponentInParent<TunnelShelterSectionController>()*/.Successivo;
            }
            else
            {
                if (transform.parent.position.z < RifugioDiRiferimento.position.z)
                    rifugio = _rifugioDiRiferimento/*transform.parent.GetComponentInParent<TunnelShelterSectionController>()*/.Precedente;
                else
                    rifugio = _rifugioDiRiferimento;//transform.parent.GetComponentInParent<TunnelShelterSectionController>();
            }

            txt = transform.GetComponentInChildren<Text>();
            var m = txt.material;
            var newm = new Material(m);
            newm.color = TextColor;
            txt.color = TextColor;
            txt.material = newm;
            if (rifugio != null)
            {
                //dist = Vector3.Distance(Rifugio.transform.position, Cartello.transform.position);

                dist = Mathf.Abs(rifugio.transform.position.z - transform.parent.position.z);

                float rem = dist % 5;
                dist -= rem;
                if (rem > 2.5f)
                    dist += 5;


                txt.text = Convert.ToInt32(dist).ToString();
            }
            else
            {
                int rem = ForcedValue % 5;
                ForcedValue -= rem;
                if (rem > 2.5f)
                    ForcedValue += 5;

                txt.text = Convert.ToInt32(ForcedValue).ToString();
            }
               
            enabled = false;
        }
    }
    private void Update()
    {
        if (startlater)
        {
            _rifugioDiRiferimento = RifugioDiRiferimento.GetComponentInChildren<TunnelShelterSectionControllerNet>();
            if (_rifugioDiRiferimento != null)
            {
                if (PosizioneCartello == Posizione.Destra)
                {
                    if (transform.parent.position.z < RifugioDiRiferimento.position.z)
                        rifugio = _rifugioDiRiferimento;// transform.parent.GetComponentInParent<TunnelShelterSectionController>();
                    else
                        rifugio = _rifugioDiRiferimento/*transform.parent.GetComponentInParent<TunnelShelterSectionController>()*/.Successivo;
                }
                else
                {
                    if (transform.parent.position.z < RifugioDiRiferimento.position.z)
                        rifugio = _rifugioDiRiferimento/*transform.parent.GetComponentInParent<TunnelShelterSectionController>()*/.Precedente;
                    else
                        rifugio = _rifugioDiRiferimento;//transform.parent.GetComponentInParent<TunnelShelterSectionController>();
                }

                txt = transform.GetComponentInChildren<Text>();
                var m = txt.material;
                var newm = new Material(m);
                newm.color = TextColor;
                txt.color = TextColor;
                txt.material = newm;
                if (rifugio != null)
                {
                    //dist = Vector3.Distance(Rifugio.transform.position, Cartello.transform.position);

                    dist = Mathf.Abs(rifugio.transform.position.z - transform.parent.position.z);
                    float rem = dist % 5;
                    dist -= rem;
                    if (rem > 2.5f)
                        dist += 5;

                    txt.text = Convert.ToInt32(dist).ToString();
                }
                else
                {
                    int rem = ForcedValue % 5;
                    ForcedValue -= rem;
                    if (rem > 2.5f)
                        ForcedValue += 5;

                    txt.text = Convert.ToInt32(ForcedValue).ToString();
                }
                startlater = false;
                enabled = false;
            }
        }
    }
}