﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum SecureState { Open, Closed, Unknown };
public class Extinguisher : GenericItem
{

    Vector3 StartPos;
    Quaternion StartRot;
    public Transform Button;
    public Transform Sicura;
    [HideInInspector]
    public SecureState Secured
    {
        get { return _secured; }
        set
        {
            if (value == SecureState.Closed)
                MettiSicura();
            else if (value == SecureState.Open)
                RimuoviSicura();
            _secured = value;
        }
    }
    SecureState _secured = SecureState.Unknown;
    [Range(0.0f, 100.0f)]
    public float Fuel = 100.0f;
    public SicuraExt sicuraExt;
    [HideInInspector]
    public BaseExt Base;
    [HideInInspector]
    public bool OnBase;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        Secured = SecureState.Closed;
    }
    private void Awake()
    {
        ItemCode = ItemCodes.Extinguisher;
        StartPos = transform.position;
        StartRot = transform.rotation;
    }

    // Update is called once per frame
    public override void Update()
    {
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {

    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {

    }

    public override void DropParent()
    {
        if (Base != null && OnBase)
            ReBase();
        else
            Drop();
    }

    internal void Drop()
    {
        base.DropParent();
    }

    internal void ReBase()
    {
        transform.position = StartPos;
        transform.rotation = StartRot;
        Drop();
        Base.DisableOutline();
    }

    public override void Reset()
    {
        transform.position = StartPos;
        transform.rotation = StartRot;
        Drop();
        Fuel = 100.0f;
        MettiSicura();
        base.Reset();
    }

    void RimuoviSicura()
    {
        Sicura.gameObject.SetActive(false);
    }

    void MettiSicura()
    {
        Sicura.gameObject.SetActive(true);
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
    }

    public override void EnablePhysics()
    {
        if (Base == null || !OnBase)
            base.EnablePhysics();
    }
}
