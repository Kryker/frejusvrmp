﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckController : VehicleData {
    public Transform FuelTank;
    public List<Transform> Wheels;
    public float WheelBurnFactor = 1.0f;
    public List<Transform> Doors;
    public float DoorBurnFactor = 1.0f;
    public Transform Tarp;
    public float TarpBurnFactor = 3.0f;
    public float VehicleBurnFactor = 0.1f;
    public bool loaded = false;
    public Transform Load;
    public float LoadBurnFactor=0.05f;
    public float TankBurnFactor = 1.0f;
    public float BaseDuration = 20.0f;

    // Use this for initialization
    public override void Start()
        {
        VehicleBody.GetComponent<CanBeOnFireSlave>().Master.BurnFactor = VehicleBurnFactor;
        VehicleBody.GetComponent<CanBeOnFireSlave>().Master.duration = 1 / VehicleBurnFactor * BaseDuration;
        Tarp.GetComponent<CanBeOnFireSlave>().Master.BurnFactor = TarpBurnFactor;
        Tarp.GetComponent<CanBeOnFireSlave>().Master.duration = 1 / TarpBurnFactor * BaseDuration;
        foreach (Transform t in Doors)
            {
            t.GetComponent<CanBeOnFireSlave>().Master.BurnFactor = DoorBurnFactor;
            t.GetComponent<CanBeOnFireSlave>().Master.duration = 1 / DoorBurnFactor * BaseDuration;
            }
        foreach (Transform t in Wheels)
            { 
            t.GetComponent<CanBeOnFireSlave>().Master.BurnFactor = WheelBurnFactor;
            t.GetComponent<CanBeOnFireSlave>().Master.duration = 1 / WheelBurnFactor * BaseDuration;
            }
        FuelTank.GetComponent<CanBeOnFireSlave>().Master.BurnFactor = TankBurnFactor;
        FuelTank.GetComponent<CanBeOnFireSlave>().Master.duration = 1 / TankBurnFactor * BaseDuration;
        base.Start();
        }

    // Update is called once per frame
    public override void Update () {
        /*if (FuelTank.GetComponent<CanBeOnFireSlave>().Master.Health <= 0 && vehicle.GetComponent<VehicleData>().FuelTankPercentage>20.0f)
            Explode(vehicle.GetComponent<VehicleData>().FuelTankPercentage);*/
        base.Update();
	}

    public void Explode(float fuelpercentage)
        {
        //implementare esplosione (particellare, wind sferico, rigid body attivato su porte e ruote) - ELIMINATO
        }
}
