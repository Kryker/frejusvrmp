﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAnimator : MonoBehaviour {

    public delegate void AnimatorDelegate(Animator animator);
    public AnimatorDelegate AnimatorEnabled;
    Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if (AnimatorEnabled != null)
            AnimatorEnabled.Invoke(animator);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
