﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Steering { Left, Right, No };
public enum EngineType { Front, Rear, Both };

public class CarMovementScript : MonoBehaviour
{
    public float MotorForce = 600;
    public float BrakeForce = 60000;
    [HideInInspector]
    public Rigidbody body;
    public WheelCollider WheelColFR;
    public WheelCollider WheelColFL;
    public WheelCollider WheelColCR;
    public WheelCollider WheelColCL;
    public WheelCollider WheelColRR;

    public EngineType Type;

    internal void StopAccelerating()
    {
        acceleraterequest = false;
    }

    public WheelCollider WheelColRL;
    bool braking = false;
    public float MaxSpeed = 70;
    [HideInInspector]
    public bool forcebrake = false;
    public bool CanBrake = true, CanStop = false;
    public delegate void MyDelegate();
    public MyDelegate BrakeEnabled, Braking, StopBraking, BrakeDisabled, CarStopped, StopEnabled, StopDisabled;
    TunnelController tc;
    public AudioSource _carMoving;
    public AudioClip moving, brake, firstbrake;
    private bool forcebrakeplayed = false;
    VehicleData Vehicle;
    [HideInInspector]
    public bool stopped = false;
    private bool brakeplayed = false;
    public bool CanBrakeFromStart = true;
    public bool CanMove = false;
    bool acceleraterequest = false;
    bool brakerequest = false;

    //public bool SeekRoad = false;
    // Use this for initialization
    void Start()
    {
        var t = GameObject.FindGameObjectWithTag("TunnelController");
        if (t != null)
            tc = t.GetComponent<TunnelController>();
        if (CanBrakeFromStart)
            SetCanBrake(true);
        /*if (SeekRoad)
            TargetX = transform.position.x;*/
    }
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        Vehicle = GetComponent<VehicleData>();
    }
    public void Stop()
    {
        if (!stopped)
        {
            stopped = true;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public bool IsBrakeRequested()
    {
        return brakerequest;
    }
    public bool IsAcclerateRequested()
    {
        return acceleraterequest;
    }
    public void RequestAccelerate()
    {
        acceleraterequest = true;
    }

    public void RequestBrake()
    {
        brakerequest = true;
    }

    public void RequestStopAccelerate()
    {
        acceleraterequest = false;
    }

    public void RequestStopBrake()
    {
        brakerequest = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (stopped || !CanMove)
            return;
        /*if(SeekRoad)
        {
            if (transform.position.x > TargetX + 0.01f && mode != Steering.Left)
            {
                if (mode != Steering.No)
                    StopCoroutine(lastroutine);
                lastroutine = StartCoroutine(StartSteering(Steering.Left));
            }
            else if (transform.position.x < TargetX - 0.01f && mode != Steering.Right)
            {
                if (mode != Steering.No)
                    StopCoroutine(lastroutine);
                lastroutine = StartCoroutine(StartSteering(Steering.Right));
            }
            else
            {
                if (mode != Steering.No)
                {
                    StopCoroutine(lastroutine);
                    lastroutine = StartCoroutine(StartSteering(Steering.Right));
                    mode = Steering.No;
                }
            }

        }*/
        if ((brakerequest && CanBrake) || forcebrake) //se sto frenando e posso frenare, o sono costretto a frenare
        {
            if (Vehicle.Accelerating)
                Vehicle.Accelerating = false;
            if (!braking)
            {
                braking = true;
                if(!forcebrake && !brakeplayed)
                {
                    _carMoving.Stop();
                    _carMoving.clip = firstbrake;
                    _carMoving.volume = 0.15f;
                    _carMoving.loop = false;
                    _carMoving.Play();
                    brakeplayed = true;
                }
                tc.PiediAnimator.SetBool("BrakePressed", true);
                tc.PedaliCarAnimator.SetBool("BrakePressed", true);
                //if (audiosource.isPlaying)
                   //audiosource.Stop();
                if (Braking != null)
                    Braking.Invoke();
            }
            if ((forcebrake && body.velocity.magnitude * 3.6 >= MaxSpeed*0.75f) && !forcebrakeplayed)
            {
                _carMoving.Stop();
                _carMoving.clip = brake;
                _carMoving.volume = 0.5f;
                _carMoving.loop = false;
                _carMoving.Play();
                forcebrakeplayed = true;
            }
            /*if (WheelColFR != null)
            {
                WheelColFR.motorTorque = 0;
            }

            if (WheelColFL != null)
            {
                WheelColFL.motorTorque = 0;
            }
            
            if (WheelColRR != null)
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = BrakeForce;
            }

            if (WheelColRL != null)
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = BrakeForce;
            }*/

        }
        else if (acceleraterequest && (!brakerequest || !CanBrake) && body.velocity.magnitude * 3.6 <= MaxSpeed)      //se non sto frenando o non posso frenare e non sono troppo veloce
        {
            if (!Vehicle.Accelerating)
                Vehicle.Accelerating = true;
            if (braking)
            {
                braking = false;
                brakeplayed = false;
                _carMoving.Stop();
                _carMoving.loop = true;
                _carMoving.volume = 1;
                _carMoving.clip = moving;
                _carMoving.Play();
                tc.PiediAnimator.SetBool("BrakePressed", false);
                tc.PedaliCarAnimator.SetBool("BrakePressed", false);
				tc.PiediAnimator.SetBool ("Stop", false);
				tc.PedaliCarAnimator.SetBool("Stop", false);
                if (StopBraking != null)
                    StopBraking.Invoke();
            }
            if (!_carMoving.isPlaying)
                _carMoving.Play();
           /* if (WheelColFR != null && (Type == EngineType.Front || Type == EngineType.Both))
                WheelColFR.motorTorque = MotorForce;
            else if (WheelColFR != null)
                WheelColFR.motorTorque = 0;

            if (WheelColFL != null && (Type == EngineType.Front || Type == EngineType.Both))
                WheelColFL.motorTorque = MotorForce;
            else if (WheelColFL != null)
                WheelColFL.motorTorque = 0;

            if (WheelColRR != null && (Type == EngineType.Rear || Type == EngineType.Both))
            {
                WheelColRR.motorTorque = MotorForce;
                WheelColRR.brakeTorque = 0;
            }
            else if (WheelColRR != null)
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = 0;
            }

            if (WheelColRR != null && (Type == EngineType.Rear || Type == EngineType.Both))
            {
                WheelColRL.motorTorque = MotorForce;
                WheelColRL.brakeTorque = 0;
            }
            else if (WheelColRL != null)
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = 0;
            }*/
            }
        else //if((IsBraking() && body.velocity.magnitude == 0) || (!IsBraking() && body.velocity.magnitude >= MaxSpeed))       //altrimenti
        {
            if (Vehicle.Accelerating)
                Vehicle.Accelerating = false;
            if (!_carMoving.isPlaying)
                _carMoving.Play();
            /*if (WheelColFR != null)
                WheelColFR.motorTorque = 0;

            if (WheelColFL != null)
                WheelColFL.motorTorque = 0;*/

            /*if (WheelColCR != null)
                WheelColCR.motorTorque = MotorForce;

            if (WheelColCL != null)
                WheelColCL.motorTorque = MotorForce;*/

            /*if (WheelColRR != null && (Type == EngineType.Rear || Type == EngineType.Both))
            {
                WheelColRR.motorTorque = 0;
                WheelColRR.brakeTorque = 0;
            }

            if (WheelColRL != null && (Type == EngineType.Rear || Type == EngineType.Both))
            {
                WheelColRL.motorTorque = 0;
                WheelColRL.brakeTorque = 0;
            }*/
        }

        if (CanStop && ((CanBrake && brakerequest || forcebrake) && body.velocity.magnitude <= 0.001f && CarStopped != null))
            CarStopped.Invoke();

        /*if (Input.GetKeyDown(KeyCode.Space))
            {
            if (WheelColFR != null)
                WheelColFR.motorTorque = MotorForce;

            if (WheelColFL != null)
                WheelColFL.motorTorque = MotorForce;

            if (WheelColCR != null)
                WheelColCR.motorTorque = MotorForce;

            if (WheelColCL != null)
                WheelColCL.motorTorque = MotorForce;

            if (WheelColRR != null)
                WheelColRR.motorTorque = MotorForce;

            if (WheelColRL != null)
                WheelColRL.motorTorque = MotorForce;
            }
        if (Input.GetKeyUp(KeyCode.Space))
            {
            if (WheelColFR != null)
                WheelColFR.motorTorque = 0;

            if (WheelColFL != null)
                WheelColFL.motorTorque = 0;

            if (WheelColCR != null)
                WheelColCR.motorTorque = 0;

            if (WheelColCL != null)
                WheelColCL.motorTorque = 0;

            if (WheelColRR != null)
                WheelColRR.motorTorque = 0;

            if (WheelColRL != null)
                WheelColRL.motorTorque = 0;
            }*/
    }

    /*private IEnumerator StartSteering(Steering mode)
    {
        if (mode == Steering.Right)
        {
            mode = Steering.Right;
            float startrot = WheelColFR.steerAngle;

            while (WheelColFR.transform.localRotation.eulerAngles.z < startrot + 33)
            {
                WheelColFR.steerAngle = startrot + Time.deltaTime / 5;
                WheelColFL.steerAngle = startrot + Time.deltaTime / 5;
                yield return null;
            }

            WheelColFR.steerAngle = startrot + 33;
            WheelColFL.steerAngle = startrot + 33;
        }
        else if (mode == Steering.Left)
        {
            mode = Steering.Left;
            float startrot = WheelColFR.steerAngle;

            while (WheelColFR.transform.localRotation.eulerAngles.z > startrot - 33)
            {
                WheelColFR.steerAngle = startrot - Time.deltaTime / 5;
                WheelColFL.steerAngle = startrot - Time.deltaTime / 5;
                yield return null;
            }

            WheelColFR.steerAngle = startrot - 33;
            WheelColFL.steerAngle = startrot - 33;
        }
        else
        {
            WheelColFR.steerAngle = 0;
            WheelColFL.steerAngle = 0;
            mode = Steering.No;
        }
    }*/

    public void SetCanBrake(bool can)
    {
        if (can != CanBrake)
        {
            CanBrake = can;
            if (can && BrakeEnabled != null)
                BrakeEnabled.Invoke();
            else if (!can && BrakeDisabled != null)
                BrakeDisabled.Invoke();
        }
    }

    public void SetCanStop(bool can)
    {
        if (can != CanStop)
        {
            CanStop = can;
            if (can && StopEnabled != null)
                StopEnabled.Invoke();
            else if (!can && StopDisabled != null)
                StopDisabled.Invoke();
        }
    }


    public void ForceBrake()
    {
        //SetCanBrake(true);
        SetCanStop(true);
        forcebrake = true;
    }
}
