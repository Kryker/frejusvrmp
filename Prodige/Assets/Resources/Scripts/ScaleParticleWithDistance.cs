﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleParticleWithDistance : MonoBehaviour
{

    public float MaxScale = 5;
    float StartScale;
    public float MaxDistanceForMinScale = 1;
    public float MaxDistanceForScaleGrowth = 150;
    PlayerStatusNet pc;
    bool scaling = true;
    // Use this for initialization
    void Start()
    {
        StartScale = GetComponent<ParticleSystem>().main.startSize.constant;
    }

    // Update is called once per frame
    void Update()
    {
        if (pc == null && GameManager.Instance != null)
        {
            pc = GameManager.Instance.TCN.GetPlayerController();
        }
            

        if (scaling && pc != null)
        {
            var main = GetComponent<ParticleSystem>().main;
            var distance = Vector3.Distance(transform.position, pc.transform.position);
            if (distance >= MaxDistanceForScaleGrowth && GetComponent<ParticleSystem>().main.startSize.constant != MaxScale)
                main.startSize = MaxScale;
            else if (distance < MaxDistanceForScaleGrowth && distance >= MaxDistanceForMinScale)
            {
                var rate = (distance - MaxDistanceForMinScale) / (MaxDistanceForScaleGrowth - MaxDistanceForMinScale);
                if (main.startSize.constant != 1.5f + (MaxScale - 1.5f) * rate)
                    main.startSize = 1.5f + (MaxScale - 1.5f) * rate;
            }
            else if (distance < MaxDistanceForMinScale && GetComponent<ParticleSystem>().main.startSize.constant != StartScale)
                main.startSize = StartScale;
        }
    }

    public void StopScaling()
    {
        if (scaling)
        {
            scaling = false;
            var main = GetComponent<ParticleSystem>().main;
            main.startSize = StartScale; 
        }
    }
}
