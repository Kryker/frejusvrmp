﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpeed : MonoBehaviour {
    public Transform leftController, rightController;
    bool _running = false;
    public bool Running
    {
        get { return _running; }
        set
        {
            if (!_running && value)
            {
                    prevpos = new Vector2(transform.position.x, transform.position.z);
                    Speed = 0.0f;
                    Acceleration = 0.0f;
                    if (GameManager.Instance.DebugMode)
                    {
                        FakeLeftHand.Target = LeftHand;
                        FakeRightHand.Target = RightHand;
                    }
            }
            else if (_running && !value)
            {
                Speed = 0.0f;
                Acceleration = 0.0f;
                if (!GameManager.Instance.DebugMode)
                    {
                        FakeLeftHand.Target = null;
                        FakeRightHand.Target = null;
                    }
            }
            _running = value;
        }
    }
    bool _leftfisting, _rightfisting;
    public bool LeftFisting
    {
        get { return _leftfisting; }
        set
        {
            if (anim != null && anim.gameObject.activeSelf)
                anim.SetBool("FistL", value);
            _leftfisting = value;
        }
    }
    public bool RightFisting
    {
        get { return _rightfisting; }
        set
        {

            if (anim != null && anim.gameObject.activeSelf)
                anim.SetBool("FistR", value);
            _rightfisting = value;
        }
    }
    Transform LeftHand, RightHand;
    public FollowSmooth FakeLeftHand, FakeRightHand;
    private Vector2 lastblendTreeMovement = Vector2.zero;
    public ItemController Controller;
    Animator anim;
    public float smoothTime = 0.05f;
    public float Speed = 0.0f, Acceleration = 0.0f;
    Vector2 prevpos = Vector2.zero;
    float prevspeed = 0.0f;
    public float MaxSpeed = 8.0f;
    public Transform Rig, Camera;
    public AnimationCurve SpeedToRunCurve = new AnimationCurve(new Keyframe(0, 0, 0, 1), new Keyframe(1, 1, 1, 0));

    // Use this for initialization
    void Start()
    {
        Running = false;
        LeftFisting = false;
        RightFisting = false;
    }
   
    public void Walking()
    {
        Running = true;
    }

    internal void SetAnimator(Animator a)
    {
        anim = a;
        if (anim != null && anim.gameObject.activeSelf)
        {
            anim.SetBool("FistL", LeftFisting);
            anim.SetBool("FistL", RightFisting);
        }
    }

    public void StopWalk()
    {
        Running = false;
    }

    public static Vector2 calculatePointOnSquare(float r, float angleInDegrees)
    {
        Vector2 p;
        p.x = 0.0f;
        p.y = 0.0f;

        double angle = (angleInDegrees % 360) * Math.PI / 180;

        double angleModPiOverTwo = angle % (Math.PI / 4);

        if (angle >= 0 && angle < Math.PI / 4)
        {
            p.x = r;
            p.y = (float)(r * Math.Tan(angle));
        }
        else if (angle >= Math.PI / 4 && angle < Math.PI / 2)
        {
            p.x = (float)(r * Math.Tan(Math.PI / 2 - angle));
            p.y = r;
        }
        else if (angle >= Math.PI / 2 && angle < 3 * Math.PI / 4)
        {
            p.x = (float)(-1 * r * Math.Tan(angle % (Math.PI / 4)));
            p.y = r;
        }
        else if (angle >= 3 * Math.PI / 4 && angle < Math.PI)
        {
            p.x = -1 * r;
            p.y = (float)(r * Math.Tan(Math.PI - angle));
        }
        else if (angle >= Math.PI && angle < 5 * Math.PI / 4)
        {
            p.x = -1 * r;
            p.y = (float)(-1 * r * Math.Tan(angle % (Math.PI / 4)));
        }
        else if (angle >= 5 * Math.PI / 4 && angle < 3 * Math.PI / 2)
        {
            p.x = (float)(-1 * r * Math.Tan(3 * Math.PI / 2 - angle));
            p.y = -1 * r;
        }
        else if (angle >= 3 * Math.PI / 2 && angle < 7 * Math.PI / 4)
        {
            p.x = (float)(r * Math.Tan(angle % (Math.PI / 4)));
            p.y = -1 * r;
        }
        else
        {
            p.x = r;
            p.y = (float)(-1 * r * Math.Tan(2 * Math.PI - angle));
        }

        return p;
    }


    private void Update()
    {

        /*if (anim != null && anim.gameObject.activeSelf)
        {*/
            if (Running /*|| anim.GetFloat("VelX") > 0*/)
            {
                var currpos = new Vector2(transform.position.x, transform.position.z);
                float deltapos = Vector2.Distance(prevpos, currpos);
                if (deltapos != 0)
                {
                    Speed = deltapos / Time.deltaTime;
                    var deltaspeed = prevspeed - Speed;
                    Acceleration = deltaspeed / Time.deltaTime;
                }
                else
                {
                    Speed = 0;
                    Acceleration = 0;
                }
                prevspeed = Speed;
                prevpos = currpos;

                var blendTreeMovement = Vector2.zero;
                if (Running)
                {
                    var l = leftController.localRotation;

                    var r = rightController.localRotation;

                    var d = Quaternion.Slerp(l, r, .5f) * Vector3.forward;

                    var e = Camera.localRotation * Vector3.forward;

                    var angleA = Mathf.Atan2(d.x, d.z) * Mathf.Rad2Deg;
                    var angleB = Mathf.Atan2(e.x, e.z) * Mathf.Rad2Deg;

                    // get the signed difference in these angles
                    var a = Mathf.DeltaAngle(angleA, angleB);

                    a = a % 360;
                    if (a < 0)
                        a = 360 + a;

                    blendTreeMovement = calculatePointOnCircle(.1f, a);

            }

            float xVelocity = 0f, yVelocity = 0f;



            var mult = SpeedToRunCurve.Evaluate(Speed / MaxSpeed) * 1.0f;
                blendTreeMovement *= mult;

            if (lastblendTreeMovement != Vector2.zero)
                //Interpolate between the input axis from the last frame and the new input axis we calculated
                blendTreeMovement = new Vector2(Mathf.SmoothDamp(lastblendTreeMovement.x, blendTreeMovement.x, ref xVelocity, smoothTime), Mathf.SmoothDamp(lastblendTreeMovement.y, blendTreeMovement.y, ref yVelocity, smoothTime));

                // Update the Animator with our values so that the blend tree updates
                if(anim !=null)
            {
                anim.SetFloat("VelX", -blendTreeMovement.y);
                anim.SetFloat("VelZ", blendTreeMovement.x);
            }
                else
            {
               //Debug.Log("VelX: " + -blendTreeMovement.y + " VelZ: " + blendTreeMovement.x);
            }

                GrabLeft();
                GrabRight();

                lastblendTreeMovement = blendTreeMovement;
            }
            else
            {
                if (Controller.ItemLeft == null)
                    DropLeft();
                if (Controller.ItemRight == null)
                    DropRight();

                lastblendTreeMovement = Vector2.zero;
        }
        //Debug.Log("Speed: " + Speed + " Accel: " + Acceleration);
        //}
        /* END ROTATION */
    }

    private Vector2 calculatePointOnCircle(float r, float angleInDegrees)
    {
        double angle = (angleInDegrees % 360) * Math.PI / 180;
        var ray = new Ray2D(new Vector2(0, 0), new Vector2(Mathf.Cos((float)angle), Mathf.Sin((float)angle)));
        return ray.GetPoint(r);
    }

    private float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }

    internal void DropRight()
    {
        if (!Running)
            RightFisting = false;
    }

    internal void GrabRight()
    {
        RightFisting = true;
    }

    internal void DropLeft()
    {
        if (!Running)
            LeftFisting = false;
    }
    internal void Grab(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
            GrabLeft();
        else if (hand == ControllerHand.RightHand)
            GrabRight();
    }

    internal void GrabLeft()
    {
        LeftFisting = true;
    }

    internal void Drop(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
            DropLeft();
        else if (hand == ControllerHand.RightHand)
            DropRight();
    }
}