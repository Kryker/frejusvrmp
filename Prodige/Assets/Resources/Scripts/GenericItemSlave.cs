﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericItemSlave : MonoBehaviour
{
    //[HideInInspector]
    public GenericItemNet MasterNet;
    //[HideInInspector]
    public GenericItem Master;

    public void Interact(ItemControllerNet c, ControllerHand hand)
    {
        MasterNet.Interact(this, c, hand);
    }
    public void Interact(ItemController c, ControllerHand hand)
    {
        Master.Interact(this, c, hand);
    }

    public void ClickButton(object sender, ClickedEventArgs e)
    {
        if (Master != null)
            Master.ClickButton(this);
        else if (MasterNet != null)
            MasterNet.ClickButton(this);
    }

    public void UnClickButton(object sender, ClickedEventArgs e)
    {
        if (Master != null)
            Master.UnClickButton(this);
        else if (MasterNet != null)
            MasterNet.UnClickButton(this);
    }
    public void DisableOutline(ItemController c)
    {
        Master.DisableOutline(this, c);
    }
    public void EnableOutline(ItemController c)
    {
        Master.EnableOutline(this, c);
    }
    public void DisableOutline(ItemControllerNet c)
    {
        MasterNet.DisableOutline(this, c);
    }
    public void EnableOutline(ItemControllerNet c)
    {
        MasterNet.EnableOutline(this, c);
    }

    internal void Select()
    {
        if (MasterNet != null)
            MasterNet.Select(this);
    }

    internal void Unselect()
    {
        if (MasterNet != null)
            MasterNet.Unselect(this);
    }
}
