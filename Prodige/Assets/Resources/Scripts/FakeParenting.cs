﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeParenting : MonoBehaviour {
     
    Transform FakeParent;
    public Vector3 FakelocalPosition;
    Quaternion startParentRotationQ, StartRotationQ;
    public bool IgnoreFakelocalPosition = false;
    ItemControllerNet ic;

    public GameObject Genitore;

    // Use this for initialization
    void Start ()
    {
        if (Genitore != null)
        {
            this.SetFakeParent(Genitore.transform, FakelocalPosition);
        }
	}

    public void SetFakeParent(Transform Parent, PlayerStatusNet player)
    {
        FakeParent = Parent;
        if (Parent != null)
        {
            FakelocalPosition = player.SeatedOffset;
            StartRotationQ = transform.rotation;
            startParentRotationQ = Parent.rotation;
            if (player.Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                ic = player.ItemController;
                ic.DeferredRaycast = true;
            }
            }
        else
            {
            FakelocalPosition = Vector3.zero;
            StartRotationQ = Quaternion.identity;
            startParentRotationQ = Quaternion.identity;
            if (ic != null)
            {
                ic.DeferredRaycast = false;
                ic = null;
            }
        }
    }

   public void SetFakeParent(Transform Parent, Vector3 FakelocalPos)
    {
        FakeParent = Parent;
        if (Parent != null)
        {
            FakelocalPosition = FakelocalPos;
            StartRotationQ = transform.rotation;
            startParentRotationQ = Parent.rotation;
        }
        else
        {
            FakelocalPosition = Vector3.zero;
            StartRotationQ = Quaternion.identity;
            startParentRotationQ = Quaternion.identity;
            if (ic != null)
            {
                ic.DeferredRaycast = false;
                ic = null;
            }
        }
    }

    public void SetFakeParent(Transform Parent, Vector3 FakelocalPos, Quaternion rot)
    {
        FakeParent = Parent;
        if (Parent != null)
        {
            FakelocalPosition = FakelocalPos;
            StartRotationQ = rot;
            startParentRotationQ = Parent.rotation;
        }
        else
        {
            FakelocalPosition = Vector3.zero;
            StartRotationQ = Quaternion.identity;
            startParentRotationQ = Quaternion.identity;
            if (ic != null)
            {
                ic.DeferredRaycast = false;
                ic = null;
            }
        }
    }

    public void SetFakeParent(Transform Parent)
    {
        FakeParent = Parent;
        if (Parent != null)
        {

            StartRotationQ = transform.rotation;
            startParentRotationQ = Parent.rotation;
        }
        else
        {
            FakelocalPosition = Vector3.zero;
            StartRotationQ = Quaternion.identity;
            startParentRotationQ = Quaternion.identity;
            if (ic != null)
            {
                ic.DeferredRaycast = false;
                ic = null;
            }
        }
    }

    // Update is called once per frame
    [HideInInspector]
    public bool forceRotation = false;
    private void LateUpdate()
    {
        Matrix4x4 parentMatrix;
        if (FakeParent != null)
        {
            parentMatrix = Matrix4x4.TRS(FakeParent.position, FakeParent.rotation, FakeParent.lossyScale);
            if(!IgnoreFakelocalPosition)
                transform.position = parentMatrix.MultiplyPoint3x4(FakelocalPosition);
            if(forceRotation)
            {
                transform.rotation = FakeParent.rotation * StartRotationQ;

            }
            else
            {
                transform.rotation = (FakeParent.rotation * Quaternion.Inverse(startParentRotationQ)) * StartRotationQ;

            }
            if (ic != null)
                ic.DoDeferredRaycast();
        }
    }
}
