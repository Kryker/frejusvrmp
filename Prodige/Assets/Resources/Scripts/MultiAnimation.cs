﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
public class MultiAnimation : MonoBehaviour {
    Animation Animation;
    public List<string> AnimationClips;
    public bool PlayAutomatically = false;
	// Use this for initialization
	void Start () {
        Animation = GetComponent<Animation>();
        if(PlayAutomatically)
            PlayAllClips();
    }
	
    public void PlayAllClips()
    {
        Animation.Play(AnimationClips[0]);
        for (int i = 1; i < AnimationClips.Count; i++)
        {
            Animation[AnimationClips[i]].layer = i;
            Animation.Play(AnimationClips[i]);
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
