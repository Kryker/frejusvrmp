﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(VibrationController))]
public class ControllerManager : MonoBehaviour
{

    public ControllerHand Hand;
    [HideInInspector]
    public VRItemController Controller;
    [HideInInspector]
    public ItemControllerNet ControllerNet;
    [HideInInspector]
    public OfflineItemController OfflineController;
    [HideInInspector]
    public VibrationController vibrationController;
    // Use this for initialization
    void Awake()
    {
        vibrationController = GetComponent<VibrationController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public NetworkInstanceId GetControllerNetId()
    {
        if (ControllerNet != null)
            return ControllerNet.netId;
        else
            return NetworkInstanceId.Invalid;
    }

    public void StartPulsePublic(ControllerButtonByFunc buttonToInteract)
    {
        if(Controller != null)
            Controller.StartPulsePublic(buttonToInteract, Hand);
        else if (ControllerNet != null)
            ControllerNet.StartPulsePublic(buttonToInteract, Hand);
        else if (OfflineController != null)
            OfflineController.StartPulsePublic(buttonToInteract, Hand);
    }

    public void StopPulsePublic()
    {
        if (Controller != null)
            Controller.StopPulsePublic(Hand);
        else if (ControllerNet != null)
            ControllerNet.StopPulsePublic(Hand);
        else if (OfflineController != null)
            OfflineController.StopPulsePublic();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Controller != null)
            Controller.ManageTriggerEnter(other, Hand);
        else if (ControllerNet != null)
            ControllerNet.ManageTriggerEnter(other, Hand);
    }

    /*private void OnTriggerStay(Collider other)
    {
        if (Controller != null)
            Controller.ManageTriggerEnter(other, Hand);
        else if (ControllerNet != null)
            ControllerNet.ManageTriggerEnter(other, Hand);
    }*/

    private void OnTriggerExit(Collider other)
    {
        if (Controller != null)
            Controller.ManageTriggerExit(other, Hand);
        else if (ControllerNet != null)
            ControllerNet.ManageTriggerExit(other, Hand);
    }

    public void StartPulsePublic(ControllerButton cbutton)
    {
        if (Controller != null)
            Controller.StartPulsePublic(cbutton, Hand);
        else if (ControllerNet != null)
            ControllerNet.StartPulsePublic(cbutton, Hand);
        else if (OfflineController != null)
            OfflineController.StartPulsePublic(cbutton, Hand);
    }

    public void DropItem(Transform t)
    {
        if (Controller != null)
            Controller.DropItem(t, false);
        else if (ControllerNet != null)
            ControllerNet.DropItem(t, false);
    }

    internal void ForceTriggerExit(Collider other)
    {
        OnTriggerExit(other);
    }
}
