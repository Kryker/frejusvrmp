﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelCharacteristics : MonoBehaviour
{

    public uint ID;
    public string Name;
    public float Height;
    public Transform LeftUpLeg, RightUpLeg, LeftShoulder, RightShoulder, LeftHand, RightHand;
    public RectTransform HealthBar;
    public AudioClip Scream, Cough;
}
