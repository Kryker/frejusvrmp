﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chiavi : GenericItem {
    
    public Vector3 KeysRotationAction;
    Quaternion KeysRotationRest;
    public List<AudioClip> clips;
    public AudioSource _carEngineSound;
    StageController SC;
    [HideInInspector]
    public VehicleData Vehicle;
    Coroutine KeysOperating;

    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.Keys;
        if(Slave != null)
            KeysRotationRest = Slave.transform.localRotation;
        else
            KeysRotationRest = transform.localRotation;
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if(tc != null)
            SC = tc.GetComponent<StageController>();
        Vehicle = GetComponent<VehicleData>();
        _carEngineSound.clip = clips[1];
        _carEngineSound.loop = true;
        _carEngineSound.Play();
    }

    public override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {

    }
    
    IEnumerator StopEngine()
    {
        _carEngineSound.Stop();
        _carEngineSound.clip = clips[2];
        _carEngineSound.loop = false;
        _carEngineSound.Play();
        Vehicle.radio.TurnOff();
        Vehicle.TurnOff();
        yield return new WaitUntil(() => !_carEngineSound.isPlaying);
        KeysOperating = null;
    }

    IEnumerator StartEngine()
    {
        _carEngineSound.Stop();
        _carEngineSound.clip = clips[0];
        _carEngineSound.loop = false;
        _carEngineSound.Play();
        Vehicle.radio.TurnOn();
        Vehicle.TurnOn();
        yield return new WaitUntil(() => !_carEngineSound.isPlaying);       
        _carEngineSound.clip = clips[1];
        _carEngineSound.loop = true;
        _carEngineSound.Play();
        KeysOperating = null;
    }

    void TurnKey()
    {
        if(Slave == null)
            transform.localRotation = Quaternion.Euler(KeysRotationAction);
        else
            Slave.transform.localRotation = Quaternion.Euler(KeysRotationAction);
    }

    void TurnKeyBack()
    {
        if (Slave == null)
            transform.localRotation = KeysRotationRest;
        else
            Slave.transform.localRotation = KeysRotationRest;
    }
    public override void Interact(ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
        {         
            if (ItemActive)
            {
                TurnKeyBack();
                if (KeysOperating != null)
                    StopCoroutine(KeysOperating);
                KeysOperating = StartCoroutine(StartEngine());
                ItemActive = false;
                SC.EngineOff = false;
            }
            else
            {
                TurnKey();
                if (KeysOperating != null)
                    StopCoroutine(KeysOperating);
                KeysOperating = StartCoroutine(StopEngine());
                ItemActive = true;
                SC.EngineOff = true;
            }
        }
    }
    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            if (ItemActive)
            {
                TurnKeyBack();
                if (KeysOperating != null)
                    StopCoroutine(KeysOperating);
                StartCoroutine(StartEngine());
                ItemActive = false;
                SC.EngineOff = false;
            }
            else
            {
                TurnKey();
                if (KeysOperating != null)
                    StopCoroutine(KeysOperating);
                KeysOperating = StartCoroutine(StopEngine());
                ItemActive = true;
                SC.EngineOff = true;
            }
        }
    }
    public override void Reset()
    {
        TurnKeyBack();
        ItemActive = false;
        SC.EngineOff = false;
        _carEngineSound.Stop();
        _carEngineSound.clip = clips[1];
        _carEngineSound.loop = true;
        _carEngineSound.Play();
        base.Reset();
    }

}
