﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using UnityEngine.UI;
using System;
using System.Linq;

public class LocalizerController : MonoBehaviour {
    
    public SystemLanguage SelectedLanguage
    {
        get { return _selectedLanguage; }
        set {
            if (_selectedLanguage != value)
                localized = false;
            _selectedLanguage = value;
            if (GameManager.Instance != null)
                GameManager.Instance.AppLanguage = value;
            }
    }
    private SystemLanguage _selectedLanguage = SystemLanguage.English;
    public List<TextAsset> LangFiles;
    List<LanguageElement> LoadedLanguages;
    [HideInInspector]
    public List<LocalizationID> LocalizedObjects;
    bool loaded = false;
    bool localized = false;

    // Use this for initialization
    void Awake () {
        LoadLangFiles();
        LocalizedObjects = new List<LocalizationID>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    [XmlRoot(ElementName = "UIElement")]
    public class UIElement
    {
        [XmlElement(ElementName = "ID")]
        public string ID { get; set; }
        [XmlElement(ElementName = "Text")]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "UIElements")]
    public class UIElements
    {
        [XmlElement(ElementName = "UIElement")]
        public List<UIElement> UIelements { get; set; }

        public static UIElements Load(TextAsset t)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(UIElements));
                using (Stream stream = GenerateStreamFromString(t.text))
                {
                    return serializer.Deserialize(stream) as UIElements;
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError("Exception loading language file: " + e);

                return null;
            }
        }
    }

    public void ApplyLanguage(LocalizationID l)
    {
        if (!loaded)
            LoadLangFiles();
        switch (SelectedLanguage)
        {
            case SystemLanguage.Italian:
                foreach (LanguageElement le in LoadedLanguages)
                    if (le.language == "Italian")
                            {                        
                            var id = l.ID;
                            var t = l.transform;
                            foreach (var elem in le.languagedata.UIelements)
                            {
                                if (elem.ID == id.ToString())
                                {
                                    t.GetComponent<Text>().text = elem.Text;
                                    break;
                                }
                            }
                    }
                return;
            case SystemLanguage.French:
                foreach (LanguageElement le in LoadedLanguages)
                    if (le.language == "French")
                            {
                            var id = l.ID;
                            var t = l.transform;
                            foreach (var elem in le.languagedata.UIelements)
                            {
                                if (elem.ID == id.ToString())
                                {
                                    t.GetComponent<Text>().text = elem.Text;
                                    break;
                                }
                            }
                    }
                return;
            default:
                foreach (LanguageElement le in LoadedLanguages)
                    if (le.language == "English")
                    {
                            var id = l.ID;
                            var t = l.transform;
                            foreach (var elem in le.languagedata.UIelements)
                            {
                                if (elem.ID == id.ToString())
                                {
                                    t.GetComponent<Text>().text = elem.Text;
                                    break;
                                }
                            }
                    }
                return;
        }
    }

    public void  ApplyLanguage()
    {
        if (!loaded)
            LoadLangFiles();
        ApplyLanguage(SelectedLanguage);
    }

    void ApplyLanguage(SystemLanguage l)
    {
        if (!loaded)
            LoadLangFiles();
        switch (l)
        {
            case SystemLanguage.Italian:
                foreach (LanguageElement le in LoadedLanguages)
                    if (le.language == "Italian")
                    {
                        foreach (var x in LocalizedObjects)
                        {
                            var id = x.ID;
                            var t = x.transform;
                            foreach (var elem in le.languagedata.UIelements)
                            {
                                if (elem.ID == id.ToString())
                                {
                                    t.GetComponent<Text>().text = elem.Text;
                                    break;
                                }
                            }
                        }
                    }
                localized = true;
                return;
            case SystemLanguage.French:
                foreach (LanguageElement le in LoadedLanguages)
                    if (le.language == "French")
                    {
                        foreach (var x in LocalizedObjects)
                        {
                            var id = x.ID;
                            var t = x.transform;
                            foreach (var elem in le.languagedata.UIelements)
                            {
                                if (elem.ID == id.ToString())
                                {
                                    t.GetComponent<Text>().text = elem.Text;
                                    break;
                                }
                            }
                        }
                    }
                localized = true;
                return;
            default:
                foreach (LanguageElement le in LoadedLanguages)
                    if (le.language == "English")
                    {
                        foreach (var x in LocalizedObjects)
                        {
                            var id = x.ID;
                            var t = x.transform;
                            foreach (var elem in le.languagedata.UIelements)
                            {
                                if (elem.ID == id.ToString())
                                {
                                    t.GetComponent<Text>().text = elem.Text;
                                    break;
                                }
                            }
                        }
                    }
                localized = true;
                return;
        }
    }


    public static MemoryStream GenerateStreamFromString(string value)
    {
        return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
    }

    public struct LanguageElement
    {
        public string language;
        public UIElements languagedata;
    }

    public bool Localized
    {
        get { return localized; }
        private set { localized = value; }
    }

    void LoadLangFiles()
    {
        if (!loaded)
        {
            LoadedLanguages = new List<LanguageElement>();
            foreach (TextAsset t in LangFiles)
            {
                var l = UIElements.Load(t);
                LanguageElement x = new LanguageElement();
                x.language = t.name;
                x.languagedata = l;
                LoadedLanguages.Add(x);
            }
            loaded = true;
        }
    }

    internal void Reset()
    {
        localized = false;
    }
}
