﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonitorStop : MonoBehaviour {

    Material MonitorOff, SemaforoOff;
    public Transform Monitor, SemaforoDX, SemaforoSX;
    public Material SemaforoOn;
    MeshRenderer MonitorMR, SemaforoDXMR, SemaforoSXMR;
    public bool EnabledFromStart = false;
    bool _itemActive = false;
    bool left = true;
    float tictime = 0.45f;
    float nexttic = 0;
    bool initialized = false;
    // Use this for initialization
    void Start () {
        Initialize();
    }
    private void Initialize()
    {
        if(!initialized)
        {
            SemaforoDXMR = SemaforoDX.GetComponent<MeshRenderer>();
            SemaforoSXMR = SemaforoSX.GetComponent<MeshRenderer>();
            MonitorMR = Monitor.GetComponent<MeshRenderer>();
            SemaforoOff = SemaforoDXMR.materials[0];
            MonitorOff = MonitorMR.materials[0];
            if (EnabledFromStart)
                EnableSign(true);
            initialized = true;
        }
    }
    // Update is called once per frame
    void Update () {
        var now = Time.time;
        if (_itemActive)
        {
            if (nexttic <= now)
            {
                if (!left)
                {
                    SignLeft();
                    nexttic = now + tictime;
                }
                else
                {
                    SignRight();
                    nexttic = now + tictime;
                }
            }
        }
    }

    void SignLeft()
    {
        var m1 = new Material[1];
        m1[0] = SemaforoOn;
        SemaforoSXMR.materials = m1;
        m1 = new Material[1];
        m1[0] = SemaforoOff;
        SemaforoDXMR.materials = m1;
        left = true;
    }

    void SignRight()
    {
        var m1 = new Material[1];
        m1[0] = SemaforoOn;
        SemaforoDXMR.materials = m1;
        m1 = new Material[1];
        m1[0] = SemaforoOff;
        SemaforoSXMR.materials = m1;
        left = false;
    }

    public void EnableSign(bool sel)
    {
        Initialize();
        if (sel)
            {
            _itemActive = true;
            var newmat = new Material(MonitorOff);
            newmat.EnableKeyword("_EMISSION");
            var m = new Material[1];
            m[0] = newmat;
            MonitorMR.materials = m;
            }
        else
        {
            _itemActive = false;
            var m = new Material[1];
            m[0] = MonitorOff;
            MonitorMR.materials = m;
            m = new Material[1];
            m[0] = SemaforoOff;
            SemaforoDXMR.materials = m;
            m = new Material[1];
            m[0] = SemaforoOff;
            SemaforoSXMR.materials = m;
        }
    }

    public bool ItemActive()
    {
        return _itemActive;
    }
}
