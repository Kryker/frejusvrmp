﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerStatusCollider : MonoBehaviour {
    public PlayerStatus Status;
    //Coroutine lastroutine;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ApplyDamageWithPain(float damage)
    {
        /*lastroutine = */StartCoroutine(Status.ApplyDamageWithPain(damage));
    }

    public void ApplyDamageWithoutPain(float damage)
    {
        /*lastroutine = */StartCoroutine(Status.ApplyDamageWithoutPain(damage));
    }
}
