﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTweaker : MonoBehaviour
{

    public SteamVR_TrackedController Left, Right;
    SteamVR_Controller.Device LeftPad, RightPad;
    public MainMenu menu;
    // Use this for initialization
    void Start()
    {

    }
    private void OnEnable()
    {
        if(GameManager.Instance.DebugMode)
        {
            Left.PadClicked += LeftPadClicked;
            Right.PadClicked += RightPadClicked;
        }
    }

    private void OnDisable()
    {
        if (GameManager.Instance.DebugMode)
        {
            Left.PadClicked -= LeftPadClicked;
            Right.PadClicked -= RightPadClicked;
        }
    }

    private void RightPadClicked(object sender, ClickedEventArgs e)
    {
        var x = RightPad.GetAxis().x;
        var y = RightPad.GetAxis().y;

        if (y >= 0)
            ScaleDown();
        else
            ScaleUp();
        /*if ((x >= 0 && x < y) || (x < 0 && x >= -y))
            Down
        else if ((x < 0 && x >= y) || (x >= 0 && -x >= -y))
            Up
        else if ((y >= 0 && y < x) || (y < 0 && y >= -x))
            Left
        else if ((y < 0 && y >= x) || (y >= 0 && y >= x))
            Right*/
    }

    private void LeftPadClicked(object sender, ClickedEventArgs e)
    {
        var x = LeftPad.GetAxis().x;
        var y = LeftPad.GetAxis().y;
        if (y >= 0)
            ArmScaleUp();
        else
            ArmScaleDown();
        /*if ((x >= 0 && x < y) || (x < 0 && x >= -y))
            Up
        else if ((x < 0 && x >= y) || (x >= 0 && -x >= -y))
            Down
        else if ((y >= 0 && y < x) || (y < 0 && y >= -x))
            Left
        else if ((y < 0 && y >= x) || (y >= 0 && y >= x))
            Right*/
    }



    // Update is called once per frame
    void Update()
    {
        LeftPad = SteamVR_Controller.Input((int)Left.controllerIndex);
        RightPad = SteamVR_Controller.Input((int)Right.controllerIndex);

        if (Input.GetKeyDown(KeyCode.UpArrow))
            ScaleUp();
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            ScaleDown();
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            ArmScaleDown();
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            ArmScaleUp();
    }

    void ScaleUp()
    {
        if (GameManager.Instance.PlayerHeight < 2f)
        {
            var s = GameManager.Instance.PlayerHeight += 0.05f;
            if (s > 2.5f)
                GameManager.Instance.PlayerHeight = 2f;
            else
                GameManager.Instance.PlayerHeight = s;
            var scale = menu.Armature.GetComponent<ModelCharacteristics>().Height / GameManager.Instance.PlayerHeight;
            menu.Armature.localScale = new Vector3(scale, scale, scale);
        }
    }
    void ScaleDown()
    {
        if (GameManager.Instance.PlayerHeight > .5f)
        {
            var s = GameManager.Instance.PlayerHeight -= 0.05f;
            if (s < .5f)
                GameManager.Instance.PlayerHeight = .5f;
            else
                GameManager.Instance.PlayerHeight = s;
            var scale = menu.Armature.GetComponent<ModelCharacteristics>().Height / GameManager.Instance.PlayerHeight;
            menu.Armature.localScale = new Vector3(scale, scale, scale);
        }
    }

    void ArmScaleUp()
    {
        {
            if (GameManager.Instance.PlayerArmScale < 1)
            {
                var s = GameManager.Instance.PlayerArmScale += 0.05f;
                if (s > 1)
                    GameManager.Instance.PlayerArmScale = 1;
                else
                    GameManager.Instance.PlayerArmScale = s;
                menu.LeftArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
                menu.RightArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);

            }
        }
    }

    void ArmScaleDown()
    {
        if (GameManager.Instance.PlayerArmScale > .5f)
        {
            var s = GameManager.Instance.PlayerArmScale -= 0.05f;
            if (s < .5f)
                GameManager.Instance.PlayerArmScale = .5f;
            else
                GameManager.Instance.PlayerArmScale = s;
            menu.LeftArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
            menu.RightArm.localScale = new Vector3(GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale, GameManager.Instance.PlayerArmScale);
        }
    }

}
