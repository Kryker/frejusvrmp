﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistancePlane : MonoBehaviour {

    public bool Front = true;
    public float CullingDistance = 1000.0f;
    PlayerStatusNet pc;
	// Use this for initialization
	void Start () {
            //pc = GlobalControl.Instance.TCN.GetPlayerController();
	}
	
	// Update is called once per frame
	void Update () {
       
        if(GameManager.Instance != null && GameManager.Instance.TCN != null && pc == null)
            pc = GameManager.Instance.TCN.GetPlayerController();
        else if (pc != null)
        { 
            var player = pc.transform.position;
            if (Front)
                transform.position = new Vector3(transform.position.x, transform.position.y, player.z + CullingDistance);
            else
                transform.position = new Vector3(transform.position.x, transform.position.y, player.z - CullingDistance);
        }
	}
}
