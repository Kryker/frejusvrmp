﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTel : MonoBehaviour {

    public TelefonoSOS telefono;
#if MULTIPLAYER
    public TelefonoSOSNet telefononet;
#endif
    //bool outlined = false;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        {
            var s = other.transform.parent.GetComponent<GrabbableItemSlave>();
            if (s != null && s.Code() == ItemCodes.SOSTelephone)
                {
                    if (telefono != null)
                        telefono.OnBase = true;
#if MULTIPLAYER
                else if (telefononet != null)
                        telefononet.OnBase = true;
#endif
                EnableOutline();
                }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.parent != null)
        {
            var s = other.transform.parent.GetComponent<GrabbableItemSlave>();
#if MULTIPLAYER
            if (s != null && s.Code() == ItemCodes.SOSTelephone && ((telefono != null && !telefono.OnBase) || (telefononet != null && !telefononet.OnBase)))
            {
                if (telefono != null)
                    telefono.OnBase = true;
                else if (telefononet != null)
                    telefononet.OnBase = true;
                EnableOutline();
            }
#else
            if (s != null && s.Code() == ItemCodes.SOSTelephone && ((telefono != null && !telefono.OnBase)))
            {
                if (telefono != null)
                    telefono.OnBase = true;
                EnableOutline();
            }
#endif
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent != null)
        {
            var s = other.transform.parent.GetComponent<GrabbableItemSlave>();
            if (s != null && s.Code() == ItemCodes.SOSTelephone)
            {
                if (telefono != null)
                    telefono.OnBase = false;
#if MULTIPLAYER
                else if (telefononet != null)
                    telefononet.OnBase = false;
#endif
                DisableOutline();
            }
        }
    }


    public virtual void EnableOutline()
    {
        var mts = GetComponent<MeshRenderer>().materials;
        Color c;
        foreach (Material m in mts)
        {
            c = m.GetColor("_OutlineColor");
            c.a = 255;
            m.SetColor("_OutlineColor", c);
        }       
        //outlined = true;
    }

    public virtual void DisableOutline()
    {
        var mts = GetComponent<MeshRenderer>().materials;
        Color c;
        foreach (Material m in mts)
        {
            c = m.GetColor("_OutlineColor");
            c.a = 0;
            m.SetColor("_OutlineColor", c);
        }      
        //outlined = false;
    }

}
