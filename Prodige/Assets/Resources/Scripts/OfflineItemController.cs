﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfflineItemController : MonoBehaviour {
    public Transform LeftController, RightController;

    public AudioSource BlipSource;
    [HideInInspector]
    public AudioSource LeftItemSource, RightItemSource;
    bool leftpulsing;
    bool rightpulsing;
    List<MeshRenderer> LGrips = new List<MeshRenderer>();
    List<MeshRenderer> LPads = new List<MeshRenderer>();
    List<MeshRenderer> LTriggers = new List<MeshRenderer>();
    List<MeshRenderer> RGrips = new List<MeshRenderer>();
    List<MeshRenderer> RPads = new List<MeshRenderer>();
    List<MeshRenderer> RTriggers = new List<MeshRenderer>();
    Material PadMaterial, GripMaterial, TriggerMaterial;
    Material oldmat;
    public Shader Outline;
    ControllerButton LeftButtonToPulse = ControllerButton.Nothing;

    internal void StartPulsePublic(ControllerButtonByFunc button, ControllerHand hand)
    {
        if(hand == ControllerHand.LeftHand)
        {
            switch (button)
            {
                case ControllerButtonByFunc.ButtonToInteract:
                    LeftExternalButtonToPulse = GetButton(ButtonToInteract);
                    break;
                /*case ControllerButtonByFunc.ButtonToDrop:
                    LeftExternalButtonToPulse = GetButton(ButtonToDrop);
                    break;
                case ControllerButtonByFunc.ButtonToUse:
                    LeftExternalButtonToPulse = GetButton(ButtonToUse);
                    break;*/
                case ControllerButtonByFunc.All:
                    LeftExternalButtonToPulse = ControllerButton.All;
                    break;
                default:
                    LeftExternalButtonToPulse = ControllerButton.Nothing;
                    break;
            }
            if (LeftButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.LeftHand);
        }
        else if (hand == ControllerHand.RightHand)
        {
            switch (button)
            {
                case ControllerButtonByFunc.ButtonToInteract:
                    RightExternalButtonToPulse = GetButton(ButtonToInteract);
                    break;
                /*case ControllerButtonByFunc.ButtonToDrop:
                    RightExternalButtonToPulse = GetButton(ButtonToDrop);
                    break;
                case ControllerButtonByFunc.ButtonToUse:
                    ExternalButtonToPulse = GetButton(ButtonToUse);
                    break;*/
                case ControllerButtonByFunc.All:
                    RightExternalButtonToPulse = ControllerButton.All;
                    break;
                default:
                    RightExternalButtonToPulse = ControllerButton.Nothing;
                    break;
            }
            if (RightButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.RightHand);
        }
    }

    ControllerButton RightButtonToPulse = ControllerButton.Nothing;
    ControllerButton LeftExternalButtonToPulse = ControllerButton.Nothing;
    ControllerButton RightExternalButtonToPulse = ControllerButton.Nothing;
    public float PulseTime = 0.5f;
    public Color PadColor = Color.yellow;
    public Color TriggerColor = Color.magenta;
    public Color GripColor = Color.cyan;
    public ControllerButtonInput ButtonToInteract = ControllerButtonInput.Trigger;

    // Use this for initialization
    public virtual void Start()
    {
        BlipSource = GetComponent<AudioSource>();
        RightItemSource = RightController.GetComponent<AudioSource>();
        LeftItemSource = LeftController.GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update() {

    }
    private void OnEnable()
    {
        var c = GetComponents<ItemController>();
        foreach (ItemController i in c)
            i.enabled = false;
        var cn = GetComponents<ItemControllerNet>();
        foreach (ItemControllerNet i in cn)
            i.enabled = false;
        var cl = LeftController.GetComponent<ControllerManager>();
        if (cl != null)
        {
            cl.OfflineController = this;
            cl.Controller = null;
            cl.ControllerNet = null;
        }
        var cr = LeftController.GetComponent<ControllerManager>();
        if (cr != null)
        {
            cr.OfflineController = this;
            cr.Controller = null;
            cr.ControllerNet = null;
        }
    }
    public void StartPulsePublic(ControllerButtonByFunc button)
    {
        switch (button)
        {
            case ControllerButtonByFunc.ButtonToInteract:
                LeftExternalButtonToPulse = GetButton(ButtonToInteract);
                RightExternalButtonToPulse = GetButton(ButtonToInteract);
                break;
            /*case ControllerButtonByFunc.ButtonToDrop:
                LeftExternalButtonToPulse = GetButton(ButtonToDrop);
                RightExternalButtonToPulse = GetButton(ButtonToDrop);
                break;
            case ControllerButtonByFunc.ButtonToUse:
                LeftExternalButtonToPulse = GetButton(ButtonToUse);
                RightExternalButtonToPulse = GetButton(ButtonToUse);
                break;*/
            case ControllerButtonByFunc.All:
                LeftExternalButtonToPulse = ControllerButton.All;
                RightExternalButtonToPulse = ControllerButton.All;
                break;
            default:
                LeftExternalButtonToPulse = ControllerButton.Nothing;
                RightExternalButtonToPulse = ControllerButton.Nothing;
                break;
        }
        if (LeftButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.LeftHand);
        if (RightButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.RightHand);
    }

    internal void StartPulsePublic(ControllerButton cbutton, ControllerHand hand)
    {
        throw new NotImplementedException();
    }

    private ControllerButton GetButton(ControllerButtonInput buttonToUse)
    {
        switch (buttonToUse)
        {
            case ControllerButtonInput.Trigger:
                return ControllerButton.Trigger;
            case ControllerButtonInput.RGrip:
                return ControllerButton.RGrip;
            case ControllerButtonInput.LGrip:
                return ControllerButton.LGrip;
            case ControllerButtonInput.Grip:
                return ControllerButton.Grip;
            case ControllerButtonInput.Pad:
                return ControllerButton.Pad;
            case ControllerButtonInput.All:
                return ControllerButton.All;
            default:
                return ControllerButton.Nothing;
        }
    }

    void StartPulse(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            if (leftpulsing || (LeftButtonToPulse == ControllerButton.Nothing && LeftExternalButtonToPulse == ControllerButton.Nothing))
                return;

            MeshRenderer r = null;
            /*if (ExternalButtonToPulse != ControllerButton.Nothing && ButtonToPulse != ExternalButtonToPulse)
                StartPulse(ExternalButtonToPulse);
            else if (ExternalButtonToPulse == ControllerButton.Nothing)
                StopPulse();*/

            //ButtonToPulse = button;
            ControllerButton button = ControllerButton.Nothing;
            if (LeftButtonToPulse != ControllerButton.Nothing)
                button = LeftButtonToPulse;
            else
                button = LeftExternalButtonToPulse;

            var c = LeftController.Find("Model");
            Transform t = null;
            switch (button)
            {
                case ControllerButton.RGrip:
                    t = c.Find("rgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    break;
                case ControllerButton.LGrip:
                    t = c.Find("lgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    break;
                case ControllerButton.Grip:
                    t = c.Find("rgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    t = c.Find("lgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    t = c.Find("handgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    break;
                case ControllerButton.Pad:
                    t = c.Find("trackpad");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LPads.Add(r);
                    }
					else
					{
                    t = c.Find("thumbstick");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LPads.Add(r);
                    }
					}
                    break;
                case ControllerButton.Trigger:
                    t = c.Find("trigger");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LTriggers.Add(r);
                    }
                    break;
                case ControllerButton.All:

                    t = c.Find("trackpad");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LPads.Add(r);
                    }
					else
					{
                    t = c.Find("thumbstick");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LPads.Add(r);
                    }
					}
                    t = c.Find("trigger");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LTriggers.Add(r);
                    }
                    t = c.Find("rgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    t = c.Find("lgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    t = c.Find("handgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            LGrips.Add(r);
                    }
                    break;
                default:
                    return;
            }
            if (oldmat == null)
            {
                if (LTriggers.Count > 0)
                    oldmat = LTriggers[0].materials[0];
                else if (LPads.Count > 0)
                    oldmat = LPads[0].materials[0];
                else if (LGrips.Count > 0)
                    oldmat = LGrips[0].materials[0];
                else
                {
                    var model = transform.Find("Model");
                    if (model != null)
                    {
                        var mr = model.GetComponent<MeshRenderer>();
                        if (mr != null)
                            oldmat = mr.materials[0];
                    }
                }
            }

            if (PadMaterial == null)
            {
                PadMaterial = new Material(Outline);
                PadMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                PadMaterial.SetColor("_OutlineColor", PadColor);
                PadMaterial.SetFloat("_Outline", 0.03f);
            }
            if (GripMaterial == null)
            {
                GripMaterial = new Material(Outline);
                GripMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                GripMaterial.SetColor("_OutlineColor", GripColor);
                GripMaterial.SetFloat("_Outline", 0.03f);
            }
            if (TriggerMaterial == null)
            {
                TriggerMaterial = new Material(Outline);
                TriggerMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                TriggerMaterial.SetColor("_OutlineColor", TriggerColor);
                TriggerMaterial.SetFloat("_Outline", 0.03f);
            }
            foreach (MeshRenderer mr in LPads)
            {
                var m = new Material[1];
                m[0] = PadMaterial;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in LTriggers)
            {
                var m = new Material[1];
                m[0] = TriggerMaterial;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in LGrips)
            {
                var m = new Material[1];
                m[0] = GripMaterial;
                mr.materials = m;
            }
            leftpulsing = true;
        }
        else if (hand == ControllerHand.RightHand)
        {
            if (rightpulsing || (RightButtonToPulse == ControllerButton.Nothing && RightExternalButtonToPulse == ControllerButton.Nothing))
                return;

            MeshRenderer r = null;
            /*if (ExternalButtonToPulse != ControllerButton.Nothing && ButtonToPulse != ExternalButtonToPulse)
                StartPulse(ExternalButtonToPulse);
            else if (ExternalButtonToPulse == ControllerButton.Nothing)
                StopPulse();*/

            //ButtonToPulse = button;
            ControllerButton button = ControllerButton.Nothing;
            if (RightButtonToPulse != ControllerButton.Nothing)
                button = RightButtonToPulse;
            else
                button = RightExternalButtonToPulse;

            var c = RightController.Find("Model");
            Transform t = null;
            switch (button)
            {
                case ControllerButton.RGrip:
                    t = c.Find("rgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    break;
                case ControllerButton.LGrip:
                    t = c.Find("lgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    break;
                case ControllerButton.Grip:
                    t = c.Find("rgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    t = c.Find("lgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    t = c.Find("handgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    break;
                case ControllerButton.Pad:
                    t = c.Find("trackpad");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RPads.Add(r);
                    }
					else
					{
                    t = c.Find("thumbstick");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RPads.Add(r);
                    }
					}
                    break;
                case ControllerButton.Trigger:
                    t = c.Find("trigger");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RTriggers.Add(r);
                    }
                    break;
                case ControllerButton.All:

                    t = c.Find("trackpad");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RPads.Add(r);
                    }
					else
					{
                    t = c.Find("thumbstick");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            if (r != null)
                            RPads.Add(r);
                    }
					}
                    t = c.Find("trigger");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RTriggers.Add(r);
                    }
                    t = c.Find("rgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    t = c.Find("lgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    t = c.Find("handgrip");
                    if (t != null)
                    {
                        r = t.GetComponent<MeshRenderer>();
                        if (r != null)
                            RGrips.Add(r);
                    }
                    break;
                default:
                    return;
            }
            if (oldmat == null)
            {
                if (RTriggers.Count > 0)
                    oldmat = RTriggers[0].materials[0];
                else if (RPads.Count > 0)
                    oldmat = RPads[0].materials[0];
                else if (RGrips.Count > 0)
                    oldmat = RGrips[0].materials[0];
                else
                {
                    var model = transform.Find("Model");
                    if (model != null)
                    {
                        var mr = model.GetComponent<MeshRenderer>();
                        if (mr != null)
                            oldmat = mr.materials[0];
                    }
                }
            }

            if (PadMaterial == null)
            {
                PadMaterial = new Material(Outline);
                PadMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                PadMaterial.SetColor("_OutlineColor", PadColor);
                PadMaterial.SetFloat("_Outline", 0.03f);
            }
            if (GripMaterial == null)
            {
                GripMaterial = new Material(Outline);
                GripMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                GripMaterial.SetColor("_OutlineColor", GripColor);
                GripMaterial.SetFloat("_Outline", 0.03f);
            }
            if (TriggerMaterial == null)
            {
                TriggerMaterial = new Material(Outline);
                TriggerMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                TriggerMaterial.SetColor("_OutlineColor", TriggerColor);
                TriggerMaterial.SetFloat("_Outline", 0.03f);
            }
            foreach (MeshRenderer mr in RPads)
            {
                var m = new Material[1];
                m[0] = PadMaterial;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in RTriggers)
            {
                var m = new Material[1];
                m[0] = TriggerMaterial;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in RGrips)
            {
                var m = new Material[1];
                m[0] = GripMaterial;
                mr.materials = m;
            }
            rightpulsing = true;
        }
    }

    public void StartPulsePublic(ControllerButton button)
    {
        LeftExternalButtonToPulse = button;
        RightExternalButtonToPulse = button;
        if (LeftButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.LeftHand);
        if (RightButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.RightHand);
    }

    void StopPulse(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            if (!leftpulsing)
                return;
            if (LeftExternalButtonToPulse != ControllerButton.Nothing && LeftExternalButtonToPulse == LeftButtonToPulse)
            {
                LeftButtonToPulse = ControllerButton.Nothing;
                return;
            }

            /*var c = transform.
             * ("Model");
            var t = c.FindChild("trigger");*/
            foreach (MeshRenderer mr in LTriggers)
            {
                var m = new Material[1];
                m[0] = oldmat;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in LGrips)
            {
                var m = new Material[1];
                m[0] = oldmat;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in LPads)
            {
                var m = new Material[1];
                m[0] = oldmat;
                mr.materials = m;
            }
            LTriggers.Clear();
            LPads.Clear();
            LGrips.Clear();
        }
        LeftButtonToPulse = ControllerButton.Nothing;
        leftpulsing = false;
        if (LeftExternalButtonToPulse != ControllerButton.Nothing)
            StartPulse(ControllerHand.LeftHand);

        else if (hand == ControllerHand.RightHand)
        {
            if (!rightpulsing)
                return;
            if (RightExternalButtonToPulse != ControllerButton.Nothing && RightExternalButtonToPulse == RightButtonToPulse)
            {
                RightButtonToPulse = ControllerButton.Nothing;
                return;
            }

            /*var c = transform.FindChild("Model");
            var t = c.FindChild("trigger");*/
            foreach (MeshRenderer mr in RTriggers)
            {
                var m = new Material[1];
                m[0] = oldmat;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in RGrips)
            {
                var m = new Material[1];
                m[0] = oldmat;
                mr.materials = m;
            }
            foreach (MeshRenderer mr in RPads)
            {
                var m = new Material[1];
                m[0] = oldmat;
                mr.materials = m;
            }
            RTriggers.Clear();
            RPads.Clear();
            RGrips.Clear();
        }
        RightButtonToPulse = ControllerButton.Nothing;
        rightpulsing = false;

        if (RightExternalButtonToPulse != ControllerButton.Nothing)
            StartPulse(ControllerHand.RightHand);

    }
    

    private void StartPulse(ControllerButton button, ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            if (button == ControllerButton.Nothing)
                return;
            if (!leftpulsing)
            {
                LeftButtonToPulse = button;
                StartPulse(hand);
            }
            else if (button != LeftButtonToPulse)
            {
                StopPulse();
                LeftButtonToPulse = button;
                StartPulse(hand);
            }
        }
        else if (hand == ControllerHand.RightHand)
        {
            if (button == ControllerButton.Nothing)
                return;
            if (!rightpulsing)
            {
                RightButtonToPulse = button;
                StartPulse(hand);
            }
            else if (button != RightButtonToPulse)
            {
                StopPulse();
                RightButtonToPulse = button;
                StartPulse(hand);
            }
        }
    }

    void StopPulse()
    {
        StopPulse(ControllerHand.LeftHand);
        StopPulse(ControllerHand.RightHand);
    }


    public void StopPulsePublic()
    {
        LeftExternalButtonToPulse = ControllerButton.Nothing;
        RightExternalButtonToPulse = ControllerButton.Nothing;
        ControllerButton button = ControllerButton.Nothing;
        if (LeftButtonToPulse != ControllerButton.Nothing)
        {
            button = LeftButtonToPulse;
            StopPulse(ControllerHand.LeftHand);
            StartPulse(button, ControllerHand.LeftHand);
        }
        else
            StopPulse(ControllerHand.LeftHand);
        if (RightButtonToPulse != ControllerButton.Nothing)
        {
            button = RightButtonToPulse;
            StopPulse(ControllerHand.RightHand);
            StartPulse(button, ControllerHand.RightHand);
        }
        else
            StopPulse();
    }
}
