﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BrochureEffect : MonoBehaviour {

    AudioSource source;
    // Use this for initialization
    void Awake () {
        source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        if (source != null)
            source.Play();
    }
    public void EnableBrochure()
    {
        gameObject.SetActive(true);
    }

    public void DisableBrochure()
    {
        if(gameObject.activeSelf)
            StartCoroutine(PlayAndDisable());
    }

    private IEnumerator PlayAndDisable()
    {
        transform.GetChild(0).gameObject.SetActive(false);
        if (source != null)
        {
            source.Play();
            yield return new WaitWhile(() => source.isPlaying);
        }
        gameObject.SetActive(false);
        transform.GetChild(0).gameObject.SetActive(true);
    }
}
