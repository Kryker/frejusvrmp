﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armadietto : GenericItem
{
    public GenericItemSlave AntaSinistra, AntaDestra;
    public BoxCollider Interno;
    public List<Transform> ContainedItems;
    Animator animator;
    public List<AudioClip> clips;
    Anta Sinistra, Destra;
    AdvancedStateMachineBehaviour AntaOpen, AntaClose;

    public class Anta
    {
        public GenericItemSlave anta;
        public AudioSource Source;
        public bool toplay = false;
    }


    // Use this for initialization
    public override void Start()
    {
        base.Start();
    }
    private void Awake()
    {
        ItemCode = ItemCodes.Locker;
        if (Slave != null)
            animator = Slave.GetComponent<Animator>();
        else
            animator = GetComponent<Animator>();
        Sinistra = new Anta();
        Destra = new Anta();
        Sinistra.anta = AntaSinistra;
        AntaSinistra.Master = this;
        Destra.anta = AntaDestra;
        AntaDestra.Master = this;
        Sinistra.Source = AntaSinistra.GetComponent<AudioSource>();
        Destra.Source = AntaDestra.GetComponent<AudioSource>();

        //List<Transform> itemstoremove = new List<Transform>();

        /*foreach (Transform t in ContainedItems)
        {
            if (Interno.bounds.Intersects(t.GetComponent<Collider>().bounds))
            {
                ContainedItems.Add(t);
                itemstoremove.Add(t);
            }
        }
        foreach (Transform t in itemstoremove)
            ContainedItems.Remove(t);*/
        MakeAllItemsUngrabbable();

        var advancedbehaviours = animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            var layername = "AnteLayer";
            if (ad.Layer == layername)
            {
                if (ad.StateName == "Closed")
                {
                    AntaClose = ad;
                }
                else if (ad.StateName == "Opened")
                {
                    AntaOpen = ad;
                }
            }
        }

        AntaOpen.StateEnter += DoorOpen;
        AntaClose.StateEnter += DoorClose;
        AntaClose.StatePlayed += DoorClosed;
        AntaOpen.StatePlayed += AntaOpened;
        AntaClose.StatePlayed += AntaClosed;
        AntaOpen.StatePlayed += AntaOpened;
        AntaClose.StatePlayed += AntaClosed;
    }

    public void AntaOpened(AdvancedStateMachineBehaviour st)
    {
        SetCanInteract(true);
        ItemActive = true;
    }

    internal void PlayClip(int v, Anta a)
    {
        StartCoroutine(PlayAudio(clips[v], a));
    }
    IEnumerator PlayAudio(AudioClip clip, Anta a)
    {
        yield return new WaitUntil(() => !a.toplay);
        if (a.Source.isPlaying)
        {
            a.toplay = true;
            yield return new WaitWhile(() => a.Source.isPlaying);
        }
        a.Source.clip = clip;
        a.Source.Play();
        yield return new WaitWhile(() => a.Source.isPlaying);
        a.toplay = false;
    }

    public void AntaClosed(AdvancedStateMachineBehaviour st)
    {
        SetCanInteract(true);
        ItemActive = false;
    }


    void MakeItemUngrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItem>();
        if (g != null)
        {
            g.SetCanInteract(false);
            g.DisablePhysics();
        }
    }

    public void DoorOpen(AdvancedStateMachineBehaviour st)
    {
        PlayClip(0, Sinistra);
        PlayClip(0, Destra);
        MakeAllItemsGrabbable();
    }

    public void DoorClosed(AdvancedStateMachineBehaviour st)
    {
        MakeAllItemsUngrabbable();
    }

    public void DoorClose(AdvancedStateMachineBehaviour st)
    {
        PlayClip(0, Sinistra);
        PlayClip(0, Destra);
    }


    void MakeItemGrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItem>();
        if (g != null)
        {
            g.SetCanInteract(true);
            g.EnablePhysics();
        }
    }

    public void MakeAllItemsGrabbable()
    {
        foreach (Transform item in ContainedItems)
            MakeItemGrabbable(item);
    }

    public void MakeAllItemsUngrabbable()
    {
        foreach (Transform item in ContainedItems)
            MakeItemUngrabbable(item);
    }


    private void OnTriggerEnter(Collider other)
    {
        var i = other.GetComponent<GenericItem>();
        if (i != null && i.Grabbable)
            if (Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other.transform))
            {
                ContainedItems.Add(other.transform);
                return;
            }
    }

    private void OnTriggerExit(Collider other)
    {
        var i = other.GetComponent<GenericItem>();
        if (i != null && i.Grabbable)
            if (ContainedItems.Contains(other.transform))
            {
                ContainedItems.Remove(other.transform);
                return;
            }
    }
    // Update is called once per frame
    public override void Update()
    {

    }

    /*public void Interact(AntaArmadietto a)
    {
        if (!a.ItemActive)
        {
            animator.SetBool("OpenAnte", true);
        }
        else
        {
            animator.SetBool("OpenAnte", false);
        }
    }*/
    public override void ClickButton(object sender)
    {
        if (Sinistra.anta == (GenericItemSlave)sender)
            Sinistra.Source.Play();
        else if (Destra.anta == (GenericItemSlave)sender)
            Destra.Source.Play();
    }

    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        if (!ItemActive)
        {
            animator.SetBool("OpenAnte", true);
        }
        else
        {
            animator.SetBool("OpenAnte", false);
        }
    }

    public override void EnableOutline(ItemController c)
    {
        Color col;
        Material[] mts = null;

        var mr = AntaSinistra.GetComponent<MeshRenderer>();
        if (mr != null)
            mts = mr.materials;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 255;
                m.SetColor("_OutlineColor", col);
            }

            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 255;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }

        mr = AntaDestra.GetComponent<MeshRenderer>();
        if (mr != null)
            mts = mr.materials;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 255;
                m.SetColor("_OutlineColor", col);
            }

            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 255;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = true;
    }

    public override void DisableOutline(ItemController c)
    {
        Color col;
        Material[] mts = null;
        var mr = AntaSinistra.GetComponent<MeshRenderer>();
        if (mr != null)
            mts = mr.materials;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 0;
                m.SetColor("_OutlineColor", col);
            }
            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 0;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        mr = AntaDestra.GetComponent<MeshRenderer>();
        if (mr != null)
            mts = mr.materials;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 0;
                m.SetColor("_OutlineColor", col);
            }
            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 0;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = false;
    }

    public override void Reset()
    {
        //CloseShutter();
        ItemActive = false;
        //ContainedItems.Clear();
        base.Reset();
    }
}
