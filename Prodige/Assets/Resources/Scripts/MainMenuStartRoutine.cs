﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MainMenuStartRoutine : MonoBehaviour {

    public List<Transform> FPSControllerChildren, HTCViveControllerChildren;
    public MainMenu menu;
	// Use this for initialization
	void Start () {
        if (!UnityEngine.VR.VRDevice.isPresent)
        {
            foreach (Transform t in FPSControllerChildren)
                t.gameObject.SetActive(true);
            transform.position = new Vector3(transform.position.x, 1.01f, transform.position.z);
            GetComponent<CharacterController>().center = new Vector3(0, 0, 0);
            GetComponent<NavMeshObstacle>().center = new Vector3(0, 0, 0);
            GetComponent<CVirtDeviceController>().enabled = false;
            GetComponent<CVirtPlayerController>().enabled = false;
            GetComponent<MouseLook>().enabled = true;
            //GetComponent<StopMoving>().enabled = true;
            //GetComponent<CharacterMotor>().enabled = true;
            GetComponent<FPSInputController>().enabled = true;
        }
        else
        {
            foreach (Transform t in HTCViveControllerChildren)
                t.gameObject.SetActive(true);
            GetComponent<CharacterControllerColliderManager>().enabled = true;
        }

    }
	
}
