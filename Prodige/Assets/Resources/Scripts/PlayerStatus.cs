﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayerStatus : MonoBehaviour
{

    [Range(0.0f, 100.0f)]
    public float health = 100.0f;
    TunnelController TC;
    StageController SC;
    public AudioSource Mouth;
    bool Intoxication;
    public bool NPC = false;
    float IntoxicationEnd = float.MinValue;
    float NextOperation = float.MinValue;
    float IntoxicationTime = 4.0f;
    float IntoxicationAmount = 1.0f;
    float fogamount = 0;
    float t = 0;
    public Animator NPCAnim;
    [HideInInspector]
    public Collider MouthCollider;
    public AudioClip Cough, Scream, Heartbeat_1, Heartbeat_2, Heartbeat_3, Heartbeat_4;
    public AudioSource Heart;
    float LastDamage = float.MinValue;
    bool damaging = false;
    public VehicleSeat CurrentSeat = null;
    public VehicleData CurrentVehicle = null;
    public Vector3 SeatedOffset;
    public ItemController ItemController;

    // Use this for initialization
    void Start()
    {
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
        {
            TC = tc.transform.GetComponent<TunnelController>();
            SC = tc.transform.GetComponent<StageController>();
        }
        if (Mouth != null)
            MouthCollider = Mouth.GetComponent<SphereCollider>();
        Mouth.playOnAwake = false;
    }

    /*private void OnDisable()
        {
        if(Head!=null)
            TC.GetTriggerModuleManager().RemoveActor(Head);
        }

    private void OnDestroy()
    {
        if (Head != null)
            TC.GetTriggerModuleManager().RemoveActor(Head);
    }*/

    /*public void SetHead(Collider c)
        {
        if(TC == null)
            {
            var tc = GameObject.FindGameObjectWithTag("TunnelController").transform;
            TC = tc.GetComponent<TunnelController>();
            }
        if(Head == null || Head != c)
            {
            if(Head!=null)
                TC.GetTriggerModuleManager().RemoveActor(Head);
            Head = c;
            TC.GetTriggerModuleManager().AddActor(c);
            }
        }*/

    public bool IsDead()
    {
        if (health > 0)
            return false;
        return true;
    }

    public Collider GetHead()
    {
        return MouthCollider;
    }

    // Update is called once per frame
    void Update()
    {
        if (health == 0)
            Die();
        else
        {
            bool operationtime = false;
            var d = Time.time;
            if (d > NextOperation)
                operationtime = true;

            /*if (!NPC)
            {
                if (SC != null && operationtime)
                    if (SC.CarLeft == false && TC.MainCarFire.IsOnFire())
                    {
                        if (TC.CarHandle.CanInteract())
                            TC.CarHandle.CanInteract(false);
                        //ApplyDamageWithPain(20);
                    }
            }*/

            if (Intoxication && d >= IntoxicationEnd)
            {
                Intoxication = false;
                if (!NPC)
                    fogamount = RenderSettings.fogDensity;
                else
                {
                    NPCAnim.SetBool("Cough", false);
                }
                t = 0;
                IntoxicationAmount = 0.0f;
                Mouth.loop = false;
                //Debug.Log("Intoxication stopped at time " + IntoxicationEnd);
            }
            else if (Intoxication && operationtime)
            {
                if (GetComponent<PlayerStatusNet>().Ruolo != SpawnMode.Componente)
                {
                    StartCoroutine(ApplyDamageWithoutPain(IntoxicationAmount * 5));
                    //Debug.Log("Intoxication damage applied: " + IntoxicationAmount * 5 + " at time "+NextOperation);
                    if (!Mouth.isPlaying)
                    {
                        Mouth.clip = Cough;
                        Mouth.loop = true;
                        Mouth.Play();
                    }
                }
              
                NextOperation = d + 1;
            }
            else if (health < 100 && d > LastDamage + 10 && operationtime)
            {
                Recover(d);
            }

            if (!NPC && (Intoxication && RenderSettings.fogDensity < 1 || !Intoxication && RenderSettings.fogDensity > 0))
                UpdateFog();

        }
    }

    private void UpdateFog()
    {
        if (Intoxication)
        {
            t += Time.deltaTime / 2;
            RenderSettings.fogDensity = Mathf.Lerp(fogamount, 1, t);
        }
        else
        {
            t += Time.deltaTime / 5;
            RenderSettings.fogDensity = Mathf.Lerp(fogamount, 0, t);
        }
    }

    public IEnumerator ApplyDamageWithPain(float damage)
    {
        if (damage == 0)
            yield break;
        yield return new WaitWhile(() => damaging);
        damaging = true;
        if (!IsDead())
        {
            ApplyDamage(damage);
            if (Mouth.isPlaying && Mouth.clip != Scream)
            {
                Mouth.Stop();
                Mouth.loop = false;
                Mouth.clip = Scream;
                Mouth.Play();
            }
            else if (!Mouth.isPlaying)
            {
                Mouth.clip = Scream;
                Mouth.loop = false;
                Mouth.Play();
            }
        }
        damaging = false;
    }

    public IEnumerator ApplyDamageWithoutPain(float damage)
    {
        if (damage == 0)
            yield break;
        yield return new WaitWhile(() => damaging);
        damaging = true;
        if (!IsDead())
            ApplyDamage(damage);
        damaging = false;
    }


    public void Reset()
    {
        health = 100.0f;
        Intoxication = false;
        if (!NPC)
            RenderSettings.fogDensity = 0;
        t = 0;
        IntoxicationAmount = 0.0f;
        IntoxicationEnd = float.MinValue;
        NextOperation = float.MinValue;
        Mouth.loop = false;
        if (!NPC)
        {
            Heart.volume = 0;
            Heart.clip = Heartbeat_1;
            Heart.Play();
        }
        else
        {
            NPCAnim.SetBool("Cough", false);
        }
    }

    void ApplyDamage(float damage)
    {
        if (IsDead() || damage == 0)
            return;
        if (damage < 0)
            damage = -damage;
        health -= damage;
        if (health < 0)
            health = 0;
        if (!NPC)
        {
            if (health == 100)
            {
                if (Heart.clip != Heartbeat_1)
                    Heart.clip = Heartbeat_1;
                Heart.volume = 0;
            }
            else if (health < 100 && health >= 50.0f)
            {
                if (Heart.clip != Heartbeat_1)
                    Heart.clip = Heartbeat_1;
                Heart.volume = 1 - ((health - 50) / 50);
                Heart.Play();
            }
            if (health < 50.0f && health >= 25.0f && Heart.clip != Heartbeat_2)
            {
                Heart.clip = Heartbeat_2;
                Heart.volume = 1;
                Heart.Play();
            }
            else if (health < 25.0f && health >= 10.0f && Heart.clip != Heartbeat_3)
            {
                Heart.clip = Heartbeat_3;
                Heart.volume = 1;
                Heart.Play();
            }
            else if (health < 10.0f && health > 0.0f && Heart.clip != Heartbeat_4)
            {
                Heart.clip = Heartbeat_4;
                Heart.volume = 1;
                Heart.Play();
            }
        }
        LastDamage = Time.time;
    }

    public void Die()
    {
        if (!NPC && IsDead())
        {
            if (GameManager.Instance != null)
                SC.LoseGame();
            else
                SceneManager.LoadScene(TC.MainMenuScene);
        }
        else
        {
            NPCAnim.SetBool("Cough", false);
            NPCAnim.SetBool("Idle", false);
            NPCAnim.SetBool("Walk", false);
            NPCAnim.SetBool("Run", false);
            NPCAnim.SetBool("Die", true);
            NPCAnim.GetComponentInParent<NavMeshAgent>().isStopped = true;
            health = 0;
        }
    }

    public void Recover(float time)
    {
        if (!IsDead())
        {
            health += 1;
            if (!NPC)
            {
                if (health >= 50.0f)
                {
                    if (Heart.clip != Heartbeat_1)
                        Heart.clip = Heartbeat_1;
                    Heart.volume = 1 - 100 / health;
                }
                if (health < 50.0f && health >= 25.0f && Heart.clip != Heartbeat_2)
                {
                    Heart.clip = Heartbeat_2;
                    Heart.volume = 1;
                }
                else if (health < 25.0f && health >= 10.0f && Heart.clip != Heartbeat_3)
                {
                    Heart.clip = Heartbeat_3;
                    Heart.volume = 1;
                }
                else if (health < 10.0f && health > 0.0f && Heart.clip != Heartbeat_4)
                {
                    Heart.clip = Heartbeat_4;
                    Heart.volume = 1;
                }
            }
            if (health > 100)
            {
                health = 100;
                NextOperation = float.MinValue;
            }
            else
                NextOperation = time + 1;
        }
    }

    public void Intoxicate(float toxicity)
    {
        if (!IsDead())
        {
            if (!Intoxication)
            {
                Intoxication = true;
                if (!NPC)
                    fogamount = RenderSettings.fogDensity;
                else
                {
                    NPCAnim.SetBool("Cough", true);
                }
                t = 0;
                IntoxicationAmount = toxicity;
                IntoxicationEnd = Time.time + IntoxicationTime;
                if (!Mouth.isPlaying)
                {
                    Mouth.clip = Cough;
                    Mouth.loop = true;
                    Mouth.Play();
                }
            }
            else
            {
                if (toxicity > IntoxicationAmount)
                    IntoxicationAmount = toxicity;
                IntoxicationEnd = Time.time + IntoxicationTime;
            }
        }
    }

    internal void CopyStatus(PlayerStatus playerStatus)
    {
        if (playerStatus != null)
        {
            health = playerStatus.health;
            Intoxication = playerStatus.Intoxication;
            IntoxicationAmount = playerStatus.IntoxicationAmount;
            IntoxicationEnd = playerStatus.IntoxicationEnd;
            IntoxicationTime = playerStatus.IntoxicationTime;
            NextOperation = playerStatus.NextOperation;
            fogamount = playerStatus.fogamount;
            LastDamage = playerStatus.LastDamage;
        }
    }
}
