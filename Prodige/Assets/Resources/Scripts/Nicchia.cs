﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//public enum NicheType { Tel, Ext };
public class Nicchia : GenericItem {

    //Quaternion StartRot;
    //public float ShutterRotationAngle;
    //public Transform FireExtL, FireExtR, Telefono;
    public List<Transform> ContainedItems;
    [HideInInspector]
    public BoxCollider Interno;
    public Animator animator;
    //VanoNicchia Vano;
    //public PulsanteChiamataSOS Pulsante;
    public List<AudioClip> clips;
    public int Index;
    public NicheType tipo;
    public Posizione posizione;

    /*public class VanoNicchia
    {*/
    //public ManigliaNicchia Anta;
    //public GenericItemSlave Anta;
    /*public */
    public AudioSource Source;
        //public BoxCollider Interno;
        //public bool CanInteract;
        /*public */AdvancedStateMachineBehaviour AntaOpen, AntaClose;
        //public List<Transform> ContainedItems = new List<Transform>();
        /*public */AdvancedStateMachineBehaviour SerraturaOpen, SerraturaClose;
        //public PulsanteChiamataSOS Pulsante;
        //public int Index = 0;
        /*public */bool toplay = false;
    //}

    //public float rotationtime = 0.01f;
    //public Transform MeshNicchia;
    //MeshRenderer mr;

    // Use this for initialization
    public override void Start () {
        base.Start();
	}

    public override void ClickButton(object sender)
    {
        Source.Play();
    }

    private void Awake()
    {
        ItemCode = ItemCodes.Niche;
        Interno = animator.GetComponents<BoxCollider>()[Index];
        //StartRot = transform.localRotation;
        //Armadietti = new List<VanoNicchia>();
        /*for (int i = 0; i < Ante.Count; i++)
        {*/
            //var a = new VanoNicchia();
            //a.Anta = Ante[i];
            //a.Interno = ColliderAnte[i];
            //a.Index = i;
            //a.CanInteract = CanInteracts[i];
            /*if (a.Interno.bounds.Intersects(Telefono.GetComponent<Collider>().bounds))
            {
                a.ContainedItems.Add(Telefono);
                a.Pulsante = Pulsante;
                a.Pulsante.CanInteract(false);
                MakeAllItemsUngrabbable(a);
            }
            else if (a.Interno.bounds.Intersects(FireExtL.GetComponent<Collider>().bounds) && a.Interno.bounds.Intersects(FireExtR.GetComponent<Collider>().bounds))
            {
                a.ContainedItems.Add(FireExtL);;
                a.ContainedItems.Add(FireExtR);
                MakeAllItemsUngrabbable(a);
            }
            Armadietti.Add(a);
        }*/
        
        var advancedbehaviours = animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach(AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            /*for (int i = 0; i < Armadietti.Count; i++)
            {*/
                var layername = "Anta" + /*Armadietti[i].*/Index + "Layer";
                if (ad.Layer == layername)
                {
                    if (ad.StateName == "Closed")
                    {
                        /*Armadietti[i].*/AntaClose = ad;
                        //break;
                    }
                    else if (ad.StateName == "Opened")
                    {
                        /*Armadietti[i].*/AntaOpen = ad;
                        //break;
                    }
                }
                else
                {
                    layername = "Serratura" + /*Armadietti[i].*/Index + "Layer";
                    if (ad.Layer == layername)
                    {
                        if (ad.StateName == "Closed")
                        {
                            /*Armadietti[i].*/SerraturaClose = ad;
                            //break;
                        }
                        else if (ad.StateName == "Opened")
                        {
                            /*Armadietti[i].*/SerraturaOpen = ad;
                            //break;
                        }
                    }
                //}
            }
           /* if (ad.StateName == "EstClosed")
                Armadietti[1].antaclosed = ad;
            else if(ad.StateName == "TelClosed")
                Armadietti[0].antaclosed = ad;
            else if (ad.StateName == "EstOpened")
                Armadietti[1].antaopen = ad;
            else if (ad.StateName == "TelOpened")
                Armadietti[0].antaopen = ad;*/
        }
        /*Armadietti[1].*/AntaOpen.StateEnter += /*Ante[1].*/DoorOpen;
        //Armadietti[0].AntaOpen.StateEnter += Ante[0].DoorOpen;
        /*Armadietti[1].*/AntaOpen.StatePlayed += /*Ante[1].*/DoorOpened;
        //Armadietti[0].AntaOpen.StatePlayed += Ante[0].DoorOpened;
        /*Armadietti[1].*/SerraturaOpen.StatePercentagePlayed += /*Ante[1].*/LockOpened;
        //Armadietti[0].SerraturaOpen.StatePercentagePlayed += Ante[0].LockOpened;
        /*Armadietti[1].SerraturaOpen.StateEnter += Ante[1].LockOpen;
        Armadietti[0].SerraturaOpen.StateEnter += Ante[0].LockOpen;*/
        /*Armadietti[1].*/AntaClose.StatePercentagePlayed += /*Ante[1].*/DoorClosed;
        //Armadietti[0].AntaClose.StatePercentagePlayed += Ante[0].DoorClosed;
        /*Armadietti[1].*/AntaClose.StateEnter += /*Ante[1].*/DoorClose;
        //Armadietti[0].AntaClose.StateEnter += Ante[0].DoorClose;
        /*Armadietti[1].*/SerraturaClose.StatePlayed += /*Ante[1].*/LockClosed;
        //Armadietti[0].SerraturaClose.StatePlayed += Ante[0].LockClosed;
        /*Armadietti[1].SerraturaClose.StateEnter += Ante[1].LockClose;
        Armadietti[0].SerraturaClose.StateEnter += Ante[0].LockClose;*/

        /*if (ItemActive)
            OpenShutter();*/
        //mr = transform.GetComponent<MeshRenderer>();
    }

    internal void PlayClip(int v)
    {
        StartCoroutine(PlayAudio(clips[v]));
    }
    /*public override void ClickButton(object sender, ClickedEventArgs e)
    {
        audiosource.Play();
    }*/

    public override void UnClickButton(object sendere)
    {
    }

    IEnumerator PlayAudio(AudioClip clip)
    {
        yield return new WaitUntil(() => !toplay);
        if (Source.isPlaying)
        {
            toplay = true;
            yield return new WaitWhile(() => Source.isPlaying);
        }
        Source.clip = clip;
        Source.Play();
        yield return new WaitWhile(() => Source.isPlaying);
        toplay = false;
    }

    internal void DoorOpened(AdvancedStateMachineBehaviour st)
    {
        SetCanInteract(true);
        ItemActive = true;
    }

    internal void LockOpened(AdvancedStateMachineBehaviour st)
    {
        OpenDoor();
        MakeAllItemsGrabbable();
    }

    internal void DoorClosed(AdvancedStateMachineBehaviour st)
    {
        MakeAllItemsUngrabbable();
        CloseLock();
    }

    internal void LockClosed(AdvancedStateMachineBehaviour st)
    {
        SetCanInteract(true);
        ItemActive = false;
    }
    
    internal void DoorOpen(AdvancedStateMachineBehaviour a)
    {
        PlayClip(0);
    }


    internal void DoorClose(AdvancedStateMachineBehaviour a)
    {
        PlayClip(0);
    }

    internal void LockClose(AdvancedStateMachineBehaviour a)
    {
        PlayClip(1);
    }

    internal void LockOpen(AdvancedStateMachineBehaviour a)
    {
        PlayClip(1);
    }

    internal void OpenDoor()
    {
        animator.SetBool("OpenAnta" + Index, true);
    }

    internal void CloseLock()
    {
        animator.SetBool("OpenSerratura" + Index, false);
    }


    /*void MakeExtUngrabbable()
    {
            FireExtL.GetComponent<Extinguisher>().CanInteract(false);
            FireExtL.GetComponent<Rigidbody>().isKinematic = true;
            FireExtR.GetComponent<Extinguisher>().CanInteract(false);
            FireExtR.GetComponent<Rigidbody>().isKinematic = true;
    }*/

    void MakeItemUngrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItem>();
        if(g!=null)
        {
            if (g.Grabbable && g.ItemCode != ItemCodes.SOSTelephone)
                g.DisablePhysics();
            g.SetCanInteract(false);
        }
    }

   /* void MakeAllItemsUngrabbableEst()
    {
        foreach (Transform item in ContainedItemsEst)
            MakeItemUngrabbable(item);
    }
    
    void MakeAllItemsUngrabbableTel()
    {
        foreach (Transform item in ContainedItemsTel)
            MakeItemUngrabbable(item);
    }*/

    void MakeItemGrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItem>();
        if (g != null)
        {
            g.SetCanInteract(true);
            if (g.Grabbable && g.ItemCode != ItemCodes.SOSTelephone)
                g.EnablePhysics();
        }
    }

    /*void MakeAllItemsGrabbableEst()
    {
        foreach (Transform item in ContainedItemsEst)
            MakeItemGrabbable(item);
    }
    */

    public void MakeAllItemsGrabbable()
    {
        foreach (Transform item in ContainedItems)
            MakeItemGrabbable(item);
        /*if (a.Pulsante != null)
        {
            Pulsante.gameObject.GetComponent<PulsanteChiamataSOS>().CanInteract(true);
        }*/
    }

    public void MakeAllItemsUngrabbable()
    {
        foreach (Transform item in ContainedItems)
            MakeItemUngrabbable(item);
        /*if (a.Pulsante != null)
        {
            Pulsante.gameObject.GetComponent<PulsanteChiamataSOS>().CanInteract(false);
        }*/
    }

   /* void MakeAllItemsGrabbableTel()
    {
        foreach (Transform item in ContainedItemsTel)
            MakeItemGrabbable(item);
    }*/

    /*void OpenShutter()
    {
        CanInteract(false);
        //StartCoroutine(OpenDoor());
    }*/

    /*void CloseShutter()
    {
        CanInteract(false);
        //StartCoroutine(CloseDoor());
    }*/
    
    /*void MakeExtGrabbable()
    {
        FireExtL.gameObject.GetComponent<Extinguisher>().CanInteract(true);
        FireExtL.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        FireExtR.gameObject.GetComponent<Extinguisher>().CanInteract(true);
        FireExtR.gameObject.GetComponent<Rigidbody>().isKinematic = false;
    }*/

    /*IEnumerator OpenDoor()
    {
        if (StartRot.eulerAngles.y > ShutterRotationAngle)
        {
            while (transform.localEulerAngles.y >= ShutterRotationAngle)
            {
                transform.Rotate(Vector3.up, -5, Space.World);
                yield return new WaitForSeconds(rotationtime);
            }
        }
        else if (StartRot.eulerAngles.y < ShutterRotationAngle)
        {
            while (transform.localEulerAngles.y <= ShutterRotationAngle)
            {
                transform.Rotate(Vector3.up, 5, Space.World);
                yield return new WaitForSeconds(rotationtime);
            }
        }
        transform.localEulerAngles = new Vector3(StartRot.eulerAngles.x, ShutterRotationAngle, StartRot.eulerAngles.z);
                
        CanInteract(true);
        ItemActive = true;
        MakeAllItemsGrabbable();
    }*/

    private void OnTriggerEnter(Collider other)
    {
        var i = other.GetComponent<GenericItem>();
        if (i != null && i.Grabbable)
            /*if(Armadietti != null)
                foreach(VanoNicchia a in Armadietti)
                {*/
                    if (Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other.transform))
                    {
                        ContainedItems.Add(other.transform);
                        return;
                    }
                //}
    }

    private void OnTriggerExit(Collider other)
    {
        var i = other.GetComponent<GenericItem>();
        if (i != null && i.Grabbable)
            /*foreach (VanoNicchia a in Armadietti)
            {*/
                if (ContainedItems.Contains(other.transform))
                {
                    ContainedItems.Remove(other.transform);
                    return;
                }
            //}
    }

    /* IEnumerator CloseDoor()
     {
         if (StartRot.eulerAngles.y > ShutterRotationAngle)
         {
             while (transform.localEulerAngles.y <= StartRot.eulerAngles.y)
             {
                 transform.Rotate(Vector3.up, 5, Space.World);
                 yield return new WaitForSeconds(0.01f);
             }
         }
         else if (StartRot.eulerAngles.y < ShutterRotationAngle)
         {
             while (transform.localEulerAngles.y >= StartRot.eulerAngles.y)
             {
                 transform.Rotate(Vector3.up, -5, Space.World);
                 yield return new WaitForSeconds(0.01f);
             }
         }
         transform.localEulerAngles = StartRot.eulerAngles;

         MakeAllItemsUngrabbable();
         CanInteract(true);
         ItemActive = false;
     }*/

    // Update is called once per frame
    public override void Update()
    {

    }

    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {           
        if (!ItemActive)
        {
        animator.SetBool("OpenSerratura"+Index, true);
        }
        else
        {
        animator.SetBool("OpenAnta" + Index, false);
        }
    }
    

    public override void Reset()
    {
        //CloseShutter();
        //ItemActive = false;
        /*Armadietti[0].ContainedItems.Clear();
        Armadietti[1].ContainedItems.Clear();*/
        /*ContainedItemsEst.Clear();
        ContainedItemsTel.Clear();*/
        /*FireExtL.GetComponent<Extinguisher>().Reset();
        FireExtR.GetComponent<Extinguisher>().Reset();
        Pulsante.Reset();
        Telefono.GetComponent<TelefonoSOS>().Reset();*/
        //MakeExtUngrabbable();
        /*Armadietti[1].ContainedItems.Add(FireExtL);
        Armadietti[1].ContainedItems.Add(FireExtR);
        Armadietti[0].ContainedItems.Add(Telefono);*/
        base.Reset();
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {}
}
