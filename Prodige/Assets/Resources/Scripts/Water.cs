﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Water : MonoBehaviour
    {
    public float damagepp = 0;
    public float force = 100;
    public float CoolDownCoefficient = 0.05f;

    ParticleSystem part;
    List<ParticleCollisionEvent> collisionEvents;

    public void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void OnParticleCollision(GameObject other)
        {

        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        int i = 0;
        while (i < numCollisionEvents)
        {
            /*
            if(collisionEvents[i].colliderComponent.name.Contains("Ruota"))
            {
                Debug.Log("Ruota");
            }*/
            var t = collisionEvents[i].colliderComponent.gameObject.GetComponent<CanBeOnFireSlave>();
            if (t != null)
            {
                t.Cooldown(CoolDownCoefficient);
            }else
            {
                var tester = collisionEvents[i].colliderComponent.gameObject.GetComponent<PunteggioAltezzaIdrante>();
                if(tester != null)
                {
                    tester.Colpito();
                }
            }
            i++;
        }
            
       

        /*
        if (transform.parent != null && transform.parent.parent != null && transform.parent.parent.GetComponent<CanBeOnFire>() != null)
        {
            if (other.transform == transform.parent.parent || transform.parent.parent.GetComponent<CanBeOnFire>().SubPieces.Contains(other.transform))
                return;
        }
        */

        //Rigidbody body = other.GetComponent<Rigidbody>();
        //other.SendMessage("Cooldown", CoolDownCoefficient, SendMessageOptions.DontRequireReceiver);
        /*
        var t = other.transform.parent.GetComponent< CanBeOnFireNet > ();
        if(t != null)
        {
            t.SendMessage("CoolDown", CoolDownCoefficient, SendMessageOptions.DontRequireReceiver);
            if(t.name == "Camion")
            {
                Debug.Log("test");
            }
        }
        */
        /*
        if (body)
            {
            Vector3 direction = other.transform.position - transform.position;
            direction = direction.normalized;
            body.AddForce(direction * force);
            }
            */
        return;
        }
    }