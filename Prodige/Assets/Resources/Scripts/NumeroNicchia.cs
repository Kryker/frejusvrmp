using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumeroNicchia : MonoBehaviour
{
	public enum Posizione { Destra, Sinistra};
	public Posizione PosizioneArmadietto;
	public TunnelTelephoneSectionControllerNet telefono;
	Text txt;
	int num;

	public void Start()
	{
		//telefono = transform.parent.parent.parent.GetComponentInChildren<TunnelTelephoneSectionControllerNet>();

		if(PosizioneArmadietto == Posizione.Destra)
			num = telefono.NumNicchiaDX;
		else
			num = telefono.NumNicchiaSX;

		txt = GetComponentInChildren<Text>();
		if (telefono != null)
		{
			txt.text = num.ToString();
		}
	}
}
