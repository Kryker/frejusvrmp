﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ArmadiettoNet : GenericItemNet {
    public GenericItemSlave AntaSinistra, AntaDestra;
    public BoxCollider Interno;
    public List<Collider> ContainedItems;
    NetworkAnimator nanimator;
    public List<AudioClip> clips;
    Anta Sinistra, Destra;
    AdvancedStateMachineBehaviour AntaOpen, AntaClose;
    [SyncVar]
    bool opened;

    public class Anta
    {
        public GenericItemSlave anta;
        public AudioSource Source;
        public bool toplay = false;
    }
    

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (isServer)
            opened = false;
        else if (opened)
            nanimator.animator.SetBool("OpenAnte", true);

        for (int i = 0; i < nanimator.animator.parameterCount; i++)
        {
            nanimator.SetParameterAutoSend(i, true);
        }
        var advancedbehaviours = nanimator.animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            var layername = "AnteLayer";
            if (ad.Layer == layername)
            {
                if (ad.StateName == "Closed")
                {
                    AntaClose = ad;
                }
                else if (ad.StateName == "Opened")
                {
                    AntaOpen = ad;
                }
            }
        }

        AntaOpen.StateEnter += DoorOpen;
        AntaClose.StateEnter += DoorClose;
        AntaClose.StatePlayed += DoorClosed;
        AntaOpen.StatePlayed += AntaOpened;
        AntaClose.StatePlayed += AntaClosed;
        AntaOpen.StatePlayed += AntaOpened;
        AntaClose.StatePlayed += AntaClosed;

        MakeAllItemsUngrabbable();
    }
    private void Awake()
    {
        ItemCode = ItemCodes.Locker;
        if (Slave != null)
            nanimator = Slave.GetComponent<NetworkAnimator>();
        else
            nanimator = GetComponent<NetworkAnimator>();
        Sinistra = new Anta();
        Destra = new Anta();
        Sinistra.anta = AntaSinistra;
        AntaSinistra.MasterNet = this;
        Destra.anta = AntaDestra;
        AntaDestra.MasterNet = this;
        Sinistra.Source = AntaSinistra.GetComponent<AudioSource>();
        Destra.Source = AntaDestra.GetComponent<AudioSource>();
    }

    public void AntaOpened(AdvancedStateMachineBehaviour st)
    {
        SetCanInteract(true);
        ItemActive = true;
    }

    internal void PlayClip(int v, Anta a)
    {
        StartCoroutine(PlayAudio(clips[v], a));
    }
    IEnumerator PlayAudio(AudioClip clip, Anta a)
    {
        yield return new WaitUntil(() => !a.toplay);
        if (a.Source.isPlaying)
        {
            a.toplay = true;
            yield return new WaitWhile(() => a.Source.isPlaying);
        }
        a.Source.clip = clip;
        a.Source.Play();
        yield return new WaitWhile(() => a.Source.isPlaying);
        a.toplay = false;
    }

    public void AntaClosed(AdvancedStateMachineBehaviour st)
    {
        SetCanInteract(true);
        ItemActive = false;
    }


    void MakeItemUngrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItemNet>();
        if (g != null)
        {
            g.SetCanInteract(false);
            g.DisablePhysics();
        }
    }
    
    public void DoorOpen(AdvancedStateMachineBehaviour st)
    {
        PlayClip(0, Sinistra);
        PlayClip(0, Destra);
        MakeAllItemsGrabbable();
    }

    public void DoorClosed(AdvancedStateMachineBehaviour st)
    {
        MakeAllItemsUngrabbable();
    }

    public void DoorClose(AdvancedStateMachineBehaviour st)
    {
        PlayClip(0, Sinistra);
        PlayClip(0, Destra);
    }


    void MakeItemGrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItemNet>();
        if (g != null)
        {
            g.SetCanInteract(true);
            g.EnablePhysics();
        }
    }

    public void MakeAllItemsGrabbable()
    {
        List<Collider> ItemsToKeep = new List<Collider>();
        foreach (Collider item in ContainedItems)
        {
            if (Interno.bounds.Intersects(item.bounds))
            {
                MakeItemGrabbable(item.transform);
                ItemsToKeep.Add(item);
            }
        }
        if (ItemsToKeep.Count < ContainedItems.Count)
        {
            var c = ContainedItems;
            ContainedItems = ItemsToKeep;
            c.Clear();
        }
    }

    public void MakeAllItemsUngrabbable()
    {
        List<Collider> ItemsToKeep = new List<Collider>();
        foreach (Collider item in ContainedItems)
        {
            if (Interno.bounds.Intersects(item.bounds))
            {
                MakeItemUngrabbable(item.transform);
                ItemsToKeep.Add(item);
            }
        }
        if (ItemsToKeep.Count < ContainedItems.Count)
        {
            var c = ContainedItems;
            ContainedItems = ItemsToKeep;
            c.Clear();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var i = other.GetComponent<GenericItemNet>();
        if (i != null)
        {
            if(i.Grabbable && Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other))
            {
                ContainedItems.Add(other);
                return;
            }
        }
        else
        {
            var isl = other.GetComponent<GenericItemSlave>();
            if (isl != null)
            {
                if (isl.MasterNet.Grabbable && Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other))
                    {
                        ContainedItems.Add(other);
                        return;
                    }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var i = other.GetComponent<GenericItemNet>();
        if (i != null)
        {
            if (i.Grabbable && ContainedItems.Contains(other))
            {
                ContainedItems.Remove(other);
                return;
            }
        }
        else
        {
            var isl = other.GetComponent<GenericItemSlave>();
            if (isl != null)
            {
                if (isl.MasterNet.Grabbable && Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other))
                {
                    ContainedItems.Add(other);
                    return;
                }
            }
        }
    }
    // Update is called once per frame
    public override void Update()
    {

    }
    
    public override void ClickButton(object sender)//object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
        if (Sinistra.anta == (GenericItemSlave) sender)
            Sinistra.Source.Play();
        else if (Destra.anta == (GenericItemSlave) sender)
            Destra.Source.Play();
    }
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (!ItemActive)
        {
            nanimator.animator.SetBool("OpenAnte", true);
        }
        else
        {
            nanimator.animator.SetBool("OpenAnte", false);
        }
    }
    public override void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        Interact(c, hand);
    }

    public override void EnableOutline(ItemControllerNet c)
    {
        Color col;
        Material[] mts = null;

        var mr = AntaSinistra.GetComponent<MeshRenderer>();
        if (mr != null)
            mts = mr.materials;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 255;
                m.SetColor("_OutlineColor", col);
            }

            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 255;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }

            mr = AntaDestra.GetComponent<MeshRenderer>();
            if (mr != null)
                mts = mr.materials;
            if (mts != null)
            {
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 255;
                    m.SetColor("_OutlineColor", col);
                }

                foreach (Transform t in Pieces)
                {
                    var mat = t.GetComponent<MeshRenderer>();
                    if (mat != null)
                        mts = mat.materials;
                    foreach (Material m in mts)
                    {
                        if (Grabbable)
                            col = Color.yellow;
                        else
                            col = Color.white;
                        col.a = 255;
                        m.SetColor("_OutlineColor", col);
                    }
                }
            }
            outlined = true;
    }

    public override void DisableOutline(ItemControllerNet c)
    {
        Color col;
        Material[] mts = null;
            var mr = AntaSinistra.GetComponent<MeshRenderer>();
            if (mr != null)
                mts = mr.materials;
            if (mts != null)
            {
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 0;
                    m.SetColor("_OutlineColor", col);
                }
                foreach (Transform t in Pieces)
                {
                    var mat = t.GetComponent<MeshRenderer>();
                    if (mat != null)
                        mts = mat.materials;
                    foreach (Material m in mts)
                    {
                        if (Grabbable)
                            col = Color.yellow;
                        else
                            col = Color.white;
                        col.a = 0;
                        m.SetColor("_OutlineColor", col);
                    }
                }
            }
        mr = AntaDestra.GetComponent<MeshRenderer>();
        if (mr != null)
            mts = mr.materials;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 0;
                m.SetColor("_OutlineColor", col);
            }
            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 0;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = false;
    }

    public override void Reset()
    {
        ItemActive = false;
        base.Reset();
    }
}
