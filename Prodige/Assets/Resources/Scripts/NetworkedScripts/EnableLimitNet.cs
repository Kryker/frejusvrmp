﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnableLimitNet : NetworkBehaviour {

    [SyncVar(hook = "OnLimitEnabledChanged")]
    public bool LimitEnabled;
    public Collider PlayerLimit;
    private void Start()
    {
        if (isServer)
        {
            if(isClient)
                LimitEnabled = false;
            else
                OnLimitEnabledChanged(false);
        }
        else
            OnLimitEnabledChanged(LimitEnabled);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (isServer && other.transform.parent != null)
        {
            var c = other.transform.parent.GetComponentInParent<CarMovementScriptNet>();
            if (c != null)
            {
                if(isClient)
                    LimitEnabled = true;
                else
                    OnLimitEnabledChanged(true);
            }
        }
    }
    public void OnLimitEnabledChanged(bool value)
    {
        if(value)
            GetComponent<SphereCollider>().enabled = false;
        PlayerLimit.enabled = value;
        LimitEnabled = value;
    }
}
