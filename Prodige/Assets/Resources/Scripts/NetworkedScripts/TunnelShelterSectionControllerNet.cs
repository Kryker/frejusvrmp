﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TunnelShelterSectionControllerNet : TunnelSectionControllerNet
{
    public List<EntrataRifugioNet> Porte;
    public PulsanteChiamataSOSNet PulsanteTelefono;
    //public Panchina Panca;
    public AudioSource Jingle, Message, Ambient, LinkRoom, Ventilation;
    AudioSourceFader jinglefader, messagefader, ambientfader, linkroomfader, ventilationfader;
    public ExtinguisherNet Estintore;
    //[SyncVar]
    public List<PlayerStatusNet> Reached;
    [SyncVar (hook = "OnVentilationChanged")]
    bool ventilation;
    public bool ShelterEnabled;
    public MonitorStop MonitorSX, MonitorDX;
    bool ventilationtransition = false;
    public AudioClip VentilationStart, VentilationEnabled, VentilationEnd;
    public Transform Segnalatori;
    bool setupinternaldone = false, setupexternaldoordone = false;
    MeshRenderer SegnalatoriMR;
    [SyncVar]
    bool _flashingenabled;
    public Sbarra SbarraDX, SbarraSX;
    bool signon = true;
    public int NumRifugio;
    float tictimeup = 0.6f;
    //bool once = false;
    float tictimedown = 0.4f;
    float nexttic = 0;
    [SyncVar (hook ="OnFireProcedurerStartedChanged")]
    bool FireProcedurerStarted;
    public TunnelShelterSectionControllerNet Precedente, Successivo;
    bool regulatelater = false;
    
    internal void GetOutFromShelter(float time)
    {
        if (isServer && !isClient)
        {
            linkroomfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Raise);
            ambientfader.SetUpAndStart(0.66f, 1, time, AudioSourceFader.FadeDirection.Fade);
            jinglefader.SetUpAndStart(0, 1, time, AudioSourceFader.FadeDirection.Raise);
            GameManager.Instance.TCN.ambientfader.SetUpAndStart(0, 0.7f, time, AudioSourceFader.FadeDirection.Raise);
        }
    }

    internal void GetInsideShelter(float time)
    {
        if (isServer && !isClient)
        {
            linkroomfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Fade);
            jinglefader.SetUpAndStart(0, 1, time, AudioSourceFader.FadeDirection.Fade);
            ambientfader.SetUpAndStart(0.66f, 1, time, AudioSourceFader.FadeDirection.Raise);
            GameManager.Instance.TCN.ambientfader.SetUpAndStart(0, 0.7f, time, AudioSourceFader.FadeDirection.Fade);
        }
    }

    public override void Start()
    {
        Reached = new List<PlayerStatusNet>();

        base.Start();
        if (Jingle != null)
            jinglefader = Jingle.GetComponent<AudioSourceFader>();
        if (Message != null)
            messagefader = Message.GetComponent<AudioSourceFader>();
        if (Ambient != null)
            ambientfader = Ambient.GetComponent<AudioSourceFader>();
        if (LinkRoom != null)
            linkroomfader = LinkRoom.GetComponent<AudioSourceFader>();
        if (Ventilation != null)
            ventilationfader = Ventilation.GetComponent<AudioSourceFader>();
     
        if (Segnalatori != null)
            SegnalatoriMR = Segnalatori.GetComponent<MeshRenderer>();
        if (PulsanteTelefono != null)
            PulsanteTelefono.posizione = Posizione.Rifugio;

        if (isServer)
        {
            _flashingenabled = false;
            if (isClient)
            {
                ventilation = false;
                FireProcedurerStarted = false;
            }
                else
            {
                OnVentilationChanged(false);
                OnFireProcedurerStartedChanged(false);
            }
        }
        else
        {
            OnFireProcedurerStartedChanged(FireProcedurerStarted);
            if (ventilation)
                OnVentilationChanged(ventilation);
        }
    }

    private void OnFireProcedurerStartedChanged(bool state)
    {
        if (state)
        {
            var p = GameManager.Instance.TCN.GetPlayerController();
            if (p == null && !isServer)
                regulatelater = true;
            else if (p != null && !p.GetComponent<StageControllerNet>().CarLeft)
                RegulateJingle(0.6f);
            PlayJingle();

            if (Segnalatori != null)
                EnableFlashing(true);

            if (transform.position.z < GameManager.Instance.TCN.FireSection.transform.position.z)
            {
                if (MonitorDX != null && !MonitorDX.ItemActive())
                    MonitorDX.EnableSign(true);
                if (SbarraDX != null)
                    SbarraDX.Close();
            }
            else
            {
                if (MonitorSX != null && !MonitorSX.ItemActive())
                    MonitorSX.EnableSign(true);
                if (SbarraSX != null)
                    SbarraSX.Close();
            }
        }
        FireProcedurerStarted = state;
    }

    private void OnVentilationChanged(bool state)
    {
        if (Ventilation != null)
        {
            if (state)
                StartCoroutine(VentilationOn());
            else
                StartCoroutine(VentilationOff());
        }
        ventilation = state;
    }

    // Use this for initialization
    void OnEnable () {
	}
	
	// Update is called once per frame
	void Update () {
        var now = Time.time;
        if (regulatelater)
        {
            var p = GameManager.Instance.TCN.GetPlayerController();
            if (p != null && !p.GetComponent<StageControllerNet>().CarLeft)
                { 
                RegulateJingle(0.6f);
                regulatelater = false;
                }
        }
        if (_flashingenabled)
        {
            if (nexttic <= now)
            {
                if (!signon)
                {
                    SignOn();
                    nexttic = now + tictimeup;
                }
                else
                {
                    SignOff();
                    nexttic = now + tictimedown ;
                }
            }
        }
    }

    public void EnableFlashing(bool sel)
    {
        if (isServer)
        {
            if (sel)
                _flashingenabled = true;
            else
                _flashingenabled = false;
        }
    }

    public bool FlashingEnabled()
    {
        return _flashingenabled;
    }

    void SignOn()
    {
        var newmat = new Material(SegnalatoriMR.materials[1]);
        newmat.EnableKeyword("_EMISSION");
        var m = new Material[2];
        m[0] = SegnalatoriMR.materials[0];
        m[1] = newmat;
        SegnalatoriMR.materials = m;
        signon = true;
    }

    void SignOff()
    {
        var newmat = new Material(SegnalatoriMR.materials[1]);
        newmat.DisableKeyword("_EMISSION");
        var m = new Material[2];
        m[0] = SegnalatoriMR.materials[0];
        m[1] = newmat;
        SegnalatoriMR.materials = m;
        signon = false;
    }

    public void PlayJingle()
    {
        if (ShelterEnabled && !Jingle.isPlaying)
            Jingle.Play();
    }
    

    void ExitShelter()
    {
        GameManager.Instance.TCN.ExitShelter();
    }

    public void EnterShelter(uint id)
    {
        var p = GameManager.Instance.TCN.GetPlayers();
        var pid = GameManager.Instance.TCN.GetPlayerNetIDByID(id);
        p[pid].playerStat.EnterShelter();
    }

    public void OpenInternalDoorOutside(float time)
    {
        if (Message.isPlaying)
            messagefader.SetUpAndStart(0, 1, time, AudioSourceFader.FadeDirection.Raise);
        if (ventilation)
            ventilationfader.SetUpAndStart(0, 1, time, AudioSourceFader.FadeDirection.Raise);
        ambientfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Raise);
    }
    public void OpenInternalDoorInside(float time)
    {
        jinglefader.SetUpAndStart(0, 0.25f, time, AudioSourceFader.FadeDirection.Raise);
        GameManager.Instance.TCN.ambientfader.SetUpAndStart(0, 0.35f, time, AudioSourceFader.FadeDirection.Raise);
        linkroomfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Raise);
        ambientfader.SetUpAndStart(0.66f, 1, time, AudioSourceFader.FadeDirection.Fade);
        //ExitShelter();
    }
    public void OpenExternalDoorOutside(float time)
    {
        linkroomfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Raise);
        ambientfader.SetUpAndStart(0.35f, 0.66f, time, AudioSourceFader.FadeDirection.Raise);
        
    }
    public void OpenExternalDoorInside(float time)
    {
        jinglefader.SetUpAndStart(0.25f, 1, time, AudioSourceFader.FadeDirection.Raise);
        GameManager.Instance.TCN.ambientfader.SetUpAndStart(0.35f, 0.7f, time, AudioSourceFader.FadeDirection.Raise);
    }

    public void CloseInternalDoorOutside(float time)
    {
        if (!setupinternaldone)
            setupinternaldone = true;
        else
        {
            if (Message.isPlaying)
                messagefader.SetUpAndStart(0, 1, time, AudioSourceFader.FadeDirection.Fade);
            if (ventilation)
                ventilationfader.SetUpAndStart(0, 1, time, AudioSourceFader.FadeDirection.Fade);
            ambientfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Fade);
        }
    }
    public void CloseInternalDoorInside(float time)
    {
        linkroomfader.SetUpAndStart(0.5f, 1, time, AudioSourceFader.FadeDirection.Fade);
        jinglefader.SetUpAndStart(0, 0.25f, time, AudioSourceFader.FadeDirection.Fade);
        ambientfader.SetUpAndStart(0.66f, 1, time, AudioSourceFader.FadeDirection.Raise);
        GameManager.Instance.TCN.ambientfader.SetUpAndStart(0, 0.35f, time, AudioSourceFader.FadeDirection.Fade);
        //EnterShelter();
    }
    public void CloseExternalDoorOutside(float time)
    {
        if (!setupexternaldoordone)
            setupexternaldoordone = true;
        else
            linkroomfader.SetUpAndStart(0, 0.5f, time, AudioSourceFader.FadeDirection.Fade);
    }
    public void CloseExternalDoorInside(float time)
    {
        jinglefader.SetUpAndStart(0.25f, 1, time, AudioSourceFader.FadeDirection.Fade);
        GameManager.Instance.TCN.ambientfader.SetUpAndStart(0.35f, 0.7f, time, AudioSourceFader.FadeDirection.Fade);
    }

    public void StartFireProcedures()
    {
        if (isServer && ShelterEnabled)
            { 
            if(isClient)
                FireProcedurerStarted = true;
            else
                OnFireProcedurerStartedChanged(true);
            StartVentilation();
            }
    }

    private IEnumerator VentilationOn()
    {
        ventilationtransition = true;
        Ventilation.clip = VentilationStart;
        Ventilation.loop = false;
        Ventilation.Play();
        yield return new WaitWhile(() => Ventilation.isPlaying);
        Ambient.Stop();
        Ventilation.clip = VentilationEnabled;
        Ventilation.loop = true;
        Ventilation.Play();
        ventilationtransition = false;
    }

    public void StopVentilation()
    {
    if (isServer && Ventilation != null && ventilation)
        {
        if(isClient)
            ventilation = false;
        else
            OnVentilationChanged(false);
        }
    }
    public void StartVentilation()
    {
        if (isServer && Ventilation != null && !ventilation)
        {
            if(isClient)
                ventilation = true;
            else
                OnVentilationChanged(true);
        }
    }

    private IEnumerator VentilationOff()
    {
        ventilationtransition = true;
        Ventilation.Stop();
        Ventilation.clip = VentilationEnd;
        Ventilation.loop = false;
        Ventilation.Play();
        yield return new WaitWhile(() => Ventilation.isPlaying);
        Ambient.Play();
        ventilationtransition = false;
    }
    
    public void RegulateJingle(float volume)
    {
        if(ShelterEnabled)
            Jingle.volume = volume;
    }
    public void RegulateMessage(float volume)
    {
        if(ShelterEnabled)
            Message.volume = volume;
    }

    public float GetJingleVolume()
    {
        return Jingle.volume;
    }

    public float GetMessageVolume()
    {
        return Message.volume;
    }


    public void Echo(bool yes)
    {
        GetComponent<AudioEchoFilter>().enabled = yes;
    }

    public override void Reset()
    {
        foreach(EntrataRifugioNet e in Porte)
            e.Reset();
        if(PulsanteTelefono != null)
            PulsanteTelefono.Reset();
        Reached.Clear();
        if(jinglefader != null)
            jinglefader.Reset();
        if (messagefader != null)
            messagefader.Reset();
        if (ambientfader != null)
            ambientfader.Reset();
        if(linkroomfader != null)
            linkroomfader.Reset();
        base.Reset();
    }

    /*public override void LightsOn(bool active)
        {
        if (active)
            {
            foreach (Transform t in Lights)
                {
                t.FindChild("LampioniAccesi").gameObject.SetActive(true);
                t.FindChild("LampioniSpenti").gameObject.SetActive(false);
                }
            }
        else
            {
            foreach(Transform t in Lights)
                {
                t.FindChild("LampioniAccesi").gameObject.SetActive(false);
                t.FindChild("LampioniSpenti").gameObject.SetActive(true);
                }
            }
        }*/
}
