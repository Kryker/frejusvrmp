﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class Health : NetworkBehaviour
{

    public const int maxHealth = 100;
    public bool destroyOnDeath;
    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth;
    private NetworkStartPosition[] spawnPoints;
    public RectTransform healthBar;

    void Start()
    {
        if (isServer)
        {
            if(isClient)
                currentHealth = 100;
            else
                OnChangeHealth(100);
        }
        else
            OnChangeHealth(currentHealth);
        if (isLocalPlayer)
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
    }



public void TakeDamage(int amount)
    {
        if (!isServer)
        {
            return;
        }
        var h = currentHealth - amount;
        if (isClient)
            currentHealth = h;
        else
            OnChangeHealth(h);

        if (currentHealth <= 0)
        {
            if (destroyOnDeath)
            {
                Destroy(gameObject);
            }
            else
            {
                if(isClient)
                    currentHealth = maxHealth;
                else
                    OnChangeHealth(currentHealth);
                // called on the Server, but invoked on the Clients
                RpcRespawn();
            }
        }
    }

    void OnChangeHealth(int health)
    {
        healthBar.sizeDelta = new Vector2(health, healthBar.sizeDelta.y);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            // Set the spawn point to origin as a default value
            Vector3 spawnPoint = Vector3.zero;

            // If there is a spawn point array and the array is not empty, pick a spawn point at random
            if (spawnPoints != null && spawnPoints.Length > 0)
            {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }

            // Set the player’s position to the chosen spawn point
            transform.position = spawnPoint;
        }
    }
}