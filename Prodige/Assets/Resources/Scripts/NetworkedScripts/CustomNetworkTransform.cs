﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class CustomNetworkTransform : NetworkBehaviour
{
    [SerializeField]
    [Range(0, 120)]
    private float NetworkSendRate = 9;
    [SerializeField]
    private float MovementThreshold = 0.001f;
    [SerializeField]
    private float snapThreshold = 5f;
    [SerializeField]
    private float InterpolateMovementFactor = 1;
    [SerializeField]
    private float InterpolateRotationFactor = 1;
    [SyncVar (hook ="OnPositionReceived")]
    private Vector3 _lastPosition;
    [SyncVar(hook = "OnRotationReceived")]
    private Quaternion _lastRotation;
    [SerializeField]
    private bool _local = false;
    float NextSend = 0;
    float _t_r = 0;
    float _t_l = 0;

    void OnPositionReceived(Vector3 value)
    {
        _t_l = 0;
        _lastPosition = value;
    }

    void OnRotationReceived(Quaternion value)
    {
        _t_r = 0;
        _lastRotation = value;
    }
    List<CustomNetworkTransformChild> ChildList;
    // Use this for initialization
    void Start () {
        ChildList = GetComponents<CustomNetworkTransformChild>().ToList();
         if (isServer)
        {
            int i = 0;
            foreach (CustomNetworkTransformChild c in ChildList)
            {
                c.SetIndex(i);
                i++;
            }
            if (_local && transform.parent != null)
                _lastPosition = transform.localPosition;
            else
                _lastPosition = transform.position;
            if (_local && transform.parent != null)
                _lastRotation = transform.localRotation;
            else
                _lastRotation = transform.rotation;
        }           
    }
    private void LateUpdate()
    {
        if (!hasAuthority)
        {
            InterpolatePosition();
            InterpolateRotation();
        }
    }
    public bool SendRotation = true;
    private void FixedUpdate()
    {
        var now = Time.time;
        if (hasAuthority && NetworkSendRate > 0 && now > NextSend)
        {
                Vector3 currPos = Vector3.zero;
				Quaternion currRot = Quaternion.identity;
				if (_local && transform.parent != null)
					currPos = transform.localPosition;
				else
					currPos = transform.position;

				if (_local && transform.parent != null)
					currRot = transform.localRotation;
				else
					currRot = transform.rotation;

				var posChanged = IsPositionChanged(currPos);

                if (posChanged)
                {
                    if (!isServer)
                        CmdSendPosition(currPos);
                    else
                    {
                    if(isClient)
                        _lastPosition = currPos;
                    else
                        OnPositionReceived(currPos);
                    }
                }
                var rotChanged = IsRotationChanged(currRot);

                if (rotChanged && SendRotation)
                {
                    if (!isServer)
                        CmdSendRotation(currRot);
                    else
                    {
                    if(isClient)
                        _lastRotation = currRot;
                    else
                        OnRotationReceived(currRot);
                    }
                }
            NextSend = now + 1 / NetworkSendRate;
        }
    }

    [Command]
    private void CmdSendPosition(Vector3 pos)
    {
        if (isClient)
            _lastPosition =  pos;
        else
            OnPositionReceived(pos);
    }

    [Command]
    private void CmdSendRotation(Quaternion rot)
    {
        if (isClient)
            _lastRotation = rot;
        else
            OnRotationReceived(rot);
    }
     private void InterpolatePosition()
    {
        if (_local && transform.parent != null)
        {
            if (InterpolateMovementFactor == 0 || Vector3.Distance(transform.localPosition, _lastPosition) > snapThreshold)
                transform.localPosition = _lastPosition;
            else
                {
                _t_l += (Time.deltaTime * InterpolateMovementFactor);
                if (_t_l > 1)
                    _t_l = 1;
                transform.localPosition = Vector3.Lerp(transform.localPosition, _lastPosition, _t_l);
                }
        }
        else
        {
            if (InterpolateMovementFactor == 0 || Vector3.Distance(transform.position, _lastPosition) > snapThreshold)
                transform.position = _lastPosition;
            else
                {
                _t_l += (Time.deltaTime * InterpolateMovementFactor);
                if (_t_l > 1)
                    _t_l = 1;
                transform.position = Vector3.Lerp(transform.position, _lastPosition, _t_l);
                }
        }
    }

    private void InterpolateRotation()
    {
        if (_local && transform.parent != null)
        {
            if (InterpolateRotationFactor == 0)
                transform.localRotation = _lastRotation;
            else
                {
                _t_r += (Time.deltaTime * InterpolateRotationFactor);
                if (_t_r > 1)
                    _t_r = 1;
                transform.localRotation = Quaternion.Lerp(transform.localRotation, _lastRotation, _t_r);
                }
        }
        else
        {
            if (InterpolateRotationFactor == 0)
                transform.rotation = _lastRotation;
            else
                {
                    _t_r += (Time.deltaTime * InterpolateRotationFactor);
                    if (_t_r > 1)
                        _t_r = 1;
                    transform.rotation = Quaternion.Lerp(transform.rotation, _lastRotation, _t_r);
                }
        }
    }

    private bool IsPositionChanged(Vector3 position)
    {
        return Vector3.Distance(position, _lastPosition) > MovementThreshold;
    }

    private bool IsRotationChanged(Quaternion rotation)
    {
        return (rotation != _lastRotation);
    }
	
    private void OnEnable()
    {
        if (ChildList != null)
            foreach (CustomNetworkTransformChild c in ChildList)
                c.enabled = true;
    }

    private void OnDisable()
    {
        if (ChildList != null)
            foreach (CustomNetworkTransformChild c in ChildList)
                c.enabled = false;
    }
}
