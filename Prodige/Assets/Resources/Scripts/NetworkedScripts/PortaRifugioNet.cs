﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PortaRifugioNet : GenericItemNet {
    
    public EntrataRifugioNet entrata;
    [HideInInspector]
    public AdvancedStateMachineBehaviour PortaOpen, PortaClose, ManigliaUnpressed, ManigliaPressed, ManigliaOpening, ManigliaClosing, ManigliaIdle;

    // Use this for initialization
    public override void Start () {
        ItemCode = ItemCodes.ShelterDoor;
        if (entrata == null)
            entrata = transform.parent.GetComponentInParent<EntrataRifugioNet>();
        base.Start();
	}

    // Update is called once per frame
    public override void Update () {
		//if torotate, rotate
	}

    public override bool CanInteract(ItemControllerNet c)
    {
        return base.CanInteract(c);
    }
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if(entrata.CanInteract(c))
            entrata.Interact(c, hand);
    }

    public override void SetCanInteract(bool can, ItemControllerNet c)
    {
        base.SetCanInteract(can, c);
    }

    public override void Reset()
    {
        base.SetCanInteract(true);
        base.Reset();
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override void ClickButton(object sender, ClickedEventArgs e)
    { }
}
