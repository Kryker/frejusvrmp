﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public enum WindDirection { Favorable, Headwind, Windless };
public enum ControllerType { MouseAndKeyboard, ArmSwing, FootSwing, CVirtualizer, KatWalk, FreeWalk };

public class SyncListNetworkInstanceId : SyncListStruct<NetID> { }

public struct NetID
{
    public NetworkInstanceId netID;
}
public class TunnelControllerNet : NetworkBehaviour
{
    [SyncVar(hook = "OnWindDirChanged")]
    WindDirection WindDir;
    Transform WindZone;
    public Transform FPSControllerPrefab;
    public Transform FPSNoWalkControllerPrefab;
    public bool lightscaling = true;
    public Transform ArmSwingControllerPrefab, ArmSwingNoWalkControllerPrefab;
    public Transform FootSwingControllerPrefab, FootSwingNoWalkControllerPrefab;
    public Transform CVirtualizerControllerPrefab, CVirtualizerNoWalkControllerPrefab;
    public Transform KatWalkControllerPrefab, KatWalkNoWalkControllerPrefab;
    public Transform FreeWalkControllerPrefab, FreeWalkNoWalkControllerPrefab;
    public TunnelSectionControllerNet FireSection;
    public bool CubeMappingEnabled = false;
    public List<CubemapperUtil> CubesToMap;
    [HideInInspector]
    public List<ScaleWithDistance> Luci = new List<ScaleWithDistance>();
    bool once = false;
    public VehicleDataNet DynamicCar;
    public TruckControllerNet DynamicTruck;
    public List<Transform> Vehicles;
    public NetworkSpawn Spawn;
    public CiaoNet Camionista;
    TunnelFireControllerNet TunnelFire;
    Dictionary<uint, NetworkInstanceId> playerIDs = new Dictionary<uint, NetworkInstanceId>();
    Dictionary<NetworkInstanceId, PlayerStatusNet> players = new Dictionary<NetworkInstanceId, PlayerStatusNet>();
    PlayerStatusNet playerController = null;
    Queue<uint> ReusableIDs = new Queue<uint>();
    uint nextID = 0;
    [SyncVar]
    public float AmbientTemp = 24.0f;

    //tesisti
    public Transform SpawnPointPompiereIniziale;
    public GestioneStartMenu MenuDegliStart; //rimuovibile, sposto in TCN
    public HeavyTruckNPC CamionNPC;
    public CarNPC CarN;
    public CustomSmoke smoke3D;
    public NPCControllerNet NPCCNet;
    public GestorePunteggiNet GPN;
    public delegate void RuoloSceltoDelegate(SpawnMode s);
    public event RuoloSceltoDelegate RuoloScelto;
    public event MyDelegate inizioPartita;
    public GameObject StartAsServer;
    public List<CanBeOnFireNet> fires;
    //fine tesisti

    internal void FlipWindDirection()
    {
        if (isServer)
        {
            if (WindDir == WindDirection.Favorable)
                ChangeWindDirection(WindDirection.Headwind);
            else if (WindDir == WindDirection.Headwind)
                ChangeWindDirection(WindDirection.Favorable);
        }
    }

    internal void Reset()
    {
        if (isServer)
        {
            foreach (Transform t in Vehicles)
            {
                Spawner.SpawnedVehicles.Add(t);
                t.GetComponent<NPCCarMovementScriptNet>().Reset();
                t.GetComponent<VehicleDataNet>().Reset();
            }

            DynamicCar.GetComponent<CarMovementScriptNet>().Reset();
            DynamicCar.GetComponent<EnableCarInteractionsNet>().Reset();
            DynamicCar.Reset();

            DynamicTruck.GetComponent<CarMovementScriptNet>().Reset();
            DynamicTruck.GetComponent<EnableCarInteractionsNet>().Reset();
            DynamicTruck.Reset();

            StopGame();
            if (isClient)
            {
                fireproceduresstarted = false;
                warningproceduresstarted = false;
            }
            else
            {
                OnFireProceduresChanged(false);
                OnWarningProceduresChanged(false);
            }
            foreach (KeyValuePair<NetworkInstanceId, PlayerStatusNet> k in players)
                k.Value.Reset();

            braking = false;

            //fireCount = 0;
            //SomethingBurning = false;
            firestarted = false;
            GPN.Reset(); //resetto i punteggi

            //SmokeFront = true;
            if (isClient)
                WindDir = InitialWindDirection;
            else
                OnWindDirChanged(InitialWindDirection);

            var clist = DynamicCar.GetComponents<CanBeOnFireNet>();
            foreach (CanBeOnFireNet c in clist)
                c.Reset();
            DynamicTruck.Reset();

            clist = DynamicTruck.GetComponents<CanBeOnFireNet>();
            foreach (CanBeOnFireNet c in clist)
                c.Reset();

            Camionista.Reset();

            foreach (TunnelSectionControllerNet s in TunnelSections)
            {
                s.Reset();
            }



            Spawn.Reset();

            FindObjectOfType<EnableLimitNet>().LimitEnabled = false;

            if (!isClient)
            {
                lightscaling = false;
                FindObjectOfType<ForceBrakeNet>().gameObject.SetActive(true);
                FindObjectOfType<StopAvailableNet>().gameObject.SetActive(true);
                FindObjectOfType<StopLimitCheckNet>().gameObject.SetActive(true);

                if (CubeMappingEnabled)
                    foreach (CubemapperUtil c in CubesToMap)
                    {
                        c.thisCubemapper.gameObject.SetActive(true);
                        c.CalculateCubeMap();
                    }
            }

            RpcReset();
        }


    }

    private void RpcReset()
    {
        lightscaling = false;

        if (CubeMappingEnabled)
            foreach (CubemapperUtil c in CubesToMap)
            {
                c.thisCubemapper.gameObject.SetActive(true);
                c.CalculateCubeMap();
            }
    }

    [HideInInspector]
    public AudioSource ambientsound;
    [HideInInspector]
    public AudioSourceFader ambientfader;
    public List<TunnelShelterSectionControllerNet> Rifugi;
    public List<TunnelTelephoneSectionControllerNet> Nicchie;
    public VRUICanvas StartGameButton;
    //int saferoom;
    //public bool StartSeated;
    [SyncVar(hook = "OnFireProceduresChanged")]
    bool fireproceduresstarted;
    [SyncVar(hook = "OnWarningProceduresChanged")]
    bool warningproceduresstarted;
    //public Transform SeatedSpawn;
    //public int TunnelLength = 0;
    //public int CrashSiteIndex = -1;
    public WindDirection InitialWindDirection = WindDirection.Headwind;
    public CarSpawnNet Spawner;
    [HideInInspector]
    public List<TunnelSectionControllerNet> TunnelSections;
    public LocalizerController Localizer;
    [SyncVar]
    [HideInInspector]
    public bool TunnelOnFire = false;
    public string MainMenuScene = "MainMenu";
    public CanBeOnFireSlave Telone;
    [SyncVar(hook = "OnBrakingChanged")]
    bool braking;
    bool spawnlater;
    uint spawnpid;
    Vector3 spawnpos;
    [SyncVar(hook = "OnGameStartedChanged")]
    public bool GameStarted;
    public ScaleParticleWithDistance Fuoco;
    //[SyncVar]
    //uint fireCount;
    //[SyncVar]
    //bool SomethingBurning;
    [SyncVar]
    public bool firestarted;

    private Coroutine endgameroutine;






    public void StartWarningProcedures()
    {
        if (isServer && !warningproceduresstarted)
        {
            foreach (TunnelTelephoneSectionControllerNet r in Nicchie)
                r.StartWarning();
            if (isClient)
                warningproceduresstarted = true;
            else
                OnWarningProceduresChanged(true);
        }
    }
    /*
    public void UpFireCount(CanBeOnFireNet c)
    {
        FireCount++;
    }
    public void DownFireCount(CanBeOnFireNet c)
    {
        FireCount--;
    }
    
    public uint FireCount
    {
        get { return fireCount; }
        set
        {
            var prev = fireCount;
            fireCount = value;
            if (fireCount > 0 && prev == 0)
                SomethingBurning = true;
            else if (fireCount == 0 && prev > 0)
            {
                SomethingBurning = false;
            }
        }
    }*/

    bool onceStart = true;
    void OnGameStartedChanged(bool value)
    {
        if (value && playerController != null)
            playerController.playerStat.StartGame();
        GameStarted = value;
        if (inizioPartita != null && GameStarted && onceStart)
        {
            onceStart = false;
            inizioPartita.Invoke();
        }
    }
    public static NetID GetNetID(NetworkInstanceId id)
    {
        NetID ID;
        ID.netID = id;
        return ID;
    }
    private void Awake()
    {
        ambientsound = GetComponents<AudioSource>()[0];
        ambientfader = GetComponents<AudioSourceFader>()[0];
        if (GameManager.Instance != null)
            GameManager.Instance.TCN = this;
    }
    private void OnDestroy()
    {
        if (GameManager.Instance != null)
            GameManager.Instance.TCN = null;
    }

    public void ManageCarHandleInteraction(PlayerStatusNet p, ManigliaPortaNet maniglia)
    {
        if (isServer)
        {
            if (p.IsInVehicle())
            {
                p.GetOutFromVehicle();
            }

            else
            {
                if (maniglia.vehicle is TruckControllerNet)
                {
                    p.GetInVehicle(DynamicTruck, maniglia.seat);
                }
                else
                {
                    p.GetInVehicle(DynamicCar, maniglia.seat);
                }

            }

        }
    }

    internal void ExitShelter() //vecchia versione di davide
    {
        if (isServer)
        {
            if (playerController != null)
                playerController.playerStat.ExitShelter();
            StopEndRoutine();
        }
    }

    internal void ExitShelter(uint playerID) //da sistemare
    {
        if (playerController != null && playerID == playerController.PlayerID)
            playerController.playerStat.ExitShelter();
        if (isServer)
            StopEndRoutine();
    }


    internal void PlayerDisconnected(PlayerStatusNet player)
    {
        if (isServer)
        {
            if (DynamicCar.LeftOccupant == player.netId)
            {
                if (isClient)
                    DynamicCar.LeftOccupant = NetworkInstanceId.Invalid;
                else
                    DynamicCar.OnLeftOccupantChanged(NetworkInstanceId.Invalid);
                Spawn.SetDriverButtonAvailable(true);
            }
            else if (DynamicCar.RightOccupant == player.netId)
            {
                if (isClient)
                    DynamicCar.RightOccupant = NetworkInstanceId.Invalid;
                else
                    DynamicCar.OnRightOccupantChanged(NetworkInstanceId.Invalid);
                //Spawn.SetPassengerButtonAvailable(true);
            }
            else if (DynamicTruck.LeftOccupant == player.netId)
            {
                if (isClient)
                    DynamicTruck.LeftOccupant = NetworkInstanceId.Invalid;
                else
                    DynamicTruck.OnLeftOccupantChanged(NetworkInstanceId.Invalid);
                Spawn.SetTruckButtonAvailable(true);
            }
            else
            {

                Spawn.SetComponenteButtonAvailable(true);
            }

            playerIDs.Remove(player.PlayerID);
            ReusableIDs.Enqueue(player.PlayerID);
            players.Remove(player.netId);
        }
    }

    public PlayerStatusNet GetPlayerByID(uint playerID)
    {
        if (playerIDs.ContainsKey(playerID))
        {
            var id = playerIDs[playerID];
            try
            {
                return players[id];
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }
        else
            return null;
    }

    internal bool AllPlayersInside()
    {
        return false;
        //questa logica non deve + essere usata
        foreach (KeyValuePair<NetworkInstanceId, PlayerStatusNet> k in players)
        {
            if (k.Value.IsAlive())
            {
                bool inside = false;
                foreach (TunnelShelterSectionControllerNet s in Rifugi)
                {
                    if (s.Porte != null && s.Porte.Count > 0 && s.Porte[1].IsPlayerInside(k.Key))
                    {
                        inside = true;
                        break;
                    }
                }
                if (!inside)
                    return false;
            }
        }
        return true;
    }

    public void StopLightScaling()
    {
        lightscaling = false;
        foreach (ScaleWithDistance l in Luci)
            l.StopScaling();
        if (Fuoco != null)
            Fuoco.StopScaling();
    }


    void Start()
    {
        //Integrato tutto nella stessa scena
        //SceneManager.LoadScene("WaitingRoom", LoadSceneMode.Additive);

        //StartGameButton.transform.GetChild(0).GetComponent<VRButtonController>().onClick += StartGame;
        if (isServer)
        {
            if (isClient)
            {
                GameStarted = false;
                fireproceduresstarted = false;
                warningproceduresstarted = false;
            }
            else
            {
                OnFireProceduresChanged(false);
                OnWarningProceduresChanged(false);
                OnGameStartedChanged(false);
            }
            braking = false;

            //fireCount = 0;
            //SomethingBurning = false;
            firestarted = false;
            //SmokeFront = true;
        }
        else
        {
            OnFireProceduresChanged(fireproceduresstarted);
            OnWarningProceduresChanged(warningproceduresstarted);
            OnGameStartedChanged(GameStarted);
        }
        if (Localizer != null)
        {
            if (GameManager.Instance != null)
                Localizer.SelectedLanguage = GameManager.Instance.AppLanguage;
            Localizer.ApplyLanguage();
        }

        UnityEngine.Random.InitState(DateTime.Now.Millisecond);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Default"), LayerMask.NameToLayer("TunnelIgnore"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLayer"), LayerMask.NameToLayer("TunnelIgnore"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("TunnelIgnore"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("Fire"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("ControllerLayer"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("Terrain"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("Default"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLayer"), LayerMask.NameToLayer("MovingItem"), true);

        if (WindZone == null)
        {
            var wind = new GameObject("WindZone").AddComponent<WindZone>();
            wind.GetComponent<WindZone>().windTurbulence = 1;
            wind.GetComponent<WindZone>().windPulseMagnitude = 0.5f;
            wind.GetComponent<WindZone>().windPulseFrequency = 0.01f;
            WindZone = wind.transform;
        }

        if (isServer)
        {
            if (isClient)
                WindDir = InitialWindDirection;
            else
                OnWindDirChanged(InitialWindDirection);
        }
        else
            OnWindDirChanged(WindDir);

        if (CubeMappingEnabled)
            foreach (CubemapperUtil c in CubesToMap)
            {
                c.thisCubemapper.gameObject.SetActive(true);
                c.CalculateCubeMap();
            }

    }

    internal void SelectSmokeDirection()
    {
        int front = 0, rear = 0;
        foreach (KeyValuePair<NetworkInstanceId, PlayerStatusNet> x in players)
        {
            if (x.Value != null)
            {
                if (x.Value.transform.position.z <= FireSection.transform.position.z)
                    front++;
                else
                    rear++;
            }
        }
        if (front > rear)
            ChangeWindDirection(WindDirection.Headwind);
        else if (rear > front || UnityEngine.Random.value > 0.5f)
            ChangeWindDirection(WindDirection.Favorable);
        else
            ChangeWindDirection(WindDirection.Headwind);
    }

    public void FireStarted(TunnelFireControllerNet f)
    {
        if (isServer && !firestarted)
        {
            firestarted = true;
            TunnelFire = f;
            if (Spawner != null)
            {
                Spawner.Stop();
                foreach (Transform t in Spawner.SpawnedVehicles)
                    Destroy(t.gameObject);
            }


            TunnelOnFire = true;
        }
    }

    private void StartFireProcedures()
    {
        if (isServer && !fireproceduresstarted)
        {
            if (isClient)
                fireproceduresstarted = true;
            else
                OnFireProceduresChanged(true);
        }
    }


    void OnFireProceduresChanged(bool state)
    {
        if (state)
        {
            foreach (TunnelShelterSectionControllerNet r in Rifugi)
                r.StartFireProcedures();
            foreach (TunnelTelephoneSectionControllerNet r in Nicchie)
                r.StartFireProcedures();
            DynamicCar.radio.FireMessage();
            DynamicTruck.radio.FireMessage();
            //controlloLuciTunnel.FireStarted();
        }
        fireproceduresstarted = state;
    }




    void OnWarningProceduresChanged(bool state)
    {
        if (!isServer)
        {
            if (state)
            {
                foreach (TunnelTelephoneSectionControllerNet r in Nicchie)
                    r.StartWarning();
            }
        }
        warningproceduresstarted = state;
    }

    public void ChangeJingleVolume(float volume)
    {
        foreach (TunnelShelterSectionControllerNet r in Rifugi)
            r.RegulateJingle(volume);
    }

    public void PlayerJoined(PlayerStatusNet p)
    {
        if (p.isLocalPlayer)
            playerController = p;
        if (isServer)
        {
            if (ReusableIDs.Count > 0)
                p.PlayerID = ReusableIDs.Dequeue();
            else
                p.PlayerID = nextID;
            nextID++;
            var id = p.GetComponent<NetworkIdentity>().netId;

            players.Add(id, p);
            playerIDs.Add(p.PlayerID, id);
            if (GameManager.Instance == null || GameManager.Instance.SinglePlayer)
                p.Spawner.SpawnPlayer(SpawnMode.Driver);
        }
    }

    public void PlayerSpawned(PlayerStatusNet p, SpawnMode mode)
    {
        if (isServer)
        {
            p.Ruolo = mode;
            SetPlayerVehicle(p, mode);
        }
    }


    public void SetPlayerVehicle(PlayerStatusNet p, SpawnMode mode)
    {
        switch (mode)
        {
            case SpawnMode.Driver:
                DynamicCar.GetComponent<NetworkIdentity>().AssignClientAuthority(p.GetComponent<NetworkIdentity>().connectionToClient);
                p.GetInVehicle(DynamicCar, VehicleSeat.Seat.Left);
                if (GameManager.Instance == null || GameManager.Instance.SinglePlayer || DynamicCar.RightOccupant != NetworkInstanceId.Invalid)
                    StartGame(p);
                else
                    EnableStartGameButton(p);
                break;
            /*
        case SpawnMode.Passenger:
            p.GetInVehicle(DynamicCar, VehicleSeat.Seat.Right);
            if (DynamicCar.LeftOccupant != NetworkInstanceId.Invalid)
            {
                var a = NetworkServer.FindLocalObject(DynamicCar.LeftOccupant);
                StartGame(a.GetComponent<PlayerStatusNet>());
            }
            break;
            */
            case SpawnMode.TruckDriver:
                DynamicTruck.GetComponent<NetworkIdentity>().AssignClientAuthority(p.GetComponent<NetworkIdentity>().connectionToClient);
                p.GetInVehicle(DynamicTruck, VehicleSeat.Seat.Left);
                if (GameManager.Instance == null || GameManager.Instance.SinglePlayer)
                    StartGame(p);
                else
                    EnableStartGameButton(p);
                break;

            case SpawnMode.Tunnel:
                var p1 = players[playerIDs[0]];
                if (p.isLocalPlayer)
                {
                    p.transform.position = new Vector3(0, 0, p1.transform.position.z - 1);
                    p.EnableListening();
                    p.StartWalking();
                }
                else
                {
                    RpcSetPosition(p.PlayerID, new Vector3(0, 0, p1.transform.position.z - 1), true);
                }

                p.ItemController.SetBrochureAvailable(true);
                break;
            case SpawnMode.Componente:
                var ComponenteSpawner = SpawnPointPompiereIniziale.position;
                var PosizioneInizialeVigile = new Vector3(ComponenteSpawner.x, ComponenteSpawner.y, ComponenteSpawner.z);
                EnableStartGameButton(p);
                if (p.isLocalPlayer)
                {
                    p.transform.position = PosizioneInizialeVigile;
                    p.EnableListening();
                    //p.StartWalking();
                    p.StopWalking();
                }
                else
                {
                    RpcSetPosition(p.PlayerID, PosizioneInizialeVigile, false);
                }

                if (mode == SpawnMode.Componente)
                {
                    p.ItemController.SetBrochureAvailable(false);
                }
                else
                {
                    p.ItemController.SetBrochureAvailable(true);
                }
                break;
            default:
                break;
        }
        /*
        if (GameManager.Instance.SinglePlayerExtended)
        {
            StartGame(p);
        }
        */
    }

    private void EnableStartGameButton(PlayerStatusNet p)
    {

        /*
        if (p.isLocalPlayer)
            StartGameButton.EnableElements();
        else
            RpcEnableStartGameButton(p.PlayerID);
            */
    }
    [ClientRpc]
    private void RpcEnableStartGameButton(uint pid)
    {
        if (playerController.PlayerID == pid)
        {
            //StartGameButton.EnableElements();
        }

    }

    [ClientRpc]
    private void RpcSetPosition(uint playerID, Vector3 newpos, bool canWalk)
    {
        if (playerController == null)
        {
            spawnpid = playerID;
            spawnpos = newpos;
            spawnlater = true;
        }
        else if (playerController.PlayerID == playerID)
        {
            playerController.transform.position = newpos;
            playerController.EnableListening();
            if (canWalk)
            {
                playerController.StartWalking();
            }
            else
            {
                //playerController.StartWalking();
                playerController.StopWalking();
            }

        }
    }

    public void Brake()
    {
        if (isServer && !braking)
        {
            CantBrake();
            braking = true;
        }
    }

    void OnBrakingChanged(bool state)
    {
        if (playerController != null)
        {
            if (state)
                playerController.ShowBrake();
            else
                playerController.StopBraking();
        }
        braking = state;
    }

    public void StopBraking()
    {
        if (isServer && braking)
        {
            CanBrake();
            braking = false;
        }
    }

    public void CanBrake()
    {
        if (isServer)
        {
            if (DynamicCar.LeftOccupant != NetworkInstanceId.Invalid)
            {
                var g = NetworkServer.FindLocalObject(DynamicCar.LeftOccupant);
                if (g != null)
                    g.GetComponent<PlayerStatusNet>().CanBrake();
            }
            if (DynamicCar.RightOccupant != NetworkInstanceId.Invalid)
            {
                var g = NetworkServer.FindLocalObject(DynamicCar.RightOccupant);
                if (g != null)
                    g.GetComponent<PlayerStatusNet>().CanBrake();
            }
            if (DynamicTruck.LeftOccupant != NetworkInstanceId.Invalid)
            {
                var g = NetworkServer.FindLocalObject(DynamicTruck.LeftOccupant);
                if (g != null)
                    g.GetComponent<PlayerStatusNet>().CanBrake();
            }
        }
    }

    public void CantBrake()
    {
        if (isServer)
        {
            if (DynamicCar.LeftOccupant != NetworkInstanceId.Invalid)
            {
                var g = NetworkServer.FindLocalObject(DynamicCar.LeftOccupant);
                if (g != null)
                    g.GetComponent<PlayerStatusNet>().CantBrake();
            }
            if (DynamicCar.RightOccupant != NetworkInstanceId.Invalid)
            {
                var g = NetworkServer.FindLocalObject(DynamicCar.RightOccupant);
                if (g != null)
                    g.GetComponent<PlayerStatusNet>().CantBrake();
            }
            if (DynamicTruck.LeftOccupant != NetworkInstanceId.Invalid)
            {
                var g = NetworkServer.FindLocalObject(DynamicTruck.LeftOccupant);
                if (g != null)
                    g.GetComponent<PlayerStatusNet>().CantBrake();
            }

        }
    }


    public void CanStop()
    {
        if (isServer && !once)
        {
            StartCoroutine(ForceFire());
            StartCoroutine(ForceFireProcedures());
            once = true;
        }
    }

    private IEnumerator ForceFireProcedures()
    {
        yield return new WaitForSeconds(0);
        StartFireProcedures();
        yield break;
    }

    private IEnumerator ForceFire()
    {
        yield return new WaitForSeconds(0);
        Telone.ForceBurn();
        yield break;
    }

    private void JingleFader(float v1, float v2, float v3, AudioSourceFader.FadeDirection mode)
    {
        foreach (TunnelShelterSectionControllerNet r in Rifugi)
        {
            r.Jingle.GetComponent<AudioSourceFader>().SetFader(v1, v2, v3, mode);
            r.Jingle.GetComponent<AudioSourceFader>().StartFader(0);
        }
    }

    // Use this for initialization



    public void LoadMainMenu()
    {
        if (isServer)
        {
            if (GameManager.Instance != null)
            {
                if (GameManager.Instance.NetworkDiscovery.broadcasting)
                    GameManager.Instance.NetworkDiscovery.StopBroadcasting();
                GameManager.Instance.NetworkManager.StopHost();
            }
        }
        else
        {
            if (GameManager.Instance != null)
                GameManager.Instance.NetworkManager.StopClient();
        }
    }

    public void EndGame()
    {
        if (isServer)
        {
            if (EveryOneInside() && endgameroutine == null)
            {

                if (ControlloPresenzaComponente() || GPN.GetSOSRequested(playerController.Ruolo))
                    endgameroutine = StartCoroutine(EndGameRoutine(5));
                else
                    endgameroutine = StartCoroutine(EndGameRoutine(40));

            }
        }
    }

    bool ControlloPresenzaComponente()
    {
        foreach (KeyValuePair<NetworkInstanceId, PlayerStatusNet> x in players)
        {
            if (x.Value.Ruolo == SpawnMode.Componente)
            {
                return true;
            }
        }
        return false;
    }

    private bool EveryOneInside()
    {
        bool inside = false;
        if (isServer)
        {
            inside = true;
            foreach (KeyValuePair<NetworkInstanceId, PlayerStatusNet> x in players)
            {
                if (x.Value.IsAlive() && !x.Value.playerStat.ShelterReached && x.Value.Ruolo != SpawnMode.Componente)
                {
                    inside = false;
                    break;
                }
                if (x.Value.Ruolo == SpawnMode.Componente && !GameCanEndForComponente()) //and NON spenti tutti i fuochi?
                {

                    inside = false;
                    break;
                }
            }
        }
        return inside;
    }
    public void StopEndRoutine()
    {
        if (isServer && endgameroutine != null)
        {
            StopCoroutine(endgameroutine);
            endgameroutine = null;
        }
    }

    IEnumerator EndGameRoutine(float time)
    {
        yield return new WaitForSeconds(time);
        foreach (KeyValuePair<NetworkInstanceId, PlayerStatusNet> x in players)
            if (!isClient || x.Value.playerStat != playerController.playerStat)
                x.Value.playerStat.EndGame();

        if (isClient)
            playerController.playerStat.EndGame();
        endgameroutine = null;
    }

    internal void SetBrochureUnavailable(NetworkInstanceId netid)
    {
        if (isServer)
        {
            var c = NetworkServer.FindLocalObject(netid);
            var p = c.GetComponent<PlayerStatusNet>();
            if (p != null)
                p.ItemController.SetBrochureAvailable(false);
        }
    }

    public void SetBrochureAvailable(NetworkInstanceId netid)
    {
        if (isServer)
        {
            var c = NetworkServer.FindLocalObject(netid);
            if (c != null)
            {
                var p = c.GetComponent<PlayerStatusNet>();
                if (p != null)
                    p.ItemController.SetBrochureAvailable(true);
            }

        }
    }

    public void StopGame()
    {
        if (isServer)
        {
            if (GameStarted)
            {
                var c = DynamicCar.GetComponent<CarMovementScriptNet>();
                var d = DynamicTruck.GetComponent<CarMovementScriptNet>();
                if (isClient)
                {
                    c.CanMove = false;
                    d.CanMove = false;
                }
                else
                {
                    c.OnCanMoveChanged(false);
                    d.OnCanMoveChanged(false);
                }

                /*
                    foreach (Transform v in Spawner.SpawnedVehicles)
                        {
                            v.GetComponent<NPCCarMovementScriptNet>().CanMove = false;
                        }
                        */
                if (isClient)
                    GameStarted = false;
                else
                    OnGameStartedChanged(false);
            }
        }
    }

    public void StartGame()
    {
        StartGame(null);
    }

    private int startRecived = 0;
    //eseguita sul server
    public void StartGame(PlayerStatusNet p)
    {
        if (isServer)
        {

            if (!GameStarted)
            {
                uint maxPlayers = GameManager.Instance.Server ? GameManager.Instance.MaxPlayers + 1 : GameManager.Instance.MaxPlayers;
                startRecived++;
                if (startRecived >= maxPlayers || GameManager.Instance.SinglePlayerExtended) //questa var  c'è solo sul server

                {

                    /* //possibile metodo di avvio scartato per possibile bug
                    bool ok = true;
                    foreach(var pl in players)
                    {
                        if(pl.Value.Ruolo == SpawnMode.None) // possibile bug: se il server non ha ricevuto i dati di entrambi i giocatori non parte ma gli arrivano poco dopo?
                        {
                            ok = false;
                        }
                    }
                    */


                    MenuDegliStart.gameObject.SetActive(false); //disabilito i menu start
                    StartAsServer.SetActive(false);
                    //Devo fillare tutti gli NPC!
                    ActivateNPCs();

                    RpcStartGameHideMenus();

                    var c = DynamicCar.GetComponent<CarMovementScriptNet>();
                    var d = DynamicTruck.GetComponent<CarMovementScriptNet>();
                    c.Initialize();
                    d.Initialize();
                    if (isClient)
                    {
                        c.CanMove = true;
                        d.CanMove = true;
                    }
                    else
                    {
                        c.OnCanMoveChanged(true);
                        d.OnCanMoveChanged(true);
                    }
                    /*
                    foreach (Transform v in Spawner.SpawnedVehicles)
                    {
                        v.GetComponent<NPCCarMovementScriptNet>().Initialize();
                        v.GetComponent<NPCCarMovementScriptNet>().CanMove = true;
                    }
                    */
                    //Spawner.StartSpawn();
                    if (!isClient)
                        DynamicCar.radio.TurnOn();
                    RpcTurnOnRadio();
                    //FuocoCamion.ForceBurn();
                    if (p != null)
                    {
                        if (isClient && playerController == p)
                        {
                            //StartGameButton.DisableElements();
                            Spawn.SpawnMenu.DisableElements();
                            Spawn.ExitMenu.EnableElements();
                        }
                        else
                            RpcDisableStartGame(p.netId);
                    }

                    if (isClient)
                        GameStarted = true;
                    else
                        OnGameStartedChanged(true);

                }

            }
        }
    }
    [ClientRpc]
    private void RpcStartGameHideMenus()
    {
        if (this.playerController.Ruolo == SpawnMode.Componente)
        {
            this.playerController.StartWalking();
            try
            {
                if (GameManager.Instance.Platform == ControllerType.MouseAndKeyboard)
                {
                    this.playerController.Spawner.CharacterMotor.enabled = true;
                    this.playerController.Spawner.CharacterMotor.canControl = true;
                }
                else
                {
                    this.playerController.Spawner.Swinger.SwingEnabled = true;
                }

            }
            catch
            {
                Debug.Log("Errore nel CharacterMotorStart del componente");
            }
        }
        MenuDegliStart.gameObject.SetActive(false);


    }



    [ClientRpc]
    private void RpcTurnOnRadio()
    {
        DynamicCar.radio.TurnOn();
        if (DynamicTruck.radio != null)
            DynamicTruck.radio.TurnOn();
    }

    /*public void StartGame(NetworkInstanceId id)
    {
        if (isServer)
        {
        if (!GameStarted)
            {
                var c = DynamicCar.GetComponent<CarMovementScriptNet>();
                c.Initialize();
                c.CanMove = true;
                foreach (Transform v in Spawner.SpawnedVehicles)
                {
                    v.GetComponent<NPCCarMovementScriptNet>().Initialize();
                    v.GetComponent<NPCCarMovementScriptNet>().CanMove = true;
                }
                //Spawner.StartSpawn();
                FuocoCamion.ForceBurn();
                StartTime = Time.time;
                StartGameButton.DisableElements();
                GameStarted = true;
            }
        }
        else
            CmdStartGame(id);
    }*/

    /*public void StartGame(NetworkInstanceId id)
    {
        if (isServer)
        {
            if (!GameStarted)
            {
                var c = DynamicCar.GetComponent<CarMovementScriptNet>();
                c.Initialize();
                c.CanMove = true;
                foreach (Transform v in Spawner.SpawnedVehicles)
                {
                    v.GetComponent<NPCCarMovementScriptNet>().Initialize();
                    v.GetComponent<NPCCarMovementScriptNet>().CanMove = true;
                }
                //Spawner.StartSpawn();
                FuocoCamion.ForceBurn();
                StartTime = Time.time;
                StartGameButton.DisableElements();
                GameStarted = true;
            }
        }
        else
            CmdStartGame(id);
    }*/

    /*[Command]
    private void CmdStartGame(NetworkInstanceId id)
    {
        if (!GameStarted)
        {
            var c = DynamicCar.GetComponent<CarMovementScriptNet>();
            c.Initialize();
            c.CanMove = true;
            foreach (Transform v in Spawner.SpawnedVehicles)
            {
                v.GetComponent<NPCCarMovementScriptNet>().Initialize();
                v.GetComponent<NPCCarMovementScriptNet>().CanMove = true;
            }
            //Spawner.StartSpawn();
            FuocoCamion.ForceBurn();
            StartTime = Time.time;
            
            RpcDisableStartGame(id);
            GameStarted = true;
        }
    }*/

    [ClientRpc]
    private void RpcDisableStartGame(NetworkInstanceId netid)
    {
        if (!isServer && playerController != null && playerController.netId == netid)
        {
            //StartGameButton.DisableElements();
            Spawn.SpawnMenu.DisableElements();
            Spawn.ExitMenu.EnableElements();
        }
    }

    public void StartIncendioTunnel()
    {
        if (isServer && FireSection != null)
            FireSection.StartFire();
    }

    public PlayerStatusNet GetPlayerController()
    {
        return playerController;
    }

    public NetworkInstanceId GetPlayerNetIDByID(uint id)
    {
        if (playerIDs.ContainsKey(id))
            return playerIDs[id];
        else
            return NetworkInstanceId.Invalid;
    }
    public Dictionary<NetworkInstanceId, PlayerStatusNet> GetPlayers()
    {
        return players;
    }
    public Dictionary<uint, NetworkInstanceId> GetPlayerIDs()
    {
        return playerIDs;
    }


    // Update is called once per frame
    void Update()
    {
        if (!isServer && spawnlater && playerController != null)
        {
            if (playerController.PlayerID == spawnpid)
            {
                playerController.transform.position = spawnpos;
                playerController.EnableListening();
            }
            spawnlater = false;
        }
        if (Input.GetKeyDown("m"))
        {
            this.playerController.ForceDie();
        }
        /*
        if (GameManager.Instance.DebugMode && GameStarted)
        {
            if (Input.GetKeyDown(KeyCode.T))
                TeleportCar();
            else if (Input.GetKeyDown(KeyCode.B))
                TeleportCarBack();
            if (Input.GetKeyDown(KeyCode.H))
                HideTruck();
            if (Input.GetKeyDown(KeyCode.L))
                HideSomeTunnel();
        }
        */
    }
    [Command]
    private void CmdTeleportCar()
    {
        TeleportCar();
    }

    private void TeleportCar()
    {
        if (isServer)
        {
            if (DynamicCar.LeftOccupant != NetworkInstanceId.Invalid)
            {
                PlayerStatusNet p = null;
                p = NetworkServer.FindLocalObject(DynamicCar.LeftOccupant).GetComponent<PlayerStatusNet>();
                if (p != null)
                {
                    if (p.isLocalPlayer)
                        DynamicCar.transform.position = new Vector3(DynamicCar.transform.position.x, DynamicCar.transform.position.y, 110);
                    else
                        RpcTeleportCar(DynamicCar.LeftOccupant);
                }
            }
        }
        else
            CmdTeleportCar();
    }
    [ClientRpc]
    private void RpcTeleportCar(NetworkInstanceId id)
    {
        if (playerController != null && playerController.netId == id)
            DynamicCar.transform.position = new Vector3(DynamicCar.transform.position.x, DynamicCar.transform.position.y, 110);
    }

    [Command]
    private void CmdTeleportCarBack()
    {
        TeleportCarBack();
    }

    private void TeleportCarBack()
    {
        if (isServer)
        {
            if (DynamicCar.LeftOccupant != NetworkInstanceId.Invalid)
            {
                PlayerStatusNet p = null;
                p = NetworkServer.FindLocalObject(DynamicCar.LeftOccupant).GetComponent<PlayerStatusNet>();
                if (p != null)
                {
                    if (p.isLocalPlayer)
                        DynamicCar.transform.position = new Vector3(DynamicCar.transform.position.x, DynamicCar.transform.position.y, -202.07f);
                    else
                        RpcTeleportCarBack(DynamicCar.LeftOccupant);
                }
            }
        }
        else
            CmdTeleportCar();
    }
    [ClientRpc]
    private void RpcTeleportCarBack(NetworkInstanceId id)
    {
        if (playerController != null && playerController.netId == id)
            DynamicCar.transform.position = new Vector3(DynamicCar.transform.position.x, DynamicCar.transform.position.y, -202.07f);
    }

    [Command]
    private void CmdHideTruck()
    {
        DynamicTruck.gameObject.SetActive(false);
        RpcHideTruck();
    }

    private void HideTruck()
    {
        if (isServer)
        {
            DynamicTruck.gameObject.SetActive(false);
            RpcHideTruck();
        }
        else
            CmdHideTruck();
    }
    [ClientRpc]
    private void RpcHideTruck()
    {
        DynamicTruck.gameObject.SetActive(false);
    }

    [Command]
    private void CmdHideSomeTunnel()
    {
        for (int i = 0; i < TunnelSections.Count; i++)
            if (TunnelSections[i].transform.position.z < DynamicCar.transform.position.z - 20 || TunnelSections[i].transform.position.z > DynamicCar.transform.position.z + 20)
                TunnelSections[i].gameObject.SetActive(false);
        RpcHideSomeTunnel();
    }

    private void HideSomeTunnel()
    {
        if (isServer)
        {
            for (int i = 0; i < TunnelSections.Count; i++)
                if (TunnelSections[i].transform.position.z < DynamicCar.transform.position.z - 20 || TunnelSections[i].transform.position.z > DynamicCar.transform.position.z + 20)
                    TunnelSections[i].gameObject.SetActive(false);
            RpcHideSomeTunnel();
        }
        else
            CmdHideSomeTunnel();
    }
    [ClientRpc]
    private void RpcHideSomeTunnel()
    {
        for (int i = 0; i < TunnelSections.Count; i++)
            if (TunnelSections[i].transform.position.z < DynamicCar.transform.position.z - 20 || TunnelSections[i].transform.position.z > DynamicCar.transform.position.z + 20)
                TunnelSections[i].gameObject.SetActive(false);
    }

    public float GetAmbientTemp()
    {
        return AmbientTemp;
    }

    public void ChangeWindSpeed(float speed)
    {
        if (isServer)
        {
            if (!isClient)
                WindZone.GetComponent<WindZone>().windMain = speed;
            RpcChangeWindSpeed(speed);
        }
        else
            CmdChangeWindSpeed(speed);
    }
    [Command]
    private void CmdChangeWindSpeed(float speed)
    {
        WindZone.GetComponent<WindZone>().windMain = speed;
        RpcChangeWindSpeed(speed);
    }
    [ClientRpc]
    private void RpcChangeWindSpeed(float speed)
    {
        WindZone.GetComponent<WindZone>().windMain = speed;
    }

    public void ChangeWindDirection(WindDirection dir)
    {
        if (isServer)
        {
            /*if (dir == WindDir || WindZone == null)
                return;
            if (dir == WindDirection.Favorable)
            {
                if (WindDir == WindDirection.Headwind)
                    WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 0, WindZone.rotation.eulerAngles.z);
                else if (WindDir == WindDirection.Windless)
                    WindZone.GetComponent<WindZone>().windMain = 0.5f;
            }
            else if (dir == WindDirection.Headwind)
            {
                if (WindDir == WindDirection.Favorable)
                    WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 180, WindZone.rotation.eulerAngles.z);
                else if (WindDir == WindDirection.Windless)
                    WindZone.GetComponent<WindZone>().windMain = 0.5f;
            }
            else if (dir == WindDirection.Windless)
            {
                WindZone.GetComponent<WindZone>().windMain = 0f;
            }*/
            if (isClient)
                WindDir = dir;
            else
                OnWindDirChanged(dir);
        }
        else
            CmdChangeWindDirection(dir);
    }
    [Command]
    private void CmdChangeWindDirection(WindDirection dir)
    {
        /*if (dir == WindDir || WindZone == null)
            return;
        if (dir == WindDirection.Favorable)
        {
            if (WindDir == WindDirection.Headwind)
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 0, WindZone.rotation.eulerAngles.z);
            else if (WindDir == WindDirection.Windless)
                WindZone.GetComponent<WindZone>().windMain = 0.5f;
        }
        else if (dir == WindDirection.Headwind)
        {
            if (WindDir == WindDirection.Favorable)
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 180, WindZone.rotation.eulerAngles.z);
            else if (WindDir == WindDirection.Windless)
                WindZone.GetComponent<WindZone>().windMain = 0.5f;
        }
        else if (dir == WindDirection.Windless)
        {
            WindZone.GetComponent<WindZone>().windMain = 0f;
            }*/
        if (isClient)
            WindDir = dir;
        else
            OnWindDirChanged(dir);
    }

    void OnWindDirChanged(WindDirection state)
    {
        if (WindZone != null)
        {
            if (state == WindDirection.Favorable)
            {
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 0, WindZone.rotation.eulerAngles.z);
                WindZone.GetComponent<WindZone>().windMain = 0.5f;
            }
            else if (state == WindDirection.Headwind)
            {
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 180, WindZone.rotation.eulerAngles.z);
                WindZone.GetComponent<WindZone>().windMain = 0.5f;
            }
            else if (state == WindDirection.Windless)
            {
                WindZone.GetComponent<WindZone>().windMain = 0f;
            }
        }
        WindDir = state;
    }




    //tesisti:
    public FireTruckControllerNPC NAFiretruck;
    public GestoreDannoFuoco GDF;

    //eseguita sul server

    public void StartedSmokeTruck() //inizio della procedura pompiere nel tunnel
    {
        NAFiretruck.inMovimento = true;
        GDF.InizioDannoDaFuoco();
        if (isServer)
        {
            NAFiretruck.OnChangeMovimento(true);
        }
        RpcStartedSmokeTruck();
        StartCoroutine(InizioSmoke3D(SmokeStartAfter));
    }


    public float SmokeStartAfter = 15f;
    [ClientRpc]
    void RpcStartedSmokeTruck()
    {
        if (this.playerController.Ruolo == SpawnMode.Componente)
        {
            //devo avvisare che sta partendo


            //Debug.LogError("Ho disattivato il movimento del pompiere iniziale");
            playerController.StarUscitaWaitingZone();
        }
        if (smoke3D != null)
        {
            StartCoroutine(InizioSmoke3D(SmokeStartAfter));
        }


        // playerController.VehicleID = NetworkInstanceId.Invalid;

    }

    public SmokeWeaveController SWC;
    private IEnumerator InizioSmoke3D(float time)
    {
        yield return new WaitForSeconds(time);
        this.StartSmokeFromFile();
        if (SWC != null)
        {
            SWC.StartSmoke();
        }
        else
        {
            Debug.Log("Non hai assegnato lo smokeWeaveCOntroller");
        }

        //bisogna far partire nuovo finto collider tossico
    }

    public void RuoloImpostato(PlayerSpawnerNet p, SpawnMode mode)
    {
        MenuDegliStart.RuoloImpostato(mode);
        if (RuoloScelto != null)
        {
            RuoloScelto.Invoke(mode);
        }
        //GameManager.Instance.TC.GetComponent<StageController>().Ruolo = mode;
    }

    public void StartSmokeFromFile()
    {
        if (smoke3D.enabled)
        { smoke3D.StartSmoke(); }

    }

    //chaiamata dal server
    private void ActivateNPCs()
    {

        //verifico ruoli mancanti
        List<SpawnMode> all = new List<SpawnMode>
        {
            SpawnMode.Componente,
            SpawnMode.Driver,
            SpawnMode.Passenger,
            SpawnMode.TruckDriver
        };

        foreach (var p in players)
        {
            switch (p.Value.Ruolo)
            {
                case SpawnMode.TruckDriver:

                    NPCCNet.DeActivateAustistaTruck();
                    break;
                case SpawnMode.Driver:
                    NPCCNet.DeActivateAustistaCar();
                    break;
                case SpawnMode.Componente:
                    NPCCNet.DeActivateComponenteVF();
                    break;


                default:
                    break;
            }
            if (all.Contains(p.Value.Ruolo))
            {
                all.Remove(p.Value.Ruolo);
            }

        }

        foreach (var ruoloMancante in all)
        {
            // GameManager.Instance.TCN.PlayerSpawned(PlayerStatus, mode);
            switch (ruoloMancante)
            {
                case SpawnMode.TruckDriver:

                    //NPCCNet.ActivateAustistaTruck();
                    if(!GameManager.Instance.NoCamionistaNPC)
                    {
                        NPCCNet.autistaTruck.GetComponent<NPCCiviliBaseNet>().visibleSkin = true;
                        NPCCNet.autistaTruck.GetComponent<NPCCiviliBaseNet>().CambioVisibilita(true);
                        NPCCNet.autistaTruck.GetComponent<NPCCiviliBaseNet>().RpcCambioVisibilita(true);                     
                    }
                    CamionNPC.StartCamionNPC();
                    //una volta serviva a più cose
                    StartCoroutine(PosizionaMezzoUsabile(18, DynamicTruck.gameObject, CamionNPC.gameObject)); //semplicemente abilita le interazioni del camion

                    break;
                case SpawnMode.Driver:
                    //NPCCNet.ActivateAustistaCar();
                    NPCCNet.autistaCar.GetComponent<NPCCiviliBaseNet>().visibleSkin = true;
                    NPCCNet.autistaCar.GetComponent<NPCCiviliBaseNet>().CambioVisibilita(true);
                    NPCCNet.autistaCar.GetComponent<NPCCiviliBaseNet>().RpcCambioVisibilita(true);
                    CarN.StartCamionNPC();
                    //rendo interagibile la macchina quando arriva in posizione
                    StartCoroutine(PosizionaMezzoUsabile(46, DynamicCar.gameObject, CarN.gameObject));//semplicemente abilita le interazioni della car
                    break;
                case SpawnMode.Componente:
                    //NPCCNet.ActivateComponenteVF();
                    break;


                default:
                    break;
            }
        }
    }



    [ClientRpc]
    private void RpcSetObjectPosition(GameObject p, Vector3 pos)
    {
        p.transform.position = pos;
    }

    [ClientRpc]
    private void RpcActivateNPC(GameObject p)
    {
        ActivateNPC(p);
    }

    private void ActivateNPC(GameObject p)
    {
        p.SetActive(true);
    }

    //public GameObject PosizioneCamionFinale;
    IEnumerator PosizionaMezzoUsabile(int time, GameObject g, GameObject gNPC)
    {


        yield return new WaitForSeconds(time);


        //rendo interagibile il camion
        var tmp = g.GetComponent<EnableTruckInteractionsNet>();
        if (tmp != null)
        {
            tmp.EnableInteractions();
        }
        var tmp2 = g.GetComponent<EnableCarInteractionsNet>();
        if (tmp2 != null)
        {
            tmp2.EnableInteractions();


        }

        //StopCoroutine(StopCoroutine);
    }

    /*
    IEnumerator PosizionaCarUsabile(int time)
    {
        yield return new WaitForSeconds(time);
        var pos = CarN.transform.position;

        //posiziono il camion per client e server
        //DynamicTruck.SetKinematic(true);
        //RpcImpostaKinematic(DynamicTruck.gameObject, true);

        DynamicCar.transform.position = pos;
        RpcSetObjectPosition(DynamicCar.gameObject, pos);
        var q = new Quaternion(0, 0, 0, 1);
        DynamicCar.gameObject.transform.rotation = q;
        RpcSetRotation(DynamicCar.gameObject, q);


        //rendo visibile l'oggetto per client e server
        DynamicCar.gameObject.SetActive(true);
        RpcActivateObject(DynamicCar.gameObject, true);

        //DynamicTruck.SetKinematic(false);
        // RpcImpostaKinematic(DynamicTruck.gameObject, false);

        //rendo interagibile il camion
        DynamicCar.GetComponent<EnableTruckInteractionsNet>().EnableInteractions();
        //devo forzare il fuoco
        //?
        //faccio sparire il vecchio camion
        CamionNPC.gameObject.SetActive(false);
        RpcActivateObject(CamionNPC.gameObject, false);
        StopAllCoroutines();
    }
    */

    private void RpcImpostaKinematic(GameObject g, bool state)
    {
        if (g != null)
        {
            g.GetComponent<Rigidbody>().isKinematic = state;
        }
    }

    public void RpcSetRotation(GameObject g, Quaternion q)
    {
        g.transform.rotation = q;
    }

    private DateTime inizioIncendio;
    public void InizioSpegnimentoIncendio(bool NPC)
    {
        inizioIncendio = DateTime.Now;
        StartCoroutine(SpegnimentoIncendio(NPC));

    }

    public event MyDelegate TuttiIFuochiSonoSpenti;
    [SyncVar]
    bool tuttiIFuochiSpenti = false;

    private bool timeCalcOnce = true;
    int giriSenzaFuoco = 0;
    private IEnumerator SpegnimentoIncendio(bool NPC)
    {

        while (true)
        {

            bool noneOn = true;
            foreach (var fire in fires)
            {
                if ((fire.Temperature > fire.IgnitionTemperature && !fire.burned) || fire.burning)
                {
                    if (NPC)
                    {
                        fire.SetTemperature(fire.Temperature - 35);
                    }

                    noneOn = false;
                    giriSenzaFuoco = 0;
                }

            }
            if (noneOn)
            {
                giriSenzaFuoco++;
                if (giriSenzaFuoco > 5)
                {
                    //tutti spenti!
                    giriSenzaFuoco = 0;
                    if (timeCalcOnce && (DateTime.Now - inizioIncendio).TotalMinutes <= 6)
                    {
                        timeCalcOnce = false;
                        if (isServer)
                            GameManager.Instance.TCN.GPN.SetIncendioSpentoEntro5Min(true, SpawnMode.Componente);
                        else
                            GameManager.Instance.TCN.GPN.CmdIncendioSpentoEntro5Min(true, SpawnMode.Componente);

                    }
                    tuttiIFuochiSpenti = true;
                    if (TuttiIFuochiSonoSpenti != null)
                    {
                        TuttiIFuochiSonoSpenti.Invoke();
                        TuttiIFuochiSonoSpenti = null;
                    }
                }

            }
            yield return new WaitForSeconds(1);
        }

    }

    public bool GameCanEndForComponente()
    {
        return tuttiIFuochiSpenti
            && once; //il once deriva dal aver iniziato a bruciare il camion
    }

    public event MyDelegate ComponenteEntraInGioco;
    public void ComponenteEsceTraining(PlayerStatusNet p)
    {
        if (ComponenteEntraInGioco != null)
        {
            ComponenteEntraInGioco.Invoke();
        }
    }

    public ControlloLuciIncendio controlloLuciTunnel;
    public void SpegniLuciArea()
    {
        controlloLuciTunnel.spegnimentoParziale = true;
        controlloLuciTunnel.FireStarted();
        RpcSpegniLuci(true);
    }
    public void SpegniLuciTunnel()
    {
        controlloLuciTunnel.spegnimentoParziale = false;
        controlloLuciTunnel.FireStarted();
        RpcSpegniLuci(false);
    }

    [ClientRpc]
    void RpcSpegniLuci(bool area)
    {
        controlloLuciTunnel.spegnimentoParziale = area;
        controlloLuciTunnel.FireStarted();
    }







}
