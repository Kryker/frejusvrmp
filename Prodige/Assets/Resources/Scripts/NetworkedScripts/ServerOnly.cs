﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerOnly : MonoBehaviour {

    //public VRButtonController InitBroad, CancelBroad, StartBroad;
    public Button InitBroad, CancelBroad, StartBroad;
    public Text Description, Description1;
    public JumpOnCameras JumpOnCam;
    string olddesc;

    // Use this for initialization
    void Start () {
        olddesc = Description1.text;
        Description.text += GameManager.Instance.NetworkManager.matchName + " Port=" + GameManager.Instance.NetworkManager.networkPort;
        Description1.text = "initialized (server)";
    }
    public void ClickStopServer()
    {
        GameManager.Instance.NetworkDiscovery.StopBroadcasting();
        GameManager.Instance.Server = false;
        GameManager.Instance.NetworkManager.StopServer();
    }

    public void ClickCancelBroadcasting()
    {
        GameManager.Instance.NetworkDiscovery.StopBroadcasting();
        Description1.text = olddesc;
        //InitBroad.enabled = true;
        InitBroad.gameObject.SetActive(true);
        CancelBroad.gameObject.SetActive(false);
    }
    public void ClickStartBroadcasting()
    {
        StartBroad.gameObject.SetActive(false);
        Description1.text = "initialized (server)";
        //CancelBroad.enabled = true;
        CancelBroad.gameObject.SetActive(true);
        GameManager.Instance.NetworkDiscovery.StartBroadcasting();
    }

    public void ClickInitBroad()
    {
        if (GameManager.Instance.NetworkDiscovery.Initialize())
        {
            Description1.text = "initialized";
            InitBroad.gameObject.SetActive(false);
            //StartBroad.enabled = true;
            StartBroad.gameObject.SetActive(true);
        }
    }

    public void ClickChangeWindDir()
    {
        GameManager.Instance.TCN.FlipWindDirection();
    }

    public void ClickNextCam()
    {
        JumpOnCam.NextCamera();
    }

    public void ClickReset()
    {
        GameManager.Instance.TCN.Reset();
    }

    public void ClickPrevCam()
    {
        JumpOnCam.PrevCamera();
    }
    // Update is called once per frame
    void Update () {
		
	}
}
