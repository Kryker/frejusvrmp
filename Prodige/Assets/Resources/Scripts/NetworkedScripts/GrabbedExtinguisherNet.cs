﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class GrabbedExtinguisherNet : GrabbableItemNet
{
    MeshRenderer PieceMeshRenderer = null;
    Material outlinedbutton = null;
    Material oldmat = null;
    public Transform Extinguisher;
    bool up = true;
    public NetworkInstanceId ExtID;
    float t = 0;
    [SyncVar]
    bool PulseEnabled = false;
    AdvancedStateMachineBehaviour buttonpressed, buttonunpressed;
    [SyncVar]
    bool firing;
    public SicuraExtNet sicuraExt;
    public ExtinguisherNet Ext;
    public NetworkAnimator nanimator;
    public Transform Button;
    public Shader Outline;
    public GenericItemSlave Sicura;
    public ControllerButton cbutton = ControllerButton.Trigger;
    public ParticleSystem extEmit;
    public float fuelduration = 30.0f;
    [HideInInspector]
    public SteamVR_TrackedController _controller;
    public AudioClip SecuredButtonUp, SecuredButtonDown;
    public AudioSource ExtinguisherSource;
    public AudioSource ButtonSource;
    [HideInInspector]
    public VibrationController vibr;
    public SecureState Secured = SecureState.Unknown;
    bool securesetup = false;
    
    public override void LoadState(GenericItemNet i)
    {
        SetExtID(i.netId);
        StopPulse();
        base.LoadState(Ext);
        nanimator.enabled = true;
        GetBehaviours();
    }

    public override void SetUp(ItemControllerNet ic, ControllerHand hand, int i)
    {
        base.SetUp(ic, hand, i);
        if (isServer)
        {
            if (ic != null && ic.Type == ItemControllerType.VR)
            {
                if (ic.isLocalPlayer)
                {
                    if (hand == ControllerHand.LeftHand)
                        vibr = ic.LeftController.GetComponent<VibrationController>();
                    else if (hand == ControllerHand.RightHand)
                        vibr = ic.RightController.GetComponent<VibrationController>();
                }
                else
                    RpcSetVibrationController(ic.GetComponent<NetworkIdentity>(), hand);
            }
        }
    }
    [ClientRpc]
    private void RpcSetVibrationController(NetworkIdentity netID, ControllerHand hand)
    {
        if (netID != null)
        {
            var ic = netID.GetComponent<ItemControllerNet>();
            if (ic != null && ic.Type == ItemControllerType.VR && ic.isLocalPlayer)
            {
                if (hand == ControllerHand.LeftHand)
                    vibr = ic.LeftController.GetComponent<VibrationController>();
                else if (hand == ControllerHand.RightHand)
                    vibr = ic.RightController.GetComponent<VibrationController>();
            }
        }
    }

    void SetExtID(NetworkInstanceId id)
    {
        if(id == NetworkInstanceId.Invalid)
        {
            if (isServer)
            {
                if (Ext != null)
                {
                    var c = Player.transform.GetComponent<PlayerStatusNet>().ItemController;
                    if (c != null && c.Type == ItemControllerType.FPS)
                        c.forcedtarget = null;                   

                    if (isClient)
                        Ext.Secured = Secured;
                    else
                        Ext.OnSecuredChanged(Secured);
                }
                SetSecured(SecureState.Unknown);
            }
            else if (Ext != null)
                SetSecured(SecureState.Unknown);
            
            Ext = null;
        }
        else
        {
            GameObject o = null;
            if(isServer)
                o = NetworkServer.FindLocalObject(id);
            else
                o = ClientScene.FindLocalObject(id);
            Ext = o.GetComponent<ExtinguisherNet>();
            SetSecured(Ext.Secured);
            Ext.OnBase = false;
        }
        ExtID = id;
    }

    void SetSecured(SecureState state)
    {
        if (!securesetup)
        {
            if (state == SecureState.Open)
            {
                sicuraExt.Hide();
                sicuraExt.RimuoviSicura();
                StartPulse();
                securesetup = true;
            }
            else if (state == SecureState.Closed)
            {
                StopPulse();
                sicuraExt.MettiSicura();
                if (Player != null && Player.Type == ItemControllerType.FPS /*&& Slave.gameObject.activeSelf*/)
                {
                    Player.forcedtarget = /*Slave.transform.GetChild(0); */sicuraExt.Slave.transform;      //test passamano
                }
                sicuraExt.Show();
                securesetup = true;
            }
        }
        else
        {
            if (state == SecureState.Open)
            {
                    sicuraExt.PlaySound();
                    sicuraExt.RimuoviSicura();
                    if (Player != null && Player.Type == ItemControllerType.FPS /*&& Slave.gameObject.activeSelf*/)
                    {
                        Player.forcedtarget = null;
                    }
                    StartPulse();
                }
            else if(state == SecureState.Closed)
                { 
                    StopPulse();
                    sicuraExt.MettiSicura();
                    if (Player != null && Player.Type == ItemControllerType.FPS /*&& Slave.gameObject.activeSelf*/)
                    {
                        Player.forcedtarget = /*Slave.transform.GetChild(0); */sicuraExt.Slave.transform;      //test passamano
                }
            }
            else
            {
                StopPulse();
                sicuraExt.MettiSicura();
                if (Player != null && Player.Type == ItemControllerType.FPS /*&& Slave.gameObject.activeSelf*/)
                {
                    Player.forcedtarget = null;
                }
                sicuraExt.Show();
                securesetup = false;
            }
        }
        Secured = state;
    }

    public void RimuoviSicura()
    {
        if (isServer && Ext != null)
            {
            if (!isClient)
                SetSecured(SecureState.Open);
            RpcSetSecured(SecureState.Open);
            }
    }

    [ClientRpc]
    private void RpcSetSecured(SecureState state)
    {
        SetSecured(state);
    }

    public override void SaveState()
    {
        if (isServer)
            nanimator.animator.SetBool("Pressed", false);
		if (vibr != null)
			vibr.StopVibration(0.05f, 0.65f, 0.04f, this);    

        SetExtID(NetworkInstanceId.Invalid);

        StopPulse();
        sicuraExt.StopPulse();
        sicuraExt.Reset();
        nanimator.enabled = false;
        base.SaveState();
    }

    public override void Initialize()
    {
        if (!initialized)
        {
            base.Initialize();
            if (initialized)
            {
                var i = Player.GetComponent<InputManagementNet>();

                if (Player.Type == ItemControllerType.VR)
                {
                    var ic = controller.GetComponent<ControllerManager>();
                    if (ic != null)
                    {
                        if (ic.Hand == ControllerHand.LeftHand)
                        {
                            i.OnLeftPadPressed += ClickButton;
                            i.OnLeftPadUnpressed += UnClickButton;
                        }
                        else if (ic.Hand == ControllerHand.RightHand)
                        {
                            i.OnRightPadPressed += ClickButton;
                            i.OnRightPadUnpressed += UnClickButton;
                        }
                    }
                }
                else
                {
                    var c1 = Player;
                    if (c1 != null)
                    {
                        if (controller == c1.LeftController)
                        {
                            i.OnLeftMouseClicked += ClickButton;
                            i.OnLeftMouseUnclicked += UnClickButton;
                        }
                        else if (controller == c1.RightController)
                        {
                            i.OnRightMouseClicked += ClickButton;
                            i.OnRightMouseUnclicked += UnClickButton;
                        }
                    }
                }
            }
        }
    }

    private void Awake()
    {
        PieceMeshRenderer = Button.GetComponent<MeshRenderer>();
        oldmat = PieceMeshRenderer.materials[0];
    }

    public override void Start()
    {
        if (isServer)
            firing = false;
        else
        {
            var c = extEmit.collision;
            c.sendCollisionMessages = false;
        }
        base.Start();
    }
    // Use this for initialization

    internal void ParticleDisabled(AdvancedStateMachineBehaviour a)
    {
        StopFire();
    }
    bool onlyOnce = false;
    internal void ParticleEnabled(AdvancedStateMachineBehaviour a)
    {
        StartFire();
        if(!onlyOnce && this.Player.GetComponent<PlayerStatusNet>().Ruolo == SpawnMode.Componente)
        {
            if(isServer)
                GameManager.Instance.TCN.GPN.SetEstintoreUsato(true, SpawnMode.Componente);
            else
                GameManager.Instance.TCN.GPN.CmdEstintoreUsato(true, SpawnMode.Componente);

            onlyOnce = true;
        }
    }
    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        if (Ext != null)
        { 
            if (Secured == SecureState.Open && !nanimator.animator.GetBool("Pressed"))
            {
                nanimator.animator.SetBool("Pressed", true);
                StopPulse();
            }
            else
                ButtonSource.Play();
        }
    }

    void GetBehaviours()
    {
        for (int i = 0; i < nanimator.animator.parameterCount; i++)
        {
            nanimator.SetParameterAutoSend(i, true);
        }
        var advancedbehaviours = nanimator.animator.GetBehaviours<AdvancedStateMachineBehaviour>();
        if (advancedbehaviours != null)
        {
            if (advancedbehaviours.Length > 0)
            {
                foreach (AdvancedStateMachineBehaviour a in advancedbehaviours)
                {
                    if (a.StateName == "Pressed")
                    {
                        buttonpressed = a;
                    }
                    else if (a.StateName == "UnPressed")
                    {
                        buttonunpressed = a;
                    }
                }
                buttonpressed.StateEnter += ParticleEnabled;
                buttonunpressed.StateEnter += ParticleDisabled;
            }
        }
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        if (Ext != null && nanimator.animator.GetBool("Pressed"))
        {
            nanimator.animator.SetBool("Pressed", false);
            StartPulse();
        }
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (initialized)
        {
            Pulse();
            if (Ext != null && !Ext.IsKinematic && (Ext.transform.parent != null && Ext.transform.parent != Ext.InitialParent))
            {
                Ext.currvelocity = (Slave.transform.position - Ext.prevpos) / Time.deltaTime;
                Ext.prevpos = Slave.transform.position;
            }
           
            if (isServer && firing)
            {
                if (Ext != null)
                {
                    var remainingtime = fuelduration * Ext.Fuel / 100;
                    remainingtime = remainingtime - Time.deltaTime;
                    if (remainingtime <= 0)
                    {
                        Ext.Fuel = 0;
                    }
                    else
                        Ext.Fuel = remainingtime / fuelduration * 100;
                    if (Ext.Fuel <= 0)
                    {
                        Ext.Fuel = 0.0f;
                        CancelInvoke("Openfire");
                        if (vibr != null)
                            vibr.StopVibration(0.05f, 0.65f, 0.04f, this);

                        extEmit.Stop();
                        ExtinguisherSource.Stop();
                        firing = false;
                    }
                }
                else
                {
                    CancelInvoke("Openfire");
                    if (vibr != null)
                        vibr.StopVibration(0.05f, 0.65f, 0.04f, this);

                    extEmit.Stop();
                    ExtinguisherSource.Stop();
                    firing = false;
                }
            }
        }
    }
    
    public void StartPulse()
    {
        if (!PulseEnabled)
        {
                if (outlinedbutton == null)
                {
                    outlinedbutton = new Material(Outline);
                    outlinedbutton.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                    outlinedbutton.SetColor("_OutlineColor", oldmat.GetColor("_OutlineColor"));
                    outlinedbutton.SetFloat("_Outline", oldmat.GetFloat("_Outline"));
                }
                var m = new Material[1];
                m[0] = outlinedbutton;
                PieceMeshRenderer.materials = m;
            PulseEnabled = true;
        }
    }
    public void StopPulse()
    {
        if (Slave != null && oldmat != null)
        {
            var m = new Material[1];
            m[0] = oldmat;

            PieceMeshRenderer.materials = m;
        }
        PulseEnabled = false;
    }

    void Pulse()
    {
        if (PulseEnabled)
        {
            if (Slave != null)
            {
                if (up)
                {
                    t += Time.deltaTime / 0.5f;
                    var m1 = PieceMeshRenderer.materials[0];
                    var c = m1.GetColor("_OutlineColor");
                    m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                    var m = new Material[1];
                    m[0] = m1;
                    PieceMeshRenderer.materials = m;
                    if (t >= 1)
                    {
                        up = false;
                        t = 0;
                    }
                }
                else
                {
                    t += Time.deltaTime / 0.5f;
                    var m1 = PieceMeshRenderer.materials[0];
                    var c = m1.GetColor("_OutlineColor");
                    m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                    var m = new Material[1];
                    m[0] = m1;
                    PieceMeshRenderer.materials = m;
                    if (t >= 1)
                    {
                        up = true;
                        t = 0;
                    }
                }
            }
        }
    }

  

    void Openfire()
    {
        extEmit.Play();
        ExtinguisherSource.Play();
        
        if(vibr != null)
            vibr.StartVibration(0.05f, 0.65f, 0.04f, this);
    }


    void StartFire()
    {
        if (Ext != null)
        {
            if (Slave.gameObject.activeSelf && Secured== SecureState.Open)
            {
                Invoke("Openfire", 0.1f);
                var ic = controller.GetComponent<ControllerManager>();
                if (ic != null)
                    ic.StopPulsePublic();
                firing = true;
            }
            else if (Slave.gameObject.activeSelf)
            {
                ButtonSource.Stop();
                ButtonSource.clip = SecuredButtonDown;
                ButtonSource.Play();
            }
        }

    }

    void StopFire()
    {
            if (Slave.gameObject.activeSelf && (Ext == null || Secured == SecureState.Open))
            {
            try
            {
                CancelInvoke("Openfire");
            }
            catch (Exception)
            {}
                extEmit.Stop();
                ExtinguisherSource.Stop();
                if (vibr != null)
                    vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
                if(isServer)
                    firing = false;
            }
            else if (Slave.gameObject.activeSelf)
            {
                ButtonSource.Stop();
                ButtonSource.clip = SecuredButtonUp;
                ButtonSource.Play();
            }
    }
}
