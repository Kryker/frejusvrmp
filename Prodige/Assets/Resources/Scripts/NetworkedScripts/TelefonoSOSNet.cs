﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class TelefonoSOSNet : GenericItemNet {
      
    public PulsanteChiamataSOSNet pulsante;
    Vector3 StartPos;
    public BaseTel Base;
    Quaternion StartRot;
    Transform StartParent;
    [HideInInspector]
    public ItemControllerNet Grabber;
    public Transform CableEnd;
    public CableComponent CableStart;
    [HideInInspector]
    public bool OnBase = true;
    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.SOSTelephone;
        StartPos = transform.position;
        StartRot = transform.rotation;
        //StartParent = transform.parent;
    }

    internal void ReBase()
    {
        //transform.parent = StartParent;
        transform.position = StartPos;
        transform.rotation = StartRot;
        Drop();
        Base.DisableOutline();
    }

    public override void Start()
    {
        base.Start();
    }

    internal void Drop()
    {
        base.DropParent();
    }


    // Update is called once per frame
    public override void Update()
    {

    }

    public override void DropParent()
    {
        if (OnBase)
            ReBase();
        else
            Drop();
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
    }
    

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }
   
    public override void Reset()
    {
        SetCanInteract(false);
    }

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        Grabber = c;
    }
}
