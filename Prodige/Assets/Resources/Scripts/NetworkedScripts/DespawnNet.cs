﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DespawnNet : NetworkBehaviour
{

    public CarSpawnNet spawn;
    public bool Hide = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (isServer && other.tag == "NPC" && other.transform.GetComponent<VehicleDataNet>() != null)
        {
            if (spawn.RemoveVehicle(other.transform))
            {
                if (Hide)
                    HideVehicle();
                else
                    Destroy(other.gameObject);

            }
        }
    }

    private void HideVehicle()
    {
        RpcHideVehicle();
        if (!isClient)
            gameObject.SetActive(false);
    }
    [ClientRpc]
    private void RpcHideVehicle()
    {
        gameObject.SetActive(false);
    }
}
