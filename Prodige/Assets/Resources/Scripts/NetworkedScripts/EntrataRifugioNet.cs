﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EntrataRifugioNet : GenericItemNet
{

    public List<AudioClip> clips;
    public PortaRifugioNet Porta;
    public NetworkAnimator nanimator;
    public AudioSource audiosource;
    public bool Interna = true;
    public EntrataRifugioNet OtherDoor;
    bool toplay = false;
    public TunnelShelterSectionControllerNet rifugio;
    [SyncVar(hook = "OnOperatoreIDChanged")]
    [HideInInspector]
    public NetworkInstanceId operatoreid;
    PlayerStatusNet operatore;
    [HideInInspector]
    [SyncVar]
    public bool opening;
    bool getbehaviourslater = false;

    [HideInInspector]
    [SyncVar]
    SyncListUInt Indoor;


    void OnOperatoreIDChanged(NetworkInstanceId id)
    {
        if (id != NetworkInstanceId.Invalid)
        {
            GameObject p = null;
            if (isServer)
                p = NetworkServer.FindLocalObject(id);
            else
                p = ClientScene.FindLocalObject(id);
            if (p != null)
                operatore = p.GetComponent<PlayerStatusNet>();
            else
            {
                id = NetworkInstanceId.Invalid;
                operatore = null;
            }
        }
        else
            operatore = null;
        operatoreid = id;
    }
    private void Awake()
    {
        Indoor = new SyncListUInt();
        if (Interna)
            ItemCode = ItemCodes.ShelterInternalDoors;
        else
            ItemCode = ItemCodes.ShelterExternalDoors;
        if (audiosource == null)
        {
            var s = transform.Find("Source");
            if (s != null)
                audiosource = s.GetComponent<AudioSource>();
        }
    }

    public override void Start()
    {
        if (isServer)
            opening = false;
        else if (opening)
            nanimator.animator.SetBool("OpenManiglia", true);

        for (int i = 0; i < nanimator.animator.parameterCount; i++)
        {
            nanimator.SetParameterAutoSend(i, true);
        }

        if (GetBehaviors())
        {
            if (Interna)
            {
                Porta.ManigliaOpening.StateEnter += ManigliaOpening;
                Porta.ManigliaOpening.StatePercentagePlayed += OpenDoor;
            }
            else
            {
                Porta.ManigliaPressed.StateEnter += ManigliaOpening;
                Porta.ManigliaPressed.StatePercentagePlayed += OpenDoor;
                Porta.ManigliaPressed.StatePercentagePlayed += ManigliaClose;
            }

            Porta.PortaOpen.StateEnter += DoorOpened;
            if (Interna)
                Porta.PortaOpen.StatePercentagePlayed += ManigliaOpened;
            Porta.PortaOpen.StatePlayed += ReCloseDoor;
            Porta.PortaClose.StateEnter += DoorClosing;
            Porta.PortaClose.StatePlayed += DoorClosed;
            Porta.PortaClose.StatePercentagePlayed += DoorCloseSound;
        }
        else
            getbehaviourslater = true;

        if (isServer)
        {
            if (isClient)
                operatoreid = NetworkInstanceId.Invalid;
            else
                OnOperatoreIDChanged(NetworkInstanceId.Invalid);
        }
        else
            OnOperatoreIDChanged(operatoreid);
        operatore = null;
        base.Start();
    }

    private bool GetBehaviors()
    {
        var advancedbehaviours = nanimator.animator.GetBehaviours<AdvancedStateMachineBehaviour>();
        if (advancedbehaviours == null || advancedbehaviours.Length == 0)
            return false;

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            var layername = "PortaLayer";
            if (ad.Layer == layername)
            {
                if (ad.StateName == "Closed")
                {
                    Porta.PortaClose = ad;
                }
                else if (ad.StateName == "Opened")
                {
                    Porta.PortaOpen = ad;
                }
            }
            layername = "ManigliaLayer";
            if (ad.Layer == layername)
            {
                if (!Interna)
                {
                    if (ad.StateName == "Pressed")
                    {
                        Porta.ManigliaPressed = ad;
                    }
                    else if (ad.StateName == "Unpressed")
                    {
                        Porta.ManigliaUnpressed = ad;
                    }
                }
                else
                {
                    if (ad.StateName == "Opening")
                    {
                        Porta.ManigliaOpening = ad;
                    }
                    else if (ad.StateName == "Closing")
                    {
                        Porta.ManigliaClosing = ad;
                    }
                    else if (ad.StateName == "Idle")
                    {
                        Porta.ManigliaIdle = ad;
                    }
                }
            }
        }
        return true;
    }

    public bool IsPlayerInside(NetworkInstanceId netid)
    {
        GameObject g = null;
        if (isServer)
            g = NetworkServer.FindLocalObject(netid);
        else
            g = ClientScene.FindLocalObject(netid);
        if (g != null)
        {
            var id = g.GetComponent<PlayerStatusNet>().PlayerID;
            if (id != uint.MaxValue && Indoor.Contains(id))
                return true;
            else
                return false;
        }
        else
            return false;
    }
    IEnumerator PlayAudio(AudioClip clip)
    {
        yield return new WaitUntil(() => !toplay);
        if (audiosource.isPlaying)
        {
            toplay = true;
            yield return new WaitWhile(() => audiosource.isPlaying);
        }
        audiosource.clip = clip;
        audiosource.Play();
        yield return new WaitWhile(() => audiosource.isPlaying);
        toplay = false;
    }

    private void ManigliaOpening(AdvancedStateMachineBehaviour a)
    {
        if (isServer)
            SetCanInteract(false, operatore.ItemController);
        StartCoroutine(PlayAudio(clips[0]));
    }

    private void OpenDoor(AdvancedStateMachineBehaviour a)
    {
        if (isServer)
            nanimator.animator.SetBool("OpenPorta", true);
    }

    private void ManigliaClose(AdvancedStateMachineBehaviour a)
    {
        if (isServer)
            nanimator.animator.SetBool("OpenManiglia", false);
        StartCoroutine(PlayAudio(clips[2]));
    }

    private void DoorOpened(AdvancedStateMachineBehaviour a)
    {
        if (isServer)
            opening = true;

        Porta.gameObject.layer = LayerMask.NameToLayer("MovingItem");
        StartCoroutine(PlayAudio(clips[1]));

        if (GameManager.Instance.TCN.GetPlayerController() != null)
        {
            var id = GameManager.Instance.TCN.GetPlayerController().netId;
            if (Interna && IsPlayerInside(id))
            {
                rifugio.OpenInternalDoorInside(0.1f);
            }
            else if (!Interna && IsPlayerInside(id))
            {
                rifugio.OpenExternalDoorInside(0.1f);
            }
            else if (Interna && !IsPlayerInside(id))
            {
                rifugio.OpenInternalDoorOutside(0.1f);
            }
            else if (!Interna && !IsPlayerInside(id))
            {
                rifugio.OpenExternalDoorOutside(0.1f);
            }
        }

        if (isServer)
        {
            ItemActive = true;
        }
    }

    private void ManigliaOpened(AdvancedStateMachineBehaviour a)
    {
        if (isServer)
            nanimator.animator.SetBool("OpenManiglia", false);
        StartCoroutine(PlayAudio(clips[2]));
    }

    private void ReCloseDoor(AdvancedStateMachineBehaviour a)
    {
        if (isServer)
            StartCoroutine(DelayedCloseDoor());
    }

    IEnumerator DelayedCloseDoor()
    {
        yield return new WaitForSeconds(4);
        nanimator.animator.SetBool("OpenPorta", false);
    }

    private void DoorClosing(AdvancedStateMachineBehaviour a)
    {
        if (isServer && operatore != null)
        {
            SetCanInteract(false, operatore.ItemController);
            opening = false;
        }
        StartCoroutine(PlayAudio(clips[4]));
    }

    private void DoorClosed(AdvancedStateMachineBehaviour a)
    {
        if (GameManager.Instance != null && GameManager.Instance.TCN.GetPlayerController() != null)
        {
            var id = GameManager.Instance.TCN.GetPlayerController().netId;
            if (Interna && IsPlayerInside(id))
            {
                rifugio.CloseInternalDoorInside(0.1f);
            }
            else if (!Interna && IsPlayerInside(id))
            {
                rifugio.CloseExternalDoorInside(0.1f);
            }
            else if (Interna && !IsPlayerInside(id))
            {
                rifugio.CloseInternalDoorOutside(0.1f);
            }
            else if (!Interna && !IsPlayerInside(id))
            {
                rifugio.CloseExternalDoorOutside(0.1f);
            }
        }
        Porta.gameObject.layer = LayerMask.NameToLayer("Default");
        if (isServer && operatore != null)
        {

            SetCanInteract(true, operatore.ItemController);
            if (isClient)
                operatoreid = NetworkInstanceId.Invalid;
            else
                OnOperatoreIDChanged(NetworkInstanceId.Invalid);
            foreach (uint n in Indoor)
                rifugio.EnterShelter(n);
            if (Interna)
            {
                //GameManager.Instance.TCN.EndGame();
            }

        }
        if (isServer)
        {
            ItemActive = false;
        }
    }


    private void DoorCloseSound(AdvancedStateMachineBehaviour a)
    {
        if (Interna)
            StartCoroutine(PlayAudio(clips[3]));
        else
            StartCoroutine(PlayAudio(clips[2]));
    }



    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            if (!ItemActive)                    //se chiusa
                nanimator.animator.SetBool("OpenManiglia", true);
            else                                 //se aperta
                nanimator.animator.SetBool("OpenPorta", false);
            if (isClient)
                operatoreid = c.netId;
            else
                OnOperatoreIDChanged(c.netId);
        }
    }




    // Update is called once per frame
    public override void Update()
    {
        if (getbehaviourslater && GetBehaviors())
            getbehaviourslater = false;
    }

    public override void SetCanInteract(bool can, ItemControllerNet c)
    {
        Porta.SetCanInteract(can, c);
        base.SetCanInteract(can, c);
    }


    public override void Reset()
    {
        Porta.Reset();
        Indoor.Clear();
        if (isServer)
            ItemActive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isServer)
        {
            if (other.tag == "Player")
            {
                if (Interna)
                {
                    other.GetComponent<StageControllerNet>().ShelterReached = true;
                    rifugio.Reached.Add(other.GetComponent<PlayerStatusNet>());
                    GameManager.Instance.TCN.EndGame();
                }
                var id = other.GetComponent<PlayerStatusNet>().PlayerID;
                if (!Indoor.Contains(id))
                    Indoor.Add(id);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isServer)
        {
            if (other.tag == "Player")
            {
                if (Interna)
                {
                    //other.GetComponent<StageControllerNet>().ShelterReached = false;
                    rifugio.Reached.Remove(other.GetComponent<PlayerStatusNet>());
                    var id = other.GetComponent<PlayerStatusNet>().PlayerID;
                    GameManager.Instance.TCN.ExitShelter(id);
                }
                if (IsPlayerInside(other.GetComponent<PlayerStatusNet>().netId))
                {
                    var id = other.GetComponent<PlayerStatusNet>().PlayerID;
                    if (Indoor.Contains(id))
                        Indoor.Remove(id);
                }
            }
        }
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override void ClickButton(object sender, ClickedEventArgs e)
    { }
}
