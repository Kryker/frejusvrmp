﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class OverriddenNetworkDiscovery : NetworkDiscovery
{
    MainMenu Menu;
    [HideInInspector]
    public bool broadcasting;

    public bool StartBroadcasting()
    {
            if (StartAsServer())
            {
                broadcasting = true;
                return true;
            }
        return false;
    }
    public bool StartListening()
    {
            if (StartAsClient())
            {
                broadcasting = true;
                return true;
            }
        return false;
    }

    public void StopBroadcasting()
    {
        if (broadcasting)
        {
            StopBroadcast();
            broadcasting = false;
        }
    }

    public override void OnReceivedBroadcast(string fromAddress, string data)
    {
        StopBroadcasting();
        NetworkManager.singleton.networkAddress = fromAddress;
        NetworkManager.singleton.StartClient();
        if (Menu == null)
        {
            var m = GameObject.FindGameObjectWithTag("MainMenuUI");
            if (m != null)
            {
                Menu = m.GetComponent<MainMenu>();
                Menu.ClickStopListening();
                Menu.Description.text += GameManager.Instance.NetworkManager.networkAddress + ":" + GameManager.Instance.NetworkManager.networkPort + "..";
                /*Menu.Manager.client.RegisterHandler(MsgType.Disconnect, Menu.Disconnect);
                Menu.Manager.client.RegisterHandler(MsgType.Connect, Menu.Connect);*/
                Menu.NetworkManagerHUD.DisableElements();
                Menu.ClientConnectPage.EnableElements();
            }
        }
        else
        {
            Menu.ClickStopListening();
            Menu.Description.text += GameManager.Instance.NetworkManager.networkAddress + ":" + GameManager.Instance.NetworkManager.networkPort + "..";
            /*Menu.Manager.client.RegisterHandler(MsgType.Disconnect, Menu.Disconnect);
            Menu.Manager.client.RegisterHandler(MsgType.Connect, Menu.Connect);*/
            Menu.NetworkManagerHUD.DisableElements();
            Menu.ClientConnectPage.EnableElements();
        }
    }
}