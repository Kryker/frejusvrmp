﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CiaoNet : NetworkBehaviour
{
    NetworkAnimator nanimator;
    public EntrataRifugioNet entratanet;
    bool salutato = false;
    AdvancedStateMachineBehaviour state;

    // Use this for initialization
    void Start()
    {
        nanimator = GetComponent<NetworkAnimator>();
        for (int i = 0; i < nanimator.animator.parameterCount; i++)
            nanimator.SetParameterAutoSend(i, true);

        if (isServer && entratanet != null)
        {
            entratanet.Porta.PortaClose.StatePercentagePlayed += Wave;
            state = nanimator.animator.GetBehaviour<AdvancedStateMachineBehaviour>();
            state.StatePlayed += StopWaving;
        }
    }

    private void StopWaving(AdvancedStateMachineBehaviour a)
    {
        if (nanimator.animator.GetBool("Wave"))
            nanimator.animator.SetBool("Wave", false);
    }

    private void Wave(AdvancedStateMachineBehaviour a)
    {
        if (!salutato && entratanet != null && entratanet.IsPlayerInside(entratanet.operatoreid))
        {
            nanimator.animator.SetBool("Wave", true);
            salutato = true;
            if (entratanet != null)
                entratanet.Porta.PortaClose.StatePercentagePlayed -= Wave;
        }
    }

    internal void Reset()
    {
        if(isClient)
            salutato = false;
        RpcReset();
    }
    [ClientRpc]
    private void RpcReset()
    {
        salutato = false;
    }
}
