﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public enum NicheType { Tel, Ext };
public class NicchiaNet : GenericItemNet {
    
    public List<Collider> ContainedItems;
    [HideInInspector]
    public BoxCollider Interno;
    public NetworkAnimator nanimator;
    public List<AudioClip> clips;
    public int Index;
    public NicheType tipo;
    [HideInInspector]
    [SyncVar]
    public bool opened;
    public Posizione posizione;
    
    public AudioSource Source;
    AdvancedStateMachineBehaviour AntaOpen, AntaClose;
    AdvancedStateMachineBehaviour SerraturaOpen, SerraturaClose;
    bool toplay = false;
    public override void Start () {
        if (isServer)
            opened = false;
        else if (opened)
            nanimator.animator.SetBool("OpenSerratura" + Index, true);

        for (int i = 0; i < nanimator.animator.parameterCount; i++)
        {
            nanimator.SetParameterAutoSend(i, true);
        }
        
        var advancedbehaviours = nanimator.animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            var layername = "Anta" + Index + "Layer";
            if (ad.Layer == layername)
            {
                if (ad.StateName == "Closed")
                {
                    AntaClose = ad;
                }
                else if (ad.StateName == "Opened")
                {
                    AntaOpen = ad;
                }
            }
            else
            {
                layername = "Serratura" + Index + "Layer";
                if (ad.Layer == layername)
                {
                    if (ad.StateName == "Closed")
                    {
                        SerraturaClose = ad;
                    }
                    else if (ad.StateName == "Opened")
                    {
                        SerraturaOpen = ad;
                    }
                }
            }
        }
        AntaOpen.StateEnter += DoorOpen;
        AntaOpen.StatePlayed += DoorOpened;
        SerraturaOpen.StatePercentagePlayed += LockOpened;
        AntaClose.StatePercentagePlayed += DoorClosed;
        AntaClose.StateEnter += DoorClose;
        SerraturaClose.StatePlayed += LockClosed;
        
        base.Start();
	}

    public override void ClickButton(object sender)//object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
        Source.Play();
    }

    private void Awake()
    {
        ItemCode = ItemCodes.Niche;
        Interno = nanimator.animator.GetComponents<BoxCollider>()[Index];
    }

    internal void PlayClip(int v)
    {
        StartCoroutine(PlayAudio(clips[v]));
    }

    public override void UnClickButton(object sender)//object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
    }

    IEnumerator PlayAudio(AudioClip clip)
    {
        yield return new WaitUntil(() => !toplay);
        if (Source.isPlaying)
        {
            toplay = true;
            yield return new WaitWhile(() => Source.isPlaying);
        }
        Source.clip = clip;
        Source.Play();
        yield return new WaitWhile(() => Source.isPlaying);
        toplay = false;
    }

    internal void DoorOpened(AdvancedStateMachineBehaviour st)
    {
        if (isServer)
        {
            opened = true;
            SetCanInteract(true);
            ItemActive = true;
        }
    }

    internal void LockOpened(AdvancedStateMachineBehaviour st)
    {
        if (isServer)
            MakeAllItemsGrabbable();
        OpenDoor();
    }

    internal void DoorClosed(AdvancedStateMachineBehaviour st)
    {
        if (isServer)
        {
            MakeAllItemsUngrabbable();
            opened = false;
        }
        CloseLock();
    }

    internal void LockClosed(AdvancedStateMachineBehaviour st)
    {
        if (isServer)
        {
            SetCanInteract(true);
            ItemActive = false;
        }
    }
    
    internal void DoorOpen(AdvancedStateMachineBehaviour a)
    {
        PlayClip(0);
    }


    internal void DoorClose(AdvancedStateMachineBehaviour a)
    {
        PlayClip(0);
    }

    internal void LockClose(AdvancedStateMachineBehaviour a)
    {
        PlayClip(1);
    }

    internal void LockOpen(AdvancedStateMachineBehaviour a)
    {
        PlayClip(1);
    }

    internal void OpenDoor()
    {
        if(isServer)
            nanimator.animator.SetBool("OpenAnta" + Index, true);
    }

    internal void CloseLock()
    {
        if (isServer)
            nanimator.animator.SetBool("OpenSerratura" + Index, false);
    }

    

    void MakeItemUngrabbable(Transform item)
    {
        StartCoroutine(LazyMakeItemUngrabbable(item));
    }
    IEnumerator LazyMakeItemUngrabbable(Transform item)
    {
        yield return new WaitForSeconds(5);
        var g = item.GetComponent<GenericItemNet>();
        if (g != null)
        {
            if (g.Grabbable && g.ItemCode != ItemCodes.SOSTelephone)
                g.DisablePhysics();
            g.SetCanInteract(false);
        }
        else
        {
            var s = item.GetComponent<GenericItemSlave>();
            if (s != null)
            {
                if (s.MasterNet.Grabbable && s.MasterNet.ItemCode != ItemCodes.SOSTelephone)
                    s.MasterNet.DisablePhysics();
                s.MasterNet.SetCanInteract(false);
            }
        }
    }


    void MakeItemGrabbable(Transform item)
    {
        var g = item.GetComponent<GenericItemNet>();
        if (g != null)
        {
            g.SetCanInteract(true);
            if (g.Grabbable && g.ItemCode != ItemCodes.SOSTelephone)
                g.EnablePhysics();
        }
        else
        {
            var s = item.GetComponent<GenericItemSlave>();
            if (s != null)
            {
                s.MasterNet.SetCanInteract(true);
                if (s.MasterNet.Grabbable && s.MasterNet.ItemCode != ItemCodes.SOSTelephone)
                    s.MasterNet.EnablePhysics();
            }
        }
    }
    

    public void MakeAllItemsGrabbable()
    {
        List<Collider> ItemsToKeep = new List<Collider>();
        foreach (Collider item in ContainedItems)
        {
            if (Interno.bounds.Intersects(item.bounds))
                {
                MakeItemGrabbable(item.transform);
                ItemsToKeep.Add(item);
                }
        }
        if (ItemsToKeep.Count < ContainedItems.Count)
        {
            var c = ContainedItems;
            ContainedItems = ItemsToKeep;
            c.Clear();
        }
    }

    public void MakeAllItemsUngrabbable()
    {
        List<Collider> ItemsToKeep = new List<Collider>();
        foreach (Collider item in ContainedItems)
        {
            if (Interno.bounds.Intersects(item.bounds))
                { 
                MakeItemUngrabbable(item.transform);
                ItemsToKeep.Add(item);
                }
        }
        if (ItemsToKeep.Count < ContainedItems.Count)
        {
            var c = ContainedItems;
            ContainedItems = ItemsToKeep;
            c.Clear();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isServer)
        {
            var i = other.GetComponent<GenericItemNet>();
            if (i != null)
            {
                if(i.Grabbable && Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other))
                {
                    ContainedItems.Add(other);
                    return;
                }
            }
            else
            {
                var isl = other.GetComponent<GenericItemSlave>();
                if (isl != null && isl.MasterNet != null)
                {
                    if (isl.MasterNet.Grabbable && Interno.bounds.Intersects(other.bounds) && !ContainedItems.Contains(other))
                    {
                        ContainedItems.Add(other);
                        return;
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isServer)
        {
            var i = other.GetComponent<GenericItemNet>();
            if (i != null)
            {
                if(i.Grabbable && ContainedItems.Contains(other))
                    {
                        ContainedItems.Remove(other);
                        return;
                    }
                }
            else
            {
                var isl = other.GetComponent<GenericItemSlave>();
                if (isl != null)
                {
                    if (isl.MasterNet.Grabbable && ContainedItems.Contains(other))
                    {
                        ContainedItems.Remove(other);
                        return;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    public override void Update()
    {

    }

    public override void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        Interact(c, hand);
    }

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (!ItemActive)
        {
            nanimator.animator.SetBool("OpenSerratura" + Index, true);
        }
        else
        {
            nanimator.animator.SetBool("OpenAnta" + Index, false);
        }
    }


    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {}
}
