﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class SmartDestroyNet : NetworkBehaviour
    {
    //Script made by: Mick Boere

    public float maxPerSecond = 3;
    public List<GameObject> list;

    public override void OnStartServer()
    {
        list = new List<GameObject>();
        InvokeRepeating("Destroy", 0, 1 / maxPerSecond);
    }

    void Add(GameObject obj)
        {
        if (isServer && obj != null)
        {
            var n = obj.GetComponent<NetworkInstanceId>();
            if (n != NetworkInstanceId.Invalid)
            {
                list.Add(obj);
                obj.SetActive(false);
            }
        }
        }

    void Destroy()
    {
        if (isServer)
        {
            if (list.Count != 0)
            {
                Destroy(list[list.Count - 1]);
                list.Remove(list[list.Count - 1]);
            }
        }
    }
    }