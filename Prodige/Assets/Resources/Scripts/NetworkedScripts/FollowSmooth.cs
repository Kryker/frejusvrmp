﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSmooth : MonoBehaviour {

    public Vector3 OffsetPos;
    public Vector3 OffsetRot;
    Quaternion StartTargetRot;
    float t = 0;
    public float SmoothFactor = 1.0f;
    public Transform Target
    {
        get { return _target; }
        set
        {
            if (value == null)
            {
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
                StartTargetRot = Quaternion.identity;
                t = 0;
            }
            else
            {
                transform.localPosition = OffsetPos;
                transform.localEulerAngles = OffsetRot;
                StartTargetRot = value.rotation;
                t = 0;
            }
            _target = value;
        }
    }
    Transform _target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(_target != null)
        {
            t += Time.deltaTime * SmoothFactor;

            var parentMatrix = Matrix4x4.TRS(_target.position, _target.rotation, _target.lossyScale);
            var targetpos = parentMatrix.MultiplyPoint3x4(OffsetPos);
            var targetrot = (_target.rotation * Quaternion.Inverse(StartTargetRot)) * Quaternion.Euler(OffsetRot);

            transform.position = Vector3.Lerp(transform.position, targetpos, t);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetrot, t);
        }
	}
}
