﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlayerStatusColliderNet : MonoBehaviour {
    PlayerStatusNet Status;
	void Start () {
        Status = GetComponent<PlayerStatusNet>();
	}

    public void ApplyDamageWithPain(float damage)
    {
        if(Status != null && Status.isServer)
            StartCoroutine(Status.ApplyDamageWithPain(damage));
    }

    public void ApplyDamageWithoutPain(float damage)
    {
        if (Status != null && Status.isServer)
            StartCoroutine(Status.ApplyDamageWithoutPain(damage));
    }
}
