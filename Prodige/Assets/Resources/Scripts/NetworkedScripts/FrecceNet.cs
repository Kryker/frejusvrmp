﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(AudioSource))]
public class FrecceNet : GenericItemNet {
    
    public Vector3 ButtonPositionAction;
    Vector3 ButtonPositionRest;
    public List<AudioClip> clips;
    public AudioSource audiosource;
    VehicleDataNet Vehicle;
    [SyncVar(hook = "OnArrowStateChanged")]
    bool ArrowState;

    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.Arrows;
        if(Slave == null)
            ButtonPositionRest = transform.localPosition;
        else
            ButtonPositionRest = Slave.transform.localPosition;
        Vehicle = GetComponent<VehicleDataNet>();
    }

    public override void Start()
    {
        base.Start();
        if(isServer)
        {
            if(isClient)
                ArrowState = false;
            else
                OnArrowStateChanged(false);
        }
        else
            OnArrowStateChanged(ArrowState);
    }

    void OnArrowStateChanged(bool value)
    {
        if(value)
        {
            ButtonPressed();
            Vehicle.StartArrows();
        }
        else
        {
            ButtonUnpressed();
            Vehicle.StopArrows();
        }
        ArrowState = value;
    }
    // Update is called once per frame
    public override void Update()
    {

    }
    public override void ClickButton(object sender)//object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
        ButtonPressed();
    }
    /*public override void ClickButton(object sender, ClickedEventArgs e)
    {
        ButtonPressed();
    }*/

    void ButtonPressed()
    {
        /*
        if (Slave == null)
            transform.localPosition = ButtonPositionAction;
        else
            Slave.transform.localPosition = ButtonPositionAction;
            */
    }

    public override void UnClickButton(object sender)//object sender, ClickedEventArgs e)
    {
        ButtonUnpressed();
    }
    /*public override void UnClickButton(object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
        ButtonUnpressed();
    }*/
    internal void ArrowsOn()
    {
        if(Slave != null)
        {
            var mr = Slave.GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 3);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        else
        {
            var mr = GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 3);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        audiosource.clip = clips[0];
        audiosource.Play();
    }

    void ButtonUnpressed()
    {
        if (Slave == null)
            transform.localPosition = ButtonPositionRest;
        else
            Slave.transform.localPosition = ButtonPositionRest;
    }
    public override void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        ItemActive = !ItemActive;
        if(isServer)
        {
            GameManager.Instance.TCN.GPN.SetArrowsOn(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);

        }
        else
        {
            GameManager.Instance.TCN.GPN.CmdSetArrowsOn(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);

        }
        if (isClient)
        {
            ArrowState = ItemActive;
        }
            
        else
            OnArrowStateChanged(ItemActive);
    }
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        ItemActive = !ItemActive;
        if (isServer)
        {
            GameManager.Instance.TCN.GPN.SetArrowsOn(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);

        }
        else
        {
            GameManager.Instance.TCN.GPN.CmdSetArrowsOn(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);

        }
        if (isClient)
        {
            ArrowState = ItemActive;
        }
           
        else
            OnArrowStateChanged(ItemActive);

    }
    public override void Reset()
    {
        if(isClient)
            ArrowState = false;
        else
            OnArrowStateChanged(false);
        ItemActive = false;
        base.Reset();
    }
    internal void ArrowsOff()
    {
        if(Slave != null)
        {
            var mr = Slave.GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 0);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        else
        {
            var mr = GetComponent<MeshRenderer>();
            var m = mr.materials[0];
            m.SetFloat("_EmitStrength", 0);
            var m1 = new Material[1];
            m1[0] = m;
            mr.materials = m1;
        }
        audiosource.clip = clips[1];
        audiosource.Play();
    }
}
