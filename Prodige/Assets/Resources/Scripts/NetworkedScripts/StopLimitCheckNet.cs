﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StopLimitCheckNet : NetworkBehaviour {
    
    // Use this for initialization
    void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        { 
            var c = other.transform.parent.GetComponentInParent<CarMovementScriptNet>();
            if (c != null)
            {
                if(!isServer)
                {
                    GameManager.Instance.TCN.GPN.CmdSetDistanceMaintained(false, SpawnMode.Driver);
                }else
                {
                    GameManager.Instance.TCN.GPN.SetDistanceMaintained(false, SpawnMode.Driver);
                }
                
                    //DistanceMaintained = false;
                gameObject.SetActive(false);
            }
        }
    }
}
