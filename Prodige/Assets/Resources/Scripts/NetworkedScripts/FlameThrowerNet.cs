﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FlameThrowerNet : NetworkBehaviour
{

    public FlameThrower flamethrower, extinguisher;
    public InputManagementNet Input;
    // Use this for initialization
    void Start()
    {
    }

    public override void OnStartLocalPlayer()
    {
        if (!isServer)
        {
            var c = flamethrower.flameEmit.collision;
            c.sendCollisionMessages = false;
            c = extinguisher.flameEmit.collision;
            c.sendCollisionMessages = false;
        }
    }
}