﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[NetworkSettings(channel = 0, sendInterval = 0.5f)]
public class CanBeOnFireNet : MultiNetworkBehaviour
{
    public delegate void FireDelegate(CanBeOnFireNet c);
    public CanBeOnFireSlave Slave;
    public List<Transform> FireEffects;
    ParticleSystem part;
    public Light[] lights;
    public float lightsIntensity = 0;
    public float duration = 20.0f;
    public float IgnitionTemperature = 100.0f;
    [Range(0.0f, 2.0f)]
    public float Conductivity = 1.0f;
    public bool BurnFromTheBeginning = false;
    public Transform LinkedObject;
    public List<Transform> SubPieces;
    public float BurnFactor = 1.0f;
    AudioSource burningsound;
    public AnimationCurve TextureInterpolationCurve = new AnimationCurve(new Keyframe(0, 0, 0, 1), new Keyframe(1, 1, 1, 0));
    [HideInInspector]
    public List<FireEffectController> parts;
    float NextOp;
    [SyncVar]
    public float Temperature;
    [SyncVar(hook = "OnWetChanged")]
    public bool wet;
    [SyncVar(hook = "OnChangedHealth")]
    [Range(-1.0f, 100.0f)]
    public float Health;
    [SyncVar(hook = "OnBurningChanged")]
    public bool burning;
    [SyncVar(hook = "OnBurnedChanged")]
    public bool burned;
    [HideInInspector]
    public GameObject SmartDestroy;
    public FireDelegate OnFireStarted, OnFireStopped;

    void OnWetChanged(bool state)
    {
        if (state)
        {
            StopFire();
            Invoke("Dry", 10);
        }
        wet = state;

    }
    void OnBurningChanged(bool state)
    {
        if (state)
        {
            StartFire();

        }

        else
        {
            StopFire();

        }
        burning = state;
    }

    private void StartFire()
    {
        if (burningsound != null && !burningsound.isPlaying)
            burningsound.Play();

        LightIntensity(lightsIntensity);
        if (part != null)
        {
            part.Play();
        }
        else if (FireEffects.Count > 0)
        {
            parts = new List<FireEffectController>();
            foreach (Transform t in FireEffects)
            {
                var p = t.GetComponent<FireEffectController>();
                if (p != null)
                {
                    p.EnableLoop();
                    p.Play();
                    parts.Add(p);
                }
            }
        }

        if (OnFireStarted != null)
            OnFireStarted.Invoke(this);
    }
    private void OnEnable()
    {
        if (Slave != null)
            Slave.enabled = true;
    }
    private void OnDisable()
    {
        if (Slave != null)
            Slave.enabled = false;
    }

    private void Awake()
    {
        NextOp = 0;
        if (Slave != null)
        {
            Slave.MasterNet = this;
            burningsound = Slave.GetComponent<AudioSource>();
        }
        else
            burningsound = GetComponent<AudioSource>();

        if (FireEffects.Count == 0)
        {
            if (Slave != null)
                part = Slave.GetComponent<ParticleSystem>();
            else
                part = GetComponent<ParticleSystem>();
        }
    }

    void OnChangedHealth(float health)
    {
        Health = health;
        UpdateFire();
    }

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        SmartDestroy = GameObject.FindGameObjectWithTag("SmartDestroy");
        GameManager.Instance.TCN.fires.Add(this);
        if (noTexture)
        {
            var tmp = Slave.GetComponent<MeshRenderer>();
            if (tmp != null)
            {
                initialColor = tmp.materials[4].GetColor("_Color");
            }
        }

        for (int l = 0; l < lights.Length; l++)
        {
            lights[l].intensity = 0;
        }

        if (isServer)
        {
            if (isClient)
            {
                Health = 100;
                wet = false;
                burning = false;
                burned = false;
            }
            else
            {
                OnChangedHealth(100);
                OnWetChanged(false);
                OnBurningChanged(false);
                OnBurnedChanged(false);
            }

            if (BurnFromTheBeginning)
            {
                Temperature = IgnitionTemperature + 2;
                Fire();
            }
            else if (Temperature == 0 || Temperature < GameManager.Instance.TCN.GetAmbientTemp())
                Temperature = GameManager.Instance.TCN.GetAmbientTemp();
        }
        else
        {
            foreach (Transform f in FireEffects)
            {
                foreach (Transform fe in f.GetComponent<FireEffectController>().FireEffects)
                {
                    if (!f.GetComponent<FireEffectController>().initialized)
                        f.GetComponent<FireEffectController>().Initialize();
                    var p = fe.GetComponent<ParticleSystem>();
                    var col = p.collision;
                    col.sendCollisionMessages = false;
                }
            }

            OnChangedHealth(Health);
            OnWetChanged(wet);
            OnBurningChanged(burning);
            OnBurnedChanged(burned);
        }
    }

    internal void Reset()
    {
        if (isServer)
        {
            if (BurnFromTheBeginning)
            {
                Temperature = IgnitionTemperature + 2;
                Fire();
            }
            else
                Temperature = GameManager.Instance.TCN.GetAmbientTemp();

            if (isClient)
            {
                wet = false;
                Health = 100.0f;
                burning = false;
                burned = false;
            }
            else
            {
                for (int l = 0; l < lights.Length; l++)
                {
                    lights[l].intensity = 0;
                }
                OnWetChanged(false);
                OnChangedHealth(100.0f);
                OnBurningChanged(false);
                OnBurnedChanged(false);
                if (!gameObject.activeSelf)
                    gameObject.SetActive(true);
            }
            RpcReset();
        }
    }

    private void RpcReset()
    {
        for (int l = 0; l < lights.Length; l++)
        {
            lights[l].intensity = 0;
        }
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
    }

    public void SetTemperature(float v)
    {
        if (isServer)
            Temperature = v;
    }

    float GetHealth()
    {
        return Health;
    }
    bool IsBurned()
    {
        return burned;
    }

    private void Fire()
    {
        if (isServer && Temperature > IgnitionTemperature && !burning && !wet)
        {
            if (isClient)
            {
                burning = true;

            }
            else
            {
                OnBurningChanged(true);
            }

        }
    }

    void OnBurnedChanged(bool value)
    {
        if (value)
        {
            if (isServer)
            {
                if (isClient)
                {
                    burning = false;
                    Health = 0;
                }
                else
                {
                    OnBurningChanged(false);
                    OnChangedHealth(0);
                }
                NextOp = 0;

                if (LinkedObject != null)
                    LinkedObject.gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                else
                {
                    if (Slave != null)
                    {
                        var t = Slave.GetComponent<MeshRenderer>();
                        if (t != null)
                            t.materials[0].SetFloat("_Element1Level", 0);
                    }


                    else
                    {
                        var t = Slave.GetComponent<MeshRenderer>();
                        if (t != null)
                            GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                    }

                }


                if (SubPieces != null)
                    foreach (Transform t in SubPieces)
                    {
                        t.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                    }

                try
                {
                    if (gameObject.isStatic || Slave != null)
                    {

                        if (tag != "Undestroyable" || (Slave != null && Slave.tag != "Undestroyable"))
                        {
                            if (LinkedObject != null && LinkedObject.tag != "Undestroyable")
                                LinkedObject.gameObject.SetActive(false);

                            if (Slave != null)
                            {
                                if(Slave.tag != "Undestroyable")
                                {
                                    Slave.gameObject.SetActive(false);
                                }
                               
                            }                               
                            else
                                gameObject.SetActive(false);

                            if (SubPieces != null)
                                foreach (Transform t in SubPieces)
                                {
                                    if (t.tag != "Undestroyable" && LinkedObject != null)
                                        LinkedObject.gameObject.SetActive(false);
                                }
                        }

                    }
                    else if (tag != "Undestroyable")
                    {
                        if (LinkedObject != null && LinkedObject.tag != "Undestroyable")
                            SmartDestroy.SendMessage("Add", LinkedObject.gameObject);

                        SmartDestroy.SendMessage("Add", gameObject);
                        if (SubPieces != null)
                            foreach (Transform t in SubPieces)
                            {
                                if (tag != "Undestroyable")
                                    SmartDestroy.SendMessage("Add", t.gameObject);
                            }
                    }
                }
                catch (NullReferenceException e)
                {
                    Debug.Log(e.Message + " in object " + transform.name);
                }
            }
            else
            {
                if (LinkedObject != null)
                    LinkedObject.gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                else
                {
                    if (Slave != null)
                    {
                        var t = Slave.GetComponent<MeshRenderer>();
                        if (t != null)
                            t.materials[0].SetFloat("_Element1Level", 0);
                    }

                    else
                    {
                        var t = GetComponent<MeshRenderer>();
                        if (t != null)
                            t.materials[0].SetFloat("_Element1Level", 0);
                    }

                }


                if (SubPieces != null)
                    foreach (Transform t in SubPieces)
                    {
                        t.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                    }

                try
                {
                    if (gameObject.isStatic || Slave != null)
                    {

                        if (tag != "Undestroyable" || (Slave != null && Slave.tag != "Undestroyable"))
                        {
                            if (LinkedObject != null && LinkedObject.tag != "Undestroyable")
                                LinkedObject.gameObject.SetActive(false);

                            if (Slave != null)
                            {
                                if (Slave.tag != "Undestroyable")
                                {
                                    Slave.gameObject.SetActive(false);
                                }

                            }
                            else
                                gameObject.SetActive(false);

                            if (SubPieces != null)
                                foreach (Transform t in SubPieces)
                                {
                                    if (t.tag != "Undestroyable" && LinkedObject != null)
                                        LinkedObject.gameObject.SetActive(false);
                                }
                        }

                    }
                    else if (tag != "Undestroyable")
                    {
                        if (LinkedObject != null && LinkedObject.tag != "Undestroyable")
                            SmartDestroy.SendMessage("Add", LinkedObject.gameObject);

                        SmartDestroy.SendMessage("Add", gameObject);
                        if (SubPieces != null)
                            foreach (Transform t in SubPieces)
                            {
                                if (tag != "Undestroyable")
                                    SmartDestroy.SendMessage("Add", t.gameObject);
                            }
                    }
                }
                catch (NullReferenceException e)
                {
                    Debug.Log(e.Message + " in object " + transform.name);
                }
            }
        }
        burned = value;
    }
    // Update is called once per frame
    // il fuoco si aggiorna 1 volta al secondo? si scalda 
    void Update()
    {
        if (isServer)
        {
            var now = Time.time;
            if (NextOp <= now)
            {
                float ambient = GameManager.Instance.TCN.GetAmbientTemp();

                if (enabled)
                {
                    if (Temperature > ambient)
                        Temperature -= 1 * Conductivity;

                    if (burning)
                    {
                        if(Temperature < maxTemp)
                        {
                            Temperature += 1.2f * Conductivity;
                        }
                       
                        var h = Health - BurnFactor;

                        if (isClient)
                            Health = h;
                        else
                            OnChangedHealth(h);

                        UpdateFire();

                        if (Temperature < IgnitionTemperature)
                            Extinguish();
                    }
                    else if (Temperature >= IgnitionTemperature && !wet && !burned)
                        Fire();

                    if (Health <= 0 && !burned)
                        Burn();
                }
                NextOp = now + 1;
            }
        }
    }

    void Burn()
    {
        if (isServer)
        {
            if (isClient)
                burned = true;
            else
                OnBurnedChanged(true);
        }
    }
    private void GetWet()
    {
        if (isServer)
        {
            if (isClient)
                wet = true;
            else
                OnWetChanged(true);
        }
    }
    public float maxTemp = 1000.0f;
    internal void WarmUp(float targettemp)
    {
        if (isServer && enabled)
        {
            if (targettemp > Temperature && Temperature < maxTemp)
                Temperature += Conductivity;
        }
    }
    internal void Cooldown(float cooldowncoeff)
    {
        if (isServer && enabled)
        {

            float ambient = GameManager.Instance.TCN.GetAmbientTemp();

            if (ambient < Temperature)
            {
                Temperature -= Conductivity * cooldowncoeff;
            }
        }
    }
    void Extinguish()
    {
        if (isClient)
            burning = false;
        else
            OnBurningChanged(false);

        NextOp = 0;
    }
    internal void ForceBurn()
    {
        if (isServer && !burned && !burning)
        {
            Temperature = IgnitionTemperature*2;
            Fire();
        }
    }
    void Dry()
    {
        if (isServer)
        {
            if (isClient)
                wet = false;
            else
                OnWetChanged(false);
        }
    }
    bool IsOnFire()
    {
        return burning;
    }
    void LightIntensity(float to)
    {
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].intensity = to;
        }
    }

    void StopFire()
    {
        if (burningsound != null && burningsound.isPlaying)
            burningsound.Stop();
        List<FireEffectController> parts = new List<FireEffectController>();
        if (part != null)
        {
            part.Stop();
        }
        else
        {
            foreach (Transform t in FireEffects)
            {
                var p = t.GetComponent<FireEffectController>();
                if (p != null)
                {
                    p.Stop();
                    parts.Add(p);
                }
            }
        }
        LightIntensity(0);

        if (OnFireStopped != null)
            OnFireStopped.Invoke(this);
    }

    public bool noTexture = false;
    private Color initialColor;
    internal void UpdateFire()
    {
        var lerp = TextureInterpolationCurve.Evaluate(Health / 100);
        if (LinkedObject != null)
            LinkedObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", lerp);
        else
        {
            if (Slave != null)
            {
                var tmp = Slave.GetComponent<MeshRenderer>();
                if (tmp != null)
                {
                    if (!noTexture)
                    {
                        tmp.materials[0].SetFloat("_Element1Level", lerp);
                    }
                    else if (initialColor != null)
                    {
                        lerp = Mathf.Max(lerp, 0.2f); 
                        //log(100,x^4+1)*1/log(100,2) 
                        //lerp = (float) Math.Max(Math.Log(100, lerp * lerp * lerp * lerp + 1) * 1 / (Math.Log(100, 2)), 0.2);
                        tmp.materials[4].SetColor("_Color", Color.Lerp(Color.black, initialColor, lerp));
                    }

                }

            }


            else
                GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", lerp);
        }

        if (SubPieces != null && SubPieces.Count > 0)
            foreach (Transform t in SubPieces)
            {
                var tmp = t.GetComponent<MeshRenderer>();
                if (tmp != null)
                    tmp.materials[0].SetFloat("_Element1Level", lerp);
            }
    }
}
