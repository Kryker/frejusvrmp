﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnNPC : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NPC" && other.transform.parent!= null && other.transform.parent.GetComponent<NPCController>() != null)
        {
        Destroy(other.transform.parent.gameObject);
        }
    }
}
