﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalIDProvider : MonoBehaviour
{
    [HideInInspector]
    public Dictionary<uint, MultiNetworkBehaviour> MultiNetworkBehaviours;
    // Use this for initialization

    void Awake()
    {
        MultiNetworkBehaviours = new Dictionary<uint, MultiNetworkBehaviour>();
        uint nextid = 0;
        var ItemList = GetComponents<MultiNetworkBehaviour>();
        foreach (MultiNetworkBehaviour c in ItemList)
        {
            c.IDProvider = this;
            c.locID = nextid;
            MultiNetworkBehaviours[nextid] = c;
            nextid++;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
