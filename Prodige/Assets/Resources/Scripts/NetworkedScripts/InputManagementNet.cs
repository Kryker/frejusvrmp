﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class InputManagementNet : NetworkBehaviour {

    PlayerSpawnerNet Spawner;
    public enum Hand { Left, Right };
    public GameObject LeftController;
    public GameObject RightController;
    private SteamVR_TrackedController LeftVRController, RightVRController;

    public event Action<object, ClickedEventArgs> OnMouseClicked, OnMouseUnclicked, OnRightMouseClicked, OnRightMouseUnclicked, OnLeftMouseClicked, OnLeftMouseUnclicked;
    public event Action<object, ClickedEventArgs> OnRightDropPressed, OnLeftDropPressed, OnRightDropUnpressed, OnLeftDropUnpressed;
    public event Action<object, ClickedEventArgs> OnLeftTriggerClicked, OnLeftTriggerUnclicked, OnLeftPadUnpressed, OnRightPadUnpressed, OnLeftPadUntouched, OnRightPadUntouched, OnLeftGripped, OnLeftUngripped,
                               OnRightTriggerClicked, OnRightTriggerUnclicked, OnRightGripped, OnRightUngripped;
    public event Action<object, ClickedEventArgs> OnLeftPadPressed, OnRightPadPressed, OnLeftPadTouched, OnRightPadTouched;

    public event Action<object, ClickedEventArgs> OnMouseClickedOffline, OnMouseUnclickedOffline, OnRightMouseClickedOffline, OnRightMouseUnclickedOffline, OnLeftMouseClickedOffline, OnLeftMouseUnclickedOffline;
    public event Action<object, ClickedEventArgs> OnRightDropPressedOffline, OnLeftDropPressedOffline, OnRightDropUnpressedOffline, OnLeftDropUnpressedOffline;
    public event Action<object, ClickedEventArgs> OnLeftTriggerClickedOffline, OnLeftTriggerUnclickedOffline, OnLeftPadUnpressedOffline, OnRightPadUnpressedOffline, OnLeftPadUntouchedOffline, OnRightPadUntouchedOffline, OnLeftGrippedOffline, OnLeftUngrippedOffline,
                               OnRightTriggerClickedOffline, OnRightTriggerUnclickedOffline, OnRightGrippedOffline, OnRightUngrippedOffline;
    public event Action<object, ClickedEventArgs> OnLeftPadPressedOffline, OnRightPadPressedOffline, OnLeftPadTouchedOffline, OnRightPadTouchedOffline;

    [SyncVar(hook = "OnLeftMousePressedChanged")]
    private bool _leftMouseClicked;
    [SyncVar(hook = "OnRightMousePressedChanged")]
    private bool _rightMouseClicked;
    [SyncVar(hook = "OnLeftDropPressedChanged")]
    private bool _leftDropPressed;
    [SyncVar(hook = "OnRightDropPressedChanged")]
    private bool _rightDropPressed;
    [SyncVar(hook = "OnLeftTriggerClickedChanged")]
    private bool _leftTriggerClicked;
    [SyncVar(hook = "OnLeftPadPressedChanged")]
    private bool _leftPadPressed;
    [SyncVar(hook = "OnLeftGrippedChanged")]
    private bool _leftGripped;
    [SyncVar(hook = "OnLeftPadTouchedChanged")]
    private bool _leftPadTouched;
    [SyncVar(hook = "OnRightTriggerClickedChanged")]
    private bool _rightTriggerClicked;
    [SyncVar(hook = "OnRightPadPressedChanged")]
    private bool _rightPadPressed;
    [SyncVar(hook = "OnRightGrippedChanged")]
    private bool _rightGripped;
    [SyncVar(hook = "OnRightPadTouchedChanged")]
    private bool _rightPadTouched;

    public bool IsLeftMouseClicked { get { return _leftMouseClicked; } private set { _leftMouseClicked = value; } }
    public bool IsRightMouseClicked { get { return _rightMouseClicked; } private set { _rightMouseClicked = value; } }
    public bool IsLeftDropPressed { get { return _leftDropPressed; } private set { _leftDropPressed = value; } }
    public bool IsRightDropPressed { get { return _rightDropPressed; } private set { _rightDropPressed = value; } }
    public bool IsLeftTriggerClicked { get { return _leftTriggerClicked; } private set { _leftTriggerClicked = value; } }
    public bool IsLeftPadPressed { get { return _leftPadPressed; } private set { _leftPadPressed = value; } }
    public bool IsLeftGripped { get { return _leftGripped; } private set { _leftGripped = value; } }
    public bool IsLeftPadTouched { get { return _leftPadTouched; } private set { _leftPadTouched = value; } }
    public bool IsRightTriggerClicked { get { return _rightTriggerClicked; } private set { _rightTriggerClicked = value; } }
    public bool IsRightPadPressed { get { return _rightPadPressed; } private set { _rightPadPressed = value; } }
    public bool IsRightGripped { get { return _rightGripped; } private set { _rightGripped = value; } }
    public bool IsRightPadTouched { get { return _rightPadTouched; } private set { _rightPadTouched = value; } }

    [SyncVar]
    private ClickedEventArgs _leftTriggerClickedEventArgs;
    [SyncVar]
    private ClickedEventArgs _rightTriggerClickedEventArgs;
    [SyncVar]
    private ClickedEventArgs _leftGripClickedEventArgs;
    [SyncVar]
    private ClickedEventArgs _rightGripClickedEventArgs;
    [SyncVar]
    private ClickedEventArgs _leftPadClickedEventArgs;
    [SyncVar]
    private ClickedEventArgs _rightPadClickedEventArgs;

    public bool IsOfflineLeftMouseClicked { get { return _offlineLeftMouseClicked; } private set { _offlineLeftMouseClicked = value; } }
    public bool IsOfflineRightMouseClicked { get { return _offlineRightMouseClicked; } private set { _offlineRightMouseClicked = value; } }
    public bool IsOfflineLeftDropPressed { get { return _offlineLeftDropPressed; } private set { _offlineLeftDropPressed = value; } }
    public bool IsOfflineRightDropPressed { get { return _offlineRightDropPressed; } private set { _offlineRightDropPressed = value; } }
    public bool IsOfflineLeftTriggerClicked { get { return _offlineLeftTriggerClicked; } private set { _offlineLeftTriggerClicked = value; } }
    public bool IsOfflineLeftPadPressed { get { return _offlineLeftPadPressed; } private set { _offlineLeftPadPressed = value; } }
    public bool IsOfflineLeftGripped { get { return _offlineLeftGripped; } private set { _offlineLeftGripped = value; } }
    public bool IsOfflineLeftPadTouched { get { return _offlineLeftPadTouched; } private set { _offlineLeftPadTouched = value; } }
    public bool IsOfflineRightTriggerClicked { get { return _offlineRightTriggerClicked; } private set { _offlineRightTriggerClicked = value; } }
    public bool IsOfflineRightPadPressed { get { return _offlineRightPadPressed; } private set { _offlineRightPadPressed = value; } }
    public bool IsOfflineRightGripped { get { return _offlineRightGripped; } private set { _offlineRightGripped = value; } }
    public bool IsOfflineRightPadTouched { get { return _offlineRightPadTouched; } private set { _offlineRightPadTouched = value; } }
    
    private bool _offlineLeftMouseClicked;
    private bool _offlineRightMouseClicked;
    private bool _offlineLeftDropPressed;
    private bool _offlineRightDropPressed;
    private bool _offlineLeftTriggerClicked;
    private bool _offlineLeftPadPressed;
    private bool _offlineLeftGripped;
    private bool _offlineLeftPadTouched;
    private bool _offlineRightTriggerClicked;
    private bool _offlineRightPadPressed;
    private bool _offlineRightGripped;
    private bool _offlineRightPadTouched;


    private void Awake()
    {
        Spawner = GetComponent<PlayerSpawnerNet>();
    }

    void OnLeftMousePressedChanged(bool value)
    {
        if (value && OnLeftMouseClicked != null)
            OnLeftMouseClicked.Invoke(netId, new ClickedEventArgs());
        else if (!value && OnLeftMouseUnclicked != null)
            OnLeftMouseUnclicked.Invoke(netId, new ClickedEventArgs());
        IsLeftMouseClicked = value;
    }
    void OnRightMousePressedChanged(bool value)
    {
        if (value && OnRightMouseClicked != null)
            OnRightMouseClicked.Invoke(netId, new ClickedEventArgs());
        else if (!value && OnRightMouseUnclicked != null)
            OnRightMouseUnclicked.Invoke(netId, new ClickedEventArgs());
        IsRightMouseClicked = value;
    }

    void OnLeftDropPressedChanged(bool value)
    {
        if (value && OnLeftDropPressed != null)
            OnLeftDropPressed.Invoke(netId, new ClickedEventArgs());
        else if (!value && OnLeftDropUnpressed != null)
            OnLeftDropUnpressed.Invoke(netId, new ClickedEventArgs());
        IsLeftDropPressed = value;
    }
    void OnRightDropPressedChanged(bool value)
    {
        if (value && OnRightDropPressed != null)
            OnRightDropPressed.Invoke(netId, new ClickedEventArgs());
        else if (!value && OnRightDropUnpressed != null)
            OnRightDropUnpressed.Invoke(netId, new ClickedEventArgs());
        IsRightDropPressed = value;
    }

    void OnLeftTriggerClickedChanged(bool value)
    {
        if (value && OnLeftTriggerClicked != null)
            OnLeftTriggerClicked.Invoke(netId, _leftTriggerClickedEventArgs);
        else if (!value && OnLeftTriggerUnclicked != null)
            OnLeftTriggerUnclicked(netId, _leftTriggerClickedEventArgs);
        IsLeftTriggerClicked = value;
    }

    void OnLeftPadPressedChanged(bool value)
    {
        if (value && OnLeftPadPressed != null)
            OnLeftPadPressed.Invoke(netId, _leftPadClickedEventArgs);
        else if (!value && OnLeftPadUnpressed != null)
            OnLeftPadUnpressed(netId, _leftPadClickedEventArgs);
        IsLeftPadPressed = value;
    }

    void OnLeftPadTouchedChanged(bool value)
    {
        if (value && OnLeftPadTouched != null)
            OnLeftPadTouched.Invoke(netId, _leftPadClickedEventArgs);
        else if (!value && OnLeftPadUntouched != null)
            OnLeftPadUntouched.Invoke(netId, _leftPadClickedEventArgs);
        IsLeftPadTouched = value;
    }

    void OnLeftGrippedChanged(bool value)
    {
        if (value && OnLeftGripped != null)
            OnLeftGripped.Invoke(netId, _leftGripClickedEventArgs);
        else if (!value && OnLeftUngripped != null)
            OnLeftUngripped(netId, _leftGripClickedEventArgs);
        IsLeftGripped = value;
    }

    void OnRightTriggerClickedChanged(bool value)
    {
        if (value && OnRightTriggerClicked != null)
            OnRightTriggerClicked.Invoke(netId, _rightTriggerClickedEventArgs);
        else if (!value && OnRightTriggerUnclicked != null)
            OnRightTriggerUnclicked(netId, _rightTriggerClickedEventArgs);
        IsRightTriggerClicked = value;
    }

    void OnRightPadPressedChanged(bool value)
    {
        if (value && OnRightPadPressed != null)
            OnRightPadPressed.Invoke(netId, _rightPadClickedEventArgs);
        else if (!value && OnRightPadUnpressed != null)
            OnRightPadUnpressed(netId, _rightPadClickedEventArgs);
        IsRightPadPressed = value;
    }

    void OnRightPadTouchedChanged(bool value)
    {
        if (value && OnRightPadTouched != null)
            OnRightPadTouched.Invoke(netId, _rightPadClickedEventArgs);
        else if (!value && OnRightPadUntouched != null)
            OnRightPadUntouched.Invoke(netId, _rightPadClickedEventArgs);
        IsRightPadTouched = value;
    }

    void OnRightGrippedChanged(bool value)
    {
        if (value && OnRightGripped != null)
            OnRightGripped.Invoke(netId, _rightGripClickedEventArgs);
        else if (!value && OnRightUngripped != null)
            OnRightUngripped(netId, _rightGripClickedEventArgs);
        IsRightGripped = value;
    }

    private void Start()
    {
        if (isServer)
        {
            if(isClient)
            {
                IsLeftMouseClicked = false;
                IsRightMouseClicked = false;
                IsLeftDropPressed = false;
                IsRightDropPressed = false;
                IsLeftTriggerClicked = false;
                IsLeftPadPressed = false;
                IsLeftGripped = false;
                IsRightTriggerClicked = false;
                IsRightPadPressed = false;
                IsRightGripped = false;
            }
            else
            {
                OnLeftMousePressedChanged(false);
                OnRightMousePressedChanged(false);
                OnLeftDropPressedChanged(false);
                OnRightDropPressedChanged(false);
                OnLeftTriggerClickedChanged(false);
                OnLeftPadPressedChanged(false);
                OnLeftGrippedChanged(false);
                OnRightTriggerClickedChanged(false);
                OnRightPadPressedChanged(false);
                OnRightGrippedChanged(false);
            }
        }

        if (isLocalPlayer)
        {
            if (Spawner.Platform != ControllerType.MouseAndKeyboard)
            {
                if (LeftController != null)
                {
                    LeftVRController = LeftController.GetComponent<SteamVR_TrackedController>();
                    if (LeftVRController != null)
                    {
                        LeftVRController.TriggerClicked += LeftTriggerClicked;

                        LeftVRController.TriggerUnclicked += LeftTriggerUnClicked;

                        LeftVRController.PadClicked += LeftPadPressed;

                        LeftVRController.PadUnclicked += LeftPadUnPressed;

                        LeftVRController.Gripped += LeftGripped;

                        LeftVRController.Ungripped += LeftUngripped;

                        LeftVRController.PadTouched += LeftPadTouched;

                        LeftVRController.PadUntouched += LeftPadUntouched;
                    }
                }
                if (RightController != null)
                {
                    RightVRController = RightController.GetComponent<SteamVR_TrackedController>();
                    if (RightVRController != null)
                    {
                        RightVRController.TriggerClicked += RightTriggerClicked;

                        RightVRController.TriggerUnclicked += RightTriggerUnClicked;

                        RightVRController.PadClicked += RightPadPressed;

                        RightVRController.PadUnclicked += RightPadUnPressed;

                        RightVRController.Gripped += RightGripped;

                        RightVRController.Ungripped += RightUnGripped;

                        RightVRController.PadTouched += RightPadTouched;

                        RightVRController.PadUntouched += RightPadUntouched;
                    }
                }
            }
        }
        else if (!isServer)
        {
            OnLeftMousePressedChanged(_leftMouseClicked);
            OnRightMousePressedChanged(_rightMouseClicked);
            OnLeftDropPressedChanged(_leftDropPressed);
            OnRightDropPressedChanged(_rightDropPressed);
            OnLeftTriggerClickedChanged(_leftTriggerClicked);
            OnLeftPadPressedChanged(_leftPadPressed);
            OnLeftPadTouchedChanged(_leftPadTouched);
            OnLeftGrippedChanged(_leftGripped);
            OnRightTriggerClickedChanged(_rightTriggerClicked);
            OnRightPadPressedChanged(_rightPadPressed);
            OnRightPadTouchedChanged(_rightPadTouched);
            OnRightGrippedChanged(_rightGripped);
        }
    }

    // Update is called once per frame
    bool TriggerXboxLeftPressed = false;
    bool TriggerXboxRightPressed = false;
    void Update () {
        if (isLocalPlayer && Spawner.Platform == ControllerType.MouseAndKeyboard)
        {
            if (Input.GetButtonDown("Fire Left Mouse"))
                {
                ButtonPressed("Fire Left Mouse", new ClickedEventArgs());
                if (!IsOfflineLeftMouseClicked)
                {
                    if (OnLeftMouseClickedOffline != null)
                        OnLeftMouseClickedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineLeftMouseClicked = true;
                }
                }
            else if (Input.GetButtonUp("Fire Left Mouse"))
                {
                ButtonUnPressed("Fire Left Mouse", new ClickedEventArgs());
                if(IsOfflineLeftMouseClicked)
                {
                if (OnLeftMouseUnclickedOffline != null)
                    OnLeftMouseUnclickedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineLeftMouseClicked = false;
                }
                }
            if (Input.GetButtonDown("Fire Right Mouse") )
                {
                ButtonPressed("Fire Right Mouse", new ClickedEventArgs());
                if (!IsOfflineRightMouseClicked)
                {
                    if (OnRightMouseClickedOffline != null)
                        OnRightMouseClickedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineRightMouseClicked = true;
                }
            }
            else if (Input.GetButtonUp("Fire Right Mouse"))
            {
                ButtonUnPressed("Fire Right Mouse", new ClickedEventArgs());
                if (IsOfflineRightMouseClicked)
                {
                    if (OnRightMouseUnclickedOffline != null)
                        OnRightMouseUnclickedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineRightMouseClicked = false;
                }
            }
            if (Input.GetButtonDown("Drop Item Left") || (!TriggerXboxLeftPressed && Input.GetAxisRaw("Drop Item Left") > 0.5))
            {
                TriggerXboxLeftPressed = true;
                ButtonPressed("Drop Item Left", new ClickedEventArgs());
                if (!IsOfflineLeftDropPressed)
                {
                    if (OnLeftDropPressedOffline != null)
                        OnLeftDropPressedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineLeftDropPressed = true;
                }
            }
            else if (Input.GetButtonUp("Drop Item Left") || (TriggerXboxLeftPressed && Input.GetAxisRaw("Drop Item Left") < 0.1))
            {
                TriggerXboxLeftPressed = false;
                ButtonUnPressed("Drop Item Left", new ClickedEventArgs());
                if (IsOfflineLeftDropPressed)
                {
                    if (OnLeftDropUnpressedOffline != null)
                        OnLeftDropUnpressedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineLeftDropPressed = false;
                }
            }
            if (Input.GetButtonDown("Drop Item Right") || (!TriggerXboxRightPressed && Input.GetAxisRaw("Drop Item Right") > 0.5))
            {
                TriggerXboxRightPressed = true;
                ButtonPressed("Drop Item Right", new ClickedEventArgs());
                if (!IsOfflineRightDropPressed)
                {
                    if (OnRightDropPressedOffline != null)
                        OnRightDropPressedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineRightDropPressed = true;
                }
            }
            else if (Input.GetButtonUp("Drop Item Right") || (TriggerXboxRightPressed && Input.GetAxisRaw("Drop Item Right") < 0.1))
            {
                TriggerXboxRightPressed = false;
                ButtonUnPressed("Drop Item Right", new ClickedEventArgs());
                if (IsOfflineRightDropPressed)
                {
                    if (OnRightDropUnpressedOffline != null)
                        OnRightDropUnpressedOffline.Invoke(netId, new ClickedEventArgs());
                    IsOfflineRightDropPressed = false;
                }
            }
        }
    }

    private void LeftTriggerClicked (object sender, ClickedEventArgs e)
    {
        ButtonPressed("Left Trigger", e);
        if (OnLeftTriggerClickedOffline != null)
            OnLeftTriggerClickedOffline.Invoke((netId), e);
    }

    private void LeftTriggerUnClicked (object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Left Trigger", e);
        if (OnLeftTriggerUnclickedOffline != null)
            OnLeftTriggerUnclickedOffline.Invoke((netId), e);
    }

    private void RightTriggerClicked(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Right Trigger", e);
        if (OnRightTriggerClickedOffline != null)
            OnRightTriggerClickedOffline.Invoke((netId), e);
        IsOfflineRightTriggerClicked = true;
    }

    private void RightTriggerUnClicked(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Right Trigger", e);
        if (OnRightTriggerUnclickedOffline != null)
            OnRightTriggerUnclickedOffline.Invoke((netId), e);
        IsOfflineRightTriggerClicked = false;
    }

    private void LeftPadPressed(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Left Pad Clicked", e);
        if (OnLeftPadPressedOffline != null)
            OnLeftPadPressedOffline.Invoke((netId), e);
        IsOfflineLeftPadPressed = true;
    }

    private void LeftPadUnPressed(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Left Pad Clicked", e);
        if (OnLeftPadUnpressedOffline != null)
            OnLeftPadUnpressedOffline.Invoke((netId), e);
        IsOfflineLeftPadPressed = false;
    }

    private void RightPadPressed(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Right Pad Clicked", e);
        if (OnRightPadPressedOffline != null)
            OnRightPadPressedOffline.Invoke((netId), e);
        IsOfflineRightPadPressed = true;
    }

    private void RightPadUnPressed(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Right Pad Clicked", e);
        if (OnRightPadUnpressedOffline != null)
            OnRightPadUnpressedOffline.Invoke((netId), e);
        IsOfflineRightPadPressed = false;
    }

    private void LeftPadTouched(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Left Pad Touched", e);
        if (OnLeftPadTouchedOffline != null)
            OnLeftPadTouchedOffline.Invoke((netId), e);
        IsOfflineLeftPadTouched = true;
    }

    private void LeftPadUntouched(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Left Pad Touched", e);
        if (OnLeftPadUntouchedOffline != null)
            OnLeftPadUntouchedOffline.Invoke((netId), e);
        IsOfflineLeftPadTouched = false;
    }

    private void RightPadTouched(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Right Pad Touched", e);
        if (OnRightPadTouchedOffline != null)
            OnRightPadTouchedOffline.Invoke((netId), e);
        IsOfflineRightPadTouched = true;
    }

    private void RightPadUntouched(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Right Pad Touched", e);
        if (OnRightPadUntouchedOffline != null)
            OnRightPadUntouchedOffline.Invoke((netId), e);
        IsOfflineRightPadTouched = false;
    }

    private void LeftGripped(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Left Grip", e);
        if (OnLeftGrippedOffline != null)
            OnLeftGrippedOffline.Invoke((netId), e);
        IsOfflineLeftGripped = true;
    }

    private void LeftUngripped(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Left Grip", e);
        if (OnLeftUngrippedOffline != null)
            OnLeftUngrippedOffline.Invoke((netId), e);
        IsOfflineLeftGripped = false;
    }
    private void RightGripped(object sender, ClickedEventArgs e)
    {
        ButtonPressed("Right Grip", e);
        if (OnRightGrippedOffline != null)
            OnRightGrippedOffline.Invoke((netId), e);
        IsOfflineRightGripped = true;
    }

    private void RightUnGripped(object sender, ClickedEventArgs e)
    {
        ButtonUnPressed("Right Grip", e);
        if (OnRightUngrippedOffline != null)
            OnRightUngrippedOffline.Invoke((netId), e);
        IsOfflineRightGripped = false;
    }

    void ButtonUnPressed(string buttonName, ClickedEventArgs e)
    {
        if (isServer)
        {
            switch (buttonName)
            {
                case "Fire Left Mouse":
                    if(isClient)
                        IsLeftMouseClicked = false;
                    else
                        OnLeftMousePressedChanged(false);
                    break;
                case "Fire Right Mouse":
                    if (isClient)
                        IsRightMouseClicked = false;
                    else
                        OnRightMousePressedChanged(false);
                    break;
                case "Drop Item Left":
                    if (isClient)
                        IsLeftDropPressed = false;
                    else
                        OnLeftDropPressedChanged(false);
                    break;
                case "Drop Item Right":
                    if (isClient)
                        IsRightDropPressed = false;
                    else
                        OnRightDropPressedChanged(false);
                    break;
                case "Left Trigger":
                    _leftTriggerClickedEventArgs = e;
                    if (isClient)
                        IsLeftTriggerClicked = false;
                    else
                        OnLeftTriggerClickedChanged(false);
                    break;
                case "Left Pad Clicked":
                    _leftPadClickedEventArgs = e;
                    if (isClient)
                        IsLeftPadPressed = false;
                    else
                        OnLeftPadPressedChanged(false);
                    break;
                case "Left Pad Touched":
                    _leftPadClickedEventArgs = e;
                    if (isClient)
                        IsLeftPadTouched = false;
                    else
                        OnLeftPadTouchedChanged(false);
                    break;
                case "Left Grip":
                    _leftGripClickedEventArgs = e;
                    if (isClient)
                        IsLeftGripped = false;
                    else
                        OnLeftGrippedChanged(false);
                    break;
                case "Right Trigger":
                    _rightTriggerClickedEventArgs = e;
                    if (isClient)
                        IsRightTriggerClicked = false;
                    else
                        OnRightTriggerClickedChanged(false);
                    break;
                case "Right Pad Clicked":
                    _rightPadClickedEventArgs = e;
                    if (isClient)
                        IsRightPadPressed = false;
                    else
                        OnRightPadPressedChanged(false);
                    break;
                case "Right Pad Touched":
                    _rightPadClickedEventArgs = e;
                    if (isClient)
                        IsRightPadTouched = false;
                    else
                        OnRightPadTouchedChanged(false);
                    break;
                case "Right Grip":
                    _rightGripClickedEventArgs = e;
                    if (isClient)
                        IsRightGripped = false;
                    else
                        OnRightGrippedChanged(false);
                    break;
            }
        }
        else
            CmdButtonUnPressed(buttonName, e);
    }

    [Command]
    void CmdButtonPressed(string buttonName, ClickedEventArgs e)
    {
        switch (buttonName)
        {

            case "Fire Left Mouse":
                if (isClient)
                    IsLeftMouseClicked = true;
                else
                    OnLeftMousePressedChanged(true);
                break;
            case "Fire Right Mouse":
                if (isClient)
                    IsRightMouseClicked = true;
                else
                    OnRightMousePressedChanged(true);
                break;
            case "Drop Item Left":
                if (isClient)
                    IsLeftDropPressed = true;
                else
                    OnLeftDropPressedChanged(true);
                break;
            case "Drop Item Right":
                if (isClient)
                    IsRightDropPressed = true;
                else
                    OnRightDropPressedChanged(true);
                break;
            case "Left Trigger":
                _leftTriggerClickedEventArgs = e;
                if (isClient)
                    IsLeftTriggerClicked = true;
                else
                    OnLeftTriggerClickedChanged(true);
                break;
            case "Left Pad Clicked":
                _leftPadClickedEventArgs = e;
                if (isClient)
                    IsLeftPadPressed = true;
                else
                    OnLeftPadPressedChanged(true);
                break;
            case "Left Pad Touched":
                _leftPadClickedEventArgs = e;
                if (isClient)
                    IsLeftPadTouched = true;
                else
                    OnLeftPadTouchedChanged(true);
                break;
            case "Left Grip":
                _leftGripClickedEventArgs = e;
                if (isClient)
                    IsLeftGripped = true;
                else
                    OnLeftGrippedChanged(true);
                break;
            case "Right Trigger":
                _rightTriggerClickedEventArgs = e;
                if (isClient)
                    IsRightTriggerClicked = true;
                else
                    OnRightTriggerClickedChanged(true);
                break;
            case "Right Pad Clicked":
                _rightPadClickedEventArgs = e;
                if (isClient)
                    IsRightPadPressed = true;
                else
                    OnRightPadPressedChanged(true);
                break;
            case "Right Pad Touched":
                _rightPadClickedEventArgs = e;
                if (isClient)
                    IsRightPadTouched = true;
                else
                    OnRightPadTouchedChanged(true);
                break;
            case "Right Grip":
                _rightGripClickedEventArgs = e;
                if (isClient)
                    IsRightGripped = true;
                else
                    OnRightGrippedChanged(true);
                break;
        }
    }
    

    void ButtonPressed(string buttonName, ClickedEventArgs e)
    {
    if(isServer)
        {
            switch (buttonName)
            {

                case "Fire Left Mouse":
                    if (isClient)
                        IsLeftMouseClicked = true;
                    else
                        OnLeftMousePressedChanged(true);
                    break;
                case "Fire Right Mouse":
                    if (isClient)
                        IsRightMouseClicked = true;
                    else
                        OnRightMousePressedChanged(true);
                    break;
                case "Drop Item Left":
                    if (isClient)
                        IsLeftDropPressed = true;
                    else
                        OnLeftDropPressedChanged(true);
                    break;
                case "Drop Item Right":
                    if (isClient)
                        IsRightDropPressed = true;
                    else
                        OnRightDropPressedChanged(true);
                    break;
                case "Left Trigger":
                    _leftTriggerClickedEventArgs = e;
                    if (isClient)
                        IsLeftTriggerClicked = true;
                    else
                        OnLeftTriggerClickedChanged(true);
                    break;
                case "Left Pad Clicked":
                    _leftPadClickedEventArgs = e;
                    if (isClient)
                        IsLeftPadPressed = true;
                    else
                        OnLeftPadPressedChanged(true);
                    break;
                case "Left Pad Touched":
                    _leftPadClickedEventArgs = e;
                    if (isClient)
                        IsLeftPadTouched = true;
                    else
                        OnLeftPadTouchedChanged(true);
                    break;
                case "Left Grip":
                    _leftGripClickedEventArgs = e;
                    if (isClient)
                        IsLeftGripped = true;
                    else
                        OnLeftGrippedChanged(true);
                    break;
                case "Right Trigger":
                    _rightTriggerClickedEventArgs = e;
                    if (isClient)
                        IsRightTriggerClicked = true;
                    else
                        OnRightTriggerClickedChanged(true);
                    break;
                case "Right Pad Clicked":
                    _rightPadClickedEventArgs = e;
                    if (isClient)
                        IsRightPadPressed = true;
                    else
                        OnRightPadPressedChanged(true);
                    break;
                case "Right Pad Touched":
                    _rightPadClickedEventArgs = e;
                    if (isClient)
                        IsRightPadTouched = true;
                    else
                        OnRightPadTouchedChanged(true);
                    break;
                case "Right Grip":
                    _rightGripClickedEventArgs = e;
                    if (isClient)
                        IsRightGripped = true;
                    else
                        OnRightGrippedChanged(true);
                    break;
            }
        }
    else
        CmdButtonPressed(buttonName, e);
    }


    [Command]
    void CmdButtonUnPressed(string buttonName, ClickedEventArgs e)
    {
        switch (buttonName)
        {

            case "Fire Left Mouse":
                if (isClient)
                    IsLeftMouseClicked = false;
                else
                    OnLeftMousePressedChanged(false);
                break;
            case "Fire Right Mouse":
                if (isClient)
                    IsRightMouseClicked = false;
                else
                    OnRightMousePressedChanged(false);
                break;
            case "Drop Item Left":
                if (isClient)
                    IsLeftDropPressed = false;
                else
                    OnLeftDropPressedChanged(false);
                break;
            case "Drop Item Right":
                if (isClient)
                    IsRightDropPressed = false;
                else
                    OnRightDropPressedChanged(false);
                break;
            case "Left Trigger":
                _leftTriggerClickedEventArgs = e;
                if (isClient)
                    IsLeftTriggerClicked = false;
                else
                    OnLeftTriggerClickedChanged(false);
                break;
            case "Left Pad Clicked":
                _leftPadClickedEventArgs = e;
                if (isClient)
                    IsLeftPadPressed = false;
                else
                    OnLeftPadPressedChanged(false);
                break;
            case "Left Pad Touched":
                _leftPadClickedEventArgs = e;
                if (isClient)
                    IsLeftPadTouched = false;
                else
                    OnLeftPadTouchedChanged(false);
                break;
            case "Left Grip":
                _leftGripClickedEventArgs = e;
                if (isClient)
                    IsLeftGripped = false;
                else
                    OnLeftGrippedChanged(false);
                break;
            case "Right Trigger":
                _rightTriggerClickedEventArgs = e;
                if (isClient)
                    IsRightTriggerClicked = false;
                else
                    OnRightTriggerClickedChanged(false);
                break;
            case "Right Pad Clicked":
                _rightPadClickedEventArgs = e;
                if (isClient)
                    IsRightPadPressed = false;
                else
                    OnRightPadPressedChanged(false);
                break;
            case "Right Pad Touched":
                _rightPadClickedEventArgs = e;
                if (isClient)
                    IsRightPadTouched = false;
                else
                    OnRightPadTouchedChanged(false);
                break;
            case "Right Grip":
                _rightGripClickedEventArgs = e;
                if (isClient)
                    IsRightGripped = false;
                else
                    OnRightGrippedChanged(false);
                break;
        }
    }
    
}

