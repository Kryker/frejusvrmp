﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class GrabbableItemNet : NetworkBehaviour
{
    public ItemCodes Code;
    public bool TwoHands = false;
    public AudioClip Drop, Grab;
    public float DropVolume = 1, GrabVolume = 1;
    public bool HideController = true;
    public GrabbableItemSlave Slave;
    [SyncVar]
    [HideInInspector]
    public NetworkIdentity PlayerNetworkIdentity = null;
    public ItemControllerNet Player;
    [SyncVar]
    public ControllerHand Hand;
    [SyncVar]
    public int Index = -1;
    [HideInInspector]
    public bool initialized = false;
    [HideInInspector]
    public Transform controller;
    public GenericItemNet Item;

    public virtual void SetUp(ItemControllerNet ic, ControllerHand hand, int i)
    {
        if (isServer)
        {
            Hand = hand;
            Index = i;
            if (ic != null)
                PlayerNetworkIdentity = ic.GetComponent<NetworkIdentity>();
        }
    }
    public virtual bool CanDrop()
    {
        return true;
    }

    public virtual void LoadState(GenericItemNet i)
    {
        Item = i;
        Slave.gameObject.SetActive(true);
        if (i.Slave != null)
            Slave.MoveToAndGetBack(i.Slave.transform.position, i.Slave.transform.rotation);
        else
            Slave.MoveToAndGetBack(i.transform.position, i.transform.rotation);
    }

    public virtual void LoadState()
    {
        Slave.gameObject.SetActive(true);
    }

    public virtual void SaveState()
    {
        Item = null;
        Slave.DisableOutline(Player);
        Slave.gameObject.SetActive(false);
    }

    public virtual void Update()
    {
        Initialize();
        if (initialized && Player == null)
        {
            if (isServer)
                Destroy(gameObject);
            else
                CmdDestroy();
        }
    }

    [Command]
    public void CmdDestroy()
    {
        Destroy(gameObject);
    }

    // Use this for initialization
    public virtual void Start()
    {
    }


    public virtual void Initialize()
    {
        if (!initialized || ((Hand == ControllerHand.LeftHand && Player.ItemsLeft[Index] == null)||((Hand == ControllerHand.RightHand && Player.ItemsRight[Index] == null))))
        {
            if (PlayerNetworkIdentity == null)
                return;
            if (Player == null)
            {
                var p = PlayerNetworkIdentity.GetComponent<PlayerStatusNet>();
                if (p != null && p.ItemController != null)
                    Player = p.ItemController;
            }
            if (Hand == ControllerHand.LeftHand)
            {
                if (Player.ItemsLeft == null || Player.ItemsLeft.Length < Index)
                    return;
                controller = Player.LeftController;
                Player.ItemsLeft[Index] = transform;
                Player.ItemsLeftReady[Index] = true;
                Slave.transform.position = Player.FakeLeftHand.position + Slave.transform.position;
                Slave.transform.rotation = Quaternion.Euler(Player.FakeLeftHand.eulerAngles + Slave.transform.rotation.eulerAngles);
                Slave.transform.parent = Player.FakeLeftHand;
            }
            else if (Hand == ControllerHand.RightHand)
            {
                if (Player.ItemsRight == null || Player.ItemsRight.Length < Index)
                    return;
                controller = Player.RightController;
                Player.ItemsRight[Index] = transform;
                Player.ItemsRightReady[Index] = true;
                Slave.transform.position = Player.FakeRightHand.position + Slave.transform.position;
                Slave.transform.rotation = Quaternion.Euler(Player.FakeRightHand.eulerAngles + Slave.transform.rotation.eulerAngles);
                Slave.transform.parent = Player.FakeRightHand;
            }
            initialized = true;
        }
    }

    public abstract void ClickButton(object sender, ClickedEventArgs e);

    public abstract void UnClickButton(object sender, ClickedEventArgs e);

}
