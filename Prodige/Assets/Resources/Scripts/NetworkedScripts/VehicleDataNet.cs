﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class VehicleDataNet : NetworkBehaviour {

    public enum VehicleType { Car, Truck };
    [Range(0.0f, 100.0f)]
    public float InitialFuelTankPercentage = 50.0f;
    [HideInInspector]
    [SyncVar]
    public float FuelTankPercentage;
    public AudioSource _burningSound, _EngineSound;
    public Transform VehicleBody;
    public VehicleType Type;
    public bool CanBurn = false;
    Vector3 InitialPosition = Vector3.zero;
    Quaternion InitialRotation = Quaternion.identity;
    public bool SingleAudioSource = false;
    List<FireEffectController> fires;
    [SyncVar(hook = "OnBrakingChanged")]
    public bool braking;
    [SyncVar]
    public bool Accelerating;
    [SerializeField]
    CruscottoControllerNet Cruscotto;
    public List<ParticleSystem> Scarichi;
    bool arrowson = false;
    float tictime = 0.45f;
    float nexttic = 0;
    [SyncVar(hook = "OnFrecceOnChanged")]
    bool FrecceOn = false;
    public MeshRenderer Quadro;
    public bool FrecceOnFromStart = false;
    public int EmissionIndex = 3;
    public bool LightsOnFromStart = true;
    public bool EngineOnFromStart = false;
    [HideInInspector]
    [SyncVar(hook = "OnEngineOnChanged")]
    public bool EngineOn;
    [SyncVar(hook = "OnFireLoopStartedChanged")]
    bool fireloopstarted;
    float t = 0;
    bool isburning = false;
    public Transform Frontlights, BackLights;
    public List<MeshRenderer> FrontlightsMR;
    public List<int> FrontLightMRIndexes;
    [SerializeField]
    FrecceNet PulsanteFrecce;
    public Transform ArrowsLights;
    public List<MeshRenderer> ArrowsMR;
    public List<int> ArrowMRIndexes;
    public Radio radio;
    [HideInInspector]
    [SyncVar(hook ="OnLeftOccupantChanged")]
    public NetworkInstanceId LeftOccupant;
    [HideInInspector]
    [SyncVar(hook = "OnRightOccupantChanged")]
    public NetworkInstanceId RightOccupant;
    public VehicleSeat SeatedSpawnLeft, SeatedSpawnRight;
    [SerializeField]
    CarMovementScriptNet CarMovement;
    public Transform Volante;

    public Animator portaDestraAnimator;
    public Animator portaSinistraAnimator;
    public AudioClip suonoAperturaPorta;
    public AudioClip suonoChiusuraPorta;

    public virtual void OnLeftOccupantChanged(NetworkInstanceId value)
    {
        OnLeftOccupantChangedReal(value);
    }
    public virtual void OnRightOccupantChanged(NetworkInstanceId value)
    {
        OnRightOccupantChangedReal(value);
    }

    protected virtual void OnLeftOccupantChangedReal(NetworkInstanceId value)
    {
        if (isServer)
        {
            if (GameManager.Instance.TCN.GameStarted)
            {
                if (RightOccupant != NetworkInstanceId.Invalid)
                {
                    //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(false);
                    if (value == NetworkInstanceId.Invalid)
                        GameManager.Instance.TCN.Spawn.SetDriverButtonAvailable(true);
                    else
                        GameManager.Instance.TCN.Spawn.SetDriverButtonAvailable(false);
                }
                else
                {
                    if (value == NetworkInstanceId.Invalid)
                    {
                        //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(true);
                        // GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(false);
                    }
                    GameManager.Instance.TCN.Spawn.SetDriverButtonAvailable(false);

                }
            }
            else
            {
                //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(false);
                if (value == NetworkInstanceId.Invalid)
                    GameManager.Instance.TCN.Spawn.SetDriverButtonAvailable(true);
                else
                    GameManager.Instance.TCN.Spawn.SetDriverButtonAvailable(false);
            }
        }

        LeftOccupant = value;
    }
    protected virtual void OnRightOccupantChangedReal(NetworkInstanceId value)
    {
        if (isServer && GameManager.Instance.TCN.GameStarted)
        {
            if (GameManager.Instance.TCN.GameStarted)
            {
                if (LeftOccupant != NetworkInstanceId.Invalid)
                {
                    //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(false);
                    // if (value == NetworkInstanceId.Invalid)
                    //GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(true);
                    //else
                    // GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(false);
                }
                else
                {
                    if (value == NetworkInstanceId.Invalid)
                    {
                        //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(true);
                        GameManager.Instance.TCN.Spawn.SetDriverButtonAvailable(false);
                    }
                    // GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(false);
                }
            }
            else
            {
                //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(false);
                // if (value == NetworkInstanceId.Invalid)
                //GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(true);
                // else
                //GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(false);
            }
        }

        RightOccupant = value;
    }

    internal void Reset()
    {
        transform.position = InitialPosition;
        transform.rotation = InitialRotation;
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        FuelTankPercentage = InitialFuelTankPercentage;
        if (isClient)
        {
            LeftOccupant = NetworkInstanceId.Invalid;
            RightOccupant = NetworkInstanceId.Invalid;
            braking = false;
            fireloopstarted = false;
        }
        else
        {
            OnLeftOccupantChanged(NetworkInstanceId.Invalid);
            OnRightOccupantChanged(NetworkInstanceId.Invalid);
            OnBrakingChanged(false);
            OnFireLoopStartedChanged(false);
        }
        Accelerating = false;
        if (EngineOnFromStart)
        {
            var fire = VehicleBody.GetComponent<CanBeOnFireSlave>();
            if (fire != null && fire.GetTemperature() < 90)
                fire.SetTemperature(90);
            TurnOn();
        }
        else
            TurnOff();
        if (FrecceOnFromStart)
            StartArrows();
    }

    // Use this for initialization
    public virtual void Awake ()
    {      
        fires = new List<FireEffectController>();
    }
    public virtual void Start()
    {
        if(portaSinistraAnimator != null)
        {
            portaSinistraAnimator.GetComponent<AudioSource>().loop = false;

        }
        if (portaDestraAnimator != null)
        {
            portaDestraAnimator.GetComponent<AudioSource>().loop = false;

        }

        InitialPosition = transform.position;
        InitialRotation = transform.rotation;
        if (CanBurn)
        {
            VehicleBody.GetComponent<CanBeOnFireSlave>().MasterNet.OnFireStarted += StartFire;
            VehicleBody.GetComponent<CanBeOnFireSlave>().MasterNet.OnFireStopped += StopFire;
        }

        /*var r = GetComponent<Rigidbody>();
        if (r != null)
            r.isKinematic = false;*/
        if (isServer)
        {
            FuelTankPercentage = InitialFuelTankPercentage;
            if (isClient)
                {
                LeftOccupant = NetworkInstanceId.Invalid;
                RightOccupant = NetworkInstanceId.Invalid;
                braking = false;
                fireloopstarted = false;
                }
            else
                {
                OnLeftOccupantChanged(NetworkInstanceId.Invalid);
                OnRightOccupantChanged(NetworkInstanceId.Invalid);
                OnBrakingChanged(false);
                OnFireLoopStartedChanged(false);
                }
            Accelerating = false;
            if (EngineOnFromStart)
            {
                var fire = VehicleBody.GetComponent<CanBeOnFireSlave>();
                if (fire != null && fire.GetTemperature() < 90)
                    fire.SetTemperature(90);
                TurnOn();
            }
            else
                TurnOff();
            if (FrecceOnFromStart)
                StartArrows();
        }
        else
        {
            OnLeftOccupantChanged(LeftOccupant);
            OnRightOccupantChanged(RightOccupant);
            if(FrecceOn)
                OnFrecceOnChanged(FrecceOn);
            if(!EngineOn)
                OnEngineOnChanged(EngineOn);
            if(braking)
                OnBrakingChanged(braking);
            if(fireloopstarted)
                OnFireLoopStartedChanged(fireloopstarted);
        }
        if (!LightsOnFromStart)
            TurnLightsOff();
    }


    public float GetVelocity()
    {
        return CarMovement.GetVelocity();
    }

    public Transform GetDriver()
    {
        GameObject driver = null;
        if(isServer)
            driver = NetworkServer.FindLocalObject(LeftOccupant);
        else
            driver = ClientScene.FindLocalObject(LeftOccupant);

        if(driver != null)
            return driver.transform;

        return null;
    }

    protected void TurnLightsOn()
    {
        if (EngineOn)
        {
            if (Frontlights != null)
            {
                Frontlights.gameObject.SetActive(true);
                for (int j = 0; j < FrontlightsMR.Count; j++)
                {
                    var m = FrontlightsMR[j].materials[FrontLightMRIndexes[j]];
                    m.EnableKeyword("_EMISSION");
                    var m1 = new Material[FrontlightsMR[j].materials.Length];
                    for (int i = 0; i < FrontlightsMR[j].materials.Length; i++)
                    {
                        if (i == FrontLightMRIndexes[j])
                            m1[i] = m;
                        else
                            m1[i] = FrontlightsMR[j].materials[i];
                    }
                    FrontlightsMR[j].materials = m1;
                }
            }
        }
    }

    protected void TurnLightsOff()
    {
        if (Frontlights != null)
        {
            Frontlights.gameObject.SetActive(false);
            for (int j = 0; j < FrontlightsMR.Count; j++)
            {
                var m = FrontlightsMR[j].materials[FrontLightMRIndexes[j]];
                m.DisableKeyword("_EMISSION");
                var m1 = new Material[FrontlightsMR[j].materials.Length];
                for (int i = 0; i < FrontlightsMR[j].materials.Length; i++)
                {
                    if (i == FrontLightMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = FrontlightsMR[j].materials[i];
                }
                FrontlightsMR[j].materials = m1;
            }
        }
    }

    public void KillPeople(Collider other)
    {
        if(isServer && other != null)
        {
            var p = other.GetComponent<PlayerStatusNet>();
            if (p != null)
            {
                p.ForceDie();
            }
        }
    }


    public void StopArrows()
    {
        if (isServer)
        {
            if(isClient)
                FrecceOn = false;
            else
                OnFrecceOnChanged(false);
            ArrowsOff();
        }
    }
    private void OnFrecceOnChanged(bool state)
    {
        if (state)
            nexttic = Time.time;
        else
            ArrowsOff();
        FrecceOn = state;
    }
    
    public void StartArrows()
{
        if (isServer)
            {
            if (ArrowsLights != null || PulsanteFrecce != null)
                {
                    if(isClient)
                        FrecceOn = true;
                    else
                        OnFrecceOnChanged(true);
                    nexttic = Time.time;
                }
            }
    }
   

    void ArrowsOn()
    {

        if (PulsanteFrecce != null)
            PulsanteFrecce.ArrowsOn();
        if (ArrowsLights != null)
        {
            ArrowsLights.gameObject.SetActive(true);
            for (int j = 0; j < ArrowsMR.Count; j++)
            {
                var m = ArrowsMR[j].materials[ArrowMRIndexes[j]];
                m.EnableKeyword("_EMISSION");
                var m1 = new Material[ArrowsMR[j].materials.Length];
                for (int i = 0; i < ArrowsMR[j].materials.Length; i++)
                {
                    if (i == ArrowMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = ArrowsMR[j].materials[i];
                }
                ArrowsMR[j].materials = m1;
            }
        }
        arrowson = true;
    }
    void ArrowsOff()
    {
        if (PulsanteFrecce != null)
            PulsanteFrecce.ArrowsOff();
        if (ArrowsLights != null)
        {
            ArrowsLights.gameObject.SetActive(false);
            for (int j = 0; j < ArrowsMR.Count; j++)
            {
                var m = ArrowsMR[j].materials[ArrowMRIndexes[j]];
                m.DisableKeyword("_EMISSION");
                var m1 = new Material[ArrowsMR[j].materials.Length];
                for (int i = 0; i < ArrowsMR[j].materials.Length; i++)
                {
                    if (i == ArrowMRIndexes[j])
                        m1[i] = m;
                    else
                        m1[i] = ArrowsMR[j].materials[i];
                }
                ArrowsMR[j].materials = m1;
            }
        }
        arrowson = false;
    }

    public bool CanMove()
    {
        return CarMovement.CanMove;
    }

    // Update is called once per frame
    public virtual void Update ()
    {
        var now = Time.time;
        if (isServer && EngineOn && !isburning && t <= now)
        {
            //Debug.Log("Ho disattivato qui!");
            VehicleBody.SendMessage("WarmUp", 90, SendMessageOptions.DontRequireReceiver);
            t = now + 0.6f;
        }
        if (FrecceOn)
        {
            if (nexttic <= now)
            {
                if (!arrowson)
                {
                    ArrowsOn();
                    nexttic = now + tictime;
                }
                else
                {
                    ArrowsOff();
                    nexttic = now + tictime;
                }
            }
        }
    }

    public bool IsBurning()
        {
        return isburning;
        }

    void StartFireLoop()
    {
        if (isServer)
        {
            if(isClient)
                fireloopstarted = true;
            else
                OnFireLoopStartedChanged(true);
        }
    }

    void StopFireLoop()
    {
        if (isServer)
        {
            if(isClient)
                fireloopstarted = false;
            else
                OnFireLoopStartedChanged(false);
        }
    }


    void OnFireLoopStartedChanged(bool state)
    {
        if(state)
            VehicleBody.SendMessage("StartFireLoop", null, SendMessageOptions.DontRequireReceiver);
        else
            VehicleBody.SendMessage("StopFireLoop", null, SendMessageOptions.DontRequireReceiver);
        fireloopstarted = state;
    }

    void StartFire(CanBeOnFireNet c)
        {
            var parts = c.parts;
            if (!_burningSound.isPlaying)
                _burningSound.Play();
            foreach (FireEffectController f in parts)
                if (!fires.Contains(f))
                    fires.Add(f);
            isburning = true;
        }
    void StopFire(CanBeOnFireNet c)
    {
        var parts = c.parts;
        foreach (FireEffectController f in parts)
            if (fires.Contains(f))
                fires.Remove(f);
        if (fires.Count == 0)
        {
            if (SingleAudioSource && _burningSound.isPlaying)
                _burningSound.Stop();
            isburning = false;
        }
    }

    internal void DropClientAuthority(NetworkInstanceId netId)
    {
        var r = GetComponent<Rigidbody>();
        if (isServer)
        {
            if (GameManager.Instance.TCN.GetPlayerController() != null && netId == GameManager.Instance.TCN.GetPlayerController().netId)
            {
                GetComponent<CarController>().enabled = false;
                r.isKinematic = true;
                GetComponent<WheelRotation>().enabled = false;
            }
            else
            {
                var clientnetid = NetworkServer.FindLocalObject(netId).GetComponent<NetworkIdentity>();
                var vehiclenetid = GetComponent<NetworkIdentity>();
                RpcDropAuthority(netId);
                vehiclenetid.RemoveClientAuthority(clientnetid.connectionToClient);
            }
        }
    }
    [ClientRpc]
    private void RpcDropAuthority(NetworkInstanceId netId)
    {
        var r = GetComponent<Rigidbody>();
        if (GameManager.Instance.TCN.GetPlayerController() != null && GameManager.Instance.TCN.GetPlayerController().netId == netId)
        {
            GetComponent<CarController>().enabled = false;
            r.isKinematic = true;
            GetComponent<WheelRotation>().enabled = false;
        }
    }

    internal void GetClientAuthority(NetworkInstanceId netId)
    {
        var r = GetComponent<Rigidbody>();
        if (isServer)
        {
            if (GameManager.Instance.TCN.GetPlayerController() != null && netId == GameManager.Instance.TCN.GetPlayerController().netId)
            {
                GetComponent<CarController>().enabled = true;
                if(CanMove())
                    //TESTr.isKinematic = false;
                GetComponent<WheelRotation>().enabled = true;
            }
            else
            {
                var clientnetid = NetworkServer.FindLocalObject(netId).GetComponent<NetworkIdentity>();
                var vehiclenetid = GetComponent<NetworkIdentity>();
                RpcGetAuthority(netId);
                vehiclenetid.AssignClientAuthority(clientnetid.connectionToClient);
                GetComponent<CarController>().enabled = false;
                r.isKinematic = true;
                GetComponent<WheelRotation>().enabled = false;
            }
        }
    }
    [ClientRpc]
    private void RpcGetAuthority(NetworkInstanceId netId)
    {
        var r = GetComponent<Rigidbody>();
        if (GameManager.Instance.TCN.GetPlayerController() != null && GameManager.Instance.TCN.GetPlayerController().netId == netId)
        {
            GetComponent<CarController>().enabled = true;
            if (CanMove())
                //TESTr.isKinematic = false;
                GetComponent<WheelRotation>().enabled = true;
        }
        else
        {
            GetComponent<CarController>().enabled = false;
            r.isKinematic = true;
            GetComponent<WheelRotation>().enabled = false;
        }
    }

    internal VehicleSeat GetSeat(VehicleSeat.Seat seat)
    {
        if (seat == VehicleSeat.Seat.Left)
            return SeatedSpawnLeft;
        else
            return SeatedSpawnRight;
    }

    internal void RemoveClientAuthority(NetworkInstanceId netId)
    {
        if (isServer)
        {
            if (GameManager.Instance.TCN.GetPlayerController() != null && netId == GameManager.Instance.TCN.GetPlayerController().netId)
            {
                GetComponent<CarController>().enabled = false;
                return;
            }
            var vehiclenetid = GetComponent<NetworkIdentity>();
            if(vehiclenetid.clientAuthorityOwner != null)
                vehiclenetid.RemoveClientAuthority(vehiclenetid.clientAuthorityOwner);
            RpcRemoveAuthority(netId);
        }
    }

    [ClientRpc]
    private void RpcRemoveAuthority(NetworkInstanceId netId)
    {
        if (GameManager.Instance.TCN.GetPlayerController() != null && GameManager.Instance.TCN.GetPlayerController().netId == netId)
            { 
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<WheelRotation>().enabled = false;
            }
    }

    public void TurnOff()
    {
        if (isServer)
        {
                EngineOn = false;
                OnEngineOnChanged(false);
        }
    }

    public GameObject radioObject;
    public GameObject cruscottoObject;

    void OnEngineOnChanged(bool state)
    {
        if (cruscottoObject != null)
        {
            var m = cruscottoObject.GetComponent<MeshRenderer>().materials[2];
            if (state)
            {
                m.EnableKeyword("_EMISSION");
            }
            else
            {
                m.DisableKeyword("_EMISSION");
            }
            cruscottoObject.GetComponent<MeshRenderer>().materials[2] = m;
        }


        if(radioObject != null)
        {
            var m = radioObject.GetComponent<MeshRenderer>().materials[0];
            if (state)
            {
                m.EnableKeyword("_EMISSION");
            }
            else
            {
                m.DisableKeyword("_EMISSION");
            }
            radioObject.GetComponent<MeshRenderer>().materials[0] = m;
        }

        if (state)
        {
            if (radio != null)
                radio.TurnOn();
            foreach (ParticleSystem s in Scarichi)
            {
                s.Play();
            }
            
            if (Quadro != null)
            {
                var m = Quadro.materials;
                var newm = new Material[m.Length];
                for (int i = 0; i < m.Length; i++)
                {
                    if (i == EmissionIndex)
                    {
                        var newmat = new Material(m[i]);
                        newmat.EnableKeyword("_EMISSION");
                        newm[i] = newmat;
                    }
                    else
                        newm[i] = m[i];

                }
                Quadro.materials = newm;
            }

            

            if (LightsOnFromStart)
                TurnLightsOn();

            if (Cruscotto != null)
                Cruscotto.TurnOn();
        }
        else
        {
            if (radio != null)
                radio.TurnOff();
            foreach (ParticleSystem s in Scarichi)
                s.Stop();

            if (Quadro != null)
            {
                var m = Quadro.materials;
                var newm = new Material[m.Length];
                for (int i = 0; i < m.Length; i++)
                {
                    if (i == EmissionIndex)
                    {
                        var newmat = new Material(m[i]);
                        newmat.DisableKeyword("_EMISSION");
                        newm[i] = newmat;
                    }
                    else
                        newm[i] = m[i];

                }
                Quadro.materials = newm;
            }

            TurnLightsOff();

            if (Cruscotto != null)
                Cruscotto.TurnOff();
        }
        EngineOn = state;
    }
    
    public void Brake()
    {
        if (isServer)
        {
            if(isClient)
                braking = true;
            else
                OnBrakingChanged(true);
        }
        else
            CmdBrake();
    }

    private void CmdBrake()
    {
        if(isClient)
            braking = true;
        else
            OnBrakingChanged(true);
    }

    private void OnBrakingChanged(bool state)
    {
        if(state && BackLights != null)
            BackLights.gameObject.SetActive(true);
        else if (BackLights != null)
            BackLights.gameObject.SetActive(false);
        braking = state;
    }

    public void StopBraking()
    {
        if (isServer)
        {
            if(isClient)
                braking = false;
            else
                OnBrakingChanged(false);
        }
    }



    public bool portaDestraAperta
    {
        get { return _portaDestraAperta; }
        set { _portaDestraAperta = value; ApriChiudiPortaDestraHook(value); }
    }
    [SyncVar(hook = "ApriChiudiPortaDestraHook")]
    private bool _portaDestraAperta = false;
    public void ApriChiudiPortaDestraHook(bool s)
    {
        var AS = portaDestraAnimator.GetComponent<AudioSource>();
        if(!AS.isPlaying)
        {
            if(s)
                AS.PlayOneShot(suonoAperturaPorta);
            else
                AS.PlayOneShot(suonoChiusuraPorta);            
        }
        portaDestraAnimator.SetBool("Aperta", s);
    }
   
    public bool portaSinistraAperta
    {
        get { return _portaSinistraAperta; }
        set { _portaSinistraAperta = value; ApriChiudiPortaSinistraHook(value); }
    }
    [SyncVar(hook = "ApriChiudiPortaSinistraHook")]
    private bool _portaSinistraAperta = false;
    public void ApriChiudiPortaSinistraHook(bool s)
    {

        portaSinistraAnimator.SetBool("Aperta", s);
        var AS = portaSinistraAnimator.GetComponent<AudioSource>();
        if (!AS.isPlaying)
        {
            if (s)
                AS.PlayOneShot(suonoAperturaPorta);
            else
                AS.PlayOneShot(suonoChiusuraPorta);
        }
    }



    public void TurnOn()
    {
        if (isServer)
        {
            EngineOn = true;
            OnEngineOnChanged(true);
        }
    }

    public static Color hexToColor(string hex)
	{
		hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		//Only use alpha if the string has enough characters
		if(hex.Length == 8){
			a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r,g,b,a);
	}
}
