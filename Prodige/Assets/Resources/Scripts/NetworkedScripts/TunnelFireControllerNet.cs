﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TunnelFireControllerNet : NetworkBehaviour
{

    // Use this for initialization
    public enum FireSide { Right, Left };
    public Transform[] FirePrefabs;
    Transform CurrFire;
    public Transform LightSmokePrefab;
    public Transform HardSmokePrefab;
    public Transform AshesPrefab;
    public bool Smoke = true;
    Transform MainFire, /*XSmallFire, SmallFire, MidFire, BigFire,*/ FireWall, FireLeanDX, FireLeanSX;
    Transform LightSmoke;
    Transform HardSmoke;
    Transform Ashes;
    List<ParticleSystem> ActiveEffects;
    bool syncedonstart = false;
    public bool NoSmokeCollision = false;

    [SyncVar]
    float StartTime;
    [SyncVar]
    float NextStep;
    [SyncVar]
    float NextTrans;
    public float[] TimePerStep;
    [SyncVar]
    int CurrentStep;
    [SyncVar]
    float CurrTime;
    public bool ManualStep = false;
    [SyncVar]
    float PastStep;
    [SyncVar]
    float LastStep;
    [SyncVar]
    float smoketime;
    public float TransitionTime = 1;
    bool synclater = false;
    TunnelSectionControllerNet Section;

    //public bool smokeFromFile = false;

    // Use this for initialization
    private void Awake()
    {
        ActiveEffects = new List<ParticleSystem>();
    }
    void Start()
    {
        if(Section == null)
            Section = GameManager.Instance.TCN.FireSection;
        
        if (isServer)
        {
            CurrentStep = 0;
            CurrTime = 0;
            LastStep = 0;
            foreach (float t in TimePerStep)
            {
                LastStep += t;
            }
            StartTime = Time.time;
            NextStep = 0;
        }
        else
            SyncOnStart();
    }

    private void SyncOnStart()
    {
        if (!syncedonstart)
        {
        if(Section == null)
            Section = GameManager.Instance.TCN.FireSection;
            switch (CurrentStep)
            {
                case 0:
                    MainFire = Instantiate(FirePrefabs[0], transform);
                    MainFire.GetComponent<FireDamage>().SetParent(Section.Fire);
                    MainFire.GetComponent<FireDamage>().enabled = false;
                    CurrFire = FirePrefabs[0];
                    ClientImmediateTransition();
                    if (Smoke)
                    {
                            LightSmoke = Instantiate(LightSmokePrefab, transform);
                       
                    }
                        
                    break;
                case 1:
                    MainFire = Instantiate(FirePrefabs[0], transform);
                    if (Smoke)
                        LightSmoke = Instantiate(LightSmokePrefab, transform);
                    CurrFire = FirePrefabs[1];
                    ClientImmediateTransition();
                    break;
                case 2:
                    MainFire = Instantiate(FirePrefabs[0], transform);
                    MainFire.GetComponent<FireDamage>().SetParent(Section.Fire);
                    MainFire.GetComponent<FireDamage>().enabled = false;
                    if (Smoke)
                        LightSmoke = Instantiate(LightSmokePrefab, transform);
                    Section.LightsOn(false);
                    if (Smoke)
                    {
                        HardSmoke = Instantiate(HardSmokePrefab, transform);
                        var p = HardSmoke.GetComponent<ParticleSystem>();
                        var c = p.collision;
                        c.sendCollisionMessages = false;
                        if(GameManager.Instance.TCN.GetPlayerController() != null)
                            CmdSyncSmokeTime(GameManager.Instance.TCN.GetPlayerController().netId);
                        else
                            synclater = true;
                    }
                    CurrFire = FirePrefabs[2];
                    ClientImmediateTransition();
                    break;
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    //FireLeanDX = Instantiate(FirePrefab[5], transform);
                    MainFire = Instantiate(FirePrefabs[0], transform);
                    MainFire.GetComponent<FireDamage>().SetParent(Section.Fire);
                    MainFire.GetComponent<FireDamage>().enabled = false;
                    if (Smoke)
                        LightSmoke = Instantiate(LightSmokePrefab, transform);
                    Section.LightsOn(false);
                    if (Smoke)
                    {
                        HardSmoke = Instantiate(HardSmokePrefab, transform);
                        var p = HardSmoke.GetComponent<ParticleSystem>();
                        var c = p.collision;
                        c.sendCollisionMessages = false;
                        if (GameManager.Instance.TCN.GetPlayerController() != null)
                            CmdSyncSmokeTime(GameManager.Instance.TCN.GetPlayerController().netId);
                        else
                            synclater = true;
                    }
                    CurrFire = FirePrefabs[3];
                    ClientImmediateTransition();
                    break;
                default:
                    break;
            }
            syncedonstart = true;
        }
    }
    [Command]
    private void CmdSyncSmokeTime(NetworkInstanceId netId)
    {
        var p = HardSmoke.GetComponent<ParticleSystem>();
        smoketime = p.time;
        RpcSyncTime(netId);
    }

    [ClientRpc]
    private void RpcSyncTime(NetworkInstanceId id)
    {
        if (GameManager.Instance.TCN.GetPlayerController().netId == id)
        { 
        var p = HardSmoke.GetComponent<ParticleSystem>();
        p.time = smoketime;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (synclater)
        {
            if (GameManager.Instance.TCN.GetPlayerController() != null)
            {
                CmdSyncSmokeTime(GameManager.Instance.TCN.GetPlayerController().netId);
                synclater = false;
            }
        }
        if (isServer)
        {
            CurrTime = Time.time;
            if (CurrentStep > TimePerStep.Length)
                return;
            if (!ManualStep)
            {
                if (NextStep <= CurrTime)
                    DoNextStep();
                else if (NextTrans <= CurrTime)
                    DoTransition();
            }
        }
    }
    void SetMainFireTransition(float rate, int NextFirePrefabIndex)
    {
        Transform NextFirePrefab = FirePrefabs[NextFirePrefabIndex];
        if(isServer)
        {
            #region Fire
            var x = Mathf.Lerp(CurrFire.localPosition.x, NextFirePrefab.localPosition.x, rate);
            var y = Mathf.Lerp(CurrFire.localPosition.y, NextFirePrefab.localPosition.y, rate);
            var z = Mathf.Lerp(CurrFire.localPosition.z, NextFirePrefab.localPosition.z, rate);

            MainFire.localPosition = new Vector3(x, y, z);

            x = Mathf.LerpAngle(CurrFire.localRotation.eulerAngles.x, NextFirePrefab.localRotation.eulerAngles.x, rate);
            y = Mathf.LerpAngle(CurrFire.localRotation.eulerAngles.y, NextFirePrefab.localRotation.eulerAngles.y, rate);
            z = Mathf.LerpAngle(CurrFire.localRotation.eulerAngles.z, NextFirePrefab.localRotation.eulerAngles.z, rate);

            MainFire.localRotation = Quaternion.Euler(new Vector3(x, y, z));

            var a1 = CurrFire.GetComponents<AudioSource>();
            var a2 = NextFirePrefab.GetComponents<AudioSource>();
            var a = MainFire.GetComponents<AudioSource>();

            a[0].volume = Mathf.Lerp(a1[0].volume, a2[0].volume, rate);
            a[1].volume = Mathf.Lerp(a1[1].volume, a2[1].volume, rate);
            a[0].maxDistance = Mathf.Lerp(a1[0].maxDistance, a2[0].maxDistance, rate);
            a[1].maxDistance = Mathf.Lerp(a1[1].maxDistance, a2[1].maxDistance, rate);

            var fl1 = CurrFire.Find("Fire light").GetComponent<UltraReal.flicker>();
            var fl2 = NextFirePrefab.Find("Fire light").GetComponent<UltraReal.flicker>();
            var fl = MainFire.Find("Fire light").GetComponent<UltraReal.flicker>();

            fl.lightIntensity = Mathf.Lerp(fl1.lightIntensity, fl2.lightIntensity, rate);
            fl.flickerCurve.timeLength = Mathf.Lerp(fl1.flickerCurve.timeLength, fl2.flickerCurve.timeLength, rate);
            if (!fl.enabled)
                fl.enabled = true;
            var l1 = CurrFire.Find("Fire light").GetComponent<Light>();
            var l2 = NextFirePrefab.Find("Fire light").GetComponent<Light>();
            var l = MainFire.Find("Fire light").GetComponent<Light>();
            l.range = Mathf.Lerp(l1.range, l2.range, rate);
            #endregion

            #region Fire_Top_Cone
            var f1 = CurrFire.Find("Fire_Top_Cone");
            var f2 = NextFirePrefab.Find("Fire_Top_Cone");
            var f = MainFire.Find("Fire_Top_Cone");

            x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
            y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
            z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

            f.localPosition = new Vector3(x, y, z);

            x = Mathf.LerpAngle(f1.localRotation.eulerAngles.x, f2.localRotation.eulerAngles.x, rate);
            y = Mathf.LerpAngle(f1.localRotation.eulerAngles.y, f2.localRotation.eulerAngles.y, rate);
            z = Mathf.LerpAngle(f1.localRotation.eulerAngles.z, f2.localRotation.eulerAngles.z, rate);

            f.localRotation = Quaternion.Euler(new Vector3(x, y, z));

            var p1 = f1.GetComponent<ParticleSystem>();
            var p2 = f2.GetComponent<ParticleSystem>();
            var p = f.GetComponent<ParticleSystem>();

            var m1 = p1.main;
            var m2 = p2.main;
            var m = p.main;
            m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
            m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));
            m.startSize = Mathf.Lerp(m1.startSize.constant, m2.startSize.constant, rate);

            var e1 = p1.emission;
            var e2 = p2.emission;
            var e = p.emission;
            e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

            var s1 = p1.shape;
            var s2 = p2.shape;
            var s = p.shape;
            s.box = new Vector3(Mathf.Lerp(s1.box.x, s2.box.x, rate), Mathf.Lerp(s1.box.y, s2.box.y, rate), s1.box.z);

            var vol1 = p1.velocityOverLifetime;
            var vol2 = p2.velocityOverLifetime;
            var vol = p.velocityOverLifetime;

            var min = Mathf.Lerp(vol1.x.constantMin, vol2.x.constantMin, rate);
            var max = Mathf.Lerp(vol1.x.constantMax, vol2.x.constantMax, rate);
            vol.x = new ParticleSystem.MinMaxCurve(min, max);
            min = Mathf.Lerp(vol1.y.constantMin, vol2.y.constantMin, rate);
            max = Mathf.Lerp(vol1.y.constantMax, vol2.y.constantMax, rate);
            vol.y = new ParticleSystem.MinMaxCurve(min, max);
            min = Mathf.Lerp(vol1.z.constantMin, vol2.z.constantMin, rate);
            max = Mathf.Lerp(vol1.z.constantMax, vol2.z.constantMax, rate);
            vol.z = new ParticleSystem.MinMaxCurve(min, max);

            var c1 = p1.collision;
            var c2 = p2.collision;
            var c = p.collision;
            c.radiusScale = Mathf.Lerp(c1.radiusScale, c2.radiusScale, rate);

            var tsa1 = p1.textureSheetAnimation;
            var tsa2 = p2.textureSheetAnimation;
            var tsa = p.textureSheetAnimation;
            var fot1 = tsa1.frameOverTime;
            var fot2 = tsa2.frameOverTime;
            var fot = tsa.frameOverTime;
            if (fot.mode == ParticleSystemCurveMode.Curve)
            {
                var k0 = Mathf.Lerp(fot1.curve.keys[0].value, fot2.curve.keys[0].value, rate);
                var k1 = Mathf.Lerp(fot1.curve.keys[1].value, fot2.curve.keys[1].value, rate);
                var k = new Keyframe[2];
                k[0] = new Keyframe(0, k0);
                k[1] = new Keyframe(1, k1);

                fot = new ParticleSystem.MinMaxCurve(1, new AnimationCurve(k));
            }

            var fire1 = f1.GetComponent<Fire>();
            var fire2 = f2.GetComponent<Fire>();
            var fire = f.GetComponent<Fire>();
            fire.Temperature = Mathf.Lerp(fire1.Temperature, fire2.Temperature, rate);
            fire.damagepp = Mathf.Lerp(fire1.damagepp, fire2.damagepp, rate);
            #endregion

            #region Fire_Bottom_Cone
            f1 = CurrFire.Find("Fire_Bottom_Cone");
            f2 = NextFirePrefab.Find("Fire_Bottom_Cone");
            f = MainFire.Find("Fire_Bottom_Cone");

            x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
            y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
            z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

            f.localPosition = new Vector3(x, y, z);

            p1 = f1.GetComponent<ParticleSystem>();
            p2 = f2.GetComponent<ParticleSystem>();
            p = f.GetComponent<ParticleSystem>();

            m1 = p1.main;
            m2 = p2.main;
            m = p.main;

            m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
            m.startSize = Mathf.Lerp(m1.startSize.constant, m2.startSize.constant, rate);
            m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));

            e1 = p1.emission;
            e2 = p2.emission;
            e = p.emission;
            e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

            s1 = p1.shape;
            s2 = p2.shape;
            s = p.shape;
            s.angle = Mathf.LerpAngle(s1.angle, s2.angle, rate);
            s.radius = Mathf.Lerp(s1.radius, s2.radius, rate);

            vol1 = p1.velocityOverLifetime;
            vol2 = p2.velocityOverLifetime;
            vol = p.velocityOverLifetime;

            min = Mathf.Lerp(vol1.x.constantMin, vol2.x.constantMin, rate);
            max = Mathf.Lerp(vol1.x.constantMax, vol2.x.constantMax, rate);
            vol.x = new ParticleSystem.MinMaxCurve(min, max);
            min = Mathf.Lerp(vol1.y.constantMin, vol2.y.constantMin, rate);
            max = Mathf.Lerp(vol1.y.constantMax, vol2.y.constantMax, rate);
            vol.y = new ParticleSystem.MinMaxCurve(min, max);
            min = Mathf.Lerp(vol1.z.constantMin, vol2.z.constantMin, rate);
            max = Mathf.Lerp(vol1.z.constantMax, vol2.z.constantMax, rate);
            vol.z = new ParticleSystem.MinMaxCurve(min, max);

            c1 = p1.collision;
            c2 = p2.collision;
            c = p.collision;
            c.radiusScale = Mathf.Lerp(c1.radiusScale, c2.radiusScale, rate);

            var sol1 = p1.sizeOverLifetime;
            var sol2 = p2.sizeOverLifetime;
            var sol = p.sizeOverLifetime;
            if (sol.size.mode == ParticleSystemCurveMode.Curve)
            {
                var k0 = Mathf.Lerp(sol1.size.curve.keys[0].value, sol2.size.curve.keys[0].value, rate);
                var k1 = Mathf.Lerp(sol1.size.curve.keys[1].value, sol2.size.curve.keys[1].value, rate);
                var k = new Keyframe[2];
                k[0] = new Keyframe(0, k0);
                k[1] = new Keyframe(1, k1);

                sol.size = new ParticleSystem.MinMaxCurve(1, new AnimationCurve(k));
            }

            tsa1 = p1.textureSheetAnimation;
            tsa2 = p2.textureSheetAnimation;
            tsa = p.textureSheetAnimation;
            fot1 = tsa1.frameOverTime;
            fot2 = tsa2.frameOverTime;
            fot = tsa.frameOverTime;
            if (fot.mode == ParticleSystemCurveMode.Curve)
            {
                var k0t = Mathf.Lerp(fot1.curve.keys[0].time, fot2.curve.keys[0].time, rate);
                var k0v = Mathf.Lerp(fot1.curve.keys[0].value, fot2.curve.keys[0].value, rate);
                var k1t = Mathf.Lerp(fot1.curve.keys[1].time, fot2.curve.keys[1].time, rate);
                var k1v = Mathf.Lerp(fot1.curve.keys[1].value, fot2.curve.keys[1].value, rate);
                var k = new Keyframe[2];
                k[0] = new Keyframe(k0t, k0v);
                k[1] = new Keyframe(k1t, k1v);

                fot = new ParticleSystem.MinMaxCurve(1, new AnimationCurve(k));
            }

            fire1 = f1.GetComponent<Fire>();
            fire2 = f2.GetComponent<Fire>();
            fire = f.GetComponent<Fire>();
            fire.Temperature = Mathf.Lerp(fire1.Temperature, fire2.Temperature, rate);
            fire.damagepp = Mathf.Lerp(fire1.damagepp, fire2.damagepp, rate);
            #endregion

            #region Fire_Sparks
            f1 = CurrFire.Find("Fire_Sparks");
            f2 = NextFirePrefab.Find("Fire_Sparks");
            f = MainFire.Find("Fire_Sparks");

            x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
            y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
            z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

            f.localPosition = new Vector3(x, y, z);

            p1 = f1.GetComponent<ParticleSystem>();
            p2 = f2.GetComponent<ParticleSystem>();
            p = f.GetComponent<ParticleSystem>();

            m1 = p1.main;
            m2 = p2.main;
            m = p.main;
            m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
            m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));
            m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSize.constantMin, m2.startSize.constantMin, rate), Mathf.Lerp(m1.startSize.constantMax, m2.startSize.constantMax, rate));

            e1 = p1.emission;
            e2 = p2.emission;
            e = p.emission;
            e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

            s1 = p1.shape;
            s2 = p2.shape;
            s = p.shape;
            s.box = new Vector3(Mathf.Lerp(s1.box.x, s2.box.x, rate), Mathf.Lerp(s1.box.y, s2.box.y, rate), s1.box.z);
            #endregion

            #region Fire_Glow
            f1 = CurrFire.Find("Fire_Glow");
            f2 = NextFirePrefab.Find("Fire_Glow");
            f = MainFire.Find("Fire_Glow");

            x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
            y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
            z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

            f.localPosition = new Vector3(x, y, z);

            p1 = f1.GetComponent<ParticleSystem>();
            p2 = f2.GetComponent<ParticleSystem>();
            p = f.GetComponent<ParticleSystem>();

            m1 = p1.main;
            m2 = p2.main;
            m = p.main;
            m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
            m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));
            m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSize.constantMin, m2.startSize.constantMin, rate), Mathf.Lerp(m1.startSize.constantMax, m2.startSize.constantMax, rate));

            e1 = p1.emission;
            e2 = p2.emission;
            e = p.emission;
            e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

            s1 = p1.shape;
            s2 = p2.shape;
            s = p.shape;
            s.box = new Vector3(Mathf.Lerp(s1.box.x, s2.box.x, rate), Mathf.Lerp(s1.box.y, s2.box.y, rate), s1.box.z);

            c1 = p1.collision;
            c2 = p2.collision;
            c = p.collision;
            c.radiusScale = Mathf.Lerp(c1.radiusScale, c2.radiusScale, rate);

            fire1 = f1.GetComponent<Fire>();
            fire2 = f2.GetComponent<Fire>();
            fire = f.GetComponent<Fire>();
            fire.Temperature = Mathf.Lerp(fire1.Temperature, fire2.Temperature, rate);
            fire.damagepp = Mathf.Lerp(fire1.damagepp, fire2.damagepp, rate);
            #endregion

            #region Fire Damage
            /*
            var co1 = CurrFire.GetComponent<SphereCollider>();
            var co2 = NextFirePrefab.GetComponent<SphereCollider>();
            var co = MainFire.GetComponent<SphereCollider>();

            var fd1 = CurrFire.GetComponent<FireDamage>();
            var fd2 = NextFirePrefab.GetComponent<FireDamage>();
            var fd = MainFire.GetComponent<FireDamage>();

            co.radius = Mathf.Lerp(co1.radius, co2.radius, rate);
            fd.Damage = Mathf.Lerp(fd1.Damage, fd2.Damage, rate);
            */
            #endregion
            RpcSetMainFireTransition(rate, NextFirePrefabIndex);
        }
    }
private void ClientImmediateTransition()
    {
        var rate = ((StartTime + CurrTime) - PastStep) / (NextStep - PastStep);
        ParticleSystem p = null;
        switch (CurrentStep)
        {
            case 1:
                ClientSetMainFireTransition(rate, 1);
                if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 4, rate), Mathf.Lerp(0, 5, rate));
                }
                break;
            case 2:
                ClientSetMainFireTransition(rate, 2);
                if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 5, rate), Mathf.Lerp(0, 6, rate));
                }
                break;
            case 3:
                ClientSetMainFireTransition(rate, 3);
                if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(5, 6);
                    var m = HardSmoke.GetComponent<ParticleSystem>().main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(1, 3, rate), Mathf.Lerp(2, 3, rate));
                    var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                    e.rateOverTime = Mathf.Lerp(25, 30, rate);
                    HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(250, 350, rate);
                }
                break;
            case 4:
                ClientSetMainFireTransition(1, 3);
                /* var f = FireLeanDX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                  f.lightIntensity = Mathf.Lerp(0.5f, 1, rate);
                  if (!f.enabled)
                      f.enabled = true;*/
                /*p = FireLeanDX.FindChild("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
                var m = p.main;
                m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 1, rate), Mathf.Lerp(0, 3, rate));
                p = FireLeanDX.FindChild("Fire_H_Line_Glow_MD").GetComponent<ParticleSystem>();
                m = p.main;
                m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 2.5f, rate), Mathf.Lerp(0, 2.5f, rate));
                p = FireLeanDX.FindChild("Fire_H_Line_Sparks").GetComponent<ParticleSystem>();
                m = p.main;
                m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 0.02f, rate), Mathf.Lerp(0, 0.05f, rate));
                f = FireLeanDX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                f.lightIntensity = Mathf.Lerp(0, 0.5f, rate);
                FireLeanDX.transform.localScale = new Vector3(1, Mathf.Lerp(0, 0.5f, rate), 1);*/
                if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(5, 6);
                    var m = HardSmoke.GetComponent<ParticleSystem>().main;
                    m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(3, 4, rate));
                    var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                    e.rateOverTime = Mathf.Lerp(30, 35, rate);
                    HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(350, 450, rate);
                }
                break;
            case 5:
                ClientSetMainFireTransition(1, 3);
                /*f = FireLeanSX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                if (!f.enabled)
                    f.enabled = true;*/
                /*FireLeanSX.transform.localScale = new Vector3(1, Mathf.Lerp(0, 0.5f, rate), 1);
                FireLeanDX.transform.localScale = new Vector3(1, Mathf.Lerp(0.5f, 1, rate), 1);
                p = FireLeanSX.FindChild("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
                m = p.main;
                m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 1, rate), Mathf.Lerp(0, 3, rate));
                p = FireLeanSX.FindChild("Fire_H_Line_Glow_MD").GetComponent<ParticleSystem>();
                m = p.main;
                m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 2.5f, rate), Mathf.Lerp(0, 2.5f, rate));
                p = FireLeanSX.FindChild("Fire_H_Line_Sparks").GetComponent<ParticleSystem>();
                m = p.main;
                m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 0.02f, rate), Mathf.Lerp(0, 0.05f, rate));
                f = FireLeanSX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                f.lightIntensity = Mathf.Lerp(0, 0.5f, rate);*/
                if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(5, 6);
                    var m = HardSmoke.GetComponent<ParticleSystem>().main;
                    m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(4, 5, rate));
                    var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                    e.rateOverTime = Mathf.Lerp(35, 40, rate);
                    HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(450, 550, rate);
                }
                break;
            case 6:
                ClientSetMainFireTransition(1, 3);
                /*f = FireWall.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                if (!f.enabled)
                    f.enabled = true;*/
                //FireLeanSX.transform.localScale = new Vector3(1, Mathf.Lerp(0.5f, 1, rate), 1);
                //SetFireWallTransition(rate);
                if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(5, 6);
                    var m = HardSmoke.GetComponent<ParticleSystem>().main;
                    m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(5, 6, rate));
                    var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                    e.rateOverTime = Mathf.Lerp(40, 45, rate);
                    HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(550, 650, rate);
                }
                break;
            case 7:
                ClientSetMainFireTransition(1, 3);
                if (Smoke)
                    if (Smoke)
                {
                    p = LightSmoke.GetComponent<ParticleSystem>();
                    var ma = p.main;
                    ma.startSize = new ParticleSystem.MinMaxCurve(5, 6);
                    var m = HardSmoke.GetComponent<ParticleSystem>().main;
                    m.startSize = new ParticleSystem.MinMaxCurve(3, 6);
                    var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                    e.rateOverTime = 45;
                    HardSmoke.GetComponent<Smoke>().Temperature = 650;
                }
                break;
            default:
                break;
        }
    }

    private void ClientSetMainFireTransition(float rate, int NextFirePrefabIndex)
    {
        Transform NextFirePrefab = FirePrefabs[NextFirePrefabIndex];
        #region Fire
        var x = Mathf.Lerp(CurrFire.localPosition.x, NextFirePrefab.localPosition.x, rate);
        var y = Mathf.Lerp(CurrFire.localPosition.y, NextFirePrefab.localPosition.y, rate);
        var z = Mathf.Lerp(CurrFire.localPosition.z, NextFirePrefab.localPosition.z, rate);

        MainFire.localPosition = new Vector3(x, y, z);

        x = Mathf.LerpAngle(CurrFire.localRotation.eulerAngles.x, NextFirePrefab.localRotation.eulerAngles.x, rate);
        y = Mathf.LerpAngle(CurrFire.localRotation.eulerAngles.y, NextFirePrefab.localRotation.eulerAngles.y, rate);
        z = Mathf.LerpAngle(CurrFire.localRotation.eulerAngles.z, NextFirePrefab.localRotation.eulerAngles.z, rate);

        MainFire.localRotation = Quaternion.Euler(new Vector3(x, y, z));

        var a1 = CurrFire.GetComponents<AudioSource>();
        var a2 = NextFirePrefab.GetComponents<AudioSource>();
        var a = MainFire.GetComponents<AudioSource>();

        a[0].volume = Mathf.Lerp(a1[0].volume, a2[0].volume, rate);
        a[1].volume = Mathf.Lerp(a1[1].volume, a2[1].volume, rate);
        a[0].maxDistance = Mathf.Lerp(a1[0].maxDistance, a2[0].maxDistance, rate);
        a[1].maxDistance = Mathf.Lerp(a1[1].maxDistance, a2[1].maxDistance, rate);

        var fl1 = CurrFire.Find("Fire light").GetComponent<UltraReal.flicker>();
        var fl2 = NextFirePrefab.Find("Fire light").GetComponent<UltraReal.flicker>();
        var fl = MainFire.Find("Fire light").GetComponent<UltraReal.flicker>();

        fl.lightIntensity = Mathf.Lerp(fl1.lightIntensity, fl2.lightIntensity, rate);
        fl.flickerCurve.timeLength = Mathf.Lerp(fl1.flickerCurve.timeLength, fl2.flickerCurve.timeLength, rate);
        if (!fl.enabled)
            fl.enabled = true;
        var l1 = CurrFire.Find("Fire light").GetComponent<Light>();
        var l2 = NextFirePrefab.Find("Fire light").GetComponent<Light>();
        var l = MainFire.Find("Fire light").GetComponent<Light>();
        l.range = Mathf.Lerp(l1.range, l2.range, rate);
        #endregion

        #region Fire_Top_Cone
        var f1 = CurrFire.Find("Fire_Top_Cone");
        var f2 = NextFirePrefab.Find("Fire_Top_Cone");
        var f = MainFire.Find("Fire_Top_Cone");

        x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
        y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
        z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

        f.localPosition = new Vector3(x, y, z);

        x = Mathf.LerpAngle(f1.localRotation.eulerAngles.x, f2.localRotation.eulerAngles.x, rate);
        y = Mathf.LerpAngle(f1.localRotation.eulerAngles.y, f2.localRotation.eulerAngles.y, rate);
        z = Mathf.LerpAngle(f1.localRotation.eulerAngles.z, f2.localRotation.eulerAngles.z, rate);

        f.localRotation = Quaternion.Euler(new Vector3(x, y, z));

        var p1 = f1.GetComponent<ParticleSystem>();
        var p2 = f2.GetComponent<ParticleSystem>();
        var p = f.GetComponent<ParticleSystem>();

        var m1 = p1.main;
        var m2 = p2.main;
        var m = p.main;
        m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
        m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));
        m.startSize = Mathf.Lerp(m1.startSize.constant, m2.startSize.constant, rate);

        var e1 = p1.emission;
        var e2 = p2.emission;
        var e = p.emission;
        e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

        var s1 = p1.shape;
        var s2 = p2.shape;
        var s = p.shape;
        s.box = new Vector3(Mathf.Lerp(s1.box.x, s2.box.x, rate), Mathf.Lerp(s1.box.y, s2.box.y, rate), s1.box.z);

        var vol1 = p1.velocityOverLifetime;
        var vol2 = p2.velocityOverLifetime;
        var vol = p.velocityOverLifetime;

        var min = Mathf.Lerp(vol1.x.constantMin, vol2.x.constantMin, rate);
        var max = Mathf.Lerp(vol1.x.constantMax, vol2.x.constantMax, rate);
        vol.x = new ParticleSystem.MinMaxCurve(min, max);
        min = Mathf.Lerp(vol1.y.constantMin, vol2.y.constantMin, rate);
        max = Mathf.Lerp(vol1.y.constantMax, vol2.y.constantMax, rate);
        vol.y = new ParticleSystem.MinMaxCurve(min, max);
        min = Mathf.Lerp(vol1.z.constantMin, vol2.z.constantMin, rate);
        max = Mathf.Lerp(vol1.z.constantMax, vol2.z.constantMax, rate);
        vol.z = new ParticleSystem.MinMaxCurve(min, max);

        var c1 = p1.collision;
        var c2 = p2.collision;
        var c = p.collision;
        c.radiusScale = Mathf.Lerp(c1.radiusScale, c2.radiusScale, rate);

        var tsa1 = p1.textureSheetAnimation;
        var tsa2 = p2.textureSheetAnimation;
        var tsa = p.textureSheetAnimation;
        var fot1 = tsa1.frameOverTime;
        var fot2 = tsa2.frameOverTime;
        var fot = tsa.frameOverTime;
        if (fot.mode == ParticleSystemCurveMode.Curve)
        {
            var k0 = Mathf.Lerp(fot1.curve.keys[0].value, fot2.curve.keys[0].value, rate);
            var k1 = Mathf.Lerp(fot1.curve.keys[1].value, fot2.curve.keys[1].value, rate);
            var k = new Keyframe[2];
            k[0] = new Keyframe(0, k0);
            k[1] = new Keyframe(1, k1);

            fot = new ParticleSystem.MinMaxCurve(1, new AnimationCurve(k));
        }

        var fire1 = f1.GetComponent<Fire>();
        var fire2 = f2.GetComponent<Fire>();
        var fire = f.GetComponent<Fire>();
        fire.Temperature = Mathf.Lerp(fire1.Temperature, fire2.Temperature, rate);
        fire.damagepp = Mathf.Lerp(fire1.damagepp, fire2.damagepp, rate);
        #endregion

        #region Fire_Bottom_Cone
        f1 = CurrFire.Find("Fire_Bottom_Cone");
        f2 = NextFirePrefab.Find("Fire_Bottom_Cone");
        f = MainFire.Find("Fire_Bottom_Cone");

        x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
        y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
        z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

        f.localPosition = new Vector3(x, y, z);

        p1 = f1.GetComponent<ParticleSystem>();
        p2 = f2.GetComponent<ParticleSystem>();
        p = f.GetComponent<ParticleSystem>();

        m1 = p1.main;
        m2 = p2.main;
        m = p.main;

        m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
        m.startSize = Mathf.Lerp(m1.startSize.constant, m2.startSize.constant, rate);
        m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));

        e1 = p1.emission;
        e2 = p2.emission;
        e = p.emission;
        e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

        s1 = p1.shape;
        s2 = p2.shape;
        s = p.shape;
        s.angle = Mathf.LerpAngle(s1.angle, s2.angle, rate);
        s.radius = Mathf.Lerp(s1.radius, s2.radius, rate);

        vol1 = p1.velocityOverLifetime;
        vol2 = p2.velocityOverLifetime;
        vol = p.velocityOverLifetime;

        min = Mathf.Lerp(vol1.x.constantMin, vol2.x.constantMin, rate);
        max = Mathf.Lerp(vol1.x.constantMax, vol2.x.constantMax, rate);
        vol.x = new ParticleSystem.MinMaxCurve(min, max);
        min = Mathf.Lerp(vol1.y.constantMin, vol2.y.constantMin, rate);
        max = Mathf.Lerp(vol1.y.constantMax, vol2.y.constantMax, rate);
        vol.y = new ParticleSystem.MinMaxCurve(min, max);
        min = Mathf.Lerp(vol1.z.constantMin, vol2.z.constantMin, rate);
        max = Mathf.Lerp(vol1.z.constantMax, vol2.z.constantMax, rate);
        vol.z = new ParticleSystem.MinMaxCurve(min, max);

        c1 = p1.collision;
        c2 = p2.collision;
        c = p.collision;
        c.radiusScale = Mathf.Lerp(c1.radiusScale, c2.radiusScale, rate);

        var sol1 = p1.sizeOverLifetime;
        var sol2 = p2.sizeOverLifetime;
        var sol = p.sizeOverLifetime;
        if (sol.size.mode == ParticleSystemCurveMode.Curve)
        {
            var k0 = Mathf.Lerp(sol1.size.curve.keys[0].value, sol2.size.curve.keys[0].value, rate);
            var k1 = Mathf.Lerp(sol1.size.curve.keys[1].value, sol2.size.curve.keys[1].value, rate);
            var k = new Keyframe[2];
            k[0] = new Keyframe(0, k0);
            k[1] = new Keyframe(1, k1);

            sol.size = new ParticleSystem.MinMaxCurve(1, new AnimationCurve(k));
        }

        tsa1 = p1.textureSheetAnimation;
        tsa2 = p2.textureSheetAnimation;
        tsa = p.textureSheetAnimation;
        fot1 = tsa1.frameOverTime;
        fot2 = tsa2.frameOverTime;
        fot = tsa.frameOverTime;
        if (fot.mode == ParticleSystemCurveMode.Curve)
        {
            var k0t = Mathf.Lerp(fot1.curve.keys[0].time, fot2.curve.keys[0].time, rate);
            var k0v = Mathf.Lerp(fot1.curve.keys[0].value, fot2.curve.keys[0].value, rate);
            var k1t = Mathf.Lerp(fot1.curve.keys[1].time, fot2.curve.keys[1].time, rate);
            var k1v = Mathf.Lerp(fot1.curve.keys[1].value, fot2.curve.keys[1].value, rate);
            var k = new Keyframe[2];
            k[0] = new Keyframe(k0t, k0v);
            k[1] = new Keyframe(k1t, k1v);

            fot = new ParticleSystem.MinMaxCurve(1, new AnimationCurve(k));
        }

        fire1 = f1.GetComponent<Fire>();
        fire2 = f2.GetComponent<Fire>();
        fire = f.GetComponent<Fire>();
        fire.Temperature = Mathf.Lerp(fire1.Temperature, fire2.Temperature, rate);
        fire.damagepp = Mathf.Lerp(fire1.damagepp, fire2.damagepp, rate);
        #endregion

        #region Fire_Sparks
        f1 = CurrFire.Find("Fire_Sparks");
        f2 = NextFirePrefab.Find("Fire_Sparks");
        f = MainFire.Find("Fire_Sparks");

        x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
        y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
        z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

        f.localPosition = new Vector3(x, y, z);

        p1 = f1.GetComponent<ParticleSystem>();
        p2 = f2.GetComponent<ParticleSystem>();
        p = f.GetComponent<ParticleSystem>();

        m1 = p1.main;
        m2 = p2.main;
        m = p.main;
        m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
        m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));
        m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSize.constantMin, m2.startSize.constantMin, rate), Mathf.Lerp(m1.startSize.constantMax, m2.startSize.constantMax, rate));

        e1 = p1.emission;
        e2 = p2.emission;
        e = p.emission;
        e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

        s1 = p1.shape;
        s2 = p2.shape;
        s = p.shape;
        s.box = new Vector3(Mathf.Lerp(s1.box.x, s2.box.x, rate), Mathf.Lerp(s1.box.y, s2.box.y, rate), s1.box.z);
        #endregion

        #region Fire_Glow
        f1 = CurrFire.Find("Fire_Glow");
        f2 = NextFirePrefab.Find("Fire_Glow");
        f = MainFire.Find("Fire_Glow");

        x = Mathf.Lerp(f1.localPosition.x, f2.localPosition.x, rate);
        y = Mathf.Lerp(f1.localPosition.y, f2.localPosition.y, rate);
        z = Mathf.Lerp(f1.localPosition.z, f2.localPosition.z, rate);

        f.localPosition = new Vector3(x, y, z);

        p1 = f1.GetComponent<ParticleSystem>();
        p2 = f2.GetComponent<ParticleSystem>();
        p = f.GetComponent<ParticleSystem>();

        m1 = p1.main;
        m2 = p2.main;
        m = p.main;
        m.startLifetime = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startLifetime.constantMin, m2.startLifetime.constantMin, rate), Mathf.Lerp(m1.startLifetime.constantMax, m2.startLifetime.constantMax, rate));
        m.startSpeed = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSpeed.constantMin, m2.startSpeed.constantMin, rate), Mathf.Lerp(m1.startSpeed.constantMax, m2.startSpeed.constantMax, rate));
        m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(m1.startSize.constantMin, m2.startSize.constantMin, rate), Mathf.Lerp(m1.startSize.constantMax, m2.startSize.constantMax, rate));

        e1 = p1.emission;
        e2 = p2.emission;
        e = p.emission;
        e.rateOverTime = Mathf.Lerp(e1.rateOverTime.constant, e2.rateOverTime.constant, rate);

        s1 = p1.shape;
        s2 = p2.shape;
        s = p.shape;
        s.box = new Vector3(Mathf.Lerp(s1.box.x, s2.box.x, rate), Mathf.Lerp(s1.box.y, s2.box.y, rate), s1.box.z);

        c1 = p1.collision;
        c2 = p2.collision;
        c = p.collision;
        c.radiusScale = Mathf.Lerp(c1.radiusScale, c2.radiusScale, rate);

        fire1 = f1.GetComponent<Fire>();
        fire2 = f2.GetComponent<Fire>();
        fire = f.GetComponent<Fire>();
        fire.Temperature = Mathf.Lerp(fire1.Temperature, fire2.Temperature, rate);
        fire.damagepp = Mathf.Lerp(fire1.damagepp, fire2.damagepp, rate);
        #endregion

        #region Fire Damage
        /*
        var co1 = CurrFire.GetComponent<SphereCollider>();
        var co2 = NextFirePrefab.GetComponent<SphereCollider>();
        var co = MainFire.GetComponent<SphereCollider>();

        var fd1 = CurrFire.GetComponent<FireDamage>();
        var fd2 = NextFirePrefab.GetComponent<FireDamage>();
        var fd = MainFire.GetComponent<FireDamage>();

        co.radius = Mathf.Lerp(co1.radius, co2.radius, rate);
        fd.Damage = Mathf.Lerp(fd1.Damage, fd2.Damage, rate);
        */
        #endregion
    }

    [ClientRpc]
    void RpcSetMainFireTransition(float rate, int NextFirePrefabIndex)
    {
        if (!isServer)
        {
            if (!syncedonstart)
                SyncOnStart();
            ClientSetMainFireTransition(rate, NextFirePrefabIndex);
        }
    }
    void SetFireWallTransition(float rate)
    {
        if (isServer)
        {
            var p = FireWall.Find("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
            var e = p.emission;
            e.rateOverTime = Mathf.Lerp(0, 40, rate);
            RpcSetFireWallTransition(rate);
        }
    }
    [ClientRpc]
    void RpcSetFireWallTransition(float rate)
    {
        if (!isServer)
        {
            if (!syncedonstart)
                SyncOnStart();
            var p = FireWall.Find("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
            var e = p.emission;
            e.rateOverTime = Mathf.Lerp(0, 40, rate);
        }
    }

    void SetSideFireTransition(float rate, FireSide side)
    {
        if (side == FireSide.Left)
        {
            throw new NotImplementedException();
        }
        else if (side == FireSide.Right)
        {
            throw new NotImplementedException();
        }
    }

    private void DoTransition()
    {
        if (isServer)
        {
            NextTrans = CurrTime + TransitionTime;
            var rate = (CurrTime - PastStep) / (NextStep - PastStep);
            ParticleSystem p = null;
            switch (CurrentStep)
            {
                case 1:
                    SetMainFireTransition(rate, 1);
                    if (Smoke)
                    {
                        p = LightSmoke.GetComponent<ParticleSystem>();
                        var ma = p.main;
                        ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 4, rate), Mathf.Lerp(0, 5, rate));
                    }
                    break;
                case 2:
                    SetMainFireTransition(rate, 2);
                    if (Smoke)
                    {
                        p = LightSmoke.GetComponent<ParticleSystem>();
                        var ma = p.main;
                        ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 5, rate), Mathf.Lerp(0, 6, rate));
                    }
                    break;
                case 3:
                    SetMainFireTransition(rate, 3);
                    if (Smoke)
                    {
                        var ma = HardSmoke.GetComponent<ParticleSystem>().main;
                        ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(1, 3, rate), Mathf.Lerp(2, 3, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(25, 30, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(250, 350, rate);
                    }
                    break;
                case 4:
                    /* var f = FireLeanDX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                      f.lightIntensity = Mathf.Lerp(0.5f, 1, rate);
                      if (!f.enabled)
                          f.enabled = true;*/
                    /*BigFire.GetComponent<AudioSource>().volume = Mathf.Lerp(0.075f, 0.1f, rate);*/
                    /*p = FireLeanDX.FindChild("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
                    var m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 1, rate), Mathf.Lerp(0, 3, rate));
                    p = FireLeanDX.FindChild("Fire_H_Line_Glow_MD").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 2.5f, rate), Mathf.Lerp(0, 2.5f, rate));
                    p = FireLeanDX.FindChild("Fire_H_Line_Sparks").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 0.02f, rate), Mathf.Lerp(0, 0.05f, rate));
                    f = FireLeanDX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    f.lightIntensity = Mathf.Lerp(0, 0.5f, rate);
                    FireLeanDX.transform.localScale = new Vector3(1, Mathf.Lerp(0, 0.5f, rate), 1);*/
                    if (Smoke)
                    {
                        var m = HardSmoke.GetComponent<ParticleSystem>().main;
                        m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(3, 4, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(30, 35, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(350, 450, rate);
                    }
                    break;
                case 5:
                    /*f = FireLeanSX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    if (!f.enabled)
                        f.enabled = true;*/
                    //BigFire.GetComponent<AudioSource>().volume = Mathf.Lerp(0.1f, 0.2f, rate);
                    /*FireLeanSX.transform.localScale = new Vector3(1, Mathf.Lerp(0, 0.5f, rate), 1);
                    FireLeanDX.transform.localScale = new Vector3(1, Mathf.Lerp(0.5f, 1, rate), 1);
                    p = FireLeanSX.FindChild("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 1, rate), Mathf.Lerp(0, 3, rate));
                    p = FireLeanSX.FindChild("Fire_H_Line_Glow_MD").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 2.5f, rate), Mathf.Lerp(0, 2.5f, rate));
                    p = FireLeanSX.FindChild("Fire_H_Line_Sparks").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 0.02f, rate), Mathf.Lerp(0, 0.05f, rate));
                    f = FireLeanSX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    f.lightIntensity = Mathf.Lerp(0, 0.5f, rate);*/
                    if (Smoke)
                    {
                        var m = HardSmoke.GetComponent<ParticleSystem>().main;
                        m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(4, 5, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(35, 40, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(450, 550, rate);
                    }
                    break;
                case 6:
                    /*f = FireWall.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    if (!f.enabled)
                        f.enabled = true;*/
                    //BigFire.GetComponent<AudioSource>().volume = Mathf.Lerp(0.2f, 0.3f, rate);
                    //FireLeanSX.transform.localScale = new Vector3(1, Mathf.Lerp(0.5f, 1, rate), 1);
                    //SetFireWallTransition(rate);
                    if (Smoke)
                    {
                        var m = HardSmoke.GetComponent<ParticleSystem>().main;
                        m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(5, 6, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(40, 45, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(550, 650, rate);                      
                    }
                    break;
                default:
                    break;
            }
            RpcDoTransition(CurrentStep);
        }
    }
    [ClientRpc]
    private void RpcDoTransition(int step)
    {
        if(!isServer)
        {
            if (!syncedonstart)
                SyncOnStart();
            var rate = (CurrTime - PastStep) / (NextStep - PastStep);
            ParticleSystem p = null;
            switch (step)
            {
                case 1:
                    SetMainFireTransition(rate, 1);
                    if (Smoke)
                    {
                        p = LightSmoke.GetComponent<ParticleSystem>();
                        var ma = p.main;
                        ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 4, rate), Mathf.Lerp(0, 5, rate));
                    }
                    break;
                case 2:
                    SetMainFireTransition(rate, 2);
                    if (Smoke)
                    {
                        p = LightSmoke.GetComponent<ParticleSystem>();
                        var ma = p.main;
                        ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 5, rate), Mathf.Lerp(0, 6, rate));
                    }
                    break;
                case 3:
                    SetMainFireTransition(rate, 3);
                    if (Smoke)
                    {
                        var ma = HardSmoke.GetComponent<ParticleSystem>().main;
                        ma.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(1, 3, rate), Mathf.Lerp(2, 3, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(25, 30, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(250, 350, rate);
                        var c = HardSmoke.GetComponent<ParticleSystem>().collision;
                        c.sendCollisionMessages = false;
                    }
                    break;
                case 4:
                    /* var f = FireLeanDX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                      f.lightIntensity = Mathf.Lerp(0.5f, 1, rate);
                      if (!f.enabled)
                          f.enabled = true;*/
                    /*BigFire.GetComponent<AudioSource>().volume = Mathf.Lerp(0.075f, 0.1f, rate);*/
                    /*p = FireLeanDX.FindChild("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
                    var m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 1, rate), Mathf.Lerp(0, 3, rate));
                    p = FireLeanDX.FindChild("Fire_H_Line_Glow_MD").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 2.5f, rate), Mathf.Lerp(0, 2.5f, rate));
                    p = FireLeanDX.FindChild("Fire_H_Line_Sparks").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 0.02f, rate), Mathf.Lerp(0, 0.05f, rate));
                    f = FireLeanDX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    f.lightIntensity = Mathf.Lerp(0, 0.5f, rate);
                    FireLeanDX.transform.localScale = new Vector3(1, Mathf.Lerp(0, 0.5f, rate), 1);*/
                    if (Smoke)
                    {
                        var m = HardSmoke.GetComponent<ParticleSystem>().main;
                        m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(3, 4, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(30, 35, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(350, 450, rate);
                    }
                    break;
                case 5:
                    /*f = FireLeanSX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    if (!f.enabled)
                        f.enabled = true;*/
                    //BigFire.GetComponent<AudioSource>().volume = Mathf.Lerp(0.1f, 0.2f, rate);
                    /*FireLeanSX.transform.localScale = new Vector3(1, Mathf.Lerp(0, 0.5f, rate), 1);
                    FireLeanDX.transform.localScale = new Vector3(1, Mathf.Lerp(0.5f, 1, rate), 1);
                    p = FireLeanSX.FindChild("GroundFire_H_Line_MD").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 1, rate), Mathf.Lerp(0, 3, rate));
                    p = FireLeanSX.FindChild("Fire_H_Line_Glow_MD").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 2.5f, rate), Mathf.Lerp(0, 2.5f, rate));
                    p = FireLeanSX.FindChild("Fire_H_Line_Sparks").GetComponent<ParticleSystem>();
                    m = p.main;
                    m.startSize = new ParticleSystem.MinMaxCurve(Mathf.Lerp(0, 0.02f, rate), Mathf.Lerp(0, 0.05f, rate));
                    f = FireLeanSX.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    f.lightIntensity = Mathf.Lerp(0, 0.5f, rate);*/
                    if (Smoke)
                    {
                        var m = HardSmoke.GetComponent<ParticleSystem>().main;
                        m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(4, 5, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(35, 40, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(450, 550, rate);
                    }
                    break;
                case 6:
                    /*f = FireWall.FindChild("Fire light").GetComponent<UltraReal.flicker>();
                    if (!f.enabled)
                        f.enabled = true;*/
                    //BigFire.GetComponent<AudioSource>().volume = Mathf.Lerp(0.2f, 0.3f, rate);
                    //FireLeanSX.transform.localScale = new Vector3(1, Mathf.Lerp(0.5f, 1, rate), 1);
                    //SetFireWallTransition(rate);
                    if (Smoke)
                    {
                        var m = HardSmoke.GetComponent<ParticleSystem>().main;
                        m.startSize = new ParticleSystem.MinMaxCurve(3, Mathf.Lerp(5, 6, rate));
                        var e = HardSmoke.GetComponent<ParticleSystem>().emission;
                        e.rateOverTime = Mathf.Lerp(40, 45, rate);
                        HardSmoke.GetComponent<Smoke>().Temperature = Mathf.Lerp(550, 650, rate);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    void DoStep()
    {
        if (isServer)
        {
            PastStep = CurrTime;
            NextTrans = CurrTime + TransitionTime;
            switch (CurrentStep)
            {
                case 0:
                    MainFire = Instantiate(FirePrefabs[0], transform);
                    MainFire.GetComponent<FireDamage>().SetParent(Section.Fire);
                    CurrFire = FirePrefabs[0];
                    if (Smoke)
                        LightSmoke = Instantiate(LightSmokePrefab, transform);
                    break;
                case 1:
                    CurrFire = FirePrefabs[1];
                    break;
                case 2:
                    Section.LightsOn(false);
                    if (Smoke)
                    {
                        HardSmoke = Instantiate(HardSmokePrefab, transform);
                        if (NoSmokeCollision)
                        {
                            var c = HardSmoke.GetComponent<ParticleSystem>().collision;
                            c.sendCollisionMessages = false;
                        }
                    }
                    GameManager.Instance.TCN.SelectSmokeDirection();
                    CurrFire = FirePrefabs[2];
                    break;
                case 3:
                    //FireLeanDX = Instantiate(FirePrefab[5], transform);
                    CurrFire = FirePrefabs[3];
                    break;
                case 4:
                    //FireLeanDX.transform.localScale = new Vector3(1, 1, 1);
                    /*FireLeanSX = Instantiate(FirePrefab[6], transform);
                       if (Smoke)
                           Ashes = Instantiate(AshesPrefab, transform);*/
                    //BigFire.GetComponent<AudioSource>().volume = 0.15f;
                    break;
                case 5:
                    //BigFire.GetComponent<AudioSource>().volume = 0.3f;
                    /*FireWall = Instantiate(FirePrefab[4], transform);
                    if (TC != null && TC.PlayerBehind())
                       FireWall.position = new Vector3(FireWall.position.x, FireWall.position.y, -FireWall.position.z);*/
                    //FireLeanSX.transform.localScale = new Vector3(1, 0.5f, 1);
                    break;
                case 6:
                    //FireLeanDX.transform.localScale = new Vector3(1, 1, 1);
                    break;
                default:
                    break;
            }
            RpcDoStep(CurrentStep);
        }
    }
    
    [ClientRpc]
    void RpcDoStep(int step)
    {
        if(!isServer)
        {
            if (!syncedonstart)
                SyncOnStart();
            PastStep = CurrTime;
            NextTrans = CurrTime + TransitionTime;
            switch (step)
            {
                case 0:
                    MainFire = Instantiate(FirePrefabs[0], transform);
                    MainFire.GetComponent<FireDamage>().SetParent(Section.Fire);
                    MainFire.GetComponent<FireDamage>().enabled = false;
                    CurrFire = FirePrefabs[0];
                    if (Smoke)
                        LightSmoke = Instantiate(LightSmokePrefab, transform);
                    break;
                case 1:
                    CurrFire = FirePrefabs[1];
                    break;
                case 2:
                    Section.LightsOn(false);
                    if (Smoke)
                        { 
                        HardSmoke = Instantiate(HardSmokePrefab, transform);
                        var c = HardSmoke.GetComponent<ParticleSystem>().collision;
                        c.sendCollisionMessages = false;
                        }
                    CurrFire = FirePrefabs[2];
                    break;
                case 3:
                    //FireLeanDX = Instantiate(FirePrefab[5], transform);
                    CurrFire = FirePrefabs[3];
                    break;
                case 4:
                    //FireLeanDX.transform.localScale = new Vector3(1, 1, 1);
                    /*FireLeanSX = Instantiate(FirePrefab[6], transform);
                       if (Smoke)
                           Ashes = Instantiate(AshesPrefab, transform);*/
                    //BigFire.GetComponent<AudioSource>().volume = 0.15f;
                    break;
                case 5:
                    //BigFire.GetComponent<AudioSource>().volume = 0.3f;
                    /*FireWall = Instantiate(FirePrefab[4], transform);
                    if (TC != null && TC.PlayerBehind())
                       FireWall.position = new Vector3(FireWall.position.x, FireWall.position.y, -FireWall.position.z);*/
                    //FireLeanSX.transform.localScale = new Vector3(1, 0.5f, 1);
                    break;
                case 6:
                    //FireLeanDX.transform.localScale = new Vector3(1, 1, 1);
                    break;
                default:
                    break;
            }
        }
    }

    public void Pause()
    {
        if (isServer)
        {
            if (!isClient)
            {
                for (int i = 0; i < ActiveEffects.Count; i++)
                    ActiveEffects[i].Pause();
            }
            RpcPause();
        }
    }
    [ClientRpc]
    private void RpcPause()
    {
        for (int i = 0; i < ActiveEffects.Count; i++)
                ActiveEffects[i].Pause();
    }

    public bool DoNextStep()
    { 
    if(isServer)
        {
        if (CurrentStep >= TimePerStep.Length)
        {
            RpcDisable();
            if (!isClient)
                enabled = false;
            return false;
        }
        NextStep = CurrTime + TimePerStep[CurrentStep];
        DoStep();
        CurrentStep++;
        return true;
        }
    return false;
    }
    [ClientRpc]
    private void RpcDisable()
    {
        enabled = false;
    }

    public void Play()
    {
        if (isServer)
        {
            if(!isClient)
                { 
                for (int i = 0; i < ActiveEffects.Count; i++)
                    ActiveEffects[i].Play();
                }
            RpcPlay();
        }
    }
    [ClientRpc]
    public void RpcPlay()
    {
        for (int i = 0; i < ActiveEffects.Count; i++)
            ActiveEffects[i].Play();
    }
    public void Stop()
    {
        if (isServer)
        {
            if (!isClient)
            {
                for (int i = 0; i < ActiveEffects.Count; i++)
                    ActiveEffects[i].Stop();
            }
            RpcStop();
        }
    }
    [ClientRpc]
    public void RpcStop()
    {
        for (int i = 0; i < ActiveEffects.Count; i++)
            ActiveEffects[i].Stop();
    }
}

