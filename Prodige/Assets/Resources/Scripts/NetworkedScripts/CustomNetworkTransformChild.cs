﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//[RequireComponent(typeof(CustomNetworkTransformChild))]
public class CustomNetworkTransformChild : NetworkBehaviour {

    [SerializeField]
    [Range(0, 120)]
    private float NetworkSendRate = 9;
    [SerializeField]
    private Transform Target;
    [SerializeField]
    private float MovementThreshold = 0.001f;
    [SerializeField]
    private float snapThreshold = 5f;
    [SerializeField]
    private float InterpolateMovementFactor = 1;
    [SerializeField]
    private float InterpolateRotationFactor = 1;
    [SyncVar(hook = "OnPositionReceived")]
    private Vector3 _lastPosition;
    [SyncVar (hook = "OnRotationReceived")]
    private Quaternion _lastRotation;
    [HideInInspector]
    [SyncVar]
    public int childIndex;
    CustomNetworkTransform Parent;
    float NextSend = 0;
    float LastSend = 0;
    float _t_r = 0;
    float _t_l = 0;
    // Use this for initialization

    void Start()
    {
        Parent = GetComponent<CustomNetworkTransform>(); 
		if(isServer)
            {
            _lastPosition = transform.localPosition;
            _lastRotation = transform.localRotation;
            }
    }

    private void LateUpdate()
    {
        if(!hasAuthority)
        {
            InterpolatePosition();
            InterpolateRotation();
        }
    }
    
    void OnPositionReceived(Vector3 value)
    {
        _t_l = 0;
        _lastPosition = value;
    }

    void OnRotationReceived(Quaternion value)
    {
        _t_r = 0;
        _lastRotation = value;
    }
	
	 internal void SetIndex(int i)
    {
        childIndex = i;
    }

    private void FixedUpdate()
    {
        var now = Time.time;
        if (hasAuthority && NetworkSendRate > 0 && now > NextSend)
        {
                var posChanged = IsPositionChanged();

                if (posChanged)
                {
                    if (!isServer)
                        CmdSendPosition(Target.localPosition);
                    else
                        {
                        if(isClient)
                            _lastPosition = Target.localPosition;
                        else
                            OnPositionReceived(Target.localPosition);
                        }
                }
                var rotChanged = IsRotationChanged();

                if (rotChanged)
                {
                    if (!isServer)
                        CmdSendRotation(Target.localRotation);
                    else
                        {
                        if(isClient)
                            _lastRotation = Target.localRotation;
                        else
                            OnRotationReceived(Target.localRotation);
                        }
                }
            NextSend = now + 1/NetworkSendRate;
        }
    }

    [Command]
    private void CmdSendPosition(Vector3 pos)
    {
        if (isClient)
            _lastPosition = pos;
        else
            OnPositionReceived(pos);
    }

    [Command]
    private void CmdSendRotation(Quaternion rot)
    {
        if (isClient)
            _lastRotation = rot;
        else
            OnRotationReceived(rot);
    }
      private void InterpolatePosition()
    {
        if (InterpolateMovementFactor == 0 || Vector3.Distance(Target.localPosition, _lastPosition) > snapThreshold)
            Target.localPosition = _lastPosition;
        else
        {
            _t_l += (Time.deltaTime * InterpolateMovementFactor);
            if (_t_l > 1)
                _t_l = 1;
            Target.localPosition = Vector3.Lerp(Target.localPosition, _lastPosition, _t_l);
        }
    }

    private void InterpolateRotation()
    {
        if (InterpolateRotationFactor == 0)
            Target.localRotation = _lastRotation;
        else
        {
            _t_r += (Time.deltaTime * InterpolateRotationFactor);
            if (_t_r > 1)
                _t_r = 1;
            Target.localRotation = Quaternion.Lerp(Target.localRotation, _lastRotation, _t_r);
        }
    }

    private bool IsPositionChanged()
    {
        return Vector3.Distance(Target.localPosition, _lastPosition) > MovementThreshold;
    }

    private bool IsRotationChanged()
    {
        return (Target.localRotation != _lastRotation);
    }
}
