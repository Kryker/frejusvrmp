﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CarSpawnNet : NetworkBehaviour {

    public List<GameObject> VehiclePrefabs;
    public List<Transform> SpawnedVehicles;
    public float MinTime = 10.0f, MaxTime = 30.0f;
    float nextcar = 0;
    public bool stop = false;
	// Use this for initialization
	void Start () {
        nextcar = 0;
    }

    public bool RemoveVehicle(Transform transform)
    {
       if(SpawnedVehicles.Contains(transform))
        {
            SpawnedVehicles.Remove(transform);
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update ()
    {
        if (!stop)
        {
            var t = Time.time;
            if (t >= nextcar)
            {
                nextcar = (int)t + UnityEngine.Random.Range(MinTime, MaxTime);
                SpawnVehicle();
            }
        }
    }

    private void SpawnVehicle()
    {
        int index = UnityEngine.Random.Range(0, VehiclePrefabs.Count);
        var t = Instantiate(VehiclePrefabs[index]);
        t.transform.position = new Vector3(t.transform.position.x, t.transform.position.y, transform.position.z);
        NetworkServer.Spawn(t);
        SpawnedVehicles.Add(t.transform);
    }
    public void Stop()
    {
        stop = true;
    }

    public void StartSpawn()
    {
        stop = false;
        foreach (Transform t in SpawnedVehicles)
            t.GetComponent<NPCCarMovementScriptNet>().CanMove = true;
    }
}
