﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChiaviNet : GenericItemNet {
    
    public Vector3 KeysRotationAction;
    Quaternion KeysRotationRest;
    public List<AudioClip> clips;
    public AudioSource _carEngineSound;
    [HideInInspector]
    public VehicleDataNet Vehicle;
    Coroutine KeysOperating;
    [SyncVar(hook ="OnKeyTurnedChanged")]
    bool keyturned = false;


    public void ForceKeyTurned(bool status)
    {
        if (isServer)
        {
            if (isClient)
                keyturned = status;
            else
                OnKeyTurnedChanged(status);
        }
        else //if (keyturned)
            OnKeyTurnedChanged(keyturned);
    }

    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.Keys;
        if(Slave != null)
            KeysRotationRest = Slave.transform.localRotation;
        else
            KeysRotationRest = transform.localRotation;
    }

    public override void Start()
    {
        base.Start();
        Vehicle = GetComponent<VehicleDataNet>();
        _carEngineSound.clip = clips[1];
        _carEngineSound.loop = true;
        _carEngineSound.Play();
        if (isServer)
        {
            if(isClient)
                keyturned = false;
            else
                OnKeyTurnedChanged(false);
        }
        else //if (keyturned)
            OnKeyTurnedChanged(keyturned);
    }

    private void OnKeyTurnedChanged(bool value)
    {
        if (value)
        {
            TurnKey();
            if (!keyturned)
            {
                if (KeysOperating != null)
                    StopCoroutine(KeysOperating);
                KeysOperating = StartCoroutine(StopEngine());
            }
        }
        else
        {
            TurnKeyBack();
            if (keyturned)
            {
                if (KeysOperating != null)
                    StopCoroutine(KeysOperating);
                KeysOperating = StartCoroutine(StartEngine());
            }
        }
        keyturned = value;
    }

    IEnumerator StopEngine()
    {
        _carEngineSound.Stop();
        _carEngineSound.clip = clips[2];
        _carEngineSound.loop = false;
        _carEngineSound.Play();
        Vehicle.radio.TurnOff();
        if(isServer)
            Vehicle.TurnOff();
        yield return new WaitUntil(() => !_carEngineSound.isPlaying);
        KeysOperating = null;
    }

    IEnumerator StartEngine()
    {
        _carEngineSound.Stop();
        _carEngineSound.clip = clips[0];
        _carEngineSound.loop = false;
        _carEngineSound.Play();
        Vehicle.radio.TurnOn();
        if(isServer)
            Vehicle.TurnOn();
        yield return new WaitUntil(() => !_carEngineSound.isPlaying);       
        _carEngineSound.clip = clips[1];
        _carEngineSound.loop = true;
        _carEngineSound.Play();
        KeysOperating = null;
    }

    void TurnKey()
    {
        if(Slave == null)
            transform.localRotation = Quaternion.Euler(KeysRotationAction);
        else
            Slave.transform.localRotation = Quaternion.Euler(KeysRotationAction);
    }
    
    void TurnKeyBack()
    {
        if (Slave == null)
            transform.localRotation = KeysRotationRest;
        else
            Slave.transform.localRotation = KeysRotationRest;
    }
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            if (ItemActive)
            {
                if(isClient)
                    keyturned = false;
                else
                    OnKeyTurnedChanged(false);
                ItemActive = false;
                //GameManager.Instance.TCN.GPN.EngineOff = false;
            }
            else
            {
                if(isClient)
                    keyturned = true;
                else
                    OnKeyTurnedChanged(true);
                ItemActive = true;
            }
            if (isServer)
                GameManager.Instance.TCN.GPN.SetEngineOff(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);

            else
                GameManager.Instance.TCN.GPN.CmdSetEngineOff(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);
        }
    }
    

    public override void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            if (ItemActive)
            {
                if (isClient)
                    keyturned = false;
                else
                    OnKeyTurnedChanged(false);
                ItemActive = false;
            }
            else
            {
                if (isClient)
                    keyturned = true;
                else
                    OnKeyTurnedChanged(true);
                ItemActive = true;
            }
            if (isServer)
                GameManager.Instance.TCN.GPN.SetEngineOff(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);

            else
                GameManager.Instance.TCN.GPN.CmdSetEngineOff(ItemActive, c.GetComponent<PlayerStatusNet>().Ruolo);
        }
    }
    public override void Reset()
    {
        TurnKeyBack();
        ItemActive = false;
        //GameManager.Instance.TCN.GPN.SetEngineOff(false, c.GetComponent<PlayerStatusNet>().Ruolo);
        _carEngineSound.Stop();
        _carEngineSound.clip = clips[1];
        _carEngineSound.loop = true;
        _carEngineSound.Play();
        base.Reset();
    }

}
