﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ForceBrakeNet : NetworkBehaviour
{    
    private void Start()
    {
    if (!isServer)
        gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        {
            var c = other.transform.parent.GetComponentInParent<CarMovementScriptNet>();
            if (c != null)
            {
                c.ForceBrake();
                gameObject.SetActive(false);
            }

            c = other.GetComponent<CarMovementScriptNet>();
            {
                if (c != null)
                {
                    c.ForceBrake();
                    gameObject.SetActive(false);
                }
            }
        }
   
       
    }
}
