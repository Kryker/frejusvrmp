﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GrabbedTelefonoSOSNet : GrabbableItemNet
{
    [HideInInspector]
    public AudioSource audiosource;
    public float newMinDistance = 0.4f;
    public float newMaxDistance = 0.5f;
    public float DropDistance = 2.0f;
    public Vector3 SpeakerPos;
    [HideInInspector]
    public TelefonoSOSNet Telefono;
    
    public NetworkInstanceId TelID;

    void SetTelID(NetworkInstanceId value)
    {
        if (value != NetworkInstanceId.Invalid)
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(value);
            else
                g = ClientScene.FindLocalObject(value);

            Telefono = g.GetComponent<TelefonoSOSNet>();
            if(Telefono == null)
            {
                TelID = NetworkInstanceId.Invalid;
                return;
            }
            audiosource = Telefono.pulsante.Speaker;

            bool paused = false;
            if (audiosource.isPlaying)
            {
                paused = true;
                audiosource.Pause();
            }
            if (Slave != null)
                audiosource.transform.parent = Slave.transform;
            else
                audiosource.transform.parent = transform;
            audiosource.transform.localPosition = SpeakerPos;
            audiosource.maxDistance = newMaxDistance;
            audiosource.minDistance = newMinDistance;
            if (paused)
                audiosource.UnPause();
            else if (isServer)
                Telefono.pulsante.ForceCall();
            
            Telefono.OnBase = false;
        }
        else
        {
            if(audiosource != null)
            { 
                bool paused = false;
                if (audiosource.isPlaying)
                {
                    paused = true;
                    audiosource.Pause();
                }
                if (Telefono.pulsante.Slave == null)
                    audiosource.transform.parent = Telefono.pulsante.transform;
                else
                    audiosource.transform.parent = Telefono.pulsante.Slave.transform;
                audiosource.transform.localPosition = Telefono.pulsante.StartSpeakerPos;
                audiosource.maxDistance = Telefono.pulsante.MaxDist;
                audiosource.minDistance = Telefono.pulsante.MinDist;
                if (paused)
                    audiosource.UnPause();
                audiosource = null;
            }
            if(Telefono != null)
            {
                Telefono.ReBase();
                Telefono = null;
            }
        }

        TelID = netId;
    }

    public override void LoadState(GenericItemNet i)
    {
        SetTelID(i.netId);
        base.LoadState(i);
    }

    public override void Start()
    {
        base.Start();
    }

    public override void SaveState()
    {
        SetTelID(NetworkInstanceId.Invalid);

        base.SaveState();
    }

    private void OnDisable()
    {

    }
    private void Awake()
    {
        Slave.GetComponent<AudioSource>();
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    { 
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override bool CanDrop()
    {
        if (Telefono.OnBase || Vector3.Distance(Telefono.CableStart.transform.position, Telefono.CableEnd.transform.position) > DropDistance)
            return true;
        else
            return false;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
        if (isServer && initialized && Telefono != null)
            if (!Telefono.OnBase)
            {
                if (Vector3.Distance(Telefono.CableStart.transform.position, Telefono.CableEnd.transform.position) > DropDistance)
                {
                    var p = Telefono.Grabber.GetComponent<PlayerStatusNet>();
                    p.ItemController.DropItem(Telefono.transform, true);                    
                }
            }
    }
}
