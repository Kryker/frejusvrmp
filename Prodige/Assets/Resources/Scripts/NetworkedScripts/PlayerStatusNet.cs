﻿using RootMotion.FinalIK;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerStatusNet : NetworkBehaviour
{

    public delegate void VehicleInteractDelegate(object sender);
    [SyncVar(hook = "OnChangedHealth")]
    [Range(0.0f, 100.0f)]
    public float health;
    public AudioSource Mouth;
    [SyncVar(hook = "OnIntoxicationChanged")]
    bool Intoxication;
    float IntoxicationEnd = float.MinValue;
    float NextOperation = float.MinValue;
    float IntoxicationTime = 4.0f;
    float IntoxicationAmount = 0.0f;
    float fogamount = 0;
    float t = 0;
    public Collider Head;
    public AudioClip Cough, Scream, Heartbeat_1, Heartbeat_2, Heartbeat_3, Heartbeat_4;
    public VehicleInteractDelegate OnVehicleLeft, OnVehicleEntered;
   
    [SyncVar]
    bool dead;

    internal void StartGame()
    {
        if (isServer)
            GameManager.Instance.TCN.StartGame(this);
        else
            CmdStartGame();
    }
    [Command]
    private void CmdStartGame()
    {
        GameManager.Instance.TCN.StartGame(this);
    }

    public AudioSource Heart;
    float LastDamage = float.MinValue;
    [SyncVar]
    bool damaging = false;
    private NetworkStartPosition[] spawnPoints;
    public RectTransform healthBar;
    public bool destroyOnDeath = false;
    [SyncVar]
    public uint PlayerID;
    [SyncVar]
    VehicleSeat.Seat CurrentSeat;
    [SyncVar]
    public SpawnMode Ruolo;

    [SyncVar(hook = "OnVehicleIDChanged")]
    NetworkInstanceId VehicleID = NetworkInstanceId.Invalid;
    VehicleDataNet Vehicle;
    public PlayerSpawnerNet Spawner;
    VibrationController leftv, rightv;
    public Vector3 BrakeVibrParameters = new Vector3(0.1f, 0.1f, 0.1f);
    public NetworkTransform charc;
    public ItemControllerNet ItemController;
    public StageControllerNet playerStat;
    Coroutine faderoutine;
    bool listening = false;
    public Vector3 SeatedOffset;
    public bool HideLocalPlayerRig = true;

    public class PlayerStatusData
    {
        public float health = 100.0f;
        public bool Intoxication;
        public float IntoxicationTime = 4.0f;
        public float IntoxicationAmount = 0.0f;
        public float IntoxicationEnd = float.MinValue;
        public float NextOperation = float.MinValue;
        public float fogamount = 0;
        public float LastDamage = float.MinValue;
        public PlayerStatusData(PlayerStatusNet playerStatus)
        {
            if (playerStatus != null)
            {
                health = playerStatus.health;
                Intoxication = playerStatus.Intoxication;
                IntoxicationAmount = playerStatus.IntoxicationAmount;
                IntoxicationEnd = playerStatus.IntoxicationEnd;
                IntoxicationTime = playerStatus.IntoxicationTime;
                NextOperation = playerStatus.NextOperation;
                fogamount = playerStatus.fogamount;
                LastDamage = playerStatus.LastDamage;
            }
        }
    }
    internal void Reset()
    {
        if (isServer)
        {
            if (isClient)
            {
                health = 100.0f;
                Intoxication = false;
                RpcReset();
            }
            else
            {
                OnChangedHealth(100.0f);
                OnIntoxicationChanged(false);
                OnVehicleIDChanged(NetworkInstanceId.Invalid);
                NextOperation = float.MinValue;
                IntoxicationEnd = float.MinValue;
                t = 0;
                dead = false;
                LastDamage = float.MinValue;
            }

            Spawner.Reset();
        }
    }

    [ClientRpc]
    private void RpcReset()
    {
        NextOperation = float.MinValue;
        IntoxicationEnd = float.MinValue;
        t = 0;
        dead = false;
        LastDamage = float.MinValue;
    }

    public void EnableListening()
    {
        if (isLocalPlayer && !listening)
        {
            AudioListener.volume = 1;
            listening = true;
        }
    }
    public void DisableListening()
    {
        if (isLocalPlayer && listening)
        {
            AudioListener.volume = 0;
            listening = false;
        }
    }

  

    public void OnVehicleIDChanged(NetworkInstanceId vid)
    {
        if (vid == NetworkInstanceId.Invalid)
        {
            if (VehicleID != NetworkInstanceId.Invalid && Spawner.IsSpawned())
                VehicleGetOut();
        }
        else
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(vid);
            else
                g = ClientScene.FindLocalObject(vid);

            Vehicle = g.GetComponent<VehicleDataNet>();

            if (VehicleID == NetworkInstanceId.Invalid && Spawner.IsSpawned())
                VehicleGetIn();
        }

        VehicleID = vid;

    }
    public void InitialVehicleID()
    {
        if (VehicleID != NetworkInstanceId.Invalid)
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(VehicleID);
            else
                g = ClientScene.FindLocalObject(VehicleID);

            Vehicle = g.GetComponent<VehicleDataNet>();

            VehicleGetIn();
        }

    }

    private void OnDestroy()
    {
        if (GameManager.Instance.TCN != null)
            GameManager.Instance.TCN.PlayerDisconnected(this);
    }

    private void VehicleGetIn()
    {
        StartCoroutine(GoInsideCar(.35f));
    }

    private void VehicleGetOut()
    {
        StartCoroutine(GetOutOfCar(.35f));
    }

    public bool CarLeft()
    {
        return playerStat.CarLeft;
    }

    // Use this for initialization
    void Start()
    {
        if (isServer)
        {
            if (isClient)
            {
                health = 100.0f;
                Intoxication = false;
            }
            else
            {
                OnChangedHealth(100.0f);
                OnIntoxicationChanged(false);
                OnVehicleIDChanged(NetworkInstanceId.Invalid);
            }
        }
        else
            OnIntoxicationChanged(Intoxication);

        if (isLocalPlayer)
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
    }

    public void GetOutFromVehicle()
    {
        if (isServer)
        {
            if (isClient)
                VehicleID = NetworkInstanceId.Invalid;
            else
                OnVehicleIDChanged(NetworkInstanceId.Invalid);
        }
    }

    public void GetInVehicle(VehicleDataNet vehicle, VehicleSeat.Seat seat)
    {
        if (isServer)
        {
            if (VehicleID == NetworkInstanceId.Invalid)
            {
                CurrentSeat = seat;
                if (isClient)
                    VehicleID = vehicle.netId;
                else
                    OnVehicleIDChanged(vehicle.netId);
            }
        }
    }


    internal void VehicleStarted()
    {
        if (isServer)
        {
            if (!isClient)
            {
                if (Vehicle != null && CurrentSeat == VehicleSeat.Seat.Left)
                {
                    var c = Spawner.DrivingArmature.GetComponent<RigIK>();
                    if (c != null)
                    {
                        c.ForcedLeftHandTarget = Vehicle.Volante.Find("Left");
                        c.ForcedRightHandTarget = Vehicle.Volante.Find("Right");
                    }
                }
            }
            RpcVehicleStarted();
        }
    }
    [ClientRpc]
    private void RpcVehicleStarted()
    {
        if (Vehicle != null && CurrentSeat == VehicleSeat.Seat.Left)
        {
            var c = Spawner.DrivingArmature.GetComponent<RigIK>();
            if (c != null)
            {
                c.ForcedLeftHandTarget = Vehicle.Volante.Find("Left");
                c.ForcedRightHandTarget = Vehicle.Volante.Find("Right");
            }
        }
    }

    bool debugOnce = false;
    public bool HasToDie()
    {
        if (health > 0)
            return false;
        return true;
    }

    public Collider GetHead()
    {
        return Head;
    }

    public void OnIntoxicationChanged(bool value)
    {
        if(Ruolo != SpawnMode.Componente)
        {
            if (value)
            {
                if (isLocalPlayer)
                    t = 0;
                if (isServer)
                    IntoxicationEnd = Time.time + IntoxicationTime;
                if (!Mouth.isPlaying)
                {
                    Mouth.clip = Cough;
                    Mouth.loop = true;
                    Mouth.Play();
                }
            }
            else
            {
                if (isLocalPlayer)
                {
                    t = 0;
                    fogamount = RenderSettings.fogDensity;
                }
                if (isServer)
                    IntoxicationAmount = 0.0f;
                Mouth.loop = false;
            }
            Intoxication = value;
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        
       
        var now = Time.time;
        if (isServer)
        {
            if (health == 0)
                Die();
            else
            {
                if (Intoxication && now >= IntoxicationEnd)
                {
                    if (isClient)
                        Intoxication = false;
                    else
                        OnIntoxicationChanged(false);
                }
                else if (Intoxication && now > NextOperation)
                {
                    if (Ruolo != SpawnMode.Componente)
                    {
                        StartCoroutine(ApplyDamageWithoutPain(IntoxicationAmount * 5));
                        if (!Mouth.isPlaying)
                        {
                            Mouth.clip = Cough;
                            Mouth.loop = true;
                            Mouth.Play();
                        }
                    }
                    NextOperation = now + 1;
                }
                else if (health < 100 && now > LastDamage + 10 && now > NextOperation)
                {
                    Recover(now);
                }
            }
        }
        else
        {
            if (Intoxication && now > NextOperation)
            {
                if (!Mouth.isPlaying && Ruolo != SpawnMode.Componente)
                {
                    Mouth.clip = Cough;
                    Mouth.loop = true;
                    Mouth.Play();
                }
                NextOperation = now + 1;
            }
        }
        /*
        if (isLocalPlayer && Ruolo != SpawnMode.Componente)
        {
            if (Intoxication && RenderSettings.fogDensity < 1 || !Intoxication && RenderSettings.fogDensity > 0)
                UpdateFog();
        }
        */

        if (Spawner != null)
        {
            if (Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                if(Spawner.CameraEye != null)
                    Spawner.CameraEye.farClipPlane -= 5;
                if (Spawner.FPSCamera != null)
                    Spawner.FPSCamera.farClipPlane -= 5;

            }
            if (Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                if (Spawner.CameraEye != null)
                    Spawner.CameraEye.farClipPlane += 5;
                if (Spawner.FPSCamera != null)
                    Spawner.FPSCamera.farClipPlane += 5;
            }
        }
    }

    internal void Heal()
    {
        if (isServer)
        {
            if (isClient)
            {
                health = 100.0f;
                Intoxication = false;
            }
            else
            {
                OnChangedHealth(100.0f);
                OnIntoxicationChanged(false);
            }
        }
        else
            CmdHeal();
    }
    [Command]
    private void CmdHeal()
    {
        if (isClient)
        {
            health = 100.0f;
            Intoxication = false;
        }
        else
        {
            OnChangedHealth(100.0f);
            OnIntoxicationChanged(false);
        }
        Mouth.loop = false;
    }

    internal bool IsInVehicle()
    {
        return (VehicleID != NetworkInstanceId.Invalid);
    }

    void OnChangedHealth(float h)
    {
        health = h;
        if (isLocalPlayer)
        {
            if (!Heart.isPlaying)
                Heart.Play();

            if (health == 100)
            {
                if (Heart.clip != Heartbeat_1)
                    Heart.clip = Heartbeat_1;
                Heart.volume = 0;
            }
            else if (health < 100 && health >= 50.0f)
            {
                if (Heart.clip != Heartbeat_1)
                    Heart.clip = Heartbeat_1;
                Heart.volume = 1 - ((health - 50) / 50);
            }
            else if (health < 50.0f && health >= 25.0f && (Heart.clip != Heartbeat_2 || Heart.volume != 1))
            {
                Heart.clip = Heartbeat_2;
                Heart.volume = 1;
            }
            else if (health < 25.0f && health >= 10.0f && (Heart.clip != Heartbeat_3 || Heart.volume != 1))
            {
                Heart.clip = Heartbeat_3;
                Heart.volume = 1;
            }
            else if (health < 10.0f && health > 0.0f && (Heart.clip != Heartbeat_4 || Heart.volume != 1))
            {
                Heart.clip = Heartbeat_4;
                Heart.volume = 1;
            }
        }
        if (healthBar != null)
            healthBar.sizeDelta = new Vector2(health, healthBar.sizeDelta.y);
    }

    private void UpdateFog()
    {
        if (isLocalPlayer)
        {
            if (Intoxication)
            {
                t += Time.deltaTime / 2;
                RenderSettings.fogDensity = Mathf.Lerp(fogamount, 1, t);
            }
            else
            {
                t += Time.deltaTime / 5;
                RenderSettings.fogDensity = Mathf.Lerp(fogamount, 0, t);
            }
        }
    }

    public void VehicleStopped()
    {
        if (isServer)
        {
            if (!isClient)
            {
                if (Vehicle != null && CurrentSeat == VehicleSeat.Seat.Left && Spawner.DrivingArmature != null)
                {
                    var c = Spawner.DrivingArmature.GetComponent<RigIK>();
                    if (c != null)
                    {
                        c.ForcedLeftHandTarget = null;
                        c.ForcedRightHandTarget = null;
                    }
                }
            }
            RpcVehicleStopped();
        }
    }
    [ClientRpc]
    private void RpcVehicleStopped()
    {
        if (Vehicle != null && CurrentSeat == VehicleSeat.Seat.Left && Spawner.DrivingArmature != null)
        {
            var c = Spawner.DrivingArmature.GetComponent<RigIK>();
            if (c != null)
            {
                c.ForcedLeftHandTarget = null;
                c.ForcedRightHandTarget = null;
            }
        }
    }

    IEnumerator GoInsideCar(float seconds)
    {
        if (isLocalPlayer)
        {
            PlayerCameraFadeOut(seconds);
            yield return new WaitForSeconds(seconds);

            PlayerControllerGetIn();

            if (isServer)
            {
                playerStat.GoInsideCar();
                if (CurrentSeat == VehicleSeat.Seat.Left)
                {
                    if (isClient)
                        Vehicle.LeftOccupant = netId;
                    else
                        Vehicle.OnLeftOccupantChanged(netId);
                }
                else
                {
                    if (isClient)
                        Vehicle.RightOccupant = netId;
                    else
                        Vehicle.OnRightOccupantChanged(netId);
                }


                if (Vehicle.GetSeat(CurrentSeat).Driver)
                {
                    Vehicle.GetClientAuthority(netId);
                    if (Vehicle.CanMove() || !GameManager.Instance.TCN.GameStarted)
                    {
                        ItemController.SetBrochureAvailable(false);
                        if (ItemController.Type == ItemControllerType.VR)
                            ItemController.StartPulsePublic(ControllerButton.All);
                        else
                            ItemController.SetCrosshairPublic(ItemControllerNet.CrosshairState.Rainbow);
                    }
                }
                else
                    ItemController.SetBrochureAvailable(true);

            }

            if (Vehicle.EngineOn&& Vehicle.radio != null)
            {
                Vehicle.radio.TurnOn();
            }
                

            Vehicle.GetComponent<CarController>().enabled = true;
            EnableListening();

            /*if (Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                Spawner.Armature.GetComponent<Animator>().SetBool("Driving", true);
                Spawner.Armature.GetComponent<Animator>().SetLayerWeight(1, 0);
            }*/
            if(Spawner.DrivingArmature != null)
            {
                var c = Spawner.DrivingArmature.GetComponent<RigIK>();
                if (c != null)
                {
                    if (CurrentSeat == VehicleSeat.Seat.Left && Vehicle.Volante != null)
                    {
                        c.ForcedLeftHandTarget = Vehicle.Volante.Find("Left");
                        c.ForcedRightHandTarget = Vehicle.Volante.Find("Right");
                    }
                    c.IKEnabled = true;
                }
            }


            PlayerCameraFadeIn(seconds);
            GameManager.Instance.TCN.ambientsound.volume = 0.3f;
            GameManager.Instance.TCN.ChangeJingleVolume(0.6f);
        }
        else
        {
            PlayerControllerGetIn();

            if (isServer)
            {
                if (CurrentSeat == VehicleSeat.Seat.Left)
                {
                    if (isClient)
                        Vehicle.LeftOccupant = netId;
                    else
                        Vehicle.OnLeftOccupantChanged(netId);
                }
                else
                {
                    if (isClient)
                        Vehicle.RightOccupant = netId;
                    else
                        Vehicle.OnRightOccupantChanged(netId);
                }

                if (Vehicle.GetSeat(CurrentSeat).Driver)
                {
                    Vehicle.GetClientAuthority(netId);
                    if (Vehicle.CanMove() || !GameManager.Instance.TCN.GameStarted)
                    {
                        ItemController.SetBrochureAvailable(false);
                        if (ItemController.Type == ItemControllerType.VR)
                            ItemController.StartPulsePublic(ControllerButton.All);
                        else
                            ItemController.SetCrosshairPublic(ItemControllerNet.CrosshairState.Rainbow);
                    }
                }
                else
                    ItemController.SetBrochureAvailable(true);
            }
            /*if (Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                Spawner.Armature.GetComponent<Animator>().SetLayerWeight(1, 0);
                Spawner.Armature.GetComponent<Animator>().SetBool("Driving", true);
            }*/
            if (Spawner.DrivingArmature != null)
            {
                var c = Spawner.DrivingArmature.GetComponent<RigIK>();
                if (c != null)
                {
                    if (CurrentSeat == VehicleSeat.Seat.Left)
                    {
                        c.ForcedLeftHandTarget = Vehicle.Volante.Find("Left");
                        c.ForcedRightHandTarget = Vehicle.Volante.Find("Right");
                    }
                    c.IKEnabled = true;
                }
            }


        }
    }

    IEnumerator GetOutOfCar(float seconds)
    {
        if (isLocalPlayer)
        {
            PlayerCameraFadeOut(seconds);
            yield return new WaitForSeconds(seconds);
            if (GameManager.Instance.TCN.lightscaling)
                GameManager.Instance.TCN.StopLightScaling();
            if(Vehicle.radio != null)

            {
                Vehicle.radio.TurnOff();
            }
            
            Vehicle.GetComponent<CarController>().enabled = false;

            PlayerControllerGetOut();

            if (isServer)
            {
                if (Vehicle.GetSeat(CurrentSeat).Driver)
                    Vehicle.RemoveClientAuthority(netId);

                playerStat.GoOutOfCar();
                if (CurrentSeat == VehicleSeat.Seat.Left)
                {
                    if (isClient)
                        Vehicle.LeftOccupant = NetworkInstanceId.Invalid;
                    else
                        Vehicle.OnLeftOccupantChanged(NetworkInstanceId.Invalid);
                }
                else
                {
                    if (isClient)
                        Vehicle.RightOccupant = NetworkInstanceId.Invalid;
                    else
                        Vehicle.OnRightOccupantChanged(NetworkInstanceId.Invalid);
                }
            }


            /*if (Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                Spawner.Armature.GetComponent<Animator>().SetBool("Driving", false);
                Spawner.Armature.GetComponent<Animator>().SetLayerWeight(1, 1);
            }*/
            if(Spawner.DrivingArmature != null)
            {
                var c = Spawner.DrivingArmature.GetComponent<RigIK>();
                if (c != null)
                {
                    if (CurrentSeat == VehicleSeat.Seat.Left)
                    {
                        c.ForcedLeftHandTarget = null;
                        c.ForcedRightHandTarget = null;
                    }
                    c.IKEnabled = false;
                }
            }
           

            PlayerCameraFadeIn(seconds);
            GameManager.Instance.TCN.ambientsound.volume = 0.7f;
            GameManager.Instance.TCN.ChangeJingleVolume(1);
        }
        else
        {
            PlayerControllerGetOut();

            if (isServer)
            {
                if (Vehicle.GetSeat(CurrentSeat).Driver)
                    Vehicle.RemoveClientAuthority(netId);

                if (CurrentSeat == VehicleSeat.Seat.Left)
                {
                    if (isClient)
                        Vehicle.LeftOccupant = NetworkInstanceId.Invalid;
                    else
                        Vehicle.OnLeftOccupantChanged(NetworkInstanceId.Invalid);
                }
                else
                {
                    if (isClient)
                        Vehicle.RightOccupant = NetworkInstanceId.Invalid;
                    else
                        Vehicle.OnRightOccupantChanged(NetworkInstanceId.Invalid);
                }
            }

            /*if (Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                Spawner.Armature.GetComponent<Animator>().SetBool("Driving", false);
                Spawner.Armature.GetComponent<Animator>().SetLayerWeight(1,1);
            }*/
            if(Spawner.DrivingArmature != null)
            {
                var c = Spawner.DrivingArmature.GetComponent<RigIK>();
                if (c != null)
                {
                    if (CurrentSeat == VehicleSeat.Seat.Left)
                    {
                        c.ForcedLeftHandTarget = null;
                        c.ForcedRightHandTarget = null;
                    }
                    c.IKEnabled = false;
                }
            }
           
        }
        Vehicle = null;
    }

    internal void StopWalking()
    {
        if (isLocalPlayer)
        {
            if (GameManager.Instance.Platform == ControllerType.MouseAndKeyboard)
                Spawner.CharacterMotor.enabled = false;
            else if (GameManager.Instance.Platform == ControllerType.CVirtualizer)
                Spawner.VirtualizerPlayerController.movementSpeedMultiplier = 0.0f;
            else if (GameManager.Instance.Platform == ControllerType.KatWalk)
            {
                Spawner.KatDeviceController.multiply = 0.0f;
                Spawner.KatDeviceController.multiplyBack = 0.0f;
            }
            else if (GameManager.Instance.Platform == ControllerType.FreeWalk)
                Spawner.CharacterControllerVR.Blocked = true;
            else
                Spawner.Swinger.SwingEnabled = false;
        }
    }

    internal void StartWalking()
    {
        if (isLocalPlayer)
        {
            if (GameManager.Instance.Platform == ControllerType.MouseAndKeyboard)
                Spawner.CharacterMotor.enabled = true;
            else if (GameManager.Instance.Platform == ControllerType.CVirtualizer)
                Spawner.VirtualizerPlayerController.movementSpeedMultiplier = 1.0f;
            else if (GameManager.Instance.Platform == ControllerType.KatWalk)
            {
                Spawner.KatDeviceController.multiply = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerSpawnerNet>().KatDeviceController.multiply;
                Spawner.KatDeviceController.multiplyBack = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerSpawnerNet>().KatDeviceController.multiplyBack;
            }
            else if (GameManager.Instance.Platform == ControllerType.FreeWalk)
                Spawner.CharacterControllerVR.Blocked = false;
            else
                Spawner.Swinger.SwingEnabled = true;
        }
    }
    void RotazioneSeCamion()
    {
        //devo girare se è un camion ruotae di 180 gradi, 
        //poichè è nel verso contrario alla macchina e selezione del pg
        if (this.Ruolo == SpawnMode.TruckDriver)
        {
            if (Spawner.FPSCamera != null)
            {
                Spawner.FPSCamera.transform.Rotate(0, 180, 0);
            }
                 Spawner.DrivingArmature.transform.Rotate(0, 180, 0);



            // Spawner.DrivingArmature.GetComponent<RigIK>().invertiVista = true;
        }
           
        //fine gestione rotazione se camion
    }

    void PlayerControllerGetIn()
    {
        if (Vehicle == null && VehicleID != NetworkInstanceId.Invalid)
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(VehicleID);
            else
                g = ClientScene.FindLocalObject(VehicleID);
            Vehicle = g.GetComponent<VehicleDataNet>();
        }

        if (Vehicle.CanMove() || !GameManager.Instance.TCN.GameStarted)
            Head.enabled = false;

        if (isLocalPlayer)
        {
            switch (Spawner.Platform)
            {
                case ControllerType.MouseAndKeyboard:
                    ItemController.ForceDrop();
                    var ccontr = GetComponent<CharacterController>();
                    var ccontr2 = GameManager.Instance.TCN.FPSNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    
                    ccontr.height = ccontr2.height;
                    var charc2 = GameManager.Instance.TCN.FPSNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    
                    

                    //charc.transformSyncMode = charc2.transformSyncMode;
                    
                    var obst = GetComponent<NavMeshObstacle>();
                    var obst2 = GameManager.Instance.TCN.FPSNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    var cm = GetComponent<CharacterMotor>();
                    cm.canControl = false;

                   /*if (!HideLocalPlayerRig)
                    {*/
                    
                    if (Spawner.DrivingArmature != null)
                    {
                        Spawner.Armature.gameObject.SetActive(false);
                        Spawner.DrivingArmature.gameObject.SetActive(true);
                        healthBar = Spawner.DrivingArmature.GetComponent<ModelCharacteristics>().HealthBar;
                    }
                    RotazioneSeCamion();
                    //}

                    break;
                case ControllerType.ArmSwing:
                    ItemController.ForceDrop();
                    var pcm = GetComponent<PlayerColliderManager>();
                    var pcm2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    RotazioneSeCamion();

                    ccontr2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    var fs = GetComponentInChildren<OpenVRSwinger>();
                    var fs2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponentInChildren<OpenVRSwinger>();
                    fs.SwingEnabled = fs2.SwingEnabled;
                    fs.transform.localPosition = fs2.transform.localPosition;
                    fs.enabled = fs2.enabled;
                    transform.localPosition = new Vector3(0, 0, 0);
                    var cvr = GetComponent<CharacterControllerVR>();
                    var cvr2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<CharacterControllerVR>();
                    cvr.Blocked = cvr2.Blocked;
                    var slt = GetComponent<SquaredLimitTrackingNet>();
                    var slt2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<SquaredLimitTrackingNet>();
                    slt.CopyConstraints(slt2);
                    if (!Vehicle.GetSeat(CurrentSeat).Driver)
                        slt.FlipXLimit();
                    slt.Reset();
                    slt.enabled = true;
                    break;
                case ControllerType.FootSwing:
                    ItemController.ForceDrop();
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    RotazioneSeCamion();

                    charc2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;

                    fs = GetComponentInChildren<OpenVRSwinger>();
                    fs2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponentInChildren<OpenVRSwinger>();
                    fs.SwingEnabled = fs2.SwingEnabled;

                    fs.transform.localPosition = fs2.transform.localPosition;
                    fs.enabled = fs2.enabled;
                    transform.localPosition = new Vector3(0, 0, 0);
                    cvr = GetComponent<CharacterControllerVR>();
                    cvr2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<CharacterControllerVR>();
                    cvr.Blocked = cvr2.Blocked;
                    slt = GetComponent<SquaredLimitTrackingNet>();
                    slt2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<SquaredLimitTrackingNet>();
                    slt.CopyConstraints(slt2);
                    if (!Vehicle.GetSeat(CurrentSeat).Driver)
                        slt.FlipXLimit();
                    slt.Reset();
                    slt.enabled = true;
                    break;
                case ControllerType.CVirtualizer:
                    ItemController.ForceDrop();
                    slt = GetComponent<SquaredLimitTrackingNet>();
                    slt2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<SquaredLimitTrackingNet>();
                    slt.CopyConstraints(slt2);
                    if (!Vehicle.GetSeat(CurrentSeat).Driver)
                        slt.FlipXLimit();
                    slt.Reset();
                    slt.enabled = true;
                    ccontr = GetComponent<CharacterController>();
                    RotazioneSeCamion();
                    ccontr2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                   // charc.transformSyncMode = charc2.transformSyncMode;
                    var cvp = GetComponent<CVirtPlayerController>();
                    var cvp2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<CVirtPlayerController>();
                    cvp.movementSpeedMultiplier = cvp2.movementSpeedMultiplier;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    transform.localPosition = new Vector3(0, 0, 0);
                    break;
                case ControllerType.KatWalk:
                    ItemController.ForceDrop();
                    slt = GetComponent<SquaredLimitTrackingNet>();
                    slt2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<SquaredLimitTrackingNet>();
                    slt.CopyConstraints(slt2);
                    if (!Vehicle.GetSeat(CurrentSeat).Driver)
                        slt.FlipXLimit();
                    slt.Reset();
                    slt.enabled = true;
                    ccontr = GetComponent<CharacterController>();
                    RotazioneSeCamion();
                    ccontr2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    var kd = Spawner.KatDeviceController;
                    var kd2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<PlayerSpawnerNet>().KatDeviceController;
                    kd.multiply = kd2.multiply;
                    kd.multiply = kd2.multiplyBack;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    transform.localPosition = new Vector3(0, 0, 0);
                    break;
                case ControllerType.FreeWalk:
                    ItemController.ForceDrop();
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    RotazioneSeCamion();
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    cvr = GetComponent<CharacterControllerVR>();
                    cvr2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<CharacterControllerVR>();
                    cvr.Blocked = cvr2.Blocked;
                    cvr.CameraRig.localPosition = cvr.CameraRig.localPosition;
                    transform.localPosition = new Vector3(0, 0, 0);
                    slt = GetComponent<SquaredLimitTrackingNet>();
                    slt2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<SquaredLimitTrackingNet>();
                    slt.CopyConstraints(slt2);
                    if (!Vehicle.GetSeat(CurrentSeat).Driver)
                        slt.FlipXLimit();
                    slt.Reset();
                    slt.enabled = true;
                    break;
                default:
                    break;
            }
        }
        else
        {
            if (Spawner == null)
                Spawner = GetComponent<PlayerSpawnerNet>();
            switch (Spawner.Platform)
            {
                case ControllerType.MouseAndKeyboard:
                    var ccontr = GetComponent<CharacterController>();
                    var ccontr2 = GameManager.Instance.TCN.FPSNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                  
                    var charc2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    var obst = GetComponent<NavMeshObstacle>();
                    var obst2 = GameManager.Instance.TCN.FPSNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    //var sm2 = GetComponent<FakeParenting>();
                    //sm2.SetFakeParent(Vehicle.GetSeat(CurrentSeat).transform, this);
                    Spawner.Armature.gameObject.SetActive(false);
                    Spawner.DrivingArmature.gameObject.SetActive(true);
                    RotazioneSeCamion();
                    /*ItemController.FakeLeftHand.parent = Spawner.DrivingArmature.GetComponent<ModelCharacteristics>().LeftHand;
                    ItemController.FakeLeftHand.localPosition = Vector3.zero;
                    ItemController.FakeLeftHand.localRotation = Quaternion.identity;
                    ItemController.FakeRightHand.parent = Spawner.DrivingArmature.GetComponent<ModelCharacteristics>().RightHand;
                    ItemController.FakeRightHand.localPosition = Vector3.zero;
                    ItemController.FakeRightHand.localRotation = Quaternion.identity;*/
                    healthBar = Spawner.DrivingArmature.GetComponent<ModelCharacteristics>().HealthBar;

                    break;
                case ControllerType.ArmSwing:
                    var pcm = GetComponent<PlayerColliderManager>();
                    var pcm2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    RotazioneSeCamion();
                    charc2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.ArmSwingNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    break;
                case ControllerType.FootSwing:
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    RotazioneSeCamion();
                    charc2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FootSwingNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    break;
                case ControllerType.CVirtualizer:                   
                    ccontr = GetComponent<CharacterController>();
                    RotazioneSeCamion();
                    ccontr2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.CVirtualizerNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    break;
                case ControllerType.KatWalk:
                    ccontr = GetComponent<CharacterController>();
                    RotazioneSeCamion();
                    ccontr2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.KatWalkNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    break;
                case ControllerType.FreeWalk:
                    pcm = GetComponent<PlayerColliderManager>();
                    RotazioneSeCamion();
                    pcm2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FreeWalkNoWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    break;
            }
        }
        if (Spawner.DrivingArmature != null)
            {
                Spawner.Armature.gameObject.SetActive(false);
                Spawner.DrivingArmature.gameObject.SetActive(true);
                healthBar = Spawner.DrivingArmature.GetComponent<ModelCharacteristics>().HealthBar;
            } 
        
        var sm = GetComponent<FakeParenting>();
        sm.SetFakeParent(Vehicle.GetSeat(CurrentSeat).transform, this);

        if (OnVehicleEntered != null)
            OnVehicleEntered.Invoke(this);
    }

    internal bool IsAlive()
    {
        if (dead || HasToDie())
            return false;
        else
            return true;
    }

    internal VehicleSeat.Seat GetCurrentSeat()
    {
        return CurrentSeat;
    }

    public void PlayerCameraFadeIn(float time)
    {
        if (isLocalPlayer)
        {
            if (Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                FPSFadeIn(time);
            }
            else
            {
                SteamVRFade.fadeIn(time);
            }
        }
    }
    public void PlayerCameraFadeOut(float time)
    {
        if (isLocalPlayer)
        {
            if (Spawner.Platform == ControllerType.MouseAndKeyboard)
            {
                FPSFadeOut(time);
            }
            else
            {
                SteamVRFade.fadeOut(time);
            }
        }
    }

    private void FPSFadeOut(float time)
    {
        if (faderoutine != null)
            StopCoroutine(faderoutine);
        faderoutine = StartCoroutine(FPSFadeOut(Spawner.BlackScreen.GetComponent<Image>(), time, Time.time));
    }

    private void FPSFadeIn(float time)
    {
        if (faderoutine != null)
            StopCoroutine(faderoutine);
        faderoutine = StartCoroutine(FPSFadeIn(Spawner.BlackScreen.GetComponent<Image>(), time, Time.time));
    }

    public static IEnumerator FPSFadeOut(Image screen, float FadeTime, float startfade)
    {
        if (FadeTime == 0)
            screen.color = new Color(screen.color.r, screen.color.g, screen.color.b, 255);
        else
            while (screen.color.a < 255)
            {
                var newalpha = screen.color.a + 255 * (Time.time - startfade) / FadeTime;
                if (newalpha > 255)
                    newalpha = 255;

                screen.color = new Color(screen.color.r, screen.color.g, screen.color.b, newalpha);
                yield return null;
            }
    }
    public static IEnumerator FPSFadeIn(Image screen, float FadeTime, float startfade)
    {
        if (FadeTime == 0)
            screen.color = new Color(screen.color.r, screen.color.g, screen.color.b, 0);
        else
            while (screen.color.a > 0)
            {
                var newalpha = screen.color.a - 255 * (Time.time - startfade) / FadeTime;
                if (newalpha < 0)
                    newalpha = 0;

                screen.color = new Color(screen.color.r, screen.color.g, screen.color.b, newalpha);
                yield return null;
            }
    }


    internal void BrochureAvailable()
    {
        if (isServer)
            ItemController.SetBrochureAvailable(true);
    }

    internal void CanBrake()
    {
        if (isServer)
        {
            if (VehicleID != NetworkInstanceId.Invalid && Vehicle.GetSeat(CurrentSeat).Driver)
            {
                if (Spawner.Platform == ControllerType.MouseAndKeyboard)
                {
                    if (!isClient)
                        GetComponent<ItemControllerNet>().SetCrosshairPublic(ItemControllerNet.CrosshairState.Rainbow);

                    GetComponent<ItemControllerNet>().RpcSetCrosshairPublic(ItemControllerNet.CrosshairState.Rainbow);
                }
                else
                {
                    GetComponent<ItemControllerNet>().StartPulsePublic(ControllerButton.All);
                }
            }
        }
    }

    internal void CantBrake()
    {
        if (isServer)
        {
            if (VehicleID != NetworkInstanceId.Invalid && Vehicle.GetSeat(CurrentSeat).Driver)
            {

                if (Spawner.Platform == ControllerType.MouseAndKeyboard)
                {
                    if (!isClient)
                        GetComponent<ItemControllerNet>().SetCrosshairPublic(ItemControllerNet.CrosshairState.None);

                    GetComponent<ItemControllerNet>().RpcSetCrosshairPublic(ItemControllerNet.CrosshairState.None);
                }
                else
                {
                    GetComponent<ItemControllerNet>().StopPulsePublic();
                }
            }
        }
    }

    internal void StopBraking()
    {
        if (isLocalPlayer)
        {
            if (Spawner.Platform != ControllerType.MouseAndKeyboard)
            {
                if (leftv != null)
                {
                    leftv.StopVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this);
                    leftv = null;
                }

                if (rightv != null)
                {
                    rightv.StopVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this);
                    rightv = null;
                }
            }
        }
    }
    internal void ShowBrake()
    {
        if (isLocalPlayer && VehicleID != NetworkInstanceId.Invalid)
        {
            if (Spawner.Platform == ControllerType.CVirtualizer)
            {
                if (transform.Find("CameraHolder").GetComponentInChildren<SteamVR_ControllerManager>().left != null)
                {
                    var l = transform.Find("CameraHolder").GetComponentInChildren<SteamVR_ControllerManager>().left.GetComponent<VibrationController>();
                    if (l.StartVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this))
                        leftv = l;
                }

                if (transform.Find("CameraHolder").GetComponentInChildren<SteamVR_ControllerManager>().right != null)
                {
                    var r = transform.Find("CameraHolder").GetComponentInChildren<SteamVR_ControllerManager>().right.GetComponent<VibrationController>();
                    if (r.StartVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this))
                        rightv = r;
                }
            }
            else if (Spawner.Platform != ControllerType.MouseAndKeyboard)
            {
                if (transform.Find("[CameraRig]").GetComponent<SteamVR_ControllerManager>().left != null)
                {
                    var l = transform.Find("[CameraRig]").GetComponent<SteamVR_ControllerManager>().left.GetComponent<VibrationController>();
                    if (l.StartVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this))
                        leftv = l;
                }

                if (transform.Find("[CameraRig]").GetComponent<SteamVR_ControllerManager>().right != null)
                {
                    var r = transform.Find("[CameraRig]").GetComponent<SteamVR_ControllerManager>().right.GetComponent<VibrationController>();
                    if (r.StartVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this))
                        rightv = r;
                }
            }
        }
    }

    void PlayerControllerGetOut()
    {
        if (Vehicle == null && VehicleID != NetworkInstanceId.Invalid)
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(VehicleID);
            else
                g = ClientScene.FindLocalObject(VehicleID);
            Vehicle = g.GetComponent<VehicleDataNet>();
        }
        Vector3 pos = Vector3.zero;

        //per gestire il camion che è girato nella direzione opposta, inverto i segni dell'incremento
        //della x
        float sideModifier = 1;
        if(Vehicle is TruckControllerNet)
        {
            sideModifier = -1;
        }

        if (CurrentSeat == VehicleSeat.Seat.Left)
            pos = transform.position + new Vector3(-1.3f* sideModifier, -transform.position.y, 0);
        else
            pos = transform.position + new Vector3(1.3f* sideModifier, -transform.position.y, 0);

        ActivatePlayerController(pos);
        if (OnVehicleLeft != null)
            OnVehicleLeft.Invoke(this);
    }

    private void ActivatePlayerController(Vector3 pos)
    {
        if (isLocalPlayer)
        {
            Head.enabled = true;

            switch (Spawner.Platform)
            {
                case ControllerType.MouseAndKeyboard:
                    transform.position = pos;
                    var ccontr = GetComponent<CharacterController>();
                    var ccontr2 = GameManager.Instance.TCN.FPSControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    var charc2 = GameManager.Instance.TCN.FPSControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    var obst = GetComponent<NavMeshObstacle>();
                    var obst2 = GameManager.Instance.TCN.FPSControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    var cm = GetComponent<CharacterMotor>();
                    cm.canControl = true;
                    break;
                case ControllerType.ArmSwing:
                    var pcm = GetComponent<PlayerColliderManager>();
                    var pcm2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    var fs = GetComponentInChildren<OpenVRSwinger>();
                    var fs2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponentInChildren<OpenVRSwinger>();
                    fs.SwingEnabled = fs2.SwingEnabled;
                    fs.transform.localPosition = fs2.transform.localPosition;
                    fs.enabled = fs2.enabled;
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    var cvr = GetComponent<CharacterControllerVR>();
                    var cvr2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<CharacterControllerVR>();
                    cvr.Blocked = cvr2.Blocked;
                    break;
                case ControllerType.FootSwing:
                    transform.position = pos;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    fs = GetComponentInChildren<OpenVRSwinger>();
                    fs2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponentInChildren<OpenVRSwinger>();
                    fs.SwingEnabled = fs2.SwingEnabled;
                    fs.transform.localPosition = fs2.transform.localPosition;
                    fs.enabled = fs2.enabled;                    
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    cvr = GetComponent<CharacterControllerVR>();
                    cvr2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<CharacterControllerVR>();
                    cvr.Blocked = cvr2.Blocked;
                    break;
                case ControllerType.CVirtualizer:
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    charc2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    transform.position = pos;
                    var cvp = GetComponent<CVirtPlayerController>();
                    var cvp2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<CVirtPlayerController>();
                    cvp.movementSpeedMultiplier = cvp2.movementSpeedMultiplier;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    break;
                case ControllerType.KatWalk:
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    charc2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    transform.position = pos;
                    var kd = Spawner.KatDeviceController;
                    var kd2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerSpawnerNet>().KatDeviceController;
                    kd.multiply = kd2.multiply;
                    kd.multiplyBack = kd2.multiplyBack;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    break;
                case ControllerType.FreeWalk:
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    //Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    cvr = GetComponent<CharacterControllerVR>();
                    cvr2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<CharacterControllerVR>();
                    cvr.Blocked = cvr2.Blocked;
                    cvr.CameraRig.localPosition = cvr.CameraRig.localPosition;
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    break;
            }
        }
        else
        {
            switch (Spawner.Platform)
            {
                case ControllerType.MouseAndKeyboard:
                    transform.position = pos;
                    var ccontr = GetComponent<CharacterController>();
                    var ccontr2 = GameManager.Instance.TCN.FPSControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    var charc2 = GameManager.Instance.TCN.FPSControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    var obst = GetComponent<NavMeshObstacle>();
                    var obst2 = GameManager.Instance.TCN.FPSControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    break;
                case ControllerType.ArmSwing:
                    var pcm = GetComponent<PlayerColliderManager>();
                    var pcm2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.ArmSwingControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    break;
                case ControllerType.FootSwing:
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FootSwingControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    break;
                case ControllerType.CVirtualizer:
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    charc2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                   // charc.transformSyncMode = charc2.transformSyncMode;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.CVirtualizerControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    break;
                case ControllerType.KatWalk:
                    GetComponent<SquaredLimitTrackingNet>().enabled = false;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    charc2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                    //charc.transformSyncMode = charc2.transformSyncMode;
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.KatWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    break;
                case ControllerType.FreeWalk:
                    pcm = GetComponent<PlayerColliderManager>();
                    pcm2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                    pcm.enabled = pcm2.enabled;
                    ccontr = GetComponent<CharacterController>();
                    ccontr2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<CharacterController>();
                    ccontr.enabled = ccontr2.enabled;
                    ccontr.center = ccontr2.center;
                    ccontr.height = ccontr2.height;
                    charc2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<PlayerStatusNet>().charc;
                   // charc.transformSyncMode = charc2.transformSyncMode;
                    obst = GetComponent<NavMeshObstacle>();
                    obst2 = GameManager.Instance.TCN.FreeWalkControllerPrefab.GetComponent<NavMeshObstacle>();
                    obst.enabled = obst2.enabled;
                    obst.center = obst2.center;
                    obst.height = obst2.height;
                    Spawner.Armature.GetComponent<VRIK>().solver.Reset();
                    transform.position = pos;
                    break;
            }
        }

        Spawner.Armature.gameObject.SetActive(true);
        if(Spawner.DrivingArmature != null)
            Spawner.DrivingArmature.gameObject.SetActive(false);

        
        healthBar = Spawner.Armature.GetComponent<ModelCharacteristics>().HealthBar;
        var sm = GetComponent<FakeParenting>();
        sm.SetFakeParent(null, Vector3.zero);

        if (OnVehicleLeft != null)
            OnVehicleLeft.Invoke(this);
    }

    public IEnumerator ApplyDamageWithPain(float damage)
    {
        if (isServer)
        {
            if (damage == 0)
                yield break;
            yield return new WaitWhile(() => damaging);
            damaging = true;
            if (!HasToDie())
            {
                ApplyDamage(damage);

                if (Mouth.isPlaying && Mouth.clip != Scream)
                {
                    Mouth.Stop();
                    Mouth.loop = false;
                    Mouth.clip = Scream;
                    Mouth.Play();
                }
                else if (!Mouth.isPlaying)
                {
                    Mouth.clip = Scream;
                    Mouth.loop = false;
                    Mouth.Play();
                }

                RpcScream();
            }
            damaging = false;
        }
    }

    [ClientRpc]
    private void RpcScream()
    {
        if (!isServer)
        {
            if (Mouth.isPlaying && Mouth.clip != Scream)
            {
                Mouth.Stop();
                Mouth.loop = false;
                Mouth.clip = Scream;
                Mouth.Play();
            }
            else if (!Mouth.isPlaying)
            {
                Mouth.clip = Scream;
                Mouth.loop = false;
                Mouth.Play();
            }
        }
    }

    public IEnumerator ApplyDamageWithoutPain(float damage)
    {
        if (damage == 0)
            yield break;
        yield return new WaitWhile(() => damaging);
        damaging = true;
        if (!HasToDie())
            ApplyDamage(damage);
        damaging = false;
    }

    void ApplyDamage(float damage)
    {
        if (isServer)
        {
            if (HasToDie() || damage == 0)
                return;

            if (damage < 0)
                damage = -damage;
            if (health - damage < 0)
            {
                if (isClient)
                    health = 0;
                else
                    OnChangedHealth(0);
            }
            else
            {
                if (isClient)
                    health -= damage;
                else
                    OnChangedHealth(health - damage);
            }

            LastDamage = Time.time;
        }
    }

    public void ForceDie()
    {
        if (isServer && !dead)
            TerminateGame();
    }

    public void Die()
    {
        if (isServer && HasToDie() && !dead)
        {
            TerminateGame();
            dead = true;
        }
    }

    public void TerminateGame()
    {
        if (isLocalPlayer)
            EndPlayerGame();
        else
            RpcTerminateGame();
    }

    void EndPlayerGame()
    {
        playerStat.EndPlayerGame();
    }

    [ClientRpc]
    private void RpcTerminateGame()
    {
        if (isLocalPlayer)
            EndPlayerGame();
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            // Set the spawn point to origin as a default value
            Vector3 spawnPoint = Vector3.zero;

            // If there is a spawn point array and the array is not empty, pick a spawn point at random
            if (spawnPoints != null && spawnPoints.Length > 0)
            {
                spawnPoint = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length)].transform.position;
            }

            // Set the player’s position to the chosen spawn point
            transform.position = spawnPoint;
        }
    }

    public void Recover(float time)
    {
        if (isServer)
        {
            if (!HasToDie())
            {
                var h = health + 1;

                if (h > 100)
                {
                    if (isClient)
                        health = 100;
                    else
                        OnChangedHealth(100);
                    NextOperation = float.MinValue;
                }
                else
                {
                    if (isClient)
                        health = h;
                    else
                        OnChangedHealth(h);
                    NextOperation = time + 1;
                }
            }
        }
    }

    public void Intoxicate(float toxicity)
    {
        if (isServer)
        {
            if (!HasToDie())
            {
                if (!Intoxication)
                {
                    IntoxicationAmount = toxicity;
                    if (isClient)
                        Intoxication = true;
                    else
                        OnIntoxicationChanged(true);
                }
                else
                {
                    if (toxicity > IntoxicationAmount)
                        IntoxicationAmount = toxicity;
                    IntoxicationEnd = Time.time + IntoxicationTime;
                }
            }
        }
    }

    internal PlayerStatusData CopyStatus(PlayerStatusNet playerStatus)
    {
        if (isServer)
        {
            return new PlayerStatusData(this);
        }
        return null;
    }

    internal void PasteStatus(PlayerStatusData playerStatus)
    {
        if (isServer)
        {
            if (playerStatus != null)
            {
                health = playerStatus.health;
                Intoxication = playerStatus.Intoxication;
                IntoxicationAmount = playerStatus.IntoxicationAmount;
                IntoxicationEnd = playerStatus.IntoxicationEnd;
                IntoxicationTime = playerStatus.IntoxicationTime;
                NextOperation = playerStatus.NextOperation;
                fogamount = playerStatus.fogamount;
                LastDamage = playerStatus.LastDamage;
            }
        }
    }

    //tesisti:
    public void StarUscitaWaitingZone()
    {
        // 3 secondi inizia l'avviso sonoro, 35 secondi spawna vicino al camion dei pompieri
        StartCoroutine(GetOutOfWaitingZone(8, 40));
  
    }
    IEnumerator GetOutOfWaitingZone(float tempoAvviso, float startTime)
    {
        yield return new WaitForSeconds(startTime-tempoAvviso);
        var sirena = GameObject.Find("SirenaFrejus").GetComponent<AudioSource>();
        sirena.Play();
        yield return new WaitForSeconds(tempoAvviso);
        sirena.Stop();
        this.ItemController.ForceDrop();
        //posoziono il pompiere vicino al camion
        Vector3 pos = GameManager.Instance.TCN.NAFiretruck.gameObject.transform.position + new Vector3(4f,0,0);
        GameManager.Instance.TCN.ComponenteEsceTraining(this);
        //SpawnPointPompiere.position;
        if (isLocalPlayer)
        {
            PlayerCameraFadeOut(3);
            yield return new WaitForSeconds(2);
            if (GameManager.Instance.TCN.lightscaling)
                GameManager.Instance.TCN.StopLightScaling();

            
            ActivatePlayerController(pos);
          
          

            PlayerCameraFadeIn(3);
            GameManager.Instance.TCN.ambientsound.volume = 0.7f;
            GameManager.Instance.TCN.ChangeJingleVolume(1);

            StartCoroutine(TimerSpegnimentoIncendio());
        }
        else
        {
            ActivatePlayerController(pos);


            var c = Spawner.DrivingArmature.GetComponent<RigIK>();
            if (c != null)
            {
                c.IKEnabled = false;
            }
        }
    }

    IEnumerator TimerSpegnimentoIncendio()
    {
        yield return new WaitForSeconds(60*15);
       
        if(GameManager.Instance.TCN != null && !GameManager.Instance.TCN.GameCanEndForComponente())
        {
            //ForceDie();
        }
    }
}
