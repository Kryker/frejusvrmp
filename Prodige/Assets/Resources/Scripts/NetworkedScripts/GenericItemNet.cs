﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class GenericItemNet : MultiNetworkBehaviour {

    [HideInInspector]
    public ControllerManager Controller;
    
	public virtual void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        Interact(c, hand);
    }

	public ItemCodes ItemCode = ItemCodes.Generic;
    public bool Grabbable = false;

    public virtual void DisableOutline(GenericItemSlave slave, ItemControllerNet c)
    {
        DisableOutline(c);
    }

    public virtual void EnableOutline(GenericItemSlave slave, ItemControllerNet c)
    {
        EnableOutline(c);
    }

    public virtual bool Unselect(GenericItemSlave slave)
    {
        return Unselect();
    }

    public virtual bool Select(GenericItemSlave slave)
    {
        return Select();
    }

    public virtual void ClickButton(object sender)
    {
    }

    public virtual void UnClickButton(object sender)
    {
    }

    /*public virtual void ClickButton(object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
        throw new NotImplementedException();
    }

    public virtual void UnClickButton(object sender, ClickedEventArgs e, GenericItemSlave slave)
    {
        throw new NotImplementedException();
    }*/
	
    public bool CanInteractFromStart = true;
    [SyncVar (hook = "OnCanInteractChanged")]
    public bool _CanInteract;
    [HideInInspector]
    [SyncVar]
    public bool AlreadySelected;
    [HideInInspector]
    public bool outlined = false;
    public bool IsKinematic = false;
    [HideInInspector]
    public Transform InitialParent;
    [HideInInspector]
    public Vector3 prevpos;
    [HideInInspector]
    public Vector3 currvelocity;
    [HideInInspector]
    public Vector3 direction;
    Rigidbody rbody;
    public List<Transform> Pieces;
    [HideInInspector]
    [SyncVar]
    public bool ItemActive;

    public enum RotationAxis { X, Y, Z };
    [HideInInspector]
    public Transform SeekTarget;

    [HideInInspector]
    public Vector3 startParentPosition;
    [HideInInspector]
    public Quaternion startParentRotationQ;
    [HideInInspector]
    public Vector3 startChildPosition;
    [HideInInspector]
    public Quaternion startChildRotationQ;

    public GenericItemSlave Slave;
    public ItemControllerNet Player;

    // Use this for initialization
    public override void Start()
    {
        base.Start();
        if (isServer)
        {
            ItemActive = false;
			
			if(Slave != null)
				prevpos = Slave.transform.position;
			else
				prevpos = transform.position;
				
            if (CanInteractFromStart)
                SetCanInteract(true);
            else
                SetCanInteract(false);
        }
		if(Slave != null)
			{
			Slave.MasterNet = this;
			InitialParent = Slave.transform.parent;
            rbody = Slave.GetComponent<Rigidbody>();
			}
		else
			{
			InitialParent = transform.parent;
            rbody = GetComponent<Rigidbody>();
			}
    }
    
    // Update is called once per frame
    public virtual void Update () {
	}

    public void OnCanInteractChanged(bool can)
    {
        //Debug.LogError(this.gameObject.name + " Passa allo stato " + can);
        _CanInteract = can;
        if (!can && outlined)
            Unselect();
    }

    public virtual void EnablePhysics()
    {
        if (!IsKinematic && rbody != null)
            {
                rbody.isKinematic = false;
            if (isServer)
                rbody.velocity = currvelocity;
            }
    }

    public virtual void DisablePhysics()
    {
            if (!IsKinematic && rbody != null)
                rbody.isKinematic = true;
    }

    public virtual bool CanInteract(ItemControllerNet c)
    {
        return _CanInteract;
    }

    public virtual void SetCanInteract(bool can, ItemControllerNet c = null)
    {
        if (isServer)
        {
            _CanInteract = can;
            if (!can && outlined)
                Unselect();
        }
    }

    [Command]
    private void CmdCanInteract(bool can, NetworkInstanceId id)
    {
        _CanInteract = can;
        if (!can && outlined)
            Unselect();
    }

    public virtual bool Select()
    {
        if (isServer)
        {
            if (AlreadySelected)
                return false;

            if(!isClient)
                EnableOutline(null);
            AlreadySelected = true;
            RpcSelect(locID);
            return true;
        }
        return false;
    }

    public virtual void EnableOutline(ItemControllerNet c)
    {
        Material[] mts = null;
        if (Slave == null)
        {
            var m = GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        else
        {
            var m = Slave.GetComponent<MeshRenderer>();
            if(m != null)
                mts = m.materials;
        }

        Color col;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 255;
                m.SetColor("_OutlineColor", col);
            }

            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 255;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = true;
    }
    

    [ClientRpc]
    public void RpcSelect(uint ID)
    {
            try
            {
                if (this.locID != ID)
                    ((GenericItemNet)IDProvider.MultiNetworkBehaviours[ID]).PropagatedRpcSelect();
                else
                    EnableOutline(null);
            }
            catch(KeyNotFoundException k)
            {
                Debug.Log(k.Message);
            }
    }

    private void PropagatedRpcSelect()
    {
        EnableOutline(null);
    }

    public virtual bool Unselect()
    {
        if (isServer)
        {
            if (!AlreadySelected)
                return false;

            if(!isClient)
                DisableOutline(null);
            AlreadySelected = false;
            RpcUnselect(locID);
            return true;
        }
        return false;
    }

    public virtual void DisableOutline(ItemControllerNet c)
    {
         Material[] mts = null;
        if (Slave == null)
        {
            var m = GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        else
        {
            var m = Slave.GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        Color col;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 0;
                m.SetColor("_OutlineColor", col);
            }
            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 0;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = false;
    }
    
    [ClientRpc]
    public void RpcUnselect(uint ID)
    {
            try
            {
                if (this.locID != ID)
                    ((GenericItemNet)IDProvider.MultiNetworkBehaviours[ID]).PropagatedUnselect();
                else
                    DisableOutline(null);
            }
            catch(KeyNotFoundException k)
            {
                Debug.Log(k.Message);
            }
    }

    private void PropagatedUnselect()
    {
        DisableOutline(null);
    }

    public void EnableItem(ItemControllerNet player, ControllerHand hand)
    {
		if(Slave != null)
		    {
            if (!Slave.gameObject.activeSelf)
                {
                    Slave.gameObject.SetActive(true);
                    if (ItemCode != ItemCodes.Generic)
                        {
                        if (player.Type == ItemControllerType.VR)
                            {
                                if (hand == ControllerHand.LeftHand)
                                {
                                    player.InputManager.OnLeftPadPressed -= ClickButton;
                                    player.InputManager.OnLeftPadUnpressed -= UnClickButton;
                                }
                                else if (hand == ControllerHand.RightHand)
                                {

                                    player.InputManager.OnRightPadPressed -= ClickButton;
                                    player.InputManager.OnRightPadUnpressed -= UnClickButton;
                                }
                            }
                        }
                    else
                        {
                        if(hand == ControllerHand.LeftHand)
                            Destroy(player.LeftController.transform.Find("placeholder").gameObject);
                        else if (hand == ControllerHand.RightHand)
                            Destroy(player.RightController.transform.Find("placeholder").gameObject);
                        }
                }
		    }
		else
		    {
            if (!gameObject.activeSelf)
                {
                    gameObject.SetActive(true);
                    if (ItemCode != ItemCodes.Generic)
                    {
                            if (player.Type == ItemControllerType.VR)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                    player.InputManager.OnLeftPadPressed -= ClickButton;
                                    player.InputManager.OnLeftPadUnpressed -= UnClickButton;
                                }
                                else if (hand == ControllerHand.RightHand)
                                    {

                                    player.InputManager.OnRightPadPressed -= ClickButton;
                                    player.InputManager.OnRightPadUnpressed -= UnClickButton;
                                }
                            }
                    }
                    else
                        {
                            if (hand == ControllerHand.LeftHand)
                                Destroy(player.LeftController.transform.Find("placeholder").gameObject);
                            else if (hand == ControllerHand.RightHand)
                                Destroy(player.RightController.transform.Find("placeholder").gameObject);
                        }
                }
		    }
    }

    private void OnDisable()
    {
        if (Controller != null)
            Controller.ForceTriggerExit(GetComponent<Collider>());
    }

    public virtual void ClickButton() { }

    public virtual void UnClickButton() { }

    public virtual void Interact(ItemControllerNet c, ControllerHand hand) { }

    public void DisableItem(ItemControllerNet player, ControllerHand hand)
    {  
	if (Slave != null)
	{
		if (Slave.gameObject.activeSelf)
        {
            Unselect();
            Slave.gameObject.SetActive(false);
            if (ItemCode != ItemCodes.Generic)
                {
                    if (player.Type == ItemControllerType.VR)
                    {
                        if (hand == ControllerHand.LeftHand)
                        {
                            player.InputManager.OnLeftPadPressed += ClickButton;
                            player.InputManager.OnLeftPadUnpressed += UnClickButton;
                        }
                        else if (hand == ControllerHand.RightHand)
                        {

                            player.InputManager.OnRightPadPressed += ClickButton;
                            player.InputManager.OnRightPadUnpressed += UnClickButton;
                        }
                    }
            }
            else
            {
                GameObject p = null;
                if(hand == ControllerHand.LeftHand)
                    p = Instantiate(Slave.gameObject, player.LeftController);
                if (hand == ControllerHand.RightHand)
                    p = Instantiate(Slave.gameObject, player.RightController);
                p.transform.localPosition = Vector3.zero;
                p.transform.localRotation = Quaternion.Euler(Vector3.zero);
                p.transform.name = "placeholder";
                Destroy(p.GetComponent<GenericItemNet>());
                Destroy(p.GetComponent<NetworkTransform>());
                Destroy(p.GetComponent<NetworkIdentity>());
                Destroy(p.GetComponent<Rigidbody>());
                p.gameObject.SetActive(true);
            }
        }
	}
else
    {
        if (gameObject.activeSelf)
        {
            Unselect();
            gameObject.SetActive(false);
            var im = player.GetComponent<PlayerStatusNet>().ItemController;
            if (ItemCode != ItemCodes.Generic)
            {
                    if (im.Type == ItemControllerType.VR)
                    {
                        if (hand == ControllerHand.LeftHand)
                        {
                            im.InputManager.OnLeftPadPressed += ClickButton;
                            im.InputManager.OnLeftPadUnpressed += UnClickButton;
                        }
                        if (hand == ControllerHand.RightHand)
                        {
                            im.InputManager.OnRightPadPressed += ClickButton;
                            im.InputManager.OnRightPadUnpressed += UnClickButton;
                        }
                    }
            }
            else
                {
                GameObject p = null;
                if (hand == ControllerHand.LeftHand)
                    p = Instantiate(gameObject, im.LeftController);
                else if (hand == ControllerHand.RightHand)
                    p = Instantiate(gameObject, im.RightController);
                p.transform.localPosition = Vector3.zero;
                p.transform.localRotation = Quaternion.Euler(Vector3.zero);
                p.transform.name = "placeholder";
                Destroy(p.GetComponent<GenericItemNet>());
                Destroy(p.GetComponent<NetworkTransform>());
                Destroy(p.GetComponent<NetworkIdentity>());
                Destroy(p.GetComponent<Rigidbody>());
                p.gameObject.SetActive(true);
            }
        }
	}
}
    

    public virtual void ForceParent(Transform p, bool KeepOrientation)
    {
        SeekTarget = p;
        if (KeepOrientation)
        {
            startParentPosition = p.position;
            startParentRotationQ = p.rotation;

            startChildPosition = transform.position;
            startChildRotationQ = transform.rotation;

            startChildPosition = DivideVectors(Quaternion.Inverse(p.rotation) * (startChildPosition - startParentPosition), p.lossyScale);
        }
        else
        {
            startParentPosition = Vector3.zero;
            startParentRotationQ = Quaternion.identity;

            startChildPosition = Vector3.zero;
            startChildRotationQ = Quaternion.identity;
        }
    }
    Vector3 DivideVectors(Vector3 num, Vector3 den)
    {

        return new Vector3(num.x / den.x, num.y / den.y, num.z / den.z);

    }
    public virtual void DropParent()
    {
        SeekTarget = null;
        startParentPosition = Vector3.zero;
        startParentRotationQ = Quaternion.identity;

        startChildPosition = Vector3.zero;
        startChildRotationQ = Quaternion.identity;
    }
    public virtual void ClickButton(object sender, ClickedEventArgs e)
    {
    }

    public virtual void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    public virtual void Reset()
    {
        if (isServer)
        {
            if (CanInteractFromStart)
                SetCanInteract(true);
            else
                SetCanInteract(false);
        }
    }
}
