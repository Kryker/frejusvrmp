﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PulsanteChiamataSOSNet : GenericItemNet
{
    //public Vector3 ButtonPositionAction;
    public NetworkAnimator nanimator;
    //Vector3 ButtonPositionRest;
    public List<AudioClip> clips;
    public bool ResponseAvailable = true;
    public AudioSource Speaker, Button;
    [HideInInspector]
    public float MaxDist;
    [HideInInspector]
    public float MinDist;
    [HideInInspector]
    public Vector3 StartSpeakerPos;
    //Transform StartParent;
    public bool InsideShelter = false;
    bool operating = false;
    AdvancedStateMachineBehaviour Pressed, Unpressed;
    public Posizione posizione;

    //TunnelController TC;
    // Use this for initialization
    private void Awake()
    {
        ItemCode = ItemCodes.SOSCall;
        //ButtonPositionRest = transform.localPosition;
    }

    public override void Start()
    {
        if (ResponseAvailable)
        {
            StartSpeakerPos = Speaker.transform.localPosition;
            MaxDist = Speaker.maxDistance;
            MinDist = Speaker.minDistance;
        }
        base.Start();

        var advancedbehaviours = nanimator.animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            if (ad.StateName == "Pressed")
            {
                Pressed = ad;
            }
            else if (ad.StateName == "Unpressed")
            {
                Unpressed = ad;
            }
        }
        Pressed.StateEnter += ButtonPressed;
        Pressed.StatePlayed += ButtonRelease;
        Unpressed.StateEnter += ButtonReleased;
        Unpressed.StatePlayed += StopOperating;
        //StartParent = transform.parent;
    }

    public void ButtonPressed(AdvancedStateMachineBehaviour st)
    {
        Button.clip = clips[0];
        Button.Play();
    }
    public void StopOperating(AdvancedStateMachineBehaviour st)
    {
        operating = false;
    }
    public void ButtonRelease(AdvancedStateMachineBehaviour st)
    {
        UnClickButton();
    }

    public void ButtonReleased(AdvancedStateMachineBehaviour st)
    {
        Button.clip = clips[1];
        Button.Play();
    }

    // Update is called once per frame
    public override void Update()
    {
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        ClickButton();
    }

    new void ClickButton()
    {
        nanimator.animator.SetBool("PressButton", true);
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
        UnClickButton();
    }
    public override void ClickButton(object sender)
    {
        ClickButton();
    }

    public override void UnClickButton(object sender)
    {
        UnClickButton();
    }
    new void UnClickButton()
    {
        nanimator.animator.SetBool("PressButton", false);
    }

    public override void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract(c))
            Call(false, c);
    }
    
    private void Call(bool forced, ItemControllerNet c)
    {

        var ruolo = c.GetComponent<PlayerStatusNet>().Ruolo;

      
        if (!operating)
        {
            operating = true;
            if (!ItemActive)
            {
                ItemActive = true;
                if (ResponseAvailable)
                {
                   
                    if (!GameManager.Instance.TCN.GPN.GetSOSRequested(ruolo))
                    {
                      
                        if(isServer)
                        {
                            GameManager.Instance.TCN.GPN.SetAlarmTriggered(true, ruolo);
                            GameManager.Instance.TCN.GPN.SetSOSRequested(true, ruolo);

                        }
                        else
                        {
                            GameManager.Instance.TCN.GPN.CmdSetAlarmTriggered(true, ruolo);
                            GameManager.Instance.TCN.GPN.CmdSetSOSRequested(true, ruolo);
                        }

                        if (posizione == Posizione.Rifugio && GameManager.Instance.TCN.AllPlayersInside())
                            GameManager.Instance.TCN.StopEndRoutine();
                        if (!isClient)
                            StartCoroutine(AudioMessaggeRequest());
                        RpcAudioMessageRequest();
                    }
                    if(!forced)
                        ClickButton();
                }
                else
                {
                    if (!GameManager.Instance.TCN.GPN.GetAlarmTriggered(ruolo))
                    {
                        if (isServer)
                        {
                            GameManager.Instance.TCN.GPN.CmdSetAlarmTriggered(true, ruolo);

                        }
                        else
                        {
                            GameManager.Instance.TCN.GPN.SetAlarmTriggered(true, ruolo);

                        }
                    }
                    if (!forced)
                        ClickButton();
                }
            }
            else if (!forced)
                ClickButton();
        }
    }

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract(c))
            Call(false, c);
    }

    public void ForceCall()
    {
        if (isServer)
            Debug.Log("Ho rimosos questo.");
            //Call(true);
    }
    [ClientRpc]
    private void RpcAudioMessageRequest()
    {
        StartCoroutine(AudioMessaggeRequest());
    }

    private IEnumerator AudioMessaggeRequest()
    {
        /*if(!InsideShelter)
            TC.ChangeJingleVolume(0.3f);*/
        if (isServer)
            operating = true;
        /*ClickButton();
        audiosource.clip = clips[0];
        audiosource.Play();
        yield return new WaitUntil(() => !audiosource.isPlaying);
        UnClickButton();
        audiosource.clip = clips[1];
        audiosource.Play();*/
        yield return new WaitUntil(() => !Speaker.isPlaying);
        Speaker.clip = clips[2];
        Speaker.Play();
        yield return new WaitUntil(() => !Speaker.isPlaying);
        Speaker.clip = clips[3];
        Speaker.Play();
        yield return new WaitUntil(() => !Speaker.isPlaying);
        if(isServer)
        { 
            operating = false;
        }
        /* if(!InsideShelter)
         TC.ChangeJingleVolume(1);*/
    }

    public override void Reset()
    {
        if (ResponseAvailable && Speaker != null)
            Speaker.Stop();
        ItemActive = false;
        if (GameManager.Instance.TCN != null)
        {
            //GameManager.Instance.TCN.GPN.AlarmTriggered = false;
            //GameManager.Instance.TCN.GPN.SOSRequested = false;
        }
    }

}
