﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class StopAvailableNet : NetworkBehaviour
{

    private void Start()
    {
        if (!isServer)
            gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.parent != null)
        {
            var cn = other.transform.parent.GetComponentInParent<CarMovementScriptNet>();
            if (cn != null)
            {
                cn.SetCanStop(true);
                gameObject.SetActive(false);
            }
        }
    }
}
