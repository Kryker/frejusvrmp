﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//[RequireComponent(typeof(Collider))]
public class SicuraExtNet : GenericItemNet
{
    public AudioSource audiosource;
    GrabbedExtinguisherNet GrabbedExt;
    Vector3 InitialPos, InitialRot;
    bool up = true;
    float t = 0; 
    bool PulseEnabled;
    MeshRenderer MR = null;
    public Shader Outline;
    Material outlinedbutton = null;
    Material oldmat = null;
    Coroutine resetandhide;

    public override void Start () {
        ItemCode = ItemCodes.ExtSecure;
        audiosource = Slave.GetComponent<AudioSource>();
        GrabbedExt = GetComponent<GrabbedExtinguisherNet>();
        MR = Slave.GetComponent<MeshRenderer>();
        oldmat = MR.materials[0];
        InitialPos = Slave.transform.localPosition;
        InitialRot = Slave.transform.localEulerAngles;
        outlinedbutton = new Material(Outline);
        outlinedbutton.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
        outlinedbutton.SetColor("_OutlineColor", oldmat.GetColor("_OutlineColor"));
        outlinedbutton.SetFloat("_Outline", oldmat.GetFloat("_Outline"));
        base.Start();
    }

    public void Hide()
    {
        StopPulse();
        MR.enabled = false;
        Slave.GetComponent<Collider>().enabled = false;
        audiosource.enabled = false;
    }
    public void Show()
    {
        MR.enabled = true;
        Slave.GetComponent<Collider>().enabled = true;
        audiosource.enabled = true;
    }

    public override void Interact(GenericItemSlave slave, ItemControllerNet cn, ControllerHand hand)
    {
        if (GrabbedExt.Player != null && GrabbedExt.Player == cn)
            Interact(cn, hand);
    }
    public override void ClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            GrabbedExt.RimuoviSicura();
        }
    }
    public override bool CanInteract(ItemControllerNet cn)
    {
        if (GrabbedExt.Player != null && GrabbedExt.Player == cn)
            return base.CanInteract(cn);
        else
            return false;
    }
    public override void Reset()
    {
        base.Reset();
        Show();
        Slave.GetComponent<Collider>().enabled = false;
    }
    public override void EnableOutline(ItemControllerNet c)
    {
        StopPulse();
        base.EnableOutline(c);
    }

    public override void DisableOutline(ItemControllerNet c)
    {
        base.DisableOutline(c);
        if(CanInteract(c))
            StartPulse();
    }

    public override void Update()
    {
        Pulse();
    }

    public void RimuoviSicura()
    {
        StopPulse();
        if (isServer)
        {
            SetCanInteract(false);
            ItemActive = false;
        }
        Slave.GetComponent<Rigidbody>().isKinematic = false;
        resetandhide = StartCoroutine(ResetAndHide());
    }

    internal void PlaySound()
    {
        audiosource.Play();
    }

    private IEnumerator ResetAndHide()
    {
        StopPulse();
        yield return new WaitForSeconds(1.5f);
        Hide();
        Slave.GetComponent<Rigidbody>().isKinematic = true;
        Slave.transform.localPosition = InitialPos;
        Slave.transform.localRotation = Quaternion.Euler(InitialRot);
        resetandhide = null;
    }

    public void MettiSicura()
    {
        if (isServer)
        {
            SetCanInteract(true);
            ItemActive = true;
            
        }

        if (resetandhide != null)
            StopCoroutine(resetandhide);
        Slave.GetComponent<Rigidbody>().isKinematic = true;
        Slave.transform.localPosition = InitialPos;
        Slave.transform.localRotation = Quaternion.Euler(InitialRot);
        Show();
        StartPulse();
    }

    public void StartPulse()
    {
        if (!PulseEnabled)
        {
            var m = new Material[1];
            m[0] = outlinedbutton;
            MR.materials = m;
            PulseEnabled = true;
        }
    }
    public void StopPulse()
    {
        var m = new Material[1];
        m[0] = oldmat;

        MR.materials = m;
        PulseEnabled = false;
    }

    void Pulse()
    {
        if (PulseEnabled)
        {         
                if (up)
                {
                    t += Time.deltaTime / 0.5f;
                    var m1 = MR.materials[0];
                    var c = m1.GetColor("_OutlineColor");
                    m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                    var m = new Material[1];
                    m[0] = m1;
                    MR.materials = m;
                    if (t >= 1)
                    {
                        up = false;
                        t = 0;
                    }
                }
                else
                {
                    t += Time.deltaTime / 0.5f;
                    var m1 = MR.materials[0];
                    var c = m1.GetColor("_OutlineColor");
                    m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                    var m = new Material[1];
                    m[0] = m1;
                    MR.materials = m;
                    if (t >= 1)
                    {
                        up = true;
                        t = 0;
                    }
                }
            }
        }
}
