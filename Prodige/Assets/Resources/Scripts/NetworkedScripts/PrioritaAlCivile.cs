﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class PrioritaAlCivile : NetworkBehaviour
{
    public CinturaNet cintura;
    public CinturaNet cinturaOK;
    public void Start()
    {
        if (isServer)
        {
            cintura.cinturaTagliata += cinturaTagliata;
            cinturaOK.cinturaTagliata += cinturaTagliata;

        }else
        { this.GetComponent<BoxCollider>().enabled = false; }
    }

    

    public void cinturaTagliata()
    {
        this.GetComponent<BoxCollider>().enabled = false;
    }

    public void OnTriggerEnter(Collider c)
    {
        var Player = c.gameObject.GetComponent<PlayerStatusNet>();
        if (Player != null && Player.Ruolo == SpawnMode.Componente)
        {
            this.GetComponent<BoxCollider>().enabled = false;
            if(isServer)
            {
                GameManager.Instance.TCN.GPN.SetPrioritaAlCivile(false, SpawnMode.Componente);
                GameManager.Instance.TCN.GPN.SetMantenuteDistanzeSicurezzaIcendio(false, SpawnMode.Componente);
            }
            else
            {
                GameManager.Instance.TCN.GPN.CmdSetPrioritaAlCivile(false, SpawnMode.Componente);
                GameManager.Instance.TCN.GPN.CmdSetMantenuteDistanzeSicurezzaIcendio(false, SpawnMode.Componente);
            }
            
        }
    }
}
