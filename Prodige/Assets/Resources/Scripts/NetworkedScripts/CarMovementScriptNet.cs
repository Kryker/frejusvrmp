﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CarMovementScriptNet : NetworkBehaviour
{
    public float MotorForce = 600;
    public float BrakeForce = 60000;
    [HideInInspector]
    public Rigidbody body;
    public WheelCollider WheelColFR;
    public WheelCollider WheelColFL;
    public WheelCollider WheelColCR;
    public WheelCollider WheelColCL;
    public WheelCollider WheelColRR;
    public WheelCollider WheelColRL;
    bool braking = false;
    [SyncVar]
    public float MaxSpeed = 70;
    [HideInInspector]
    [SyncVar]
    public bool forcebrake;
    [SyncVar]
    public bool CanBrake;
    [SyncVar]
    public bool CanStop;
    public EngineType Type = EngineType.Rear;
    public delegate void MyDelegate();
    public MyDelegate BrakeEnabled, Braking, StopBraking, BrakeDisabled, CarStopped, StopEnabled, StopDisabled;
    public Transform Wheels;
    public AudioSource _carMoving;
    public AudioClip moving, brake, firstbrake;
    private bool forcebrakeplayed = false;
    VehicleDataNet Vehicle;
    [HideInInspector]
    [SyncVar(hook = "OnStoppedChanged")]
    public bool stopped;
    private bool brakeplayed = false;
    public bool CanMoveFromStart;
    public bool CanBrakeFromStart = true;
    [HideInInspector]
    [SyncVar(hook = "OnCanMoveChanged")]
    public bool CanMove;
    [SyncVar]
    bool brakerequest;
    [SyncVar]
    bool acceleraterequest;
    bool initialized = false;
    [SyncVar]
    float currvelocity;

    public enum Steering { Left, Right, No };


    // Use this for initialization
    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        Vehicle = GetComponent<VehicleDataNet>();
    }
    public void OnCanMoveChanged(bool value)
    {
        var r = GetComponent<Rigidbody>();
        if (value)
        {
            if (hasAuthority)
                r.isKinematic = false;
            else
                r.isKinematic = true;
        }
        CanMove = value;
    }
    void Start()
    {
        if (isServer)
        {
            forcebrake = false;
            CanStop = false;
        }
        Initialize();
    }


    public void Initialize()
    {
        if (!initialized)
        {
            if (isServer)
            {
                acceleraterequest = false;
                brakerequest = false;
                if (isClient)
                {
                    stopped = false;
                    CanMove = CanMoveFromStart;
                }
                else
                {
                    OnStoppedChanged(false);
                    OnCanMoveChanged(CanMoveFromStart);
                }
                if (CanBrakeFromStart)
                    SetCanBrake(true);
            }
            else
            {
                OnStoppedChanged(stopped);
                OnCanMoveChanged(CanMove);
            }
            initialized = true;
        }
    }
    public void StopEngine()
    {
        if (isServer)
        {
            if (!stopped)
            {
                if (isClient)
                    stopped = true;
                else
                    OnStoppedChanged(true);
                CanMove = false;
                currvelocity = 0.0f;
            }
        }
    }

    public void StartEngine()
    {
        if (isServer)
        {
            if (stopped)
            {
                if (isClient)
                    stopped = false;
                else
                    OnStoppedChanged(false);
            }
        }
    }
    void OnStoppedChanged(bool state)
    {
        if (state)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            //GetComponent<CustomNetworkTransform>().enabled = false;
        }
        else
        {

            GetComponent<Rigidbody>().isKinematic = false;
            //GetComponent<CustomNetworkTransform>().enabled = true;
        }
        stopped = state;
    }

    internal void Reset()
    {
        if (isServer)
        {
            if (!isClient)
            {
                braking = false;
                forcebrakeplayed = false;
                brakeplayed = false;
                body.velocity = Vector3.zero;
                if (WheelColFR != null)
                {
                    WheelColFR.motorTorque = 0;
                }

                if (WheelColFL != null)
                {
                    WheelColFL.motorTorque = 0;
                }

                if (WheelColRR != null)
                {
                    WheelColRR.motorTorque = 0;
                    WheelColRR.brakeTorque = 0;
                }

                if (WheelColRL != null)
                {
                    WheelColRL.motorTorque = 0;
                    WheelColRL.brakeTorque = 0;
                }

            }
            forcebrake = false;
            CanStop = false;
            brakerequest = false;
            acceleraterequest = false;

            if (isClient)
            {
                stopped = false;
                CanMove = CanMoveFromStart;
            }
            else
            {
                OnStoppedChanged(false);
                OnCanMoveChanged(CanMoveFromStart);
            }
            if (CanBrakeFromStart)
                SetCanBrake(true);
            else
                SetCanBrake(false);

            RpcReset();
        }
    }

    private void RpcReset()
    {
        braking = false;
        forcebrakeplayed = false;
        brakeplayed = false;
        body.velocity = Vector3.zero;
        if (WheelColFR != null)
        {
            WheelColFR.motorTorque = 0;
        }

        if (WheelColFL != null)
        {
            WheelColFL.motorTorque = 0;
        }

        if (WheelColRR != null)
        {
            WheelColRR.motorTorque = 0;
            WheelColRR.brakeTorque = 0;
        }

        if (WheelColRL != null)
        {
            WheelColRL.motorTorque = 0;
            WheelColRL.brakeTorque = 0;
        }

    }

    public float GetVelocity()
    {
        return currvelocity;
    }

    public bool IsBrakeRequested()
    {
        return brakerequest;
    }
    public bool IsAcclerateRequested()
    {
        return acceleraterequest;
    }
    public void RequestAccelerate()
    {
        if (hasAuthority)
        {
            if (isServer)
                acceleraterequest = true;
            else
                CmdRequestAccelerate();
        }
    }
    [Command]
    private void CmdRequestAccelerate()
    {
        acceleraterequest = true;
    }

    public void RequestBrake()
    {
        if (hasAuthority)
        {
            if (isServer)
                brakerequest = true;
            else
                CmdRequestBrake();
        }
    }
    [Command]
    private void CmdRequestBrake()
    {
        brakerequest = true;
    }

    public void RequestStopAccelerate()
    {
        if (hasAuthority)
        {
            if (isServer)
                acceleraterequest = false;
            else
                CmdRequestStopAccelerate();
        }
    }
    [Command]
    private void CmdRequestStopAccelerate()
    {
        acceleraterequest = false;
    }

    public void RequestStopBrake()
    {
        if (hasAuthority)
        {
            if (isServer)
                brakerequest = false;
            else
                CmdRequestStopBrake();
        }
    }
    [Command]
    private void CmdRequestStopBrake()
    {
        brakerequest = false;
    }
    bool doneOnce = false;
    void Update()
    {
        if (!doneOnce && CanStop && ((CanBrake && brakerequest || forcebrake) && currvelocity <= 0.0005f && CarStopped != null)) //Era 1.5f
        {
            doneOnce = true;
            //Debug.LogError(this.gameObject.name + " Ha chiamato Invoke. Mezzo fermato.");
            CarStopped.Invoke();
        }

        if (stopped || !CanMove)
            return;

        if (hasAuthority)
        {
            if (isServer)
                currvelocity = body.velocity.magnitude;
            else if (hasAuthority)
                CmdSetVelocity(body.velocity.magnitude);
        }
        if ((brakerequest && CanBrake) || forcebrake) //se sto frenando e posso frenare, o sono costretto a frenare
        {
            if (Vehicle.Accelerating)
                Vehicle.Accelerating = false;
            if (!braking)
            {
                braking = true;
                if (!forcebrake && !brakeplayed)
                {
                    braking = true;
                    if (!forcebrake && !brakeplayed)
                    {
                        _carMoving.Stop();
                        _carMoving.clip = firstbrake;
                        _carMoving.volume = 0.15f;
                        _carMoving.loop = false;
                        _carMoving.Play();
                        brakeplayed = true;
                        }
                    else
                    _carMoving.Play();
                    if (Braking != null)
                        Braking.Invoke();
                }
                if ((forcebrake && currvelocity * 3.6 >= MaxSpeed * 0.75f) && !forcebrakeplayed)
                    {
                        _carMoving.Stop();
                        _carMoving.clip = brake;
                        _carMoving.volume = 0.5f;
                        _carMoving.loop = false;
                        _carMoving.Play();
                        forcebrakeplayed = true;
                }
                
//>>>>>>> origin/master
            }
            if ((forcebrake && currvelocity * 3.6 >= MaxSpeed * 0.75f) && !forcebrakeplayed)
            {
                if (!Vehicle.Accelerating)
                    Vehicle.Accelerating = true;
                if (braking)
                {
                    braking = false;
                    brakeplayed = false;
                    _carMoving.Stop();
                    _carMoving.loop = true;
                    _carMoving.volume = 1;
                    _carMoving.clip = moving;
                    _carMoving.Play();

                    if (StopBraking != null)
                            StopBraking.Invoke();
                }
                if (!_carMoving.isPlaying)
                    _carMoving.Play();
                
            }

        }
        else if (acceleraterequest && (!brakerequest || !CanBrake) && currvelocity * 3.6 <= MaxSpeed)      //se non sto frenando o non posso frenare e non sono troppo veloce
        {
            if (!Vehicle.Accelerating)
                Vehicle.Accelerating = true;
            if (braking)
            {
                braking = false;
                brakeplayed = false;

                if (StopBraking != null)
                    StopBraking.Invoke();

                if (Vehicle.Accelerating)
                    Vehicle.Accelerating = false;
                if (!_carMoving.isPlaying)
                    _carMoving.Play();               
            }
           
        }
        else     //altrimenti
        {
            if (Vehicle.Accelerating)
                Vehicle.Accelerating = false;
            if (!_carMoving.isPlaying)
                _carMoving.Play();
        }

       
            
    }

    [Command]
    private void CmdSetVelocity(float v)
    {
        currvelocity = v;
    }


    public void SetCanBrake(bool can)
    {
        if (isServer && can != CanBrake)
        {
            CanBrake = can;
            if (can && BrakeEnabled != null)
                BrakeEnabled.Invoke();
            else if (!can && BrakeDisabled != null)
                BrakeDisabled.Invoke();
        }
    }

    public void SetCanStop(bool can)
    {
        if (isServer && can != CanStop)
        {
            CanStop = can;
            if (can && StopEnabled != null)
                StopEnabled.Invoke();
            else if (!can && StopDisabled != null)
                StopDisabled.Invoke();
        }
    }


    public void ForceBrake()
    {
        if (isServer)
        {
            SetCanStop(true);
            forcebrake = true;
        }
    }
}
