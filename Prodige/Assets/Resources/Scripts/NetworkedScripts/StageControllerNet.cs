﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum Stage { Start, InCar, InTunnel, End };

public class StageControllerNet : NetworkBehaviour
{
    [HideInInspector]
    [SyncVar]
    public Stage CurrStage;
    [SyncVar]
    bool _ShelterReached;
    [SyncVar]
    float ShelterReachedTime;
    [HideInInspector]
    [SyncVar]
    public bool CarLeft;
    public PlayerStatusNet PlayerStatus;
    float EndTime = float.MinValue;
    public float StartTime = float.MinValue;
    bool sendresult = false;
    bool waitingforexit = false;

    public bool ShelterReached
    {
        get { return _ShelterReached; }
        set
        {
            if (!_ShelterReached && value)
            {
                ShelterReachedTime = Time.time;
            }
            _ShelterReached = value;
        }
    }

    Coroutine playerendgameroutine;
    public class GameResult
    {
        public class Risultati
        {
            public SpawnMode Ruolo;
            //VF
            public bool PrioritaAlCivile = true;
            public bool MantenuteDistanzeSicurezzaIcendio = true;
            public bool IncendioSpentoEntro5Min = true;
            public bool RitornoAlCamion = false;
            public bool CivileSoccorso = false;
            public bool TaglioCinturaOk = false;
            public bool AttaccoIncendioBasso = false;
            //truck
            public bool EstintoreUsato = false;
            //civile
            public Stage StageReached = Stage.Start;
            public bool ArrowsOn = false;
            public float ArrowsOnTime = 0.0f;
            public bool EngineOff = false;
            public float EngineOffTime = 0.0f;
            public bool SOSRequested = false;
            public float SOSRequestedTime = 0.0f;
            public bool AlarmTriggered = false;
            public float AlarmTriggeredTime = 0.0f;
            public bool ShelterReached = false;
            public float ShelterReachedTime = 0.0f;
            public bool DistanceMaintained = false;
            public bool CamionistaSaved = false;
            public bool CarLeft;
            public float Time;
            public bool NoCamionista = false;
            public bool CorpoVisibile = false;
        };
        public Risultati risultati = new Risultati();

        public GameResult(StageControllerNet s)
        {
            if (s == null)
                return;
            risultati.Ruolo = GameManager.Instance.TCN.GetPlayerController().Ruolo;
            risultati.NoCamionista = GameManager.Instance.NoCamionistaNPC;
            risultati.CorpoVisibile = GameManager.Instance.CorpoVisibile;

            GestorePunteggiNet.Punteggi p = GameManager.Instance.TCN.GPN.OttieniPunteggi(risultati.Ruolo);

            risultati.PrioritaAlCivile = p.PrioritaAlCivile;
            risultati.MantenuteDistanzeSicurezzaIcendio = p.MantenuteDistanzeSicurezzaIcendio;
            risultati.IncendioSpentoEntro5Min = p.IncendioSpentoEntro5Min;
            risultati.RitornoAlCamion = p.RitornoAlCamion;
            risultati.CivileSoccorso = p.CivileSoccorso;
            risultati.TaglioCinturaOk = p.TaglioCinturaOk;
            risultati.AttaccoIncendioBasso = p.AttaccoIncendioBasso;

            risultati.EstintoreUsato = p.estintoreUsato; 
            risultati.StageReached = s.CurrStage;
            risultati.ArrowsOn = p._arrowsOn;
            risultati.ArrowsOnTime = p.ArrowsOnTime;
            risultati.EngineOff = p._engineOff;
            risultati.EngineOffTime = p.EngineOffTime;
            risultati.SOSRequested = p._SOSRequested;
            risultati.SOSRequestedTime = p.SOSRequestedTime ;
            risultati.AlarmTriggered = p._AlarmTriggered;
            risultati.AlarmTriggeredTime = p.AlarmTriggeredTime;
            risultati.ShelterReached = s.ShelterReached;
            risultati.ShelterReachedTime = s.ShelterReachedTime - s.StartTime;
            risultati.DistanceMaintained = p.DistanceMaintained;
            risultati.Time = s.EndTime - s.StartTime;
        }
        public override string ToString()
        {
            string s = "";
            if (risultati.DistanceMaintained)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.ArrowsOn)
                s += "1 " + MainMenu.FloatTimeToString(risultati.ArrowsOnTime) + " ";
            else
                s += "0 0 ";

            if (risultati.EngineOff)
                s += "1 " + MainMenu.FloatTimeToString(risultati.EngineOffTime) + " ";
            else
                s += "0 0 ";

            if (risultati.AlarmTriggered)
                s += "1 " + MainMenu.FloatTimeToString(risultati.AlarmTriggeredTime) + " ";
            else
                s += "0 0 ";

            if (risultati.SOSRequested)
                s += "1 " + MainMenu.FloatTimeToString(risultati.SOSRequestedTime) + " ";
            else
                s += "0 0 ";

            if (risultati.ShelterReached)
                s += "1 " + MainMenu.FloatTimeToString(risultati.ShelterReachedTime) + " ";
            else
                s += "0 0 ";

            if (risultati.EstintoreUsato)
                s += "1 ";
            else
                s += "0 ";

           //s += "";
            switch (risultati.Ruolo)
            {
                case SpawnMode.Componente:
                    s += "0";
                    break;
                case SpawnMode.Driver:
                    s += "1";
                    break;
                case SpawnMode.Passenger:
                    s += "2";
                    break;
                case SpawnMode.TruckDriver:
                    s += "3";
                    break;

            }
            s += " ";
            if (risultati.PrioritaAlCivile)
                s += "1 ";
            else
                s += "0 ";
            if (risultati.MantenuteDistanzeSicurezzaIcendio)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.IncendioSpentoEntro5Min)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.RitornoAlCamion)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.CivileSoccorso)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.TaglioCinturaOk)
                s += "1 ";
            else
                s += "0 ";
        
            if (risultati.AttaccoIncendioBasso)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.NoCamionista)
                s += "1 ";
            else
                s += "0 ";

            if (risultati.CorpoVisibile)
                s += "1 ";
            else
                s += "0 ";





            return s += risultati.Time.ToString();
        }
    }
    
    public void EnterShelter()
    {
        if (isServer)
        {
            GoInsideShelter();
            GameManager.Instance.TCN.EndGame();
        }
    }
   
    public void ExitShelter()
    {
        if (isServer)
        {
            GoOutsideShelter();
        }
    }
  

    // Use this for initialization
    void Start()
    {
        if (isLocalPlayer)
            sendresult = true;
    }

    private void OnDestroy()
    {
        if (sendresult)
        {
            if (EndTime == float.MinValue)
                EndTime = Time.time;
            if (GameManager.Instance != null && GameManager.Instance.ResultNet == null) 
                GameManager.Instance.ResultNet = new GameResult(this);
        }
    }

    private void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        if (isServer)
            Debug.Log("Local server connection disconnected");
        else
            if (info == NetworkDisconnection.LostConnection)
                Debug.Log("Lost connection to the server");
            else
                Debug.Log("Successfully diconnected from the server");
    }

    // Update is called once per frame
    void Update()
    {
        if(isServer && waitingforexit)
        {
            if (GameManager.Instance.TCN.GetPlayers().Count == 1)
            { 
                PlayerStatus.EnableListening();
                ExitScene();
            }
        }
    }

    public void GoOutOfCar()
    {
        if (isServer)
        {
            CarLeft = true;
            CurrStage = Stage.InTunnel;
        }
        else
            CmdGetOutOfCar();
    }
    [Command]
    private void CmdGetOutOfCar()
    {
        CarLeft = true;
        CurrStage = Stage.InTunnel;
    }

    public void GoInsideCar()
    {
        if (isServer)
        {
            CarLeft = false;
            CurrStage = Stage.InCar;
        }
        else
            CmdGoInsideCar();
    }
    [Command]
    private void CmdGoInsideCar()
    {
        CarLeft = false;
        CurrStage = Stage.InCar;
    }
    

    IEnumerator EndGameRoutine()
    {
        PlayerStatus.ItemController.ForceDrop();
        if (EndTime == float.MinValue)
            EndTime = Time.time;
        PlayerStatus.PlayerCameraFadeOut(2);
        yield return new WaitForSeconds(2);

        if (GameManager.Instance != null)
            GameManager.Instance.ResultNet = new GameResult(this);
        if (!isServer || GameManager.Instance.TCN.GetPlayers().Count == 1)
            ExitScene();
        else
        {
            PlayerStatus.DisableListening();
            WaitForOtherPlayer();
        }
    }

    private void WaitForOtherPlayer()
    {
        if (!waitingforexit)
        {
            if (PlayerStatus.IsInVehicle())
                {
                PlayerStatus.OnVehicleLeft += StartWaiting;
                PlayerStatus.GetOutFromVehicle();
                }
            else
                StartWaiting(this);
        }
    }

    private void StartWaiting(object sender)
    {
        var s = GameObject.FindObjectOfType<NetworkStartPosition>();
        PlayerStatus.ItemController.SetBrochureAvailable(false);
        PlayerStatus.Heal();
        if (GameManager.Instance.Platform == ControllerType.MouseAndKeyboard)
        {
            PlayerStatus.Spawner.Listener.enabled = false;
            PlayerStatus.Spawner.CharacterMotor.enabled = false;
        }
        else
            { 
            PlayerStatus.Spawner.CameraEar.enabled = false;
            PlayerStatus.Spawner.SteamVREars.enabled = false;
            }

        PlayerStatus.transform.position = s.transform.position;
        PlayerStatus.transform.rotation = s.transform.rotation;
        GameManager.Instance.TCN.Spawn.SpawnMenu.DisableElements();
        GameManager.Instance.TCN.Spawn.ExitMenu.EnableElements();
        PlayerStatus.PlayerCameraFadeIn(0);
        waitingforexit = true;
    }

    void ExitScene()
    {
        StartCoroutine(ExitFade());
    }

    IEnumerator ExitFade()
    {
        PlayerStatus.PlayerCameraFadeOut(0.5f);
        yield return new WaitForSeconds(0.5f);
        if (isServer && isClient)
            GameManager.Instance.NetworkManager.StopHost();
        else
            GameManager.Instance.NetworkManager.StopClient();
        //GlobalControl.Instance.TCN.LoadMainMenu();
    }
    

    void GoInsideShelter()
    {
        if (isServer)
            CurrStage = Stage.End;   
    }

    void GoOutsideShelter()
    {
        if (isServer)
            CurrStage = Stage.InTunnel;
    }
    
    internal void EndGame()
    {
        if (isServer)
        {
            if (isLocalPlayer)
                EndPlayerGame();
            else
                RpcEngGame();
            //GlobalControl.Instance.TCN.EndGame();
        }
    }
    internal void EndPlayerGame()
    {
        if (isLocalPlayer)
        {
            EndTime = Time.time;
            playerendgameroutine = StartCoroutine(EndGameRoutine());
        }
    }

    [ClientRpc]
    private void RpcEngGame()
    {
        if (isLocalPlayer)
            EndPlayerGame();
    }

    internal void StartGame()
    {
        if (isLocalPlayer)
            StartTime = Time.time;
    }
}
