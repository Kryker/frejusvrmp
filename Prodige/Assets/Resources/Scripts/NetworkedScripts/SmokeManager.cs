﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SmokeManager : NetworkBehaviour {
    [SyncVar]
    float time;
    ParticleSystem p;
    bool tosync = false;
	// Use this for initialization
	void Awake () {
    }
    private void Start()
    {
        if (p == null)
            p = GetComponent<ParticleSystem>();
        if (!isServer)
        {
            var c = p.collision;
            c.sendCollisionMessages = false;
        }
        else
            tosync = true;
    }
    [Command]
    private void CmdSyncTime(NetworkInstanceId id)
    {
        if (p == null)
            p = GetComponent<ParticleSystem>();
        time = p.time;
        RpcSyncTime(id);
    }

    [ClientRpc]
    private void RpcSyncTime(NetworkInstanceId id)
    {
        if (p == null)
            p = GetComponent<ParticleSystem>();
        if (GameManager.Instance.TCN.GetPlayerController().netId == id)
            p.time = time;
    }

    // Update is called once per frame
    void Update () {
        if (!isServer && tosync && GameManager.Instance.TCN.GetPlayerController() != null)
        {
            CmdSyncTime(GameManager.Instance.TCN.GetPlayerController().netId);
            tosync = false;
        }
    }
}
