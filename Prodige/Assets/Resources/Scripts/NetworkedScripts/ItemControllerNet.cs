﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[NetworkSettings(channel = 0, sendInterval = 0.011f)]
public class ItemControllerNet : NetworkBehaviour
{
    public ItemControllerType Type;
    public Transform VRLeftController, VRRightController, FPSLeftController, FPSRightController;

    [HideInInspector]
    [SyncVar(hook = "OnLeftBrochureChanged")]
    public bool LeftBrochureEnabled;
    [HideInInspector]
    [SyncVar(hook = "OnRightBrochureChanged")]
    public bool RightBrochureEnabled;
    [HideInInspector]
    public Transform ItemLeft, ItemRight;
    [HideInInspector]
    public Transform[] ItemsLeft;
    [HideInInspector]
    public Transform[] ItemsRight;
    [HideInInspector]
    public int ItemRightIndex;
    [HideInInspector]
    [SyncVar (hook ="OnLeftItemIDChanged")]
    public NetworkInstanceId ItemLeftID = NetworkInstanceId.Invalid;
    [HideInInspector]
    [SyncVar (hook ="OnRightItemIDChanged")]
    public NetworkInstanceId ItemRightID = NetworkInstanceId.Invalid;
    [HideInInspector]
    public int ItemLeftIndex;
    [HideInInspector]
    public AudioSource LeftItemSource, RightItemSource;
    [HideInInspector]
    [SyncVar(hook = "OnBrochureAvailableChanged")]
    public bool BrochureAvailable;
    public bool InitialBrochureAvailable = true;
    public bool ShowVREquipment = false;
    public AudioClip DefaultGrabSound, DefaultDropSound;
    public float DefaultGrabSoundVolume = 1, DefaultDropSoundVolume = 1;
    [HideInInspector]
    public ItemNumbering ItemIDs;
    [HideInInspector]
    public bool ItemsToSpawn = false;
    [HideInInspector]
    public InputManagementNet InputManager;
    public bool SpawnItems = true;
    bool grableftlater, grabrightlater;
    [HideInInspector]
    public bool[] ItemsLeftReady, ItemsRightReady;
    [Header("FPS ItemController")]
    public Transform FPSCamera;
    Transform offlineraycasteditem;
    public Transform raycasteditem, toucheditemleft, toucheditemright;
    [SyncVar(hook = "OnRayCastedItemIDChanged")]
    NetworkInstanceId raycasteditemnetid = NetworkInstanceId.Invalid;
    [SyncVar(hook = "OnTouchedItemLeftIDChanged")]
    NetworkInstanceId toucheditemleftnetid = NetworkInstanceId.Invalid;
    [SyncVar(hook = "OnTouchedItemRightIDChanged")]
    NetworkInstanceId toucheditemrightnetid = NetworkInstanceId.Invalid;
    [SyncVar]
    uint raycasteditemlocid;
    [SyncVar]
    uint toucheditemleftlocid;
    [SyncVar]
    uint toucheditemrightlocid;
    bool raycastednpc = false;
    public bool UIElement = false;
    public Image Crosshair;
    public Sprite BaseCrosshair;
    public Sprite CyanCrosshair;
    public Sprite MagentaCrosshair;
    public Sprite YellowCrosshair;
    public Sprite RainbowCrosshair;
    public float RaycastMaxDistance = 2;
    [HideInInspector]
    public Transform forcedtarget = null;
    public enum CrosshairState { None, Default, Cyan, Magenta, Yellow, Rainbow };
    CrosshairState CurrentCrosshair = CrosshairState.Default;
    CrosshairState ForcedCrosshair = CrosshairState.None;
    public RayCastUtil RayCaster;
    public bool DeferredRaycast = false;
    [Header("VR ItemController")]
    Material oldmat;
    public Shader Outline;
    float t;
    bool up;
    [SyncVar]
    ControllerButton LeftButtonToPulse;
    [SyncVar]
    ControllerButton RightButtonToPulse;
    [SyncVar]
    ControllerButton ExternalLeftButtonToPulse;
    [SyncVar]
    ControllerButton ExternalRightButtonToPulse;
    public ControllerButtonInput ButtonToInteract = ControllerButtonInput.Trigger;
    public ControllerButtonInput ButtonToDrop = ControllerButtonInput.Trigger;
    public ControllerButtonInput ButtonToUse = ControllerButtonInput.Pad;
    public GrabMode GrabbingMethod = GrabMode.ClickButton;
    List<MeshRenderer> LGrips = new List<MeshRenderer>();
    List<MeshRenderer> LPads = new List<MeshRenderer>();
    List<MeshRenderer> LTriggers = new List<MeshRenderer>();
    List<MeshRenderer> RGrips = new List<MeshRenderer>();
    List<MeshRenderer> RPads = new List<MeshRenderer>();
    List<MeshRenderer> RTriggers = new List<MeshRenderer>();
    Material PadMaterial, GripMaterial, TriggerMaterial;
    [SyncVar(hook = "OnLeftPulsingChanged")]
    bool leftpulsing;
    [SyncVar(hook = "OnRightPulsingChanged")]
    bool rightpulsing;
    public float PulseTime = 0.5f;
    public Color PadColor = Color.yellow;
    public Color TriggerColor = Color.magenta;
    public Color GripColor = Color.cyan;
    [HideInInspector]
    public VibrationController LeftVibrationController, RightVibrationController;
    bool leftoperating, rightoperating;
    bool LeftInteracting, RightInteracting = false;
    public Transform FakeLeftHand, FakeRightHand;		
    public NetworkRun Run;
    GenericItemNet itemlefttograb, itemrighttograb;
    ItemControllerNet otherplayer;
    ControllerHand otherplayerhand;
    public event Action<object> OnLeftInteractionFinished, OnRightInteractionFinished;
    [HideInInspector]
    public Transform LeftController
    {
        get { if (Type == ItemControllerType.FPS)
                return FPSLeftController;
            else if (Type == ItemControllerType.VR)
                return VRLeftController;
            else
                return null;
        }
        set {
            if (Type == ItemControllerType.FPS)
                FPSLeftController = value;
            else if (Type == ItemControllerType.VR)
                VRLeftController = value;
        }
    }
    [HideInInspector]
    public Transform RightController
    {
        get
        {
            if (Type == ItemControllerType.FPS)
                return FPSRightController;
            else if (Type == ItemControllerType.VR)
                return VRRightController;
            else
                return null;
        }
        set
        {
            if (Type == ItemControllerType.FPS)
                FPSRightController = value;
            else if (Type == ItemControllerType.VR)
                VRRightController = value;
        }
    }
  

    bool AreItemsLeftReady()
    {
        if (ItemsLeft != null)
        {
            foreach (Transform t in ItemsLeft)
                if (t == null)
                    return false;
            return true;
        }
        else
            return false;
    }
    bool AreItemsRightReady()
    {
        if (ItemsRight != null)
        {
            foreach (Transform t in ItemsRight)
                if (t == null)
                    return false;
            return true;
        }
        else
            return false;
    }


    void Start()
    {
        RightItemSource = RightController.GetComponent<AudioSource>();
        LeftItemSource = LeftController.GetComponent<AudioSource>();
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
            ItemIDs = tc.GetComponent<ItemNumbering>();
        ItemRightIndex = -1;
        ItemLeftIndex = -1;
        if (isServer)
        {
            if(isClient)
                {
                BrochureAvailable = InitialBrochureAvailable;
                ItemLeftID = NetworkInstanceId.Invalid;
                ItemRightID = NetworkInstanceId.Invalid;
                }
            else
            {
                OnLeftItemIDChanged(NetworkInstanceId.Invalid);
                OnRightItemIDChanged(NetworkInstanceId.Invalid);
                OnBrochureAvailableChanged(InitialBrochureAvailable);
            }
        }
        else
        {
            if (ItemLeftID != NetworkInstanceId.Invalid)
                grableftlater = true;
            if (ItemRightID != NetworkInstanceId.Invalid)
                grabrightlater = true;
        }

        var il = GameObject.FindGameObjectWithTag("ItemList");
        if (il != null)
        {
            var list = il.GetComponent<ItemList>().ItemPrefabs;
            int i = 0;
            ItemsLeft = new Transform[list.Count];
            ItemsRight = new Transform[list.Count];
            ItemsLeftReady = new bool[list.Count];
            ItemsRightReady = new bool[list.Count];
            if (isServer && SpawnItems)
                foreach (Transform t in list)
                {
                    var o = Instantiate(t, t.position, t.rotation, null);
                    NetworkServer.Spawn(o.gameObject);
                    var g = o.GetComponent<GrabbableItemNet>();
                    g.SetUp(this, ControllerHand.LeftHand, i);
                    ItemsLeft[i] = o.transform;
                    ItemsLeftReady[i] = true;

                    o = Instantiate(t, t.position, t.rotation, null);
                    NetworkServer.Spawn(o.gameObject);
                    g = o.GetComponent<GrabbableItemNet>();
                    g.SetUp(this, ControllerHand.RightHand, i);
                    ItemsRight[i] = o.transform;
                    ItemsRightReady[i] = true;

                    i++;
                }
        }

        if (BrochureAvailable && !isServer)
        {
            if (LeftBrochureEnabled)
                OnLeftBrochureChanged(LeftBrochureEnabled);
            if (RightBrochureEnabled)
                OnRightBrochureChanged(RightBrochureEnabled);
        }

        InputManager = GetComponent<InputManagementNet>();
        if (Type == ItemControllerType.VR)
        {
            if (isServer)
            {
                LeftButtonToPulse = ControllerButton.Nothing;
                RightButtonToPulse = ControllerButton.Nothing;
                ExternalRightButtonToPulse = ControllerButton.Nothing;
                ExternalRightButtonToPulse = ControllerButton.Nothing;
                if(isClient)
                {
                    rightpulsing = false;
                    leftpulsing = false;
                }
                else
                {
                    OnLeftPulsingChanged(false);
                    OnRightPulsingChanged(false);
                }

            }
            if (isLocalPlayer)
            {
                LeftVibrationController = LeftController.GetComponent<VibrationController>();
                RightVibrationController = RightController.GetComponent<VibrationController>();
            }
            else if (ShowVREquipment)
            {
                LeftController.Find("Model").Find("ModelFake").gameObject.SetActive(true);
                RightController.Find("Model").Find("ModelFake").gameObject.SetActive(true);
            }

            if (isServer)
            {
                ReSetTouchedItem(ControllerHand.LeftHand, false);
                ReSetTouchedItem(ControllerHand.RightHand, false);
                if (InputManager != null)
                {
                    switch (ButtonToUse)
                    {
                        case ControllerButtonInput.Trigger:
                            InputManager.OnLeftTriggerClicked += ToggleLeftBrochure;
                            InputManager.OnRightTriggerClicked += ToggleRightBrochure;
                            break;
                        case ControllerButtonInput.Grip:
                            InputManager.OnLeftGripped += ToggleLeftBrochure;
                            InputManager.OnRightGripped += ToggleRightBrochure;
                            break;
                        default:
                            InputManager.OnLeftPadPressed += ToggleLeftBrochure;
                            InputManager.OnRightPadPressed += ToggleRightBrochure;
                            break;
                    }
                    switch (ButtonToInteract)
                    {
                        case ControllerButtonInput.Pad:
                            InputManager.OnLeftPadPressed += LeftInteract;
                            InputManager.OnRightPadPressed += RightInteract;
                            break;
                        case ControllerButtonInput.Grip:
                            InputManager.OnLeftGripped += LeftInteract;
                            InputManager.OnRightGripped += RightInteract;
                            break;
                        case ControllerButtonInput.Any:
                            InputManager.OnLeftPadPressed += LeftInteract;
                            InputManager.OnLeftGripped += LeftInteract;
                            InputManager.OnLeftTriggerClicked += LeftInteract;
                            InputManager.OnRightPadPressed += RightInteract;
                            InputManager.OnRightGripped += RightInteract;
                            InputManager.OnRightTriggerClicked += RightInteract;
                            break;
                        default:
                            InputManager.OnLeftTriggerClicked += LeftInteract;
                            InputManager.OnRightTriggerClicked += RightInteract;
                            break;
                    }

                    if (GrabbingMethod == GrabMode.HoldButton)
                    {
                        switch (ButtonToInteract)
                        {
                            case ControllerButtonInput.Pad:
                                InputManager.OnLeftPadUnpressed += LeftDropPressed;
                                InputManager.OnRightPadUnpressed += RightDropPressed;
                                break;
                            case ControllerButtonInput.Grip:
                                InputManager.OnLeftUngripped += LeftDropPressed;
                                InputManager.OnRightUngripped += RightDropPressed;
                                break;
                            default:
                                InputManager.OnLeftTriggerUnclicked += LeftDropPressed;
                                InputManager.OnRightTriggerUnclicked += RightDropPressed;
                                break;
                        }
                    }
                }
            }
            else
            {
                OnTouchedItemLeftIDChanged(toucheditemleftnetid);
                OnTouchedItemRightIDChanged(toucheditemrightnetid);
            }
        }
        else if (Type == ItemControllerType.FPS)
        {
            if (isServer)
            {
                InputManager.OnLeftMouseClicked += ToggleLeftBrochure;
                InputManager.OnRightMouseClicked += ToggleRightBrochure;
                InputManager.OnLeftMouseClicked += LeftInteractPressed;
                InputManager.OnRightMouseClicked += RightInteractPressed;
                InputManager.OnLeftDropPressed += LeftDropPressed;
                InputManager.OnRightDropPressed += RightDropPressed;
                ReSetRayCastedItem();
            }
            else if(isLocalPlayer)
            {
                InputManager.OnLeftMouseClickedOffline += OfflineLeftInteractPressed;
                InputManager.OnRightMouseClickedOffline += OfflineRightInteractPressed;
            }
            else
                OnRayCastedItemIDChanged(raycasteditemnetid);
        }

    }

    public bool ItemsReady()
    {
        if (AreItemsLeftReady() && AreItemsRightReady())
            return true;
        else
            return false;
    }

    void OnLeftPulsingChanged(bool value)
    {
        if (leftpulsing != value)
        {
            if (isLocalPlayer)
            {
                if (value)
                    StartPulse(ControllerHand.LeftHand);
                else
                    StopPulse(ControllerHand.LeftHand);
            }
            leftpulsing = value;
        }
    }
    void OnRightPulsingChanged(bool value)
    {
        if (rightpulsing != value)
        {
            if (isLocalPlayer)
            {
                if (value)
                    StartPulse(ControllerHand.RightHand);
                else
                    StopPulse(ControllerHand.RightHand);
            }
            rightpulsing = value;
        }
    }
    public int GetGrabbableItemByCode(ItemCodes code, ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            for (int i = 0; i < ItemsRight.Length; i++)
            {
                var item = ItemsRight[i].GetComponent<GrabbableItemNet>();
                if (item != null && item.Code == code)
                {
                    return i;
                }
            }
        }
        else if (hand == ControllerHand.LeftHand)
        {
            for (int i = 0; i < ItemsLeft.Length; i++)
            {
                var item = ItemsLeft[i].GetComponent<GrabbableItemNet>();
                if (item != null && item.Code == code)
                {
                    return i;
                }
            }
        }
        return -1;
    }

    private void OnDestroy()
    {
        if (isServer)
        {
            foreach (Transform t in ItemsLeft)
                if(t != null)
                    Destroy(t.gameObject);
            
            foreach (Transform t in ItemsRight)
                if (t != null)
                    Destroy(t.gameObject);
        }
    }

    #region EnableItem
    public GrabbableItemNet EnableItem(ItemCodes code, GenericItemNet g, ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            for (int i = 0; i < ItemsRight.Length; i++)
            {
                var item = ItemsRight[i].GetComponent<GrabbableItemNet>();
                if (item != null && item.Code == code)
                {
                    if (code == ItemCodes.Brochure)
                        return EnableItem(i, null, hand);
                    else if (g.Slave != null)
                        return EnableItem(i, g.Slave.transform, hand);
                    else
                        return EnableItem(i, g.transform, hand);
                }
            }
        }
        else if (hand == ControllerHand.LeftHand)
        {
            for (int i = 0; i < ItemsLeft.Length; i++)
            {
                var item = ItemsLeft[i].GetComponent<GrabbableItemNet>();
                if (item != null && item.Code == code)
                {
                    if (code == ItemCodes.Brochure)
                        return EnableItem(i, null, hand);
                    else if (g.Slave != null)
                        return EnableItem(i, g.Slave.transform, hand);
                    else
                        return EnableItem(i, g.transform, hand);
                }
            }
        }
        return null;
    }
    GrabbableItemNet EnableItem(int i, Transform t, ControllerHand hand)
    {
        GrabbableItemNet gb = null;
        GenericItemNet g = null;
        if (hand == ControllerHand.RightHand)
        {
            if (i < 0 || i > ItemsRight.Length)
                return null;
            ItemRightIndex = i;
            if (t != null)
            {
                g = t.GetComponent<GenericItemNet>();
                if (g != null)
                    ItemRight = g.transform;
                else
                {
                    var gs = t.GetComponent<GenericItemSlave>();
                    if(gs != null)
                        ItemRight = gs.MasterNet.transform;
                }
            }
            else
                ItemRight = null;

            gb = ItemsRight[i].GetComponent<GrabbableItemNet>();

            if (gb != null)
            {
                if (gb.HideController)
                    RightController.Find("Model").gameObject.SetActive(false);
                if (g != null)
                    gb.LoadState(g);
                else
                    gb.LoadState();
            }
            return gb;
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (i < 0 || i > ItemsLeft.Length)
                return null;
            ItemLeftIndex = i;
            if (t != null)
            {
                g = t.GetComponent<GenericItemNet>();
                if (g != null)
                    ItemLeft = g.transform;
                else
                {
                    var gs = t.GetComponent<GenericItemSlave>();
                    if (gs != null)
                        ItemLeft = gs.MasterNet.transform;
                }
            }
            else
                ItemLeft = null;
            gb = ItemsLeft[i].GetComponent<GrabbableItemNet>();

            if (gb != null)
            {
                if (gb.HideController)
                    LeftController.Find("Model").gameObject.SetActive(false);
                if (g != null)
                    gb.LoadState(g);
                else
                    gb.LoadState();
            }
            return gb;
        }
        else
            return null;

    }
    #endregion
    #region DisableItem
    public GrabbableItemNet DisableItem(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex != -1)
            {
                return DisableItem(ItemRightIndex, hand);
            }
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex != -1)
            {
                return DisableItem(ItemLeftIndex, hand);
            }
        }
        return null;

    }
    GrabbableItemNet DisableItem(int i, ControllerHand hand)
    {
        GrabbableItemNet g = null;
        if (hand == ControllerHand.RightHand)
        {
            if (i < 0 || i > ItemsRight.Length || ItemRightIndex == -1)
                return g;
            ItemRightIndex = -1;
            ItemRight = null;
            g = ItemsRight[i].GetComponent<GrabbableItemNet>();
            if (g.HideController)
            {
                RightController.Find("Model").gameObject.SetActive(true);
            }
            g.SaveState();
            return g;
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (i < 0 || i > ItemsLeft.Length || ItemLeftIndex == -1)
                return null;
            ItemLeftIndex = -1;
            ItemLeft = null;
            g = ItemsLeft[i].GetComponent<GrabbableItemNet>();
            if (g.HideController)
            {
                LeftController.Find("Model").gameObject.SetActive(true);
            }
            g.SaveState();
            return g;
        }
        else
            return null;
    }
    #endregion
    #region DropItem
    public void DropItem(ControllerHand hand, bool forced)
    {
        if (isServer)
        {
            if (hand == ControllerHand.RightHand)
            {
                if (ItemRight != null)
                {
                    var i = ItemRight.GetComponent<GenericItemNet>();

                    if (i != null)
                        if (i.ItemCode == ItemCodes.Generic || forced || (i.ItemCode != ItemCodes.Generic && GetCurrentItem(ControllerHand.RightHand).CanDrop()))
                        {
                            if (isClient && !forced)
                                ItemRightID = NetworkInstanceId.Invalid;
                            else
                                OnRightItemIDChanged(NetworkInstanceId.Invalid);
                        }
                }
                else if (RightBrochureEnabled)
                    DisableRightBrochure();
            }
            else if (hand == ControllerHand.LeftHand)
            {
                if (ItemLeft != null)
                {
                    var i = ItemLeft.GetComponent<GenericItemNet>();
                    if (i != null)
                        if (i.ItemCode == ItemCodes.Generic || forced || (i.ItemCode != ItemCodes.Generic && GetCurrentItem(ControllerHand.LeftHand).CanDrop()))
                        {
                            if(isClient && !forced)
                                ItemLeftID = NetworkInstanceId.Invalid;
                            else
                                OnLeftItemIDChanged(NetworkInstanceId.Invalid);
                        }
                }
                else if (LeftBrochureEnabled)
                    DisableLeftBrochure();
            }
        }
    }
    public void DropItem(Transform item, bool forced)
    {
        if (ItemRight == item)
            DropItem(ControllerHand.RightHand, forced);
        else if (ItemLeft == item)
            DropItem(ControllerHand.LeftHand, forced);
    }

    
    public void ForceDrop()
    {
        if (isServer)
        {
            DropItem(ControllerHand.RightHand, true);
            DropItem(ControllerHand.LeftHand, true);
        }
        else
            CmdForceDrop();
    }
    [Command]
    private void CmdForceDrop()
    {
        DropItem(ControllerHand.LeftHand, true);
        DropItem(ControllerHand.RightHand, true);
    }
    #endregion
    #region GrabItem
    public void GrabItem(GenericItemNet i, ControllerHand hand, bool ForceHook)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
                {
                if(isClient && !ForceHook)
                    ItemLeftID = i.netId;
                else
                    OnItemIDChanged(i.netId, hand, true);
                }
            else if (hand == ControllerHand.RightHand)
                {
                if(isClient && !ForceHook)
                    ItemRightID = i.netId;
                else
                    OnItemIDChanged(i.netId, hand, true);
                }
        }
    }
    #endregion
    void UnSelectItem(ControllerHand hand)
    {
        if(Type == ItemControllerType.VR)
        {
            if ((hand == ControllerHand.LeftHand && ItemLeft != null) || (hand == ControllerHand.RightHand && ItemRight != null))
                StopPulse(hand);
        }

        if (hand == ControllerHand.LeftHand && ItemLeft != null)
        {
            if (ItemLeft.tag == "Item")
            {
                GenericItemNet g = ItemLeft.GetComponent<GenericItemNet>();
                if (g != null)
                    g.Unselect();
                
            }
            else if (ItemLeft.tag == "NPC")
            {
                ItemLeft.GetComponentInParent<NPCController>().DisableOutline();
            }
        }
        else if (hand == ControllerHand.RightHand && ItemRight != null)
        {
            if (ItemRight.tag == "Item")
            {
                GenericItemNet g = ItemRight.GetComponent<GenericItemNet>();
                if (g != null)
                    g.Unselect();
            }
            else if (ItemRight.tag == "NPC")
            {
                ItemRight.GetComponentInParent<NPCController>().DisableOutline();
            }
        }
    }
    void OnTouchedItemRightIDChanged(NetworkInstanceId id)
    {
        OnTouchedItemIDChanged(id, ControllerHand.RightHand);
    }
    void OnTouchedItemLeftIDChanged(NetworkInstanceId id)
    {
        OnTouchedItemIDChanged(id, ControllerHand.LeftHand);
    }

    private void OnTouchedItemIDChanged(NetworkInstanceId id, ControllerHand hand)
    {
        if (id == NetworkInstanceId.Invalid)
        {
            if(hand == ControllerHand.LeftHand)
                toucheditemleft = null;
            else if (hand == ControllerHand.RightHand)
                toucheditemright = null;
        }
        else
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(id);
            else
                g = ClientScene.FindLocalObject(id);
            if(g != null)
                { 
                var glist = g.GetComponents<GenericItemNet>();
                    if (hand == ControllerHand.LeftHand)
                    {
                    if (toucheditemleftlocid == uint.MaxValue)
                    {
                        var gb = g.GetComponent<GrabbableItemNet>();
                        if (gb != null)
                            toucheditemleft = gb.Slave.transform.GetChild(0);
                    }
                    else
                    {
                        if (glist != null && glist.Length > 0 && (glist.Length == 1 || glist[0].locID == toucheditemleftlocid))
                        {
                            if (glist[0].Slave == null)
                                toucheditemleft = glist[0].transform;
                            else
                                toucheditemleft = glist[0].Slave.transform;
                        }
                        else if (glist.Length > 1)
                        {
                            var i = g.GetComponent<LocalIDProvider>();
                            if (i != null)
                            {
                                try
                                {
                                    var g1 = (GenericItemNet)i.MultiNetworkBehaviours[toucheditemleftlocid];
                                    if (g1 != null)
                                    {
                                        if (g1.Slave == null)
                                            toucheditemleft = g1.transform;
                                        else
                                            toucheditemleft = g1.Slave.transform;
                                    }
                                }
                                catch (KeyNotFoundException k)
                                {
                                    Debug.Log(k.Message);
                                }
                            }
                        }
                    }
                }
                else if (hand == ControllerHand.RightHand)
                {
                    if (toucheditemrightlocid == uint.MaxValue)
                    {
                        var gb = g.GetComponent<GrabbableItemNet>();
                        if (gb != null)
                            toucheditemright = gb.Slave.transform.GetChild(0);
                    }
                    else
                    {
                        if (glist != null && glist.Length > 0 && (glist.Length == 1 || glist[0].locID == toucheditemrightlocid))
                        {
                            if (glist[0].Slave == null)
                                toucheditemright = glist[0].transform;
                            else
                                toucheditemright = glist[0].Slave.transform;
                        }
                        else if (glist.Length > 1)
                        {
                            var i = g.GetComponent<LocalIDProvider>();
                            if (i != null)
                            {
                                try
                                {
                                    var g1 = (GenericItemNet)i.MultiNetworkBehaviours[toucheditemrightlocid];
                                    if (g1 != null)
                                    {
                                        if (g1.Slave == null)
                                            toucheditemright = g1.transform;
                                        else
                                            toucheditemright = g1.Slave.transform;
                                    }
                                }
                                catch (KeyNotFoundException k)
                                {
                                    Debug.Log(k.Message);
                                }
                            }
                        }
                    }
                }
            }
          
        }
        if (hand == ControllerHand.LeftHand)
            toucheditemleftnetid = id;
        else if (hand == ControllerHand.RightHand)
            toucheditemrightnetid = id;
    }
    void ReSetTouchedItem(ControllerHand hand, bool ForceHook)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                toucheditemleftlocid = uint.MaxValue;
                if (isClient && !ForceHook)
                    toucheditemleftnetid = NetworkInstanceId.Invalid;
                else
                    OnTouchedItemLeftIDChanged(NetworkInstanceId.Invalid);
            }
            else if (hand == ControllerHand.RightHand)
            {
                toucheditemrightlocid = uint.MaxValue;
                if (isClient && !ForceHook)
                    toucheditemrightnetid = NetworkInstanceId.Invalid;
                else
                    OnTouchedItemRightIDChanged(NetworkInstanceId.Invalid);
            }
        }
    }
    void SetTouchedItem(GenericItemNet g, ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (g != null)
                {
                    toucheditemleftlocid = g.locID;
                    if (isClient)
                        toucheditemleftnetid = g.netId;
                    else
                        OnTouchedItemLeftIDChanged(g.netId);
                }                  
                else
                {
                    toucheditemleftlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemleftnetid = NetworkInstanceId.Invalid;
                    else
                        OnTouchedItemLeftIDChanged(NetworkInstanceId.Invalid);
                }
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (g != null)
                    {
                        toucheditemrightlocid = g.locID;
                        if (isClient)
                            toucheditemrightnetid = g.netId;
                        else
                            OnTouchedItemRightIDChanged(g.netId);
                    }
                else
                {
                    toucheditemrightlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemrightnetid = NetworkInstanceId.Invalid;
                    else
                        OnTouchedItemRightIDChanged(NetworkInstanceId.Invalid);
                }
            }
        }
    }

    void SetTouchedItem(GenericItemSlave g, ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (g != null)
                {
                    toucheditemleftlocid = g.MasterNet.locID;
                    if (isClient)
                        toucheditemleftnetid = g.MasterNet.netId;
                    else
                        OnTouchedItemLeftIDChanged(g.MasterNet.netId);
                }
                else
                {
                    toucheditemleftlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemleftnetid = NetworkInstanceId.Invalid;
                    else
                        OnTouchedItemLeftIDChanged(NetworkInstanceId.Invalid);
                }
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (g != null)
                {
                    toucheditemrightlocid = g.MasterNet.locID;
                    if (isClient)
                        toucheditemrightnetid = g.MasterNet.netId;
                    else
                        OnTouchedItemRightIDChanged(g.MasterNet.netId);
                }
                else
                {
                    toucheditemrightlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemrightnetid = NetworkInstanceId.Invalid;
                    else
                        OnTouchedItemRightIDChanged(NetworkInstanceId.Invalid);
                }
            }
        }
    }

    void SetTouchedItem(GrabbableItemSlave g, ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (g != null)
                {
                    toucheditemleftlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemleftnetid = g.MasterNet.netId;
                    else
                        OnTouchedItemLeftIDChanged(g.MasterNet.netId);
                }               
                else
                {
                    toucheditemleftlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemleftnetid = NetworkInstanceId.Invalid;
                    else
                        OnTouchedItemLeftIDChanged(NetworkInstanceId.Invalid);
                }
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (g != null)
                    {
                        toucheditemrightlocid = uint.MaxValue;
                        if (isClient)
                            toucheditemrightnetid = g.MasterNet.netId;
                        else
                            OnTouchedItemRightIDChanged(g.MasterNet.netId);
                    }
                else
                {
                    toucheditemrightlocid = uint.MaxValue;
                    if (isClient)
                        toucheditemrightnetid = NetworkInstanceId.Invalid;
                    else
                        OnTouchedItemRightIDChanged(NetworkInstanceId.Invalid);
                }
            }
        }
    }

    void SetTouchedItem(NPCController t, ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                toucheditemleftlocid = uint.MaxValue;
                toucheditemleft = t.transform;
            }
            else if (hand == ControllerHand.RightHand)
            {
                toucheditemrightlocid = uint.MaxValue;
                toucheditemright = t.transform;
            }
        }
    }

    void SetRayCastedItem(GenericItemNet g)
    {
        if (isServer)
        {
            if (g != null)
            {
                raycasteditemlocid = g.locID;
                if (isClient)
                    raycasteditemnetid = g.netId;
                else
                    OnRayCastedItemIDChanged(g.netId);
            }               
            else
            {
                raycasteditemlocid = uint.MaxValue;
                if (isClient)
                    raycasteditemnetid = NetworkInstanceId.Invalid;
                else
                    OnRayCastedItemIDChanged(NetworkInstanceId.Invalid);
            }
        }
    }

    void SetRayCastedItem(GenericItemSlave g)
    {
        if (isServer)
        {
            if (g != null)
            {
                raycasteditemlocid = g.MasterNet.locID;
                if (isClient)
                    raycasteditemnetid = g.MasterNet.netId;
                else
                    OnRayCastedItemIDChanged(g.MasterNet.netId);
            }
            else
            {
                raycasteditemlocid = uint.MaxValue;
                if (isClient)
                    raycasteditemnetid = NetworkInstanceId.Invalid;
                else
                    OnRayCastedItemIDChanged(NetworkInstanceId.Invalid);
            }
        }
    }

    void SetRayCastedItem(GrabbableItemSlave g)
    {
        if (isServer)
        {
            if(g != null)
                {
                raycasteditemlocid = uint.MaxValue;
                if (isClient)
                    raycasteditemnetid = g.MasterNet.netId;
                else
                    OnRayCastedItemIDChanged(g.MasterNet.netId);
                }
            else
                {
                raycasteditemlocid = uint.MaxValue;
                if (isClient)
                    raycasteditemnetid = NetworkInstanceId.Invalid;
                else
                    OnRayCastedItemIDChanged(NetworkInstanceId.Invalid);
                }
        }
    }

    void SetRayCastedItem(Transform t)
    {
        if (isServer)
        {
            if (t != null)
            {
                raycasteditemlocid = uint.MaxValue;
                raycasteditem = t;
            }
            else
            {
                raycasteditemlocid = uint.MaxValue;
                if (isClient)
                    raycasteditemnetid = NetworkInstanceId.Invalid;
                else
                    OnRayCastedItemIDChanged(NetworkInstanceId.Invalid);
            }
        }
    }

    void SetRayCastedItem(NPCController t)
    {
        if (isServer)
        {
            if (t != null)
            {
                raycasteditemlocid = uint.MaxValue;
                raycasteditem = t.transform;
            }
            else
            {
                raycasteditemlocid = uint.MaxValue;
                if (isClient)
                    raycasteditemnetid = NetworkInstanceId.Invalid;
                else
                    OnRayCastedItemIDChanged(NetworkInstanceId.Invalid);
            }
        }
    }


    void ReSetRayCastedItem()
    {
        if (isServer)
        {
            raycasteditemlocid = uint.MaxValue;
            if (isClient)
                raycasteditemnetid = NetworkInstanceId.Invalid;
            else
                OnRayCastedItemIDChanged(NetworkInstanceId.Invalid);
        }
    }

    GenericItemNet GetGenericItemNet(Transform item, uint id)
    {      
        if (item != null && id != uint.MaxValue)
        {
            var glist = item.GetComponents<GenericItemNet>();
            if (glist != null && glist.Length > 0 && (glist.Length == 1 || glist[0].locID == id))
                return glist[0];
            else if (glist.Length > 1)
            {
                var i = item.GetComponent<LocalIDProvider>();
                if (i != null)
                {
                    try
                    {
                        var g1 = (GenericItemNet)i.MultiNetworkBehaviours[id];
                        if (g1 != null)
                            return g1;
                    }
                    catch (KeyNotFoundException k)
                    {
                        Debug.Log(k.Message);                        
                    }
                }
            }
            else
            {
                var gs = item.GetComponent<GenericItemSlave>();
                if (gs != null)
                    return gs.MasterNet;
            }

        }
        return null;
    }

    void OnRayCastedItemIDChanged(NetworkInstanceId id)
    {
        if (id == NetworkInstanceId.Invalid)
        {
            raycasteditem = null;
        }
        else
        {
            GameObject g = null;
            if (isServer)
                g = NetworkServer.FindLocalObject(id);
            else
                g = ClientScene.FindLocalObject(id);
            if (g != null)
            {
                if (raycasteditemlocid == uint.MaxValue)
                {
                    var gb = g.GetComponent<GrabbableItemNet>();
                    if (gb != null)
                        raycasteditem = gb.Slave.transform.GetChild(0);
                }
                else
                {
                    var glist = g.GetComponents<GenericItemNet>();
                    if (glist != null && glist.Length > 0 && (glist.Length == 1 || glist[0].locID == raycasteditemlocid))
                    {
                        if (glist[0].Slave == null)
                            raycasteditem = glist[0].transform;
                        else
                            raycasteditem = glist[0].Slave.transform;
                    }
                    else if (glist.Length > 1)
                    {
                        var i = g.GetComponent<LocalIDProvider>();
                        if (i != null)
                        {
                            try
                            {
                                var g1 = (GenericItemNet)i.MultiNetworkBehaviours[raycasteditemlocid];
                                if (g1 != null)
                                {
                                    if (g1.Slave == null)
                                        raycasteditem = g1.transform;
                                    else
                                        raycasteditem = g1.Slave.transform;
                                }
                            }
                            catch (KeyNotFoundException k)
                            {
                                Debug.Log(k.Message);
                            }
                        }
                    }
                }
            }
        }
        raycasteditemnetid = id;
    }
    void SetCrosshair(CrosshairState s)
    {
        if (isLocalPlayer)
        {
            switch (s)
            {
                case CrosshairState.Cyan:
                    Crosshair.sprite = CyanCrosshair;
                    break;
                case CrosshairState.Magenta:
                    Crosshair.sprite = MagentaCrosshair;
                    break;
                case CrosshairState.Yellow:
                    Crosshair.sprite = YellowCrosshair;
                    break;
                case CrosshairState.Rainbow:
                    Crosshair.sprite = RainbowCrosshair;
                    break;
                default:
                    Crosshair.sprite = BaseCrosshair;
                    break;
            }
        }
        CurrentCrosshair = s;
    }
    [ClientRpc]
    void RpcSetCrosshair(CrosshairState s)
    {
        if (isLocalPlayer)
        {
            switch (s)
            {
                case CrosshairState.Cyan:
                    Crosshair.sprite = CyanCrosshair;
                    break;
                case CrosshairState.Magenta:
                    Crosshair.sprite = MagentaCrosshair;
                    break;
                case CrosshairState.Yellow:
                    Crosshair.sprite = YellowCrosshair;
                    break;
                case CrosshairState.Rainbow:
                    Crosshair.sprite = RainbowCrosshair;
                    break;
                default:
                    Crosshair.sprite = BaseCrosshair;
                    break;
            }
        }
        CurrentCrosshair = s;
    }
    public void DoDeferredRaycast()
    {
        if (DeferredRaycast)
            DoRayCast();
    }
    public void SetCrosshairPublic(CrosshairState s)
    {
        if (isLocalPlayer)
        {
            switch (s)
            {
                case CrosshairState.Cyan:
                    Crosshair.sprite = CyanCrosshair;
                    break;
                case CrosshairState.Magenta:
                    Crosshair.sprite = MagentaCrosshair;
                    break;
                case CrosshairState.Yellow:
                    Crosshair.sprite = YellowCrosshair;
                    break;
                case CrosshairState.Rainbow:
                    Crosshair.sprite = RainbowCrosshair;
                    break;
                default:
                    Crosshair.sprite = BaseCrosshair;
                    break;
            }
        }
        ForcedCrosshair = s;
    }
    [ClientRpc]
    public void RpcSetCrosshairPublic(CrosshairState s)
    {
        if (isLocalPlayer)
        {
            switch (s)
            {
                case CrosshairState.Cyan:
                    Crosshair.sprite = CyanCrosshair;
                    break;
                case CrosshairState.Magenta:
                    Crosshair.sprite = MagentaCrosshair;
                    break;
                case CrosshairState.Yellow:
                    Crosshair.sprite = YellowCrosshair;
                    break;
                case CrosshairState.Rainbow:
                    Crosshair.sprite = RainbowCrosshair;
                    break;
                default:
                    Crosshair.sprite = BaseCrosshair;
                    break;
            }
        }
        ForcedCrosshair = s;
    }

    public GrabbableItemNet GetCurrentItem(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex == -1)
                return null;
            else
                return ItemsLeft[ItemLeftIndex].GetComponent<GrabbableItemNet>();
        }
        else if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex == -1)
                return null;
            else
                return ItemsRight[ItemRightIndex].GetComponent<GrabbableItemNet>();
        }
        else
            return null;
    }


    void OnLeftItemIDChanged(NetworkInstanceId id)
    {
        OnItemIDChanged(id, ControllerHand.LeftHand, false);
    }

    void OnItemIDChanged(NetworkInstanceId id, ControllerHand hand, bool ForceHook)
    {
        if (Type == ItemControllerType.VR)
        {
            if (isServer)
            {
                if (id != NetworkInstanceId.Invalid) //grabitem server
                {
                    var i = NetworkServer.FindLocalObject(id);
                    var gi = i.GetComponent<GenericItemNet>();
                    if (gi != null)
                    {
                        gi.Interact(this, hand);
                        gi.DisablePhysics();
                        if (gi.ItemCode == ItemCodes.Generic)
                        {
                            if (gi.Player != null)
                                gi.Player.DropItem(gi.transform, true);
                            gi.Player = this;
                            gi.Unselect();
                            if (hand == ControllerHand.LeftHand)
                            {
                                gi.ForceParent(LeftController, true);
                                ItemLeft = gi.transform;
                                if (isLocalPlayer)
                                {
                                    LeftItemSource.clip = DefaultGrabSound;
                                    LeftItemSource.volume = DefaultGrabSoundVolume;
                                    LeftItemSource.Play();
                                }
                            }
                            else if (hand == ControllerHand.RightHand)
                            {
                                gi.ForceParent(RightController, true);
                                ItemRight = gi.transform;
                                if (isLocalPlayer)
                                {
                                    RightItemSource.clip = DefaultGrabSound;
                                    RightItemSource.volume = DefaultGrabSoundVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }
                        else
                        {
                            gi.Player = this;
                            gi.DisableItem(this, hand);
                            var g = EnableItem(gi.ItemCode, gi, hand);
                            gi.ForceParent(g.Slave.transform.GetChild(0), false);
                            if (isLocalPlayer)
                            {
                                if (hand == ControllerHand.LeftHand)
                                {
                                    LeftItemSource.clip = g.Grab;
                                    LeftItemSource.volume = g.GrabVolume;
                                    LeftItemSource.Play();
                                }
                                else if (hand == ControllerHand.RightHand)
                                {
                                    RightItemSource.clip = g.Grab;
                                    RightItemSource.volume = g.GrabVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }
                        RpcShortVibration(hand);
                    }
                    ReSetTouchedItem(hand, ForceHook);
                    if (Run != null)
                        Run.Grab(hand);
                }
                else //dropitem server
                {
                    if ((hand == ControllerHand.LeftHand && ItemLeft != null) || (hand == ControllerHand.RightHand && ItemRight != null))
                    {
                        GenericItemNet i = null;
                        if (hand == ControllerHand.LeftHand)
                            i = ItemLeft.GetComponent<GenericItemNet>();
                        else if (hand == ControllerHand.RightHand)
                            i = ItemRight.GetComponent<GenericItemNet>();


                        if (i != null)
                        {
                            i.Player = null;
                            i.Unselect();
                            i.DropParent();
                            i.EnablePhysics();
                            i.EnableItem(this, hand);

                            if (i.ItemCode != ItemCodes.Generic)
                            {
                                var g = DisableItem(hand);
                                if (isLocalPlayer)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                        LeftItemSource.clip = g.Drop;
                                        LeftItemSource.volume = g.DropVolume;
                                        LeftItemSource.Play();
                                    }
                                    else if (hand == ControllerHand.RightHand)
                                    {
                                        RightItemSource.clip = g.Drop;
                                        RightItemSource.volume = g.DropVolume;
                                        RightItemSource.Play();
                                    }
                                }
                            }
                            else
                            {
                                if (hand == ControllerHand.LeftHand)
                                    ItemLeft = null;
                                else if (hand == ControllerHand.RightHand)
                                    ItemRight = null;
                                if (isLocalPlayer)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                        LeftItemSource.clip = DefaultDropSound;
                                        LeftItemSource.volume = DefaultDropSoundVolume;
                                        LeftItemSource.Play();
                                    }
                                    else if (hand == ControllerHand.RightHand)
                                    {
                                        RightItemSource.clip = DefaultDropSound;
                                        RightItemSource.volume = DefaultDropSoundVolume;
                                        RightItemSource.Play();
                                    }
                                }
                            }
                            StopPulse(hand);
                            RpcShortVibration(hand);
                        }

                    }
                    if(Run!= null)
                        Run.Drop(hand);
                }
            }
            else
            {
                if (id != NetworkInstanceId.Invalid) //grabitem client
                {
                    var i = ClientScene.FindLocalObject(id);
                    var gi = i.GetComponent<GenericItemNet>();
                    if (gi != null)
                    {
                        gi.Player = this;
                        gi.DisablePhysics();
                        if (gi.ItemCode == ItemCodes.Generic)
                        {
                            if (hand == ControllerHand.LeftHand)
                            {
                                gi.ForceParent(LeftController, true);
                                ItemLeft = gi.transform;
                                if (isLocalPlayer)
                                {
                                    gi.DisableItem(this, hand);
                                    LeftItemSource.clip = DefaultGrabSound;
                                    LeftItemSource.volume = DefaultGrabSoundVolume;
                                    LeftItemSource.Play();
                                }
                            }
                            else if (hand == ControllerHand.RightHand)
                            {
                                gi.ForceParent(RightController, true);
                                ItemRight = gi.transform;
                                if (isLocalPlayer)
                                {
                                    gi.DisableItem(this, hand);
                                    RightItemSource.clip = DefaultGrabSound;
                                    RightItemSource.volume = DefaultGrabSoundVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }
                        else
                        {
                            gi.DisableItem(this, hand);
                            var g = EnableItem(gi.ItemCode, gi, hand);
                            gi.ForceParent(g.Slave.transform.GetChild(0), false);
                            if (hand == ControllerHand.LeftHand)
                            {
                                if (isLocalPlayer)
                                {
                                    LeftItemSource.clip = g.Grab;
                                    LeftItemSource.volume = g.GrabVolume;
                                    LeftItemSource.Play();
                                }

                            }
                            else if (hand == ControllerHand.RightHand)
                            {
                                if (isLocalPlayer)
                                {
                                    RightItemSource.clip = g.Grab;
                                    RightItemSource.volume = g.GrabVolume;
                                    RightItemSource.Play();
                                }
                            }

                        }
                    }
                }
                else //dropitem client
                {
                    if ((hand == ControllerHand.LeftHand && ItemLeft != null) || (hand == ControllerHand.RightHand && ItemRight != null))
                    {
                        GenericItemNet i = null;
                        if (hand == ControllerHand.LeftHand)
                            i = ItemLeft.GetComponent<GenericItemNet>();
                        else if (hand == ControllerHand.RightHand)
                            i = ItemRight.GetComponent<GenericItemNet>();
                        if (i != null)
                        {
                            i.Player = null;
                            i.DropParent();
                            i.EnablePhysics();
                            i.EnableItem(this, hand);

                            if (i.ItemCode != ItemCodes.Generic)
                            {
                                if (hand == ControllerHand.LeftHand)
                                {
                                    if (ItemLeftIndex == -1)
                                        ItemLeftIndex = GetGrabbableItemByCode(i.ItemCode, hand);
                                    var g = DisableItem(hand);
                                    if (isLocalPlayer)
                                    {
                                        LeftItemSource.clip = g.Drop;
                                        LeftItemSource.volume = g.DropVolume;
                                        LeftItemSource.Play();
                                    }
                                }
                                else if (hand == ControllerHand.RightHand)
                                {
                                    if (ItemRightIndex == -1)
                                        ItemRightIndex = GetGrabbableItemByCode(i.ItemCode, hand);
                                    var g = DisableItem(hand);
                                    if (isLocalPlayer)
                                    {
                                        RightItemSource.clip = g.Drop;
                                        RightItemSource.volume = g.DropVolume;
                                        RightItemSource.Play();
                                    }
                                }
                            }
                            else
                            {
                                if (hand == ControllerHand.LeftHand)
                                    ItemLeft = null;
                                else if (hand == ControllerHand.RightHand)
                                    ItemRight = null;
                                if (isLocalPlayer)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                        LeftItemSource.clip = DefaultDropSound;
                                        LeftItemSource.volume = DefaultDropSoundVolume;
                                        LeftItemSource.Play();
                                    }
                                    else if (hand == ControllerHand.RightHand)
                                    {
                                        RightItemSource.clip = DefaultDropSound;
                                        RightItemSource.volume = DefaultDropSoundVolume;
                                        RightItemSource.Play();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (Type == ItemControllerType.FPS)
        {
            if (isServer)
            {
                if (id != NetworkInstanceId.Invalid) //grabitem server
                {
                    var i = NetworkServer.FindLocalObject(id);
                    var gi = i.GetComponent<GenericItemNet>();
                    if (gi != null)
                    {
                        gi.Interact(this, hand);
                        gi.DisablePhysics();
                        if (gi.ItemCode == ItemCodes.Generic)
                        {
                            if (gi.Player != null)
                                gi.Player.DropItem(gi.transform, true);
                            gi.Player = this;
                            gi.Unselect();
                            if (hand == ControllerHand.LeftHand)
                            {
                                gi.ForceParent(LeftController, true);
                                ItemLeft = gi.transform;
                                if (isLocalPlayer)
                                {
                                    LeftItemSource.clip = DefaultGrabSound;
                                    LeftItemSource.volume = DefaultGrabSoundVolume;
                                    LeftItemSource.Play();
                                }
                            }
                            else if (hand == ControllerHand.RightHand)
                            {
                                gi.ForceParent(RightController, true);
                                ItemRight = gi.transform;
                                if (isLocalPlayer)
                                {
                                    RightItemSource.clip = DefaultGrabSound;
                                    RightItemSource.volume = DefaultGrabSoundVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }
                        else
                        {
                            gi.Player = this;
                            gi.DisableItem(this, hand);

                            var g = EnableItem(gi.ItemCode, gi, hand);
                            gi.ForceParent(g.Slave.transform.GetChild(0), false);
                            if (hand == ControllerHand.LeftHand)
                            {
                                if (isLocalPlayer)
                                {
                                    LeftItemSource.clip = g.Grab;
                                    LeftItemSource.volume = g.GrabVolume;
                                    LeftItemSource.Play();
                                }
                            }
                            else if (hand == ControllerHand.RightHand)
                            {
                                if (isLocalPlayer)
                                {
                                    RightItemSource.clip = g.Grab;
                                    RightItemSource.volume = g.GrabVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }

                        if ((hand == ControllerHand.LeftHand && raycasteditem == ItemLeft) || (hand == ControllerHand.RightHand && raycasteditem == ItemRight))
                        {
                            ReSetRayCastedItem();
                            ShowCanUseFPS();
                        }
                    }
                }
                else //dropitem server
                {

                    if ((hand == ControllerHand.LeftHand && ItemLeft != null) || (hand == ControllerHand.RightHand && ItemRight != null))
                    {
                        GenericItemNet i = null;
                        if (hand == ControllerHand.LeftHand)
                            i = ItemLeft.GetComponent<GenericItemNet>();
                        else if (hand == ControllerHand.RightHand)
                            i = ItemRight.GetComponent<GenericItemNet>();
                        if (i != null)
                        {
                            i.Player = null;
                            i.Unselect();
                            i.DropParent();
                            i.EnablePhysics();
                            i.EnableItem(this, hand);

                            if (i.ItemCode != ItemCodes.Generic)
                            {
                                var g = DisableItem(hand);
                                if (isLocalPlayer)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                        LeftItemSource.clip = g.Drop;
                                        LeftItemSource.volume = g.DropVolume;
                                        LeftItemSource.Play();
                                    }
                                    else if (hand == ControllerHand.RightHand)
                                    {
                                        RightItemSource.clip = g.Drop;
                                        RightItemSource.volume = g.DropVolume;
                                        RightItemSource.Play();
                                    }
                                    SetCrosshair(CrosshairState.Default);
                                }
                                else
                                    RpcSetCrosshair(CrosshairState.Default);
                            }
                            else
                            {
                                if (isLocalPlayer)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                        LeftItemSource.clip = DefaultDropSound;
                                        LeftItemSource.volume = DefaultDropSoundVolume;
                                        LeftItemSource.Play();
                                    }
                                    else if (hand == ControllerHand.RightHand)
                                    {
                                        RightItemSource.clip = DefaultDropSound;
                                        RightItemSource.volume = DefaultDropSoundVolume;
                                        RightItemSource.Play();
                                    }
                                    SetCrosshair(CrosshairState.Default);
                                }
                                else
                                    RpcSetCrosshair(CrosshairState.Default);
                            }
                        }

                        if (hand == ControllerHand.LeftHand)
                            ItemLeft = null;
                        else if (hand == ControllerHand.RightHand)
                            ItemRight = null;
                    }
                }
            }
            else
            {
                if (id != NetworkInstanceId.Invalid) //grabitem client
                {
                    var i = ClientScene.FindLocalObject(id);
                    var gi = i.GetComponent<GenericItemNet>();
                    if (gi != null)
                    {
                        gi.Player = this;
                        gi.DisablePhysics();
                        if (gi.ItemCode == ItemCodes.Generic)
                        {
                            if (hand == ControllerHand.LeftHand)
                            {
                                gi.ForceParent(LeftController, true);
                                ItemLeft = gi.transform;
                                if (isLocalPlayer)
                                {
                                    gi.DisableItem(this, hand);
                                    LeftItemSource.clip = DefaultGrabSound;
                                    LeftItemSource.volume = DefaultGrabSoundVolume;
                                    LeftItemSource.Play();
                                }

                            }
                            else if (hand == ControllerHand.RightHand)
                            {
                                gi.ForceParent(RightController, true);
                                ItemRight = gi.transform;
                                if (isLocalPlayer)
                                {
                                    gi.DisableItem(this, hand);
                                    RightItemSource.clip = DefaultGrabSound;
                                    RightItemSource.volume = DefaultGrabSoundVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }
                        else
                        {
                            gi.DisableItem(this, hand);

                            var g = EnableItem(gi.ItemCode, gi, hand);
                            gi.ForceParent(g.Slave.transform.GetChild(0), false);
                            if (isLocalPlayer)
                            {
                                if (hand == ControllerHand.LeftHand)
                                {
                                    LeftItemSource.clip = g.Grab;
                                    LeftItemSource.volume = g.GrabVolume;
                                    LeftItemSource.Play();
                                }
                                else if (hand == ControllerHand.RightHand)
                                {
                                    RightItemSource.clip = g.Grab;
                                    RightItemSource.volume = g.GrabVolume;
                                    RightItemSource.Play();
                                }
                            }
                        }
                    }
                }
                else //dropitem client
                {
                    if ((hand == ControllerHand.LeftHand && ItemLeft != null) || (hand == ControllerHand.RightHand && ItemRight != null))
                    {
                        GenericItemNet i = null;
                        if (hand == ControllerHand.LeftHand)
                            i = ItemLeft.GetComponent<GenericItemNet>();
                        else if (hand == ControllerHand.RightHand)
                            i = ItemRight.GetComponent<GenericItemNet>();

                        if (i != null)
                        {
                            i.Player = null;
                            i.DropParent();
                            i.EnablePhysics();
                            i.EnableItem(this, hand);

                            if (i.ItemCode != ItemCodes.Generic)
                            {
                                if (hand == ControllerHand.LeftHand)
                                {
                                    if (ItemLeftIndex == -1)
                                        ItemLeftIndex = GetGrabbableItemByCode(i.ItemCode, hand);
                                    var g = DisableItem(hand);
                                    if (isLocalPlayer)
                                    {
                                        LeftItemSource.clip = g.Drop;
                                        LeftItemSource.volume = g.DropVolume;
                                        LeftItemSource.Play();
                                    }
                                }
                                else if (hand == ControllerHand.RightHand)
                                {
                                    if (ItemRightIndex == -1)
                                        ItemRightIndex = GetGrabbableItemByCode(i.ItemCode, hand);
                                    var g = DisableItem(hand);
                                    if (isLocalPlayer)
                                    {
                                        RightItemSource.clip = g.Drop;
                                        RightItemSource.volume = g.DropVolume;
                                        RightItemSource.Play();
                                    }
                                }
                            }
                            else
                            {
                                if (isLocalPlayer)
                                {
                                    if (hand == ControllerHand.LeftHand)
                                    {
                                        LeftItemSource.clip = DefaultDropSound;
                                        LeftItemSource.volume = DefaultDropSoundVolume;
                                        LeftItemSource.Play();
                                    }
                                    else if (hand == ControllerHand.RightHand)
                                    {
                                        RightItemSource.clip = DefaultDropSound;
                                        RightItemSource.volume = DefaultDropSoundVolume;
                                        RightItemSource.Play();
                                    }
                                }
                            }

                            if (hand == ControllerHand.LeftHand)
                                ItemLeft = null;
                            else if (hand == ControllerHand.RightHand)
                                ItemRight = null;
                        }
                    }                   
                }
            }
        }
        if (hand == ControllerHand.LeftHand)
        {
            ItemLeftID = id;
            LeftInteracting = false;
            if(isServer && OnLeftInteractionFinished != null)
                OnLeftInteractionFinished.Invoke(this);
            leftoperating = false;
        }
        else if (hand == ControllerHand.RightHand)
        {
            ItemRightID = id;
            RightInteracting = false;
            if (isServer && OnRightInteractionFinished != null)
                OnRightInteractionFinished.Invoke(this);
            rightoperating = false;
        }
        ReSetTouchedItem(hand, ForceHook);
    }


    void OnRightItemIDChanged(NetworkInstanceId id)
    {
        OnItemIDChanged(id, ControllerHand.RightHand, false);
    }

    public void ToggleLeftBrochure(object sender, ClickedEventArgs e)
    {
        if (isServer && !leftoperating)
        {
            leftoperating = true;
            ToggleBrochure(ControllerHand.LeftHand);
        }
    }

    public void ToggleRightBrochure(object sender, ClickedEventArgs e)
    {
        if (isServer && !rightoperating)
        {
            rightoperating = true;
            ToggleBrochure(ControllerHand.RightHand);
        }
    }

    public void ToggleBrochure(ControllerHand hand)
    {
        if (isServer)
        {
            if (Type == ItemControllerType.VR)
            {
                if (hand == ControllerHand.RightHand)
                {
                    if (RightBrochureEnabled)
                        DisableRightBrochure();
                    else if (BrochureAvailable && !LeftBrochureEnabled && ItemRight == null)
                        EnableRightBrochure();
                    rightoperating = false;

                }
                else if (hand == ControllerHand.LeftHand)
                {
                    if (LeftBrochureEnabled)
                        DisableLeftBrochure();
                    else if (BrochureAvailable && !RightBrochureEnabled && ItemLeft== null)
                        EnableLeftBrochure();
                    leftoperating = false;
                }
            }
            else if (Type == ItemControllerType.FPS)
            {
                if (hand == ControllerHand.RightHand)
                {
                    if (RightBrochureEnabled)
                        DisableRightBrochure();
                    else if (BrochureAvailable && !LeftBrochureEnabled && ItemRight == null && raycasteditem == null && forcedtarget == null)
                        EnableRightBrochure();
                    rightoperating = false;
                }
                else if (hand == ControllerHand.LeftHand)
                {
                    if (LeftBrochureEnabled)
                        DisableLeftBrochure();
                    else if (BrochureAvailable && !RightBrochureEnabled && ItemLeft== null && raycasteditem == null && forcedtarget == null)
                        EnableLeftBrochure();
                    leftoperating = false;
                }
            }
        }
    }


  
    private void DoRayCast()
    {
        if (isServer)
        {
            RaycastHit[] hits;
            string[] layers = new string[4];
            layers[0] = "Default";
            layers[1] = "MovingItem";
            layers[2] = "PlayerLayer";
            layers[3] = "UI";
            var mask = LayerMask.GetMask(layers);
            bool onehit = false;

            if (ItemLeftID == NetworkInstanceId.Invalid || ItemRightID == NetworkInstanceId.Invalid)
            {
                hits = RayCaster.RayCastAll(RaycastMaxDistance, mask);
                if (hits.Length > 0)
                {
                    foreach (RaycastHit hit in hits)
                    {
                        var o = hit.collider.transform;
                        if (transform != o)
                        {
                            if (o.tag == "Item")
                            {
                                if (o != ItemLeft && o != ItemRight  && (!LeftBrochureEnabled || !RightBrochureEnabled))
                                {
                                    if (o != raycasteditem)
                                    {
                                        var g = o.GetComponent<GenericItemNet>();
                                        if (g != null)
                                            FPSInteract(g);
                                        else
                                        {
                                            var gs = o.GetComponent<GenericItemSlave>();
                                            if (gs != null)
                                                FPSInteract(gs);
                                            else
                                            {
                                                var gbs = GetGrabbableItemSlave(o);
                                                if (gbs != null)
                                                    FPSInteract(gbs);
                                                else
                                                    Debug.Log("No usable component on Item-tagged object: " + o.name);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (raycasteditem != null && !raycastednpc && !UIElement)
                                    {
                                        var gi = GetGenericItemNet(raycasteditem, raycasteditemlocid);
                                        if (gi != null)
                                            gi.Unselect();
                                        else
                                        {
                                                var gbs = GetGrabbableItemSlave(raycasteditem);
                                                if (gbs != null)
                                                {
                                                    if (!isClient)
                                                        gbs.DisableOutline(this);
                                                    RpcDisableOutline(gbs.MasterNet.netId);
                                                }
                                        }
                                        if (ItemLeft != null || ItemRight != null)
                                            ShowCanUseFPS();
                                        else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                        {
                                            if (isLocalPlayer)
                                                SetCrosshair(ForcedCrosshair);
                                            else
                                                RpcSetCrosshair(ForcedCrosshair);
                                        }
                                        else if (ForcedCrosshair == CrosshairState.None)
                                        {
                                            if (isLocalPlayer)
                                                SetCrosshair(CrosshairState.Default);
                                            else
                                                RpcSetCrosshair(CrosshairState.Default);
                                        }
                                        ReSetRayCastedItem();
                                    }
                                    else if (raycasteditem != null && raycastednpc)
                                    {
                                        raycasteditem.GetComponentInParent<NPCController>().DisableOutline();
                                        if (ItemLeft != null || ItemRight != null)
                                            ShowCanUseFPS();
                                        else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                        {
                                            if (isLocalPlayer)
                                                SetCrosshair(ForcedCrosshair);
                                            else
                                                RpcSetCrosshair(ForcedCrosshair);
                                        }
                                        else if (ForcedCrosshair == CrosshairState.None)
                                        {
                                            if (isLocalPlayer)
                                                SetCrosshair(CrosshairState.Default);
                                            else
                                                RpcSetCrosshair(CrosshairState.Default);
                                        }
                                        ReSetRayCastedItem();
                                        raycastednpc = false;
                                    }
                                    else if (raycasteditem != null && UIElement)
                                    {
                                        var b = raycasteditem.GetComponent<VRUIElement>();

                                        if (!b.transition)
                                            b.UnSelected();

                                        if (!b.IsElementActive() || !b.gameObject.activeSelf)
                                        {
                                            b.UnSelected();
                                            raycasteditem = null;
                                        }

                                        ReSetRayCastedItem();
                                        UIElement = false;
                                    }
                                }
                                onehit = true;
                                break;
                            }
                            else if (o.tag == "NPC" && (ItemLeft== null || ItemRight == null))
                            {
                                var g = o.GetComponentInParent<NPCController>();
                                if (g == null)
                                    Debug.Log("No NPC Controller component on NPC-tagged object: " + o.name);
                                if (g != null && g.CanInteract())
                                {

                                    DeselectRaycastedItem();
                                    SetRayCastedItem(g);
                                    raycastednpc = true;
                                    ShowCanInteractFPS();
                                    g.EnableOutline();
                                }
                                else if (ItemLeft != null || ItemRight != null)
                                    ShowCanUseFPS();
                                else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                {
                                    if (isLocalPlayer)
                                        SetCrosshair(ForcedCrosshair);
                                    else
                                        RpcSetCrosshair(ForcedCrosshair);
                                }
                                else if (ForcedCrosshair == CrosshairState.None)
                                {
                                    if (isLocalPlayer)
                                        SetCrosshair(CrosshairState.Default);
                                    else
                                        RpcSetCrosshair(CrosshairState.Default);
                                }
                                onehit = true;
                                break;
                            }
                            else if (o.gameObject.layer == LayerMask.NameToLayer("UI"))
                            {
                                var uihit = hit.collider.transform.GetComponent<VRUIElement>();
                                if (uihit != null && uihit.IsElementActive() && raycasteditem != uihit.transform && !uihit.transition)
                                {
                                    DeselectRaycastedItem();
                                    SetRayCastedItem(uihit.transform);
                                    UIElement = true;
                                    raycasteditem.GetComponent<VRUIElement>().Selected(this);
                                    ShowCanInteractFPS();
                                }
                                else if (uihit == null || !uihit.IsElementActive() || uihit.transition)
                                {

                                    DeselectRaycastedItem();
                                    ReSetRayCastedItem();
                                    UIElement = false;

                                    if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                    {
                                        if (isLocalPlayer)
                                            SetCrosshair(ForcedCrosshair);
                                        else
                                            RpcSetCrosshair(ForcedCrosshair);
                                    }
                                    else if (ForcedCrosshair == CrosshairState.None)
                                    {
                                        if (isLocalPlayer)
                                            SetCrosshair(CrosshairState.Default);
                                        else
                                            RpcSetCrosshair(CrosshairState.Default);
                                    }
                                }
                                onehit = true;
                            }
                        }
                    }
                }
            }

            if (!onehit && forcedtarget != null)
            {
                var g = forcedtarget.GetComponent<GenericItemNet>();
                if (g != null)
                {
                    if (g.CanInteract(this))
                    {
                        if (forcedtarget != raycasteditem)
                        {
                                DeselectRaycastedItem();
                                SetRayCastedItem(g);
                                raycastednpc = false;
                                ShowCanInteractFPS();
                                g.Select();
                        }
                    }
                    else if (raycasteditem != null)
                        {
                            DeselectRaycastedItem();
                            if (ItemLeft != null || ItemRight != null)
                                ShowCanUseFPS();
                            else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                            {
                                if (isLocalPlayer)
                                    SetCrosshair(ForcedCrosshair);
                                else
                                    RpcSetCrosshair(ForcedCrosshair);
                            }
                            else if (ForcedCrosshair == CrosshairState.None)
                            {
                                if (isLocalPlayer)
                                    SetCrosshair(CrosshairState.Default);
                                else
                                    RpcSetCrosshair(CrosshairState.Default);
                            }
                            ReSetRayCastedItem();
                        }
                }
                else
                {
                    var gi = forcedtarget.GetComponent<GenericItemSlave>();
                    if (gi != null)
                    {
                        if (gi.MasterNet.CanInteract(this))
                        {
                            if (forcedtarget != raycasteditem)
                            {
                                DeselectRaycastedItem();
                                SetRayCastedItem(gi);
                                raycastednpc = false;
                                ShowCanInteractFPS();
                                gi.MasterNet.Select();
                            }
                        }
                        else if (raycasteditem != null)
                            {
                                DeselectRaycastedItem();
                                if (ItemLeft != null || ItemRight != null)
                                    ShowCanUseFPS();
                                else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                {
                                    if (isLocalPlayer)
                                        SetCrosshair(ForcedCrosshair);
                                    else
                                        RpcSetCrosshair(ForcedCrosshair);
                                }
                                else if (ForcedCrosshair == CrosshairState.None)
                                {
                                    if (isLocalPlayer)
                                        SetCrosshair(CrosshairState.Default);
                                    else
                                        RpcSetCrosshair(CrosshairState.Default);
                                }
                                ReSetRayCastedItem();
                            }
                    }
                    else
                    {
                        var gbs = GetGrabbableItemSlave(forcedtarget);
                        if (gbs != null)
                        {
                            if (forcedtarget != raycasteditem)
                            {
                                SetRayCastedItem(gbs);
                                raycastednpc = false;
                                ShowCanInteractFPS();
                                if (!isClient)
                                    gbs.EnableOutline(this);
                                RpcEnableOutline(gbs.MasterNet.netId);
                            }
                        }
                        else
                            Debug.Log("No usable component on Item-tagged object: " + forcedtarget.name);
                    }
                }

            }
            else if (!onehit && raycasteditem != null)
                {
                    DeselectRaycastedItem();
                    if (ItemLeft != null || ItemRight != null)
                        ShowCanUseFPS();
                    else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                    {
                        if (isLocalPlayer)
                            SetCrosshair(ForcedCrosshair);
                        else
                            RpcSetCrosshair(ForcedCrosshair);
                    }
                    else if (ForcedCrosshair == CrosshairState.None)
                    {
                        if (isLocalPlayer)
                            SetCrosshair(CrosshairState.Default);
                        else
                            RpcSetCrosshair(CrosshairState.Default);
                    }
                    ReSetRayCastedItem();
            }
      
        }
        else if (isLocalPlayer)
        {
            RaycastHit[] hits;
            string[] layers = new string[1];
            layers[0] = "UI";
            var mask = LayerMask.GetMask(layers);
            bool onehit = false;

            if ((ItemLeftID == NetworkInstanceId.Invalid || ItemRightID == NetworkInstanceId.Invalid) && raycasteditem == null)
            {
                hits = RayCaster.RayCastAll(RaycastMaxDistance, mask);
                if (hits.Length > 0)
                {
                    foreach (RaycastHit hit in hits)
                    {
                        var o = hit.collider.transform;
                        if (transform != o)
                        {
                            if (o.gameObject.layer == LayerMask.NameToLayer("UI"))
                            {
                                var uihit = hit.collider.transform.GetComponent<VRUIElement>();


                                if (uihit != null && uihit.IsElementActive() && offlineraycasteditem != uihit.transform && !uihit.transition)
                                {
                                    if (offlineraycasteditem != null)
                                    {
                                        var b = offlineraycasteditem.GetComponent<VRUIElement>();
                                        b.UnSelected();
                                    }
                                    offlineraycasteditem = uihit.transform;
                                    offlineraycasteditem.GetComponent<VRUIElement>().Selected(this);

                                    ShowCanInteractFPS();

                                }
                                else if (uihit == null || !uihit.IsElementActive() || uihit.transition)
                                {
                                    if (offlineraycasteditem != null)
                                    {
                                        var b = offlineraycasteditem.GetComponent<VRUIElement>();
                                        b.UnSelected();
                                    }
                                    offlineraycasteditem = null;

                                    if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                                        SetCrosshair(ForcedCrosshair);
                                    else if (ForcedCrosshair == CrosshairState.None)
                                        SetCrosshair(CrosshairState.Default);
                                }
                                onehit = true;
                            }
                        }
                    }
                }
                else if (!onehit && offlineraycasteditem != null)
                {
                    var b = offlineraycasteditem.GetComponent<VRUIElement>();
                    if (!b.transition)
                        b.UnSelected();

                    if (!b.IsElementActive() || !b.gameObject.activeSelf)
                    {
                        b.UnSelected();
                        offlineraycasteditem = null;
                    }

                    if (ItemLeft != null || ItemRight != null)
                        ShowCanUseFPS();
                    else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                    {
                        SetCrosshair(ForcedCrosshair);
                    }
                    else if (ForcedCrosshair == CrosshairState.None)
                    {
                        SetCrosshair(CrosshairState.Default);
                    }
                    offlineraycasteditem = null;
                }
            }
        }
    }

   
    private void FPSInteract(GrabbableItemSlave gbs)
    {
        if (gbs != null)
        {
            if (gbs.transform != raycasteditem)
            {
                DeselectRaycastedItem();
                SetRayCastedItem(gbs);
                ShowCanInteractFPS();
                if (!isClient)
                    gbs.EnableOutline(this);
                RpcEnableOutline(gbs.MasterNet.netId);
            }
        }

    }

    private void FPSInteract(GenericItemSlave gs)
    {
        if (gs != null && gs.MasterNet.CanInteract(this) && !gs.MasterNet.AlreadySelected)
        {
            if (gs.transform != raycasteditem)
            {
                    DeselectRaycastedItem();
                    SetRayCastedItem(gs);
                    ShowCanInteractFPS();
                    gs.Select();
                }
            }
        
    }

    private void FPSInteract(GenericItemNet g)
    {
        if (g != null && g.CanInteract(this) && !g.AlreadySelected)
        {
            if (g.transform != raycasteditem)
            {
                    DeselectRaycastedItem();
                    SetRayCastedItem(g);
                    ShowCanInteractFPS();
                    g.Select();
                }
        }
    }

    private void DeselectRaycastedItem()
    {
        if (raycasteditem != null)
        {
            if (!raycastednpc && !UIElement)
            {
                var gi = GetGenericItemNet(raycasteditem, raycasteditemlocid);
                if (gi != null)
                    gi.Unselect();
                else
                {
                    var gbs = GetGrabbableItemSlave(raycasteditem);
                    if (gbs != null)
                    {
                        if (!isClient)
                            gbs.DisableOutline(this);
                        RpcDisableOutline(gbs.MasterNet.netId);
                    }
                }
            }
            else if (raycastednpc)
            {
                raycasteditem.GetComponentInParent<NPCController>().DisableOutline();
                raycastednpc = false;
            }
            else
            {
                var b = raycasteditem.GetComponent<VRUIElement>();
                if (b != null)
                {
                    if (!b.transition)
                        b.UnSelected();

                    if (!b.IsElementActive() || !b.gameObject.activeSelf)
                        b.UnSelected();
                }
                UIElement = false;
            }
        }
    }

    private void ShowCanUseFPS()
    {
        if (isLocalPlayer)
        {
            switch (ButtonToUse)
            {
                case ControllerButtonInput.Any:
                    SetCrosshair(CrosshairState.Rainbow);
                    break;
                case ControllerButtonInput.Pad:
                    SetCrosshair(CrosshairState.Yellow);
                    break;
                case ControllerButtonInput.Grip:
                    SetCrosshair(CrosshairState.Cyan);
                    break;
                default:
                    SetCrosshair(CrosshairState.Magenta);
                    break;
            }
        }
        else
        {
            switch (ButtonToUse)
            {
                case ControllerButtonInput.Any:
                    RpcSetCrosshair(CrosshairState.Rainbow);
                    break;
                case ControllerButtonInput.Pad:
                    RpcSetCrosshair(CrosshairState.Yellow);
                    break;
                case ControllerButtonInput.Grip:
                    RpcSetCrosshair(CrosshairState.Cyan);
                    break;
                default:
                    RpcSetCrosshair(CrosshairState.Magenta);
                    break;
            }

        }
    }
    private void ShowCanInteractFPS()
    {
        if (isLocalPlayer)
        {
            switch (ButtonToInteract)
            {
                case ControllerButtonInput.Any:
                    SetCrosshair(CrosshairState.Rainbow);
                    break;
                case ControllerButtonInput.Pad:
                    SetCrosshair(CrosshairState.Yellow);
                    break;
                case ControllerButtonInput.Grip:
                    SetCrosshair(CrosshairState.Cyan);
                    break;
                default:
                    SetCrosshair(CrosshairState.Magenta);
                    break;
            }
        }
        else
        {
            switch (ButtonToInteract)
            {
                case ControllerButtonInput.Any:
                    RpcSetCrosshair(CrosshairState.Rainbow);
                    break;
                case ControllerButtonInput.Pad:
                    RpcSetCrosshair(CrosshairState.Yellow);
                    break;
                case ControllerButtonInput.Grip:
                    RpcSetCrosshair(CrosshairState.Cyan);
                    break;
                default:
                    RpcSetCrosshair(CrosshairState.Magenta);
                    break;
            }
        }
    }

    internal CrosshairState GetCrosshairPublic()
    {
        return ForcedCrosshair;
    }
    void OfflineLeftInteractPressed(object sender, ClickedEventArgs e)
    {
        if (offlineraycasteditem != null && ItemLeftID == NetworkInstanceId.Invalid && !LeftBrochureEnabled && raycasteditem == null)
            offlineraycasteditem.GetComponent<VRUIElement>().Click(this, new ClickedEventArgs());
    }
    void OfflineRightInteractPressed(object sender, ClickedEventArgs e)
    {
       if (offlineraycasteditem != null && ItemRightID == NetworkInstanceId.Invalid && !RightBrochureEnabled && raycasteditem == null)
            offlineraycasteditem.GetComponent<VRUIElement>().Click(this, new ClickedEventArgs());
    }
    void InteractPressed(ControllerHand hand)
    {
        if (isServer)
        {
            if (((hand == ControllerHand.LeftHand && !LeftBrochureEnabled) || (hand == ControllerHand.RightHand && !RightBrochureEnabled)) && (raycasteditem != null || forcedtarget != null))
            {
                if (hand == ControllerHand.LeftHand && ItemLeft== null && !LeftInteracting)
                {
                    LeftInteracting = true;

                    if (raycastednpc)
                    {
                        ClickItem(raycasteditem, hand);
                        LeftInteracting = false;
                    }
                    else if (UIElement)
                    {
                        raycasteditem.GetComponent<VRUIElement>().Click(this, new ClickedEventArgs());
                        LeftInteracting = false;
                    }
                    else
                    {
                        var g = GetGenericItemNet(raycasteditem, raycasteditemlocid);
                        if (g != null)
                        {
                            if (g.Grabbable)
                                GrabItem(g, hand, false);
                            else
                            {
                                if (g.Slave != null)
                                    ClickItem(g.Slave.transform, hand);
                                else
                                    ClickItem(raycasteditem, hand);
                                LeftInteracting = false;
                            }
                        }
                        else
                        {
                            var gbs = GetGrabbableItemSlave(raycasteditem);
                            if (gbs != null)
                                GrabItem(gbs.MasterNet, hand);
                            else 
                            {
                                if (ItemLeft == null || forcedtarget != null)
                                    ClickItem(raycasteditem, hand);
                                LeftInteracting = false;
                            }
                        }
                    }
                }
                else if (hand == ControllerHand.RightHand && ItemRight == null && !RightInteracting)
                {
                    RightInteracting = true;

                    if (raycastednpc)
                    {
                        ClickItem(raycasteditem, hand);
                        RightInteracting = false;
                    }
                    else if (UIElement)
                    {
                        raycasteditem.GetComponent<VRUIElement>().Click(this, new ClickedEventArgs());
                        RightInteracting = false;
                    }
                    else
                    {
                        var g = GetGenericItemNet(raycasteditem, raycasteditemlocid);
                        if (g != null)
                        {
                            if (g.Grabbable)
                                GrabItem(g, hand, false);
                            else
                            {
                                if (g.Slave != null)
                                    ClickItem(g.Slave.transform, hand);
                                else
                                    ClickItem(raycasteditem, hand);
                                RightInteracting = false;
                            }
                        }
                        else
                        {
                            var gbs = GetGrabbableItemSlave(raycasteditem);
                            if (gbs != null)
                                GrabItem(gbs.MasterNet, hand);
                            else
                            {
                                if(ItemRight == null || forcedtarget != null)
                                    ClickItem(raycasteditem, hand);
                                RightInteracting = false;
                            }                            
                        }
                    }
                }
            }
        }
    }

    void LeftInteractPressed(object sender, ClickedEventArgs e)
    {
        InteractPressed(ControllerHand.LeftHand);
    }
    
    void RightInteractPressed(object sender, ClickedEventArgs e)
    {
        InteractPressed(ControllerHand.RightHand);
    }
    void LeftDropPressed(object sender, ClickedEventArgs e)
    {
        if (isServer)
        {
            if (ItemLeft != null && !LeftInteracting)
            {
                LeftInteracting = true;
                DropItem(ControllerHand.LeftHand, false);
            }
        }
    }
    void RightDropPressed(object sender, ClickedEventArgs e)
    {
        if (isServer)
        {
            if (ItemRight != null && !RightInteracting)
            {
                RightInteracting = true;
                DropItem(ControllerHand.RightHand, false);
            }
        }
    }
    #region ClickItem/NPC
    private void ClickItem(Transform i, ControllerHand hand)
    {
        if (isServer)
        {
            if (raycastednpc)
            {
                var n = i.GetComponentInParent<NPCController>();
                n.Interact(this, hand);
            }
            else
            {
                var gi = i.GetComponent<GenericItemNet>();
                if (gi != null)
                    gi.Interact(this, hand);
                else
                {
                    var gis = i.GetComponent<GenericItemSlave>();
                    if (gis != null)
                        gis.Interact(this, hand);
                }
            }
        }
    }
    #endregion
    public virtual void OnBrochureAvailableChanged(bool state)
    {
        if (!state && isServer)
        {
            if (LeftBrochureEnabled)
                ToggleBrochure(ControllerHand.LeftHand);
            else if (RightBrochureEnabled)
                ToggleBrochure(ControllerHand.RightHand);
        }
        BrochureAvailable = state;
        
    }

    public virtual void OnLeftBrochureChanged(bool state)
    {
        if(Type == ItemControllerType.VR)
        {
            if (state)
            {
                if (isServer)
                {
                    if (!RightBrochureEnabled && ItemLeft== null)
                    {
                        EnableItem(ItemCodes.Brochure, null, ControllerHand.LeftHand);
                    }
                }
                else
                    EnableItem(ItemCodes.Brochure, null, ControllerHand.LeftHand);
            }
            else
            {
                if (isServer)
                {
                    DisableItem(ControllerHand.LeftHand);
                }
                else
                    DisableItem(ControllerHand.LeftHand);
            }
        }
        else if (Type == ItemControllerType.FPS)
        {
            if (!state)
                FakeLeftHand.Find("Brochure").GetComponent<BrochureEffect>().DisableBrochure();
            else
                FakeLeftHand.Find("Brochure").GetComponent<BrochureEffect>().EnableBrochure();
        }
        LeftBrochureEnabled = state;
    }

    void OnRightBrochureChanged(bool state)
    {
        if (Type == ItemControllerType.VR)
        {
            if (state)
            {
                if (isServer)
                {
                    if (!LeftBrochureEnabled && ItemRight == null)
                    {
                        EnableItem(ItemCodes.Brochure, null, ControllerHand.RightHand);
                    }
                }
                else
                    EnableItem(ItemCodes.Brochure, null, ControllerHand.RightHand);
            }
            else
            {
                if (isServer)
                {
                    DisableItem(ControllerHand.RightHand);
                }
                else
                    DisableItem(ControllerHand.RightHand);
            }
        }
        else if (Type == ItemControllerType.FPS)
        {
            if (!state)
                FakeRightHand.Find("Brochure").GetComponent<BrochureEffect>().DisableBrochure();
            else
                FakeRightHand.Find("Brochure").GetComponent<BrochureEffect>().EnableBrochure();
        }
        RightBrochureEnabled = state;
    }
    public void SetBrochureAvailable(bool state)
    {
        if (isServer)
        {
            if(isClient)
                BrochureAvailable = state;
            else
                OnBrochureAvailableChanged(state);
        }
    }
    public void EnableLeftBrochure()
    {
        if (isServer)
        {
            if(isClient)
                LeftBrochureEnabled = true;
            else
                OnLeftBrochureChanged(true);
        }
    }
    public void EnableRightBrochure()
    {
        if (isServer)
        {
            if(isClient)
                RightBrochureEnabled = true;
            else
                OnRightBrochureChanged(true);
        }
    }
    public void DisableRightBrochure()
    {
        if (isServer)
        {
            if(isClient)
                RightBrochureEnabled = false;
            else
                OnRightBrochureChanged(false);
        }
    }
    public void DisableLeftBrochure()
    {
        if (isServer)
        {
            if(isClient)
                LeftBrochureEnabled = false;
            else
                OnLeftBrochureChanged(false);
        }
    }

    private void OnEnable()
    {
        if(Type == ItemControllerType.VR)
        {
            var c = GetComponents<ItemController>();
            foreach (ItemController i in c)
                i.enabled = false;
            var cn = GetComponents<ItemControllerNet>();
            foreach (ItemControllerNet i in cn)
                if (i != this)
                    i.enabled = false;
            var oc = GetComponent<OfflineItemController>();
            if (oc != null)
                oc.enabled = false;
            var cl = LeftController.GetComponent<ControllerManager>();
            if (cl != null)
            {
                cl.Controller = null;
                cl.ControllerNet = this;
                cl.OfflineController = null;
            }
            var cr = RightController.GetComponent<ControllerManager>();
            if (cr != null)
            {
                cr.Controller = null;
                cr.ControllerNet = this;
                cr.OfflineController = null;
            }
        }
    }

    private void Update()
    {
        if (!isServer)
        {
            if (grabrightlater)
            {
                if (ItemRightID == NetworkInstanceId.Invalid)
                    grableftlater = false;
                else if (AreItemsRightReady())
                {
                    OnRightItemIDChanged(ItemRightID);
                    grabrightlater = false;
                }
            }
            if (grableftlater)
            {
                if (ItemLeftID == NetworkInstanceId.Invalid)
                    grableftlater = false;
                else if (AreItemsRightReady())
                {
                    OnLeftItemIDChanged(ItemLeftID);
                    grableftlater = false;
                }
            }
        }
        if(Type == ItemControllerType.VR)
            PulseButton();
        else if (Type == ItemControllerType.FPS && !DeferredRaycast)
            DoRayCast();
    }
    private void LateUpdate()
    {
        Matrix4x4 parentMatrix;
        if (isServer)
        {
            if (ItemLeftID != NetworkInstanceId.Invalid || ItemRightID != NetworkInstanceId.Invalid)
            {
                if (ItemLeftID != NetworkInstanceId.Invalid && ItemLeft == null)
                {
                    var g = NetworkServer.FindLocalObject(ItemLeftID);
                    ItemLeft = g.transform;
                }
                if (ItemLeft != null)
                {
                    var g = ItemLeft.GetComponent<GenericItemNet>();
                    if (g != null)
                    {
                        if (g.SeekTarget != null)
                        {
                            parentMatrix = Matrix4x4.TRS(g.SeekTarget.position, g.SeekTarget.rotation, g.SeekTarget.lossyScale);
                            ItemLeft.transform.position = parentMatrix.MultiplyPoint3x4(g.startChildPosition);
                            ItemLeft.transform.rotation = (g.SeekTarget.rotation * Quaternion.Inverse(g.startParentRotationQ)) * g.startChildRotationQ;
                        }

                        if (!g.IsKinematic && (g.SeekTarget != null))
                        {
                            g.currvelocity = (g.transform.position - g.prevpos) / Time.deltaTime;
                            g.prevpos = g.transform.position;
                        }
                    }
                }
                if (ItemRightID != NetworkInstanceId.Invalid && ItemRight == null)
                {
                    var g = NetworkServer.FindLocalObject(ItemRightID);
                    ItemRight  = g.transform;
                }
                if (ItemRight != null)
                {
                    var g = ItemRight.GetComponent<GenericItemNet>();
                    if (g != null)
                    {
                        if (g.SeekTarget != null)
                        {
                            parentMatrix = Matrix4x4.TRS(g.SeekTarget.position, g.SeekTarget.rotation, g.SeekTarget.lossyScale);
                            ItemRight.transform.position = parentMatrix.MultiplyPoint3x4(g.startChildPosition);
                            ItemRight.transform.rotation = (g.SeekTarget.rotation * Quaternion.Inverse(g.startParentRotationQ)) * g.startChildRotationQ;
                        }

                        if (!g.IsKinematic && (g.SeekTarget != null))
                        {
                            g.currvelocity = (g.transform.position - g.prevpos) / Time.deltaTime;
                            g.prevpos = g.transform.position;
                        }
                    }
                }
            }
        }
        else
        {
            if (ItemLeftID != NetworkInstanceId.Invalid || ItemRightID != NetworkInstanceId.Invalid)
            {
                if (ItemLeftID != NetworkInstanceId.Invalid && ItemLeft== null)
                {
                    var g = ClientScene.FindLocalObject(ItemLeftID);
                    ItemLeft = g.transform;
                }
                if (ItemLeft != null)
                {
                    var g = ItemLeft.GetComponent<GenericItemNet>();
                    if (g != null)
                    {
                        if (g.SeekTarget != null)
                        {
                            ItemLeft.position = g.SeekTarget.position;
                            ItemLeft.rotation = g.SeekTarget.rotation;
                        }

                        if (!g.IsKinematic && (g.SeekTarget != null))
                        {
                            g.currvelocity = (g.transform.position - g.prevpos) / Time.deltaTime;
                            g.prevpos = g.transform.position;
                        }
                    }
                }
                if (ItemRightID != NetworkInstanceId.Invalid && ItemRight == null)
                {
                    var g = ClientScene.FindLocalObject(ItemRightID);
                    ItemRight = g.transform;
                }
                if (ItemRight != null)
                {
                    var g = ItemRight.GetComponent<GenericItemNet>();
                    if (g != null)
                    {
                        if (g.SeekTarget != null)
                        {
                            ItemRight.position = g.SeekTarget.position;
                            ItemRight.rotation = g.SeekTarget.rotation;
                        }

                        if (!g.IsKinematic && (g.SeekTarget != null))
                        {
                            g.currvelocity = (g.transform.position - g.prevpos) / Time.deltaTime;
                            g.prevpos = g.transform.position;
                        }
                    }
                }
            }
        }
    }

    void StartPulse(ControllerButton button)
    {
        if (isServer)
        {
            StartPulse(button, ControllerHand.LeftHand);
            StartPulse(button, ControllerHand.RightHand);
        }
        else
            CmdStartPulseBoth(button);
    }
    [Command]
    private void CmdStartPulseBoth(ControllerButton button)
    {
        StartPulse(button, ControllerHand.LeftHand);
        StartPulse(button, ControllerHand.RightHand);
    }
    void StartPulse(ControllerButton button, ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.RightHand)
            {
                if (button == ControllerButton.Nothing)
                    return;
                if (!rightpulsing)
                {
                    RightButtonToPulse = button;
                    StartPulse(hand);
                }
                else if (button != RightButtonToPulse)
                {
                    StopPulse(hand);
                    RightButtonToPulse = button;
                    StartPulse(hand);
                }
            }
            else if (hand == ControllerHand.LeftHand)
            {
                if (button == ControllerButton.Nothing)
                    return;
                if (!leftpulsing)
                {
                    LeftButtonToPulse = button;
                    StartPulse(hand);
                }
                else if (button != LeftButtonToPulse)
                {
                    StopPulse(hand);
                    LeftButtonToPulse = button;
                    StartPulse(hand);
                }
            }
        }
        else
            CmdStartPulse(button, hand);
    }
    [Command]
    private void CmdStartPulse(ControllerButton button, ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            if (button == ControllerButton.Nothing)
                return;
            if (!leftpulsing)
            {
                LeftButtonToPulse = button;
                StartPulse(hand);
            }
            else if (button != LeftButtonToPulse)
            {
                StopPulse();
                LeftButtonToPulse = button;
                StartPulse(hand);
            }
        }
        else if (hand == ControllerHand.RightHand)
        {
            if (button == ControllerButton.Nothing)
                return;
            if (!rightpulsing)
            {
                RightButtonToPulse = button;
                StartPulse(hand);
            }
            else if (button != RightButtonToPulse)
            {
                StopPulse();
                RightButtonToPulse = button;
                StartPulse(hand);
            }
        }
    }
    void StartPulse(ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (leftpulsing || (LeftButtonToPulse == ControllerButton.Nothing && ExternalLeftButtonToPulse == ControllerButton.Nothing))
                    return;

                if (isLocalPlayer)
                {
                    MeshRenderer r = null;
                    ControllerButton button = ControllerButton.Nothing;
                    if (LeftButtonToPulse != ControllerButton.Nothing)
                        button = LeftButtonToPulse;
                    else
                        button = ExternalLeftButtonToPulse;

                    var c = LeftController.Find("Model");
                    Transform t = null;
                    switch (button)
                    {
                        case ControllerButton.RGrip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if(r!= null)
                                    LGrips.Add(r);
                            }
                            break;
                        case ControllerButton.LGrip:
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Grip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Pad:
                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							}
                            break;
                        case ControllerButton.Trigger:
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LTriggers.Add(r);
                            }
                            break;
                        case ControllerButton.All:

                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							}
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LTriggers.Add(r);
                            }
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        default:
                            return;
                    }
                    if (oldmat == null)
                    {
                        if (LTriggers.Count > 0)
                            oldmat = LTriggers[0].materials[0];
                        else if (LPads.Count > 0)
                            oldmat = LPads[0].materials[0];
                        else if (LGrips.Count > 0)
                            oldmat = LGrips[0].materials[0];
                        else
                        {
                            var model = transform.Find("Model");
                            if (model != null)
                            {
                                var mr = model.GetComponent<MeshRenderer>();
                                if (mr != null)
                                    oldmat = mr.materials[0];
                            }
                        }
                    }
                    if (oldmat != null)
                    {
                        if (PadMaterial == null)
                        {
                            PadMaterial = new Material(Outline);
                            PadMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                            PadMaterial.SetColor("_OutlineColor", PadColor);
                            PadMaterial.SetFloat("_Outline", 0.03f);
                        }
                        if (GripMaterial == null)
                        {
                            GripMaterial = new Material(Outline);
                            GripMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                            GripMaterial.SetColor("_OutlineColor", GripColor);
                            GripMaterial.SetFloat("_Outline", 0.03f);
                        }
                        if (TriggerMaterial == null)
                        {
                            TriggerMaterial = new Material(Outline);
                            TriggerMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                            TriggerMaterial.SetColor("_OutlineColor", TriggerColor);
                            TriggerMaterial.SetFloat("_Outline", 0.03f);
                        }
                        foreach (MeshRenderer mr in LPads)
                        {
                            var m = new Material[1];
                            m[0] = PadMaterial;
                            mr.materials = m;
                        }
                        foreach (MeshRenderer mr in LTriggers)
                        {
                            var m = new Material[1];
                            m[0] = TriggerMaterial;
                            mr.materials = m;
                        }
                        foreach (MeshRenderer mr in LGrips)
                        {
                            var m = new Material[1];
                            m[0] = GripMaterial;
                            mr.materials = m;
                        }
                    }
                }
                if(isClient)
                    leftpulsing = true;
                else
                    OnLeftPulsingChanged(true);
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (rightpulsing || (RightButtonToPulse == ControllerButton.Nothing && ExternalRightButtonToPulse == ControllerButton.Nothing))
                    return;

                if (isLocalPlayer)
                {
                    MeshRenderer r = null;
                    ControllerButton button = ControllerButton.Nothing;
                    if (RightButtonToPulse != ControllerButton.Nothing)
                        button = RightButtonToPulse;
                    else
                        button = ExternalRightButtonToPulse;

                    var c = RightController.Find("Model");
                    Transform t = null;
                    switch (button)
                    {
                        case ControllerButton.RGrip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        case ControllerButton.LGrip:
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Grip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Pad:
                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							}
                            break;
                        case ControllerButton.Trigger:
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RTriggers.Add(r);
                            }
                            break;
                        case ControllerButton.All:

                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							}
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RTriggers.Add(r);
                            }
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        default:
                            return;
                    }
                    if (oldmat == null)
                    {
                        if (RTriggers.Count > 0)
                            oldmat = RTriggers[0].materials[0];
                        else if (RPads.Count > 0)
                            oldmat = RPads[0].materials[0];
                        else if (RGrips.Count > 0)
                            oldmat = RGrips[0].materials[0];
                        else
                        {
                            var model = transform.Find("Model");
                            if (model != null)
                            {
                                var mr = model.GetComponent<MeshRenderer>();
                                if (mr != null)
                                    oldmat = mr.materials[0];
                            }
                        }
                    }

                    if (PadMaterial == null)
                    {
                        PadMaterial = new Material(Outline);
                        PadMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        PadMaterial.SetColor("_OutlineColor", PadColor);
                        PadMaterial.SetFloat("_Outline", 0.03f);
                    }
                    if (GripMaterial == null)
                    {
                        GripMaterial = new Material(Outline);
                        GripMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        GripMaterial.SetColor("_OutlineColor", GripColor);
                        GripMaterial.SetFloat("_Outline", 0.03f);
                    }
                    if (TriggerMaterial == null)
                    {
                        TriggerMaterial = new Material(Outline);
                        TriggerMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        TriggerMaterial.SetColor("_OutlineColor", TriggerColor);
                        TriggerMaterial.SetFloat("_Outline", 0.03f);
                    }
                    foreach (MeshRenderer mr in RPads)
                    {
                        var m = new Material[1];
                        m[0] = PadMaterial;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RTriggers)
                    {
                        var m = new Material[1];
                        m[0] = TriggerMaterial;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RGrips)
                    {
                        var m = new Material[1];
                        m[0] = GripMaterial;
                        mr.materials = m;
                    }
                }
                if(isClient)
                    rightpulsing = true;
                else
                    OnRightPulsingChanged(true);
            }
        }
        else
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (leftpulsing || (LeftButtonToPulse == ControllerButton.Nothing && ExternalLeftButtonToPulse == ControllerButton.Nothing))
                    return;

                if (isLocalPlayer)
                {
                    MeshRenderer r = null;
                    ControllerButton button = ControllerButton.Nothing;
                    if (LeftButtonToPulse != ControllerButton.Nothing)
                        button = LeftButtonToPulse;
                    else
                        button = ExternalLeftButtonToPulse;

                    var c = LeftController.Find("Model");
                    Transform t = null;
                    switch (button)
                    {
                        case ControllerButton.RGrip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        case ControllerButton.LGrip:
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Grip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Pad:
                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							}
                            break;
                        case ControllerButton.Trigger:
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LTriggers.Add(r);
                            }
                            break;
                        case ControllerButton.All:

                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LPads.Add(r);
                            }
							}
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LTriggers.Add(r);
                            }
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    LGrips.Add(r);
                            }
                            break;
                        default:
                            return;
                    }
                    if (oldmat == null)
                    {
                        if (LTriggers.Count > 0)
                            oldmat = LTriggers[0].materials[0];
                        else if (LPads.Count > 0)
                            oldmat = LPads[0].materials[0];
                        else if (LGrips.Count > 0)
                            oldmat = LGrips[0].materials[0];
                        else
                        {
                            var model = transform.Find("Model");
                            if (model != null)
                            {
                                var mr = model.GetComponent<MeshRenderer>();
                                if (mr != null)
                                    oldmat = mr.materials[0];
                            }
                        }
                    }

                    if (PadMaterial == null)
                    {
                        PadMaterial = new Material(Outline);
                        PadMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        PadMaterial.SetColor("_OutlineColor", PadColor);
                        PadMaterial.SetFloat("_Outline", 0.03f);
                    }
                    if (GripMaterial == null)
                    {
                        GripMaterial = new Material(Outline);
                        GripMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        GripMaterial.SetColor("_OutlineColor", GripColor);
                        GripMaterial.SetFloat("_Outline", 0.03f);
                    }
                    if (TriggerMaterial == null)
                    {
                        TriggerMaterial = new Material(Outline);
                        TriggerMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        TriggerMaterial.SetColor("_OutlineColor", TriggerColor);
                        TriggerMaterial.SetFloat("_Outline", 0.03f);
                    }
                    foreach (MeshRenderer mr in LPads)
                    {
                        var m = new Material[1];
                        m[0] = PadMaterial;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in LTriggers)
                    {
                        var m = new Material[1];
                        m[0] = TriggerMaterial;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in LGrips)
                    {
                        var m = new Material[1];
                        m[0] = GripMaterial;
                        mr.materials = m;
                    }
                }
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (rightpulsing || (RightButtonToPulse == ControllerButton.Nothing && ExternalRightButtonToPulse == ControllerButton.Nothing))
                    return;

                if (isLocalPlayer)
                {
                    MeshRenderer r = null;
                    ControllerButton button = ControllerButton.Nothing;
                    if (RightButtonToPulse != ControllerButton.Nothing)
                        button = RightButtonToPulse;
                    else
                        button = ExternalRightButtonToPulse;

                    var c = RightController.Find("Model");
                    Transform t = null;
                    switch (button)
                    {
                        case ControllerButton.RGrip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        case ControllerButton.LGrip:
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Grip:
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        case ControllerButton.Pad:
                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							}
                            break;
                        case ControllerButton.Trigger:
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RTriggers.Add(r);
                            }
                            break;
                        case ControllerButton.All:

                            t = c.Find("trackpad");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							else
							{
                            t = c.Find("thumbstick");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RPads.Add(r);
                            }
							}
                            t = c.Find("trigger");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RTriggers.Add(r);
                            }
                            t = c.Find("rgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("lgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            t = c.Find("handgrip");
                            if (t != null)
                            {
                                r = t.GetComponent<MeshRenderer>();
                                if (r != null)
                                    RGrips.Add(r);
                            }
                            break;
                        default:
                            return;
                    }
                    if (oldmat == null)
                    {
                        if (RTriggers.Count > 0)
                            oldmat = RTriggers[0].materials[0];
                        else if (RPads.Count > 0)
                            oldmat = RPads[0].materials[0];
                        else if (RGrips.Count > 0)
                            oldmat = RGrips[0].materials[0];
                        else
                        {
                            var model = transform.Find("Model");
                            if (model != null)
                            {
                                var mr = model.GetComponent<MeshRenderer>();
                                if (mr != null)
                                    oldmat = mr.materials[0];
                            }
                        }
                    }

                    if (PadMaterial == null)
                    {
                        PadMaterial = new Material(Outline);
                        PadMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        PadMaterial.SetColor("_OutlineColor", PadColor);
                        PadMaterial.SetFloat("_Outline", 0.03f);
                    }
                    if (GripMaterial == null)
                    {
                        GripMaterial = new Material(Outline);
                        GripMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        GripMaterial.SetColor("_OutlineColor", GripColor);
                        GripMaterial.SetFloat("_Outline", 0.03f);
                    }
                    if (TriggerMaterial == null)
                    {
                        TriggerMaterial = new Material(Outline);
                        TriggerMaterial.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
                        TriggerMaterial.SetColor("_OutlineColor", TriggerColor);
                        TriggerMaterial.SetFloat("_Outline", 0.03f);
                    }
                    foreach (MeshRenderer mr in RPads)
                    {
                        var m = new Material[1];
                        m[0] = PadMaterial;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RTriggers)
                    {
                        var m = new Material[1];
                        m[0] = TriggerMaterial;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RGrips)
                    {
                        var m = new Material[1];
                        m[0] = GripMaterial;
                        mr.materials = m;
                    }
                }
            }
        }
    }
    void StopPulse()
    {
        if (isServer)
        {
            StopPulse(ControllerHand.LeftHand);
            StopPulse(ControllerHand.RightHand);
        }
        else
            CmdStopPulse();
    }
    [Command]
    private void CmdStopPulse()
    {
        StopPulse(ControllerHand.LeftHand);
        StopPulse(ControllerHand.RightHand);
    }
    void StopPulse(ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (!leftpulsing)
                    return;
                if (ExternalLeftButtonToPulse != ControllerButton.Nothing && ExternalLeftButtonToPulse == LeftButtonToPulse)
                {
                    LeftButtonToPulse = ControllerButton.Nothing;
                    return;
                }

                if (isLocalPlayer)
                {
                    foreach (MeshRenderer mr in LTriggers)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in LGrips)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in LPads)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    LTriggers.Clear();
                    LPads.Clear();
                    LGrips.Clear();
                }
                LeftButtonToPulse = ControllerButton.Nothing;
                if(isClient)
                    leftpulsing = false;
                else
                    OnLeftPulsingChanged(false);
                if (ExternalLeftButtonToPulse != ControllerButton.Nothing)
                    StartPulse(ControllerHand.LeftHand);
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (!rightpulsing)
                    return;
                if (ExternalRightButtonToPulse != ControllerButton.Nothing && ExternalRightButtonToPulse == RightButtonToPulse)
                {
                    RightButtonToPulse = ControllerButton.Nothing;
                    return;
                }

                if (isLocalPlayer)
                {
                    foreach (MeshRenderer mr in RTriggers)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RGrips)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RPads)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    RTriggers.Clear();
                    RPads.Clear();
                    RGrips.Clear();
                }
                RightButtonToPulse = ControllerButton.Nothing;
                if(isClient)
                    rightpulsing = false;
                else
                    OnRightPulsingChanged(false);

                if (ExternalRightButtonToPulse != ControllerButton.Nothing)
                    StartPulse(ControllerHand.RightHand);
            }
        }
        else
        {
            if (hand == ControllerHand.LeftHand)
            {
                if (!leftpulsing)
                    return;
                if (ExternalLeftButtonToPulse != ControllerButton.Nothing && ExternalLeftButtonToPulse == LeftButtonToPulse)
                {
                    return;
                }

                if (isLocalPlayer)
                {
                    foreach (MeshRenderer mr in LTriggers)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in LGrips)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in LPads)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    LTriggers.Clear();
                    LPads.Clear();
                    LGrips.Clear();
                }
                if (ExternalLeftButtonToPulse != ControllerButton.Nothing)
                    StartPulse(ControllerHand.LeftHand);
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (!rightpulsing)
                    return;
                if (ExternalRightButtonToPulse != ControllerButton.Nothing && ExternalRightButtonToPulse == RightButtonToPulse)
                {
                    return;
                }

                if (isLocalPlayer)
                {
                    foreach (MeshRenderer mr in RTriggers)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RGrips)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    foreach (MeshRenderer mr in RPads)
                    {
                        var m = new Material[1];
                        m[0] = oldmat;
                        mr.materials = m;
                    }
                    RTriggers.Clear();
                    RPads.Clear();
                    RGrips.Clear();
                }
                if (ExternalRightButtonToPulse != ControllerButton.Nothing)
                    StartPulse(ControllerHand.RightHand);
            }
        }

    }
    void PulseButton()
    {
        if (isLocalPlayer)
        {
            if (LeftButtonToPulse != ControllerButton.Nothing || RightButtonToPulse != ControllerButton.Nothing || ExternalRightButtonToPulse != ControllerButton.Nothing || ExternalLeftButtonToPulse != ControllerButton.Nothing)
            {
                t += Time.deltaTime / PulseTime;

                if (leftpulsing)
                {
                    foreach (MeshRenderer mr in LGrips)
                    {
                        if (up)
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                        else
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                    }
                    foreach (MeshRenderer mr in LTriggers)
                    {
                        if (up)
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                        else
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                    }
                    foreach (MeshRenderer mr in LPads)
                    {
                        if (up)
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                        else
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                    }
                }
                if (rightpulsing)
                {
                    foreach (MeshRenderer mr in RGrips)
                    {
                        if (up)
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                        else
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                    }
                    foreach (MeshRenderer mr in RTriggers)
                    {
                        if (up)
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                        else
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                    }
                    foreach (MeshRenderer mr in RPads)
                    {
                        if (up)
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                        else
                        {
                            var m1 = mr.materials[0];
                            var c = m1.GetColor("_OutlineColor");
                            m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                            var m = new Material[1];
                            m[0] = m1;
                            mr.materials = m;
                        }
                    }
                }
                if (t >= 1)
                {
                    up = true;
                    t = 0;
                }
            }
        }
    }

    private GrabbableItemSlave GetGrabbableItemSlave(Transform item)
    {
        GrabbableItemSlave g = null;
        if (item != null && item.parent != null)
            g = item.parent.GetComponent<GrabbableItemSlave>();
        return g;
    }

    public void ManageTriggerEnter(Collider other, ControllerHand hand)
    {
        if (other == null)
            return;
        if (isServer)
        {
            var o = other.transform;
            if (hand == ControllerHand.LeftHand && ItemLeft == null)
            {
                if (o.tag == "Item" && toucheditemleft == null && !LeftBrochureEnabled)
                {
                    var g = o.GetComponent<GenericItemNet>();
                    if (g != null)
                        VRInteract(g, hand);
                    else
                    {
                        var gs = o.GetComponent<GenericItemSlave>();
                        if (gs != null)
                            VRInteract(gs, hand);
                        else
                        {
                            var gbs = GetGrabbableItemSlave(o);
                            if (gbs != null)
                                VRInteract(gbs, hand);
                            else
                                Debug.Log("No usable component on Item-tagged object: " + o.name);
                        }
                    }
                }
                else if (o.tag == "NPC" && toucheditemleft == null && o != toucheditemright && !LeftBrochureEnabled)
                {
                    var n = o.GetComponentInParent<NPCController>();
                    if (n == null)
                        Debug.Log("No NPC Controller component on NPC-tagged object: " + o.name);
                    else
                        VRInteract(n, hand);
                }
            }
            else if (hand == ControllerHand.RightHand && ItemRight == null)
            {
                if (o.tag == "Item" && toucheditemright == null && !RightBrochureEnabled)
                {
                    var g = o.GetComponent<GenericItemNet>();
                    if (g != null)
                        VRInteract(g, hand);
                    else
                    {
                        var gs = o.GetComponent<GenericItemSlave>();
                        if (gs != null)
                            VRInteract(gs, hand);
                        else
                        {
                            var gbs = GetGrabbableItemSlave(o);
                            if (gbs != null)
                                VRInteract(gbs, hand);
                            else
                                Debug.Log("No usable component on Item-tagged object: " + o.name);
                        }
                    }
                }
                else if (o.tag == "NPC" && toucheditemright == null && o != toucheditemleft && !RightBrochureEnabled)
                {
                    var n = o.GetComponentInParent<NPCController>();
                    if (n == null)
                        Debug.Log("No NPC Controller component on NPC-tagged object: " + o.name);
                    else
                        VRInteract(n, hand);                    
                }
            }
        }
    }

    private void VRInteract(NPCController n, ControllerHand hand)
    {
        if (n != null && n.CanInteract())
        {
            SetTouchedItem(n, hand);
            n.EnableOutline();
            ShowCanInteractVR(hand);
        }
    }

    private void VRInteract(GrabbableItemSlave gbs, ControllerHand hand)
    {
        if (gbs != null)
        {
            if (hand == ControllerHand.LeftHand && toucheditemleft == null)
            {
                SetTouchedItem(gbs, hand);
                if(!isClient)
                    gbs.EnableOutline(this);
                RpcEnableOutline(gbs.MasterNet.netId);
                ShowCanInteractVR(hand);
            }
            else if (hand == ControllerHand.RightHand && toucheditemright == null)
            {
                SetTouchedItem(gbs, hand);
                if (!isClient)
                    gbs.EnableOutline(this);
                RpcEnableOutline(gbs.MasterNet.netId);
                ShowCanInteractVR(hand);
            }
        }
    }

    private void VRInteract(GenericItemSlave gs, ControllerHand hand)
    {
        if (gs != null && gs.MasterNet.CanInteract(this))
        {
            if (hand == ControllerHand.LeftHand && toucheditemleft == null)
            {
                SetTouchedItem(gs, hand);
                gs.MasterNet.Controller = LeftController.GetComponent<ControllerManager>();
                gs.Select();
                ShowCanInteractVR(hand);
            }
            else if (hand == ControllerHand.RightHand && toucheditemright == null)
            {
                SetTouchedItem(gs, hand);
                gs.MasterNet.Controller = RightController.GetComponent<ControllerManager>();
                gs.Select();
                ShowCanInteractVR(hand);
            }
        }
    }

    private void VRInteract(GenericItemNet g, ControllerHand hand)
    {
        if (g != null && g.CanInteract(this))
        {
            if(hand == ControllerHand.LeftHand && toucheditemleft == null)
            {
                SetTouchedItem(g, hand);
                g.Controller = LeftController.GetComponent<ControllerManager>();
                g.Select();
                ShowCanInteractVR(hand);

            }
            else if (hand == ControllerHand.RightHand && toucheditemright == null)
            {
                SetTouchedItem(g, hand);
                g.Controller = RightController.GetComponent<ControllerManager>();
                g.Select();
                ShowCanInteractVR(hand);

            }
        }
    }

    private void ShowCanInteractVR(ControllerHand hand)
    {
        switch (ButtonToInteract)
        {
            case ControllerButtonInput.Pad:
                StartPulse(ControllerButton.Pad, hand);
                break;
            case ControllerButtonInput.Grip:
                StartPulse(ControllerButton.Grip, hand);
                break;
            case ControllerButtonInput.Any:
                StartPulse(ControllerButton.All, hand);
                break;
            default:
                StartPulse(ControllerButton.Trigger, hand);
                break;
        }
        RpcShortVibration(hand);
    }

    [ClientRpc]
    private void RpcShortVibration(ControllerHand hand)
    {
        if (isLocalPlayer && Type == ItemControllerType.VR)
        {
            if (hand == ControllerHand.LeftHand)
                LeftVibrationController.ShortVibration();
            else if (hand == ControllerHand.RightHand)
                RightVibrationController.ShortVibration();
        }

    }
    public void ManageTriggerExit(Collider other, ControllerHand hand)
    {
        if (other == null)
            return;
        if (isServer)
        {
            var o = other.transform;
            if (hand == ControllerHand.LeftHand)
            {
                if (o.tag == "Item")
                {
                    GenericItemNet g = o.GetComponent<GenericItemNet>();
                    if (g != null)
                    {
                        if(g != toucheditemleft || (g.Slave != null && g.Slave != toucheditemleft))
                        {
                            g.Unselect();
                            g.Controller = null;
                        }
                    }
                    else
                    {
                        GenericItemSlave gs = o.GetComponent<GenericItemSlave>();
                        if (gs != null && gs != toucheditemleft)
                        {
                            gs.Unselect();
                            gs.MasterNet.Controller = null;
                        }
                        else  if (o != toucheditemleft)
                        {
                            var gbs = GetGrabbableItemSlave(o);
                            if (gbs != null)
                            {
                                if (!isClient)
                                    gbs.DisableOutline(this);
                                RpcDisableOutline(gbs.MasterNet.netId);
                            }
                        }
                    }
                    StopPulse(hand);
                    ReSetTouchedItem(hand, false);
                }
                else if (o.tag == "NPC" && toucheditemleft == o)
                {
                    o.GetComponentInParent<NPCController>().DisableOutline();
                    StopPulse(hand);
                    ReSetTouchedItem(hand, false);
                }
            }
            else if (hand == ControllerHand.RightHand)
            {
                if (o.tag == "Item" && toucheditemright == o)
                {
                    GenericItemNet g = o.GetComponent<GenericItemNet>();
                    if (g != null)
                    {
                        if (g != toucheditemright || (g.Slave != null && g.Slave != toucheditemright))
                        {
                            g.Unselect();
                            g.Controller = null;
                        }
                    }
                    else
                    {
                        GenericItemSlave gs = o.GetComponent<GenericItemSlave>();
                        if (gs != null && gs != toucheditemright)
                        {
                            gs.Unselect();
                            gs.MasterNet.Controller = null;
                        }
                        else if (o != toucheditemright)
                        {
                            var gbs = GetGrabbableItemSlave(o);
                            if (gbs != null)
                            {
                                if (!isClient)
                                    gbs.DisableOutline(this);
                                RpcDisableOutline(gbs.MasterNet.netId);
                            }
                        }
                    }
                    StopPulse(hand);
                    ReSetTouchedItem(hand, false);
                }
                else if (o.tag == "NPC" && toucheditemright == o)
                {
                    o.GetComponentInParent<NPCController>().DisableOutline();
                    StopPulse(hand);
                    ReSetTouchedItem(hand, false);
                }
            }
        }
    }
    
    [ClientRpc]
    private void RpcDisableOutline(NetworkInstanceId netid)
    {
        GrabbableItemSlave gbs = null;
        if (isServer)
            gbs = NetworkServer.FindLocalObject(netid).GetComponent<GrabbableItemNet>().Slave;
        else
            gbs = ClientScene.FindLocalObject(netid).GetComponent<GrabbableItemNet>().Slave;
        gbs.DisableOutline(this);
    }
    [ClientRpc]
    private void RpcEnableOutline(NetworkInstanceId netid)
    {
        GrabbableItemSlave gbs = null;
        if (isServer)
            gbs = NetworkServer.FindLocalObject(netid).GetComponent<GrabbableItemNet>().Slave;
        else
            gbs = ClientScene.FindLocalObject(netid).GetComponent<GrabbableItemNet>().Slave;
        gbs.EnableOutline(this);
    }
    private void LeftInteract(object sender, ClickedEventArgs e)
    {
        if (isServer)
        {
            if (!leftoperating)
            {
                leftoperating = true;
                if (GrabbingMethod == GrabMode.ClickButton && ItemLeft != null && !LeftInteracting)
                    {
                    LeftInteracting = true;
					leftoperating = false;
                    DropItem(ControllerHand.LeftHand, false);
                    return;
                    }
                else if (!LeftBrochureEnabled && toucheditemleft != null)
                {
                    if (ItemLeft== null && !LeftInteracting)
                    {
                        LeftInteracting = true;
                        var g = GetGenericItemNet(toucheditemleft, toucheditemleftlocid);
                        if (g != null)
                        {
                            if (g.Grabbable)
                            {
                                GrabItem(g, ControllerHand.LeftHand, false);
                                return;
                            }
                            else
                            {
                                ClickItem(g, ControllerHand.LeftHand);
                                LeftInteracting = false;
                            }
                        }
                        else
                        {
                                GrabbableItemSlave gbs = GetGrabbableItemSlave(toucheditemleft);
                                if (gbs != null)
                                    GrabItem(gbs.MasterNet, ControllerHand.LeftHand);
                                else
                                    LeftInteracting = false;
                        }
                    }
                    else
                    {
                        var i = toucheditemleft.GetComponentInParent<NPCController>();
                        if (i != null)
                            ClickItem(i, ControllerHand.LeftHand);
                    }
                }
                    leftoperating = false;
            }
        }
    }
    private void RightInteract(object sender, ClickedEventArgs e)
    {
        if (isServer)
        {
            if (!rightoperating)
            {
                rightoperating = true;
                if (GrabbingMethod == GrabMode.ClickButton && ItemRight != null && !RightInteracting)
                    {
                    RightInteracting = true;
					rightoperating = false;
                    DropItem(ControllerHand.RightHand, false);
                    return;
                    }
                else if (!RightBrochureEnabled && toucheditemright != null)
                {
                    if (ItemRight == null && !RightInteracting)
                    {
                        RightInteracting = true;
                        var g = GetGenericItemNet(toucheditemright, toucheditemrightlocid);
                        if (g != null)
                        {
                            if (g.Grabbable)
                            {
                                GrabItem(g, ControllerHand.RightHand, false);
                                return;
                            }
                            else
                            {
                                ClickItem(g, ControllerHand.RightHand);
                                RightInteracting = false;
                            }
                        }
                        else
                        {
                                GrabbableItemSlave gbs = GetGrabbableItemSlave(toucheditemright);
                                if (gbs != null)
                                    GrabItem(gbs.MasterNet, ControllerHand.RightHand);
                                else
                                    RightInteracting = false;
                        }
                    }
                    else
                    {
                        var i = toucheditemright.GetComponentInParent<NPCController>();
                        if (i != null)
                            ClickItem(i, ControllerHand.RightHand);
                    }
                }
            rightoperating = false;
            }
        }
    }
    #region ClickItem/NPC
    private void ClickItem(GenericItemSlave i, ControllerHand hand)
    {
        if (isServer)
        {
            i.Interact(this, hand);
            if (isLocalPlayer)
            {
                if (hand == ControllerHand.LeftHand)
                    LeftVibrationController.ShortVibration();
                else if (hand == ControllerHand.RightHand)
                    RightVibrationController.ShortVibration();
            }
            if (hand == ControllerHand.RightHand)
                RightInteracting = false;
            else if (hand == ControllerHand.LeftHand)
                LeftInteracting = false;
        }
    }
    private void ClickItem(GenericItemNet i, ControllerHand hand)
    {
        if (isServer)
        {
            i.Interact(this, hand);
            RpcShortVibration(hand);

            if (hand == ControllerHand.RightHand)
                RightInteracting = false;
            else if (hand == ControllerHand.LeftHand)
                LeftInteracting = false;
        }
    }
    private void ClickItem(NPCController i, ControllerHand hand)
    {
        if (isServer)
        {
            i.Interact(this, hand);
            RpcShortVibration(hand);

            if (hand == ControllerHand.RightHand)
                RightInteracting = false;
            else if (hand == ControllerHand.LeftHand)
                LeftInteracting = false;
        }
    }
    #endregion

    public void StartPulsePublic(ControllerButton button)
    {
        if (isServer)
        {
            ExternalLeftButtonToPulse = button;
            ExternalRightButtonToPulse = button;
            if (LeftButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.LeftHand);
            if (RightButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.RightHand);
        }
        else
            CmdStartPulsePublicBoth(button);
    }

    private void GrabItem(GrabbableItemNet grabbeditem, ControllerHand hand)
    {
        StopPulse(hand);
        var item = grabbeditem.Item;
        if (item == null)
            return;
        otherplayer = grabbeditem.Item.Player;
        if (grabbeditem.Item.Player == null)
            return; //servono per quando si continua a passarsi l'oggetto da una mano all'altra e mentre si passa lo si fa cadere
        if (hand == ControllerHand.LeftHand)
            itemlefttograb = item;
        else if (hand == ControllerHand.RightHand)
            itemrighttograb = item;
        if(otherplayer.ItemLeft == item.transform || (item.Slave != null && otherplayer.ItemLeft == item.Slave.transform))
        {
            otherplayerhand = ControllerHand.LeftHand;
            if (hand == ControllerHand.LeftHand)
                otherplayer.OnLeftInteractionFinished += GrabLeftLater;
            else if (hand == ControllerHand.RightHand)
                otherplayer.OnLeftInteractionFinished += GrabRightLater;
        }
        else if(otherplayer.ItemRight == item.transform || (item.Slave != null && otherplayer.ItemRight == item.Slave.transform))
        {
            otherplayerhand = ControllerHand.RightHand;
            if (hand == ControllerHand.LeftHand)
                otherplayer.OnRightInteractionFinished += GrabLeftLater;
            else if (hand == ControllerHand.RightHand)
                otherplayer.OnRightInteractionFinished += GrabRightLater;
        }
        otherplayer.DropItem(item.transform, true);
       
    }
    private void GrabRightLater(object sender)
    {
        if(itemrighttograb != null)
            GrabLater(itemrighttograb, ControllerHand.RightHand);
    }

    private void GrabLeftLater(object sender)
    {
        if(itemlefttograb != null)
            GrabLater(itemlefttograb, ControllerHand.LeftHand);
    }

    private void GrabLater(GenericItemNet g, ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            SetTouchedItem(g, hand);
            if (otherplayerhand == ControllerHand.LeftHand)
                otherplayer.OnLeftInteractionFinished -= GrabLeftLater;
            else if (otherplayerhand == ControllerHand.RightHand)
                OnRightInteractionFinished -= GrabLeftLater;
            otherplayer = null;
            itemlefttograb = null;
            GrabItem(g, hand, true);
        }
        else if (hand == ControllerHand.RightHand)
        {
            SetTouchedItem(g, hand);
            if (otherplayerhand == ControllerHand.LeftHand)
                otherplayer.OnLeftInteractionFinished -= GrabRightLater;
            else if (otherplayerhand == ControllerHand.RightHand)
                OnRightInteractionFinished -= GrabRightLater;
            otherplayer = null;
            itemrighttograb = null;
            GrabItem(g, hand, true);
        }
    }

    [Command]
    private void CmdStartPulsePublicBoth(ControllerButton button)
    {
        ExternalLeftButtonToPulse = button;
        ExternalRightButtonToPulse = button;
        if (LeftButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.LeftHand);
        if (RightButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.RightHand);
    }
    public void StartPulsePublic(ControllerButtonByFunc button)
    {
        switch (button)
        {
            case ControllerButtonByFunc.ButtonToInteract:
                ExternalLeftButtonToPulse = GetButton(ButtonToInteract);
                ExternalRightButtonToPulse = GetButton(ButtonToInteract);
                break;
            case ControllerButtonByFunc.ButtonToDrop:
                ExternalLeftButtonToPulse = GetButton(ButtonToDrop);
                ExternalRightButtonToPulse = GetButton(ButtonToDrop);
                break;
            case ControllerButtonByFunc.ButtonToUse:
                ExternalLeftButtonToPulse = GetButton(ButtonToUse);
                ExternalRightButtonToPulse = GetButton(ButtonToUse);
                break;
            case ControllerButtonByFunc.All:
                ExternalLeftButtonToPulse = ControllerButton.All;
                ExternalRightButtonToPulse = ControllerButton.All;
                break;
            default:
                ExternalLeftButtonToPulse = ControllerButton.Nothing;
                ExternalRightButtonToPulse = ControllerButton.Nothing;
                break;
        }
        if (LeftButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.LeftHand);
        if (RightButtonToPulse == ControllerButton.Nothing)
            StartPulse(ControllerHand.RightHand);
    }
    public void StartPulsePublic(ControllerButton button, ControllerHand hand)
    {
        if (isServer)
        {

            if (hand == ControllerHand.LeftHand)
            {
                ExternalLeftButtonToPulse = button;
                if (LeftButtonToPulse == ControllerButton.Nothing)
                    StartPulse(ControllerHand.LeftHand);
            }
            else if (hand == ControllerHand.RightHand)
            {
                ExternalRightButtonToPulse = button;
                if (RightButtonToPulse == ControllerButton.Nothing)
                    StartPulse(ControllerHand.RightHand);

            }
        }
        else
            CmdStartPulsePublic(button, hand);
    }
    [Command]
    private void CmdStartPulsePublic(ControllerButton button, ControllerHand hand)
    {

        if (hand == ControllerHand.LeftHand)
        {
            ExternalLeftButtonToPulse = button;
            if (LeftButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.LeftHand);
        }
        else if (hand == ControllerHand.RightHand)
        {
            ExternalRightButtonToPulse = button;
            if (RightButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.RightHand);

        }
    }
    public void StartPulsePublic(ControllerButtonByFunc button, ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            switch (button)
            {
                case ControllerButtonByFunc.ButtonToInteract:
                    ExternalLeftButtonToPulse = GetButton(ButtonToInteract);
                    break;
                case ControllerButtonByFunc.ButtonToDrop:
                    ExternalLeftButtonToPulse = GetButton(ButtonToDrop);
                    break;
                case ControllerButtonByFunc.ButtonToUse:
                    ExternalLeftButtonToPulse = GetButton(ButtonToUse);
                    break;
                case ControllerButtonByFunc.All:
                    ExternalLeftButtonToPulse = ControllerButton.All;
                    break;
                default:
                    ExternalLeftButtonToPulse = ControllerButton.Nothing;
                    break;
            }
            if (LeftButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.LeftHand);
        }
        else if (hand == ControllerHand.RightHand)
        {
            switch (button)
            {
                case ControllerButtonByFunc.ButtonToInteract:
                    ExternalRightButtonToPulse = GetButton(ButtonToInteract);
                    break;
                case ControllerButtonByFunc.ButtonToDrop:
                    ExternalRightButtonToPulse = GetButton(ButtonToDrop);
                    break;
                case ControllerButtonByFunc.ButtonToUse:
                    ExternalRightButtonToPulse = GetButton(ButtonToUse);
                    break;
                case ControllerButtonByFunc.All:
                    ExternalRightButtonToPulse = ControllerButton.All;
                    break;
                default:
                    ExternalRightButtonToPulse = ControllerButton.Nothing;
                    break;
            }
            if (RightButtonToPulse == ControllerButton.Nothing)
                StartPulse(ControllerHand.RightHand);
        }
    }
    public void StopPulsePublic()
    {
        if (isServer)
        {
            ExternalLeftButtonToPulse = ControllerButton.Nothing;
            ExternalRightButtonToPulse = ControllerButton.Nothing;
            ControllerButton button = ControllerButton.Nothing;
            if (LeftButtonToPulse != ControllerButton.Nothing)
            {
                button = LeftButtonToPulse;
                StopPulse(ControllerHand.LeftHand);
                StartPulse(button, ControllerHand.LeftHand);
            }
            else
                StopPulse(ControllerHand.LeftHand);
            if (RightButtonToPulse != ControllerButton.Nothing)
            {
                button = RightButtonToPulse;
                StopPulse(ControllerHand.RightHand);
                StartPulse(button, ControllerHand.RightHand);
            }
            else
                StopPulse();
        }
        else
            CmdStopPulsePublicBoth();
    }
    [Command]
    private void CmdStopPulsePublicBoth()
    {
        ExternalLeftButtonToPulse = ControllerButton.Nothing;
        ExternalRightButtonToPulse = ControllerButton.Nothing;
        ControllerButton button = ControllerButton.Nothing;
        if (LeftButtonToPulse != ControllerButton.Nothing)
        {
            button = LeftButtonToPulse;
            StopPulse(ControllerHand.LeftHand);
            StartPulse(button, ControllerHand.LeftHand);
        }
        else
            StopPulse(ControllerHand.LeftHand);
        if (RightButtonToPulse != ControllerButton.Nothing)
        {
            button = RightButtonToPulse;
            StopPulse(ControllerHand.RightHand);
            StartPulse(button, ControllerHand.RightHand);
        }
        else
            StopPulse();
    }
    public void StopPulsePublic(ControllerHand hand)
    {
        if (isServer)
        {
            if (hand == ControllerHand.LeftHand)
            {
                ExternalLeftButtonToPulse = ControllerButton.Nothing;
                ControllerButton button = ControllerButton.Nothing;
                if (LeftButtonToPulse != ControllerButton.Nothing)
                {
                    button = LeftButtonToPulse;
                    StopPulse(ControllerHand.LeftHand);
                    StartPulse(button, ControllerHand.LeftHand);
                }
                else
                    StopPulse(ControllerHand.LeftHand);
            }
            else if (hand == ControllerHand.RightHand)
            {
                ExternalRightButtonToPulse = ControllerButton.Nothing;
                ControllerButton button = ControllerButton.Nothing;
                if (RightButtonToPulse != ControllerButton.Nothing)
                {
                    button = RightButtonToPulse;
                    StopPulse(ControllerHand.RightHand);
                    StartPulse(button, ControllerHand.RightHand);
                }
                else
                    StopPulse();
            }
        }
        else
            CmdStopPulsePublic(hand);
    }
    [Command]
    private void CmdStopPulsePublic(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
        {
            ExternalLeftButtonToPulse = ControllerButton.Nothing;
            ControllerButton button = ControllerButton.Nothing;
            if (LeftButtonToPulse != ControllerButton.Nothing)
            {
                button = LeftButtonToPulse;
                StopPulse(ControllerHand.LeftHand);
                StartPulse(button, ControllerHand.LeftHand);
            }
            else
                StopPulse(ControllerHand.LeftHand);
        }
        else if (hand == ControllerHand.RightHand)
        {
            ExternalRightButtonToPulse = ControllerButton.Nothing;
            ControllerButton button = ControllerButton.Nothing;
            if (RightButtonToPulse != ControllerButton.Nothing)
            {
                button = RightButtonToPulse;
                StopPulse(ControllerHand.RightHand);
                StartPulse(button, ControllerHand.RightHand);
            }
            else
                StopPulse();
        }

    }

    private ControllerButton GetButton(ControllerButtonInput buttonToUse)
    {
        switch (buttonToUse)
        {
            case ControllerButtonInput.Trigger:
                return ControllerButton.Trigger;
            case ControllerButtonInput.RGrip:
                return ControllerButton.RGrip;
            case ControllerButtonInput.LGrip:
                return ControllerButton.LGrip;
            case ControllerButtonInput.Grip:
                return ControllerButton.Grip;
            case ControllerButtonInput.Pad:
                return ControllerButton.Pad;
            case ControllerButtonInput.All:
                return ControllerButton.All;
            default:
                return ControllerButton.Nothing;
        }
    }
    
    public bool LeftHandFree()
    {
        return ItemLeftIndex == -1 && ItemLeft == null;
    }
    public bool RightHandFree()
    {
        return ItemRightIndex == -1 && ItemRight == null;
    }

    public Vector3 CurrentItemPos(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex != -1)
                return ItemsRight[ItemRightIndex].transform.position;
            else return new Vector3(0, 0, 0);
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex != -1)
                return ItemsLeft[ItemLeftIndex].transform.position;
            else return new Vector3(0, 0, 0);
        }
        else
            return new Vector3(0, 0, 0);
    }
    public Quaternion CurrentItemRot(ControllerHand hand)
    {
        if (hand == ControllerHand.RightHand)
        {
            if (ItemRightIndex != -1)
                return ItemsRight[ItemRightIndex].transform.rotation;
            else
                return new Quaternion(0, 0, 0, 0);
        }
        else if (hand == ControllerHand.LeftHand)
        {
            if (ItemLeftIndex != -1)
                return ItemsLeft[ItemLeftIndex].transform.rotation;
            else
                return new Quaternion(0, 0, 0, 0);
        }
        else
            return new Quaternion(0, 0, 0, 0);
    }
}
