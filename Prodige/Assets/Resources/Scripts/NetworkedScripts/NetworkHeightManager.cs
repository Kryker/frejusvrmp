﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkHeightManager : NetworkBehaviour
{

    [HideInInspector]
    public List<float> ModelHeights;
    public float DebugHeight;
    [SyncVar]
    float ArmScale;
    [SyncVar(hook = "OnPlayerHeightChanged")]
    float Height;
    [HideInInspector]
    public List<Transform> Rigs;

    // Use this for initialization
    private void Awake()
    {
        GetComponent<PlayerSpawnerNet>().OnPlayerModelSetEvent += ManageHeight;
    }

    public void ManageHeight()
    {
        if (isServer)
        {
            if (isLocalPlayer)
            {
                SetScale();
            }
            /*
            else
            {
                if (isClient)
                    Height = 0;
                else
                    OnPlayerHeightChanged(0);
            }*/
        }
        else
        {
            if (isLocalPlayer)
                SetScale();
            else
                OnPlayerHeightChanged(Height);
        }

       //GetComponent<PlayerSpawnerNet>().OnPlayerModelSetEvent -= ManageHeight;
    }

    private void SetScale()
    {
        if (isServer)
        {
            ArmScale = GameManager.Instance.PlayerArmScale;
            if (isClient)
                Height = GameManager.Instance.PlayerHeight;
            else
                OnPlayerHeightChanged(GameManager.Instance.PlayerHeight);
        }
        else
            CmdSetScale(GameManager.Instance.PlayerHeight, GameManager.Instance.PlayerArmScale);
    }
    [Command]
    private void CmdSetScale(float playerHeight, float armScale)
    {
        ArmScale = armScale;
        if (isClient)
            Height = playerHeight;
        else
            OnPlayerHeightChanged(playerHeight);
    }

    private void OnPlayerHeightChanged(float value)
    {
        if (Height != 0)
        {
            for (int i = 0; i < Rigs.Count; i++)
            {
                var scale = value / ModelHeights[i];
                if(scale <= 0)
                {
                    return;
                }
                Rigs[i].localScale = new Vector3(scale, scale, scale);
                var LeftArm = Rigs[i].GetComponent<ModelCharacteristics>().LeftShoulder.GetChild(0);
                var RightArm = Rigs[i].GetComponent<ModelCharacteristics>().RightShoulder.GetChild(0);
                LeftArm.localScale = new Vector3(ArmScale, ArmScale, ArmScale);
                RightArm.localScale = new Vector3(ArmScale, ArmScale, ArmScale);

            }
        }
        Height = value;
    }

}
