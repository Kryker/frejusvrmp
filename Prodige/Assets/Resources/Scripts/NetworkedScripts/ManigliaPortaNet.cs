﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum TipoManiglia { Interna, Esterna };

public class ManigliaPortaNet : GenericItemNet
{
    public ManigliaPortaNet LinkedHandle;
    public ManigliaPortaNet OppositeHandle;
    AudioSource audiosource;
    public VehicleSeat.Seat seat;
    [HideInInspector]
    public VehicleDataNet vehicle;
    public TipoManiglia Tipo;

    // Use this for initialization
    void Awake () {
        ItemCode = ItemCodes.CarHandle;
        if (Slave != null)
            audiosource = Slave.GetComponent<AudioSource>();
        else
            audiosource = transform.GetComponent<AudioSource>();
        vehicle = GetComponent<VehicleDataNet>();
    }

    // Update is called once per frame
    public override void Update()
    {

    }

    public override void Start()
    {
        base.Start();
    }

    void OpenHandle(ItemControllerNet c)
    {
        audiosource.Play();
        var p = c.GetComponent<PlayerStatusNet>();
        if (p.Ruolo == SpawnMode.TruckDriver && vehicle.Type == VehicleDataNet.VehicleType.Truck && this.Tipo == TipoManiglia.Interna)
        {
            this.GetComponent<TruckControllerNet>().portaSinistraAperta = true;
        }
        GameManager.Instance.TCN.ManageCarHandleInteraction(p, this);
        
    }
  

    public override void ClickButton(object sender, ClickedEventArgs e)
    {}
    
    public override void UnClickButton(object sender, ClickedEventArgs e)
    {}

    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
        //Debug.LogError("QUIManigliaA");
        if (CanInteract(c))
            OpenHandle(c);
    }

    public override void Interact(GenericItemSlave slave, ItemControllerNet c, ControllerHand hand)
    {
        //Debug.LogError("QUIManigliaB");
        if (CanInteract(c))
            OpenHandle(c);
    }

    public override bool CanInteract(ItemControllerNet c)
    {
        //Debug.LogError(seat + "Interagisce con "+ this.gameObject.name);

        if (base.CanInteract(c))
        {
            var p = c.GetComponent<PlayerStatusNet>();
            if (seat == VehicleSeat.Seat.Left)
            {
                if (Tipo == TipoManiglia.Esterna)
                {
                    if (vehicle.LeftOccupant == NetworkInstanceId.Invalid)
                        return true;
                }
                else
                {
                    if (vehicle.LeftOccupant == p.netId)
                        return true;
                }
            }
            else
            {
                if (Tipo == TipoManiglia.Esterna)
                {
                    if (vehicle.RightOccupant == NetworkInstanceId.Invalid)
                        return true;
                }
                else
                {
                    if (vehicle.RightOccupant == p.netId)
                        return true;
                }
            }
        }
       // Debug.LogError("False");
        return false;
    }
    public override void Reset()
    {
        base.Reset();
    }
}
