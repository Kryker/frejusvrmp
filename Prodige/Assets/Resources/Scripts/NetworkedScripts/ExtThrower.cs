﻿using UnityEngine;
using System.Collections;
using System;

public class ExtThrower : ItemBehaviour
{
    public ParticleSystem waterEmit;
    VibrationController vibr;
    public float fuelduration = 30.0f;
    [HideInInspector]
    public ExtinguisherNet Ext;
    public AudioClip SecuredButtonUp, SecuredButtonDown;
    Transform GrabbedExt;
    AudioSource audiosource;

    bool firing = false;

    private void Awake()
    {
        audiosource = GetComponent<AudioSource>();
        GrabbedExt = transform.parent;
    }

    public override void OnDisable()
    {
        if (firing)
            StopFire();
        base.OnDisable();
    }
    internal void ParticleDisabled(AdvancedStateMachineBehaviour a)
    {
        StopFire();
    }

    internal void ParticleEnabled(AdvancedStateMachineBehaviour a)
    {
        StartFire();
    }


    public override void HandleClicked(object sender, ClickedEventArgs e)
        {
        StartFire();
        }

    public override void HandleUnClicked(object sender, ClickedEventArgs e)
        {
        StopFire();
        }

    public override void OnClient()
    {
        var c = waterEmit.collision;
        c.sendCollisionMessages = false;
    }


    public override void OnEnable()
    {
        if (controller == null)
            controller = GrabbedExt.parent;
        base.OnEnable();
    }


    void Update()
    {
        if (!Networked && UseMouseAsController)
        {
            if (Input.GetButtonDown(mousebutton))
            {
                StartFire();
            }
            if (Input.GetButtonDown(mousebutton))
            {
                StopFire();
            }
        }
        if (firing)
        {
            var remainingtime = fuelduration * Ext.Fuel / 100;
            remainingtime = remainingtime - Time.deltaTime;
            if (remainingtime <= 0)
            {
                Ext.Fuel = 0;
            }
            else
                Ext.Fuel = remainingtime / fuelduration * 100;
            if (Ext.Fuel <= 0)
            {
                Ext.Fuel = 0.0f;
                CancelInvoke("Openfire");
                if (vibr != null)
                {
                    vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
                    vibr = null;
                }
                firing = false;                
            }
        }
    }



    void Openfire()
    {
        waterEmit.Play();
        waterEmit.GetComponent<AudioSource>().Play();

        if (vibr == null)
        {
            var ic = controller.GetComponent<ControllerManager>();
            if (ic != null)
            {
                var v = ic.vibrationController;
                if (v.StartVibration(0.05f, 0.65f, 0.04f, this))
                    vibr = v;
            }
        }
    }

    void StartFire()
    {
        if (gameObject.activeSelf && Ext.Secured == SecureState.Open)
        {
            Invoke("Openfire", 0.1f);
            var ic = controller.GetComponent<ControllerManager>();
            if (ic != null)
                ic.StopPulsePublic();
            firing = true;
        }
        else if (gameObject.activeSelf)
        {
            audiosource.Stop();
            audiosource.clip = SecuredButtonDown;
            audiosource.Play();
        }
    }

    void StopFire()
    {
        if (gameObject.activeSelf && (Ext == null || Ext.Secured == SecureState.Open))
        {
            CancelInvoke("Openfire");
            waterEmit.Stop();
            if (vibr != null)
            {
                vibr.StopVibration(0.05f, 0.65f, 0.04f, this);
                vibr = null;
            }
            waterEmit.GetComponent<AudioSource>().Stop();
            firing = false;
        }
        else if (gameObject.activeSelf)
        {
            audiosource.Stop();
            audiosource.clip = SecuredButtonUp;
            audiosource.Play();
        }
    }
}