﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MultiNetworkBehaviour : NetworkBehaviour {

    [HideInInspector]
    public LocalIDProvider IDProvider;
    [HideInInspector]
    public uint locID = 0;

    // Use this for initialization
    public virtual void Start ()
    {
        IDProvider = GetComponent<LocalIDProvider>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
