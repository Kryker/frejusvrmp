﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkStartButton : MonoBehaviour
{
    public VRButtonController tastoStart;
    public VRButtonController tastoWaiting;

    // Use this for initialization
    void Start()
    {
        
        tastoStart.onClick += StartGame;
    }

    private void StartGame(object sender)
    {
        ItemControllerNet i = null;
        SteamVR_TrackedController t = null;
        try
        {
            t = (SteamVR_TrackedController)sender;
            var c = t.GetComponent<ControllerManager>();
            if (c != null)
            {
                tastoWaiting.gameObject.SetActive(true);
                tastoStart.gameObject.SetActive(false);
                c.ControllerNet.GetComponent<PlayerStatusNet>().StartGame();
            }
        }
        catch (InvalidCastException)
        {
            try
            {
                i = (ItemControllerNet)sender;
                tastoWaiting.gameObject.SetActive(true);
                tastoStart.gameObject.SetActive(false);
                i.GetComponent<PlayerStatusNet>().StartGame();
            }
            catch (InvalidCastException)
            { }
        }
    }
}

