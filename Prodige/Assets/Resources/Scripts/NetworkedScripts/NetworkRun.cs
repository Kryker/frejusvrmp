﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkRun : NetworkBehaviour
{
    
    public RootMotion.FinalIK.VRIK IK;
    public RigIK FPSIK;
    public Transform leftController, rightController;
    public Transform Camera;
    public AvatarMask LegMask;
    bool _running = false;
    
    public bool Running
    {
        get { return _running; }
        set
        {
            if (!Running && value)
            {
                if (IK != null)
                {
                    prevpos = new Vector2(transform.position.x, transform.position.z);
                    if (!isLocalPlayer)
                    {
                        FakeLeftHand.Target = LeftHand;
                        FakeRightHand.Target = RightHand;
                    }
                    ResetStartLegPose();
                  
                    IKWeightChanger = StartCoroutine(StartRun());
                }

            }
            else if (Running && !value)
            {
                if (IK != null)
                {
                    Speed = 0.0f;
                    Acceleration = 0.0f;
                    if (!isLocalPlayer)
                    {
                        FakeLeftHand.Target = null;
                        FakeRightHand.Target = null;
                    }
                    ResetStartLegPose();
                    IK.solver.Reset();
                    if (IKWeightChanger != null)
                    {
                        StopCoroutine(IKWeightChanger);
                    }
                    IKWeightChanger = StartCoroutine(StopRun());
                }
            }
            _running = value;
        }
    }
    [SyncVar(hook = "OnLeftFistingChanged")]
    public bool LeftFisting;
    [SyncVar(hook = "OnRightFistingChanged")]
    public bool RightFisting;
    Transform LeftHand, RightHand;
    public Transform LeftUpLeg, RightUpLeg;
    public Transform LeftShoulder, RightShoulder;
    Transform LeftLeg, RightLeg, LeftFoot, RightFoot, LeftToeBase, RightToeBase, LeftToe_End, RightToe_End;
    Quaternion LeftUpLegStartRot, RightUpLegStartRot, LeftLegStartRot, RightLegStartRot, LeftFootStartRot, RightFootStartRot, LeftToeBaseStartRot, RightToeBaseStartRot, LeftToe_EndStartRot, RightToe_EndStartRot;
    Transform LeftArm, RightArm, LeftForeArm, RightForeArm, LeftHandIndex1, RightHandIndex1, LeftHandIndex2, RightHandIndex2, LeftHandIndex3, RightHandIndex3, LeftHandIndex4, RightHandIndex4, LeftHandMiddle1, RightHandMiddle1, LeftHandMiddle2, RightHandMiddle2, LeftHandMiddle3, RightHandMiddle3, LeftHandMiddle4, RightHandMiddle4, LeftHandPinky1, RightHandPinky1, LeftHandPinky2, RightHandPinky2, LeftHandPinky3, RightHandPinky3, LeftHandPinky4, RightHandPinky4, LeftHandRing1, RightHandRing1, LeftHandRing2, RightHandRing2, LeftHandRing3, RightHandRing3, LeftHandRing4, RightHandRing4, LeftHandThumb1, RightHandThumb1, LeftHandThumb2, RightHandThumb2, LeftHandThumb3, RightHandThumb3, LeftHandThumb4, RightHandThumb4;
    Quaternion LeftShoulderStartRot, RightShoulderStartRot, LeftArmStartRot, RightHandStartRot, RightArmStartRot, LeftForeArmStartRot, RightForeArmStartRot, LeftHandStartRot, LeftHandIndex1StartRot, RightHandIndex1StartRot, LeftHandIndex2StartRot, RightHandIndex2StartRot, LeftHandIndex3StartRot, RightHandIndex3StartRot, LeftHandIndex4StartRot, RightHandIndex4StartRot, LeftHandMiddle1StartRot, RightHandMiddle1StartRot, LeftHandMiddle2StartRot, RightHandMiddle2StartRot, LeftHandMiddle3StartRot, RightHandMiddle3StartRot, LeftHandMiddle4StartRot, RightHandMiddle4StartRot, LeftHandPinky1StartRot, RightHandPinky1StartRot, LeftHandPinky2StartRot, RightHandPinky2StartRot, LeftHandPinky3StartRot, RightHandPinky3StartRot, LeftHandPinky4StartRot, RightHandPinky4StartRot, LeftHandRing1StartRot, RightHandRing1StartRot, LeftHandRing2StartRot, RightHandRing2StartRot, LeftHandRing3StartRot, RightHandRing3StartRot, LeftHandRing4StartRot, RightHandRing4StartRot, LeftHandThumb1StartRot, RightHandThumb1StartRot, LeftHandThumb2StartRot, RightHandThumb2StartRot, LeftHandThumb3StartRot, RightHandThumb3StartRot, LeftHandThumb4StartRot, RightHandThumb4StartRot;
    public FollowSmooth FakeLeftHand, FakeRightHand;
    private Vector2 lastblendTreeMovement = Vector2.zero;
    private Vector3 lastLeftStickInputAxis = Vector3.zero;
    public ItemControllerNet Controller;
    Animator anim;
    public float smoothTimeRun = 0.001f, smoothTimeRot = 0.05f;
    Coroutine IKWeightChanger;
    float _weight = 1;
    public float Speed, Acceleration;
    Vector2 prevpos = Vector2.zero;
    Vector2 currpos = Vector2.zero;
    float deltapos = 0;
    Vector3 direction = Vector3.zero;
    float prevspeed = 0.0f;
    public float MaxSpeed = 8.0f;
    float _nextCheck = -1;
    public float intervalloControlloSpeed = 1f;
    Vector3 _prevAngle = new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);
    public AnimationCurve SpeedToRunCurve = new AnimationCurve(new Keyframe(0, 0, 0, 1), new Keyframe(1, 1, 1, 0));

    // Use this for initialization
    void Start()
    {
        if (IK != null)
        {
            GetStartLegPose();
            GetStartArmPose();
        }

        if (isServer)
        {
            if (isClient)
            {
                LeftFisting = false;
                RightFisting = false;
            }
            else
            {
                OnLeftFistingChanged(false);
                OnRightFistingChanged(false);
            }
        }
        else
        {
            OnLeftFistingChanged(LeftFisting);
            OnRightFistingChanged(RightFisting);
        }
    }
    internal void SetAnimator(Animator a)
    {
        anim = a;
        if (anim != null && anim.gameObject.activeSelf)
        {
            anim.SetBool("FistL", LeftFisting);
            anim.SetBool("FistR", RightFisting);

            anim.SetLayerWeight(anim.GetLayerIndex("LowerBody"), 1 - _weight);
            if (!isLocalPlayer)
                anim.SetLayerWeight(anim.GetLayerIndex("UpperBody"), 1 - _weight);

           
        }

        if (FPSIK != null)
        {
            if (anim != null && anim.layerCount > 1)
            {
                int indexLower = anim.GetLayerIndex("LowerBody");
                int indexUpper = anim.GetLayerIndex("UpperBody");
                if (indexLower >= 0)
                {
                    anim.SetLayerWeight(indexLower, 1);
                    if (!isLocalPlayer && indexUpper >= 0)
                        anim.SetLayerWeight(anim.GetLayerIndex("UpperBody"), 1);
                }

            }
        }
    }

    public void ReInit()
    {
        Start();
    }

    private void GetStartArmPose()
    {
        LeftShoulderStartRot = LeftShoulder.localRotation;
        RightShoulderStartRot = RightShoulder.localRotation;
        LeftArm = LeftShoulder.GetChild(0);
        LeftArmStartRot = LeftArm.localRotation;
        RightArm = RightShoulder.GetChild(0);
        RightArmStartRot = RightArm.localRotation;
        LeftForeArm = LeftArm.GetChild(0);
        LeftForeArmStartRot = LeftForeArm.localRotation;
        RightForeArm = RightArm.GetChild(0);
        RightForeArmStartRot = RightForeArm.localRotation;
        LeftHand = LeftForeArm.GetChild(0);
        LeftHandStartRot = LeftHand.localRotation;
        RightHand = RightForeArm.GetChild(0);
        RightHandStartRot = RightHand.localRotation;
        LeftHandIndex1 = LeftHand.GetChild(0);
        LeftHandIndex1StartRot = LeftHandIndex1.localRotation;
        RightHandIndex1 = RightHand.GetChild(0);
        RightHandIndex1StartRot = RightHandIndex1.localRotation;
        LeftHandIndex2 = LeftHand.GetChild(0);
        LeftHandIndex2StartRot = LeftHandIndex2.localRotation;
        RightHandIndex2 = RightHand.GetChild(0);
        RightHandIndex2StartRot = RightHandIndex2.localRotation;
        LeftHandIndex3 = LeftHand.GetChild(0);
        LeftHandIndex3StartRot = LeftHandIndex3.localRotation;
        RightHandIndex3 = RightHand.GetChild(0);
        RightHandIndex3StartRot = RightHandIndex3.localRotation;
        LeftHandIndex4 = LeftHand.GetChild(0);
        LeftHandIndex4StartRot = LeftHandIndex4.localRotation;
        RightHandIndex4 = RightHand.GetChild(0);
        RightHandIndex4StartRot = RightHandIndex4.localRotation;
        LeftHandMiddle1 = LeftHand.GetChild(1);
        LeftHandMiddle1StartRot = LeftHandMiddle1.localRotation;
        RightHandMiddle1 = RightHand.GetChild(1);
        RightHandMiddle1StartRot = RightHandMiddle1.localRotation;
        LeftHandMiddle2 = LeftHand.GetChild(1);
        LeftHandMiddle2StartRot = LeftHandMiddle2.localRotation;
        RightHandMiddle2 = RightHand.GetChild(1);
        RightHandMiddle2StartRot = RightHandMiddle2.localRotation;
        LeftHandMiddle3 = LeftHand.GetChild(1);
        LeftHandMiddle3StartRot = LeftHandMiddle3.localRotation;
        RightHandMiddle3 = RightHand.GetChild(1);
        RightHandMiddle3StartRot = RightHandMiddle3.localRotation;
        LeftHandMiddle4 = LeftHand.GetChild(1);
        LeftHandMiddle4StartRot = LeftHandMiddle4.localRotation;
        RightHandMiddle4 = RightHand.GetChild(1);
        RightHandMiddle4StartRot = RightHandMiddle4.localRotation;
        LeftHandPinky1 = LeftHand.GetChild(2);
        LeftHandPinky1StartRot = LeftHandPinky1.localRotation;
        RightHandPinky1 = RightHand.GetChild(2);
        RightHandPinky1StartRot = RightHandPinky1.localRotation;
        LeftHandPinky2 = LeftHand.GetChild(2);
        LeftHandPinky2StartRot = LeftHandPinky2.localRotation;
        RightHandPinky2 = RightHand.GetChild(2);
        RightHandPinky2StartRot = RightHandPinky2.localRotation;
        LeftHandPinky3 = LeftHand.GetChild(2);
        LeftHandPinky3StartRot = LeftHandPinky3.localRotation;
        RightHandPinky3 = RightHand.GetChild(2);
        RightHandPinky3StartRot = RightHandPinky3.localRotation;
        LeftHandPinky4 = LeftHand.GetChild(2);
        LeftHandPinky4StartRot = LeftHandPinky4.localRotation;
        RightHandPinky4 = RightHand.GetChild(2);
        RightHandPinky4StartRot = RightHandPinky4.localRotation;
        LeftHandRing1 = LeftHand.GetChild(3);
        LeftHandRing1StartRot = LeftHandRing1.localRotation;
        RightHandRing1 = RightHand.GetChild(3);
        RightHandRing1StartRot = RightHandRing1.localRotation;
        LeftHandRing2 = LeftHand.GetChild(3);
        LeftHandRing2StartRot = LeftHandRing2.localRotation;
        RightHandRing2 = RightHand.GetChild(3);
        RightHandRing2StartRot = RightHandRing2.localRotation;
        LeftHandRing3 = LeftHand.GetChild(3);
        LeftHandRing3StartRot = LeftHandRing3.localRotation;
        RightHandRing3 = RightHand.GetChild(3);
        RightHandRing3StartRot = RightHandRing3.localRotation;
        LeftHandRing4 = LeftHand.GetChild(3);
        LeftHandRing4StartRot = LeftHandRing4.localRotation;
        RightHandRing4 = RightHand.GetChild(3);
        RightHandRing4StartRot = RightHandRing4.localRotation;
        LeftHandThumb1 = LeftHand.GetChild(3);
        LeftHandThumb1StartRot = LeftHandThumb1.localRotation;
        //controllo se null, aluni modelli non hanno 5 dita .-. BAH
        /*
        RightHandThumb1 = RightHand.GetChild(4);
        if(RightHandThumb1 != null)
            RightHandThumb1StartRot = RightHandThumb1.localRotation;
            LeftHandThumb2 = LeftHand.GetChild(4);
        if (LeftHandThumb2 != null)
            LeftHandThumb2StartRot = LeftHandThumb2.localRotation;
            RightHandThumb2 = RightHand.GetChild(4);
        if (RightHandThumb2 != null)
            RightHandThumb2StartRot = RightHandThumb2.localRotation;
            LeftHandThumb3 = LeftHand.GetChild(4);
        if (LeftHandThumb3 != null)
            LeftHandThumb3StartRot = LeftHandThumb3.localRotation;
            RightHandThumb3 = RightHand.GetChild(4);
        if (RightHandThumb3 != null)
            RightHandThumb3StartRot = RightHandThumb3.localRotation;
            LeftHandThumb4 = LeftHand.GetChild(4);
        if (LeftHandThumb4 != null)
            LeftHandThumb4StartRot = LeftHandThumb4.localRotation;
            RightHandThumb4 = RightHand.GetChild(4);
        if (RightHandThumb4 != null)
            RightHandThumb4StartRot = RightHandThumb4.localRotation;
       */
    }

    private void ResetStartArmPose()
    {
        LeftShoulder.localRotation = LeftShoulderStartRot;
        RightShoulder.localRotation = RightShoulderStartRot;
        LeftArm.localRotation = LeftArmStartRot;
        RightArm.localRotation = RightArmStartRot;
        LeftForeArm.localRotation = LeftForeArmStartRot;
        RightForeArm.localRotation = RightForeArmStartRot;
        LeftHand.localRotation = LeftHandStartRot;
        RightHand.localRotation = RightHandStartRot;
    }



    private void GetStartLegPose()
    {
        LeftUpLegStartRot = LeftUpLeg.localRotation;
        RightUpLegStartRot = RightUpLeg.localRotation;
        LeftLeg = LeftUpLeg.GetChild(0);
        LeftLegStartRot = LeftLeg.localRotation;
        RightLeg = RightUpLeg.GetChild(0);
        RightLegStartRot = RightLeg.localRotation;
        LeftFoot = LeftUpLeg.GetChild(0);
        LeftFootStartRot = LeftFoot.localRotation;
        RightFoot = RightUpLeg.GetChild(0);
        RightFootStartRot = RightFoot.localRotation;
        LeftToeBase = LeftFoot.GetChild(0);
        LeftToeBaseStartRot = LeftToeBase.localRotation;
        RightToeBase = RightFoot.GetChild(0);
        RightToeBaseStartRot = RightToeBase.localRotation;
        LeftToe_End = LeftToeBase.GetChild(0);
        LeftToe_EndStartRot = LeftToe_End.localRotation;
        RightToe_End = RightToeBase.GetChild(0);
        RightToe_EndStartRot = RightToe_End.localRotation;
    }

    private void ResetStartLegPose()
    {
        LeftUpLeg.localRotation = LeftUpLegStartRot;
        RightUpLeg.localRotation = RightUpLegStartRot;
        if(LeftLeg != null)
        {
            LeftLeg.localRotation = LeftLegStartRot;
        }
        
        RightLeg.localRotation = RightLegStartRot;
        LeftFoot.localRotation = LeftFootStartRot;
        RightFoot.localRotation = RightFootStartRot;
        LeftToeBase.localRotation = LeftToeBaseStartRot;
        RightToeBase.localRotation = RightToeBaseStartRot;
        LeftToe_End.localRotation = LeftToe_EndStartRot;
        RightToe_End.localRotation = RightToe_EndStartRot;
    }

    private void OnEnable()
    {
        if (IK != null)
            IK.solver.Reset();
        
    }


    public float _pesoMaxIK = 1.0f;
    public float factor = 5;
    IEnumerator StopRun()
    {
        float weight = 0;
        if (isLocalPlayer)
            weight = IK.solver.locomotion.weight;
        else
            weight = IK.solver.IKPositionWeight;
        while (_weight < _pesoMaxIK)
        {
            var newik = _weight + Time.deltaTime * factor;
            if (newik > 1)
                newik = 1;
            _weight = newik;

            if (isLocalPlayer)
                IK.solver.locomotion.weight = _weight;
            else
                IK.solver.IKPositionWeight = _weight;

            if (anim != null && anim.layerCount > 1)
            {
                var indexLower = anim.GetLayerIndex("LowerBody");
                if(indexLower >= 0)
                {
                    anim.SetLayerWeight(anim.GetLayerIndex("LowerBody"), 1 - _weight);
                    if (!isLocalPlayer && anim.GetLayerIndex("UpperBody") >= 0)
                        anim.SetLayerWeight(anim.GetLayerIndex("UpperBody"), 1 - _weight);
                }
              
            }
            
            yield return null;
        }
        ResetStartLegPose();
        IK.solver.Reset();
        IKWeightChanger = null;
    }

    IEnumerator StartRun()
    {
        GetStartLegPose();
        float weight = 0;
        if (isLocalPlayer)
            weight = IK.solver.locomotion.weight;
        else
            weight = IK.solver.IKPositionWeight;
        while (IK.solver.IKPositionWeight > 0)
        {
            var newik = _weight - Time.deltaTime * factor;
            if (newik < 0)
                newik = 0;
            _weight = newik;

            if (isLocalPlayer)
                IK.solver.locomotion.weight = _weight;
            else
                IK.solver.IKPositionWeight = _weight;

            if (anim != null && anim.layerCount > 1)
            {
                int indexLower = anim.GetLayerIndex("LowerBody");
                int indexUpper = anim.GetLayerIndex("UpperBody");
                if (indexLower >= 0)
                {
                    anim.SetLayerWeight(indexLower, 1 - _weight);
                    if (!isLocalPlayer && indexUpper >= 0)
                        anim.SetLayerWeight(anim.GetLayerIndex("UpperBody"), 1 - _weight);
                }
              
            }
            yield return null;
        }
        IKWeightChanger = null;
    }

  
    public static Vector2 calculatePointOnSquare(float r, float angleInDegrees)
    {
        Vector2 p;
        p.x = 0.0f;
        p.y = 0.0f;

        double angle = (angleInDegrees % 360) * Math.PI / 180;

        double angleModPiOverTwo = angle % (Math.PI / 4);

        if (angle >= 0 && angle < Math.PI / 4)
        {
            p.x = r;
            p.y = (float)(r * Math.Tan(angle));
        }
        else if (angle >= Math.PI / 4 && angle < Math.PI / 2)
        {
            p.x = (float)(r * Math.Tan(Math.PI / 2 - angle));
            p.y = r;
        }
        else if (angle >= Math.PI / 2 && angle < 3 * Math.PI / 4)
        {
            p.x = (float)(-1 * r * Math.Tan(angle % (Math.PI / 4)));
            p.y = r;
        }
        else if (angle >= 3 * Math.PI / 4 && angle < Math.PI)
        {
            p.x = -1 * r;
            p.y = (float)(r * Math.Tan(Math.PI - angle));
        }
        else if (angle >= Math.PI && angle < 5 * Math.PI / 4)
        {
            p.x = -1 * r;
            p.y = (float)(-1 * r * Math.Tan(angle % (Math.PI / 4)));
        }
        else if (angle >= 5 * Math.PI / 4 && angle < 3 * Math.PI / 2)
        {
            p.x = (float)(-1 * r * Math.Tan(3 * Math.PI / 2 - angle));
            p.y = -1 * r;
        }
        else if (angle >= 3 * Math.PI / 2 && angle < 7 * Math.PI / 4)
        {
            p.x = (float)(r * Math.Tan(angle % (Math.PI / 4)));
            p.y = -1 * r;
        }
        else
        {
            p.x = r;
            p.y = (float)(-1 * r * Math.Tan(2 * Math.PI - angle));
        }

        return p;
    }

    

    private void Update()
    {
        var now = Time.time;
        if (now >= _nextCheck || !Running)
        {
            currpos = new Vector2(transform.position.x, transform.position.z);
            deltapos = Vector2.Distance(prevpos, currpos);
            if (deltapos > 0 && !Running)
                Running = true;
            else if (deltapos == 0 && Running)
                Running = false;

            direction = currpos - prevpos;
            if (deltapos != 0)
            {
                Speed = deltapos / Time.deltaTime;
                var deltaspeed = prevspeed - Speed;
                Acceleration = deltaspeed / Time.deltaTime;
            }
            else
            {
                Speed = 0;
                Acceleration = 0;
            }
            prevspeed = Speed;
            prevpos = currpos;
            _nextCheck = now + intervalloControlloSpeed;
        }

        if (anim != null && anim.gameObject.activeSelf)
        {
            if (FPSIK != null)
            {
                if (!anim.GetBool("Driving"))
                {
                    float yVelocity = 0f;

                    if (_prevAngle != new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity)
                        && _prevAngle != new Vector3(0, 0, 0)
                        && _prevAngle.sqrMagnitude < float.PositiveInfinity)
                    {
                        anim.transform.localEulerAngles = new Vector3(anim.transform.localEulerAngles.x, Mathf.SmoothDamp(_prevAngle.y, Camera.localEulerAngles.y, ref yVelocity, 0.01f), anim.transform.localEulerAngles.z);
                    }

                    _prevAngle = anim.transform.localEulerAngles;
                }


                if (Running || anim.GetFloat("VelX") != 0 || anim.GetFloat("VelZ") != 0)
                {          
                    var blendTreeMovement = Vector2.zero;
                    if (Running)
                    {
                        var e = Camera.localRotation * Vector3.forward;

                        var angleA = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
                        var angleB = Mathf.Atan2(e.x, e.z) * Mathf.Rad2Deg;

                        // get the signed difference in these angles
                        var a = Mathf.DeltaAngle(angleA, angleB);

                        blendTreeMovement = calculatePointOnCircle(SpeedToRunCurve.Evaluate(Speed / MaxSpeed), a);
                    }

                    float xVelocity = 0f, yVelocity = 0f;

                    if (lastblendTreeMovement != Vector2.zero)
                        //Interpolate between the input axis from the last frame and the new input axis we calculated
                        blendTreeMovement = new Vector2(Mathf.SmoothDamp(lastblendTreeMovement.x, blendTreeMovement.x, ref xVelocity, smoothTimeRun), Mathf.SmoothDamp(lastblendTreeMovement.y, blendTreeMovement.y, ref yVelocity, smoothTimeRun));

                    // Update the Animator with our values so that the blend tree updates
                    anim.SetFloat("VelX", blendTreeMovement.x);
                    anim.SetFloat("VelZ", blendTreeMovement.y);
                    if (isServer)
                    {
                        GrabLeft();
                        GrabRight();
                    }
                    lastblendTreeMovement = blendTreeMovement;
                }
                else
                {
                    //FPSIK.IKEnabled = true;
                }
            }
            else if (IK != null)
            {

                float xVelocity = 0f, yVelocity = 0f;
                if (Running || anim.GetFloat("VelX") != 0 || anim.GetFloat("VelZ") != 0)
                {                  
                    var blendTreeMovement = Vector2.zero;
                    if (Running)
                    {
                        var l = leftController.localRotation;

                        var r = rightController.localRotation;

                        var d = Quaternion.Slerp(l, r, .5f) * Vector3.forward;

                        var e = Camera.localRotation * Vector3.forward;

                        var angleA = Mathf.Atan2(d.x, d.z) * Mathf.Rad2Deg;
                        var angleB = Mathf.Atan2(e.x, e.z) * Mathf.Rad2Deg;

                        // get the signed difference in these angles
                        var a = Mathf.DeltaAngle(angleA, angleB);

                        a = a % 360;
                        if (a < 0)
                            a = 360 + a;

                        blendTreeMovement = calculatePointOnCircle(SpeedToRunCurve.Evaluate(Speed / MaxSpeed), a);
                        
                        if (IK.solver.rootBone != null && _weight < 1)
                        {
                            if (_prevAngle != new Vector3(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity))
                                anim.transform.localEulerAngles = new Vector3(anim.transform.localEulerAngles.x,
                                    Mathf.SmoothDamp(_prevAngle.y, Camera.localEulerAngles.y, ref yVelocity, 0.01f), anim.transform.localEulerAngles.z);

                            _prevAngle = anim.transform.localEulerAngles;
                        }

                        /*if (IK.solver.rootBone != null && _weight < 1)
                            IK.transform.localEulerAngles = new Vector3(IK.transform.localEulerAngles.x, Mathf.Lerp(IK.transform.localEulerAngles.y, Camera.transform.localEulerAngles.y, 1 - IKWeight), IK.transform.localEulerAngles.z);
                    */}

                    xVelocity = 0f;
                    yVelocity = 0f;

                    if (lastblendTreeMovement != Vector2.zero)
                        //Interpolate between the input axis from the last frame and the new input axis we calculated
                        blendTreeMovement = new Vector2(Mathf.SmoothDamp(lastblendTreeMovement.x, blendTreeMovement.x, ref xVelocity, smoothTimeRun), Mathf.SmoothDamp(lastblendTreeMovement.y, blendTreeMovement.y, ref yVelocity, smoothTimeRun));

                    // Update the Animator with our values so that the blend tree updates
                    anim.SetFloat("VelX", blendTreeMovement.x);
                    anim.SetFloat("VelZ", blendTreeMovement.y);
                    if (isServer)
                    {
                        GrabLeft();
                        GrabRight();
                    }
                    lastblendTreeMovement = blendTreeMovement;
                }
                else
                {
                    if (isServer)
                    {
                        if (Controller.ItemLeftID == NetworkInstanceId.Invalid)
                            DropLeft();
                        if (Controller.ItemRightID == NetworkInstanceId.Invalid)
                            DropRight();
                    }
                    lastblendTreeMovement = Vector2.zero;
                }
            }
        }
        //Debug.Log(Speed);
        //prevspeed = Speed;
        //prevpos = currpos;
    }

    private Vector2 calculatePointOnCircle(float r, float angleInDegrees)
    {
        float angle = (angleInDegrees % 360);
        var dir = Quaternion.Euler(0, -angle, 0) * Vector3.forward;
        var ray = new Ray(Vector3.zero, dir);
        var p = ray.GetPoint(r);
        return new Vector2(p.x, p.z);
    }

    private float SignedAngle(Vector3 a, Vector3 b)
    {
        return Vector3.Angle(a, b) * Mathf.Sign(Vector3.Cross(a, b).y);
    }

    void OnLeftFistingChanged(bool state)
    {
        if (anim != null && anim.gameObject.activeSelf)
            anim.SetBool("FistL", state);
        LeftFisting = state;
    }
    void OnRightFistingChanged(bool state)
    {
        if (anim != null && anim.gameObject.activeSelf)
            anim.SetBool("FistR", state);
        RightFisting = state;
    }

    internal void DropRight()
    {
        if (isServer)
        {
            if (!Running)
            {
                if (isClient)
                    RightFisting = false;
                else
                    OnRightFistingChanged(false);
            }
        }
    }

    internal void GrabRight()
    {
        if (isServer)
        {
            if (isClient)
                RightFisting = true;
            else
                OnRightFistingChanged(true);
        }
    }

    internal void DropLeft()
    {
        if (isServer)
        {
            if (!Running)
            {
                if (isClient)
                    LeftFisting = false;
                else
                    OnLeftFistingChanged(false);
            }
        }
    }
    internal void Grab(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
            GrabLeft();
        else if (hand == ControllerHand.RightHand)
            GrabRight();
    }

    internal void GrabLeft()
    {
        if (isServer)
        {
            if (isClient)
                LeftFisting = true;
            else
                OnLeftFistingChanged(true);
        }
    }

    internal void Drop(ControllerHand hand)
    {
        if (hand == ControllerHand.LeftHand)
            DropLeft();
        else if (hand == ControllerHand.RightHand)
            DropRight();
    }
}
