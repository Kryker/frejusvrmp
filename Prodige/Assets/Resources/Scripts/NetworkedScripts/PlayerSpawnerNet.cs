﻿using RootMotion.FinalIK;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerSpawnerNet : NetworkBehaviour
{
    [Header("HTC Vive Components")]
    public SteamVR_ControllerManager SteamVRControllerManager;
    public SteamVR_PlayArea SteamVRPlayArea;
    public Transform CameraRig;
    public SteamVR_TrackedController SteamVRTrackedControllerLeft;
    public SteamVR_TrackedController SteamVRTrackedControllerRight;
    public SteamVR_TrackedObject SteamVRTrackedObjectLeft;
    public SteamVR_TrackedObject SteamVRTrackedObjectRight;
    public SteamVR_TrackedObject SteamVRTrackedObjectHead;
    public Transform ControllerLeft, ControllerRight;
    private NetworkStartPosition spawnPoint;
    public SteamVR_Camera SteamVRCamera;
    public SteamVR_Ears SteamVREars;
    public Camera CameraEye;
    public Camera CameraHead;
    public Transform VRVisor;
    public bool ShowVREquipment = false;
    public AudioListener CameraEar;
    public OpenVRSwinger Swinger;
    public SquaredLimitTrackingNet SquaredLimitTracking;
    public CharacterControllerVR CharacterControllerVR;
    public VibrationController VibrationLeft, VibrationRight;
    public SphereCollider ControllerColliderLeft, ControllerColliderRight;
    public AudioSource VRMouth;
    public Transform headTarget;
    public Transform leftArmTarget;
    public Transform rightArmTarget;
    public RootMotion.Demos.VRIKPlatform platform;

    [Header("Foot Swinger Components")]
    public SteamVR_TrackedObject SteamVRTrackedObjectLegLeft;
    public SteamVR_TrackedObject SteamVRTrackedObjectLegRight;
    public NetworkTransformChild TrackerLeftNet, TrackerRightNet;
    public Transform FootSwingTrackerControllerPrefab;
    public Transform leftLegTarget;
    public Transform rightLegTarget;

    [Header("Cyberith Virtualizer Components")]
    public CVirtDeviceController VirtualizerDeviceController;
    public CVirtPlayerController VirtualizerPlayerController;
    public Transform CameraHolder;
    public Transform ForwardDirection;

    [Header("KatWalk Components")]
    public KATDevice KatDeviceController;    

    [Header("FPS Controller Components")]
    public Transform Visor;
    public Camera FPSCamera;
    public AudioListener Listener;
    public CharacterMotor CharacterMotor;
    public MouseLook MouseLook;
    public MouseController MouseController;
    public FPSInputController FPSInputController;
    public AudioSource FPSMouth;
    public Transform Crosshair;
    public Transform BlackScreen;
    public Vector3 DrivingModelLocalPos;

    [Header("Generic Components")]
    public ItemControllerNet ItemController;
    public List<string> ModelNames;
    [HideInInspector]
    public Transform Armature, DrivingArmature;
    Camera MainCamera;
    public PlayerStatusNet PlayerStatus;

    public ControllerType Platform;
    [SyncVar(hook = "OnSpawnedChanged")]
    bool spawned;
    [SyncVar(hook = "OnJoinedChanged")]
    bool joined;
    [SyncVar(hook = "OnPlayerModelSet")]
    int PlayerModel;
    public event Action OnPlayerModelSetEvent;

    void OnPlayerModelSet(int value)
    {
        UnityEngine.Object m, md = null;
        if (value != 0)
        {
            if (value == -1)
            {
                if (Platform == ControllerType.MouseAndKeyboard)
                {

                    if (isLocalPlayer)
                    {
                        if (isServer)
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\FPS\\" + ModelNames[0]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\NoHead" + ModelNames[0]);
                        }
                        else
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\FPS\\" + ModelNames[1]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\NoHead" + ModelNames[1]);
                        }
                    }
                    else
                    {
                        if (isServer)
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\FPS\\" + ModelNames[1]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\" + ModelNames[1]);
                        }
                        else
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\FPS\\" + ModelNames[0]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\" + ModelNames[0]);
                        }
                    }
                }
                else
                {
                    if (isLocalPlayer)
                    {
                        if (isServer)
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[0]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\NoHead" + ModelNames[0]);
                        }
                        else
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[1]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\NoHead" + ModelNames[1]);
                        }
                    }
                    else
                    {
                        if (isServer)
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[1]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\" + ModelNames[1]);
                        }
                        else
                        {
                            m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[0]);
                            md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\" + ModelNames[0]);
                        }
                    }
                }
            }
            else if (Platform == ControllerType.MouseAndKeyboard)
            {
                m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\FPS\\" + ModelNames[value - 1]);
                if(isLocalPlayer)
                    md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\NoHead" + ModelNames[value - 1]);
                else
                    md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\" + ModelNames[value - 1]);
            }
            else
            {
                if (isLocalPlayer)
                    m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\NoHead" + ModelNames[value - 1]);
                else
                    m = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\VR\\" + ModelNames[value - 1]);
                if(isLocalPlayer)
                    md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\NoHead" + ModelNames[value - 1]);
                else
                    md = Resources.Load("Prefabs\\Player Controllers\\PlayerModels\\Driving\\" + ModelNames[value - 1]);
            }

            //devo distruggere l'armatura esistente
            if(Armature != null)
            {
                GetComponent<NetworkHeightManager>().Rigs.Clear();
                DestroyImmediate(Armature.gameObject);
            }
            if(DrivingArmature != null)
            {
                GetComponent<NetworkHeightManager>().Rigs.Clear();
                DestroyImmediate(DrivingArmature.gameObject);
            }

            if (Platform == ControllerType.MouseAndKeyboard)
            {
              
                Armature = (Instantiate(m, transform) as GameObject).transform;
                Armature.name = "Armature";
                PlayerStatus.Scream = Armature.GetComponent<ModelCharacteristics>().Scream;
                PlayerStatus.Cough = Armature.GetComponent<ModelCharacteristics>().Cough;
                DrivingArmature = (Instantiate(md, transform) as GameObject).transform;
                DrivingArmature.name = "DrivingArmature";
                DrivingArmature.localPosition = DrivingModelLocalPos;
                PlayerStatus.healthBar = Armature.GetComponent<ModelCharacteristics>().HealthBar;
                if (isClient && !GameManager.Instance.DebugMode)
                {
                    Armature.GetComponent<ModelCharacteristics>().HealthBar.parent.gameObject.SetActive(false);
                    DrivingArmature.GetComponent<ModelCharacteristics>().HealthBar.parent.gameObject.SetActive(false);
                }
                
                var rigik = Armature.GetComponent<RigIK>();
                rigik.Head = FPSCamera.transform;
                rigik.LeftHandTarget = FPSCamera.transform.Find("Controller (left)");
                rigik.RightHandTarget = FPSCamera.transform.Find("Controller (right)");
                var rigik2 = DrivingArmature.GetComponent<RigIK>();
                rigik2.Head = FPSCamera.transform;
                rigik2.LeftHandTarget = FPSCamera.transform.Find("Controller (left)");
                rigik2.RightHandTarget = FPSCamera.transform.Find("Controller (right)");
                GetComponent<NetworkRun>().SetAnimator(Armature.GetComponent<Animator>());
                GetComponent<NetworkRun>().FPSIK = rigik;
                GetComponent<NetworkHeightManager>().Rigs.Add(Armature);
                GetComponent<NetworkHeightManager>().ModelHeights.Add(Armature.GetComponent<ModelCharacteristics>().Height);
                GetComponent<NetworkHeightManager>().Rigs.Add(DrivingArmature);
                GetComponent<NetworkHeightManager>().ModelHeights.Add(DrivingArmature.GetComponent<ModelCharacteristics>().Height);
                rigik.enabled = true;
                rigik2.enabled = true;
                 
                GetComponent<NetworkRun>().enabled = true;
                GetComponent<NetworkHeightManager>().enabled = true;
                Armature.gameObject.SetActive(true);
                
                
            }
            else if(Platform == ControllerType.FreeWalk)
            {

                string resToLoad = "Prefabs\\Player Controllers\\PlayerModels\\xsens\\NoHead"+ ModelNames[value - 1];
                if (!isLocalPlayer)
                    resToLoad = "Prefabs\\Player Controllers\\PlayerModels\\xsens\\"+ ModelNames[value - 1];
                var CaricoXSENS = Resources.Load(resToLoad);
                Armature = (Instantiate(CaricoXSENS, transform) as GameObject).transform;

                Armature.GetComponent<xsens.XsLiveAnimator>().mvnActors = this.GetComponent<xsens.XsStreamReader>();
                Armature.GetComponent<xsens.XsLiveAnimator>().diffusorePosizioni = this.GetComponent<DataStreamXsens>();
                this.GetComponent<DataStreamXsens>().anim = Armature.GetComponent<xsens.XsLiveAnimator>();
                //Armature.GetComponent<xsens.XsLiveAnimator>().LoadAnimator();
                if(isLocalPlayer)
                    this.GetComponent<xsens.XsStreamReader>().AvviaServer();


                PlayerStatus.Scream = Armature.GetComponent<ModelCharacteristics>().Scream;
                PlayerStatus.Cough = Armature.GetComponent<ModelCharacteristics>().Cough;
                PlayerStatus.healthBar = Armature.GetComponent<ModelCharacteristics>().HealthBar;

                if (isClient && !GameManager.Instance.DebugMode)
                    Armature.GetComponent<ModelCharacteristics>().HealthBar.parent.gameObject.SetActive(false);

                GetComponent<NetworkHeightManager>().Rigs.Clear();
                GetComponent<NetworkHeightManager>().Rigs.Add(Armature);
                GetComponent<NetworkHeightManager>().ModelHeights.Clear(); //rimuovo le vecchie RIG
                GetComponent<NetworkHeightManager>().ModelHeights.Add(Armature.GetComponent<ModelCharacteristics>().Height);
                GetComponent<NetworkHeightManager>().enabled = true;
                GetComponent<NetworkHeightManager>().ManageHeight();

                /*
                var rigik = Armature.GetComponent<VRIK>();    
                rigik.solver.leftArm.target = this.transform.Find("[CameraRig]").Find("Controller (left)").Find("FakeIK");
                rigik.solver.rightArm.target = this.transform.Find("[CameraRig]").Find("Controller (right)").Find("FakeIK"); ;
                // rigik.enabled = true;
                rigik.enabled = false;
               //ControllerLeft.Find("Model").gameObject.SetActive(false);
                //ControllerRight.Find("Model").gameObject.SetActive(false);
                */
                //?? se la attivo non funziona la cattura.
                //Non importa attivarlo qui perché viene attivato Con il PlayerGetIn quando si è deciso il ruolo.
               // if(!isLocalPlayer)
                    Armature.gameObject.SetActive(true);

            }
            else
            {
                Armature = (Instantiate(m, CameraRig) as GameObject).transform;
                Armature.name = "Armature";
                PlayerStatus.Scream = Armature.GetComponent<ModelCharacteristics>().Scream;
                PlayerStatus.Cough = Armature.GetComponent<ModelCharacteristics>().Cough;
                var vrik = Armature.GetComponent<RootMotion.FinalIK.VRIK>();
                platform.ik = vrik;
                vrik.solver.spine.headTarget = headTarget;
                vrik.solver.leftArm.target = leftArmTarget;
                vrik.solver.rightArm.target = rightArmTarget;
                vrik.solver.leftLeg.target = leftLegTarget;
                vrik.solver.rightLeg.target = rightLegTarget;
                DrivingArmature = (Instantiate(md, transform) as GameObject).transform;
                DrivingArmature.name = "DrivingArmature";
                DrivingArmature.localPosition = DrivingModelLocalPos;
                PlayerStatus.healthBar = Armature.GetComponent<ModelCharacteristics>().HealthBar;
                if (isClient && !GameManager.Instance.DebugMode)
                {
                    Armature.GetComponent<ModelCharacteristics>().HealthBar.parent.gameObject.SetActive(false);
                    DrivingArmature.GetComponent<ModelCharacteristics>().HealthBar.parent.gameObject.SetActive(false);
                }
                var rigik = DrivingArmature.GetComponent<RigIK>();
                rigik.Head = CameraEye.transform;
                rigik.LeftHandTarget = ControllerLeft;
                rigik.RightHandTarget = ControllerRight;
                GetComponent<NetworkRun>().SetAnimator(Armature.GetComponent<Animator>());
                GetComponent<NetworkRun>().IK = vrik;
                GetComponent<NetworkRun>().LeftUpLeg = Armature.GetComponent<ModelCharacteristics>().LeftUpLeg;
                GetComponent<NetworkRun>().RightUpLeg = Armature.GetComponent<ModelCharacteristics>().RightUpLeg;
                GetComponent<NetworkRun>().LeftShoulder = Armature.GetComponent<ModelCharacteristics>().LeftShoulder;
                GetComponent<NetworkRun>().RightShoulder = Armature.GetComponent<ModelCharacteristics>().RightShoulder;
                GetComponent<NetworkHeightManager>().Rigs.Add(Armature);
                GetComponent<NetworkHeightManager>().ModelHeights.Add(Armature.GetComponent<ModelCharacteristics>().Height);
                GetComponent<NetworkHeightManager>().Rigs.Add(DrivingArmature);
                GetComponent<NetworkHeightManager>().ModelHeights.Add(DrivingArmature.GetComponent<ModelCharacteristics>().Height);
                vrik.enabled = true;
                rigik.enabled = true;
                GetComponent<NetworkRun>().enabled = true;
                GetComponent<NetworkHeightManager>().enabled = true;
                Armature.gameObject.SetActive(true);
                GetComponent<NetworkRun>().ReInit(); // serve per il VF che cambia mesh dopo aver già fatto lo start
                vrik.solver.Reset();
            }





            if (isLocalPlayer)
            {
                if (PlayerStatus.HideLocalPlayerRig && !GameManager.Instance.DebugMode)
                {
                    for (int i = 0; i < Armature.childCount; i++)
                        if (Armature.GetChild(i).GetComponent<SkinnedMeshRenderer>() != null)
                            Armature.GetChild(i).GetComponent<SkinnedMeshRenderer>().enabled = false;
                    if(DrivingArmature != null)
                    {
                        for (int i = 0; i < DrivingArmature.childCount; i++)
                            if (DrivingArmature.GetChild(i).GetComponent<SkinnedMeshRenderer>() != null)
                                DrivingArmature.GetChild(i).GetComponent<SkinnedMeshRenderer>().enabled = false;
                    }
                  

                }
            }
            if (OnPlayerModelSetEvent != null)
                OnPlayerModelSetEvent.Invoke();
        }
        PlayerModel = value;
    }

    internal void Reset()
    {
        if (isServer)
        {
            DeSpawn();
            if (isLocalPlayer)
            {
                AudioListener.volume = 0;
                PlayerStatus.PlayerCameraFadeOut(0);
            }
            else
                RpcReset();
        }
    }
    [ClientRpc]
    private void RpcReset()
    {
        if (isLocalPlayer)
        {
            AudioListener.volume = 0;
            PlayerStatus.PlayerCameraFadeOut(0);
        }
    } 

    public void SetPlayerModel(int value)
    {
        if (isServer)
        {
            if (isClient)
                PlayerModel = value;
            else
                OnPlayerModelSet(value);
        }
        else
            CmdSetPlayerModel(value);
    }

    [Command]
    private void CmdSetPlayerModel(int value)
    {
        if (isClient)
            PlayerModel = value;
        else
            OnPlayerModelSet(value);
    }

    public bool IsSpawned()
    {
        return spawned;
    }

    [Command]
    private void CmdPlatform(ControllerType platform)
    {
        Platform = platform;
    }

    private void Start()
    {
        spawnPoint = FindObjectOfType<NetworkStartPosition>();
        MainCamera = Camera.main;
        if (isServer)
        {
            if (isClient)
            {
                PlayerModel = 0;
                joined = false;
                spawned = false;
            }
            else
            {
                OnPlayerModelSet(0);
                OnJoinedChanged(false);
                OnSpawnedChanged(false);
            }
        }
        else
        {
            OnPlayerModelSet(PlayerModel);
            OnJoinedChanged(joined);
            OnSpawnedChanged(spawned);
        }

        if (isLocalPlayer)
        {
            AudioListener.volume = 0;
            SetPlayerModel(GameManager.Instance.PlayerModel);
            PlayerStatus.PlayerCameraFadeOut(0);
            Join();
        }
    }

    private void Join()
    {
        if (isServer)
        {
            if (isClient)
                joined = true;
            else
                OnJoinedChanged(true);
        }
        else
            CmdJoin();
    }
    [Command]
    private void CmdJoin()
    {
        if (isClient)
            joined = true;
        else
            OnJoinedChanged(true);
    }


    private void OnSpawnedChanged(bool value)
    {
        if (value)
        {
            if (!isLocalPlayer && Platform == ControllerType.MouseAndKeyboard)
                GetComponent<CharacterController>().enabled = true;
            PlayerStatus.StartWalking();
            PlayerStatus.InitialVehicleID();
            if (isLocalPlayer)
                PlayerStatus.PlayerCameraFadeIn(.35f);
            if (!PlayerStatus.IsInVehicle())
                Armature.gameObject.SetActive(true);
            else
                DrivingArmature.gameObject.SetActive(true);
        }
        spawned = value;
    }

    private void OnJoinedChanged(bool value)
    {
        if (value)
        {
            if (!isLocalPlayer)
            {
                if (Platform == ControllerType.MouseAndKeyboard)
                {
                    FPSCamera.transform.Find("Controller (right)").Find("Model").GetComponent<MeshRenderer>().enabled = false;
                    FPSCamera.transform.Find("Controller (left)").Find("Model").GetComponent<MeshRenderer>().enabled = false;
                    GetComponent<CharacterController>().enabled = false;                }
                else
                {
                    if (ShowVREquipment)
                        VRVisor.gameObject.SetActive(true);

                    CameraEye.transform.parent = CameraRig;
                    CameraHead.transform.parent = CameraEye.transform;
                    int c = CameraHead.transform.childCount;
                    for (int i = 0; i < c; i++)
                    {
                        var t = CameraHead.transform.GetChild(0);
                        var p = t.localPosition;
                        var r = t.localRotation;
                        t.parent = CameraEye.transform;
                        t.localPosition = p;
                        t.localRotation = r;
                    }
                    CameraHead.gameObject.SetActive(false);
                }
            }
            else
            {
                if (MainCamera != null)
                {
                    if (MainCamera.transform.parent.name == "Camera (head)")
                        MainCamera.transform.parent.parent.gameObject.SetActive(false);
                    else
                        MainCamera.transform.parent.gameObject.SetActive(false);
                }

                var ui = GameObject.FindGameObjectWithTag("MainMenuUI");
                if (ui != null)
                    ui.SetActive(false);
                
                if (Platform == ControllerType.MouseAndKeyboard)
                {
                    FPSCamera.enabled = true;
                    Listener.enabled = true;
                    MouseLook.enabled = true;
                    FPSInputController.enabled = true;
                    MouseController.enabled = true;
                    Crosshair.gameObject.SetActive(true);
                    FPSMouth.spatialize = false;
                }
                else
                {
                    SteamVRControllerManager.enabled = true;
                    SteamVRPlayArea.enabled = true;
                    SteamVRTrackedControllerLeft.enabled = true;
                    SteamVRTrackedControllerRight.enabled = true;
                    SteamVRTrackedObjectLeft.enabled = true;
                    SteamVRTrackedObjectRight.enabled = true;
                    SteamVRTrackedObjectHead.enabled = true;
                    VibrationLeft.enabled = true;
                    VibrationRight.enabled = true;
                    CameraEye.enabled = true;
                    CameraHead.enabled = true;
                    CameraEar.enabled = true;
                    SteamVRCamera.enabled = true;
                    SteamVREars.enabled = true;
                    VRMouth.spatialize = false;
                }
            PlayerStatus.PlayerCameraFadeIn(.35f);
            }
            GameManager.Instance.TCN.PlayerJoined(PlayerStatus);
        }
        joined = value;
    }

    public void SpawnPlayer(SpawnMode mode)
    {
        GameManager.Instance.TCN.RuoloImpostato(this, mode);
        if(mode == SpawnMode.Componente)
        {
            this.SetPlayerModel(3);
        }
        

        if (isServer)
        {
            if (isClient)
                spawned = true;
            else
                OnSpawnedChanged(true);
            //se sono server chiamo direttamente la funzione altrimenti il cmd
            SpawnPlayerAndComunicate(mode);



        }
        else
        {
            //dico al server che voglio nascere
            CmdSpawnPlayer(mode);
        }
            
    }

    void DeSpawn()
    {
        if (isServer)
        {
            if (isLocalPlayer)
            {
                transform.position = spawnPoint.transform.position;
                transform.rotation = spawnPoint.transform.rotation;
            }
            else
                RpcDeSpawn();

            if (isServer)
            {
                if (isClient)
                    spawned = false;
                else
                    OnSpawnedChanged(false);
            }

        }
    }
    [ClientRpc]
    void RpcDeSpawn()
    {
        if (isLocalPlayer)
        {
            transform.position = spawnPoint.transform.position;
            transform.rotation = spawnPoint.transform.rotation;
        }
    }


    [Command]
    private void CmdSpawnPlayer(SpawnMode mode)
    {
        if (isClient)
        {
            spawned = true;
        }
            
        else
            OnSpawnedChanged(true);
        SpawnPlayerAndComunicate(mode);
    }

    private void SpawnPlayerAndComunicate(SpawnMode mode)
    {
        GameManager.Instance.TCN.PlayerSpawned(PlayerStatus, mode);
    }

}
