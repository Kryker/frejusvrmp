﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ExtinguisherNet : GenericItemNet {

    Vector3 StartPos;
    Quaternion StartRot;
    public Transform Button;
    public Transform Sicura;
    [HideInInspector]
    [SyncVar(hook = "OnSecuredChanged")]
    public SecureState Secured = SecureState.Unknown;
    [SyncVar]
    [Range(0.0f, 100.0f)]
    public float Fuel = 100.0f;
    [HideInInspector]
    public BaseExt Base;
    [HideInInspector]
    [SyncVar]
    public bool OnBase;

    [SerializeField]
    protected bool isTheTruckExtinguisher = false;

    // Use this for initialization
    public override void Start() {
        base.Start();
        if (isServer)
        {
            if(isClient)
                Secured = SecureState.Closed;
            else
                OnSecuredChanged(SecureState.Closed);
        }
        else
            OnSecuredChanged(Secured);
    }
    private void Awake()
    {
        //ItemCode = ItemCodes.Extinguisher;
        StartPos = transform.position;
        StartRot = transform.rotation;
    }
    
    // Update is called once per frame
    public override void Update () {
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {

    }
    public void OnSecuredChanged(SecureState state)
    {
        if (state == SecureState.Closed)
            MettiSicura();
        else if(state == SecureState.Open)
            RimuoviSicura();
        
        Secured = state;
    }
    public override void UnClickButton(object sender, ClickedEventArgs e)
    {

    }

    public override void DropParent()
    {
        if (isServer)
        {
            if (Base != null && OnBase)
                ReBase();
            else
                Drop();
        }
    }

    internal void Drop()
    {
        base.DropParent();
    }

    internal void ReBase()
    {
        if (isServer)
        {
            transform.position = StartPos;
            transform.rotation = StartRot;
            Drop();
            if(!isClient)
                Base.DisableOutline();
            RpcDisableBaseOutline();
        }        
    }
    [ClientRpc]
    private void RpcDisableBaseOutline()
    {
        Base.DisableOutline();
    }

    public override void Reset()
    {
        transform.position = StartPos;
        transform.rotation = StartRot;
        if (isServer)
            {
            Drop();
            Fuel = 100.0f;
            if(isClient)
                Secured = SecureState.Closed;
            else
                OnSecuredChanged(SecureState.Closed);
            }
        Sicura.gameObject.SetActive(true);
        base.Reset();
    }

    void RimuoviSicura()
    {
        Sicura.gameObject.SetActive(false);
    }

    void MettiSicura()
    {
        Sicura.gameObject.SetActive(true);
    }
    public override void Interact(ItemControllerNet c, ControllerHand hand)
    {
       
        if (isTheTruckExtinguisher && c.GetComponent<PlayerStatusNet>().Ruolo == SpawnMode.TruckDriver)
        {

            GameManager.Instance.TCN.GPN.CmdEstintoreUsato(true, SpawnMode.TruckDriver);
        }
        base.Interact(c, hand);
    }

    public override void EnablePhysics()
    {
        if(Base == null || !OnBase)
            base.EnablePhysics();
    }
}
