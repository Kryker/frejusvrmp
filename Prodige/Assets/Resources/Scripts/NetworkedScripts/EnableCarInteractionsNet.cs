﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnableCarInteractionsNet : NetworkBehaviour
{

    VehicleDataNet Vehicle;
    public CarMovementScriptNet engine;
    [SyncVar(hook = "OnInteractionsEnabledChanged")]
    bool interactionsenabled;
    ManigliaPortaNet ExtLeftCarHandle;
    ManigliaPortaNet ExtRightCarHandle;
    public ManigliaPortaNet IntLeftCarHandle;
    ManigliaPortaNet IntRightCarHandle;
    public AudioSource _carEngineSound;
    FrecceNet Arrows;
    ChiaviNet Keys;
    public ForceBrakeNet forceBrake;
    public StopLimitCheckNet LimitCheck;
    public PreserveY LightLeft, LightRight;
    bool TCEventsSubscribed = false;
    // Use this for initialization

    void Awake()
    {
        ExtLeftCarHandle = IntLeftCarHandle.LinkedHandle;
        IntRightCarHandle = IntLeftCarHandle.OppositeHandle;
        ExtRightCarHandle = IntRightCarHandle.LinkedHandle;
        Vehicle = GetComponent<VehicleDataNet>();
        Arrows = GetComponent<FrecceNet>();
        Keys = GetComponent<ChiaviNet>();
        engine.CarStopped += EnableInteractions;
        engine.Braking += EngineSoundOn;
        engine.StopBraking += EngineSoundOff;
    }

    private void Start()
    {
        if (isServer)
        {
            if (isClient)
                interactionsenabled = false;
            else
                OnInteractionsEnabledChanged(false);
        }
        else
            OnInteractionsEnabledChanged(interactionsenabled);
    }

    private void OnInteractionsEnabledChanged(bool state)
    {
        if (state)
        {
            var body = transform.Find("Body");
            foreach (Collider c in body.GetComponents<Collider>())
                c.enabled = true;

            engine.Braking -= EngineSoundOn;
            engine.StopBraking -= EngineSoundOff;
            if (LightLeft != null)
                LightLeft.enabled = false;
            if (LightRight != null)
                LightRight.enabled = false;
        }
        interactionsenabled = state;
    }

    private void EngineSoundOn()
    {
        _carEngineSound.volume = 1;
    }
    private void EngineSoundOff()
    {
        _carEngineSound.volume = 0;
    }

    public void EnableInteractions()
    {
        if (isServer)
        {
            if (!interactionsenabled)
            {
                engine.StopEngine();
                GameManager.Instance.TCN.StopBraking();
                GameManager.Instance.TCN.CantBrake();
                if (Vehicle.LeftOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.LeftOccupant);
                    p.GetComponent<CharacterController>().enabled = true;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = true;
                    p.GetComponent<PlayerStatusNet>().VehicleStopped();
                }
                if (Vehicle.RightOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.RightOccupant);
                    p.GetComponent<CharacterController>().enabled = true;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = true;
                }

                ExtLeftCarHandle.SetCanInteract(true);
                ExtRightCarHandle.SetCanInteract(true);
                IntLeftCarHandle.SetCanInteract(true);
                IntRightCarHandle.SetCanInteract(true);
                Keys.SetCanInteract(true);
                Arrows.SetCanInteract(true);

                engine.Braking -= Vehicle.Brake;
                engine.StopBraking -= Vehicle.StopBraking;
                engine.BrakeDisabled.Invoke();
                engine.BrakeEnabled -= GameManager.Instance.TCN.CanBrake;
                engine.BrakeDisabled -= GameManager.Instance.TCN.CantBrake;
                engine.Braking -= GameManager.Instance.TCN.Brake;
                engine.StopBraking -= GameManager.Instance.TCN.StopBraking;
                GameManager.Instance.TCN.SetBrochureAvailable(Vehicle.LeftOccupant);

                if (!isClient)
                {
                    var body = transform.Find("Body");
                    foreach (Collider c in body.GetComponents<Collider>())
                        c.enabled = true;

                    engine.Braking -= EngineSoundOn;
                    engine.StopBraking -= EngineSoundOff;

                    if (LightLeft != null)
                        LightLeft.enabled = false;
                    if (LightRight != null)
                        LightRight.enabled = false;
                }

                if (isClient)
                    interactionsenabled = true;
                else
                    OnInteractionsEnabledChanged(true);
            }
        }
    }

    public void Reset()
    {
        DisableInteractions();
    }

    public void DisableInteractions()
    {
        if (isServer)
        {
            if (interactionsenabled)
            {
                engine.StartEngine();
                if (Vehicle.LeftOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.LeftOccupant);
                    p.GetComponent<CharacterController>().enabled = false;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = false;
                    p.GetComponent<PlayerStatusNet>().VehicleStarted();
                }
                if (Vehicle.RightOccupant != NetworkInstanceId.Invalid)
                {
                    var p = NetworkServer.FindLocalObject(Vehicle.RightOccupant);
                    p.GetComponent<CharacterController>().enabled = false;
                    p.GetComponent<PlayerStatusNet>().GetHead().enabled = false;
                }

                ExtLeftCarHandle.SetCanInteract(false);
                ExtRightCarHandle.SetCanInteract(false);
                IntLeftCarHandle.SetCanInteract(false);
                IntRightCarHandle.SetCanInteract(false);
                Keys.SetCanInteract(false);
                Arrows.SetCanInteract(false);

                engine.Braking += Vehicle.Brake;
                engine.StopBraking += Vehicle.StopBraking;
                engine.BrakeEnabled.Invoke();
                engine.BrakeEnabled += GameManager.Instance.TCN.CanBrake;
                engine.BrakeDisabled += GameManager.Instance.TCN.CantBrake;
                engine.Braking += GameManager.Instance.TCN.Brake;
                engine.StopBraking += GameManager.Instance.TCN.StopBraking;
                GameManager.Instance.TCN.SetBrochureUnavailable(Vehicle.LeftOccupant);

                if (!isClient)
                {
                    var body = transform.Find("Body");
                    foreach (Collider c in body.GetComponents<Collider>())
                        c.enabled = false;

                    engine.Braking += EngineSoundOn;
                    engine.StopBraking += EngineSoundOff;

                    if (LightLeft != null)
                        LightLeft.enabled = true;
                    if (LightRight != null)
                        LightRight.enabled = true;
                }

                if (isClient)
                    interactionsenabled = false;
                else
                    OnInteractionsEnabledChanged(false);
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (isServer && !TCEventsSubscribed && GameManager.Instance.TCN != null)
        {
            engine.Braking += Vehicle.Brake;
            engine.Braking += GameManager.Instance.TCN.Brake;
            engine.StopBraking += Vehicle.StopBraking;
            engine.StopBraking += GameManager.Instance.TCN.StopBraking;
            engine.BrakeEnabled += GameManager.Instance.TCN.CanBrake;
            //engine.StopEnabled += GameManager.Instance.TCN.CanStop; ora comanda il camion
            engine.BrakeDisabled += GameManager.Instance.TCN.CantBrake;
            engine.BrakeDisabled += Vehicle.StopBraking;
            TCEventsSubscribed = true;
        }
    }
}
