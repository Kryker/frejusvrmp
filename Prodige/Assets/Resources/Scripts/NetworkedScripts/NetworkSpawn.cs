﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public enum SpawnMode { None, Driver, Passenger, Tunnel, TruckDriver, Componente };
public class NetworkSpawn : NetworkBehaviour {

    [SyncVar(hook = "OnDriverButtonAvailableChanged")]
    bool DriverButtonAvailable = true;
    [SyncVar(hook = "OnTruckDriverButtonAvailableChanged")]
    bool TruckDriverButtonAvailable = true;
    [SyncVar(hook = "OnComponenteButtonAvailableChanged")]
    bool ComponenteButtonAvailable = true;

    public VRButtonController Driver, TruckDriver, Componente; //Passenger,  Tunnel
    public VRUICanvas SpawnMenu, ExitMenu;

    void Start() {
        Driver.onClick += DriverClicked;
        TruckDriver.onClick += TruckClicked;
        Componente.onClick += ComponenteClicked;
        if (isServer)
        {
            if (isClient)
            { 
                DriverButtonAvailable = true;
                TruckDriverButtonAvailable = true;
                ComponenteButtonAvailable = true;
            }
            
        }
        else
        {
            OnDriverButtonAvailableChanged(DriverButtonAvailable);
            OnTruckDriverButtonAvailableChanged(TruckDriverButtonAvailable);
            OnComponenteButtonAvailableChanged(ComponenteButtonAvailable);
        }

    }
    public void OnDriverButtonAvailableChanged(bool value)
    {
        if (value)
            Driver.ElementActive();
        else
            Driver.ElementNotActive();
        DriverButtonAvailable = value;
    }

    public void OnTruckDriverButtonAvailableChanged(bool value)
    {
        if (value)
            TruckDriver.ElementActive();
        else
            TruckDriver.ElementNotActive();
        TruckDriverButtonAvailable = value;
    }

    public void OnComponenteButtonAvailableChanged(bool value)
    {
        if (value)
        {
            Componente.ElementActive();
        }
        else
        { Componente.ElementNotActive(); }

        ComponenteButtonAvailable = value;
    }

    public void DriverClicked(object sender)
    {
        if(DriverButtonAvailable)
        {
            RuoloImpostato(SpawnMode.Driver, sender);
        }
    }

    internal void Reset()
    {
        if (isServer)
        {
            if (isClient)
            {
                DriverButtonAvailable = true;
                TruckDriverButtonAvailable = true;
                ComponenteButtonAvailable = true;
            }
            else
            {
                OnDriverButtonAvailableChanged(true);
                OnTruckDriverButtonAvailableChanged(true);
                OnComponenteButtonAvailableChanged(true);
            }
        }
    }

    public void TruckClicked(object sender)
    {
        if (TruckDriverButtonAvailable)
        {
            RuoloImpostato(SpawnMode.TruckDriver, sender);
        }
    }

    public void ComponenteClicked(object sender)
    {
        if (ComponenteButtonAvailable)
        {
            RuoloImpostato(SpawnMode.Componente, sender);
        }
    }

    private void RuoloImpostato(SpawnMode sm, object sender)
    {
        PlayerStatusNet p = null;
        ItemControllerNet i = null;
        SteamVR_TrackedController t = null;

        try
        {
            t = (SteamVR_TrackedController)sender;
            var c = t.GetComponent<ControllerManager>();
            if (c != null)
            {
                p = c.ControllerNet.GetComponent<PlayerStatusNet>();
                p.Ruolo = sm;

            }
        }
        catch (InvalidCastException)
        {
            try
            {
                i = (ItemControllerNet)sender;
                p = i.GetComponent<PlayerStatusNet>();
                p.Ruolo = sm;

            }
            catch (InvalidCastException)
            { }
        }
        p.Spawner.SpawnPlayer(sm);
    }

    public void SetDriverButtonAvailable(bool state)
    {
        if (isServer)
        {
            if (isClient)
                DriverButtonAvailable = state;
            else
                OnDriverButtonAvailableChanged(state);
        }
    }

    public void SetTruckButtonAvailable(bool state)
    {
        if (isServer)
        {
            if (isClient)
                TruckDriverButtonAvailable = state;
            else
                OnTruckDriverButtonAvailableChanged(state);
        }
    }
    public void SetComponenteButtonAvailable(bool state)
    {
        if (isServer)
        {
            if (isClient)
               ComponenteButtonAvailable = state;
            else
                OnComponenteButtonAvailableChanged(state);
        }
    }
}
