﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Reflection;
using UnityEngine.SceneManagement;
using System;
//subclass for sending network messages
public class CustomNetworkMessage : MessageBase
{
    public ControllerType Platform;
}

//This makes the GameObject a NetworkManager GameObject
public class OverriddenNetworkManager : NetworkManager
{
    public bool m_ServerStarted, m_ClientStarted;
    //public Button m_ClientButton;
    MainMenu Menu;
    public GameObject FPSPrefab, ArmSwingPrefab, FootSwingPrefab, CVirtPrefab, KatWalkPrefab, FreeWalkPrefab;
    private void Start()
    {
        SceneManager.sceneLoaded += LevelWasLoaded;
    }

    private void LevelWasLoaded(Scene arg0, LoadSceneMode arg1)
    {
        
            var s = GameObject.FindGameObjectWithTag("ServerOnlyPC");
            if (s != null)
            {
            if (GameManager.Instance.Server == true)
            {
                /*if (GlobalControl.Instance.Platform != ControllerType.MouseAndKeyboard)
                    s.transform.Find("MainMenuHTCViveController").gameObject.SetActive(true);
                else
                    s.transform.Find("First Person Main Menu Controller").gameObject.SetActive(true);*/
                s.transform.Find("ServerOnlyPlayerController").gameObject.SetActive(true);
            }
            else
                s.gameObject.SetActive(false);
            }
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
        /*NetworkMessage pmessage = new NetworkMessage();
        pmessage.Platform = GlobalControl.Instance.Platform;*/
        CustomNetworkMessage message = extraMessageReader.ReadMessage<CustomNetworkMessage>();
        ControllerType selectedPlatform = message.Platform;
        var s = FindObjectOfType<NetworkStartPosition>();
        GameObject player, selectedPrefab = null;
        switch(selectedPlatform)
        {
            case ControllerType.ArmSwing:
                selectedPrefab = ArmSwingPrefab;
                break;
            case ControllerType.FootSwing:
                selectedPrefab = FootSwingPrefab;
                break;
            case ControllerType.CVirtualizer:
                selectedPrefab = CVirtPrefab;
                break;
            case ControllerType.KatWalk:
                selectedPrefab = KatWalkPrefab;
                break;
            case ControllerType.FreeWalk:
                selectedPrefab = FreeWalkPrefab;
                break;
            default:
                selectedPrefab = FPSPrefab;
                break;
        }
        if (s != null)
            player = Instantiate(selectedPrefab, s.transform.position, s.transform.rotation);
        else
            player = Instantiate(selectedPrefab, Vector3.zero, Quaternion.identity);
        if (NetworkServer.AddPlayerForConnection(conn, player, playerControllerId))
        { 
            //Debug.Log("Player " + playerControllerId + " spawned.");
        }
        else
        {
            //Debug.Log("Player not spawned.");
        }

    }

    //Detect when a client connects to the Server
    public override void OnClientConnect(NetworkConnection conn)
    {
        m_ClientStarted = true;
        //Output text to show the connection on the client side
        Debug.Log("Client Side : Client " + conn.connectionId + " Connected!");

        //Register and receive the message on the Client's side (NetworkConnection.Send Example)
        //client.RegisterHandler(MsgType.Ready, ReadyMessage);
        if (Menu == null)
        {
            var m = GameObject.FindGameObjectWithTag("MainMenuUI");
            if (m != null)
                { 
                Menu = m.GetComponent<MainMenu>();
                Menu.Connect();
                }
        }
        else
            Menu.Connect();
    }
    //Use this to receive the message from the Server on the Client's side
    public void ReadyMessage(NetworkMessage networkMessage)
    {
        //Debug.Log("Client Ready! ");
    }

    //Detect when a client disconnects from the Server
    public override void OnClientDisconnect(NetworkConnection connection)
    {
        //Change the text to show the connection loss on the client side
        Debug.Log("Client Side : Client " + connection.connectionId + " Lost!");
        m_ClientStarted = false;
        if (Menu == null)
        {
            var m = GameObject.FindGameObjectWithTag("MainMenuUI");
            if (m != null)
            {
                Menu = m.GetComponent<MainMenu>();
                Menu.Disconnect();
            }
        }
        else
            Menu.Disconnect();
    }

    /*public override void OnServerConnect(NetworkConnection conn)
    {
        NetworkServer.SetClientReady(conn);
        SetClientReadyIncludeInactive(conn);
    }*/

    /*private void SetClientReadyIncludeInactive(NetworkConnection conn)
    {
        NetworkServer.SetClientReady(conn);

        //tell client to spawn inactive objects, because SetClientReady wont
        AddObserverToInactive(conn);
    }*/
    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        CustomNetworkMessage pmessage = new CustomNetworkMessage();
        pmessage.Platform = GameManager.Instance.Platform;
        ClientScene.Ready(conn);
        ClientScene.AddPlayer(conn, 0, pmessage);
        //base.OnClientSceneChanged(conn);
    }
    public override void OnServerReady(NetworkConnection conn)
    {
        AddObserverToInactive(conn);
        base.OnServerReady(conn);
    }
   
    private void AddObserverToInactive(NetworkConnection conn)
    {
        if (conn.connectionId == 0)
        {
            //local host has observer added to inactive NetworkBehaviours by SetClientReady already
            return;
        }

        foreach (NetworkIdentity netId in NetworkServer.objects.Values)
        {
            if (netId == null)
            {
                Debug.LogError("Trying to add observer to object of null NetworkIdentity.", this);
                continue;
            }
            if (!netId.gameObject.activeSelf)
            {
                MethodInfo OnCheckObserver = typeof(NetworkIdentity).GetMethod("OnCheckObserver", BindingFlags.NonPublic | BindingFlags.Instance);
                MethodInfo AddObserver = typeof(NetworkIdentity).GetMethod("AddObserver", BindingFlags.NonPublic | BindingFlags.Instance);

                if ((bool)OnCheckObserver.Invoke(netId, new object[] { conn }))
                {
                    AddObserver.Invoke(netId, new object[] { conn });
                }
            }
        }
    }

    /* public void ClientButton()
     {
         if (!m_ClientStarted)
         {
             NetworkServer.Reset();
             singleton.StartClient();
             m_ClientButton.GetComponentInChildren<Text>().text = "Disconnect";
         }
         else
         {
             singleton.StopClient();
         }
     }*/
}