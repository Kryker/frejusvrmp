﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum TunnelFireType { NoFire, SmallFire, MidFire, LargeFire};
public enum TunnelSectionType { Default, Telephone, Sherter, Bypass }
public enum Posizione { Destra, Sinistra, Rifugio };

public class TunnelSectionControllerNet : NetworkBehaviour
    {
    public Transform TunnelFirePrefab;
    [SyncVar]
    NetworkInstanceId fireid;
    Transform fire;
    public PulsanteChiamataSOSNet PulsanteSOSSX, PulsanteSOSDX;
    [SyncVar]
    bool onfire;
    [SyncVar(hook = "OnLightsOnChanged")]
    bool lightson;
    [HideInInspector]
    public CanBeOnFireNet Fire;

    public virtual void Start()
    {
        GameManager.Instance.TCN.TunnelSections.Add(this);
        Fire = GetComponent<CanBeOnFireNet>();

        if (isServer)
        {
            onfire = false;
            if (isClient)
                lightson = true;
            else
                OnLightsOnChanged(true);

            if (Fire != null)
                Fire.OnFireStarted += StartFire;
        }
        else
            OnLightsOnChanged(lightson);

        if (PulsanteSOSDX != null)
            PulsanteSOSDX.posizione = Posizione.Destra;
        if (PulsanteSOSSX != null)
            PulsanteSOSSX.posizione = Posizione.Sinistra;
    }

    // Update is called once per frame
    void Update () {
		
	}
    public void StartFire (CanBeOnFireNet c)
        {
        if(isServer && c == Fire)
            StartFire();
        }

    public void StartFire()
    {
        if (isServer && !onfire && Fire.enabled)
        {
            fire = Instantiate(TunnelFirePrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            NetworkServer.Spawn(fire.gameObject);

            onfire = true;
            if (isClient)
                lightson = false;
            else
                OnLightsOnChanged(false);
            
            GameManager.Instance.TCN.FireStarted(fire.GetComponent<TunnelFireControllerNet>());
            Fire.enabled = false;
        }
    }
    public virtual void Reset()
    {
        if (isServer)
            {
            if (fire != null)
                Destroy(fire.gameObject);

            onfire = false;
            if (isClient)
                lightson = true;
            else
                OnLightsOnChanged(true);
            }
    }

    void OnLightsOnChanged(bool state)
    {
        if(state)
        {
            transform.Find("LampioniAccesi").gameObject.SetActive(true);
            transform.Find("LampioniSpenti").gameObject.SetActive(false);
        }
        else
        {
            transform.Find("LampioniAccesi").gameObject.SetActive(false);
            transform.Find("LampioniSpenti").gameObject.SetActive(true);
        }
        lightson = state;
    }

    public virtual void LightsOn(bool active)
    {
        if (isServer && active != lightson)
        {
            if (active)
            {
                transform.Find("LampioniAccesi").gameObject.SetActive(true);
                transform.Find("LampioniSpenti").gameObject.SetActive(false);
            }
            else
            {
                transform.Find("LampioniAccesi").gameObject.SetActive(false);
                transform.Find("LampioniSpenti").gameObject.SetActive(true);
            }
        }
    }
}
