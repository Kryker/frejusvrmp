﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TruckControllerNet : VehicleDataNet {
    public Transform FuelTank;
    public List<Transform> Wheels;
    public float WheelBurnFactor = 1.0f;
    public List<Transform> Doors;
    public float DoorBurnFactor = 1.0f;
    public Transform Tarp;
    public float TarpBurnFactor = 3.0f;
    public float VehicleBurnFactor = 0.1f;
    public bool loaded = false;
    public Transform Load;
    public float LoadBurnFactor=0.05f;
    public float TankBurnFactor = 1.0f;
    public float BaseDuration = 20.0f;
    public ParticleSystem fumoCamionFrontale;

    public List<GameObject> ToActivateOnDestination;
    public List<Collider> ToDisableOnDestination;
    CanBeOnFireNet[] listaFuochi;
    // Use this for initialization
    public override void Start()
        {
            listaFuochi = GetComponentsInChildren<CanBeOnFireNet>();
        /*
            VehicleBody.GetComponent<CanBeOnFireSlave>().MasterNet.BurnFactor = VehicleBurnFactor;
            VehicleBody.GetComponent<CanBeOnFireSlave>().MasterNet.duration = 1 / VehicleBurnFactor * BaseDuration;
            Tarp.GetComponent<CanBeOnFireSlave>().MasterNet.BurnFactor = TarpBurnFactor;
            Tarp.GetComponent<CanBeOnFireSlave>().MasterNet.duration = 1 / TarpBurnFactor * BaseDuration;
            foreach (Transform t in Doors)
            {
                t.GetComponent<CanBeOnFireSlave>().MasterNet.BurnFactor = DoorBurnFactor;
                t.GetComponent<CanBeOnFireSlave>().MasterNet.duration = 1 / DoorBurnFactor * BaseDuration;
            }
            foreach (Transform t in Wheels)
            {
                t.GetComponent<CanBeOnFireSlave>().MasterNet.BurnFactor = WheelBurnFactor;
                t.GetComponent<CanBeOnFireSlave>().MasterNet.duration = 1 / WheelBurnFactor * BaseDuration;
            }
            if(FuelTank != null)
        {
            FuelTank.GetComponent<CanBeOnFireSlave>().MasterNet.BurnFactor = TankBurnFactor;
            FuelTank.GetComponent<CanBeOnFireSlave>().MasterNet.duration = 1 / TankBurnFactor * BaseDuration;
        }
        */
        this.gameObject.GetComponent<CarNPC>().MezzoFermo += UscitaAbilitata;
        //this.gameObject.GetComponent<CarMovementScriptNet>().CarStopped += UscitaAbilitata;

        base.Start();
    }

    // Update is called once per frame
    public override void Update () {
        /*if (FuelTank.GetComponent<CanBeOnFire>().Health <= 0 && vehicle.GetComponent<VehicleData>().FuelTankPercentage>20.0f)
            Explode(vehicle.GetComponent<VehicleData>().FuelTankPercentage);*/
        base.Update();
    }



    protected override void OnLeftOccupantChangedReal(NetworkInstanceId value)
    {
        if (isServer)
        {
            if (GameManager.Instance.TCN.GameStarted)
            {
                if (RightOccupant != NetworkInstanceId.Invalid)
                {
                    //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(false);
                    if (value == NetworkInstanceId.Invalid)
                        GameManager.Instance.TCN.Spawn.SetTruckButtonAvailable(true);
                    else
                        GameManager.Instance.TCN.Spawn.SetTruckButtonAvailable(false);
                }
                else
                {
                    if (value == NetworkInstanceId.Invalid)
                    {
                        //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(true);
                        //GameManager.Instance.TCN.Spawn.SetPassengerButtonAvailable(false);
                    }
                    GameManager.Instance.TCN.Spawn.SetTruckButtonAvailable(false);

                }
            }
            else
            {
                //GameManager.Instance.TCN.Spawn.SetTunnelButtonAvailable(false);
                if (value == NetworkInstanceId.Invalid)
                    GameManager.Instance.TCN.Spawn.SetTruckButtonAvailable(true);
                else
                    GameManager.Instance.TCN.Spawn.SetTruckButtonAvailable(false);
            }
        }

        LeftOccupant = value;
        //Debug.LogError("cambio player Left truck: "+value.ToString());
    }

    protected override void OnRightOccupantChangedReal(NetworkInstanceId value)
    {
      // non c'è posto destro!
    }

    //eseguito sul server
    public void ActivateSmoke()
    {
        LocalActivateSmoke();
        RpcActivateSmoke();
        GameManager.Instance.TCN.StartedSmokeTruck();
    }
    public void LocalActivateSmoke()
    {
        fumoCamionFrontale.Play();
        StartCoroutine(SpegniFumo());
    }

    IEnumerator SpegniFumo()
    {
        yield return new WaitForSeconds(15);
        fumoCamionFrontale.Stop();
    }

    [ClientRpc]
    private void RpcActivateSmoke()
    {
        LocalActivateSmoke();
    }
    /*
    public void DisableSmoke()
    {
        var tmp = fumoCamionFrontale.emission;
        tmp.enabled = false;
    }*/

    public void SetKinematic(bool state)
    {
        this.GetComponent<Rigidbody>().isKinematic = state;
    }

    public void UscitaAbilitata()
    {
        foreach(var e in ToActivateOnDestination)
        {
            var p = e.GetComponents<Collider>();
            foreach(var ei in p)
            {
                ei.enabled = true;
            }
        }

     foreach(var e in ToDisableOnDestination)
        {
          
           e.enabled = false;

        }

        StartCoroutine(SpegniLuciPerLeFiamme());
       
    }

    IEnumerator SpegniLuciPerLeFiamme()
    {
        yield return new WaitForSeconds(15);
        TurnLightsOff();
    }

  

    public Slider controlloreFuocoServer;
    //di base 0.5f
    public void EditFire()
    {
        float val = controlloreFuocoServer.value;
        //controlloreFuocoServer.value;
        foreach (var el in listaFuochi)
        {
            el.Conductivity = val;
        }
    }
}
