﻿using UnityEngine;
using UnityEngine.VR;

public class LimitTrackingLocal : MonoBehaviour
{
    public bool IgnoreTarget = false;
    public Vector3 Target;
    Vector3 StartPos, PrevPos, PrevLocal;
    [Range(0.0f, 5.0f)]
    public float Xmax, Ymax, Zmax;
    [Range(-5.0f, 0.0f)]
    public float Xmin, Ymin, Zmin;
    public Transform HTCViveController;
    public Transform CameraEye;
    public bool IgnoreY;
    bool prevposset = false;

    private void Start()
    {
        StartPos = InputTracking.GetLocalPosition(VRNode.CenterEye);
        PrevPos = StartPos;
        if (IgnoreTarget)
            Target = StartPos;
    }

    void Update()
    {
        var newlocalpos = InputTracking.GetLocalPosition(VRNode.CenterEye);      
        
        if (prevposset && PrevLocal == newlocalpos)
        {
            transform.localPosition = PrevPos;
        }
        else
        {
            float x, y, z;

            if (PrevLocal.x != newlocalpos.x)                             //se è cambiata la Target.localPositionizione dal frame precedente
            {
                if (StartPos.x != newlocalpos.x)                                //se è diversa dalla Target.localPositionizione di partenza
                {
                    var offs = StartPos.x - newlocalpos.x;

                    if (newlocalpos.x > StartPos.x + Xmax)
                        x = Target.x - newlocalpos.x + Xmax;
                    else if (newlocalpos.x < StartPos.x + Xmin)
                        x = Target.x - newlocalpos.x + Xmin;
                    else
                        x = Target.x - newlocalpos.x - offs;
                }
                else
                    x = StartPos.x - newlocalpos.x;
            }
            else
                x = PrevPos.x;

            if (!IgnoreY)
            {
                if (PrevLocal.y != newlocalpos.y)                             //se è cambiata la Target.localPositionizione dal frame precedente
                {
                    if (StartPos.y != newlocalpos.y)                                //se è diversa dalla Target.localPositionizione di partenza
                    {
                        var offs = StartPos.y - newlocalpos.y;

                        if (newlocalpos.y > StartPos.y + Ymax)
                            y = Target.y - newlocalpos.y + Ymax;
                        else if (newlocalpos.y < StartPos.y + Ymin)
                            y = Target.y - newlocalpos.y + Ymin;
                        else
                            y = Target.y - newlocalpos.y - offs;
                    }
                    else
                        y = StartPos.y - newlocalpos.y;
                }
                else
                    y = PrevPos.y;
            }
            else
                y = transform.localPosition.y;

            if (PrevLocal.z != newlocalpos.z)                             //se è cambiata la Target.localPositionizione dal frame precedente
            {
                if (StartPos.z != newlocalpos.z)                                //se è diversa dalla Target.localPositionizione di partenza
                {
                    var offs = StartPos.z - newlocalpos.z;

                    if (newlocalpos.z > StartPos.z + Zmax)
                        z = Target.z - newlocalpos.z + Zmax;
                    else if (newlocalpos.z < StartPos.z + Zmin)
                        z = Target.z - newlocalpos.z + Zmin;
                    else
                        z = Target.z - newlocalpos.z - offs;
                }
                else
                    z = StartPos.z - newlocalpos.z;
            }
            else
                z = PrevPos.z;


            PrevLocal = newlocalpos;
            
            transform.localPosition = new Vector3(x, y, z);

            PrevPos = transform.localPosition;
            prevposset = true;
        }        
        //transform.rotation = Quaternion.Inverse(InputTracking.GetLocalRotation(VRNode.CenterEye));
    }
}
