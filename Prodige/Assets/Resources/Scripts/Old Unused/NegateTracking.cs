﻿using UnityEngine;
using UnityEngine.VR;

public class NegateTracking : MonoBehaviour
{
    public Vector3 Pos;
    public Transform MainMenuHTCViveController, CameraHolder, HTCViveController, CameraEye;

    void Update()
    {
        var newlocalpos = -InputTracking.GetLocalPosition(VRNode.CenterEye);
        var newglobalpos = HTCViveController.localToWorldMatrix * (newlocalpos);
        if (transform.parent == null)
            transform.position = new Vector3(Pos.x + newglobalpos.x, Pos.y + newglobalpos.y, Pos.z + newglobalpos.z);
        else
            {
            var par = transform.parent;
            var newpos = new Vector3(Pos.x + newglobalpos.x, Pos.y + newglobalpos.y, Pos.z + newglobalpos.z);
            while (par != null)
            {
                newpos = par.localToWorldMatrix * (newpos);
                par = par.parent;
            }
            transform.position = newpos;
            }

        //transform.rotation = Quaternion.Inverse(InputTracking.GetLocalRotation(VRNode.CenterEye));
    }
}
