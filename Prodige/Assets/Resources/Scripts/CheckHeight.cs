﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckHeight : MonoBehaviour
{

    // Use this for initialization
    bool CheckEnabled = false;
    public VRSelectableObject[] Avatars;
    public MainMenu Menu;
    public SteamVR_TrackedController LeftController, RightController;
    [SerializeField]
    float _heightMin = 1.35f;
    void Start()
    {
        if (!UnityEngine.VR.VRDevice.isPresent)
        {
            foreach (VRSelectableObject s in Avatars)
                s.ElementActive();
        }
        else
            CheckEnabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckEnabled)
        {
            if (UnityEngine.VR.InputTracking.GetLocalPosition(UnityEngine.VR.VRNode.CenterEye).y >= _heightMin)
            {
                Menu.ForcedHeight = false;
                foreach (VRSelectableObject s in Avatars)
                    s.ElementActive();
            }
            else if (GameManager.Instance.DebugMode && LeftController.gripped && RightController.gripped)
            {
                Menu.ForcedHeight = true;
                foreach (VRSelectableObject s in Avatars)
                    s.ElementActive();
            }
            else
            {
                foreach (VRSelectableObject s in Avatars)
                    s.ElementNotActive();
            }
        }
    }
}
