﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*public enum TunnelFireType { NoFire, SmallFire, MidFire, LargeFire};
public enum TunnelSectionType { Default, Telephone, Sherter, Bypass }
public enum Posizione { Destra, Sinistra, Rifugio };*/

public class TunnelSectionController : MonoBehaviour
    {
    public Transform TunnelFirePrefab;
    Transform fire = null;
    public PulsanteChiamataSOS PulsanteSOSSX, PulsanteSOSDX;
    bool onfire = false;
    [HideInInspector]
    public CanBeOnFire Fire;
    [HideInInspector]
    public TunnelController TC;
	// Use this for initialization
	void OnEnable () {
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
        {
            TC = tc.GetComponent<TunnelController>();
            if (TC != null)
                TC.TunnelSections.Add(this);
        }
	}

    public virtual void Start()
    {
        Fire = GetComponent<CanBeOnFire>();

        if (Fire != null)
            Fire.OnFireStarted += StartFire;
        if (PulsanteSOSDX != null)
            PulsanteSOSDX.posizione = Posizione.Destra;
        if (PulsanteSOSSX != null)
            PulsanteSOSSX.posizione = Posizione.Sinistra;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StartFire(CanBeOnFire c)
    {
        if (c == Fire)
            StartFire();
    }

    public void StartFire()
        {
        if (!onfire && Fire.enabled)
        {
            fire = Instantiate(TunnelFirePrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            if (TC.TunnelOnFire)
                fire.GetComponent<TunnelFireController>().Smoke = false;
            fire.parent = this.transform;
            onfire = true;
            if (TC != null)
                TC.FireStarted(fire.GetComponent<TunnelFireController>());
            Fire.enabled = false;
        }
        }

    public virtual void Reset()
    {
        LightsOn(true);
        if (fire != null)
            {
            Destroy(fire.gameObject);
            fire = null;
        }
        onfire = false;
    }

    public Transform GetFire()
        {
        return fire;
        }

    public virtual void LightsOn(bool active)
        {
        if (active)
            {
            /*foreach (Transform t in Lights)
                {*/
                /*t*/transform.Find("LampioniAccesi").gameObject.SetActive(true);
                /*t*/transform.Find("LampioniSpenti").gameObject.SetActive(false);
                //}
            }
        else
            {
            /*foreach(Transform t in Lights)
                {*/
                /*t*/transform.Find("LampioniAccesi").gameObject.SetActive(false);
                /*t*/transform.Find("LampioniSpenti").gameObject.SetActive(true);
                //}
            }
        }
}
