﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableCarInteractions : MonoBehaviour {

    //AdvancedStateMachineBehaviour behaviour;
    //Animator animator;
    VehicleData Vehicle;
    public CarMovementScript engine;
    bool interactionsenabled = false;
    ManigliaPorta ExtLeftCarHandle;
    ManigliaPorta ExtRightCarHandle;
    public ManigliaPorta IntLeftCarHandle;
    ManigliaPorta IntRightCarHandle;
    public AudioSource _carEngineSound;
    Frecce Arrows;
    Chiavi Keys;
    TunnelController tc;
    public ForceBrakeNet forceBrake;
    public StopLimitCheckNet LimitCheck;
    public PreserveY LightLeft, LightRight;
    bool TCEventsSubscribed = false;
    // Use this for initialization

    void Awake()
    {
        ExtLeftCarHandle = IntLeftCarHandle.LinkedHandle;
        IntRightCarHandle = IntLeftCarHandle.OppositeHandle;
        ExtRightCarHandle = IntRightCarHandle.LinkedHandle;
        /*animator = GetComponent<Animator>();
        behaviour = animator.GetBehaviour<AdvancedStateMachineBehaviour>();
        behaviour.StatePlayed += EnableInteractions;*/
        Vehicle = GetComponent<VehicleData>();
        Arrows = GetComponent<Frecce>();
        Keys = GetComponent<Chiavi>();
        var t = GameObject.FindGameObjectWithTag("TunnelController");
        if(t!=null)
            tc =t.GetComponent<TunnelController>();
        //CarMovingSound = a[2];
        engine.CarStopped += EnableInteractions;       
        engine.Braking += EngineSoundOn;
        engine.StopBraking += EngineSoundOff;
    }

    private void EngineSoundOn()
    {
        _carEngineSound.volume = 1;
    }
    private void EngineSoundOff()
    {
        _carEngineSound.volume = 0;
    }

    private void EnableInteractions(/*AdvancedStateMachineBehaviour a*/)
    {
        if (!interactionsenabled)
        {
            tc.StopBraking();
            Vehicle.GetComponent<CarController>().enabled = false;
            tc.SpegniMani();
			tc.PiediAnimator.SetBool ("Stop", true);
			tc.PedaliCarAnimator.SetBool("Stop", true);
            engine.GetComponent<CarMovementScript>().Stop();

            //if ((GlobalControl.Instance != null && GlobalControl.Instance.Platform == ControllerType.MouseAndKeyboard) || (tc.Platform == ControllerType.MouseAndKeyboard))
                tc.GetPlayerController().GetComponent<CharacterController>().enabled = true;
            /*else
                tc.GetPlayerController().GetComponent<CapsuleCollider>().enabled = true;*/

            ExtLeftCarHandle.SetCanInteract(true);
            ExtRightCarHandle.SetCanInteract(true);
            IntLeftCarHandle.SetCanInteract(true);
            IntRightCarHandle.SetCanInteract(true);
            Keys.SetCanInteract(true);
            Arrows.SetCanInteract(true);
            //CarMovingSound.Stop();

            var body = transform.Find("Body");
            foreach (Collider c in body.GetComponents<Collider>())
                c.enabled = true;
            if (tc != null)
            {
                engine.Braking -= Vehicle.Brake;
                engine.StopBraking -= Vehicle.StopBraking;
                engine.BrakeDisabled.Invoke();
                engine.BrakeEnabled -= tc.CanBrake;
                /*engine.Braking -= tc.CantBrake;
                engine.StopBraking -= tc.CanBrake;*/
                engine.BrakeDisabled -= tc.CantBrake;
                engine.Braking -= tc.Brake;
                engine.StopBraking -= tc.StopBraking;
                tc.BrochureAvailable();
                tc.GetPlayerController().GetComponent<PlayerStatus>().MouthCollider.enabled = true;
            }
            engine.Braking -= EngineSoundOn;
            engine.StopBraking -= EngineSoundOff;
            if (LimitCheck.gameObject.activeSelf)
                LimitCheck.gameObject.SetActive(false);
            if (forceBrake.gameObject.activeSelf)
                forceBrake.gameObject.SetActive(false);
            if (LightLeft != null)
                LightLeft.enabled = false;
            if (LightRight != null)
                LightRight.enabled = false;
            interactionsenabled = true;
        }
        
    }


    // Update is called once per frame
    void Update () {
		if(!TCEventsSubscribed)
        {
            if (tc != null && tc.GetPlayerController() != null)
            {
                engine.Braking += Vehicle.Brake;
                engine.Braking += tc.Brake;
                engine.StopBraking += Vehicle.StopBraking;
                engine.StopBraking += tc.StopBraking;
                engine.BrakeEnabled += tc.CanBrake;
                engine.StopEnabled += tc.CanStop;
                engine.BrakeDisabled += tc.CantBrake;
                engine.BrakeDisabled += Vehicle.StopBraking;
                /*engine.Braking += tc.CantBrake;
                engine.StopBraking += tc.CanBrake;*/
                TCEventsSubscribed = true;
            }
        }
	}
}
