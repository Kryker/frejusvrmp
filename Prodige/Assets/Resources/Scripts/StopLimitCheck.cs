﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopLimitCheck : MonoBehaviour {

    StageController SC;
    // Use this for initialization
    void Start () {
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
            SC = tc.GetComponent<StageController>();
        else
            gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null)
        { 
            var c = other.transform.parent.GetComponentInParent<CarMovementScript>();
            if (c != null)
            {
                SC.DistanceMaintained = false;
                gameObject.SetActive(false);
            }
        }
    }
}
