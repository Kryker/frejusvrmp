﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Collider))]
public class SicuraExt : GenericItem
{
    [HideInInspector]
    public Extinguisher Ext;
    public AudioSource audiosource;
    GrabbedExtinguisher GrabbedExt;
    Vector3 InitialPos, InitialRot;
    bool up = true;
    float t = 0; 
    bool PulseEnabled;
    MeshRenderer MR = null;
    public Shader Outline;
    Material outlinedbutton = null;
    Material oldmat = null;
    Coroutine resetandhide;
    // Use this for initialization
    
    public override void Start () {
        ItemCode = ItemCodes.ExtSecure;
        audiosource = Slave.GetComponent<AudioSource>();
        GrabbedExt = GetComponent<GrabbedExtinguisher>();
        MR = Slave.GetComponent<MeshRenderer>();
        oldmat = MR.materials[0];
        InitialPos = Slave.transform.localPosition;
        InitialRot = Slave.transform.localEulerAngles;
        outlinedbutton = new Material(Outline);
        outlinedbutton.SetTexture("_MainTex", oldmat.GetTexture("_MainTex"));
        outlinedbutton.SetColor("_OutlineColor", oldmat.GetColor("_OutlineColor"));
        outlinedbutton.SetFloat("_Outline", oldmat.GetFloat("_Outline"));
        base.Start();
	}
    public void Hide()
    {
        StopPulse();
        MR.enabled = false;
        Slave.GetComponent<Collider>().enabled = false;
        audiosource.enabled = false;
    }
    public void Show()
    {
        MR.enabled = true;
        Slave.GetComponent<Collider>().enabled = true;
        audiosource.enabled = true;
    }

    public override void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        Interact(c, hand);
    }
    public override void ClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override void Reset()
    {
        base.Reset();
        Show();
        Slave.GetComponent<Collider>().enabled = false;
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            GrabbedExt.RimuoviSicura();
        }
    }

    internal void PlaySound()
    {
        audiosource.Play();
    }

    public override void EnableOutline(ItemController c)
    {
        StopPulse();
        base.EnableOutline(c);
    }

    public override void DisableOutline(ItemController c)
    {
        base.DisableOutline(c);
        if(CanInteract(c))
            StartPulse();
    }

    public override void Update()
    {
        Pulse();
    }

    public void RimuoviSicura()
    {
        StopPulse();
        SetCanInteract(false);
        ItemActive = false;
        transform.parent = null;
        Slave.transform.GetComponent<Rigidbody>().isKinematic = false;

        resetandhide = StartCoroutine(ResetAndHide());
        GrabbedExt.StartPulse();
    }

    private IEnumerator ResetAndHide()
    {
        StopPulse();
        yield return new WaitForSeconds(1.5f);
        Hide();
        Slave.transform.GetComponent<Rigidbody>().isKinematic = true;
        Slave.transform.localPosition = InitialPos;
        Slave.transform.localRotation = Quaternion.Euler(InitialRot);
        resetandhide = null;
    }

    internal void MettiSicura()
    {
        SetCanInteract(true);
        ItemActive = true;
        if (resetandhide != null)
            StopCoroutine(resetandhide);
        Slave.transform.GetComponent<Rigidbody>().isKinematic = true;
        Slave.transform.localPosition = InitialPos;
        Slave.transform.localRotation = Quaternion.Euler(InitialRot);
        Show();        
        StartPulse();
        GrabbedExt.StopPulse();
    }

    public void StartPulse()
    {
        if (!PulseEnabled)
        {
            var m = new Material[1];
            m[0] = outlinedbutton;
            MR.materials = m;
            PulseEnabled = true;
        }
    }
    public void StopPulse()
    {
        var m = new Material[1];
        m[0] = oldmat;

        if(MR!=null)
            MR.materials = m;
        PulseEnabled = false;
    }

    void Pulse()
    {
        if (PulseEnabled)
        {
                if (up)
                {
                    t += Time.deltaTime / 0.5f;
                    var m1 = MR.materials[0];
                    var c = m1.GetColor("_OutlineColor");
                    m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(0, 1, t)));
                    var m = new Material[1];
                    m[0] = m1;
                    MR.materials = m;
                    if (t >= 1)
                    {
                        up = false;
                        t = 0;
                    }
                }
                else
                {
                    t += Time.deltaTime / 0.5f;
                    var m1 = MR.materials[0];
                    var c = m1.GetColor("_OutlineColor");
                    m1.SetColor("_OutlineColor", new Color(c.r, c.g, c.b, Mathf.Lerp(1, 0, t)));
                    var m = new Material[1];
                    m[0] = m1;
                    MR.materials = m;
                    if (t >= 1)
                    {
                        up = true;
                        t = 0;
                    }
                }
            }
        }

}
