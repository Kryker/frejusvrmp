﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanBeOnFire : MonoBehaviour
{
    public delegate void FireDelegate(CanBeOnFire c);
    public CanBeOnFireSlave Slave;
    public List<Transform> FireEffects;
    ParticleSystem part;
    public Light[] lights;
    public float lightsIntensity = 0;
    public float duration = 20.0f;
    public float IgnitionTemperature = 100.0f;
    [Range(0.0f, 2.0f)]
    public float Conductivity = 1.0f;
    public bool BurnFromTheBeginning = false;
    public Transform LinkedObject;
    public List<Transform> SubPieces;
    public float BurnFactor = 1.0f;
    AudioSource burningsound;
    public FireDelegate OnFireStarted, OnFireStopped;

    public AnimationCurve TextureInterpolationCurve = new AnimationCurve(new Keyframe(0, 0, 0, 1), new Keyframe(1, 1, 1, 0));
    [HideInInspector]
    public List<FireEffectController> parts;
    float NextOp;
    TunnelController TC;
    public float Temperature;
    public bool wet;
    [Range(-1.0f, 100.0f)]
    public float Health;
    public bool burning;
    public bool burned;
    [HideInInspector]
    public GameObject SmartDestroy;

    private void StartFire()
    {
        if (burningsound != null && !burningsound.isPlaying)
            burningsound.Play();

        LightIntensity(lightsIntensity);
        if (part != null)
        {
            part.Play();
            //SendMessageUpwards("FireStarted", part, SendMessageOptions.DontRequireReceiver);
        }
        else if (FireEffects.Count > 0)
        {
            parts = new List<FireEffectController>();
            foreach (Transform t in FireEffects)
            {
                var p = t.GetComponent<FireEffectController>();
                if (p != null)
                {
                    p.EnableLoop();
                    p.Play();
                    parts.Add(p);
                }
            }
            //SendMessageUpwards("FireStarted", this, SendMessageOptions.DontRequireReceiver);
        }

        if (OnFireStarted != null)
            OnFireStarted.Invoke(this);
    }
    
    private void Awake()
    {
        NextOp = 0;
        if (Slave != null)
            {
            Slave.Master = this;
            burningsound = Slave.GetComponent<AudioSource>();
            }
        else
            burningsound = GetComponent<AudioSource>();

        if (FireEffects.Count == 0)
        {
            if (Slave != null)
                part = Slave.GetComponent<ParticleSystem>();
            else
                part = GetComponent<ParticleSystem>();
        }
    }

    private void OnEnable()
    {
        if (Slave != null)
            Slave.enabled = true;
    }
    private void OnDisable()
    {
        if (Slave != null)
            Slave.enabled = false;
    }
    void StopFire()
    {
        if (burningsound != null && burningsound.isPlaying)
            burningsound.Stop();
        List<FireEffectController> parts = new List<FireEffectController>();

        if (part != null)
        {
            part.Stop();
        }
        else
        {
            foreach (Transform t in FireEffects)
            {
                var p = t.GetComponent<FireEffectController>();
                if (p != null)
                {
                    p.Stop();
                    parts.Add(p);
                }
            }
        }
        LightIntensity(0);

        if (OnFireStopped != null)
            OnFireStopped.Invoke(this);
    }

    // Use this for initialization
    void Start()
    {
        SmartDestroy = GameObject.FindGameObjectWithTag("SmartDestroy");
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
            {
            TC = tc.GetComponent<TunnelController>();
            OnFireStarted += TC.UpFireCount;
            OnFireStopped += TC.DownFireCount;
        }
        for (int l = 0; l < lights.Length; l++)
        {
            lights[l].intensity = 0;
        }
        
        if (BurnFromTheBeginning)
        {
            Temperature = IgnitionTemperature + 2;
            Fire();
        }
        else if (Temperature == 0 || Temperature < TC.GetAmbientTemp())
            Temperature = TC.GetAmbientTemp();
       
    }
    public void SetTemperature(float v)
    {
        Temperature = v;
    }

    float GetHealth()
    {
        return Health;
    }
    bool IsBurned()
    {
        return burned;
    }

    private void Fire()
    {
        if (Temperature > IgnitionTemperature && !burning && !wet)
        {
            StartFire();
            burning = true;
        }
    }
    private void Burn()
    {
        if (!burned)
        {
            StopFire();
            //Burn();
            burning = false;
            NextOp = 0;
            Health = 0;
            burned = true;

            try
            {
                if (LinkedObject != null)
                    LinkedObject.gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                else
                {
                    if(Slave != null)
                        Slave.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                    else
                        GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                }


                if (SubPieces != null)
                    foreach (Transform t in SubPieces)
                    {
                        t.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", 0);
                    }

                if (gameObject.isStatic || Slave != null)
                {

                    if (tag != "Undestroyable" || Slave != null)
                    {
                        if (LinkedObject != null && LinkedObject.tag != "Undestroyable")
                            LinkedObject.gameObject.SetActive(false);

                        if (Slave != null)
                            Slave.gameObject.SetActive(false);
                        else
                            gameObject.SetActive(false);

                        if (SubPieces != null)
                            foreach (Transform t in SubPieces)
                            {
                                if (t.tag != "Undestroyable")
                                    LinkedObject.gameObject.SetActive(false);
                            }
                    }

                }
                else if (tag != "Undestroyable")
                {
                    if (LinkedObject != null && LinkedObject.tag != "Undestroyable")
                        SmartDestroy.SendMessage("Add", LinkedObject.gameObject);

                    SmartDestroy.SendMessage("Add", gameObject);
                    if (SubPieces != null)
                        foreach (Transform t in SubPieces)
                        {
                            if (tag != "Undestroyable")
                                SmartDestroy.SendMessage("Add", t.gameObject);
                        }
                }
            }
            catch (NullReferenceException e)
            {
                Debug.Log(e.Message + " in object " + transform.name);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        var now = Time.time;
        if (NextOp <= now)
        {
            float ambient = 24.0f;
            if (TC != null)
                ambient = TC.GetAmbientTemp();

            if (enabled)
            {
                if (Temperature > ambient)
                    Temperature -= 1 * Conductivity * UnityEngine.Random.Range(0.5f, 1.5f);

                if (burning)
                {
                    Temperature += 1.5f * Conductivity * UnityEngine.Random.Range(0.5f, 1.5f);
                    Health -= BurnFactor;

                    UpdateFire();

                    if (Temperature < IgnitionTemperature)
                        GetWet();
                }
                else if (Temperature >= IgnitionTemperature && !wet && !burned)
                    Fire();

                if (Health <= 0 && !burned)
                    Burn();
            }
            NextOp = now + 1;
        }
    }
    
    private void GetWet()
    {
        wet = true;
        StopFire();
        Invoke("Dry", 10);
    }
    internal void WarmUp(float targettemp)
    {
        if (enabled)
        {
            if (targettemp > Temperature)
                Temperature += Conductivity;
        }
    }
    internal void Cooldown(float cooldowncoeff)
    {
        if (enabled)
        {
            if (TC == null)
                TC = GameObject.FindGameObjectWithTag("TunnelController").GetComponent<TunnelController>();

            float ambient = 24.0f;
            if (TC != null)
                ambient = TC.GetAmbientTemp();

            if (ambient < Temperature)
            {
                Temperature -= Conductivity * cooldowncoeff;
            }
        }
    }
    void Extinguish()
    {
        burning = false;
        StopFire();
        NextOp = 0;
    }
    internal void ForceBurn()
    {
        if (!burned && !burning)
        {
            Temperature = IgnitionTemperature + 2;
            Fire();
        }
    }
    void Dry()
    {
        wet = false;
    }
    bool IsOnFire()
    {
        return burning;
    }
    void LightIntensity(float to)
    {
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].intensity = to;
        }
    }

    internal void UpdateFire()
    {
        var lerp = TextureInterpolationCurve.Evaluate(Health / 100);
        if (LinkedObject != null)
            LinkedObject.gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", lerp);
        else
        {
            if (Slave != null)
                Slave.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", lerp);

            else
                GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", lerp);
        }

        if (SubPieces != null)
            foreach (Transform t in SubPieces)
            {
                t.gameObject.GetComponent<MeshRenderer>().materials[0].SetFloat("_Element1Level", lerp);
            }
    }
}
