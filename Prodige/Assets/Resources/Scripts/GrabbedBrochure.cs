﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbedBrochure : GrabbableItem
{
    // Use this for initialization
    [HideInInspector]
    public AudioSource source;
    public override void Start()
    {
        base.Start();
        source = Slave.GetComponent<AudioSource>();
    }
    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }
    public override void ClickButton(object sender, ClickedEventArgs e)
    { }
    
    public override void LoadState()
    {
        base.LoadState();
        if (source != null)
            source.Play();
    }
    public override void SaveState()
    {
        if (gameObject.activeSelf)
            StartCoroutine(PlayAndDisable());
    }
    
    private IEnumerator PlayAndDisable()
    {
        if (source != null)
        {
            source.Play();
            yield return new WaitWhile(() => source.isPlaying);
        }
        base.SaveState();
    }
}
