﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanBeOnFireSlave : MonoBehaviour {

    [HideInInspector]
    public CanBeOnFireNet MasterNet;
    [HideInInspector]
    public CanBeOnFire Master;

    public void WarmUp(float targettemp)
    {
        if (Master != null)
            Master.WarmUp(targettemp);
        else if (MasterNet != null)
            MasterNet.WarmUp(targettemp);
    }
    public void Cooldown(float cooldowncoeff)
    {
        if (Master != null)
            Master.Cooldown(cooldowncoeff);
        else if (MasterNet != null)
            MasterNet.Cooldown(cooldowncoeff);
    }
    
    internal void SetTemperature(int v)
    {
        if (Master != null)
            Master.SetTemperature(v);
        else if (MasterNet != null)
            MasterNet.SetTemperature(v);
    }
    
    internal float GetTemperature()
    {
        if (Master != null)
            return Master.Temperature;
        else if (MasterNet != null)
            return MasterNet.Temperature;
        return float.MinValue;
    }

    internal void ForceBurn()
    {
        if (Master != null)
            Master.ForceBurn();
        else if (MasterNet != null)
            MasterNet.ForceBurn();
    }
}
