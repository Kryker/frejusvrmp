﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Clacson : MonoBehaviour {
    
    AudioSource source;
	// Use this for initialization
	void Start () {
        source = GetComponents<AudioSource>()[0];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && !source.isPlaying)
        {
            source.Play();
        }
    }
}
