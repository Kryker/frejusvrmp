﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semaforo : MonoBehaviour {

    public Material MaterialOff;
    Material MaterialVerde, MaterialGiallo, MaterialRosso;
    Transform Verde, Rosso, Giallo;
    public enum Stato { Spento, Verde, Rosso, Giallo};
    public Stato InitialState;
    [HideInInspector]
    public Stato CurrState;
    bool initial = true;
	// Use this for initialization
	void Awake () {
        Verde = transform.Find("Verde");
        MaterialVerde = Verde.GetComponent<MeshRenderer>().materials[0];
        Rosso = transform.Find("Rosso");
        MaterialRosso = Rosso.GetComponent<MeshRenderer>().materials[0];
        Giallo = transform.Find("Giallo");
        MaterialGiallo = Giallo.GetComponent<MeshRenderer>().materials[0];
    }

    private void Start()
    {
        SetState(InitialState);
    }

    public void SetState(Stato State)
    {
        if (CurrState != State || initial)
        {
            switch (State)
            {
                case Stato.Verde:
                    SemaforoVerdeOn();
                    break;
                case Stato.Rosso:
                    SemaforoRossoOn();
                    break;
                case Stato.Giallo:
                    SemaforoGialloOn();
                    break;
                default:
                    SemaforoOff();
                    break;
            }
            CurrState = State;
            if (initial)
                initial = false;
        }
    }

    private void SemaforoVerdeOn()
    {
    if (!enabled)
        return;
    var mts = new Material[1];
    mts[0] = MaterialVerde;
    if (Verde != null)
        Verde.GetComponent<MeshRenderer>().materials = mts;
    mts = new Material[1];
    mts[0] = MaterialOff;
    if (Giallo != null)
        Giallo.GetComponent<MeshRenderer>().materials = mts;
    mts = new Material[1];
    mts[0] = MaterialOff;
    if (Rosso != null)
        Rosso.GetComponent<MeshRenderer>().materials = mts;
    }

    private void SemaforoRossoOn()
    {
        if (!enabled)
            return;
        var mts = new Material[1];
        mts[0] = MaterialOff;
        if(Verde != null)
            Verde.GetComponent<MeshRenderer>().materials = mts;
        mts = new Material[1];
        mts[0] = MaterialOff;
        if (Giallo != null)
            Giallo.GetComponent<MeshRenderer>().materials = mts;
        mts = new Material[1];
        mts[0] = MaterialRosso;
        if (Rosso != null)
            Rosso.GetComponent<MeshRenderer>().materials = mts;
    }
    private void SemaforoGialloOn()
    {
        if (!enabled)
            return;
        var mts = new Material[1];
        mts[0] = MaterialOff;
        if (Verde != null)
            Verde.GetComponent<MeshRenderer>().materials = mts;
        mts = new Material[1];
        mts[0] = MaterialGiallo;
        if (Giallo != null)
            Giallo.GetComponent<MeshRenderer>().materials = mts;
        mts = new Material[1];
        mts[0] = MaterialOff;
        if (Rosso != null)
            Rosso.GetComponent<MeshRenderer>().materials = mts;
    }
    private void SemaforoOff()
    {
        if (!enabled)
            return;
        var mts = new Material[1];
        mts[0] = MaterialOff;
        if (Verde != null)
            Verde.GetComponent<MeshRenderer>().materials = mts;
        mts = new Material[1];
        mts[0] = MaterialOff;
        if (Giallo != null)
            Giallo.GetComponent<MeshRenderer>().materials = mts;
        mts = new Material[1];
        mts[0] = MaterialOff;
        if (Rosso != null)
            Rosso.GetComponent<MeshRenderer>().materials = mts;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
