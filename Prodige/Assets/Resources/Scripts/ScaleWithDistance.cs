﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleWithDistance : MonoBehaviour {
    
    public float MaxScale = 3;
    public float MaxDistanceForMinScale = 1;
    public float MaxDistanceForScaleGrowth = 150;
    PlayerStatusNet pc;
    TunnelControllerNet TC;
    bool scaling = true;
    // Use this for initialization
    void Start () {

        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
        {
            TC = tc.GetComponent<TunnelControllerNet>();
            if (TC != null)
            {
                TC.Luci.Add(this);
                pc = TC.GetPlayerController();
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(TC == null)
        {
            var tc = GameObject.FindGameObjectWithTag("TunnelController");
            if (tc != null)
            {
                TC = tc.GetComponent<TunnelControllerNet>();
                if (TC != null)
                {
                    TC.Luci.Add(this);
                    pc = TC.GetPlayerController();
                }
            }
        }
        if (TC != null && pc == null)
            pc = TC.GetPlayerController();
        if (scaling && pc != null && pc.Ruolo != SpawnMode.Componente)
        {
            var distance = Vector3.Distance(transform.position, pc.transform.position);
            if (distance >= MaxDistanceForScaleGrowth && transform.localScale != new Vector3(MaxScale, MaxScale, MaxScale))
                transform.localScale = new Vector3(MaxScale, MaxScale, MaxScale);
            else if (distance < MaxDistanceForScaleGrowth && distance >= MaxDistanceForMinScale)
            {
                var rate = (distance - MaxDistanceForMinScale) / (MaxDistanceForScaleGrowth - MaxDistanceForMinScale);
                if (transform.localScale != new Vector3(1 + (MaxScale - 1) * rate, 1 + (MaxScale - 1) * rate, 1 + (MaxScale - 1) * rate))
                    transform.localScale = new Vector3(1 + (MaxScale - 1) * rate, 1 + (MaxScale - 1) * rate, 1 + (MaxScale - 1) * rate);
            }
            else if (distance < MaxDistanceForMinScale && transform.localScale != new Vector3(1, 1, 1))
                transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void StopScaling()
    {
        if(scaling)
        {
            scaling = false;
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}
