﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FirstPersonMainView : MonoBehaviour {

    public Transform FPSCamera;
    Transform currentelement = null;
    public Image Crosshair;
    public Sprite BaseCrosshair;
    public Sprite CyanCrosshair;
    public Sprite MagentaCrosshair;
    public Sprite YellowCrosshair;
    public Sprite RainbowCrosshair;
    public delegate void MyDelegate(object sender, ClickedEventArgs e);
    public MyDelegate OnButtonClicked, OnButtonUnClicked;
    public AudioSource Source;
    public enum CrosshairState { None, Default, Cyan, Magenta, Yellow, Rainbow };
    CrosshairState CurrentCrosshair = CrosshairState.Default;
    CrosshairState ForcedCrosshair = CrosshairState.None;
    public ControllerButtonInput ButtonToInteract = ControllerButtonInput.Trigger;
    public RayCastUtil RayCaster;

    void SetCrosshair(CrosshairState s)
    {
        switch (s)
        {
            case CrosshairState.Cyan:
                Crosshair.sprite = CyanCrosshair;
                break;
            case CrosshairState.Magenta:
                Crosshair.sprite = MagentaCrosshair;
                break;
            case CrosshairState.Yellow:
                Crosshair.sprite = YellowCrosshair;
                break;
            case CrosshairState.Rainbow:
                Crosshair.sprite = RainbowCrosshair;
                break;
            default:
                Crosshair.sprite = BaseCrosshair;
                break;
        }
        CurrentCrosshair = s;
    }

    private void OnDisable()
    {
        SetCrosshair(CrosshairState.Default);
    }

    public void SetCrosshairPublic(CrosshairState s)
    {
        switch (s)
        {
            case CrosshairState.Cyan:
                Crosshair.sprite = CyanCrosshair;
                break;
            case CrosshairState.Magenta:
                Crosshair.sprite = MagentaCrosshair;
                break;
            case CrosshairState.Yellow:
                Crosshair.sprite = MagentaCrosshair;
                break;
            case CrosshairState.Rainbow:
                Crosshair.sprite = RainbowCrosshair;
                break;
            default:
                Crosshair.sprite = BaseCrosshair;
                break;
        }
        ForcedCrosshair = s;
    }

    // Use this for initialization
    void Start () {
       // Source = GetComponent<AudioSource>();
    }
    
    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        var mask = LayerMask.GetMask("UI");
        if (RayCaster.RayCast(out hit, 100.0F, mask))
        {
            /*var buttonhit = hit.collider.transform.GetComponent<VRButtonController>();
            var togglehit = hit.collider.transform.GetComponent<VRToggleController>();
            var inputfieldhit = hit.collider.transform.GetComponent<VRInputFieldController>();*/
            var uihit = hit.collider.transform.GetComponent<VRUIElement>();

            if (uihit != null && uihit.IsElementActive() && currentelement != uihit.transform && !uihit.transition)
            {
                if (currentelement != null)
                {
                    var b = currentelement.GetComponent<VRUIElement>();
                        b.UnSelected();                    
                }
                currentelement = uihit.transform;
                currentelement.GetComponent<VRUIElement>().Selected(this);
                OnButtonClicked += ClickEffect;
                switch (ButtonToInteract)
                {
                    case ControllerButtonInput.Any:
                        SetCrosshair(CrosshairState.Rainbow);
                        break;
                    case ControllerButtonInput.Pad:
                        SetCrosshair(CrosshairState.Yellow);
                        break;
                    case ControllerButtonInput.Grip:
                        SetCrosshair(CrosshairState.Cyan);
                        break;
                    default:
                        SetCrosshair(CrosshairState.Magenta);
                        break;
                }
            }
            else if (uihit == null || !uihit.IsElementActive() || uihit.transition)
            {
                if (currentelement != null)
                {
                    var b = currentelement.GetComponent<VRUIElement>();
                    b.UnSelected();
                 
                }
                currentelement = null;
                OnButtonClicked -= ClickEffect;

                if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                    SetCrosshair(ForcedCrosshair);
                else if (ForcedCrosshair == CrosshairState.None)
                    SetCrosshair(CrosshairState.Default);
            }
        }
        else
        {
            if (currentelement != null)
            {
                var b = currentelement.GetComponent<VRUIElement>();

                if (!b.transition)
                {
                    b.UnSelected();
                    currentelement = null;
                    if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                        SetCrosshair(ForcedCrosshair);
                    else
                        SetCrosshair(CrosshairState.Default);
                }

                if (!b.IsElementActive() || !b.gameObject.activeSelf)
                {
                    b.UnSelected();
                    currentelement = null;
                    if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                        SetCrosshair(ForcedCrosshair);
                    else
                        SetCrosshair(CrosshairState.Default);
                }
                else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
                {
                    SetCrosshair(ForcedCrosshair);
                }
                else
                    SetCrosshair(CrosshairState.Default);

                currentelement = null;
                OnButtonClicked -= ClickEffect;
            }
            else if (ForcedCrosshair != CrosshairState.None && CurrentCrosshair != ForcedCrosshair)
            {
                SetCrosshair(ForcedCrosshair);
            }
        }

        if (Input.GetButtonDown("Fire Left Mouse") || Input.GetButtonDown("Fire Right Mouse"))
        {
            if (OnButtonClicked != null)
                OnButtonClicked.Invoke(null, new ClickedEventArgs());
        }
        else if (Input.GetButtonUp("Fire Left Mouse") || Input.GetButtonUp("Fire Right Mouse"))
        {
            if (OnButtonUnClicked != null)
                OnButtonUnClicked.Invoke(null, new ClickedEventArgs());
        }
    }

    public void ClickEffect(object sender, ClickedEventArgs e)
    {
        if (Source.isPlaying)
            Source.Stop();
        Source.Play();
    }
}
