﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastUtil : MonoBehaviour
{
    float nextray = 0;
    public bool DebugDrayRay = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public RaycastHit[] RayCastAll(float maxDistance, int layerMask)
    {
        Ray ray = new Ray(transform.position, transform.rotation * Vector3.forward);

        if (DebugDrayRay && nextray < Time.time)
        {
            Debug.Log("Ray: " + transform.position + " " + transform.rotation * Vector3.forward);
            Debug.DrawRay(transform.position, transform.rotation * Vector3.forward, Color.red, 0.05f, false);
            nextray = Time.time + 0.05f;
        }

        return Physics.RaycastAll(ray, maxDistance, layerMask);
    }

    public bool RayCast(out RaycastHit hitInfo, float maxDistance, int layerMask)
    {
        Ray ray = new Ray(transform.position, transform.rotation * Vector3.forward);

        if (DebugDrayRay && nextray < Time.time)
        {
            Debug.Log("Ray: " + transform.position + " " + transform.rotation * Vector3.forward);
            Debug.DrawRay(transform.position, transform.rotation * Vector3.forward, Color.red, 0.05f, false);
            nextray = Time.time + 0.05f;
        }

        return Physics.Raycast(ray, out hitInfo, maxDistance, layerMask);
    }
}
