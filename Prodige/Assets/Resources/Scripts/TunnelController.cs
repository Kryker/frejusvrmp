﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*public enum WindDirection { Favorable, Headwind, Windless};
public enum ControllerType { MouseAndKeyboard, ArmSwing, FootSwing, CVirtualizer, KatWalk };*/

public class TunnelController : MonoBehaviour {
    

    WindDirection WindDir = WindDirection.Headwind;
    Transform WindZone;
    public Transform FPSControllerPrefab;  
    public Transform FPSNoWalkControllerPrefab;
    bool lightscaling = true;
    public Transform ArmSwingControllerPrefab, ArmSwingNoWalkControllerPrefab;
    public Transform FootSwingControllerPrefab, FootSwingNoWalkControllerPrefab;
    public Transform CVirtualizerControllerPrefab, CVirtualizerNoWalkControllerPrefab;
    public Transform KatWalkControllerPrefab, KatWalkNoWalkControllerPrefab;
    public TunnelSectionController FireSection;
    public bool CubeMappingEnabled = false;
    public List<CubemapperUtil> CubesToMap;
    [HideInInspector]
    public List<ScaleWithDistance> Luci = new List<ScaleWithDistance>();
    bool once = false;
    public VehicleData DynamicCar;
    public TruckController Camion;
    bool Inside = true;
    public Transform Mani, Piedi;
    public Animator PiediAnimator;
    public Animator PedaliCarAnimator;
    StageController SC;
    bool PlayerFront = true;
    PlayerStatus playerController;
    public float AmbientTemp = 24.0f;
    [HideInInspector]
    AudioSource ambientsound;
    [HideInInspector]
    public AudioSourceFader ambientfader;
    VibrationController leftv, rightv;
    public List<TunnelShelterSectionController> Rifugi;
    public List<TunnelTelephoneSectionController> Nicchie;
    public ControllerType Platform = ControllerType.MouseAndKeyboard;
    public WindDirection InitialWindDirection = WindDirection.Headwind;
    public CarSpawn Spawner;
    TunnelFireController TunnelFire;
    [HideInInspector]
    public List<TunnelSectionController> TunnelSections;
    public LocalizerController Localizer;
    [HideInInspector]
    public bool TunnelOnFire = false;
    public string MainMenuScene = "MainMenu";
    public CanBeOnFireSlave Telone;
    private bool fireproceduresenabled = false;
    bool braking = false;
    public Vector3 BrakeVibrParameters = new Vector3(0.1f, 0.1f, 0.1f);

    public ScaleParticleWithDistance Fuoco;
    public CanBeOnFireSlave FuocoCamion;

    bool VRCheckDone = false;
    
    private void Awake()
    {
        if(GameManager.Instance != null)
            GameManager.Instance.TC = this;
        SC = GetComponent<StageController>();
        ambientsound = GetComponents<AudioSource>()[0];
        ambientfader = GetComponents<AudioSourceFader>()[0];
    }

    public void ManageCarHandleInteraction(VehicleSeat Seat)
    {
        if (Inside)
            StartCoroutine(GoOutOfTheCar());
        else
            StartCoroutine(GoInsideCar(Seat));
    }

    public void StopLightScaling()
    {
        lightscaling = false;
        foreach (ScaleWithDistance l in Luci)
            l.StopScaling();
        if (Fuoco != null)
            Fuoco.StopScaling();
    }
    void Start()
    {
        if (GameManager.Instance != null)
            Localizer.SelectedLanguage = GameManager.Instance.AppLanguage;
        Localizer.ApplyLanguage();

        SteamVRFade.fadeOut(0);
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Default"), LayerMask.NameToLayer("TunnelIgnore"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLayer"), LayerMask.NameToLayer("TunnelIgnore"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("TunnelIgnore"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("Fire"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("ControllerLayer"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("Terrain"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLimit"), LayerMask.NameToLayer("Default"), true);
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerLayer"), LayerMask.NameToLayer("MovingItem"), true);
        
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

        switch (platform)
        {
            case ControllerType.MouseAndKeyboard:
                var p = Instantiate(FPSNoWalkControllerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                playerController = p.GetComponent<PlayerStatus>();
                playerController.GetComponent<FakeParenting>().SetFakeParent(DynamicCar.SeatedSpawnLeft.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                DynamicCar.SeatedSpawnLeft.Occupant = playerController.GetComponent<PlayerStatus>().transform;
                playerController.GetComponent<PlayerStatus>().CurrentSeat = DynamicCar.SeatedSpawnLeft;
                playerController.GetComponent<PlayerStatus>().CurrentVehicle = DynamicCar;
                break;
            case ControllerType.CVirtualizer:
                p = Instantiate(CVirtualizerNoWalkControllerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                playerController = p.GetComponent<PlayerStatus>();
                playerController.GetComponent<FakeParenting>().SetFakeParent(DynamicCar.SeatedSpawnLeft.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                DynamicCar.SeatedSpawnLeft.Occupant = playerController.GetComponent<PlayerStatus>().transform;
                playerController.GetComponent<PlayerStatus>().CurrentSeat = DynamicCar.SeatedSpawnLeft;
                playerController.GetComponent<PlayerStatus>().CurrentVehicle = DynamicCar;
                break;
            case ControllerType.FootSwing:
                p = Instantiate(FootSwingNoWalkControllerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                playerController = p.GetComponent<PlayerStatus>();
                playerController.GetComponent<FakeParenting>().SetFakeParent(DynamicCar.SeatedSpawnLeft.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                DynamicCar.SeatedSpawnLeft.Occupant = playerController.GetComponent<PlayerStatus>().transform;
                playerController.GetComponent<PlayerStatus>().CurrentSeat = DynamicCar.SeatedSpawnLeft;
                playerController.GetComponent<PlayerStatus>().CurrentVehicle = DynamicCar;
                break;
            case ControllerType.ArmSwing:
                p = Instantiate(ArmSwingNoWalkControllerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                playerController = p.GetComponent<PlayerStatus>();
                playerController.GetComponent<FakeParenting>().SetFakeParent(DynamicCar.SeatedSpawnLeft.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                DynamicCar.SeatedSpawnLeft.Occupant = playerController.GetComponent<PlayerStatus>().transform;
                playerController.GetComponent<PlayerStatus>().CurrentSeat = DynamicCar.SeatedSpawnLeft;
                playerController.GetComponent<PlayerStatus>().CurrentVehicle = DynamicCar;
                break;
            case ControllerType.KatWalk:
                p = Instantiate(KatWalkNoWalkControllerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
                playerController = p.GetComponent<PlayerStatus>();
                playerController.GetComponent<FakeParenting>().SetFakeParent(DynamicCar.SeatedSpawnLeft.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                DynamicCar.SeatedSpawnLeft.Occupant = playerController.GetComponent<PlayerStatus>().transform;
                playerController.GetComponent<PlayerStatus>().CurrentSeat = DynamicCar.SeatedSpawnLeft;
                playerController.GetComponent<PlayerStatus>().CurrentVehicle = DynamicCar;
                break;
            default:
                break;
        }
        CanBrake();
   
        if (WindZone == null)
        {
            var wind = new GameObject("WindZone").AddComponent<WindZone>();
            wind.GetComponent<WindZone>().windTurbulence = 1;
            wind.GetComponent<WindZone>().windPulseMagnitude = 0.5f;
            wind.GetComponent<WindZone>().windPulseFrequency = 0.01f;
            WindZone = wind.transform;
            WindDir = InitialWindDirection;
            if (WindDir == WindDirection.Headwind)
            {
                wind.GetComponent<WindZone>().windMain = 0.5f;
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 180, WindZone.rotation.eulerAngles.z);
            }
            else if (WindDir == WindDirection.Favorable)
            {
                wind.GetComponent<WindZone>().windMain = 0.5f;
            }
            else
            {
                wind.GetComponent<WindZone>().windMain = 0f;
            }
        }

            if(CubeMappingEnabled)
                foreach (CubemapperUtil c in CubesToMap)
                {
                    c.thisCubemapper.gameObject.SetActive(true);
                    c.CalculateCubeMap();
                    //c.thisCubemapper.gameObject.SetActive(false);
                }
				
        foreach (Transform v in Spawner.SpawnedVehicles)
            v.GetComponent<NPCCarMovementScript>().CanMove = true;
        DynamicCar.GetComponent<CarMovementScript>().CanMove = true;
        //Spawner.StartSpawn();
        FuocoCamion.ForceBurn();
        SteamVRFade.fadeIn(0.35f);
    }

    internal void LocatePlayer()
    {
        if (playerController.transform.position.z <= FireSection.transform.position.z)
            PlayerFront = true;
        else
        {
            PlayerFront = false;
            ChangeWindDirection(WindDirection.Favorable);
        }
    }

    internal bool PlayerBehind()
    {
        return !PlayerFront;
    }

    


    public void FireStarted(TunnelFireController f)
    {
        SC.FireStarted();
        TunnelFire = f;
        Spawner.Stop();
        foreach (Transform t in Spawner.SpawnedVehicles)
            Destroy(t.gameObject);
        TunnelOnFire = true;
    }

    private void StartFireProcedures()
    {
        //PlayJingle(0.8f);
        foreach (TunnelShelterSectionController r in Rifugi)
        {
            r.StartFireProcedures();
        }
        foreach (TunnelTelephoneSectionController r in Nicchie)
        {
            r.StartFireProcedures();
        }
        DynamicCar.GetComponent<VehicleData>().radio.FireMessage();
        fireproceduresenabled = true;
    }

    public void StartWarningProcedures()
    {
        if (fireproceduresenabled)
            return;
        foreach (TunnelTelephoneSectionController r in Nicchie)
            {
                r.StartWarning();
            }
    }


    public void ChangeJingleVolume(float volume)
    {
        foreach (TunnelShelterSectionController r in Rifugi)
            r.RegulateJingle(volume);
    }

    public void Brake()
    {
        if (!braking)
        {
            ControllerType platform;

            CantBrake();

            if (GameManager.Instance != null)
                platform = GameManager.Instance.Platform;
            else
                platform = Platform;

            if (platform != ControllerType.MouseAndKeyboard)
                {
                var l = playerController.GetComponent<VRItemController>().LeftController.GetComponent<VibrationController>();
                if (l.StartVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this))
                    leftv = l;
                
                var r = playerController.GetComponent<VRItemController>().RightController.GetComponent<VibrationController>();
                if (r.StartVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this))
                    rightv = r;
                }          

            braking = true;
        }
    }

    public void StopBraking()
    {
    if(braking)
        { 
        ControllerType platform;

        CanBrake();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

        if (platform != ControllerType.MouseAndKeyboard)
        {
            if (leftv != null)
            {
                leftv.StopVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this);
                leftv = null;
            }

            if (rightv != null)
            {
                rightv.StopVibration(BrakeVibrParameters.x, BrakeVibrParameters.y, BrakeVibrParameters.z, this);
                rightv = null;
            }
        }
        braking = false;
        }
    }

    public void CanBrake()
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;


        if (platform == ControllerType.MouseAndKeyboard)
            playerController.GetComponent<FirstPersonItemController>().SetCrosshairPublic(FirstPersonItemController.CrosshairState.Rainbow);
        else
            playerController.GetComponent<VRItemController>().StartPulsePublic(ControllerButton.All);
        
    }

    private void VRCheck()
    {
        if (VRCheckDone)
            return;

        if (!UnityEngine.VR.VRDevice.isPresent)
        {
            if (GameManager.Instance != null)
                GameManager.Instance.Platform = ControllerType.MouseAndKeyboard;
            else
                Platform = ControllerType.MouseAndKeyboard;
        }
        VRCheckDone = true;
    }

    public void CanStop()
    {
        if (!once)
        {
            StartCoroutine(ForceFire());
            StartCoroutine(ForceFireProcedures());
            once = true;
        }

    }

    private IEnumerator ForceFireProcedures()
    {
        yield return new WaitForSeconds(0);
        StartFireProcedures();
        yield break;
    }

    private IEnumerator ForceFire()
    {
        yield return new WaitForSeconds(0);
        Telone.ForceBurn();
        yield break;
    }
    public void CantBrake()
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

        if (platform == ControllerType.MouseAndKeyboard)
            playerController.GetComponent<FirstPersonItemController>().SetCrosshairPublic(FirstPersonItemController.CrosshairState.None);
        else
            playerController.GetComponent<VRItemController>().StopPulsePublic();
        DisableBrakeMessage();
    }

  

    private void JingleFader(float v1, float v2, float v3, AudioSourceFader.FadeDirection mode)
    {
        foreach(TunnelShelterSectionController r in Rifugi)
        {
            r.Jingle.GetComponent<AudioSourceFader>().SetFader(v1, v2, v3, mode);
            r.Jingle.GetComponent<AudioSourceFader>().StartFader(0);
        }
    }

    // Use this for initialization
   
    

    private void LoadMainMenu()
    {
        SceneManager.LoadScene(MainMenuScene);
    }

    public void EndGame()
    {
        StartCoroutine(ExitFade());
    }

    IEnumerator ExitFade()
    {
        SteamVRFade.fadeOut(0.5f);
        yield return new WaitForSeconds(0.5f);
        LoadMainMenu();
    }


    public void BrochureAvailable()
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

        if (platform == ControllerType.MouseAndKeyboard)
            playerController.GetComponent<FirstPersonItemController>().SetBrochureAvailable(true);
        else
            playerController.GetComponent<VRItemController>().SetBrochureAvailable(true);
    }

    public void EnableBrakeMessage()
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

    if (platform == ControllerType.MouseAndKeyboard)
        playerController.transform.Find("Main Camera").Find("Canvas").gameObject.SetActive(true);
    else
        playerController.GetComponent<LimitTracking>().CameraEye.Find("Canvas").gameObject.SetActive(true);
    }

    public void DisableBrakeMessage()
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

    if (platform == ControllerType.MouseAndKeyboard)
        {
            playerController.transform.Find("Main Camera").Find("Canvas").gameObject.SetActive(false);
        }
        else
        {
            playerController.GetComponent<LimitTracking>().CameraEye.Find("Canvas").gameObject.SetActive(false);
        }
    }

     public void SpegniMani()
    {
        Mani.gameObject.SetActive(false);
    }


    public void StartIncendioTunnel()
        {
        if(FireSection != null)
            FireSection.StartFire();
    }

    public PlayerStatus GetPlayerController()
        {
        return playerController;
        }

    // Update is called once per frame
    void Update () {
		
	}

    public float GetAmbientTemp()
        {
        return AmbientTemp;
        }

    public IEnumerator GoOutOfTheCar()
    {
        SteamVRFade.fadeOut(.35f);
        yield return new WaitForSeconds(.35f);
        if(lightscaling)
            StopLightScaling();
        DynamicCar.GetComponent<VehicleData>().radio.TurnOff();
        SC.GoOutOfCar();
        if (playerController.GetComponent<PlayerStatus>().CurrentSeat.Driver)
        {
            Piedi.gameObject.SetActive(false);
            if (Mani.gameObject.activeSelf)
                Mani.gameObject.SetActive(false);
        }
        GetOut();
        
        Inside = false;
        SteamVRFade.fadeIn(.35f);
        ambientsound.volume = 0.7f;
        ChangeJingleVolume(1);
    }

    private void GetIn(VehicleSeat Seat)
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

        var prev = playerController;

        switch (platform)
        {
            case ControllerType.MouseAndKeyboard:
                if (prev != null)
                    prev.GetComponent<FirstPersonItemController>().ForceDrop();
                var ccontr = playerController.GetComponent<CharacterController>();
                var ccontr2 = FPSNoWalkControllerPrefab.GetComponent<CharacterController>();
                ccontr.enabled = ccontr2.enabled;
				ccontr.center = ccontr2.center;
                ccontr.height = ccontr2.height;				
				var obst = playerController.GetComponent<UnityEngine.AI.NavMeshObstacle>();
                var obst2 = FPSNoWalkControllerPrefab.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				obst.enabled = obst2.enabled;
				obst.center = obst2.center;
				obst.height = obst2.height;
                var sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(Seat.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                var cm = playerController.GetComponent<CharacterMotor>();
                var cm2 = FPSNoWalkControllerPrefab.GetComponent<CharacterMotor>();
                cm.canControl = cm2.canControl;
                cm.CopySettings(cm2);
                break;
            case ControllerType.ArmSwing:
                if (prev != null)
                    prev.GetComponent<VRItemController>().ForceDrop();
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(Seat.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                var pcm = playerController.GetComponent<PlayerColliderManager>();
                var pcm2 = ArmSwingNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                ccontr = playerController.GetComponent<CharacterController>();
                ccontr2 = ArmSwingNoWalkControllerPrefab.GetComponent<CharacterController>();
                ccontr.enabled = ccontr2.enabled;
                ccontr.center = ccontr2.center;
                ccontr.height = ccontr2.height;
				obst = playerController.GetComponent<UnityEngine.AI.NavMeshObstacle>();
                obst2 = ArmSwingNoWalkControllerPrefab.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				obst.enabled = obst2.enabled;
				obst.center = obst2.center;
				obst.height = obst2.height;
                var fs = playerController.GetComponentInChildren<FootSwinger>();
                var fs2 = ArmSwingNoWalkControllerPrefab.GetComponentInChildren<FootSwinger>();
                fs.FootSwingNavigation = fs2.FootSwingNavigation;
                fs.transform.localPosition = fs2.transform.localPosition;
                fs.enabled = fs2.enabled;
                fs.Target = null;
                playerController.transform.localPosition = new Vector3(0, 0, 0);
                playerController.GetComponent<CircularLimitTracking>().enabled = false;
                var slt = playerController.GetComponent<SquaredLimitTracking>();
                var slt2 = ArmSwingNoWalkControllerPrefab.GetComponent<SquaredLimitTracking>();
                slt.CopyConstraints(slt2);
                if (!Seat.Driver)
                    slt.FlipXLimit();
                slt.Reset();
                slt.enabled = true;
                break;
            case ControllerType.FootSwing:
                if (prev != null)
                    prev.GetComponent<VRItemController>().ForceDrop();
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(Seat.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                pcm = playerController.GetComponent<PlayerColliderManager>();
                pcm2 = FootSwingNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                ccontr = playerController.GetComponent<CharacterController>();
                ccontr2 = FootSwingNoWalkControllerPrefab.GetComponent<CharacterController>();
                ccontr.enabled = ccontr2.enabled;
                ccontr.center = ccontr2.center;
                ccontr.height = ccontr2.height;
				obst = playerController.GetComponent<UnityEngine.AI.NavMeshObstacle>();
                obst2 = FootSwingNoWalkControllerPrefab.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				obst.enabled = obst2.enabled;
				obst.center = obst2.center;
				obst.height = obst2.height;
                fs = playerController.GetComponentInChildren<FootSwinger>();
                fs2 = FootSwingNoWalkControllerPrefab.GetComponentInChildren<FootSwinger>();
                fs.FootSwingNavigation = fs2.FootSwingNavigation;
                fs.transform.localPosition = fs2.transform.localPosition;
                fs.enabled = fs2.enabled;
                fs.Target = null;
                playerController.transform.localPosition = new Vector3(0, 0, 0);
                playerController.GetComponent<CircularLimitTracking>().enabled = false;
                slt = playerController.GetComponent<SquaredLimitTracking>();
                slt2 = FootSwingNoWalkControllerPrefab.GetComponent<SquaredLimitTracking>();
                slt.CopyConstraints(slt2);
                if (!Seat.Driver)
                    slt.FlipXLimit();
                slt.Reset();
                slt.enabled = true;
                break;
            case ControllerType.CVirtualizer:
                if (prev != null)
                    prev.GetComponent<VRItemController>().ForceDrop();
                slt = playerController.GetComponent<SquaredLimitTracking>();
                slt2 = CVirtualizerNoWalkControllerPrefab.GetComponent<SquaredLimitTracking>();
                slt.CopyConstraints(slt2);
                if (!Seat.Driver)
                    slt.FlipXLimit();
                slt.Reset();
                slt.enabled = true;
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(Seat.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                var cvp = playerController.GetComponent<CVirtPlayerController>();
                var cvp2 = CVirtualizerNoWalkControllerPrefab.GetComponent<CVirtPlayerController>();
                cvp.movementSpeedMultiplier = cvp2.movementSpeedMultiplier;
                pcm = playerController.GetComponent<PlayerColliderManager>();
                pcm2 = CVirtualizerNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                playerController.transform.localPosition = new Vector3(0, 0, 0);
                break;
            case ControllerType.KatWalk:
                if (prev != null)
                    prev.GetComponent<VRItemController>().ForceDrop();
                slt = playerController.GetComponent<SquaredLimitTracking>();
                slt2 = KatWalkNoWalkControllerPrefab.GetComponent<SquaredLimitTracking>();
                slt.CopyConstraints(slt2);
                if (!Seat.Driver)
                    slt.FlipXLimit();
                slt.Reset();
                slt.enabled = true;
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(Seat.transform, playerController.GetComponent<PlayerStatus>().SeatedOffset);
                var kd = playerController.GetComponent<KATDevice>();
                var kd2 = KatWalkNoWalkControllerPrefab.GetComponent<KATDevice>();
                kd.multiply = kd2.multiply;
                kd.multiplyBack = kd2.multiplyBack;
                pcm = playerController.GetComponent<PlayerColliderManager>();
                pcm2 = KatWalkNoWalkControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                playerController.transform.localPosition = new Vector3(0, 0, 0);
                break;
            default:
                break;
        }
        playerController.GetComponent<PlayerStatus>().CurrentSeat = Seat;
        playerController.GetComponent<PlayerStatus>().CurrentVehicle = Seat.vehicle;
        Seat.Occupant = playerController.GetComponent<PlayerStatus>().transform;
    }

    private void GetOut()
    {
        ControllerType platform;

        VRCheck();

        if (GameManager.Instance != null)
            platform = GameManager.Instance.Platform;
        else
            platform = Platform;

        Vector3 pos = Vector3.zero;
        if (playerController.GetComponent<PlayerStatus>().CurrentSeat.Position == VehicleSeat.Seat.Left)
            pos = playerController.transform.position + new Vector3(-1.3f, -playerController.transform.position.y, 0);
        else
            pos = playerController.transform.position + new Vector3(1.3f, -playerController.transform.position.y, 0);

        switch (platform)
        {
            case ControllerType.MouseAndKeyboard:
                var ccontr = playerController.GetComponent<CharacterController>();
                var ccontr2 = FPSControllerPrefab.GetComponent<CharacterController>();
                ccontr.enabled = ccontr2.enabled;
				ccontr.center = ccontr2.center;
                ccontr.height = ccontr2.height;
				var obst = playerController.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				var obst2 = FPSControllerPrefab.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				obst.enabled = obst2.enabled;
				obst.center = obst2.center;
				obst.height = obst2.height;
                var sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(null, Vector3.zero);
                pos += new Vector3(0, 1, 0);
                playerController.transform.position = pos;
                var cm = playerController.GetComponent<CharacterMotor>();
                var cm2 = FPSControllerPrefab.GetComponent<CharacterMotor>();
                cm.canControl = cm2.canControl;
                cm.CopySettings(cm2);
                break;
            case ControllerType.ArmSwing:
                var pcm = playerController.GetComponent<PlayerColliderManager>();
                var pcm2 = ArmSwingControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                ccontr = playerController.GetComponent<CharacterController>();
                ccontr2 = ArmSwingControllerPrefab.GetComponent<CharacterController>();
                ccontr.enabled = ccontr2.enabled;
                ccontr.center = ccontr2.center;
                ccontr.height = ccontr2.height;
				obst = playerController.GetComponent<UnityEngine.AI.NavMeshObstacle>();
                obst2 = ArmSwingControllerPrefab.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				obst.enabled = obst2.enabled;
				obst.center = obst2.center;
				obst.height = obst2.height;
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(null, Vector3.zero);
                playerController.transform.position = pos;
                var fs = playerController.GetComponentInChildren<FootSwinger>();
                var fs2 = ArmSwingControllerPrefab.GetComponentInChildren<FootSwinger>();
                fs.FootSwingNavigation = fs2.FootSwingNavigation;
                fs.transform.localPosition = fs2.transform.localPosition;
                fs.enabled = fs2.enabled;
                fs.Target = ccontr;
                playerController.GetComponent<SquaredLimitTracking>().enabled = false;
                playerController.GetComponent<CircularLimitTracking>().Reset();
                playerController.GetComponent<CircularLimitTracking>().enabled = true;
                break;
            case ControllerType.FootSwing:
                pcm = playerController.GetComponent<PlayerColliderManager>();
                pcm2 = FootSwingControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                ccontr = playerController.GetComponent<CharacterController>();
                ccontr2 = FootSwingControllerPrefab.GetComponent<CharacterController>();
                ccontr.enabled = ccontr2.enabled;
                ccontr.center = ccontr2.center;
                ccontr.height = ccontr2.height;
				obst = playerController.GetComponent<UnityEngine.AI.NavMeshObstacle>();
                obst2 = FootSwingControllerPrefab.GetComponent<UnityEngine.AI.NavMeshObstacle>();
				obst.enabled = obst2.enabled;
				obst.center = obst2.center;
				obst.height = obst2.height;
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(null, Vector3.zero);
                playerController.transform.position = pos;
                fs = playerController.GetComponentInChildren<FootSwinger>();
                fs2 = FootSwingControllerPrefab.GetComponentInChildren<FootSwinger>();
                fs.FootSwingNavigation = fs2.FootSwingNavigation;
                fs.transform.localPosition = fs2.transform.localPosition;
                fs.enabled = fs2.enabled;
                fs.Target = ccontr;
                playerController.GetComponent<SquaredLimitTracking>().enabled = false;
                playerController.GetComponent<CircularLimitTracking>().Reset();
                playerController.GetComponent<CircularLimitTracking>().enabled = true;
                break;
            case ControllerType.CVirtualizer:
                playerController.GetComponent<SquaredLimitTracking>().enabled = false;
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(null, Vector3.zero);
                playerController.transform.position = pos;
                var cvp = playerController.GetComponent<CVirtPlayerController>();
                var cvp2 = CVirtualizerControllerPrefab.GetComponent<CVirtPlayerController>();
                cvp.movementSpeedMultiplier = cvp2.movementSpeedMultiplier;
                pcm = playerController.GetComponent<PlayerColliderManager>();
                pcm2 = CVirtualizerControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                break;
            case ControllerType.KatWalk:
                playerController.GetComponent<SquaredLimitTracking>().enabled = false;
                sm = playerController.GetComponent<FakeParenting>();
                sm.SetFakeParent(null, Vector3.zero);
                playerController.transform.position = pos;
                var kd = playerController.GetComponent<KATDevice>();
                var kd2 = KatWalkControllerPrefab.GetComponent<KATDevice>();
                kd.multiply = kd2.multiply;
                kd.multiplyBack = kd2.multiplyBack;
                pcm = playerController.GetComponent<PlayerColliderManager>();
                pcm2 = CVirtualizerControllerPrefab.GetComponent<PlayerColliderManager>();
                pcm.enabled = pcm2.enabled;
                break;
        }
        playerController.GetComponent<PlayerStatus>().CurrentSeat.Occupant = null;
        playerController.GetComponent<PlayerStatus>().CurrentSeat = null;
        playerController.GetComponent<PlayerStatus>().CurrentVehicle = null;
    }

    public IEnumerator GoInsideCar(VehicleSeat Seat)
    {
        SteamVRFade.fadeOut(.35f);
        yield return new WaitForSeconds(.35f);
        if(DynamicCar.GetComponent<VehicleData>().EngineOn)
            DynamicCar.GetComponent<VehicleData>().radio.TurnOn();
        SC.GoInsideCar();
        if(Seat.Driver)
            Piedi.gameObject.SetActive(true);
        GetIn(Seat);
        
        Inside = true;
        SteamVRFade.fadeIn(.35f);
        ambientsound.volume = 0.3f;
        ChangeJingleVolume(0.6f);
    }


    public void UpFireCount(CanBeOnFire c)
    {
        SC.FireCount++;
    }
    public void DownFireCount(CanBeOnFire c)
    {
        SC.FireCount--;
    }

    public void ChangeWindSpeed(float speed)
    {
        WindZone.GetComponent<WindZone>().windMain = speed;
    }

        public void ChangeWindDirection(WindDirection dir)
        {
        if (dir == WindDir || WindZone == null)
            return;
        if(dir == WindDirection.Favorable)
            {
            if (WindDir == WindDirection.Headwind)
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 0, WindZone.rotation.eulerAngles.z);
            else if (WindDir == WindDirection.Windless)
                WindZone.GetComponent<WindZone>().windMain = 0.5f;
            }
        else if (dir == WindDirection.Headwind)
            {
            if (WindDir == WindDirection.Favorable)
                WindZone.rotation = Quaternion.Euler(WindZone.rotation.eulerAngles.x, 180, WindZone.rotation.eulerAngles.z);
            else if (WindDir == WindDirection.Windless)
                WindZone.GetComponent<WindZone>().windMain = 0.5f;
            }
        else if (dir == WindDirection.Windless)
            {
            WindZone.GetComponent<WindZone>().windMain = 0f;
            }
        WindDir = dir;
        }
}
