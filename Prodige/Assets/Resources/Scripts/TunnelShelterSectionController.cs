﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TunnelShelterSectionController : TunnelSectionController
{
    public List<EntrataRifugio> Porte;
    public PulsanteChiamataSOS PulsanteTelefono;
    //public Panchina Panca;
    public AudioSource Jingle, Message, Ambient, LinkRoom, Ventilation;
    AudioSourceFader jinglefader, messagefader, ambientfader, linkroomfader, ventilationfader;
    public Extinguisher Estintore;
    public bool Reached = false;
    bool ventilation = false;
    public bool ShelterEnabled = true;
    public MonitorStop MonitorSX, MonitorDX;
    bool ventilationtransition = false;
    public AudioClip VentilationStart, VentilationEnabled, VentilationEnd;
    public Transform Segnalatori;
    bool setupinternaldone = false, setupexternaldoordone = false;
    MeshRenderer SegnalatoriMR;
    bool _flashingenabled = false;
    public Sbarra SbarraDX, SbarraSX;
    bool signon = true;
    public int NumRifugio;
    float tictimeup = 0.6f;
    float tictimedown = 0.4f;
    float nexttic = 0;
    public TunnelShelterSectionController Precedente, Successivo;
    [HideInInspector]
    public StageController SC;
	
    private void Awake()
    {
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if (tc != null)
            { 
            TC = tc.GetComponent<TunnelController>();
            SC = tc.GetComponent<StageController>();
            }
        if(Jingle != null)
            jinglefader = Jingle.GetComponent<AudioSourceFader>();
        if (Message != null)
            messagefader = Message.GetComponent<AudioSourceFader>();
        if (Ambient != null)
            ambientfader = Ambient.GetComponent<AudioSourceFader>();
        if(LinkRoom != null)
            linkroomfader = LinkRoom.GetComponent<AudioSourceFader>();
        if (Ventilation != null)
            ventilationfader = Ventilation.GetComponent<AudioSourceFader>();
        if (Segnalatori != null)
            SegnalatoriMR = Segnalatori.GetComponent<MeshRenderer>();
    }

    public override void Start()
    {
        base.Start();
        if (PulsanteTelefono != null)
            PulsanteTelefono.posizione = Posizione.Rifugio;
    }
    // Use this for initialization
    void OnEnable () {
	}
	
	// Update is called once per frame
	void Update () {
        var now = Time.time;
        if (_flashingenabled)
        {
            if (nexttic <= now)
            {
                if (!signon)
                {
                    SignOn();
                    nexttic = now + tictimeup;
                }
                else
                {
                    SignOff();
                    nexttic = now + tictimedown ;
                }
            }
        }
    }

    public void EnableFlashing(bool sel)
    {
        if (sel)
            _flashingenabled = true;
        else
            _flashingenabled = false;
    }

    public bool FlashingEnabled()
    {
        return _flashingenabled;
    }

    void SignOn()
    {
        var newmat = new Material(SegnalatoriMR.materials[1]);
        newmat.EnableKeyword("_EMISSION");
        var m = new Material[2];
        m[0] = SegnalatoriMR.materials[0];
        m[1] = newmat;
        SegnalatoriMR.materials = m;
        signon = true;
    }

    void SignOff()
    {
        var newmat = new Material(SegnalatoriMR.materials[1]);
        newmat.DisableKeyword("_EMISSION");
        var m = new Material[2];
        m[0] = SegnalatoriMR.materials[0];
        m[1] = newmat;
        SegnalatoriMR.materials = m;
        signon = false;
    }

    public void PlayJingle()
    {
        if(!Jingle.isPlaying && ShelterEnabled)
            Jingle.Play();
    }

    public void StopJingle()
    {
        if(ShelterEnabled)
            Jingle.Stop();
    }
    void ExitShelter()
    {
        if (SC != null)
            SC.ExitShelter();
    }

    void EnterShelter()
    {
        if (SC != null)
            SC.EnterShelter();
    }

    public void OpenInternalDoorOutside()
    {
        if (Message.isPlaying)
            messagefader.SetUpAndStart(0, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
        if (ventilation)
            ventilationfader.SetUpAndStart(0, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
        ambientfader.SetUpAndStart(0.5f, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
    }
    public void OpenInternalDoorInside()
    {
        jinglefader.SetUpAndStart(0, 0.25f, 0.1f, AudioSourceFader.FadeDirection.Raise);
        TC.ambientfader.SetUpAndStart(0, 0.35f, 0.1f, AudioSourceFader.FadeDirection.Raise);
        linkroomfader.SetUpAndStart(0.5f, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
        ambientfader.SetUpAndStart(0.66f, 1, 0.1f, AudioSourceFader.FadeDirection.Fade);
        ExitShelter();
    }
    public void OpenExternalDoorOutside()
    {
        linkroomfader.SetUpAndStart(0.5f, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
        ambientfader.SetUpAndStart(0.35f, 0.66f, 0.1f, AudioSourceFader.FadeDirection.Raise);
        
    }
    public void OpenExternalDoorInside()
    {
        jinglefader.SetUpAndStart(0.25f, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
        TC.ambientfader.SetUpAndStart(0.35f, 0.7f, 0.1f, AudioSourceFader.FadeDirection.Raise);
    }

    public void CloseInternalDoorOutside()
    {
        if (!setupinternaldone)
            setupinternaldone = true;
        else
        {
            if (Message.isPlaying)
                messagefader.SetUpAndStart(0, 1, 0.1f, AudioSourceFader.FadeDirection.Fade);
            if (ventilation)
                ventilationfader.SetUpAndStart(0, 1, 0.1f, AudioSourceFader.FadeDirection.Fade);
            ambientfader.SetUpAndStart(0.5f, 1, 0.1f, AudioSourceFader.FadeDirection.Fade);
        }
    }
    public void CloseInternalDoorInside()
    {
        linkroomfader.SetUpAndStart(0.5f, 1, 0.1f, AudioSourceFader.FadeDirection.Fade);
        jinglefader.SetUpAndStart(0, 0.25f, 0.1f, AudioSourceFader.FadeDirection.Fade);
        ambientfader.SetUpAndStart(0.66f, 1, 0.1f, AudioSourceFader.FadeDirection.Raise);
        TC.ambientfader.SetUpAndStart(0, 0.35f, 0.1f, AudioSourceFader.FadeDirection.Fade);
        EnterShelter();
    }
    public void CloseExternalDoorOutside()
    {
        if (!setupexternaldoordone)
            setupexternaldoordone = true;
        else
            linkroomfader.SetUpAndStart(0, 0.5f, 0.1f, AudioSourceFader.FadeDirection.Fade);
    }
    public void CloseExternalDoorInside()
    {
        jinglefader.SetUpAndStart(0.25f, 1, 0.1f, AudioSourceFader.FadeDirection.Fade);
        TC.ambientfader.SetUpAndStart(0.35f, 0.7f, 0.1f, AudioSourceFader.FadeDirection.Fade);
    }           

    public void StartFireProcedures()
    {
        if (!SC.CarLeft)
            RegulateJingle(0.6f);        
        PlayJingle();

        if (!ventilation && !ventilationtransition && Ventilation != null)
        {
            StartCoroutine(VentilationOn());
        }

        if (Segnalatori != null)
            EnableFlashing(true);

        if (TC == null)
        {
            var tc = GameObject.FindGameObjectWithTag("TunnelController");
            if (tc != null)
                TC = tc.GetComponent<TunnelController>();
            else
            {
                if (MonitorDX != null && !MonitorDX.ItemActive())
                    MonitorDX.EnableSign(true);
                if (SbarraDX != null)
                    SbarraDX.Close();
                if (MonitorSX != null && !MonitorSX.ItemActive())
                    MonitorSX.EnableSign(true);
                if (SbarraSX != null)
                    SbarraSX.Close();
                return;
            }
        }

        if (transform.position.z < TC.FireSection.transform.position.z)
        {
            if (MonitorDX != null && !MonitorDX.ItemActive())
                MonitorDX.EnableSign(true);
            if (SbarraDX != null)
                SbarraDX.Close();
        }
        else
        {
            if (MonitorSX != null && !MonitorSX.ItemActive())
                MonitorSX.EnableSign(true);
            if (SbarraSX != null)
                SbarraSX.Close();
        }
    }

    private IEnumerator VentilationOn()
    {
        ventilationtransition = true;
        Ventilation.clip = VentilationStart;
        Ventilation.loop = false;
        Ventilation.Play();
        yield return new WaitWhile(() => Ventilation.isPlaying);
        Ambient.Stop();
        Ventilation.clip = VentilationEnabled;
        Ventilation.loop = true;
        Ventilation.Play();
        ventilation = true;
        ventilationtransition = false;
    }

    public void StopVentilation()
    {
        if (ventilation && !ventilationtransition)
        {
            StartCoroutine(VentilationOff());
        }
    }

    private IEnumerator VentilationOff()
    {
        ventilationtransition = true;
        Ventilation.Stop();
        Ventilation.clip = VentilationEnd;
        Ventilation.loop = false;
        Ventilation.Play();
        yield return new WaitWhile(() => Ventilation.isPlaying);
        ventilation = false;
        Ambient.Play();
        ventilationtransition = false;
    }

    public void StopMessage()
    {
        Message.Stop();
    }

    public void PlayMessage()
    {
            if (!Message.isPlaying && ShelterEnabled)
            Message.Play();
    }

    public void RegulateJingle(float volume)
    {
        if(ShelterEnabled)
            Jingle.volume = volume;
    }
    public void RegulateMessage(float volume)
    {
        if(ShelterEnabled)
            Message.volume = volume;
    }

    public float GetJingleVolume()
    {
        return Jingle.volume;
    }

    public float GetMessageVolume()
    {
        return Message.volume;
    }


    public void Echo(bool yes)
    {
        GetComponent<AudioEchoFilter>().enabled = yes;
    }

    public override void Reset()
    {
        foreach(EntrataRifugio e in Porte)
            e.Reset();
        PulsanteTelefono.Reset();
        StopJingle();
        StopMessage();
        jinglefader.Reset();
        messagefader.Reset();
        ambientfader.Reset();
        if(linkroomfader != null)
            linkroomfader.Reset();
        if (ventilation)
            StopVentilation();
        base.Reset();
    }

    /*public override void LightsOn(bool active)
        {
        if (active)
            {
            foreach (Transform t in Lights)
                {
                t.FindChild("LampioniAccesi").gameObject.SetActive(true);
                t.FindChild("LampioniSpenti").gameObject.SetActive(false);
                }
            }
        else
            {
            foreach(Transform t in Lights)
                {
                t.FindChild("LampioniAccesi").gameObject.SetActive(false);
                t.FindChild("LampioniSpenti").gameObject.SetActive(true);
                }
            }
        }*/
}
