﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemCodes
{
    Brochure, Extinguisher, Keys, Niche, NicheDoor, Arrows,
    CarHandle, SOSCall, ShelterInternalDoors,
    ShelterExternalDoors, ShelterDoor, Bench, ExtSecure,
    SOSTelephone, Locker, LockerDoor, Generic, FlameThrower, Invalid,
    Idrante, Cintura, Forbici, IdranteLeva, TuboHose
};
public class GenericItem : MonoBehaviour
{


    public virtual void Interact(GenericItemSlave slave, ItemController c, ControllerHand hand)
    {
        throw new NotImplementedException();
    }

    public ItemCodes ItemCode = ItemCodes.Generic;
    public bool Grabbable = false;

    public virtual void DisableOutline(GenericItemSlave slave, ItemController c)
    {
        DisableOutline(c);
    }

    public virtual void EnableOutline(GenericItemSlave slave, ItemController c)
    {
        EnableOutline(c);
    }

    public virtual void ClickButton(object sender)
    {
        throw new NotImplementedException();
    }

    public virtual void UnClickButton(object sender)
    {
        throw new NotImplementedException();
    }

    public bool CanInteractFromStart = true;
    bool _CanInteract;
    [HideInInspector]
    public bool outlined = false;
    public bool IsKinematic = false;
    [HideInInspector]
    public Transform InitialParent;
    [HideInInspector]
    public Vector3 prevpos;
    [HideInInspector]
    public Vector3 currvelocity;
    [HideInInspector]
    public Vector3 direction;
    Rigidbody rbody;
    public List<Transform> Pieces;
    [HideInInspector]
    public bool ItemActive = false;
    public enum RotationAxis { X, Y, Z };
    [HideInInspector]
    public Transform SeekTarget;
    public GenericItemSlave Slave;
    public ItemController Player;

    [HideInInspector]
    public Vector3 startParentPosition;
    [HideInInspector]
    public Quaternion startParentRotationQ;
    [HideInInspector]
    public Vector3 startChildPosition;
    [HideInInspector]
    public Quaternion startChildRotationQ;

    // Use this for initialization
    public virtual void Start()
    {
        if (Slave != null)
        {
            Slave.Master = this;
            prevpos = Slave.transform.position;
            InitialParent = Slave.transform.parent;
            rbody = Slave.GetComponent<Rigidbody>();
        }
        else
        {
            prevpos = transform.position;
            InitialParent = transform.parent;
            rbody = GetComponent<Rigidbody>();
        }
        if (CanInteractFromStart)
            SetCanInteract(true);
        else
            SetCanInteract(false);
    }

    // Update is called once per frame
    public virtual void Update()
    {
    }

    public virtual void EnablePhysics()
    {
        if (!IsKinematic && rbody != null)
        {
            rbody.isKinematic = false;
            rbody.velocity = currvelocity;
        }
    }


    public virtual void DisablePhysics()
    {
        if (!IsKinematic && rbody != null)
            rbody.isKinematic = true;
    }

    public virtual bool CanInteract(ItemController c)
    {
        return _CanInteract;
    }

    public virtual void SetCanInteract(bool can, ItemController c = null)
    {
        _CanInteract = can;
        if (!can && outlined)
            DisableOutline(c);
    }

    public virtual void EnableOutline(ItemController c)
    {
        Material[] mts = null;
        if (Slave == null)
        {
            var m = GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        else
        {
            var m = Slave.GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        Color col;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 255;
                m.SetColor("_OutlineColor", col);
            }

            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 255;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = true;
    }

    public virtual void DisableOutline(ItemController c)
    {
        Material[] mts = null;
        if (Slave == null)
        {
            var m = GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        else
        {
            var m = Slave.GetComponent<MeshRenderer>();
            if (m != null)
                mts = m.materials;
        }

        Color col;
        if (mts != null)
        {
            foreach (Material m in mts)
            {
                if (Grabbable)
                    col = Color.yellow;
                else
                    col = Color.white;
                col.a = 0;
                m.SetColor("_OutlineColor", col);
            }
            foreach (Transform t in Pieces)
            {
                var mat = t.GetComponent<MeshRenderer>();
                if (mat != null)
                    mts = mat.materials;
                foreach (Material m in mts)
                {
                    if (Grabbable)
                        col = Color.yellow;
                    else
                        col = Color.white;
                    col.a = 0;
                    m.SetColor("_OutlineColor", col);
                }
            }
        }
        outlined = false;
    }

    public virtual void ClickButton(object sender, ClickedEventArgs e) { }

    public virtual void UnClickButton(object sender, ClickedEventArgs e) { }

    public virtual void Interact(ItemController c, ControllerHand hand) { }

    public void DisableItem(ItemController player, ControllerHand hand)
    {
        if (Slave != null)
        {
            if (Slave.gameObject.activeSelf)
            {
                Slave.gameObject.SetActive(false);
                var im = player.GetComponent<PlayerStatus>().ItemController;
                if (im.Type == ItemControllerType.VR)
                {
                    if (hand == ControllerHand.LeftHand)
                    {
                        im.InputManager.OnLeftPadPressed += ClickButton;
                        im.InputManager.OnLeftPadUnpressed += UnClickButton;
                    }
                    if (hand == ControllerHand.RightHand)
                    {
                        im.InputManager.OnRightPadPressed += ClickButton;
                        im.InputManager.OnRightPadUnpressed += UnClickButton;
                    }
                }
                return;
            }

        }
        else
        {
            if (gameObject.activeSelf)
            {
                gameObject.SetActive(false);
                var im = player.GetComponent<PlayerStatus>().ItemController;
                if (im.Type == ItemControllerType.VR)
                {
                    if (hand == ControllerHand.LeftHand)
                    {
                        im.InputManager.OnLeftPadPressed += ClickButton;
                        im.InputManager.OnLeftPadUnpressed += UnClickButton;
                    }
                    if (hand == ControllerHand.RightHand)
                    {
                        im.InputManager.OnRightPadPressed += ClickButton;
                        im.InputManager.OnRightPadUnpressed += UnClickButton;
                    }
                }
                return;
            }
        }
    }

    public void EnableItem(ItemController player, ControllerHand hand)
    {
        if (Slave != null)
        {
            if (!Slave.gameObject.activeSelf)
            {
                Slave.gameObject.SetActive(true);
                if (player.Type == ItemControllerType.VR)
                {
                    if (hand == ControllerHand.LeftHand)
                    {
                        player.InputManager.OnLeftPadPressed -= ClickButton;
                        player.InputManager.OnLeftPadUnpressed -= UnClickButton;
                    }
                    else if (hand == ControllerHand.RightHand)
                    {

                        player.InputManager.OnRightPadPressed -= ClickButton;
                        player.InputManager.OnRightPadUnpressed -= UnClickButton;
                    }
                }
                DropParent();
                return;
            }
        }
        else
        {
            if (!gameObject.activeSelf)
            {
                gameObject.SetActive(true);
                if (player.Type == ItemControllerType.VR)
                {
                    if (hand == ControllerHand.LeftHand)
                    {
                        player.InputManager.OnLeftPadPressed -= ClickButton;
                        player.InputManager.OnLeftPadUnpressed -= UnClickButton;
                    }
                    else if (hand == ControllerHand.RightHand)
                    {

                        player.InputManager.OnRightPadPressed -= ClickButton;
                        player.InputManager.OnRightPadUnpressed -= UnClickButton;
                    }
                }
                DropParent();
                return;
            }
        }
    }

    public virtual void ForceParent(Transform p, bool KeepOrientation)
    {
        SeekTarget = p;
        //if (type != ItemControllerType.FPS)
        if (KeepOrientation)
        {
            startParentPosition = p.position;
            startParentRotationQ = p.rotation;

            startChildPosition = transform.position;
            startChildRotationQ = transform.rotation;

            startChildPosition = DivideVectors(Quaternion.Inverse(p.rotation) * (startChildPosition - startParentPosition), p.lossyScale);
        }
        else
        {
            startParentPosition = Vector3.zero;
            startParentRotationQ = Quaternion.identity;

            startChildPosition = Vector3.zero;
            startChildRotationQ = Quaternion.identity;
        }
        //transform.parent = p;
    }
    Vector3 DivideVectors(Vector3 num, Vector3 den)
    {

        return new Vector3(num.x / den.x, num.y / den.y, num.z / den.z);

    }
    public virtual void DropParent()
    {
        SeekTarget = null;
        startParentPosition = Vector3.zero;
        startParentRotationQ = Quaternion.identity;

        startChildPosition = Vector3.zero;
        startChildRotationQ = Quaternion.identity;
        //transform.parent = null;
    }

    public virtual void Reset()
    {
        if (CanInteractFromStart)
            SetCanInteract(true);
        else
            SetCanInteract(false);
        DisableOutline(null);
    }
}
