﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyOrientation : MonoBehaviour {

    Vector3 InitialDir;
    float InitialDist;
    Vector3 prevheadpos = Vector3.zero;
    Quaternion prevrighthandrot, prevlefthandrot = Quaternion.identity;
    [SerializeField]
    Transform Head;
    [SerializeField]
    Transform leftController;
    [SerializeField]
    Transform rightController;
    float targety;
    [SerializeField]
    private float _rotLerpRate = 0.8f;

    private void Start()
    {
        InitialDir = transform.position - Head.transform.position;
        InitialDist = Vector3.Distance(transform.position, Head.transform.position);
    }
    // Use this for initialization
    private void LateUpdate()
    {
        if (Head.localPosition != prevheadpos)
        {
            var r = new Ray(Head.transform.position, InitialDir);
            var p = r.GetPoint(InitialDist);
            prevheadpos = Head.localPosition;
            transform.position = new Vector3(p.x, transform.position.y, p.z);
        }
        if (leftController.localRotation != prevlefthandrot || rightController.localRotation != prevrighthandrot || Head.localPosition != prevheadpos)
            {
            targety = determineAverageArmControllerRotation().eulerAngles.y;
            prevlefthandrot = leftController.localRotation;
            prevrighthandrot = rightController.localRotation;
            }
        if (transform.eulerAngles.y != targety)
            InterpolateRotation();
    }


    // Returns the average rotation of the two controllers
    Quaternion determineAverageArmControllerRotation()
    {
        // Build the average rotation of the controller(s)
        Quaternion newRotation;

        // Both controllers are present
        if (Head != null && leftController != null && rightController != null)
        {
            newRotation = averageRotation(leftController.rotation, rightController.rotation, Head.rotation);
        }
        else if (leftController != null && rightController != null)
        {
            newRotation = averageRotation(leftController.rotation, rightController.rotation);
        }
        // Left controller only
        else if (leftController != null && rightController == null)
        {
            if (Head == null)
                newRotation = leftController.rotation;
            else
                newRotation = averageRotation(leftController.rotation, Head.rotation);

        }
        // Right controller only
        else if (rightController != null && leftController == null)
        {
            if (Head == null)
                newRotation = rightController.rotation;
            else
                newRotation = averageRotation(rightController.rotation, Head.rotation);
        }
        // No controllers!
        else
        {
            if (Head == null)
                newRotation = Quaternion.identity;
            else
                newRotation = Head.transform.rotation;
        }

        return newRotation;
    }

    private void InterpolateRotation()
    {
        var t = Quaternion.Euler(transform.eulerAngles.x, targety, transform.eulerAngles.z);
        transform.rotation = Quaternion.Lerp(transform.rotation, t, Time.deltaTime * _rotLerpRate);
    }

    Quaternion averageRotation(Quaternion rot1, Quaternion rot2)
    {
        return Quaternion.Slerp(rot1, rot2, 0.5f);
    }

    Quaternion averageRotation(Quaternion rot1, Quaternion rot2, Quaternion rot3)
    {
        var r = averageRotation(rot1, rot2);
        return averageRotation(r, rot3);
    }
}
