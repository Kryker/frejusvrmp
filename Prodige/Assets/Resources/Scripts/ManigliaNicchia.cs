﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManigliaNicchia : GenericItem
{
    /*Nicchia nicchia;
    AudioSource audiosource;
    [HideInInspector]
    public Nicchia.VanoNicchia Armadietto;
    public int Index = 0;
    public List<AudioClip> clips;
    bool toplay = false;

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
        audiosource.Play();
    }

    public override void Interact()
    {
       nicchia.Interact(this);
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    IEnumerator PlayAudio(AudioClip clip)
    {
        yield return new WaitUntil(() => !toplay);
        if (audiosource.isPlaying)
        {
            toplay = true;
            yield return new WaitWhile(() => audiosource.isPlaying);
        }
        audiosource.clip = clip;
        audiosource.Play();
        yield return new WaitWhile(() => audiosource.isPlaying);
        toplay = false;
    }


    // Use this for initialization
    public override void Start () {
        ItemCode = ItemCodes.NicheDoor;
        var anta = transform.parent;
        nicchia = anta.GetComponentInParent<Nicchia>();
        audiosource = anta.GetComponent<AudioSource>();
        base.Start();
	}

    internal void PlayClip(int v)
    {
        StartCoroutine(PlayAudio(clips[v]));
    }

    internal void DoorOpened(AdvancedStateMachineBehaviour st)
    {
        CanInteract(true);
        ItemActive = true;
    }

    internal void LockOpened(AdvancedStateMachineBehaviour st)
    {
        nicchia.OpenDoor(Armadietto);
        nicchia.MakeAllItemsGrabbable(Armadietto);
    }

    internal void DoorClosed(AdvancedStateMachineBehaviour st)
    {
        nicchia.MakeAllItemsUngrabbable(Armadietto);
        nicchia.CloseLock(Armadietto);
    }

    internal void LockClosed(AdvancedStateMachineBehaviour st)
    {
        CanInteract(true);
        ItemActive = false;
    }

    // Update is called once per frame
    public override void Update()
    {

    }

    internal void DoorOpen(AdvancedStateMachineBehaviour a)
    {
        PlayClip(0);
    }


    internal void DoorClose(AdvancedStateMachineBehaviour a)
    {
        PlayClip(0);
    }

    internal void LockClose(AdvancedStateMachineBehaviour a)
    {
        PlayClip(1);
    }

    internal void LockOpen(AdvancedStateMachineBehaviour a)
    {
        PlayClip(1);
    }*/
}
