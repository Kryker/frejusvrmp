﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetLegRotationRight : MonoBehaviour {

    public FootSwinger swing;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Quaternion left, right;
        swing.determineAverageLegControllerRotationPublic(out left, out right);
        transform.rotation = right;
    }
}
