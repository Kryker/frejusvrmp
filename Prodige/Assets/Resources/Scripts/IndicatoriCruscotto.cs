﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IndicatoriCruscotto : MonoBehaviour {

	public int startKm = 3309;
	public Color TextColor = Color.red;
	public Text txtKm, txtHour;
	VehicleData car;
    VehicleDataNet netcar;
    float startPosition, actualPosition;
	int difference;
	DateTime currentTime;
    float t = 0;

	public void Start()
	{
		car = transform.parent.GetComponentInParent<VehicleData>();
        if (car == null)
        {
            netcar = transform.parent.GetComponentInParent<VehicleDataNet>();
            if(netcar != null)
                startPosition = netcar.transform.position.z;
        }
        else
            startPosition = car.transform.position.z;

		var m = txtKm.material;
		var newm = new Material(m);
		newm.color = TextColor;

		txtKm.color = TextColor;
		txtKm.material = newm;

		txtHour.color = TextColor;
		txtHour.material = newm;
	}

	public void Update()
	{
        var now = Time.time;
        if (now >= t)
        {
            currentTime = System.DateTime.Now;
            if (currentTime.Hour < 10)
                txtHour.text = "0" + currentTime.Hour + ":";
            else
                txtHour.text = currentTime.Hour + ":";

            if (currentTime.Hour < 10)
                txtHour.text += "0" + currentTime.Minute;
            else
                txtHour.text += currentTime.Minute;

            if (car != null)
                actualPosition = car.transform.position.z;
            else if(netcar != null)
                actualPosition = netcar.transform.position.z;

            difference = Convert.ToInt32(actualPosition - startPosition);

            if (difference >= 100)
            {

                if (car != null || netcar != null)
                {
                    startKm = startKm + 1;
                    txtKm.text = "00" + startKm;
                    startPosition = actualPosition;
                }
            }
            else
                txtKm.text = "00" + startKm;
            t = now + 1;
        }
	}
}
