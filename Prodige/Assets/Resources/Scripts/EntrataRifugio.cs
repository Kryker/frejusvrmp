﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntrataRifugio : GenericItem {

    public List<AudioClip> clips;
    public PortaRifugio Porta;
    //Animation Anim;
    public Animator animator;
    public AudioSource audiosource;
    StageController SC;
    public Panchina Panchina;
    public bool Interna = true;
    public EntrataRifugio OtherDoor;
    //bool transition = false;
    [HideInInspector]
    public bool opening = false;
    bool toplay = false;
    //Coroutine lastroutine;
    TunnelShelterSectionController rifugio;
    
    public bool Indoor { get; private set; }

    private void Awake()
    {
        
        rifugio = transform.parent.GetComponentInParent<TunnelShelterSectionController>();
        if (Interna)
            ItemCode = ItemCodes.ShelterInternalDoors;
        else
            ItemCode = ItemCodes.ShelterExternalDoors;
        //Anim = GetComponent<Animation>();
        if (audiosource == null)
            audiosource = transform.Find("Source").GetComponent<AudioSource>();

        
        var advancedbehaviours = animator.GetBehaviours<AdvancedStateMachineBehaviour>();

        foreach (AdvancedStateMachineBehaviour ad in advancedbehaviours)
        {
            var layername = "PortaLayer";
            if (ad.Layer == layername)
            {
                if (ad.StateName == "Closed")
                {
                    Porta.PortaClose = ad;
                }
                else if (ad.StateName == "Opened")
                {
                    Porta.PortaOpen = ad;
                }
            }
            layername = "ManigliaLayer";
            if (ad.Layer == layername)
            {
                if (!Interna)
                {
                    if (ad.StateName == "Pressed")
                    {
                        Porta.ManigliaPressed = ad;
                    }
                    else if (ad.StateName == "Unpressed")
                    {
                        Porta.ManigliaUnpressed = ad;
                    }
                }
              else
                {
                    if (ad.StateName == "Opening")
                    {
                        Porta.ManigliaOpening = ad;
                    }
                    else if (ad.StateName == "Closing")
                    {
                        Porta.ManigliaClosing = ad;
                    }
                    else if (ad.StateName == "Idle")
                    {
                        Porta.ManigliaIdle = ad;
                    }
                }
            }           
        }

        if (Interna)
        {
            Porta.ManigliaOpening.StateEnter += ManigliaOpening;
            Porta.ManigliaOpening.StatePercentagePlayed += OpenDoor;
        }
        else
        {
            Porta.ManigliaPressed.StateEnter += ManigliaOpening;
            Porta.ManigliaPressed.StatePercentagePlayed += OpenDoor;
            Porta.ManigliaPressed.StatePercentagePlayed += ManigliaClose;
        }
        
        Porta.PortaOpen.StateEnter += DoorOpened;
        if(Interna)
            Porta.PortaOpen.StatePercentagePlayed += ManigliaOpened;
        Porta.PortaOpen.StatePlayed += ReCloseDoor;
        Porta.PortaClose.StateEnter += DoorClosing;
        Porta.PortaClose.StatePlayed += DoorClosed;
        Porta.PortaClose.StatePercentagePlayed += DoorCloseSound;
    }

    IEnumerator PlayAudio(AudioClip clip)
    {
        yield return new WaitUntil(() => !toplay);
        if (audiosource.isPlaying)
        {
            toplay = true;
            yield return new WaitWhile(() => audiosource.isPlaying);
        }
        audiosource.clip = clip;
        audiosource.Play();
        yield return new WaitWhile(() => audiosource.isPlaying);
        toplay = false;
    }

    private void ManigliaOpening(AdvancedStateMachineBehaviour a)
    {
        SetCanInteract(false);
        /*lastroutine = */StartCoroutine(PlayAudio(clips[0]));
    }

    private void OpenDoor(AdvancedStateMachineBehaviour a)
    {
        animator.SetBool("OpenPorta", true);
    }

    private void ManigliaClose(AdvancedStateMachineBehaviour a)
    {
        animator.SetBool("OpenManiglia", false);
        /*lastroutine = */StartCoroutine(PlayAudio(clips[2]));
    }

    private void DoorOpened(AdvancedStateMachineBehaviour a)
    {
        //transition = true;
        opening = true;
        ItemActive = true;
        Porta.transform.parent.gameObject.layer = LayerMask.NameToLayer("MovingItem");
        /*lastroutine = */StartCoroutine(PlayAudio(clips[1]));
        if (SC != null)
        {
            if (Interna && Indoor/*SC.ShelterReached*/)
            {
                rifugio.OpenInternalDoorInside();
            }
            else if (!Interna && Indoor)
            {
                rifugio.OpenExternalDoorInside();
            }
            else if (Interna && !Indoor)
            {
                rifugio.OpenInternalDoorOutside();
            }
            else if (/*!Indoor && !SC.ShelterReached*/!Interna && !Indoor)
            {
                rifugio.OpenExternalDoorOutside();
            }
        }
    }

    private void ManigliaOpened(AdvancedStateMachineBehaviour a)
    {
        animator.SetBool("OpenManiglia", false);
        /*lastroutine = */StartCoroutine(PlayAudio(clips[2]));
    }

    private void ReCloseDoor(AdvancedStateMachineBehaviour a)
    {
        //transition = false;
        animator.SetBool("OpenPorta", false);
    }

    private void DoorClosing(AdvancedStateMachineBehaviour a)
    {
        SetCanInteract(false);
        //transition = true;
        opening = false;
        /*lastroutine = */StartCoroutine(PlayAudio(clips[4]));
        //CanInteract(true);
    }

    private void DoorClosed(AdvancedStateMachineBehaviour a)
    {
        //CanInteract(false);
        if (SC != null)
        {
            if (Interna && Indoor/*SC.ShelterReached*/)
            {
                rifugio.CloseInternalDoorInside();
                Porta.SetCanInteract(false);
            }
            else if (!Interna && Indoor)
            {
                rifugio.CloseExternalDoorInside();
            }
            else if (Interna && !Indoor)
            {
                rifugio.CloseInternalDoorOutside();
            }
            else if (!Interna && !Indoor /*!Indoor && !SC.ShelterReached*/)
            {
                rifugio.CloseExternalDoorOutside();
            }
        }
        //transition = false;
        Porta.transform.parent.gameObject.layer = LayerMask.NameToLayer("Default");
        ItemActive = false;
        SetCanInteract(true);
    }

    private void DoorCloseSound(AdvancedStateMachineBehaviour a)
    {
        if (Interna)
            /*lastroutine = */StartCoroutine(PlayAudio(clips[3]));
        else
            /*lastroutine = */StartCoroutine(PlayAudio(clips[2]));
    }



    
  

    public override void Start()
    {
        var tc = GameObject.FindGameObjectWithTag("TunnelController");
        if(tc!=null)
            SC = tc.GetComponent<StageController>();

        base.Start();
    }

    public override void Interact(ItemController c, ControllerHand hand)
    {
        if (CanInteract(c))
        {
            if (!ItemActive)                    //se chiusa
            {
            animator.SetBool("OpenManiglia", true);
            }
            else                                 //se aperta
            {
            animator.SetBool("OpenPorta", false);
            }
        }
    }



    // Update is called once per frame
    public override void Update()
    {

    }

    public override void SetCanInteract(bool can, ItemController c = null)
    {
        Porta.SetCanInteract(can, c);
        /*PortaDX.CanInteract(can);
        PortaSX.CanInteract(can);*/
        base.SetCanInteract(can, c);
    }

    
    public override void Reset()
    {
        //CloseShutter(0, 1);
        Porta.Reset();
        /*PortaDX.Reset();
        PortaSX.Reset();*/
        if (SC != null)
            SC.ShelterReached = false;
        rifugio.Reached = false;
        Indoor = false;
        if (Interna)
            SetCanInteract(false);
        else
            SetCanInteract(true);
        ItemActive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Interna)
                {
                if (SC != null)
                    SC.ShelterReached = true;
                rifugio.Reached = true;
                }
            //else
                Indoor = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Interna)
            {
                if (SC != null)
                    SC.ShelterReached = false;
                rifugio.Reached = false;
            }
            //else
                Indoor = false;
        }
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    { }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {}
}
