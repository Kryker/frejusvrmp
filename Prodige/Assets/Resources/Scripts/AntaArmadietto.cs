﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntaArmadietto : GenericItem
{
    /* Armadietto armadietto;
     AudioSource audiosource;
     bool toplay = false;
     [HideInInspector]
     public Armadietto.Vano Vano;
     public int Index = 0;
     public List<AudioClip> clips;

     public override void ClickButton(object sender, ClickedEventArgs e)
     {
         audiosource.Play();
     }

     public override void Interact()
     {
         armadietto.Interact(this);
     }

     public override void UnClickButton(object sender, ClickedEventArgs e)
     {
     }

     // Use this for initialization
     public override void Start()
     {
         ItemCode = ItemCodes.LockerDoor;
         var armadio = transform.parent;
         armadietto = armadio.GetComponentInParent<Armadietto>();
         audiosource = GetComponent<AudioSource>();
         base.Start();
     }

     IEnumerator PlayAudio(AudioClip clip)
     {
         yield return new WaitUntil(() => !toplay);
         if (audiosource.isPlaying)
         {
             toplay = true;
             yield return new WaitWhile(() => audiosource.isPlaying);
         }
         audiosource.clip = clip;
         audiosource.Play();
         yield return new WaitWhile(() => audiosource.isPlaying);
         toplay = false;
     }

     public void DoorOpened(AdvancedStateMachineBehaviour st)
     {
         CanInteract(true);
         ItemActive = true;
     }


     public void DoorClosed(AdvancedStateMachineBehaviour st)
     {
         CanInteract(true);
         ItemActive = false;
     }

     // Update is called once per frame
     public override void Update()
     {

     }

     internal void PlayClip(int v)
     {
         StartCoroutine(PlayAudio(clips[v]));
     }*/
}
