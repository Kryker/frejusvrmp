﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VehicleSeat : MonoBehaviour {

    public enum Seat { Left, Right };
    public bool Driver = false;
    public Transform Occupant = null;
    public Seat Position;
    public VehicleDataNet vehiclenet;
    public VehicleData vehicle;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
