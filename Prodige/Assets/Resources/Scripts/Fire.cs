﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Fire : MonoBehaviour
    {
    public float damagepp = 8;
    public float Temperature = 1000.0f;
    public float force = 100;
    Transform father, grandfather;
    private void Start()
    {
        if(transform.parent != null)
        {
            father = transform.parent;
            if (father != null && father.parent != null)
                grandfather = father.parent;
        }
    }
    private void OnParticleCollision(GameObject other)
    {
        if (ContainsFire(other.transform))
            return;

        Rigidbody body = other.GetComponent<Rigidbody>();
        other.SendMessage("ApplyDamageWithPain", damagepp, SendMessageOptions.DontRequireReceiver);
        other.SendMessage("WarmUp", Temperature, SendMessageOptions.DontRequireReceiver);

        if (body)
        {
            Vector3 direction = other.transform.position - transform.position;
            direction = direction.normalized;
            body.AddForce(direction * force);
        }
    }

        bool ContainsFire(Transform t)
        {
            if (grandfather != null && 
                (grandfather == t
                || (grandfather.GetComponent<CanBeOnFire>() != null && grandfather.GetComponent<CanBeOnFire>().SubPieces.Contains(t))
                || (grandfather.GetComponent<CanBeOnFireNet>() != null && grandfather.GetComponent<CanBeOnFireNet>().SubPieces.Contains(t))
                || (grandfather.GetComponent<CanBeOnFireSlave>() != null
                        && grandfather.GetComponent<CanBeOnFireSlave>().Master != null
                        && grandfather.GetComponent<CanBeOnFireSlave>().Master.SubPieces.Contains(t))
                || (grandfather.GetComponent<CanBeOnFireSlave>() != null
                        && grandfather.GetComponent<CanBeOnFireSlave>().MasterNet != null
                        && grandfather.GetComponent<CanBeOnFireSlave>().MasterNet.SubPieces.Contains(t))))
                return true;
            else
                return false;
        }
    }