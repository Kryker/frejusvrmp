﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenVRSwinger : GenericSwinger {

    public InputManagementNet _inputManagerNet;
    public InputManagement _inputManager;

    public override void EventGeneration()
    {
    }

    public override bool EventSubscription()
    {
        if (_inputManager != null)
        {
            switch (_swingButton)
            {
                case ControllerButton.Trigger:
                    _inputManager.OnLeftTriggerClicked += LeftButtonPressed;
                    _inputManager.OnLeftTriggerUnclicked += LeftButtonUnpressed;
                    _inputManager.OnRightTriggerClicked += RightButtonPressed;
                    _inputManager.OnRightTriggerUnclicked += RightButtonUnpressed;
                    break;
                case ControllerButton.TouchPad:
                    _inputManager.OnLeftPadPressed += LeftButtonPressed;
                    _inputManager.OnLeftPadUnpressed += LeftButtonUnpressed;
                    _inputManager.OnRightPadPressed += RightButtonPressed;
                    _inputManager.OnRightPadUnpressed += RightButtonUnpressed;
                    break;
                default:
                    _inputManager.OnLeftGripped += LeftButtonPressed;
                    _inputManager.OnLeftUngripped += LeftButtonUnpressed;
                    _inputManager.OnRightGripped += RightButtonPressed;
                    _inputManager.OnRightUngripped += RightButtonUnpressed;
                    break;
            }
            return true;
        }
        else if (_inputManagerNet != null)
            {
                switch (_swingButton)
                {
                    case ControllerButton.Trigger:
                    _inputManagerNet.OnLeftTriggerClickedOffline += LeftButtonPressed;
                    _inputManagerNet.OnLeftTriggerUnclickedOffline += LeftButtonUnpressed;
                    _inputManagerNet.OnRightTriggerClickedOffline += RightButtonPressed;
                    _inputManagerNet.OnRightTriggerUnclickedOffline += RightButtonUnpressed;
                        break;
                    case ControllerButton.TouchPad:
                    _inputManagerNet.OnLeftPadPressedOffline += LeftButtonPressed;
                    _inputManagerNet.OnLeftPadUnpressedOffline += LeftButtonUnpressed;
                    _inputManagerNet.OnRightPadPressedOffline += RightButtonPressed;
                    _inputManagerNet.OnRightPadUnpressedOffline += RightButtonUnpressed;
                        break;
                    default:
                    _inputManagerNet.OnLeftGrippedOffline += LeftButtonPressed;
                    _inputManagerNet.OnLeftUngrippedOffline += LeftButtonUnpressed;
                    _inputManagerNet.OnRightGrippedOffline += RightButtonPressed;
                    _inputManagerNet.OnRightUngrippedOffline += RightButtonUnpressed;
                        break;
                }
                return true;
            }
        return false;
    }
}
