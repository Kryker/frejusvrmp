﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class NPCController : MonoBehaviour {

    public Animator animator;
    [HideInInspector]
    public NavMeshAgent agent;
    bool _CanInteract = true;
    bool outlined = false;
    public List<Transform> Pieces;
    public uint NPCID;
    
    public virtual bool CanInteract()
    {
        return _CanInteract;
    }

    public virtual void CanInteract(bool can)
    {
        _CanInteract = can;
        if (!can && outlined)
            DisableOutline();
    }

    public virtual void EnableOutline()
    {
        foreach (Transform t in Pieces)
        {
            var mts = t.GetComponent<SkinnedMeshRenderer>().materials;
            foreach (Material m in mts)
            {
                var c = m.GetColor("_OutlineColor");
                c.a = 255;
                m.SetColor("_OutlineColor", c);
            }
        }
        outlined = true;
    }

    public abstract void Interact(ItemController c, ControllerHand hand);
    public abstract void Interact(ItemControllerNet c, ControllerHand hand);

    public virtual void DisableOutline()
    {
        foreach (Transform t in Pieces)
        {
            var mts = t.GetComponent<SkinnedMeshRenderer>().materials;
            foreach (Material m in mts)
            {
                var c = m.GetColor("_OutlineColor");
                c.a = 0;
                m.SetColor("_OutlineColor", c);
            }
        }
        outlined = false;
    }


    public virtual void Reset()
    {
        DisableOutline();
        CanInteract(false);
    }

}
