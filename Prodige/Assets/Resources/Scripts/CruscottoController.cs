﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CruscottoController : MonoBehaviour
{

    public VehicleData Vehicle;
    public Transform LancettaKM, LancettaGiri, LancettaBenzina, LancettaTemp, LancettaOlio;
    float TargetTemp, TargetOlio, TargetBenzina;
    public Transform CanvasDate, CanvasKm;
    int currgear = 2;
    float[] gears = new float[3] { 4.23f, 2.47f, 1.67f };
    bool TurnedOff = false;
    // Use this for initialization
    void Start()
    {
        if (Vehicle != null)
            TargetBenzina = -Vehicle.FuelTankPercentage;
        TargetOlio = 60;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vehicle != null && Vehicle.EngineOn)
        {
            var tempangle = Vehicle.VehicleBody.GetComponent<CanBeOnFireSlave>().GetTemperature() - 45;
            if (tempangle > 100)
                tempangle = 100;
            else if (tempangle < 1)
                tempangle = 1;
            TargetTemp = tempangle;
            LancettaKM.localRotation = Quaternion.Euler(LancettaKM.localEulerAngles.x, LancettaKM.localEulerAngles.y, Vehicle.GetComponent<Rigidbody>().velocity.magnitude * 3.6f);
            LancettaGiri.localRotation = Quaternion.Euler(LancettaGiri.localEulerAngles.x, LancettaGiri.localEulerAngles.y, Vehicle.GetComponent<Rigidbody>().velocity.magnitude / (2 * Mathf.PI * 0.34f / 60) * gears[currgear] * 0.04f);
        }
        UpdateLancette();
    }

    private void UpdateLancette()
    {
        var l = LancettaTemp.localEulerAngles.z;
        l = l % 360;
        if (l < 0)
            l = 360 - l;

        if (l < TargetTemp)
        {
            LancettaTemp.Rotate(Vector3.forward, 1);
            l = LancettaTemp.localEulerAngles.z;
            l = l % 360;
            if (l < 0)
                l = 360 - l;
            if (l > TargetTemp)
                LancettaTemp.localRotation = Quaternion.Euler(LancettaTemp.localEulerAngles.x, LancettaTemp.localEulerAngles.y, TargetTemp);
        }
        else if (l > TargetTemp)
        {
            LancettaTemp.Rotate(Vector3.back, 1);
            l = LancettaTemp.localEulerAngles.z;
            l = l % 360;
            if (l < 0)
                l = 360 - l;
            if (l < TargetTemp)
                LancettaTemp.localRotation = Quaternion.Euler(LancettaTemp.localEulerAngles.x, LancettaTemp.localEulerAngles.y, TargetTemp);
        }

        l = LancettaOlio.localEulerAngles.z;
        l = l % 360;
        if (l < 0)
            l = 360 - l;

        if (l < TargetOlio)
        {
            LancettaOlio.Rotate(Vector3.forward, 1);
            l = LancettaOlio.localEulerAngles.z;
            l = l % 360;
            if (l < 0)
                l = 360 - l;
            if (l > TargetOlio)
                LancettaOlio.localRotation = Quaternion.Euler(LancettaOlio.localEulerAngles.x, LancettaOlio.localEulerAngles.y, TargetOlio);
        }
        else if (l > TargetOlio)
        {
            LancettaOlio.Rotate(Vector3.back, 1);
            l = LancettaOlio.localEulerAngles.z;
            l = l % 360;
            if (l < 0)
                l = 360 - l;
            if (l < TargetOlio)
                LancettaOlio.localRotation = Quaternion.Euler(LancettaOlio.localEulerAngles.x, LancettaOlio.localEulerAngles.y, TargetOlio);
        }

        l = LancettaBenzina.localEulerAngles.z;
        l = l % 360;
        if (l > 0)
            l = -360 + l;

        if (l < TargetBenzina)
        {
            LancettaBenzina.Rotate(Vector3.forward, 1);
            l = LancettaBenzina.localEulerAngles.z;
            l = l % 360;
            if (l > 0)
                l = -360 + l;
            if (l > TargetBenzina)
                LancettaBenzina.localRotation = Quaternion.Euler(LancettaBenzina.localEulerAngles.x, LancettaBenzina.localEulerAngles.y, TargetBenzina);
        }
        else if (l > TargetBenzina)
        {
            LancettaBenzina.Rotate(Vector3.back, 1);
            l = LancettaBenzina.localEulerAngles.z;
            l = l % 360;
            if (l > 0)
                l = -360 + l;
            if (l < TargetBenzina)
                LancettaBenzina.localRotation = Quaternion.Euler(LancettaBenzina.localEulerAngles.x, LancettaBenzina.localEulerAngles.y, TargetBenzina);
        }
    }

    public void TurnOn()
    {
        if (TurnedOff)
        {
            CanvasDate.gameObject.SetActive(true);
            CanvasKm.gameObject.SetActive(true);
            if (Vehicle != null)
                TargetBenzina = -Vehicle.FuelTankPercentage;
            TargetOlio = 60;
            TurnedOff = false;
        }
    }
    public void TurnOff()
    {
        if (!TurnedOff)
        {
            CanvasDate.gameObject.SetActive(false);
            CanvasKm.gameObject.SetActive(false);
            TargetTemp = 1;
            TargetBenzina = -1;
            TargetOlio = 1;
            TurnedOff = true;
        }
    }
}
