﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpOnCameras : MonoBehaviour {

    public ServerCamera[] Cameras;
    int currcamera = 0;
    public Image BlackScreen;
    Coroutine faderoutine;
    public int StartingCamera = 0;
    public Text CameraName;
    bool switching = false;

    private void FadeOut(float time)
    {
        if (faderoutine != null)
            StopCoroutine(faderoutine);
        faderoutine = StartCoroutine(PlayerStatusNet.FPSFadeOut(BlackScreen.GetComponent<Image>(), time, Time.time));
    }

    private void FadeIn(float time)
    {
        if (faderoutine != null)
            StopCoroutine(faderoutine);
        faderoutine = StartCoroutine(PlayerStatusNet.FPSFadeIn(BlackScreen.GetComponent<Image>(), time, Time.time));
    }

    IEnumerator GoToCamera(int index)
    {            
            FadeOut(.35f);
            if (index != currcamera && Cameras[currcamera].Shelter != null)
                Cameras[currcamera].Shelter.GetOutFromShelter(.05f);
            yield return new WaitForSeconds(.05f);
            //Cameras[index].GetComponent<Camera>().enabled = true;
            SnapToCamera(index);
            FadeIn(.05f);
            if (Cameras[index].Shelter != null)
                Cameras[index].Shelter.GetInsideShelter(.05f);
           // Cameras[currcamera].GetComponent<Camera>().enabled = false;
            currcamera = index;
            yield return new WaitForSeconds(.05f);
            switching = false;
    }

    private void OnEnable()
    {
        FadeOut(0);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        StartCoroutine(GoToCamera(StartingCamera));
    }

    private void SnapToCamera(int v)
    {
        if (Cameras != null)
        {
            transform.position = Cameras[v].transform.position;
            transform.rotation = Cameras[v].transform.rotation;
            CameraName.text = Cameras[v].Name;
        }
    }

    internal void NextCamera()
    {

        if (Cameras != null && !switching)
        {
            switching = true;
            int nextcamera = currcamera;
            if (currcamera == Cameras.Length-1)
                nextcamera = 0;
            else
                nextcamera++;
            StartCoroutine(GoToCamera(nextcamera));
        }
    }

    internal void PrevCamera()
    {
        if (Cameras != null && !switching)
        {
            switching = true;
            int nextcamera = currcamera;
            if (currcamera == 0)
                nextcamera = Cameras.Length-1;
            else
                nextcamera--;
            StartCoroutine(GoToCamera(nextcamera));
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
