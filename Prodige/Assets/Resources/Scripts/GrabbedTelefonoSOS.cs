﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbedTelefonoSOS : GrabbableItem
{
    [HideInInspector]
    public AudioSource audiosource;
    public float newMinDistance = 0.4f;
    public float newMaxDistance = 0.5f;
    public float DropDistance = 1.0f;
    public Vector3 SpeakerPos;
    public TelefonoSOS Telefono
    {
        get { return _tel; }
        set
        {
            _tel = value;
            if (_tel != null)
            { 
				audiosource = _tel.pulsante.Speaker;
				bool paused = false;
				if (audiosource.isPlaying)
				{
					paused = true;
					audiosource.Pause();
				}				
				if (Slave != null)
					audiosource.transform.parent = Slave.transform;
				else
					audiosource.transform.parent = transform;
				
				audiosource.transform.localPosition = SpeakerPos;
				audiosource.maxDistance = newMaxDistance;
				audiosource.minDistance = newMinDistance;
				if (paused)
					audiosource.UnPause();
				else
					_tel.pulsante.ForceCall();
                _tel.OnBase = false;
            }
        }
    }
    TelefonoSOS _tel;


    public override void LoadState(GenericItem i)
    {
        var e = i as TelefonoSOS;
        Telefono = e;
        base.LoadState(i);
    }

    public override void SaveState()
    {
        /*if(Tel.OnBase)
             {*/
        if (audiosource != null)
        {
			bool paused = false;
			if (audiosource.isPlaying)
			{
				paused = true;
				audiosource.Pause();
			}
			if (Telefono.pulsante.Slave == null)
				audiosource.transform.parent = Telefono.pulsante.transform;
			else
				audiosource.transform.parent = Telefono.pulsante.Slave.transform;
            audiosource.transform.localPosition = _tel.pulsante.StartSpeakerPos;
            audiosource.maxDistance = _tel.pulsante.MaxDist;
            audiosource.minDistance = _tel.pulsante.MinDist;
            if (paused)
                audiosource.UnPause();
            audiosource = null;
        }
        Telefono.ReBase();
        /*}
   else
        {
            Tel.Drop();
        }*/

        base.SaveState();
        /*if (gameObject.activeSelf)
            StartCoroutine(PlayAndDisable());*/
    }

    /* private IEnumerator PlayAndDisable()
     {
         if (source != null)
         {
             source.clip = Drop;
             source.Play();
             yield return new WaitWhile(() => source.isPlaying);
         }
     }*/
     
    // Use this for initialization
    
    public override void Start()
    {
        base.Start();
    }

    public override void ClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override void UnClickButton(object sender, ClickedEventArgs e)
    {
    }

    public override bool CanDrop()
    {
        if (Telefono.OnBase || Vector3.Distance(Telefono.CableStart.transform.position, Telefono.CableEnd.transform.position) > DropDistance)
            return true;
        else
            return false;
    }

    // Update is called once per frame
    public override void Update()
    {
        if (initialized && Telefono != null)
            if (!Telefono.OnBase)
            {
                if (Vector3.Distance(Telefono.CableStart.transform.position, Telefono.CableEnd.transform.position) > DropDistance)
                {
                    if (GameManager.Instance != null)
                        GameManager.Instance.TC.GetPlayerController().GetComponent<ItemController>().DropItem(Telefono.transform, true);
                }
            }
    }
}
